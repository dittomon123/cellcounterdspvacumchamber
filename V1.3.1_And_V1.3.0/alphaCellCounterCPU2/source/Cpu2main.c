/******************************************************************************/
/*! \file Cpu2main.c
 *
 *  \brief Main function of Cell counter module
 *
 *  \b Description:
 *			This file contains, initialization of CPU, CPU peripherals,
 *			MBD module and Algorithm
 *
 *   $Version: $ 0.1
 *   $Date:    $ Jan-12-2015
 *   $Author:  $ MSK,20040919, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_main_module
//! @{
//******************************************************************************

#include <Cpu2SystemInitialize.h>
#include "F28x_Project.h"     // Device Headerfile and Examples Include File

extern volatile uint16_t usnTimer2Flag;
extern XINT_FLAGS st_XintFlags;
extern void SysHandleVolumetricTubeStatus(void);
extern unsigned int g_usnCurrentModeCPU2;
extern Uint16 g_usnAlgoExecutionTime;
extern DmaAdcParameters DmaAdcParams;
#define FOR_EVER	1
/******************************************************************************/
/*!
 * \fn void main(void)
 * \brief main function of cell counter
 * \brief Description:
 *      This function calls initialization functions and sequence of operations
 *      based on cpu timer
 * \return void
 */
/******************************************************************************/
void main(void)
{
    //!Initialize all the parameters
    SysSystemInitialize();

	while(FOR_EVER)
	{
	    //CRH_PDA - START

	    //!Function to check if any IPC command has come from CPU1
	    SysIpcCommModule();

	    //!Send the analyzed data only after completion of the data acquisition.
	    //! So that the RBC and WBC volumetric interrupt shall be handled at CPU1
	    if(FALSE == st_XintFlags.m_SampleStartFlag)
	    {
            //!Check whether the WBC or RBC/PLT analysis completed, If it is
            //! completed send the results to CPU1 depending upon the mode selected
            if((TRUE == st_XintFlags.m_bWBC_SampleAnalysisFlagEnd) || \
                    (TRUE == st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd))
            {
                switch(g_usnCurrentModeCPU2)
                {
                    case WHOLE_BLOOD_MODE:
                    case PRE_DILUENT_MODE:
                    case START_UP_SEQUENCE_CMD:
                    case AUTO_CALIBRATION_WB:
                    case QUALITY_CONTROL_WB:
                    case QUALITY_CONTROL_BODY_FLUID:
                    case STARTUP_FROM_SERVICE_SCREEN_CMD:
                    case STARTUP_FAILED_CMD:
                        //!If any of the above modes are selected and if WBC, RBC & PLT
                        //! analysis is completed, send the results to CPU1
                        if((TRUE == st_XintFlags.m_bWBC_SampleAnalysisFlagEnd) && \
                                (TRUE == st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd))
                        {
                            st_XintFlags.m_bWBC_SampleAnalysisFlagEnd = FALSE;
                            st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd = FALSE;
                            g_usnAlgoExecutionTime = CpuTimer2.InterruptCount - \
                                    g_usnAlgoExecutionTime;
                            SysIpcSendAnalysisInfo(FIRST_COUNT_CYCLE);
                            DmaAdcParams.SampleAnalysisState = DATA_ANALYSIS_COMPLETE_STATE;
                        }
                        break;

                    case BODY_FLUID_MODE:
                    case COMMERCIAL_CALIBRATION_WBC:
                    case PARTIAL_BLOOD_COUNT_WBC:
                        //!If any of the above modes are selected and if WBC,
                        //! analysis is completed, send the results to CPU1
                        if(TRUE == st_XintFlags.m_bWBC_SampleAnalysisFlagEnd)
                        {
                            st_XintFlags.m_bWBC_SampleAnalysisFlagEnd = FALSE;
                            g_usnAlgoExecutionTime = CpuTimer2.InterruptCount - \
                                    g_usnAlgoExecutionTime;
                            SysIpcSendAnalysisInfo(FIRST_COUNT_CYCLE);
                            DmaAdcParams.SampleAnalysisState = DATA_ANALYSIS_COMPLETE_STATE;
                        }
                        break;

                    case COMMERCIAL_CALIBRATION_RBC:
                    case COMMERCIAL_CALIBRATION_PLT:
                    case PARTIAL_BLOOD_COUNT_RBC_PLT:
                        //!If any of the above modes are selected and if RBC & PLT
                        //! analysis is completed, send the results to CPU1
                        if(TRUE == st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd)
                        {
                            st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd = FALSE;
                            g_usnAlgoExecutionTime = CpuTimer2.InterruptCount - \
                                    g_usnAlgoExecutionTime;
                            SysIpcSendAnalysisInfo(FIRST_COUNT_CYCLE);
                            DmaAdcParams.SampleAnalysisState = DATA_ANALYSIS_COMPLETE_STATE;
                        }
                        break;

                    default:
                        break;
                }
            }
	    }

	    //!Check if CPU1 has indicated to monitor the Volumetric interrupt
	    if(st_XintFlags.m_SampleStartFlag)
        {
            //!When ADC sampling update the status of Volumetric
	        SysHandleVolumetricTubeStatus();
        }

	    //!Check whether the WBC acquisition is completed. If it is completed,
	    //! start analyzing the data
        if(TRUE == st_XintFlags.m_bWBC_SampleAnalysisFlagStart)
        {
            DAWBCCellDataSampleAnalysis();
        }

        //!Check whether the RBC_PLT acquisition is completed. If it is completed,
        //! start analyzing the data
        if(TRUE == st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart)
        {
            DARBCPLTCellDataSampleAnalysis();
        }
	}

}
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

/******************************************************************************/
/*! \file Cpu2ConfigSDRAM.c
 *
 *  \brief CPU2 SDRAM configuration for CPU1 & CPU2 ownership.
 *
 *  \b Description:
 *      In this file, SDRAM configuration for CPU1 & CPU2 ownership
 *      Also reads the status of which CPU holding ownership of SDRAM
 *
 *
 *   $Version: $ 0.1
 *   $Date:    $ Nov-09-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_sdram_config_module
//! @{
//******************************************************************************
#include <Cpu2ConfigSDRAM.h>

/******************************************************************************/
/*!
 * \fn int16 CSConfigEmif1CPU(Uint16 CpuConfig)
 * \param CpuConfig CPU2 cofigures for emif1 R/W control
 * \brief EMIF1 CPU configuration
 * \brief  Description:
 *      This function provided R/W access to the CPU2 core
 *
 * \return 0 if Success -1 if Failure
 */
/******************************************************************************/

int16 CSConfigEmif1CPU(Uint16 CpuConfig)
{
    if(CpuConfig == 2)
    {
    	EALLOW;
    	//! Grab EMIF1 For CPU2
    	Emif1ConfigRegs.EMIF1MSEL.all = 0x93A5CE72;
    	if (Emif1ConfigRegs.EMIF1MSEL.all != 0x2)
    	{
    		return (-1);
    	}
    	EDIS;
    	return (0);

    }
    else
    {
    	EALLOW;
    	//! CPU1 is master but not grabbed
    	Emif1ConfigRegs.EMIF1MSEL.all = 0x93A5CE70;
    	if (Emif1ConfigRegs.EMIF1MSEL.all != 0x0)
    	{
    		return (-1);
    	}
    	EDIS;
    	return (0);
    }
}
/******************************************************************************/
/*!
 * \fn Uint16 CSGetEmif1Status(void)
 *
 * \brief EMIF1 status
 * \brief  Description:
 *      This function read EMIF1MSEL bit and returns the value
 *
 * \return 0 CPU1 is master but not grabbed
 * 		   1 CPU1 Master
 * 		   2 CPU2 master
 * 		   3 CPU1 is master but not grabbed
 */
/******************************************************************************/
Uint16 CSGetEmif1Status(void)
{
	Uint16 Emif1Sel = 0xFFFF;
	Emif1Sel = (Uint16)Emif1ConfigRegs.EMIF1MSEL.all;
	return Emif1Sel;
}
/******************************************************************************/
/*!
 * \fn void CSSdramWriteWORD(Uint32 MemAddr, Uint16 Data)
 * \param Data	Data to be written to the memory
 * \param MemAddr	Location/Address of the memory
 * \brief To write word to the memory address
 * \brief Description:
 *      This function writes data to the specific address passed
 *		Function call:
 *		__addr32_write_uint16(MemAddr, Data);
 * \return void
 */
/******************************************************************************/
void CSSdramWriteWORD(Uint32 MemAddr, Uint16 Data)
{
	__addr32_write_uint16(MemAddr, Data);
}
/******************************************************************************/
/*!
 * \fn void CSSdramWriteDWORD(Uint32 MemAddr,Uint32 Data)
 * \param Data	Data to be written to the memory
 * \param MemAddr	Location/Address of the memory
 * \brief To write word to the memory address
 * \brief Description:
 *      This function writes data to the specific address passed
 *		Function call:
 *		__addr32_write_uint32(MemAddr, Data);
 * \return void
 */
/******************************************************************************/
void CSSdramWriteDWORD(Uint32 MemAddr,Uint32 Data)
{
	__addr32_write_uint32(MemAddr, Data);
}
/******************************************************************************/
/*!
 * \fn Uint16 CSSdramReadWORD(Uint32 MemAddr)
 *
 * \param MemAddr	Location/Address of the memory
 * \brief To read word to the memory address
 * \brief Description:
 *      This function read data to the specific address passed
 *		Function call:
 *		__addr32_read_uint16(MemAddr, Data);
 * \return Data read from sdram memory address
 */
/******************************************************************************/
Uint16 CSSdramReadWORD(Uint32 MemAddr)
{
	return __addr32_read_uint16(MemAddr);
}
/******************************************************************************/
/*!
 * \fn Uint32 CSSdramReadDWORD(Uint32 MemAddr)
 *
 * \param MemAddr	Location/Address of the memory
 * \brief To read word to the memory address
 * \brief Description:
 *      This function read data to the specific address passed
 *		Function call:
 *		__addr32_read_uint32(MemAddr, Data);
 * \return Data read from sdram memory address
 */
/******************************************************************************/
Uint32 CSSdramReadDWORD(Uint32 MemAddr)
{
	return __addr32_read_uint32(MemAddr);
}
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

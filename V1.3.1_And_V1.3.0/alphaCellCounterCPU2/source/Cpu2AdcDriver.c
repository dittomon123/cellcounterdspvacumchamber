/******************************************************************************/
/*! \file Cpu2AdcDriver.c
 *
 *  \brief Analog input channel configuration for sensor data Aquisition.
 *	\brief
 *  \b Description:
 *      In this file the comple Analog input channel Drivers configuration
 *      and it's sub-sequent
 *      functions definition has been implemented.
 *
 *   $Version: $ 0.1
 *   $Date:    $ Jul-21-2015
 *   $Author:  $ ADITYA B G, GURUDUTT R
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_data_acquisition_module
//! @{
//******************************************************************************
#include <Cpu2AdcDriver.h>
#include<stdio.h>
#include<stdlib.h>
#include"Cpu2ConfigSDRAM.h"
#include"Cpu2DataAcquisition.h"
#include "Cpu2InitBoard.h"

#define SDRAM_DMA_DISABLE									0

/******************************************************************************/
/*!
  * \brief DataAqParam structure that defines the ADC data aquisition instance
 *  these fields are used by ADC wrapper functions
 * \struct StructDataAqParam
 * \brief StructDataAqParam instance creation to access ADCSampleCounter &
 * 	Aquisition status
 */
/******************************************************************************/
DataAqParam StructDataAqParam;
#pragma DATA_SECTION(usnAdcDataCapture,".RAMGS4_Cpu2AdcBufCapture");
Uint16 usnAdcDataCapture[4096];

/******************************************************************************/
/*!
 *
 * \fn void BDAPeakDetectionAlgorithm(unsigned int fVoltage)
 * \brief External function definition
 */
/******************************************************************************/

extern void BDAPeakDetectionAlgorithm(unsigned int fVoltage);

extern void SysResetCellDataAnalysisParams(Uint16 AnalysisCycle);

extern void Start_Timer2(void);
extern void Stop_Timer2(void);

/******************************************************************************/
/*!
 * \fn DataAqParam* ADGetStructDataAqParam(void)
 *
 * \brief  Description: Function to get the data praramter of StructDataAqParam
 *          instance to access ADCSampleCounter & Aquisition status
 * \return StructDataAqParam structure address
 */
/******************************************************************************/
DataAqParam* ADGetStructDataAqParam(void)
{
	return  &StructDataAqParam;

}
/******************************************************************************/
/*!
 * \fn void ADAdca_Init(void)
 * \brief ADCA block initialization
 * 		Description:
 *      This function initialize ADCA registers and associated SOC registers
 *		Calls ADConfigureAdca(), ADConfigureAdcaEpwm(),ADSetupAdcaEpwm()
 * \return void
 */
/******************************************************************************/

void ADAdca_Init(void)
{
//	EnableInternalTemperatureSense();
    ADConfigureAdca();
    ADConfigureAdcaEpwm();
    ADSetupAdcaEpwm();


//!  Map ISR functions
    EALLOW;
    PieVectTable.ADCA1_INT = &adca1_isr;
    EDIS;

    EALLOW;

	IER |= M_INT1;                      //! Enable CPU Interrupt 1

	IER |= M_INT10;

	EINT;                               //! Enable Global interrupt INTM
	EDIS;

	EALLOW;
	  //! Sync ePWM
	CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;

	//To get ADC interrupt and each sample processing purpose
//	PieCtrlRegs.PIEIER1.bit.INTx1 = 1;	//! Enable INT 1 in the PIE
#if !SDRAM_DMA_DISABLE
	PieCtrlRegs.PIEIER1.bit.INTx1 = 0;	//! Enable INT 1 in the PIE
#else
	PieCtrlRegs.PIEIER1.bit.INTx1 = 0;	//! Enable INT 1 in the PIE
#endif
	EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADConfigureAdca(void)
 * \brief To configure ADCA block registers
 *  Description:
 *      This function configures adc resolution, signal mode, clock
 * \return void
 */
/******************************************************************************/
void ADConfigureAdca(void)
{
    EALLOW;
    //! set ADCCLK divider to /4
    AdcaRegs.ADCCTL2.bit.PRESCALE = 6;
    //! 12-bit resolution
    AdcaRegs.ADCCTL2.bit.RESOLUTION = 1;
    //! single-ended channel conversions (12-bit mode only)
    AdcaRegs.ADCCTL2.bit.SIGNALMODE = 1;
    //! Set pulse positions to late
    AdcaRegs.ADCCTL1.bit.INTPULSEPOS = 1;
    //! power up the ADC
    AdcaRegs.ADCCTL1.bit.ADCPWDNZ = 1;
    //! delay for 1ms to allow ADC time to power up
    DELAY_US(1000);
    EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADConfigureAdcaEpwm(void)
 * \brief To configure ADCA block for pwm trigger
 *   Description:
 *      This function configures pwm soc to trigger SOC
 * \return void
 */
/******************************************************************************/
void ADConfigureAdcaEpwm(void)
{
#ifdef _ALPHA_2_BOARD_		//CRH_TEST - START
#if 1
	EALLOW;
	//! Assumes ePWM clock is already enabled
	//! freeze counter
	EPwm9Regs.TBCTL.bit.CTRMODE = 3;
	//! TBCLK pre-scaler = /1
	EPwm9Regs.TBCTL.bit.HSPCLKDIV = 0;
	//! Set period to 4000 counts (50kHz)
//	    EPwm3Regs.TBPRD = 0x0FA0;
	//! Set compare A value to 2048 counts
//	    EPwm3Regs.CMPA.bit.CMPA = 0x0800;
	//! Set period to 4000 counts (50kHz)
	EPwm9Regs.TBPRD = 0x66;
	//! Set compare A value to 0x33 counts
//    EPwm3Regs.CMPA.half.CMPA = 0x0800;
	EPwm9Regs.CMPA.bit.CMPA = 0x33;

	//! Disable SOC on A group
	EPwm9Regs.ETSEL.bit.SOCAEN  = 0;
	//! Select SOC on up-count
	EPwm9Regs.ETSEL.bit.SOCASEL = 4;
	//! enable SOCA
	EPwm9Regs.ETSEL.bit.SOCAEN = 1;
	//! Generate pulse on 1st event
	EPwm9Regs.ETPS.bit.SOCAPRD = 1;
	EPwm9Regs.TBCTL.bit.CTRMODE = 3;
	EDIS;
#endif

#if 0
	EALLOW;
	//! Assumes ePWM clock is already enabled
	EPwm10Regs.TBCTL.bit.CTRMODE = 3;            //! freeze counter
	EPwm10Regs.TBCTL.bit.HSPCLKDIV = 0;          //! TBCLK pre-scaler = /1
//	    EPwm3Regs.TBPRD = 0x0FA0;                   //! Set period to 4000 counts (50kHz)
//	    EPwm3Regs.CMPA.bit.CMPA = 0x0800;          	//! Set compare A value to 2048 counts
	EPwm10Regs.TBPRD = 0x66;                   	//! Set period to 4000 counts (50kHz)
//    EPwm3Regs.CMPA.half.CMPA = 0x0800;          	//! Set compare A value to 0x33 counts
	EPwm10Regs.CMPA.bit.CMPA = 0x33;

	EPwm10Regs.ETSEL.bit.SOCAEN  = 0;            //! Disable SOC on A group
	EPwm10Regs.ETSEL.bit.SOCASEL = 4;            //! Select SOC on up-count
	EPwm10Regs.ETSEL.bit.SOCAEN = 1;             //! enable SOCA
	EPwm10Regs.ETPS.bit.SOCAPRD = 1;             //! Generate pulse on 1st event
	EPwm10Regs.TBCTL.bit.CTRMODE = 3;
	EDIS;
#endif
#if 0
	EALLOW;
	//! Assumes ePWM clock is already enabled
	EPwm3Regs.TBCTL.bit.CTRMODE = 3;            //! freeze counter
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = 0;          //! TBCLK pre-scaler = /1
//	    EPwm3Regs.TBPRD = 0x0FA0;                   //! Set period to 4000 counts (50kHz)
//	    EPwm3Regs.CMPA.bit.CMPA = 0x0800;          	//! Set compare A value to 2048 counts
	EPwm3Regs.TBPRD = 0x66;                   	//! Set period to 4000 counts (50kHz)
//    EPwm3Regs.CMPA.half.CMPA = 0x0800;          	//! Set compare A value to 0x33 counts
	EPwm3Regs.CMPA.bit.CMPA = 0x33;

	EPwm3Regs.ETSEL.bit.SOCAEN  = 0;            //! Disable SOC on A group
	EPwm3Regs.ETSEL.bit.SOCASEL = 4;            //! Select SOC on up-count
	EPwm3Regs.ETSEL.bit.SOCAEN = 1;             //! enable SOCA
	EPwm3Regs.ETPS.bit.SOCAPRD = 1;             //! Generate pulse on 1st event
	EPwm3Regs.TBCTL.bit.CTRMODE = 3;
	EDIS;
#endif
#endif		//CRH_TEST - END
}

/******************************************************************************/
/*!
 * \fn void ADSetupAdcaEpwm(void)
 * \brief To configure PWM trigger selection
 *      Description:
 *      This function configures pwm trigger selection , aquisition window
 * \return void
 */
/******************************************************************************/
void ADSetupAdcaEpwm(void)
{
    EALLOW;
    //! SOC0 will convert pin A0
    AdcaRegs.ADCSOC0CTL.bit.CHSEL = 0;
    //! sample window is 15 SYSCLK cycles
    AdcaRegs.ADCSOC0CTL.bit.ACQPS = 14;
#ifdef _ALPHA_2_BOARD_
    //! trigger on ePWM9 SOCA/C
    AdcaRegs.ADCSOC0CTL.bit.TRIGSEL = 0x15;
#else
    //! trigger on ePWM3 SOCA/C
    AdcaRegs.ADCSOC0CTL.bit.TRIGSEL = 9;
#endif

    //! SOC13 will convert pin A13
//    AdcaRegs.ADCSOC13CTL.bit.CHSEL = 13;
    //! sample window is 15 SYSCLK cycles
//    AdcaRegs.ADCSOC13CTL.bit.ACQPS = 14;
    //! trigger on ePWM3 SOCA/C
//    AdcaRegs.ADCSOC13CTL.bit.TRIGSEL = 9;

    //! end of SOC0 will set INT1 flag
    AdcaRegs.ADCINTSEL1N2.bit.INT1SEL = 0;
    //! enable INT1 flag
    AdcaRegs.ADCINTSEL1N2.bit.INT1E = 1;
    //! make sure INT1 flag is cleared
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
    AdcaRegs.ADCINTSEL1N2.bit.INT1CONT = 1;
}
/******************************************************************************/
/*!
 * \fn void ADAdcb_Init(void)
 * \brief ADCB block initialization
 *      Description:
 *      This function initialize ADCB registers and associated SOC registers
 *		Calls ADConfigureAdcb(), ADConfigureAdcbEpwm(),ADSetupAdcbEpwm()
 * \return void
 */
/******************************************************************************/
void ADAdcb_Init(void)
{
    ADConfigureAdcb();
    ADConfigureAdcbEpwm();
    ADSetupAdcbEpwm();

//!  Map ISR functions
    EALLOW;
    PieVectTable.ADCB1_INT = &adcb1_isr;
    EDIS;

	EALLOW;
	//! Enable CPU Interrupt 1
	IER |= M_INT1;
	IER |= M_INT10;
	//! Enable Global interrupt INTM
	EINT;
	//    ERTM;
	EDIS;

	EALLOW;
	  //! Sync ePWM
	CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;

	//! Enable INT 2 in the PIE
	PieCtrlRegs.PIEIER1.bit.INTx2 = 0;
//	PieCtrlRegs.PIEIER1.all = 0xFFFF;
}
/******************************************************************************/
/*!
 * \fn void ADConfigureAdcb(void)
 * \brief To configure ADCB block registers
 * 		Description:
 *      This function configures adc resolution, signal mode, clock
 * \return void
 */
/******************************************************************************/
void ADConfigureAdcb(void)
{
    EALLOW;
    //! set ADCCLK divider to /4
    AdcbRegs.ADCCTL2.bit.PRESCALE = 6;
    //! 12-bit resolution
    AdcbRegs.ADCCTL2.bit.RESOLUTION = 1;
    //! single-ended channel conversions (12-bit mode only)
    AdcbRegs.ADCCTL2.bit.SIGNALMODE = 1;
    //! Set pulse positions to late
    AdcbRegs.ADCCTL1.bit.INTPULSEPOS = 1;
    //! power up the ADC
    AdcbRegs.ADCCTL1.bit.ADCPWDNZ = 1;
    //! delay for 1ms to allow ADC time to power up
    DELAY_US(1000);
    EDIS;

}
/******************************************************************************/
/*!
 * \fn void ADConfigureAdcbEpwm(void)
 * \brief To configure ADCB block for pwm trigger
 * 		Description:
 *      This function configures pwm soc to trigger SOC
 * \return void
 */
/******************************************************************************/
void ADConfigureAdcbEpwm(void)
{
		EALLOW;
	    //! Assumes ePWM clock is already enabled
		//! freeze counter
	    EPwm3Regs.TBCTL.bit.CTRMODE = 3;
	    //! TBCLK pre-scaler = /1
	    EPwm3Regs.TBCTL.bit.HSPCLKDIV = 0;
	    //! Set period to 4000 counts (50kHz)
//	    EPwm3Regs.TBPRD = 0x0FA0;
	    //! Set compare A value to 2048 counts
//	    EPwm3Regs.CMPA.bit.CMPA = 0x0800;

	    //! Set period to 4000 counts (50kHz)
	    EPwm3Regs.TBPRD = 0x66;
	    //! Set compare A value to 0x33 counts
//    EPwm3Regs.CMPA.half.CMPA = 0x0800;
	    EPwm3Regs.CMPA.bit.CMPA = 0x33;

	    //! Disable SOC on A group
	    EPwm3Regs.ETSEL.bit.SOCAEN  = 0;
	    //! Select SOC on up-count
	    EPwm3Regs.ETSEL.bit.SOCASEL = 4;
	    //! enable SOCA
	    EPwm3Regs.ETSEL.bit.SOCAEN = 1;
	    //! Generate pulse on 1st event
	    EPwm3Regs.ETPS.bit.SOCAPRD = 1;
	    EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADSetupAdcbEpwm(void)
 * \brief To configure PWM trigger selection
 * 		Description:
 *      This function configures pwm trigger selection , aquisition window
 * \return void
 */
/******************************************************************************/
void ADSetupAdcbEpwm(void)
{
    EALLOW;
    //! SOC0 will convert pin B0
    AdcbRegs.ADCSOC0CTL.bit.CHSEL = 0;
    //! sample window is 15 SYSCLK cycles
    AdcbRegs.ADCSOC0CTL.bit.ACQPS = 14;
    //! trigger on ePWM3 SOCA/C
    AdcbRegs.ADCSOC0CTL.bit.TRIGSEL = 9;
    //! end of SOC0 will set INT1 flag
    AdcbRegs.ADCINTSEL1N2.bit.INT1SEL = 0;
    //! enable INT1 flag
    AdcbRegs.ADCINTSEL1N2.bit.INT1E = 1;
    //! make sure INT1 flag is cleared
    AdcbRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
    AdcbRegs.ADCINTSEL1N2.bit.INT1CONT = 1;
}
/******************************************************************************/
/*!
 * \fn void ADAdcc_Init(void)
 * \brief ADCC block initialization
 * 		Description:
 *      This function initialize ADCC registers and associated SOC registers
 *		Calls ADConfigureAdcc(), ADConfigureAdccEpwm(), ADSetupAdccEpwm()
 * \return void
 */
/******************************************************************************/
void ADAdcc_Init(void)
{
    ADConfigureAdcc();
    ADConfigureAdccEpwm();
    ADSetupAdccEpwm();
//!  Map ISR functions
    EALLOW;
    PieVectTable.ADCC1_INT = &adcc1_isr;
    EDIS;
    //! Enable INT 1 in the PIE
//    PieCtrlRegs.PIEIER1.all = 0xFFFF;
	EALLOW;
	//! Enable CPU Interrupt 1
	IER |= M_INT1;
	IER |= M_INT10;

	//! Enable Global interrupt INTM
	EINT;
	//    ERTM;
	EDIS;

	EALLOW;
	  //! Sync ePWM
	CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;

	//! Enable INT 1 in the PIE
	PieCtrlRegs.PIEIER1.bit.INTx3 = 0;
	//PieCtrlRegs.PIEIER1.all = 0xFFFF;

}

/******************************************************************************/
/*!
 * \fn void ADConfigureAdcc(void)
 * \brief To configure ADCC block registers
 * 		Description:
 *      This function configures adc resolution, signal mode, clock
 * \return void
 */
/******************************************************************************/
void ADConfigureAdcc(void)
{
    EALLOW;
    //! set ADCCLK divider to /4
    AdccRegs.ADCCTL2.bit.PRESCALE = 6;
    //! 12-bit resolution
    AdccRegs.ADCCTL2.bit.RESOLUTION = 1;
    //! single-ended channel conversions (12-bit mode only)
    AdccRegs.ADCCTL2.bit.SIGNALMODE = 1;
    //! Set pulse positions to late
    AdccRegs.ADCCTL1.bit.INTPULSEPOS = 1;
    //! power up the ADC
    AdccRegs.ADCCTL1.bit.ADCPWDNZ = 1;
    //! delay for 1ms to allow ADC time to power up
    DELAY_US(1000);
    EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADConfigureAdccEpwm(void)
 * \brief To configure ADCC block for pwm trigger
 * 		Description:
 *      This function configures pwm soc to trigger SOC
 * \return void
 */
/******************************************************************************/
void ADConfigureAdccEpwm(void)
{
		EALLOW;
	    //! Assumes ePWM clock is already enabled
		//! freeze counter
	    EPwm3Regs.TBCTL.bit.CTRMODE = 3;
	    //! TBCLK pre-scaler = /1
	    EPwm3Regs.TBCTL.bit.HSPCLKDIV = 0;
	    //! Set period to 4000 counts (50kHz)
	    EPwm3Regs.TBPRD = 0x66;
	    //! Set compare A value to 0x33 counts
//    EPwm3Regs.CMPA.half.CMPA = 0x0800;
	    EPwm3Regs.CMPA.bit.CMPA = 0x33;
	    //! Disable SOC on A group
	    EPwm3Regs.ETSEL.bit.SOCAEN  = 0;
	    //! Select SOC on up-count
	    EPwm3Regs.ETSEL.bit.SOCASEL = 4;
	    //! enable SOCA
	    EPwm3Regs.ETSEL.bit.SOCAEN = 1;
	    //! Generate pulse on 1st event
	    EPwm3Regs.ETPS.bit.SOCAPRD = 1;
	    EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADSetupAdccEpwm(void)
 * \brief To configure PWM trigger selection
 * 		Description:
 *      This function configures pwm trigger selection, aquisition window
 * \return void
 */
/******************************************************************************/
void ADSetupAdccEpwm(void)
{
    EALLOW;
    //! SOC0 will convert pin C0
    AdccRegs.ADCSOC0CTL.bit.CHSEL = 2;
    //! sample window is 15 SYSCLK cycles
    AdccRegs.ADCSOC0CTL.bit.ACQPS = 14;
    //! trigger on ePWM3 SOCA/C
    AdccRegs.ADCSOC0CTL.bit.TRIGSEL = 9;
    //! end of SOC0 will set INT1 flag
    AdccRegs.ADCINTSEL1N2.bit.INT1SEL = 0;
    //! enable INT1 flag
    AdccRegs.ADCINTSEL1N2.bit.INT1E = 1;
    //! make sure INT1 flag is cleared
    AdccRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
    AdccRegs.ADCINTSEL1N2.bit.INT1CONT = 1;
}
/******************************************************************************/
/*!
 * \fn void ADStartAdcDataAcquisition(void)
 * \brief To start WBC channel ADC data Aquisition
 * 		Description:
 *      This function configures and initiates the PWM trigger to start
 *      Aquisition
 * \return void
 */
/******************************************************************************/
void ADStartAdcDataAcquisition(void)
{
	StructDataAqParam.AcquisitionStatusFlag = 0;
    EALLOW;
	EPwm3Regs.TBCTL.bit.CTRMODE = 0;
	EDIS;
	DELAY_US(50000);
	while(StructDataAqParam.AcquisitionStatusFlag != 0xFFFF)
	{
	    DACellDataSampleAnalysis();
//		DELAY_US(2000);
	}
}
/******************************************************************************/
/*!
 * \fn void ADStartAdcDataAqWBC(void)
 * \brief To start WBC channel ADC data Aquisition
 * 		Description:
 *      This function configures and initiates the PWM trigger to start
 *      Aquisition of WBC channel
 * \return void
 */
/******************************************************************************/
void ADStartAdcDataAqWBC(void)
{
	StructDataAqParam.AcquisitionStatusFlag = 0;
	StructDataAqParam.ADCSampleCounter = 0;
    EALLOW;
#ifdef _ALPHA_2_BOARD_
    EPwm9Regs.TBCTL.bit.CTRMODE = 0;
#else
	EPwm3Regs.TBCTL.bit.CTRMODE = 0;
#endif
	EDIS;
#ifndef _ALPHA_2_BOARD_
	while(StructDataAqParam.AcquisitionStatusFlag != 0xFFFF)
	{
		DELAY_US(20000);
	}
#endif
}
/******************************************************************************/
/*!
 * \fn void ADStopAdcDataAqWBC(void)
 * \brief To start WBC channel ADC data Aquisition
 * 		Description:
 *      This function configures and initiates the PWM trigger to stop
 *      Aquisition of WBC channel
 * \return void
 */
/******************************************************************************/
void ADStopAdcDataAqWBC(void)
{
	StructDataAqParam.AcquisitionStatusFlag = \
	        StructDataAqParam.AcquisitionStatusFlag | 0x000F;
	//if(StructDataAqParam.AcquisitionStatusFlag == 0x0FFF)
	{
	    EALLOW;
#ifdef _ALPHA_2_BOARD_
	    EPwm9Regs.TBCTL.bit.CTRMODE = 3;
#else
	    EPwm3Regs.TBCTL.bit.CTRMODE = 3;
#endif
		EDIS;
		StructDataAqParam.AcquisitionStatusFlag = 0xFFFF;
	}
}
/******************************************************************************/
/*!
 * \fn void ADStartAdcDataAqRBC(void)
 * \brief To start WBC channel ADC data Aquisition
 * 		Description:
 *      This function configures and initiates the PWM trigger to start
 *      Aquisition
 * \return void
 */
/******************************************************************************/
void ADStartAdcDataAqRBC(void)
{
	StructDataAqParam.AcquisitionStatusFlag = 0;
	StructDataAqParam.ADCSampleCounter = 0;
    EALLOW;
	EPwm3Regs.TBCTL.bit.CTRMODE = 0;
	EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADStopAdcDataAqRBC(void)
 * \brief To start WBC channel ADC data Aquisition
 * 		Description:
 *      This function configures and initiates the PWM trigger to stop
 *      Aquisition
 * \return void
 */
/******************************************************************************/
void ADStopAdcDataAqRBC(void)
{
	StructDataAqParam.AcquisitionStatusFlag = \
	        StructDataAqParam.AcquisitionStatusFlag | 0x00F0;
	//if(StructDataAqParam.AcquisitionStatusFlag == 0x0FFF)
	{
	    EALLOW;
		EPwm3Regs.TBCTL.bit.CTRMODE = 3;
		EDIS;
		StructDataAqParam.AcquisitionStatusFlag = 0xFFFF;
	}
}
/******************************************************************************/
/*!
 * \fn void ADStartAdcDataAqPLT(void)
 * \brief To start WBC channel ADC data Aquisition
 * 		Description:
 *      This function configures and initiates the PWM trigger to start
 *      Aquisition
 * \return void
 */
/******************************************************************************/
void ADStartAdcDataAqPLT(void)
{
	StructDataAqParam.AcquisitionStatusFlag = 0;
	StructDataAqParam.ADCSampleCounter = 0;
    EALLOW;
	EPwm3Regs.TBCTL.bit.CTRMODE = 0;
	EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADStopAdcDataAqPLT(void)
 * \brief To start WBC channel ADC data Aquisition
 * 		Description:
 *      This function configures and initiates the PWM trigger to stop
 *      Aquisition
 * \return void
 */
/******************************************************************************/
void ADStopAdcDataAqPLT(void)
{
	StructDataAqParam.AcquisitionStatusFlag = \
	        StructDataAqParam.AcquisitionStatusFlag | 0x0F00;
	//if(StructDataAqParam.AcquisitionStatusFlag == 0x0FFF)
	{
	    EALLOW;
		EPwm3Regs.TBCTL.bit.CTRMODE = 3;
		EDIS;
		StructDataAqParam.AcquisitionStatusFlag = 0xFFFF;
	}
}
/******************************************************************************/
/*!
 * \fn void ADStopAdcDataAcquisition(Uint16 CompletionStatus)
 * \param CompletionStatus individual channel completion status
 * \brief To start WBC channel ADC data Aquisition
 * 		Description:
 *      This function configures and initiates the PWM trigger to stop
 *      Aquisition
 * \return void
 */
/******************************************************************************/
void ADStopAdcDataAcquisition(void)
{
	StructDataAqParam.AcquisitionStatusFlag = \
	        StructDataAqParam.AcquisitionStatusFlag | 0x0FFF;
	if(StructDataAqParam.AcquisitionStatusFlag == 0x0FFF)
	{
	    EALLOW;
		EPwm3Regs.TBCTL.bit.CTRMODE = 3;
		EDIS;
		StructDataAqParam.AcquisitionStatusFlag = 0xFFFF;
	}
}
/******************************************************************************/
/*!
 * \fn void ADBloodDataAcquisition(void)
 * \brief ADC measurement module
 * \brief Description:
 *      Function to configure data acquisition of WBC, RBC & PLTfor First & Second
 *      counting cycles
 * \return void
 */
/******************************************************************************/
void ADBloodDataAcquisition(Uint16 CountingCycle)
{
	switch(CountingCycle)
	{
	case FIRST_COUNT_CYCLE:
	    DAConfigSDRAM_DMA(FIRST_COUNT_CYCLE);
	    SysResetCellDataAnalysisParams(FIRST_COUNT_CYCLE);
		StructDataAqParam.CountingCycleFlag = FIRST_COUNT_CYCLE;
		printf("First Measurement starts \n");
		ADStartAdcDataAcquisition();// Blocking call
		break;

	case SECOND_COUNT_CYCLE:
	    DAConfigSDRAM_DMA(SECOND_COUNT_CYCLE);
		StructDataAqParam.CountingCycleFlag = SECOND_COUNT_CYCLE;
		printf("Second Measurement starts \n");
		ADStartAdcDataAcquisition(); // Blocking call
//		StructDataAqParam.CountingCycleFlag = 0;
		break;

	default:
		break;
	}
}
/******************************************************************************/
/*!
 * \fn adca1_isr()
 *
 * \brief ADCA channel 1 ISR handler
 * 		Description:
 *      This ISR handler calls on every sample conversion
 *      Clear INT1 flag
 *      acknowledge PIE group 1 to enable further interrupts
 * \return void
 */
/******************************************************************************/

interrupt void adca1_isr(void)
{
#if	SDRAM_DMA_DISABLE
	if(StructDataAqParam.ADCSampleCounter < 6000000)
	{
	    BDAPeakDetectionAlgorithm(AdcaResultRegs.ADCRESULT0);
	}
	else
	{
		if(StructDataAqParam.ADCSampleCounter < 6004000)
		{
			usnAdcDataCapture[StructDataAqParam.ADCSampleCounter-6000000] = \
			        AdcaResultRegs.ADCRESULT0;
		}
		else
		{
		    ADStopAdcDataAcquisition();
		}
	}

	StructDataAqParam.ADCSampleCounter++;

	//! Clear INT1 flag
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
    //! acknowledge PIE group 1 to enable further interrupts
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
#endif
}

/******************************************************************************/
/*!
 * \fn adcb1_isr()
 *
 * \brief ADCB channel 1 ISR handler
 * 		Description:
 *      This ISR handler calls on every sample conversion
 *      Clear INT1 flag
 *      acknowledge PIE group 1 to enable further interrupts
 * \return void
 */
/******************************************************************************/
interrupt void adcb1_isr(void){

#if SDRAM_DMA_DISABLE
    //! Clear INT1 flag
	AdcbRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
	//! acknowledge PIE group 1 to enable
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
#endif
}

/******************************************************************************/
/*!
 * \fn adcc1_isr()
 *
 * \brief ADCC channel 1 ISR handler
 * 		Description:
 *      This ISR handler calls on every sample conversion
 *      Clear INT1 flag
 *      acknowledge PIE group 1 to enable further interrupts
 * \return void
 */
/******************************************************************************/
interrupt void adcc1_isr(void)
{
#if SDRAM_DMA_DISABLE
    //! Clear INT1 flag
	 AdccRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
	 //! acknowledge PIE group 1 to enable
	 PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
#endif

}


//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

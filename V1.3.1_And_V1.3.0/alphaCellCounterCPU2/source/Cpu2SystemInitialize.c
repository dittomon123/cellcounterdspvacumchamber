/******************************************************************************/
/*! \file Cpu2SystemInitialize.c
 *
 *  \brief System initialization
 *
 *  \b Description:
 *      System initializarion and main controlled logic from mbd module called
 *      here
 *      Also test standalone code for individual peripherals modules
 *
 *   $Version: $ 0.1
 *   $Date:    $ Apr-21-2015
 *   $Author:  $ MSK, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu2_system_module
//! @{
//*****************************************************************************
#include <Cpu2SystemInitialize.h>
#include "Cpu2InitBoard.h"
#include <stdio.h>
#include <stdlib.h>

/******************************************************************************/
/*!
 *
 * \brief variable is used to store FW version control string
 * \var char cCpu2FwVer[]
 */
/******************************************************************************/

//char cCpu2FwVerLocal[32] = "cpu2-ver-0.05.11_build-17052016";

int cCpu2FwVerLocal[03] = {1,3,0};
#pragma DATA_SECTION(cCpu2FwVer,".RAMGS12_Cpu2AdcBuf");
int cCpu2FwVer[03]; //CRH_CHANGE_13_09

unsigned int unWBCCellCount=0;
unsigned int unRBCCellCount=0;
unsigned int unPLTCellCount=0;
unsigned int unPrevWBCCellCount=0;
unsigned int unPrevRBCCellCount=0;
unsigned int unPrevPLTCellCount=0;

#pragma DATA_SECTION(Cpu1IpcSharedBuf,".RAMGS13_Cpu2IpcParam");
uint32_t Cpu1IpcSharedBuf[10];
uint32_t *Cpu1IpcSharedBufPtr;

static IpcParameters *stCpu2IpcCommPtr;

static stBloodCellVoltVal *stWbcBloodCellInfoPtr;
static stBloodCellVoltVal *stRbcBloodCellInfoPtr;
static stBloodCellVoltVal *stPltBloodCellInfoPtr;

static DmaAdcParameters *stDmaAdcParametersPtr;

//MSK_TEST
XINT_FLAGS st_XintFlags;
#define XINT_RBCPLT_WBC	(st_XintFlags.m_RBCPLT_XintFlag & \
        st_XintFlags.m_WBC_XintFlag)


int16_t g_usnWbcCalibratedTimeCPU2 = 330;
int16_t g_usnRbcCalibratedTimeCPU2 = 440;

Uint32 g_usnRBCPLTAcqtime = 0;
Uint32 g_usnWBCAcqtime = 0;
Uint32 g_usnWBCAnalysetime = 0;
Uint32 g_usnRBCPLTAnalysetime = 0;
Uint16 g_usnAlgoExecutionTime = 0;
Uint32 g_usnWBCAcqStartTime;
Uint32 g_usnRBCPLTAcqStartTime;
union UT_VOLUMETRIC_ERROR utVolumetricErrorInfo;

extern Uint16 guCycleFlag;
extern Uint32 DAGetDestAddress(E_CELL_TYPE eTypeofCell, \
        bool_t bAddofCellHtReq);
extern DmaAdcParameters DmaAdcParams;
extern unsigned int g_usnCurrentModeCPU2;
extern Uint32 g_unWBCNoSamples;
extern Uint32 g_unRBCPLTNoSamples;
/******************************************************************************/
/*!
 * \fn void SysSystemInitialize(void)
 * \brief system initialization
 * \brief Description:
 *      This function initialize CPU peripherals and application modules
 * \return void
 */
/******************************************************************************/
void SysSystemInitialize(void)
{
	Uint16 Emif1Status=0;

	//!Update WBC cell thresholds
	stWbcBloodCellInfoPtr = BDAGetWBCBloodCellStatistics();
	//!Update RBC Cell thresholds
	stRbcBloodCellInfoPtr = BDAGetRBCBloodCellStatistics();
	//!Update PLT Cell Threshold
	stPltBloodCellInfoPtr = BDAGetPLTBloodCellStatistics();

//	stAdcDataAqParamPtr = ADGetStructDataAqParam();
	//!Get CPU 2 IPC parameters for synchronization
	stCpu2IpcCommPtr = IPCGetCpu2IpcParam();

	//!Get DMA related parameters
	stDmaAdcParametersPtr = DAGetDmaAdcParams();

	//!Initilaze the board
	IBInitialize_Board();

#ifdef _ALPHA_2_BOARD_
	//!Initialize volumetric interrupt
	SysInitVolumetricInterrupt(); //MSK_TEST
#endif
	GPIO_WritePin(LED2_GPIO, 0);
	DELAY_US(200000);
	GPIO_WritePin(LED2_GPIO, 1);
	DELAY_US(200000);
	GPIO_WritePin(LED2_GPIO, 0);
	DELAY_US(200000);
	GPIO_WritePin(LED2_GPIO, 1);

	//!Configure timer 2 for 20mSec
	Configure_Timer2();

	//!Start Timer 2
	Start_Timer2();

	//!Initialize IPC
	IPCInitialize_IPC();

	//!Sync CPU2 to CPU2
	IPCCpu2toCpu1IpcCommSync();

	//strcpy(cCpu2FwVer,cCpu2FwVerLocal);
	memcpy(cCpu2FwVer, cCpu2FwVerLocal, 3*sizeof(int));

	//!Delay should be given after synchronization. Do not modify the delay.
	DELAY_US(5000);
	//!Initialize ADC A for Acquisition
	ADAdca_Init();

	//!Initialize ADC B for Acquisition
	ADAdcb_Init();

	//!Initialize ADC C for Acquisition
	ADAdcc_Init();

	//!Delay should be given after initializing. Do not modify the delay.
	DELAY_US(5000);

	printf("\n FW version: %s \n",cCpu2FwVer);
	printf("\n CPU2 initialization finished \n");

	//!External memory interface status
	Emif1Status = CSGetEmif1Status();
	while(Emif1Status)
	{
		DELAY_US(500000);
		Emif1Status = CSGetEmif1Status();
	}
	CSConfigEmif1CPU(0x02);

#ifdef _ALPHA_2_BOARD_
	//!Reset all the flags used for volumetric and the error flags
	//usnExtInterruptFlag = 0;
	st_XintFlags.m_SampleStartFlag = FALSE;
	st_XintFlags.m_SampleEndFlag = FALSE;
	st_XintFlags.m_Xint_RBCPLTStartFlag = FALSE;
	st_XintFlags.m_Xint_WBCStartFlag = FALSE;
	st_XintFlags.m_Xint_RBCPLTStopFlag = FALSE;
	st_XintFlags.m_Xint_WBCStopFlag = FALSE;

	st_XintFlags.m_bWBC_SampleAnalysisFlagStart = FALSE;
	st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = FALSE;
	st_XintFlags.m_bWBC_SampleAnalysisFlagEnd = FALSE;
	st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd = FALSE;
	st_XintFlags.m_bWBC_StartInterrupt = FALSE;
	st_XintFlags.m_bRBC_PLT_StartInterrupt = FALSE;
	utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE;
	utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE;
#endif
}

#ifdef _ALPHA_2_BOARD_ 	//MSK_BEG
/******************************************************************************/
/*!
 * \fn void SysHandleVolumetricTubeStatus(void)
 * \brief
 * \brief Description:
 *      This function process the volumetric interrupts and the errors handling
 *      during acquisition
  * \return void
 */
/******************************************************************************/
void SysHandleVolumetricTubeStatus(void)
{
   //!Monitor if the second interrupt for WBC channel has
   //!occurred within the maximum range of 10 Seconds

   //!If the interrupt does not occur within the specified time of 10 Seconds,
   //!then terminate the channel
   if((TRUE == st_XintFlags.m_bWBC_StartInterrupt))
   {
       //!To monitor the WBC channel whether the WBC LED start interrupt has started
       //!as expected
       if(TRUE == st_XintFlags.m_bWBC_Acq_Start_Time_Flag)
       {
           st_XintFlags.m_bWBC_Acq_Start_Time_Flag = FALSE;
           //!If the WBC start time is not within the expected limit then set the error
           //! for WBC counting started improper
           if((g_usnWBCAcqStartTime < WBC_ACQ_START_TIME_LOW) || \
                   (g_usnWBCAcqStartTime > WBC_ACQ_START_TIME_HIGH))
           {
               utVolumetricErrorInfo.stVolumetricError.unWBCCountingStartedImproper = TRUE;
           }
           else
           {
               utVolumetricErrorInfo.stVolumetricError.unWBCCountingStartedImproper = FALSE;
           }
       }

       //!If it is not count time sequence and flow calibration, then limit the
       //! data acquisition time to 10 seconds
       if((COUNTING_TIME_SEQUENCE == g_usnCurrentModeCPU2) || \
               (FLOW_CALIBRATION == g_usnCurrentModeCPU2))
       {
           //!If the acquisition time reaches to maximum supported acquisition time,
           //! Stop the acquisition and disable the interrupts
           if(g_usnWBCAcqtime >= MAX_VOLUMETRIC_ACQUISITION_TIME)
           {
               st_XintFlags.m_Xint_WBCStopFlag = TRUE;
               st_XintFlags.m_bWBC_StartInterrupt = FALSE;
               XintRegs.XINT3CR.bit.ENABLE = 0;        // Disable XINT3
               XintRegs.XINT4CR.bit.ENABLE = 0;        // Disable XINT4
               utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotCompleted = TRUE;
               //utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotCompleted = FALSE;   //PH_TESTING
               //st_XintFlags.m_bWBC_SampleAnalysisFlagStart = TRUE;
           }
       }
       else
       {
           //!If the acquisition time reaches to maximum supported acquisition time,
           //! Stop the acquisition and disable the interrupts
           if(g_usnWBCAcqtime >= MAX_DATA_ACQUISITION_TIME)
           {
               st_XintFlags.m_Xint_WBCStopFlag = TRUE;
               st_XintFlags.m_bWBC_StartInterrupt = FALSE;
               XintRegs.XINT3CR.bit.ENABLE = 0;        // Disable XINT3
               XintRegs.XINT4CR.bit.ENABLE = 0;        // Disable XINT4
               utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotCompleted = TRUE;
               //utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotCompleted = FALSE;   //PH_TESTING
               //st_XintFlags.m_bWBC_SampleAnalysisFlagStart = TRUE;
           }

       }
       //!If the acquisition time is greater than the time required for processing,
       //! stop the ADC data and set the Analysis flag start
       if(g_usnWBCAcqtime >= PROCESSING_TIME)
       {
           //!Stop Data acquisition
           ADStopAdcDataAqWBC();
           //!Set WBC sample analysis flag start
           //st_XintFlags.m_bWBC_SampleAnalysisFlagStart = TRUE;
       }
   }

   //!Monitor if the second interrupt for RBC_PLT channel has
   //!occurred within the maximum range of 10 Seconds

   //!If the interrupt does not occur within the specified time of 10 Seconds,
   //!then terminate the channel
   if((TRUE == st_XintFlags.m_bRBC_PLT_StartInterrupt))
   {
       //!To monitor the RBC_PLT channel whether the RBC_PLT LED start interrupt has started
       //!as expected
       if(TRUE == st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag)
       {
           st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag = FALSE;
           //!If the RBC_PLT start time is not within the expected limit then set the error
           //! for RBC_PLT counting started improper
           if((g_usnRBCPLTAcqStartTime < RBC_PLT_ACQ_START_TIME_LOW) || \
                   (g_usnRBCPLTAcqStartTime > RBC_PLT_ACQ_START_TIME_HIGH))
           {
               utVolumetricErrorInfo.stVolumetricError.unRBCCountingStartedImproper = TRUE;
           }
           else
           {
               utVolumetricErrorInfo.stVolumetricError.unRBCCountingStartedImproper = FALSE;
           }
       }

       //!If it is not count time sequence and flow calibration, then limit the
       //! data acquisition time to 10 seconds
       if((COUNTING_TIME_SEQUENCE == g_usnCurrentModeCPU2) || \
               (FLOW_CALIBRATION == g_usnCurrentModeCPU2))
       {
           //!If the acquisition time reaches to maximum supported acquisition time,
           //! Stop the acquisition and disable the interrupts
           if(g_usnRBCPLTAcqtime >= MAX_VOLUMETRIC_ACQUISITION_TIME)
           {
               st_XintFlags.m_Xint_RBCPLTStopFlag = TRUE;
               st_XintFlags.m_bRBC_PLT_StartInterrupt = FALSE;
               //st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = TRUE;
               XintRegs.XINT1CR.bit.ENABLE = 0;        // Disable XINT1
               XintRegs.XINT2CR.bit.ENABLE = 0;        // Disable XINT2
               utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotCompleted = TRUE;
               //utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotCompleted = FALSE;   //PH_TESTING
           }
       }
       else
       {
           //!If the acquisition time reaches to maximum supported acquisition time,
           //! Stop the acquisition and disable the interrupts
           if(g_usnRBCPLTAcqtime >= MAX_DATA_ACQUISITION_TIME)
           {
               st_XintFlags.m_Xint_RBCPLTStopFlag = TRUE;
               st_XintFlags.m_bRBC_PLT_StartInterrupt = FALSE;
               //st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = TRUE;
               XintRegs.XINT1CR.bit.ENABLE = 0;        // Disable XINT1
               XintRegs.XINT2CR.bit.ENABLE = 0;        // Disable XINT2
               utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotCompleted = TRUE;
               //utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotCompleted = FALSE;   //PH_TESTING
           }
       }

       //!If the acquisition time is greater than the time required for processing,
       //! stop the ADC data and set the Analysis flag start
       if(g_usnRBCPLTAcqtime >= PROCESSING_TIME)
       {
           //!Stop Data acquisition
           ADStopAdcDataAqRBC();
           ADStopAdcDataAqPLT();
           //!Set RBC_PLT sample analysis flag start
           //st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = TRUE;
       }
   }

    //!Check for the RBC_PLT start interrupt
    if(st_XintFlags.m_Xint_RBCPLTStartFlag)
    {
        unRBCCellCount = 0;
        unPLTCellCount = 0;

        unPrevRBCCellCount = 0;
        unPrevPLTCellCount = 0;

        st_XintFlags.m_Xint_RBCPLTStartFlag = FALSE;
        //!Check if the second interrupt has come
        if(TRUE == st_XintFlags.m_bRBC_PLT_StartInterrupt)
        {
            //Stop the acquisition interrupt and set bubble detected
            //PH_NOV_02_REMOVE - START
            //st_XintFlags.m_Xint_RBCPLTStopFlag = TRUE;
            //st_XintFlags.m_bRBC_PLT_BubbleDetected = TRUE;
            //PH_NOV_02_REMOVE - END
        }
        //!If it is not count time sequence and flow calibration, start acquisition for RBC and PLT
        if((COUNTING_TIME_SEQUENCE != g_usnCurrentModeCPU2) && \
                (FLOW_CALIBRATION != g_usnCurrentModeCPU2))
        {
            ADStartAdcDataAqRBC();
            ADStartAdcDataAqPLT();
        }
        //!Set the RBC_PLT start interrupt
        st_XintFlags.m_bRBC_PLT_StartInterrupt = TRUE;
        //!Clear the RBC_PLT Acquisition time
        g_usnRBCPLTAcqtime=0;

        //!Disable the start interrupt and enable the stop interrupt
        XintRegs.XINT1CR.bit.ENABLE = 0;        // Disable XINT1  //Do not disable the interrupt
        XintRegs.XINT2CR.bit.ENABLE = 1;        // Enable XINT2

        //!Update the data acquisition completed command and send to CPU1
        //! Send WBC start sensor detection
        /*stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                CPU2_VOLUMETRIC_SENSOR_DETECTION;
        //CRH_RBC_WBC_TIME - START
        IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 2;
        IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,2);*/
    }

    //!Check for the WBC start interrupt
    if(st_XintFlags.m_Xint_WBCStartFlag)
    {
        unWBCCellCount = 0;
        unPrevWBCCellCount = 0;
        st_XintFlags.m_Xint_WBCStartFlag = FALSE;
        //!Check if the second interrupt has come
        if(TRUE == st_XintFlags.m_bWBC_StartInterrupt)
        {
            //Stop the acquisition interrupt and set bubble detected
            //PH_NOV_02_REMOVE - START
            //st_XintFlags.m_Xint_WBCStopFlag = TRUE;
            //st_XintFlags.m_bWBC_BubbleDetected = TRUE;
            //PH_NOV_02_REMOVE - END
        }
        //!If it is not count time sequence and flow calibration, start acquisition for WBC
        if((COUNTING_TIME_SEQUENCE != g_usnCurrentModeCPU2) && \
                (FLOW_CALIBRATION != g_usnCurrentModeCPU2))
        {
            ADStartAdcDataAqWBC();
        }
        //!Set the WBC start interrupt
        st_XintFlags.m_bWBC_StartInterrupt = TRUE;
        //!Clear the WBC Acquisition time
        g_usnWBCAcqtime=0;
        //!Disable the start interrupt and enable the stop interrupt
        XintRegs.XINT3CR.bit.ENABLE = 0;        // Disable XINT3  //Do not disable the interrupt
        XintRegs.XINT4CR.bit.ENABLE = 1;        // Enable XINT4

        //!Update the data acquisition completed command and send to CPU1
        //! Send WBC start sensor detection
     /*   stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                CPU2_VOLUMETRIC_SENSOR_DETECTION;
        //CRH_RBC_WBC_TIME - START
        IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 1;
        IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,2);*/
    }

    //!Check for the RBC_PLT stop interrupt
    if(st_XintFlags.m_Xint_RBCPLTStopFlag)
    {
     //!Clear the RBC_PLT stop interrupt flag and set the RBC_PLT interrupt
     st_XintFlags.m_Xint_RBCPLTStopFlag = FALSE;
     st_XintFlags.m_RBCPLT_XintFlag = TRUE;
     //!If it is not count time sequence and flow calibration,
     //! stop the ADC acquisition for RBC and PLT
     if((COUNTING_TIME_SEQUENCE != g_usnCurrentModeCPU2) && \
             (FLOW_CALIBRATION != g_usnCurrentModeCPU2))//testing or
         {
         ADStopAdcDataAqRBC();
         ADStopAdcDataAqPLT();
         //!Enable flag to start the analysis on RBC and PLT
         st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = TRUE;
         }
      //!Update the RBC & PLT Acquisition time
      stDmaAdcParametersPtr->unRBCPLTAcqtime = g_usnRBCPLTAcqtime;
      //!Update RBC & PLT Analyse time same as Acquisition time
      g_usnRBCPLTAnalysetime = g_usnRBCPLTAcqtime;

      //!Update the no. of samples for RBC and PLT to be processed
      g_unRBCPLTNoSamples = DAGetDestAddress(eCellPLT, FALSE) + (g_usnRBCPLTAcqtime * 20 * 1000);

      //!Disable RBC and PLT start and stop external interrupt
      XintRegs.XINT1CR.bit.ENABLE = 0;        // Disable XINT1
      XintRegs.XINT2CR.bit.ENABLE = 0;        // Disable XINT2
      //!Disable the flag for RBC_PLT start interrupt
      st_XintFlags.m_bRBC_PLT_StartInterrupt = FALSE;

      //!Update the data acquisition completed command and send to CPU1
      //! Send WBC start sensor detection
/*      stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
              CPU2_VOLUMETRIC_SENSOR_DETECTION;
      //CRH_RBC_WBC_TIME - START
      IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
      stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 3;
      IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,2);*/
    }

    //!Check for the WBC stop interrupt
    if(st_XintFlags.m_Xint_WBCStopFlag)
    {
        //!Clear the WBC stop interrupt flag and set the WBC interrupt
        st_XintFlags.m_Xint_WBCStopFlag = FALSE;
        st_XintFlags.m_WBC_XintFlag = TRUE;
        //!If it is not count time sequence and flow calibration,
        //! stop the ADC acquisition for WBC
        if((COUNTING_TIME_SEQUENCE != g_usnCurrentModeCPU2) && \
                (FLOW_CALIBRATION != g_usnCurrentModeCPU2))//testing or
        {
            ADStopAdcDataAqWBC();
            //!Enable flag to start the analysis on WBC
            st_XintFlags.m_bWBC_SampleAnalysisFlagStart = TRUE;
        }
        //!Update WBC Acquisition time
        stDmaAdcParametersPtr->unWBCAcqtime = g_usnWBCAcqtime;
        //!Update WBC Analyse time same as Acquisition time
        g_usnWBCAnalysetime = g_usnWBCAcqtime;

        //!Update the no. of samples for WBC to be processed
        g_unWBCNoSamples = DAGetDestAddress(eCellWBC, FALSE) + (g_usnWBCAcqtime * 20 * 1000); //6900000;

        //!Disable RBC and PLT start and stop external interrupt
        XintRegs.XINT3CR.bit.ENABLE = 0;        // Disable XINT3
        XintRegs.XINT4CR.bit.ENABLE = 0;        // Disable XINT4
        //!Disable the flag for RBC_PLT start interrupt
        st_XintFlags.m_bWBC_StartInterrupt = FALSE;
        //!Update the data acquisition completed command and send to CPU1
        //! Send WBC start sensor detection
        /*stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                CPU2_VOLUMETRIC_SENSOR_DETECTION;
        //CRH_RBC_WBC_TIME - START
        IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 4;
        IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,2);*/

    }


    //!Process the interrupts depending on the mode selected
    switch(g_usnCurrentModeCPU2)
    {
       case WHOLE_BLOOD_MODE:
       case PRE_DILUENT_MODE:
       case START_UP_SEQUENCE_CMD://SRK_STARTUP_CHANGE
       case AUTO_CALIBRATION_WB:
       case QUALITY_CONTROL_WB:
       case QUALITY_CONTROL_BODY_FLUID:
       case STARTUP_FROM_SERVICE_SCREEN_CMD:
       case STARTUP_FAILED_CMD:
           //!If the WBC acquisition has to be started and if the acquisition
           //! has not started even after 10 seconds, then disable the interrupt
           //! and set the error indicating WBC acquisition not started
           if((TRUE == st_XintFlags.m_bWBC_Acq_Start_Time_Flag) ||\
                   ((TRUE == st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag)))
           {
               if((g_usnWBCAcqStartTime > 500) || (g_usnRBCPLTAcqStartTime > 500))
               {
                   if((g_usnWBCAcqStartTime > 500))
                   {
                       //!Disable the WBC start external interrupt
                       XintRegs.XINT3CR.bit.ENABLE = 0;        // Disable VOL3
                       //!Disable WBC start timer flag
                       st_XintFlags.m_bWBC_Acq_Start_Time_Flag = FALSE;
                       //!Set the WBC interrupt flag to true to monitor next time
                       //st_XintFlags.m_WBC_XintFlag = TRUE;
                       //!Set the volumetric error indicating WBC counting not started
                       utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted = TRUE;
                       //!Reset the WBC acquisition time to Zero
                       stDmaAdcParametersPtr->unWBCAcqtime = 0;

                       st_XintFlags.m_bWBC_SampleAnalysisFlagStart = TRUE;
                   }
                   if(g_usnRBCPLTAcqStartTime > 500)
                   {
                       //!Disable the RBC & PLT start external interrupt
                      XintRegs.XINT1CR.bit.ENABLE = 0;        // Disable VOL1
                      //!Disable RBC_PLT start timer flag
                      st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag = FALSE;
                      //!Set the RBC_PLT interrupt flag to true to monitor next time
                      //st_XintFlags.m_RBCPLT_XintFlag = TRUE;
                      //!Set the volumetric error indicating RBC_PLT counting not started
                      utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted = TRUE;
                      //!Reset the RBC & PLT acquisition time to Zero
                      stDmaAdcParametersPtr->unRBCPLTAcqtime = 0;

                      st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = TRUE;

                   }
                   //!Send the command to CPU 1 indicating acquisition not happening

                   //!Update the data acquisition completed command and send to CPU1
                  //! Send WBC & RBC_PLT acquisition time, and volumetric error information
                   stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                          CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED;
                  //CRH_RBC_WBC_TIME - START
                  //IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = \
                          stDmaAdcParametersPtr->unWBCAcqtime;
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = \
                          stDmaAdcParametersPtr->unRBCPLTAcqtime;
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = \
                          utVolumetricErrorInfo.unVolumetricInfo;
                  IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,4);

                  //!Reset the volumetric error info and the interrupt timer counter
                  utVolumetricErrorInfo.unVolumetricInfo = 0;
                  //CRH_RBC_WBC_TIME - END

                  CpuTimer2.InterruptCount=0;
                  g_usnAlgoExecutionTime = CpuTimer2.InterruptCount;

                  //!When Both WBC and RBC is not started, then there is a miss communication b/w CPU1 and CPU2
                  //!For that one we have added this delay
                  DELAY_US(3000000);    //30ms

                  //!Reset the sample start, WBC interrupt and RBC interrupt flags
                  st_XintFlags.m_SampleStartFlag = FALSE;

               }
           }
/*
           //!If the RBC_PLT acquisition has to be started and if the acquisition
           //! has not started even after 10 seconds, then disable the interrupt
           //! and set the error indicating RBC_PLT acquisition not started
           if(TRUE == st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag)
           {
               if(g_usnRBCPLTAcqStartTime > 500)
               {
                   //!Disable the RBC & PLT start external interrupt
                   XintRegs.XINT1CR.bit.ENABLE = 0;        // Disable VOL1
                   //!Disable RBC_PLT start timer flag
                   st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag = FALSE;
                   //!Set the RBC_PLT interrupt flag to true to monitor next time
                   //st_XintFlags.m_RBCPLT_XintFlag = TRUE;
                   //!Set the volumetric error indicating RBC_PLT counting not started
                   utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted = TRUE;
                   //!Reset the RBC & PLT acquisition time to Zero
                   stDmaAdcParametersPtr->unRBCPLTAcqtime = 0;



                   if(!(utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted))
                   {
                       //!Reset the sample start, WBC interrupt and RBC interrupt flags
                       st_XintFlags.m_SampleStartFlag = FALSE;

                       //!Update the data acquisition completed command and send to CPU1
                          //! Send WBC & RBC_PLT acquisition time, and volumetric error information
                        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                                CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED;
                        //CRH_RBC_WBC_TIME - START
                        //IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
                        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = \
                                stDmaAdcParametersPtr->unWBCAcqtime;
                        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = \
                                stDmaAdcParametersPtr->unRBCPLTAcqtime;
                        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = \
                                utVolumetricErrorInfo.unVolumetricInfo;
                        IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,4);

                        //!Reset the volumetric error info and the interrupt timer counter
                        utVolumetricErrorInfo.unVolumetricInfo = 0;
                        //CRH_RBC_WBC_TIME - END

                        CpuTimer2.InterruptCount=0;
                        g_usnAlgoExecutionTime = CpuTimer2.InterruptCount;

                   }

               }
           }
*/

           //!If both the flag for WBC acquisition and RBC acquisition is
           //! completed, then check for volumetric error
           if(st_XintFlags.m_WBC_XintFlag & st_XintFlags.m_RBCPLT_XintFlag)
           {
               //!Reset the sample start, WBC interrupt and RBC interrupt flags
               st_XintFlags.m_SampleStartFlag = FALSE;
               st_XintFlags.m_WBC_XintFlag = FALSE;
               st_XintFlags.m_RBCPLT_XintFlag = FALSE;

               if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted)
               {
                   stDmaAdcParametersPtr->unWBCAcqtime = 0;
                   st_XintFlags.m_bWBC_SampleAnalysisFlagStart = TRUE;
               }

               if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted)
               {
                   stDmaAdcParametersPtr->unRBCPLTAcqtime = 0;
                   st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = TRUE;
               }

/*
//Added
               if((0 != stDmaAdcParametersPtr->unWBCAcqtime) && \
                       ((stDmaAdcParametersPtr->unWBCAcqtime < ACQ_TIME_LOW) || \
                       (stDmaAdcParametersPtr->unWBCAcqtime > ACQ_TIME_HIGH)))
                {
                    utVolumetricErrorInfo.stVolumetricError.unWBCImproperAbortion = TRUE;
                }
                else
                {
                    utVolumetricErrorInfo.stVolumetricError.unWBCImproperAbortion = FALSE;
                }

                if((0 != stDmaAdcParametersPtr->unRBCPLTAcqtime) && \
                        ((stDmaAdcParametersPtr->unRBCPLTAcqtime < ACQ_TIME_LOW) || \
                        (stDmaAdcParametersPtr->unRBCPLTAcqtime >= ACQ_TIME_HIGH)))
                {
                    utVolumetricErrorInfo.stVolumetricError.unRBCImproperAbortion = TRUE;
                }
                else
                {
                    utVolumetricErrorInfo.stVolumetricError.unRBCImproperAbortion = FALSE;
                }

//Added
*/
               //Commented on 3-1-2020 -Started
/*               //!Check if the WBC interrupt time is within the expected range.
               //! If it is out of range, then set the error, else reset the error
               if((0 != stDmaAdcParametersPtr->unWBCAcqtime) && \
                       ((stDmaAdcParametersPtr->unWBCAcqtime < ACQ_TIME_LOW) || \
                      (stDmaAdcParametersPtr->unWBCAcqtime > ACQ_TIME_HIGH)))
               {
                   utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = TRUE;
                   //utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE;  //PH_TESTING
               }
               else
               {
                   utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE;
               }

               //!Check if the RBC_PLT interrupt time is within the expected range.
               //! If it is out of range, then set the error, else reset the error
               if((0 != stDmaAdcParametersPtr->unRBCPLTAcqtime) && \
                       ((stDmaAdcParametersPtr->unRBCPLTAcqtime < ACQ_TIME_LOW) || \
                  (stDmaAdcParametersPtr->unRBCPLTAcqtime > ACQ_TIME_HIGH)))
               {
                   utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = TRUE;
                   //utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE;  //PH_TESTING
               }
               else
               {
                   utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE;
               }*/

               //Commented on 3-1-2020 -Ended

               //Hari Early and late count check on 3-1-2020 -Started
               //!Check if the WBC interrupt time is within the expected range.
               //! If it is out of range, then set the error, else reset the error
               if((0 != stDmaAdcParametersPtr->unWBCAcqtime) && \
                       ((stDmaAdcParametersPtr->unWBCAcqtime < WBC_ACQ_TIME_LOW) || \
                      (stDmaAdcParametersPtr->unWBCAcqtime > WBC_ACQ_TIME_HIGH)))
               {
                   utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = TRUE;
                   //utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE;  //PH_TESTING
               }
               else
               {
                   utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE;
               }

               //!Check if the RBC_PLT interrupt time is within the expected range.
               //! If it is out of range, then set the error, else reset the error
               if((0 != stDmaAdcParametersPtr->unRBCPLTAcqtime) && \
                       ((stDmaAdcParametersPtr->unRBCPLTAcqtime < RBC_ACQ_TIME_LOW) || \
                  (stDmaAdcParametersPtr->unRBCPLTAcqtime > RBC_ACQ_TIME_HIGH)))
               {
                   utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = TRUE;
                   //utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE;  //PH_TESTING
               }
               else
               {
                   utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE;
               }

               //Hari Early and late count check on 3-1-2020 -Ended

               //!Update the data acquisition completed command and send to CPU1
               //! Send WBC & RBC_PLT acquisition time, and volumetric error information
               stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                       CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED;
               //CRH_RBC_WBC_TIME - START
               //IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
               stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = \
                       stDmaAdcParametersPtr->unWBCAcqtime;
               stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = \
                       stDmaAdcParametersPtr->unRBCPLTAcqtime;
               stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = \
                       utVolumetricErrorInfo.unVolumetricInfo;
               IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,4);

               //!Reset the volumetric error info and the interrupt timer counter
               utVolumetricErrorInfo.unVolumetricInfo = 0;
               //CRH_RBC_WBC_TIME - END

               CpuTimer2.InterruptCount=0;
               g_usnAlgoExecutionTime = CpuTimer2.InterruptCount;

           }
       break;

      case BODY_FLUID_MODE:
      case COMMERCIAL_CALIBRATION_WBC:
      case PARTIAL_BLOOD_COUNT_WBC:
              //!If the WBC acquisition has to be started and if the acquisition
              //! has not started even after 10 seconds, then disable the interrupt
              //! and set the error indicating WBC acquisition not started
              if(TRUE == st_XintFlags.m_bWBC_Acq_Start_Time_Flag)
              {
                  if(g_usnWBCAcqStartTime > 500)
                  {
                      //!Disable the WBC start external interrupt
                      XintRegs.XINT3CR.bit.ENABLE = 0;        // Disable VOL3
                      //!Disable WBC start timer flag
                      st_XintFlags.m_bWBC_Acq_Start_Time_Flag = FALSE;
                      //!Set the WBC interrupt flag to true to monitor next time
                      //st_XintFlags.m_WBC_XintFlag = TRUE;
                      //!Set the volumetric error indicating WBC counting not started
                      utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted = TRUE;
                      //!Reset the WBC acquisition time to Zero
                      stDmaAdcParametersPtr->unWBCAcqtime = 0;
                  }
              }
              //!If both the flag for WBC acquisition is
              //! completed, then check for volumetric error
              if(st_XintFlags.m_WBC_XintFlag)
              {
                  //!Reset the sample start, WBC interrupt flags
                  st_XintFlags.m_SampleStartFlag = FALSE;
                  st_XintFlags.m_WBC_XintFlag = FALSE;


                  if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted)
                  {
                      stDmaAdcParametersPtr->unWBCAcqtime = 0;
                  }

                  //!Check if the WBC interrupt time is within the expected range.
                  //! If it is out of range, then set the error, else reset the error
                  if((0 != stDmaAdcParametersPtr->unWBCAcqtime) && \
                          ((stDmaAdcParametersPtr->unWBCAcqtime < ACQ_TIME_LOW) || \
                    (stDmaAdcParametersPtr->unWBCAcqtime > ACQ_TIME_HIGH)))
                  {
                      utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = TRUE;
                  }
                  else
                  {
                      utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE;
                  }

                  //!Update the data acquisition completed command and send to CPU1
                  //! Send WBC acquisition time, and volumetric error information
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                          CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED;
                  //CRH_RBC_WBC_TIME - START
                  //IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = \
                          stDmaAdcParametersPtr->unWBCAcqtime;
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = 0;
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = \
                          utVolumetricErrorInfo.unVolumetricInfo;
                  IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,4);
                  //CRH_RBC_WBC_TIME - END

                  //!Reset the volumetric error info and the interrupt timer counter
                  utVolumetricErrorInfo.unVolumetricInfo = 0;
                  CpuTimer2.InterruptCount=0;
                  g_usnAlgoExecutionTime = CpuTimer2.InterruptCount;
              }
      break;

      case COMMERCIAL_CALIBRATION_RBC:
      case COMMERCIAL_CALIBRATION_PLT:
      case PARTIAL_BLOOD_COUNT_RBC_PLT:
              //!If the RBC_PLT acquisition has to be started and if the acquisition
              //! has not started even after 10 seconds, then disable the interrupt
              //! and set the error indicating RBC_PLT acquisition not started
              if(TRUE == st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag)
              {
                  if(g_usnRBCPLTAcqStartTime > 500)
                  {
                      //!Disable the RBC & PLT start external interrupt
                      XintRegs.XINT1CR.bit.ENABLE = 0;        // Disable VOL1
                      //!Disable RBC_PLT start timer flag
                      st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag = FALSE;
                      //!Set the RBC_PLT interrupt flag to true to monitor next time
                      st_XintFlags.m_RBCPLT_XintFlag = TRUE;
                      //!Set the volumetric error indicating RBC_PLT counting not started
                      utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted = TRUE;
                      //!Reset the RBC & PLT acquisition time to Zero
                      stDmaAdcParametersPtr->unRBCPLTAcqtime = 0;
                  }
              }
              //!If RBC_PLT acquisition is completed, then check for volumetric error
              if(st_XintFlags.m_RBCPLT_XintFlag)
              {
                  //!Reset the sample start, RBC_PLT interrupt flags
                  st_XintFlags.m_SampleStartFlag = FALSE;
                  st_XintFlags.m_RBCPLT_XintFlag = FALSE;

                  if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted)
                  {
                      stDmaAdcParametersPtr->unRBCPLTAcqtime = 0;
                  }

                  //!Check if the RBC_PLT interrupt time is within the expected range.
                  //! If it is out of range, then set the error, else reset the error
                  if((0 != stDmaAdcParametersPtr->unRBCPLTAcqtime ) && \
                          ((stDmaAdcParametersPtr->unRBCPLTAcqtime < ACQ_TIME_LOW) || \
                    (stDmaAdcParametersPtr->unRBCPLTAcqtime > ACQ_TIME_HIGH)))
                  {
                      utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = TRUE;
                  }
                  else
                  {
                      utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE;
                  }



                  //!Update the data acquisition completed command and send to CPU1
                  //! Send RBC_PLT acquisition time, and volumetric error information
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                          CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED;
                  //CRH_RBC_WBC_TIME - START
                  //IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 0;
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = \
                          stDmaAdcParametersPtr->unRBCPLTAcqtime;
                  stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = \
                          utVolumetricErrorInfo.unVolumetricInfo;
                  IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,4);
                  //CRH_RBC_WBC_TIME - END

                  //!Reset the volumetric error info and the interrupt timer counter
                  utVolumetricErrorInfo.unVolumetricInfo = 0;
                  CpuTimer2.InterruptCount=0;
                  g_usnAlgoExecutionTime = CpuTimer2.InterruptCount;
              }
              break;

       case COUNTING_TIME_SEQUENCE:
       case FLOW_CALIBRATION:
           //!If the WBC acquisition has to be started and if the acquisition
           //! has not started even after 10 seconds, then disable the interrupt
           //! and set the error indicating WBC acquisition not started
           if(TRUE == st_XintFlags.m_bWBC_Acq_Start_Time_Flag)
           {
               if(g_usnWBCAcqStartTime > 500)
               {
                   //!Disable the WBC start external interrupt
                   XintRegs.XINT3CR.bit.ENABLE = 0;        // Disable VOL3
                   //!Disable WBC start timer flag
                   st_XintFlags.m_bWBC_Acq_Start_Time_Flag = FALSE;
                   //!Set the WBC interrupt flag to true to monitor next time
                   st_XintFlags.m_WBC_XintFlag = TRUE;
                   //!Set the volumetric error indicating WBC counting not started
                   utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted = FALSE;
                   //!Reset the WBC acquisition time to Zero
                   stDmaAdcParametersPtr->unWBCAcqtime = 0;
               }
           }
           //!If the RBC_PLT acquisition has to be started and if the acquisition
           //! has not started even after 10 seconds, then disable the interrupt
           //! and set the error indicating RBC_PLT acquisition not started
           if(TRUE == st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag)
           {
               if(g_usnRBCPLTAcqStartTime > 500)
               {
                   //!Disable the RBC & PLT start external interrupt
                   XintRegs.XINT1CR.bit.ENABLE = 0;        // Disable VOL1
                   //!Disable RBC_PLT start timer flag
                   st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag = FALSE;
                   //!Set the RBC_PLT interrupt flag to true to monitor next time
                   st_XintFlags.m_RBCPLT_XintFlag = TRUE;
                   //!Set the volumetric error indicating RBC_PLT counting not started
                   utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted = FALSE;
                   //!Reset the RBC & PLT acquisition time to Zero
                   stDmaAdcParametersPtr->unRBCPLTAcqtime = 0;
               }
           }
           //!If both the flag for WBC acquisition and RBC acquisition is
           //! completed, then check for volumetric error
          if(st_XintFlags.m_WBC_XintFlag & st_XintFlags.m_RBCPLT_XintFlag)
          {
              //!Reset the sample start, WBC interrupt and RBC interrupt flags
              st_XintFlags.m_SampleStartFlag = FALSE;
              st_XintFlags.m_WBC_XintFlag = FALSE;
              st_XintFlags.m_RBCPLT_XintFlag = FALSE;

              //!Check if the WBC interrupt time is within the expected range.
              //! If it is out of range, then set the error, else reset the error
              if(((stDmaAdcParametersPtr->unWBCAcqtime != 0)) && \
                      ((stDmaAdcParametersPtr->unWBCAcqtime < ACQ_TIME_LOW) || \
                  (stDmaAdcParametersPtr->unWBCAcqtime > ACQ_TIME_HIGH)))
              {
                  //!In count time sequence, analysis is not done.
                  //! So Sample analysis flag is set to false
                  st_XintFlags.m_bWBC_SampleAnalysisFlagStart = FALSE;
                  utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = TRUE;//TRUE;   //PH_NOV_02_REMOVE
              }
              else
              {
                  //!In count time sequence, analysis is not done. So Sample analysis flag is set to false
                  st_XintFlags.m_bWBC_SampleAnalysisFlagStart = FALSE;
              }

              //!Check if the RBC_PLT interrupt time is within the expected range.
              //! If it is out of range, then set the error, else reset the error
              if((stDmaAdcParametersPtr->unRBCPLTAcqtime != 0) && \
                      ((stDmaAdcParametersPtr->unRBCPLTAcqtime < ACQ_TIME_LOW) || \
              (stDmaAdcParametersPtr->unRBCPLTAcqtime >= ACQ_TIME_HIGH)))
              {
                  //!In count time sequence, analysis is not done.
                  //! So Sample analysis flag is set to false
                  st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = FALSE;
                  utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = TRUE;//TRUE;   //PH_NOV_02_REMOVE
              }
              else
              {
                  st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = FALSE;
              }

              //!Update the data acquisition completed command and send to CPU1
              //! Send WBC ad RBC_PLT acquisition time, and volumetric error information
              stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                      CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED;

              stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = \
                      stDmaAdcParametersPtr->unWBCAcqtime;
              stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = \
                      stDmaAdcParametersPtr->unRBCPLTAcqtime;
              stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = \
                      utVolumetricErrorInfo.unVolumetricInfo;
              IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,4);
              //CRH_RBC_WBC_TIME - END

              //!Reset the volumetric error information
              utVolumetricErrorInfo.unVolumetricInfo = 0;
          }
          break;

       default:
           break;
    }
}
#endif //MSK_END
/******************************************************************************/
/*!
 * \fn void SIIpcCommModule(void)
 * \brief Ipc communication module
 * \brief Description:
 *      This function process the tx & rx of ipc communication data
 * \brief Modified by Mohan: Added logic to check volumetric tube interrupt flag
 *      and process
 * \return void
 */
/******************************************************************************/
void SysIpcCommModule(void)
{
	static uint16_t status=0;

	switch(stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[0])
	{
	case CPU1_IPC_CPU2_FW_VERSION_REQ:
	    //!Reset the command received from CPU 1 to Zero
		stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[0] = 0;
		//!Reset the flag after reading the data from CPU1
		stCpu2IpcCommPtr->LocalReceiveFlag = 0;
		//!Update the shared pointer of CPU1 and CPU2 IPC buffer
		Cpu1IpcSharedBufPtr = (void *)stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[1];
		//!Update the address location of the shared buffer
		Cpu1IpcSharedBuf[0] = Cpu1IpcSharedBufPtr[0];
		//!Set the WBC cell threshold information
		SysSetWbcBloodCellInfoDefault(stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[2]);
		//!Set the RBC cell threshold information
		SysSetRbcBloodCellInfoDefault(stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[3]);
		//!Set the PLT cell threshold information
		SysSetPltBloodCellInfoDefault(stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[4]);

		//!Frame the FW version of CPU2 packet
		stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = CPU2_IPC_CPU2_FW_VERSION_RESPONSE;
		stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = \
		        (uint32_t)&stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0];
		//!Update the CPU2 version information
		stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = (uint32_t)&cCpu2FwVer[0];
		//!Update the WBC cell information pointer
		stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = (uint32_t)stWbcBloodCellInfoPtr;
		//!Update the RBC cell information pointer
		stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[4] = (uint32_t)stRbcBloodCellInfoPtr;
		//!Update the PLT cell information pointer
		stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[5] = (uint32_t)stPltBloodCellInfoPtr;

		//!Send command to CPU1
		status = IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr, 6);
		break;

	case CPU1_IPC_FIRST_COUNT_DATA_ACQUISITION_CMD:
#ifdef _ALPHA_2_BOARD_ //MSK_BEG
	    //!Reset the command received from CPU 1 to Zero
	   stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[0] = 0;
	   //!Reset the flag after reading the data from CPU1
       stCpu2IpcCommPtr->LocalReceiveFlag = 0;

       //!Set the current running mode in CPU1 to CPU2
       g_usnCurrentModeCPU2 = stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[1];
       g_usnWbcCalibratedTimeCPU2 = stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[2];
       g_usnRbcCalibratedTimeCPU2 = stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[3];

       //!Configure SDRAM for the first cycle
       DAConfigSDRAM_DMA(FIRST_COUNT_CYCLE);
       //!Reset the cell data analysis parameters for acquiring data
       SysResetCellDataAnalysisParams(FIRST_COUNT_CYCLE);
       //!Set the cycle counting flag to First cycle count
       StructDataAqParam.CountingCycleFlag = FIRST_COUNT_CYCLE;
       printf("First Measurement starts \n");

       //!Set cycle flag to First Cycle Count
       guCycleFlag = FIRST_COUNT_CYCLE;

       //!Send command to CPU1 indicating Data Acquisition started
       stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
               CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_STARTED;
       //!Update the WBC Raw data address
       stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = DAGetDestAddress(eCellWBC, FALSE);
       //!Update the RBC Raw data address
       stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = DAGetDestAddress(eCellRBC, FALSE);
       //!Update the PLT Raw data address
       stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = DAGetDestAddress(eCellPLT, FALSE);
       stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[4] = SECOND_COUNT_OFFSET;
       //!Update the WBC pulse height data address
       stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[5] = DAGetDestAddress(eCellWBC, TRUE);
       //!Update the RBC pulse height data address
       stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[6] = DAGetDestAddress(eCellRBC, TRUE);
       //!Update the PLT pulse height data address
       stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[7] = DAGetDestAddress(eCellPLT, TRUE);
       //!Send command to CPU1
       status = IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,8);

       //!Reset the flags used for acquisition and volumetric errors
       st_XintFlags.m_bWBC_SampleAnalysisFlagStart = FALSE;
       st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = FALSE;
       st_XintFlags.m_bWBC_SampleAnalysisFlagEnd = FALSE;
       st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd = FALSE;
       st_XintFlags.m_bWBC_StartInterrupt = FALSE;
       st_XintFlags.m_bRBC_PLT_StartInterrupt = FALSE;
       utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE;
       utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE;
       st_XintFlags.m_bWBC_BubbleDetected = FALSE;
       st_XintFlags.m_bRBC_PLT_BubbleDetected = FALSE;

       //!Reset WBC and RBC_PLT acquisition start time
       g_usnWBCAcqStartTime = 0;
       g_usnRBCPLTAcqStartTime = 0;

       DmaAdcParams.unWBCSampleAnalysis = 0;
       DmaAdcParams.unRBCSampleAnalysis = 0;
       DmaAdcParams.unPLTSampleAnalysis = 0;
       //!Enable the interrupts based on the current mode
       switch(g_usnCurrentModeCPU2)
       {
           case WHOLE_BLOOD_MODE:
           case PRE_DILUENT_MODE:
           case COUNTING_TIME_SEQUENCE:
           case START_UP_SEQUENCE_CMD:
           case AUTO_CALIBRATION_WB:
           case QUALITY_CONTROL_WB:
           case QUALITY_CONTROL_BODY_FLUID:
           case FLOW_CALIBRATION:
           case STARTUP_FROM_SERVICE_SCREEN_CMD:
           case STARTUP_FAILED_CMD:
               //!Enable sample start flag
               st_XintFlags.m_SampleStartFlag = TRUE;
               //!Enable the start interrupt for WBC and RBC_PLT
               //! Disable the stop interrupt for WBC and RBC_PLT
               XintRegs.XINT1CR.bit.ENABLE = 1;        // Enable VOL1
               XintRegs.XINT2CR.bit.ENABLE = 0;        // Disable VOL2
               XintRegs.XINT3CR.bit.ENABLE = 1;        // Enable VOL3
               XintRegs.XINT4CR.bit.ENABLE = 0;        // Disable VOL4
               //!Set WBC acquisition start time flag
               st_XintFlags.m_bWBC_Acq_Start_Time_Flag = TRUE;
               //!Set RBC_PLT acquisition start time flag
               st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag = TRUE;
           break;

           case BODY_FLUID_MODE:
           case COMMERCIAL_CALIBRATION_WBC:
           case PARTIAL_BLOOD_COUNT_WBC:
               //!Set WBC acquisition start time flag
               st_XintFlags.m_SampleStartFlag = TRUE;
               //!Enable the start interrupt for WBC
               //! Disable the stop interrupt for WBC
               XintRegs.XINT3CR.bit.ENABLE = 1;        // Enable VOL3
               XintRegs.XINT4CR.bit.ENABLE = 0;        // Disable VOL4
               //!Set WBC acquisition start time flag
               st_XintFlags.m_bWBC_Acq_Start_Time_Flag = TRUE;
          break;

           case COMMERCIAL_CALIBRATION_RBC:
           case COMMERCIAL_CALIBRATION_PLT:
           case PARTIAL_BLOOD_COUNT_RBC_PLT:
               //!Set RBC acquisition start time flag
               st_XintFlags.m_SampleStartFlag = TRUE;
               //!Enable the start interrupt for RBC_PLT
               //! Disable the stop interrupt for RBC_PLT
               XintRegs.XINT1CR.bit.ENABLE = 1;        // Enable VOL1
               XintRegs.XINT2CR.bit.ENABLE = 0;        // Disable VOL2
               //!Set RBC_PLT acquisition start time flag
               st_XintFlags.m_bRBC_PLT_Acq_Start_Time_Flag = TRUE;
           default:
               break;
       }
#else //MSK_END
        guCycleFlag = FIRST_COUNT_CYCLE;
        stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[0] = 0;
        stCpu2IpcCommPtr->LocalReceiveFlag = 0;
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_STARTED;
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = GetDestAddress(eCellWBC, FALSE);
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = GetDestAddress(eCellRBC, FALSE);
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = GetDestAddress(eCellPLT, FALSE);
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[4] = SECOND_COUNT_OFFSET;
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[5] = GetDestAddress(eCellWBC, TRUE);
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[6] = GetDestAddress(eCellRBC, TRUE);
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[7] = GetDestAddress(eCellPLT, TRUE);
        status = Cpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,8);
        BloodDataAcquisition(FIRST_COUNT_CYCLE);
        stCpu2IpcCommPtr->LocalReceiveFlag = 0;
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED;
        status = Cpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
#endif
		break;

	case CPU1_IPC_SECOND_COUNT_DATA_ACQUISITION_CMD:
		guCycleFlag = SECOND_COUNT_CYCLE;
		//!Reset the command received from CPU 1 to Zero
		stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[0] = 0;
		//!Reset the flag after reading the data from CPU1
		stCpu2IpcCommPtr->LocalReceiveFlag = 0;
		stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
		        CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_STARTED;
		status = IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);

		ADBloodDataAcquisition(SECOND_COUNT_CYCLE);

		stCpu2IpcCommPtr->LocalReceiveFlag = 0;
		stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
		        CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_COMPLETED;
		status = IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
		break;

	case CPU1_IPC_ADC_CAPTURED_DATA_PRINT_REQ:
	    //!Reset the command received from CPU 1 to Zero
		stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[0] = 0;
		//!Reset the flag after reading the data from CPU1
		stCpu2IpcCommPtr->LocalReceiveFlag = 0;
		printf("\n CPU2 Aquired data - Code not implemented\n");
		break;

	case CPU1_IPC_RESET_FOR_ACQ:
	       printf("Reset the parameters\n");
	       //!Reset the command received from CPU 1 to Zero
	       stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[0] = 0;
	       //!Reset the flag after reading the data from CPU1
	       stCpu2IpcCommPtr->LocalReceiveFlag = 0;

	       //!Configure SDRAM for Reset cycle count
	       DAConfigSDRAM_DMA(RESET_COUNT_CYCLE);
	       //!Reset the Cell data analysis
	       SysResetCellDataAnalysisParams(RESET_COUNT_CYCLE);
	       //!Reset the cycle count flag
	       StructDataAqParam.CountingCycleFlag = RESET_COUNT_CYCLE;

	       //!Reset the variables used for acquisition and volumetric errors
	       st_XintFlags.m_bWBC_SampleAnalysisFlagStart = FALSE;
	       st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = FALSE;
	       st_XintFlags.m_bWBC_SampleAnalysisFlagEnd = FALSE;
	       st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd = FALSE;
	       st_XintFlags.m_bWBC_StartInterrupt = FALSE;
	       st_XintFlags.m_bRBC_PLT_StartInterrupt = FALSE;
	       utVolumetricErrorInfo.unVolumetricInfo = 0;
	       st_XintFlags.m_bWBC_BubbleDetected = FALSE;
	       st_XintFlags.m_bRBC_PLT_BubbleDetected = FALSE;

	       //!Clear SDRAM
	       ClearSDRAM();

	       ADStopAdcDataAqWBC();
           ADStopAdcDataAqRBC();
           ADStopAdcDataAqPLT();

	       //!Reset the acquisition start time for WBC and EBC_PLT
	       g_usnWBCAcqStartTime = 0;
	       g_usnRBCPLTAcqStartTime = 0;
	       //!Reset the current mode
	       g_usnCurrentModeCPU2 = 0;

	       stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
	               CPU2_IPC_RESET_DONE;
	       //!Send reset Completed command to CPU1
	       status = IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
	    break;

	case CPU1_IPC_RESET_FLAG:
	    //!Reset the command received from CPU 1 to Zero
        stCpu2IpcCommPtr->unCpu1ToCpu2RxBuf[0] = 0;
        //!Reset the flag after reading the data from CPU1
        stCpu2IpcCommPtr->LocalReceiveFlag = 0;
        //!Reset the cycle count flag
        StructDataAqParam.CountingCycleFlag = RESET_COUNT_CYCLE;

        //!Reset the variables used for acquisition and volumetric errors
        st_XintFlags.m_bWBC_SampleAnalysisFlagStart = FALSE;
        st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = FALSE;
        st_XintFlags.m_bWBC_SampleAnalysisFlagEnd = FALSE;
        st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd = FALSE;
        st_XintFlags.m_bWBC_StartInterrupt = FALSE;
        st_XintFlags.m_bRBC_PLT_StartInterrupt = FALSE;
        utVolumetricErrorInfo.unVolumetricInfo = 0;
        st_XintFlags.m_bWBC_BubbleDetected = FALSE;
        st_XintFlags.m_bRBC_PLT_BubbleDetected = FALSE;

        //!Reset the acquisition start time for WBC and EBC_PLT
        g_usnWBCAcqStartTime = 0;
        g_usnRBCPLTAcqStartTime = 0;
        //!Reset the current mode
        g_usnCurrentModeCPU2 = 0;

        //!Reset all the variables used in the algorithm for all new data acquisition
        BDAAlgorithmVarReset();

        //!Send command to CPU1 indicating reset done
        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                CPU2_IPC_RESET_DONE;
        status = IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
	    break;

	default:
		break;
	}

	if(stDmaAdcParametersPtr->SampleAnalysisState == FIRST_COUNT_DATA_ANALYSIS_STATE)
	{
		//DACellDataSampleAnalysis();
	}
	else
	{
		static uint16_t blinkLed = 0,stCounter=0;
		stCounter++;
		if(blinkLed == 1 && !(stCounter%50))
		{
			blinkLed = 0;
			GPIO_WritePin(LED2_GPIO, 0);
		}
		else if(!blinkLed && !(stCounter%50))
		{
			blinkLed = 1;
			GPIO_WritePin(LED2_GPIO, 1);
		}

		//!If error in IPC communication, send IPC communication error to CPU1
		if(status == 1 || stCpu2IpcCommPtr->ErrFlag == 1)
		{
			printf("\n CPU2 Communication Error\n");
			stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = CPU2_IPC_COMMUNICATION_ERROR;
			status = IPCCpu2toCpu1IpcCmd(stCpu2IpcCommPtr);
			stCpu2IpcCommPtr->ErrFlag = 0;
			DELAY_US(500000);
		}

		//!If the Data analysis is completed, set sample analysis state to IDLE
		if(stDmaAdcParametersPtr->SampleAnalysisState == DATA_ANALYSIS_COMPLETE_STATE)
		{
			printf("\n Data analysis is finished - IPC update cpu1\n");
			stDmaAdcParametersPtr->SampleAnalysisState = DATA_ANALYSIS_IDLE_STATE;
		}
		DELAY_US(10000);
	}
}
/******************************************************************************/
/*!
 * \fn void SysSetWbcBloodCellInfoDefault(uint16_t addrBloodCellInfoDefault)
 * \param addrBloodCellInfoDefault
 * \brief To update default values for blood cell info structure
 * \brief Description:
 *      This function copy default values for blood cell info structure
 * \return void
 */
/******************************************************************************/
void SysSetWbcBloodCellInfoDefault(uint32_t addrBloodCellInfoDefault)
{
    //!Update the WBC threshold information to the corresponding structure
	stBloodCellVoltVal *stBloodCellInfoDefaultPtr;
	stBloodCellInfoDefaultPtr = (void *) addrBloodCellInfoDefault;
	memcpy(stWbcBloodCellInfoPtr, stBloodCellInfoDefaultPtr, \
	        (uint16_t)sizeof(stBloodCellVoltVal));

}
/******************************************************************************/
/*!
 * \fn void SysSetRbcBloodCellInfoDefault(uint16_t addrBloodCellInfoDefault)
 * \param addrBloodCellInfoDefault
 * \brief To update default values for blood cell info structure
 * \brief Description:
 *      This function copy default values for blood cell info structure
 * \return void
 */
/******************************************************************************/
void SysSetRbcBloodCellInfoDefault(uint32_t addrBloodCellInfoDefault)
{
    //!Update the RBC threshold information to the corresponding structure
	stBloodCellVoltVal *stBloodCellInfoDefaultPtr;
	stBloodCellInfoDefaultPtr = (void *) addrBloodCellInfoDefault;
	memcpy(stRbcBloodCellInfoPtr, stBloodCellInfoDefaultPtr, \
	        (uint16_t)sizeof(stBloodCellVoltVal));

}
/******************************************************************************/
/*!
 * \fn void SysSetPltBloodCellInfoDefault(uint16_t addrBloodCellInfoDefault)
 * \param addrBloodCellInfoDefault
 * \brief To update default values for blood cell info structure
 * \brief Description:
 *      This function copy default values for blood cell info structure
 * \return void
 */
/******************************************************************************/
void SysSetPltBloodCellInfoDefault(uint32_t addrBloodCellInfoDefault)
{
    //!Update the PLT threshold information to the corresponding structure
	stBloodCellVoltVal *stBloodCellInfoDefaultPtr;
	stBloodCellInfoDefaultPtr = (void *) addrBloodCellInfoDefault;
	memcpy(stPltBloodCellInfoPtr, stBloodCellInfoDefaultPtr, \
	        (uint16_t)sizeof(stBloodCellVoltVal));

}
/******************************************************************************/
/*!
 * \fn void SysIpcSendAnalysisInfo(Uint16 AnalysisCycle)
 * \param AnalysisCycle
 * \brief To update count values to cpu1
 * \brief Description:
 *      This function sends counts to cpu1 after analysis
 * \return void
 */
/******************************************************************************/
void SysIpcSendAnalysisInfo(Uint16 AnalysisCycle)
{
    //!If the Analysis is completed, set the command as First data analysis completed
	switch(AnalysisCycle)
	{
        case FIRST_COUNT_CYCLE:

            ADStopAdcDataAqWBC();
            ADStopAdcDataAqRBC();
            ADStopAdcDataAqPLT();

            //!Reset sample end flag
            st_XintFlags.m_SampleEndFlag = FALSE;
            //!Reset no. of WBC samples processed to zero
            g_unWBCNoSamples = 0;
            //!Reset no. of RBC & PLT samples processed to zero
            g_unRBCPLTNoSamples = 0;
            //!Reset the sample analysis length of WBC, RBC_PLT to zero
            stDmaAdcParametersPtr->unWBCSampleAnalysisLength = 0;
            stDmaAdcParametersPtr->unRBC_PLTSampleAnalysisLength = 0;
            stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
                    CPU2_IPC_FIRST_COUNT_DATA_ANALYSIS_COMPLETED;
            break;
        case SECOND_COUNT_CYCLE:
            stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
            CPU2_IPC_SECOND_COUNT_DATA_ANALYSIS_COMPLETED;
            break;

        default:
            break;
	}

	//!Check for the current mode and update the results accordingly
	switch(g_usnCurrentModeCPU2)
	{
	    case WHOLE_BLOOD_MODE:
	    case PRE_DILUENT_MODE:
	    case START_UP_SEQUENCE_CMD:
	    case AUTO_CALIBRATION_WB:
        case QUALITY_CONTROL_WB:
        case QUALITY_CONTROL_BODY_FLUID:
        case STARTUP_FROM_SERVICE_SCREEN_CMD:
        case STARTUP_FAILED_CMD:
	        //!If there is no error in WBC counting time, then update the WBC
	        //! results, else set the result to Zero
	        if(FALSE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper)
	        {
                stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = \
                        stWbcBloodCellInfoPtr->m_ulnCellCount;
	        }
	        else
	        {
	            stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 0;
	        }

            //!If there is no error in RBC_PLT counting time, then update the RBC & PLT
            //! results, else set the result to Zero
	        if(FALSE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper)
	        {
                stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = \
                        stRbcBloodCellInfoPtr->m_ulnCellCount;
                stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = \
                        stPltBloodCellInfoPtr->m_ulnCellCount;
	        }
	        else
	        {
	            stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = 0;
	            stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = 0;
	        }
            break;

        case BODY_FLUID_MODE:
	    case COMMERCIAL_CALIBRATION_WBC:
	    case PARTIAL_BLOOD_COUNT_WBC:
            //!If there is no error in WBC counting time, then update the WBC
            //! results, else set the result to Zero
	        if(FALSE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper)
            {
                stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = \
                        stWbcBloodCellInfoPtr->m_ulnCellCount;
            }
            else
            {
                stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 0;
            }
	        //!Set RBC and PLT result to Zero in these modes
	        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = 0;
	        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = 0;
	        break;

	    case COMMERCIAL_CALIBRATION_RBC:
	    case COMMERCIAL_CALIBRATION_PLT:
	    case PARTIAL_BLOOD_COUNT_RBC_PLT:
	        //!Set WBC result to Zero in these modes
	        stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 0;

            //!If there is no error in RBC_PLT counting time, then update the RBC & PLT
            //! results, else set the result to Zero
	        if(FALSE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper)
            {
                stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = \
                        stRbcBloodCellInfoPtr->m_ulnCellCount;
                stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = \
                        stPltBloodCellInfoPtr->m_ulnCellCount;
            }
            else
            {
                stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = 0;
                stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = 0;
            }
	        break;

	    default:
	        break;

	}
	//!Update the RBC pulse height address
	stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[5] = DAGetDestAddress(eCellRBC, TRUE);
	//!Update the PLT pulse height address
	stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[6] = DAGetDestAddress(eCellPLT, TRUE);
	//!Update the WBC pulse height address
	stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[7] = DAGetDestAddress(eCellWBC, TRUE);

	//!Update the algorithm execution time
	stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[4] = (uint32_t)g_usnAlgoExecutionTime;
	//!Update the volumetric error information
	stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[8] = (uint32_t)utVolumetricErrorInfo.unVolumetricInfo;

	//!Send IPC command to CPU1
	IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr, 9);
	//!Clear the volumetric error
	utVolumetricErrorInfo.unVolumetricInfo = 0;
}
/******************************************************************************/
/*!
 * \fn void SysResetCellDataAnalysisParams(Uint16 AnalysisCycle)
 * \brief rest variables
 * \brief Description:
 *      To reset the blood cell variables for acquisition
 * \return void
 */
/******************************************************************************/
void SysResetCellDataAnalysisParams(Uint16 AnalysisCycle)
{
    //!Based on the analysis cycle perform the below actions:
	switch(AnalysisCycle)
	{
	case FIRST_COUNT_CYCLE:
	    //!Get the sample data length to analyze
		stDmaAdcParametersPtr->SampleAnalysisLength = (DAGetDestAddress(eCellWBC,\
		        FALSE) + SECOND_COUNT_OFFSET);
		//!Reset the WBC cell parameters used for dell detection
		BDAResetBloodCellStatistics(stWbcBloodCellInfoPtr);
		//!Reset the RBC cell parameters used for dell detection
		BDAResetBloodCellStatistics(stRbcBloodCellInfoPtr);
		//!Reset the PLT cell parameters used for dell detection
		BDAResetBloodCellStatistics(stPltBloodCellInfoPtr);
		//!Set the Sample Analysis state to Data analysis state
		stDmaAdcParametersPtr->SampleAnalysisState = FIRST_COUNT_DATA_ANALYSIS_STATE;
		//!Get the address of WBC raw data location in SDRAM
		stDmaAdcParametersPtr->WbcAddrCounter = DAGetDestAddress(eCellWBC, FALSE);
		//!Get the address of RBC raw data location in SDRAM
		stDmaAdcParametersPtr->RbcAddrCounter = DAGetDestAddress(eCellRBC, FALSE);
		//!Get the address of PLT raw data location in SDRAM
		stDmaAdcParametersPtr->PltAddrCounter = DAGetDestAddress(eCellPLT, FALSE);

		//!Reset the no. of WBC, RBC and PLT samples processed to Zero
		g_unWBCNoSamples = 0;
		g_unRBCPLTNoSamples = 0;

		//!Get WBC sample analysis length
		stDmaAdcParametersPtr->unWBCSampleAnalysisLength = (DAGetDestAddress(eCellWBC,\
                FALSE) + SECOND_COUNT_OFFSET);
		//!Get RBC_PLT sample analysis length
        stDmaAdcParametersPtr->unRBC_PLTSampleAnalysisLength = (DAGetDestAddress(eCellPLT,\
                FALSE) + SECOND_COUNT_OFFSET);
		printf("\n Cpu2:SysResetCellDataAnalysisParams() - FIRST_COUNT_CYCLE\n");
		break;

	case SECOND_COUNT_CYCLE:
	    //!Get the sample data length to analyze
		stDmaAdcParametersPtr->SampleAnalysisLength = (DAGetDestAddress(eCellWBC, \
		        FALSE) + (SECOND_COUNT_OFFSET*2));
		//!Reset the WBC cell parameters used for dell detection
		BDAResetBloodCellStatistics(stWbcBloodCellInfoPtr);
		//!Reset the RBC cell parameters used for dell detection
		BDAResetBloodCellStatistics(stRbcBloodCellInfoPtr);
		//!Reset the PLT cell parameters used for dell detection
		BDAResetBloodCellStatistics(stPltBloodCellInfoPtr);
		//!Set the Sample Analysis state to Data analysis state
		stDmaAdcParametersPtr->SampleAnalysisState = SECOND_COUNT_DATA_ANALYSIS_STATE;
		//!Get the address of WBC raw data location in SDRAM
		stDmaAdcParametersPtr->WbcAddrCounter = \
		        (DAGetDestAddress(eCellWBC, FALSE) + (SECOND_COUNT_OFFSET));
		//!Get the address of RBC raw data location in SDRAM
		stDmaAdcParametersPtr->RbcAddrCounter = \
		        (DAGetDestAddress(eCellRBC, FALSE) + (SECOND_COUNT_OFFSET));
		//!Get the address of PLT raw data location in SDRAM
		stDmaAdcParametersPtr->PltAddrCounter = \
		        (DAGetDestAddress(eCellPLT, FALSE) + (SECOND_COUNT_OFFSET));
		printf("\n Cpu2:SysResetCellDataAnalysisParams() - SECOND_COUNT_CYCLE\n");
		break;

	case RESET_COUNT_CYCLE:
	    //!Get the sample data length to analyze
		stDmaAdcParametersPtr->SampleAnalysisLength = \
		(DAGetDestAddress(eCellWBC, FALSE) + SECOND_COUNT_OFFSET);
		//!Reset the WBC cell parameters used for dell detection
		BDAResetBloodCellStatistics(stWbcBloodCellInfoPtr);
		//!Reset the RBC cell parameters used for dell detection
		BDAResetBloodCellStatistics(stRbcBloodCellInfoPtr);
		//!Reset the PLT cell parameters used for dell detection
		BDAResetBloodCellStatistics(stPltBloodCellInfoPtr);
		//!Reset the Sample Analysis state to zero
		stDmaAdcParametersPtr->SampleAnalysisState = 0;
		//!Get the address of WBC raw data location in SDRAM
		stDmaAdcParametersPtr->WbcAddrCounter = DAGetDestAddress(eCellWBC, FALSE);
		//!Get the address of RBC raw data location in SDRAM
		stDmaAdcParametersPtr->RbcAddrCounter = DAGetDestAddress(eCellRBC, FALSE);
		//!Get the address of PLT raw data location in SDRAM
		stDmaAdcParametersPtr->PltAddrCounter = DAGetDestAddress(eCellPLT, FALSE);
		printf("\n Cpu2:SysResetCellDataAnalysisParams() - RESET_COUNT_CYCLE\n");
		break;

	default:
		break;
	}
}
#ifdef _ALPHA_2_BOARD_ //MSK_BEG
/******************************************************************************/
/*!
 * \fn void SysInitVolumetricInterrupt(void)
 * \brief interrupt config
 * \brief Description:
 *      This function is for external interrupt configuration
 * \return void
 */
/******************************************************************************/
void SysInitVolumetricInterrupt(void)
{
	//!Interrupts that are used in this example are re-mapped to
	//!ISR functions found within this file.
	EALLOW;	// This is needed to write to EALLOW protected registers
	PieVectTable.XINT1_INT = &xint1_isr;
	PieVectTable.XINT2_INT = &xint2_isr;
	PieVectTable.XINT3_INT = &xint3_isr;
	PieVectTable.XINT4_INT = &xint4_isr;


	//!Enable XINT1 and XINT2 in the PIE: Group 1 interrupt 4 & 5
	//!Enable INT1 which is connected to WAKEINT:
	//!Enable the PIE block
	PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
	//!Enable PIE Gropu 1 INT1
	PieCtrlRegs.PIEIER1.bit.INTx4 = 1;
	//!Enable PIE Gropu 1 INT2
	PieCtrlRegs.PIEIER1.bit.INTx5 = 1;
	//!Enable PIE Gropu 1 INT2
	PieCtrlRegs.PIEIER12.bit.INTx1 = 1;
	//!Enable PIE Gropu 1 INT2
	PieCtrlRegs.PIEIER12.bit.INTx2 = 1;
	//!Enable CPU INT1
	IER |= M_INT1|M_INT12;
	//IER |= M_INT12;
	EINT;
	//!This is needed to disable write to EALLOW protected registers
	EDIS;

	//!Rising edge// Falling/Rising edge interrupt
	XintRegs.XINT1CR.bit.POLARITY = XINT1_POLARITY;
	//!Rising edge// Falling/Rising edge interrupt
	XintRegs.XINT2CR.bit.POLARITY = XINT2_POLARITY;
	//!Rising edge// Falling/Rising edge interrupt
	XintRegs.XINT3CR.bit.POLARITY = XINT3_POLARITY;
	//!Rising edge// Falling/Rising edge interrupt
	XintRegs.XINT4CR.bit.POLARITY = XINT4_POLARITY;

	//!Disable XINT1
	XintRegs.XINT1CR.bit.ENABLE = 0;
	//!Disable XINT2
	XintRegs.XINT2CR.bit.ENABLE = 0;
	//!Disable XINT2
	XintRegs.XINT3CR.bit.ENABLE = 0;
	//!Disable XINT2
	XintRegs.XINT4CR.bit.ENABLE = 0;
}
/******************************************************************************/
/*!
 * \fn interrupt void xint1_isr(void)
 * \brief external interrupt 1
 * \brief  Description:
 *      interrupt handler of volumetric RBC_PLT Start interrupt
 *
 * \return void
 */
/******************************************************************************/
interrupt void xint1_isr(void)
{
	//usnExtInterruptFlag = 1;

    //!Set the RBC_PLT start flag
	st_XintFlags.m_Xint_RBCPLTStartFlag = TRUE;

	//!Acknowledge this interrupt to receive more interrupts from group 1
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
/******************************************************************************/
/*!
 * \fn interrupt void xint2_isr(void)
 * \brief external interrupt 2
 * \brief  Description:
 *      interrupt handler of volumetric RBC_PLT stop interrupt
 *
 * \return void
 */
/******************************************************************************/
interrupt void xint2_isr(void)
{
	//usnExtInterruptFlag = 2;

    //!Set the RBC_PLT stop flag
	st_XintFlags.m_Xint_RBCPLTStopFlag = TRUE;

	//!Acknowledge this interrupt to receive more interrupts from group 1
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
/******************************************************************************/
/*!
 * \fn interrupt void xint3_isr(void)
 * \brief external interrupt 3
 * \brief  Description:
 *      interrupt handler of volumetric WBC start interrupt
 *
 * \return void
 */
/******************************************************************************/
interrupt void xint3_isr(void)
{
	//usnExtInterruptFlag = 3;

    //!Set the WBC start flag
	st_XintFlags.m_Xint_WBCStartFlag = TRUE;

	//!Acknowledge this interrupt to receive more interrupts from group 1
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
}
/******************************************************************************/
/*!
 * \fn interrupt void xint4_isr(void)
 * \brief external interrupt 4
 * \brief  Description:
 *      interrupt handler of volumetric WBC stop interrupt
 *
 * \return void
 */
/******************************************************************************/
interrupt void xint4_isr(void)
{
	//usnExtInterruptFlag = 4;

    //!Set the WBC stop flag
	st_XintFlags.m_Xint_WBCStopFlag = TRUE;

	//!Acknowledge this interrupt to receive more interrupts from group 1
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
}

void SendWBCOneSecondData(void)
{
    //Send WBC Data
    unWBCCellCount = GetWBCPulse();
    //!Update the each second data to CPU1
    stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
            CPU2_ONE_SECOND_DATA;
    stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 1;
    stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = unWBCCellCount - unPrevWBCCellCount;
    IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,3);

    unPrevWBCCellCount=unWBCCellCount;
}

void SendRBCPLTOneSecondData(void)
{
    unRBCCellCount = GetRBCPulse();
    unPLTCellCount = GetPLTPulse();

    //!Update the each second data to CPU1
    stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[0] = \
            CPU2_ONE_SECOND_DATA;
    stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[1] = 2;
    stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[2] = unRBCCellCount - unPrevRBCCellCount;
    stCpu2IpcCommPtr->unCpu2ToCpu1TxBuf[3] = unPLTCellCount - unPrevPLTCellCount;
    IPCCpu2toCpu1IpcCmdData(stCpu2IpcCommPtr,4);

    unPrevRBCCellCount=unRBCCellCount;
    unPrevPLTCellCount=unPLTCellCount;
}
#endif //MSK_END
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

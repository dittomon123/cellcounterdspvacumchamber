/******************************************************************************/
/*! \file Cpu2IpcModule.c
 *
 *  \brief IPC communication between CPU1 & CPU2
 *
 *  \b Description:
 *      In this file IPC communication wrapper funtions & IPC interrupt handles
 *      are written.
 *      IPC communication between CPU1 & CPU2 for data exchanging between CPU's.
 *      CPU1 invokes CPU2 to process ADC data while motor control sequence
 *      execution
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-16-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_ipc_communication_module
//! @{
//******************************************************************************
#include <Cpu2IpcModule.h>

//------------------------------IPC contents----------------------------------//
#pragma DATA_SECTION(Cpu2IpcParam,".RAMGS13_Cpu2IpcParam");

static IpcParameters Cpu2IpcParam;
uint32_t *Cpu1pulMsgRam;
uint32_t *Cpu2pulMsgRam;

//******************************************************************************
//! Definitions used in this example
//******************************************************************************

/******************************************************************************/
//! At least 1 volatile global tIpcController instance is required when using
//! IPC API Drivers.
/******************************************************************************/
volatile tIpcController g_sIpcController1;
volatile tIpcController g_sIpcController2;

//-------------------------IPC contents Ends ---------------------------------//

/******************************************************************************/
/*!
 * \fn void IPCInitialize_IPC(void)
 * \brief Configuration of IPC interrupts & registers of CPU1
 * \brief  Description:
 *      This function configures IPC interrupts registers of CPU1
 *      initializes peripheral & enables interrupts
 *
 * \return void
 */
/******************************************************************************/

//------------------------------IPC contents----------------------------------//
void IPCInitialize_IPC(void)
{

	// Interrupts that are used in this example are re-mapped to
	// ISR functions found within this file.
	EALLOW; // This is needed to write to EALLOW protected registers
	PieVectTable.IPC0_INT = &CPU01toCPU02IPC0IntHandler;
	PieVectTable.IPC1_INT = &CPU01toCPU02IPC1IntHandler;
	EDIS;    // This is needed to disable write to EALLOW protected registers

	// Step 4. Initialize the Device Peripherals:
	IPCInitialize (&g_sIpcController1, IPC_INT0, IPC_INT0);
	IPCInitialize (&g_sIpcController2, IPC_INT1, IPC_INT1);

	// Step 5. User specific code, enable interrupts:

	// Enable CPU INT1 which is connected to Upper PIE IPC INT0-3:
	IER |= M_INT1;

	// Enable CPU01 to CPU02 INTn in the PIE: Group 11 interrupts
	PieCtrlRegs.PIEIER1.bit.INTx13 = 1;   // CPU1 to CPU2 INT0
	PieCtrlRegs.PIEIER1.bit.INTx14 = 1;   // CPU1 to CPU2 INT1

	// Enable global Interrupts and higher priority real-time debug events:
	EINT;       // Enable Global interrupt INTM
	ERTM;   // Enable Global realtime interrupt DBGM


	InitIpc();
}
/******************************************************************************/
/*!
 * \fn void IPCCpu2toCpu1IpcCommSync(void)
 * \brief To intiate communication from CPU1 to CPU2
 * \brief  Description:
 *      CPU1 shall initiates communication by polling STS bit to ensure CPU2
 *      is ready for the handshaking signals.
 *
 * \return void
 */
/******************************************************************************/
void IPCCpu2toCpu1IpcCommSync(void)
{
	Cpu2pulMsgRam = (void *)CPU02TOCPU01_PASSMSG;
	Cpu1pulMsgRam = (void *)CPU01TOCPU02_PASSMSG;

	Cpu2pulMsgRam[0] = (uint32_t)&Cpu2IpcParam.unCpu1ToCpu2RxBuf[0];
    IpcSync(FLAG17);
}
//******************************************************************************
//! \fn IpcParameters* IPCGetCpu2IpcParam(void)
//! Responds to 16/32-bit data word write command the remote CPU system
//!
//!
//! CPU system which includes the 16/32-bit data word to write to the local CPU
//! address.
//!
//! This function will allow the local CPU system to update all ipc related
//! variables and status flags
//!
//! \return Cpu2IpcParam structure address
//******************************************************************************
IpcParameters* IPCGetCpu2IpcParam(void)
{
	return &Cpu2IpcParam;
}
/******************************************************************************/
/*!
 * \fn uint16_t IPCCpu2toCpu1IpcCmdData(IpcParameters *Cpu2IpcPacket,
 *              uint16_t dataLength)
 * \param Cpu2IpcPacket data packet structure
 * \param dataLength
 * \brief To Transmit data packet to another CPU
 * \brief  Description:
 *      CPU2 shall transmit data packet to CPU1 using IPCLtoRBlockWrite call
 *
 *! \return status of command (\b STATUS_PASS =success, \b STATUS_FAIL = error
 *! because PutBuffer was full, command was not sent)
 */
/******************************************************************************/
uint16_t IPCCpu2toCpu1IpcCmdData(IpcParameters *Cpu2IpcPacket, uint16_t dataLength)
{
	uint16_t status = 0;
	if(dataLength > 8)
	{
		dataLength = 8;
	}
	status = IPCLtoRBlockWrite(&g_sIpcController1, Cpu1pulMsgRam[0], \
	        (uint32_t)&Cpu2IpcPacket->unCpu2ToCpu1TxBuf[0], dataLength, \
	        IPC_LENGTH_32_BITS,ENABLE_BLOCKING);
	// Write retry code for status failure
	return status;
}
/******************************************************************************/
/*!
 * \fn uint16_t IPCCpu2toCpu1IpcCmd(IpcParameters *Cpu2IpcPacket)
 * \param Cpu2IpcPacket data packet structure
 * \brief To Transmit data packet to another CPU
 * \brief  Description:
 *      CPU2 shall transmit data packet to CPU1 using IPCLtoRBlockWrite call
 *
 *! \return status of command (\b STATUS_PASS =success, \b STATUS_FAIL = error
 *! because PutBuffer was full, command was not sent)
 */
/******************************************************************************/
uint16_t IPCCpu2toCpu1IpcCmd(IpcParameters *Cpu2IpcPacket)
{
	uint16_t status = 0;

	status = IPCLtoRDataWrite(&g_sIpcController1, Cpu1pulMsgRam[0], \
	        Cpu2IpcPacket->unCpu2ToCpu1TxBuf[0], IPC_LENGTH_32_BITS, \
	        ENABLE_BLOCKING, NO_FLAG);
	// Write retry code for status failure
	return status;
}
//******************************************************************************
//! \fn void IPCClearCpu2IpcPacketTxBuf(IpcParameters *Cpu2IpcPacket)
//! \param Cpu2IpcPacket instance
//!
//! Clears the Tx buffer of IpcParameters
//!
//! \return None.
//******************************************************************************
void IPCClearCpu2IpcPacketTxBuf(IpcParameters *Cpu2IpcPacket)
{
	uint16_t Counter=0;
	for(Counter = 0; Counter < 8 ; Counter++)
	{
		Cpu2IpcPacket->unCpu2ToCpu1TxBuf[Counter] = 0x00000000;
	}
}
//******************************************************************************
//! \fn void IPCClearCpu2IpcPacketRxBuf(IpcParameters *Cpu2IpcPacket)
//! \param Cpu2IpcPacket instance
//!
//! Clears the Rx buffer of IpcParameters
//!
//! \return None.
//******************************************************************************
void IPCClearCpu2IpcPacketRxBuf(IpcParameters *Cpu2IpcPacket)
{
	uint16_t Counter=0;
	for(Counter = 0; Counter < 8 ; Counter++)
	{
		Cpu2IpcPacket->unCpu1ToCpu2RxBuf[Counter] = 0x00000000;
	}
}
/******************************************************************************/
/*!
 * \fn uint16_t IPCCpu2IpcReqMemAccess(void)
 * \brief To request for shared memory access
 * \brief  Description:
 *      CPU1 or CPU2 requests memory access for RW
 *
 *
 * \return status
 */
/******************************************************************************/
uint16_t IPCCpu2IpcReqMemAccess(void)
{
	uint16_t status = 0,counter = 0;
    // Request Memory Access to GS0 SARAM for CPU02
    // Set bits to let CPU02 own GS0
	status = IPCReqMemAccess (&g_sIpcController1, GS13_ACCESS, \
	        IPC_GSX_CPU2_MASTER, ENABLE_BLOCKING);


    while((MemCfgRegs.GSxMSEL.all & GS13_ACCESS) != GS13_ACCESS)
    {
    	if(status == 1)
    	{
    		break;
    	}
    	counter++;
    	if(counter > 100)
    	{
    		status = 1;
    		break;
    	}

    }

    return status;
}
//******************************************************************************
// Function to Indicate an Error has Occurred (Invalid Command Received).
//******************************************************************************
void Error(void)
{
    // An error has occurred (invalid command received). Loop forever.
    for (;;)
    {
    }
}
/******************************************************************************/
/*!
 * \fn __interrupt void CPU01toCPU02IPC0IntHandler (void)
 *
 * \brief interrupt handler for CPU2 interrupt 0
 * \brief  Description:
 *      CPU1 shall read/write to the shared memroy based on the command
 *      received from CPU2
 * \return uint32_t
 */
/******************************************************************************/

//******************************************************************************
// CPU01 to CPU02 INT0 Interrupt Handler - Handles Data Word Reads/Writes
//******************************************************************************
__interrupt void CPU01toCPU02IPC0IntHandler (void)
{
    tIpcMessage sMessage;

    // Continue processing messages as long as CPU01toCPU02 GetBuffer1 is full
    while (IpcGet (&g_sIpcController1, &sMessage,
                   DISABLE_BLOCKING)!= STATUS_FAIL)
    {
        switch (sMessage.ulcommand)
        {

        case IPC_BLOCK_WRITE:
            IPCRtoLBlockWrite(&sMessage);
            Cpu2IpcParam.LocalReceiveFlag = 1;
            break;

        case IPC_DATA_WRITE:
            IPCRtoLDataWrite(&sMessage);
            Cpu2IpcParam.LocalReceiveFlag = 1;
            break;

        default:
        	Cpu2IpcParam.ErrFlag = 1;
            break;
        }
    }
    // Acknowledge IPC INT0 Flag and PIE to receive more interrupts
    IpcRegs.IPCACK.bit.IPC0 = 1;
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
/******************************************************************************/
/*!
 * \fn __interrupt void CPU01toCPU02IPC1IntHandler (void)
 *
 * \brief interrupt handler for CPU2 interrupt 1
 * \brief  Description:
 *      CPU1 shall read/write to the shared memory based on the
 *      command  received from CPU2
 * \return uint32_t
 */
/******************************************************************************/

//******************************************************************************
// CPU01toCPU02 INT1 Interrupt Handler - Handles Data Block Reads/Writes
//******************************************************************************
__interrupt void CPU01toCPU02IPC1IntHandler (void)
{
#if 0
    tIpcMessage sMessage;

#endif
    Cpu2IpcParam.INT1Flag = 1;
    // Acknowledge IPC INT1 Flag and PIE to receive more interrupts
    IpcRegs.IPCACK.bit.IPC1 = 1;
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
//--------------------------IPC contents Ends --------------------------------//
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

/******************************************************************************/
/*! \file Cpu2BloodDataAnalysis.c
 *
 *  \brief Post processing of ADC raw data
 *
 *  \b  Description:
 *      In this file the Analog raw data shall be processed.
 *      Here, WBC, RBC & PLT counts shall be calculated
 *      Alaong with counts, peak information shall be stored in RAM
 *
 *
 *   $Version: $ 0.1
 *   $Date:    $ May-21-2015
 *   $Author:  $ GURUDUTT R, ADITYA B G, Payam Hemadri
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/

//******************************************************************************
//! \addtogroup cpu1_data_analysis_module
//! @{
//******************************************************************************
#include <Cpu2BloodDataAnalysis.h>
#include <Cpu2ConfigSDRAM.h>
#include <stdio.h>
#include <stdlib.h>
#include "Cpu2InitBoard.h"

#include "fpu_vector.h"       // FPU headerfile to access memcpy_fast_far()


extern IpcParameters *stCpu2IpcCommPtr;

/******************************************************************************/
/*!
 * \var m_bRBCPeakDetected
 * \brief  To indicate RBC peak is detected
 */
/******************************************************************************/
bool_t m_bRBCPeakDetected;

/******************************************************************************/
/*!
 * \var m_bWBCPeakDetected
 * \brief  To indicate WBC peak is detected
 */
/******************************************************************************/
bool_t m_bWBCPeakDetected;

/******************************************************************************/
/*!
 * \var m_bPLTPeakDetected
 * \brief  To indicate PLT peak is detected
 */
/******************************************************************************/
bool_t m_bPLTPeakDetected;

/******************************************************************************/
/*!
 * \var unRBCPulseHeight
 * \brief  To store RBC pulse height
 */
/******************************************************************************/
unsigned int unRBCPulseHeight = 0;

/******************************************************************************/
/*!
 * \def MIN_RBC_PLT_GAIN
 * \brief  To store Minimum RBC PLT Gain factor to be considered
 * - presently not used
 */
/******************************************************************************/
#define MIN_RBC_PLT_GAIN    1.07

/******************************************************************************/
/*!
 * \def MAX_RBC_PLT_GAIN
 * \brief  To store maximum RBC PLT Gain factor to be considered 
 * - presently not used
 */
/******************************************************************************/
#define MAX_RBC_PLT_GAIN    1.14

/******************************************************************************/
/*!
 * \def RBC_MOVING_WINDOW
 * \brief  Moving window averaging size to reduce noise in the raw data
 */
/******************************************************************************/
#define RBC_MOVING_WINDOW                    2

/******************************************************************************/
/*!
 * \def MOVING_WINDOW
 * \brief  Moving window averaging size to reduce noise in the raw data
 */
/******************************************************************************/
#define MOVING_WINDOW                       8

/******************************************************************************/
/*!
 * \def PROCESS_ARRAY_SIZE
 * \brief  Raw data is processed in the array of 100 sample to detect peaks 
 * and peak to peak amplitude 
 */
/******************************************************************************/
#define PROCESS_ARRAY_SIZE                  100

/******************************************************************************/
/*!
 * \def MINIMUM_PULSE_HEIGHT
 * \brief  To store minimum pulse height to be considered
 */
/******************************************************************************/
#define MINIMUM_PULSE_HEIGHT                800

/******************************************************************************/
/*!
 * \def MAX_PERCENTAGE_PEAK_DETECTION
 * \brief  To store Maximum percentage of the peak to be considered 
 * - presently not used
 */
/******************************************************************************/
#define MAX_PERCENTAGE_PEAK_DETECTION       0.8

/******************************************************************************/
/*!
 * \def WBC_MAX_PULSE_DETECTION
 * \brief  To store Maximum pulse height percentage for WBC - 
 * - presently not used
 */
/******************************************************************************/
#define WBC_MAX_PULSE_DETECTION             0.97

/******************************************************************************/
/*!
 * \def RBC_MAX_PULSE_DETECTION
 * \brief  To store Maximum pulse height percentage for RBC - 
 * - presently not used
 */
/******************************************************************************/
#define RBC_MAX_PULSE_DETECTION             0.97

/******************************************************************************/
/*!
 * \def PLT_MAX_PULSE_DETECTION
 * \brief  To store Maximum pulse height percentage for RBC - 
 * - presently not used
 */
/******************************************************************************/
#define PLT_MAX_PULSE_DETECTION             0.97

/******************************************************************************/
/*!
 * \def PLT_CELL_SIZE
 * \brief  To store PLT cell size - presently not used
 */
/******************************************************************************/
#define PLT_CELL_SIZE                       5000

/******************************************************************************/
/*!
 * \def PHASE_DELAY
 * \brief  To store phase delay with the Raw data and processed data
 * - presently not used
 */
/******************************************************************************/
#define PHASE_DELAY                         80

/******************************************************************************/
/*!
 * \def WBC_ARRAY_SIZE
 * \brief  To store the location of WBC data in the processed array
 */
/******************************************************************************/
#define WBC_ARRAY_SIZE                      3072+200

/******************************************************************************/
/*!
 * \def RBC_ARRAY_SIZE
 * \brief  To store the location of RBC data in the processed array
 */
/******************************************************************************/
#define RBC_ARRAY_SIZE                      WBC_ARRAY_SIZE + 200

/******************************************************************************/
/*!
 * \def PLT_ARRAY_SIZE
 * \brief  To store the location of PLT data in the processed array
 */
/******************************************************************************/
#define PLT_ARRAY_SIZE                      RBC_ARRAY_SIZE + 200

/******************************************************************************/
/*!
 * \def WBC_BASELINE
 * \brief  To store the minumum value for WBC below which the signal is not 
 * - considered for peak detection - Presently not used
 */
/******************************************************************************/
#define WBC_BASELINE    31000

/******************************************************************************/
/*!
 * \def RBC_BASELINE
 * \brief  To store the minimum value for RBC below which the signal is not
 * - considered for peak detection - Presently not used
 */
/******************************************************************************/
#define RBC_BASELINE    31000

/******************************************************************************/
/*!
 * \def PLT_BASELINE
 * \brief  To store the minimum value for PLT below which the signal is not
 * - considered for peak detection - Presently not used
 */
/******************************************************************************/
#define PLT_BASELINE    29000

extern Uint16 usnAdcDataCapture[4096];

/******************************************************************************/
/*!
 * \var count
 * \brief  To store the count value for the data processed
 */
/******************************************************************************/
unsigned long int count = 0;

/******************************************************************************/
/*!
 * \var finalcountWBC
 * \brief  To store final WBC count - presently not used
 */
/******************************************************************************/
unsigned long int finalcountWBC = 0;

/******************************************************************************/
/*!
 * \var finalcountRBC
 * \brief  To store final RBC count - presently not used
 */
/******************************************************************************/
unsigned long int finalcountRBC = 0;

/******************************************************************************/
/*!
 * \def finalcountPLT
 * \brief  To store final PLT count - presently not used
 */
/******************************************************************************/
unsigned long int finalcountPLT = 0;

/******************************************************************************/
/*!
 * \var garr_usnMovingWindow1WBC[MOVING_WINDOW]
 * \brief  To store the averaged of moving window of WBC raw data
 */
/******************************************************************************/
unsigned long garr_usnMovingWindow1WBC[MOVING_WINDOW];

/******************************************************************************/
/*!
 * \var garr_usnMovingWindow1RBC[MOVING_WINDOW]
 * \brief  To store the averaged of moving window of RBC raw data
 */
/******************************************************************************/
unsigned long garr_usnMovingWindow1RBC[MOVING_WINDOW];
//unsigned long garr_usnMovingWindow1RBC[RBC_MOVING_WINDOW];

/******************************************************************************/
/*!
 * \var garr_usnMovingWindow1PLT[MOVING_WINDOW];
 * \brief  To store the averaged of moving window of PLT raw data
 */
/******************************************************************************/
unsigned long garr_usnMovingWindow1PLT[MOVING_WINDOW];

/******************************************************************************/
/*!
 * \var g_ucWBCAvgPtr1
 * \brief  Pointer to the WBC averaged array
 */
/******************************************************************************/
unsigned char g_ucWBCAvgPtr1;

/******************************************************************************/
/*!
 * \var g_ucRBCAvgPtr1
 * \brief  Pointer to the RBC averaged array
 */
/******************************************************************************/
unsigned char g_ucRBCAvgPtr1;

/******************************************************************************/
/*!
 * \var g_ucPLTAvgPtr1
 * \brief  Pointer to the PLT averaged array
 */
/******************************************************************************/
unsigned char g_ucPLTAvgPtr1;

/******************************************************************************/
/*!
 * \var ucWBCProcessIndex
 * \brief  Pointer to the WBC processing array
 */
/******************************************************************************/
unsigned int ucWBCProcessIndex = 0;

/******************************************************************************/
/*!
 * \var ucRBCProcessIndex
 * \brief  Pointer to the RBC processing array
 */
/******************************************************************************/
unsigned int ucRBCProcessIndex = 0;

/******************************************************************************/
/*!
 * \var ucPLTProcessIndex
 * \brief  Pointer to the PLT processing array
 */
/******************************************************************************/
unsigned int ucPLTProcessIndex = 0;

/******************************************************************************/
/*!
 * \var ucWBCSquareIndex
 * \brief  To store the squared value of the averaged WBC raw data
 * - presently not used
 */
/******************************************************************************/
unsigned int ucWBCSquareIndex = 0;

/******************************************************************************/
/*!
 * \var ucRBCSquareIndex
 * \brief  To store the squared value of the averaged RBC raw data
 * - presently not used
 */
/******************************************************************************/
unsigned int ucRBCSquareIndex = 0;

/******************************************************************************/
/*!
 * \var ucPLTSquareIndex
  * \brief  To store the squared value of the averaged PLT raw data
 * - presently not used
 */
/******************************************************************************/
unsigned int ucPLTSquareIndex = 0;

/******************************************************************************/
/*!
 * \def RBC_MIN_VOLTAGE
 * \brief  To store minimum RBC value below which the peaks are not considered
 * - presently not used
 */
/******************************************************************************/
#define RBC_MIN_VOLTAGE                         25000

/******************************************************************************/
/*!
 * \def RBC_LEFT_MIN
 * \brief  Presently not used
 */
/******************************************************************************/
#define RBC_LEFT_MIN                            RBC_MIN_VOLTAGE + 1000

/******************************************************************************/
/*!
 * \def RBC_NOTCH_LIMIT
 * \brief  To store the minimum notch to be considered when subsequent peak occurs
 */
/******************************************************************************/
#define RBC_NOTCH_LIMIT                         250

/******************************************************************************/
/*!
 * \def RBC_NEXT_PEAK_INTERVAL
 * \brief  To the minimum interval to be considered to declare the peak between
 * two consecutive peaks. This count shall help in obtaining the left minimum
 * value of the consecutive peak
 */
/******************************************************************************/
#define RBC_NEXT_PEAK_INTERVAL                  70

/******************************************************************************/
/*!
 * \def m_bRBCNotPeak
 * \brief  To indicate the regular or consecutive peak is valid or not
 */
/******************************************************************************/
bool m_bRBCNotPeak = false;

/******************************************************************************/
/*!
 * \def unRBCMinVoltage
 * \brief  To store RBC Minimum Voltage below which the peak is not considered -
 * presently not used
 */
/******************************************************************************/
unsigned int unRBCMinVoltage = RBC_MIN_VOLTAGE;

/******************************************************************************/
/*!
 * \def ulnSumRBCAvg1
 * \brief  To store averaged value of the RBC samples for processing
 */
/******************************************************************************/
unsigned int ulnSumRBCAvg1 = 0;

/******************************************************************************/
/*!
 * \def ulnRBCPrevVoltage
 * \brief  To store previous RBC processed sample
 */
/******************************************************************************/
unsigned int ulnRBCPrevVoltage = 0;

/******************************************************************************/
/*!
 * \def usnRBCPrevPeaktoPeak
 * \brief  To store Previous RBC peak to peak value
 */
/******************************************************************************/
unsigned int usnRBCPrevPeaktoPeak = 0;

/******************************************************************************/
/*!
 * \def usnRBCPrevLeftMinValue
 * \brief  To store Previous RBC left minimum value. This is used when
 * consecutive peaks occur
 */
/******************************************************************************/
unsigned int usnRBCPrevLeftMinValue = RBC_MIN_VOLTAGE;

/******************************************************************************/
/*!
 * \def usnRBCNextPeakInterval
 * \brief  To store the interval between two peaks. It is used to determine
 * the consecutive peaks
 */
/******************************************************************************/
unsigned int usnRBCNextPeakInterval = 0;

/******************************************************************************/
/*!
 * \def WBC_MIN_VOLTAGE
 * \brief  To store minimum WBC value below which the peaks are not considered
 * - presently not used
 */
/******************************************************************************/
#define WBC_MIN_VOLTAGE                         32000

/******************************************************************************/
/*!
 * \def WBC_LEFT_MIN
 * \brief  Presently not used
 */
/******************************************************************************/
#define WBC_LEFT_MIN                            33000

/******************************************************************************/
/*!
 * \def WBC_NOTCH_LIMIT
 * \brief To store the minimum notch to be considered when subsequent peak occurs
 */
/******************************************************************************/
#define WBC_NOTCH_LIMIT                         250

/******************************************************************************/
/*!
 * \def WBC_NEXT_PEAK_INTERVAL
 * \brief  To the minimum interval to be considered to declare the peak between
 * two consecutive peaks. This count shall help in obtaining the left minimum
 * value of the consecutive peak
 */
/******************************************************************************/
#define WBC_NEXT_PEAK_INTERVAL                  70

/******************************************************************************/
/*!
 * \def m_bWBCNotPeak
 * \brief  To indicate the regular or consecutive peak is valid or not
 */
/******************************************************************************/
bool m_bWBCNotPeak = false;

/******************************************************************************/
/*!
 * \def m_bWBCConsecutivePeak
 * \brief  To indicate it is consecutive peak, so that the left minimum value
 * is taken from the previous peak
 */
/******************************************************************************/
bool m_bWBCConsecutivePeak = false;

/******************************************************************************/
/*!
 * \def unWBCMinVoltage
 * \brief  To store WBC minimum voltage below which peak is not considered
 */
/******************************************************************************/
unsigned int unWBCMinVoltage = WBC_MIN_VOLTAGE;

/******************************************************************************/
/*!
 * \def usnWBCPrevPeaktoPeak
 * \brief  To store Previous WBC peak to peak value
 */
/******************************************************************************/
unsigned int usnWBCPrevPeaktoPeak = 0;

/******************************************************************************/
/*!
 * \def usnWBCPrevLeftMinValue
 * \brief  To store Previous WBC left minimum value. This is used when
 * consecutive peaks occur
 */
/******************************************************************************/
unsigned int usnWBCPrevLeftMinValue = WBC_MIN_VOLTAGE;

/******************************************************************************/
/*!
 * \def ulnSumWBCAvg1
 * \brief  To store averaged value of the RBC samples for processing
 */
/******************************************************************************/
unsigned int ulnSumWBCAvg1 = 0;

/******************************************************************************/
/*!
 * \def ulnWBCPrevVoltage
 * \brief  To store previous RBC processed sample
 */
/******************************************************************************/
unsigned int ulnWBCPrevVoltage = 0;

/******************************************************************************/
/*!
 * \def usnPLTNextPeakInterval
 * \brief  To store the interval between two peaks. It is used to determine
 * the consecutive peaks
 */
/******************************************************************************/
unsigned int usnWBCNextPeakInterval = 0;

/******************************************************************************/
/*!
 * \def PLT_MIN_VOLTAGE
 * \brief  To store minimum RBC value below which the peaks are not considered
 */
/******************************************************************************/
#define PLT_MIN_VOLTAGE                     30000

/******************************************************************************/
/*!
 * \def PLT_LEFT_MIN
 * \brief  Presently not used
 */
/******************************************************************************/
#define PLT_LEFT_MIN                            PLT_MIN_VOLTAGE + 1000

/******************************************************************************/
/*!
 * \def PLT_NEXT_PEAK_INTERVAL
 * \brief  To the minimum interval to be considered to declare the peak between
 * two consecutive peaks. This count shall help in obtaining the left minimum
 * value of the consecutive peak
 */
/******************************************************************************/
#define PLT_NEXT_PEAK_INTERVAL                  70

/******************************************************************************/
/*!
 * \def PLT_NOTCH_LIMIT
 * \brief  To store the minimum notch to be considered when subsequent peak occurs
 */
/******************************************************************************/
#define PLT_NOTCH_LIMIT                         300

/******************************************************************************/
/*!
 * \def m_bPLTNotPeak
 * \brief  To indicate the regular or consecutive peak is valid or not
 */
/******************************************************************************/
bool m_bPLTNotPeak = false;

/******************************************************************************/
/*!
 * \def m_bPLTConsecutivePeak
 * \brief  To indicate it is consecutive peak, so that the left minimum value
 * is taken from the previous peak
 */
/******************************************************************************/
bool m_bPLTConsecutivePeak = false;

/******************************************************************************/
/*!
 * \def m_bRBCConsecutivePeak
 * \brief  To indicate it is consecutive peak, so that the left minimum value
 * is taken from the previous peak
 */
/******************************************************************************/
bool m_bRBCConsecutivePeak = false;

/******************************************************************************/
/*!
 * \def unPLTMinVoltage
 * \brief  To store PLT Minimum Voltage below which the peak is not considered -
 * presently not used
 */
/******************************************************************************/
unsigned int unPLTMinVoltage = PLT_MIN_VOLTAGE;

/******************************************************************************/
/*!
 * \def ulnSumPLTAvg1
 * \brief  To store averaged value of the RBC samples for processing
 */
/******************************************************************************/
unsigned int ulnSumPLTAvg1=0;

/******************************************************************************/
/*!
 * \def ulnPLTPrevVoltage
 * \brief  To store previous PLT processed sample
 */
/******************************************************************************/
unsigned int ulnPLTPrevVoltage = 0;

/******************************************************************************/
/*!
 * \def usnPLTPrevPeaktoPeak
 * \brief  To store Previous PLT peak to peak value
 */
/******************************************************************************/
unsigned int usnPLTPrevPeaktoPeak=0;

/******************************************************************************/
/*!
 * \def usnPLTPrevLeftMinValue
 * \brief  To store Previous PLT left minimum value. This is used when
 * consecutive peaks occur
 */
/******************************************************************************/
unsigned int usnPLTPrevLeftMinValue = PLT_MIN_VOLTAGE;

/******************************************************************************/
/*!
 * \def usnPLTNextPeakInterval
 * \brief  To store the interval between two peaks. It is used to determine
 * the consecutive peaks
 */
/******************************************************************************/
unsigned int usnPLTNextPeakInterval = 0;

/******************************************************************************/
/*!
 * \def usnWBCPeakDetectionCount
 * \brief To store the no. of samples processed after peak is detected
 */
/******************************************************************************/
int usnWBCPeakDetectionCount = 0;

/******************************************************************************/
/*!
 * \def usnRBCPeakDetectionCount
 * \brief To store the no. of samples processed after peak is detected
 */
/******************************************************************************/
int usnRBCPeakDetectionCount = 0;

/******************************************************************************/
/*!
 * \def usnPLTPeakDetectionCount
 * \brief To store the no. of samples processed after peak is detected
 */
/******************************************************************************/
int usnPLTPeakDetectionCount = 0;

/******************************************************************************/
/*!
 * \def usnPLTPeakDetectionCount
 * \brief To store the no. of WBC consecutive peak is detected
 */
/******************************************************************************/
unsigned int m_usnWBCConsecutivePeakCount = 0;

/******************************************************************************/
/*!
 * \def usnPLTPeakDetectionCount
 * \brief To store the no. of RBC consecutive peak is detected
 */
/******************************************************************************/
unsigned int m_usnRBCConsecutivePeakCount = 0;

/******************************************************************************/
/*!
 * \def usnPLTPeakDetectionCount
 * \brief To store the no. of PLT consecutive peak is detected
 */
/******************************************************************************/

unsigned int m_usnPLTConsecutivePeakCount = 0;

/******************************************************************************/
/*!
 * \def WBC_MAX_PERCENTAGE_PEAK_DETECTION
 * \brief  Not used
 */
/******************************************************************************/
#define WBC_MAX_PERCENTAGE_PEAK_DETECTION       0.2

/******************************************************************************/
/*!
 * \def WBC_PHASE_DELAY
 * \brief  Not used
 */
/******************************************************************************/
#define WBC_PHASE_DELAY                         5

/*! Buffer for algorithm and cell signal processing */
/******************************************************************************/
/*!
 * \brief structure for Blood cell statistics
 * \struct stBloodCellVoltVal
 * \var sBloodCellStatistics
 *
 */
/******************************************************************************/
#pragma DATA_SECTION(sBloodCellStatistics,".RAMGS12_Cpu2AdcBuf");

//!These default members values are updated by CPU1 th' IPC command along with
//!Firmware info
static stBloodCellVoltVal sBloodCellStatistics[FOUR] = {
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, FLOAT_MAX, FLOAT_MIN, CELL_LOW_V_RBC, CELL_HIGH_V_RBC,
	APERTURE_LENGTH_RBC_PLT, APERTURE_DIA_RBC_PLT,
	APERTURE_LENGTH_ADJUSTED_RBC_PLT, YES, 0, 0, 0, DELTA_MIN_MAX_RBC,
	PULSEWIDTH_MIN_RBC, PULSEWIDTH_MAX_RBC,	FLOAT_MAX, FLOAT_MIN,
	CELL_LOW_V_PLT, CELL_HIGH_V_PLT, APERTURE_LENGTH_RBC_PLT,
	APERTURE_DIA_RBC_PLT, APERTURE_LENGTH_ADJUSTED_RBC_PLT,	YES, 0, 0, 0,
	DELTA_MIN_MAX_PLT, PULSEWIDTH_MIN_PLT, PULSEWIDTH_MAX_PLT,
	FLOAT_MAX, FLOAT_MIN, CELL_LOW_V_WBC, CELL_HIGH_V_WBC, APERTURE_LENGTH_WBC,
	APERTURE_DIA_WBC, APERTURE_LENGTH_ADJUSTED_WBC,	YES, 0, 0, 0,
	DELTA_MIN_MAX_WBC, PULSEWIDTH_MIN_WBC, PULSEWIDTH_MAX_WBC
};
/******************************************************************************/
/*!
 * \fn extern Uint16 DAGetDestAddress(Uint16 Type);
 * \param Type Type of blood channel selection
 * \brief external function definition:
 *
 */
/******************************************************************************/
extern Uint32 DAGetDestAddress(E_CELL_TYPE eTypeofCell, bool_t bAddofCellHtReq);
extern DmaAdcParameters DmaAdcParams;

/******************************************************************************/
/*!
 * \fn void BDAPeakDetectionAlgorithm(unsigned int unVoltage)
 * \param unVoltage
 * \brief Peak detection algorithm
 * \brief  Description:
 *      This function takes ADC raw data and processed to detect peaks
 *      peaks shall be counter and stored in m_ulnCellCount variable
 *      - Not used
 * \return void
 */
/******************************************************************************/
void BDAPeakDetectionAlgorithm(unsigned int unVoltage) {
    if ((unVoltage > sBloodCellStatistics[2].m_fCellLowVolt)
            && (unVoltage < sBloodCellStatistics[2].m_fCellHighVolt)) {
        sBloodCellStatistics[2].m_Flag = 1;
        sBloodCellStatistics[2].m_PulseWidth++;

        if (sBloodCellStatistics[2].m_PulseWidth >= 1000000) {
            sBloodCellStatistics[2].m_PulseWidth = 0;
        }
        // Check and get the maxima
        if (unVoltage > sBloodCellStatistics[2].m_fMaxVolt) {
            sBloodCellStatistics[2].m_fMaxVolt = unVoltage;
        }
        // Check and get he minima
        if (unVoltage < sBloodCellStatistics[2].m_fMinVolt) {
            sBloodCellStatistics[2].m_fMinVolt = unVoltage;
        }

        // Expected upcoming pulse is for the maxima
        if (sBloodCellStatistics[2].m_ucLookForMax == YES) {
            // Checkout for the deviation of the double pulse which will be of min 0.1V gap
            if (unVoltage
                    < (sBloodCellStatistics[2].m_fMaxVolt
                            - sBloodCellStatistics[2].m_deltaMinMax)
                    && (sBloodCellStatistics[2].m_PulseWidth
                            < sBloodCellStatistics[2].m_pulseWidthMax
                            && sBloodCellStatistics[2].m_PulseWidth
                                    > sBloodCellStatistics[2].m_pulseWidthMin))	//WBC
                    {
//                fVolume = ((0.000000000018200113844627 /(FLOAT32)unVoltage ) - 0.00000000000606670461487569);
                // Cell has been identified
                sBloodCellStatistics[2].m_ulnCellCount++;
//                g_BloodCellCountsWBC++;
                // Update the the minima with the current voltage to identify the slope
                sBloodCellStatistics[2].m_fMinVolt = unVoltage;
                // Check for the minima
                sBloodCellStatistics[2].m_ucLookForMax = NO;
            }
        } else {
            // Expected upcoming pulse is for the minima
            if (unVoltage
                    > (sBloodCellStatistics[2].m_fMinVolt
                            + sBloodCellStatistics[2].m_deltaMinMax)) {
                // Update the the maxima with the current voltage to identify the slope
                sBloodCellStatistics[2].m_fMaxVolt = unVoltage;
                // Check for the maxima
                sBloodCellStatistics[2].m_ucLookForMax = YES;
                sBloodCellStatistics[2].m_PulseWidth = 0;
            }
        }
    } else if (sBloodCellStatistics[2].m_Flag == 1) {
        sBloodCellStatistics[2].m_fMaxVolt = FLOAT_MIN;
        sBloodCellStatistics[2].m_fMinVolt = FLOAT_MAX;
        sBloodCellStatistics[2].m_ucLookForMax = NO;
        sBloodCellStatistics[2].m_PulseWidth = 0;
        sBloodCellStatistics[2].m_Flag = 0;
    }
}

/********************************************************************************/
/*!
 * \fn void BDAWBCPeakDetectionAlgorithm(unsigned int unVoltage)
 * \param unVoltage
 * \brief Peak detection algorithm
 * \brief  Description:
 *      This function takes WBC ADC raw data and processed to detect peaks
 *      peaks shall be counter and stored in m_ulnCellCount variable
 * \brief Modified by Mohan: Added function call to store the pulse height
 *      of the cells
 * \return void
 */
/********************************************************************************/
void BDAWBCPeakDetectionAlgorithm(unsigned int unVoltageIn)
{
    //!Var ulnVoltage is used for processing the algorithm
    unsigned long ulnVoltage;

    //!Var nTraverseIndex is used to traverse through the processing array
    int nTraverseIndex = 0;
    //!Get the present pointer in the processing array
    //nTraverseIndex = ucWBCProcessIndex;
    //!Var cMaxPeakIndex to point to the position of the Max peak position
    //!Var cLeftMinPeakIndex to point to the position of the left side lowest
    //! point in the peak
    int cMaxPeakIndex = 0, cLeftMinPeakIndex = 0;
    //!Var to store the Maximum peak value
    unsigned int usnMaxValue = 0;
    //!Var to store the minimum value of the left side of the peak
    unsigned int usnLeftMinValue = 0;
    //!Var to store the half of the peak width (Left side half)
    unsigned int usnLeftWidthValue = 0;

    //!Var to store the peak to peak value of the peak
    unsigned int usnPeaktoPeakValue = 0;

    int i;
    for(i=0;i<1024;i++)
    {
        unVoltageIn = usnAdcDataCapture[i];

        //!Get the present pointer in the processing array
        nTraverseIndex = ucWBCProcessIndex;

    //!Divide the input sample by 8 (Left shift by 3)
    //! Moving window average of 8 is considered for averaging
    //! Divide the sample and update to array
    unVoltageIn = unVoltageIn>>3;
    //!Remove the old divided sample from the sum of the array
    ulnSumWBCAvg1 = ulnSumWBCAvg1 - garr_usnMovingWindow1WBC[g_ucWBCAvgPtr1];
    //!Update the array with the latest divided sample
    garr_usnMovingWindow1WBC[g_ucWBCAvgPtr1] = unVoltageIn;
    //!Add the latest divided value to the sum
    ulnSumWBCAvg1 = ulnSumWBCAvg1+garr_usnMovingWindow1WBC[g_ucWBCAvgPtr1];
    //!Move the moving window average pointer to the next location
    g_ucWBCAvgPtr1++;
    //!Reset the moving window average pointer if it reaches maximum array size
    if(g_ucWBCAvgPtr1>=MOVING_WINDOW)
    {
        g_ucWBCAvgPtr1 = 0;
    }

    //!Update the averaged value to the processing variable
    ulnVoltage = ulnSumWBCAvg1;
    //!Update the processing value to the processing array
    usnAdcDataCapture[WBC_ARRAY_SIZE + ucWBCProcessIndex] = ulnVoltage;
    //!Increment the processing array pointer so that the next processed
    //! value shall be updated to the next location
    ucWBCProcessIndex++;
    //!Reset the processing array pointer if it reaches maximum processing array size
    if(ucWBCProcessIndex >= PROCESS_ARRAY_SIZE)
    {
        ucWBCProcessIndex = 0;
    }

    //!WBC next peak interval
    usnWBCNextPeakInterval++;

    //!Sample refers to the processed sample
    //!Process the data sample if it is above the minimum specified voltage
    if(ulnVoltage > unWBCMinVoltage)
    {
        //! Check for the maximum value in the sample by comparing with the previous sample
        if(ulnWBCPrevVoltage < ulnVoltage)
        {
            //!If the the signal is increasing slope update the max voltage and
            //! set the peak detection count to zero and set the look for max
            if(ulnVoltage > sBloodCellStatistics[WBC].m_fMaxVolt)
            {
                sBloodCellStatistics[WBC].m_fMaxVolt = ulnVoltage;

                sBloodCellStatistics[WBC].m_ucLookForMax = YES;
                usnWBCPeakDetectionCount = 0;
            }
        }
        else
        {
            //!If it is falling slope and if the peak is detected, then
            //! increment the peak detection count
            if(YES == sBloodCellStatistics[WBC].m_ucLookForMax)
            {
                usnWBCPeakDetectionCount++;
            }
        }

        //!If the signal is below the minimum voltage, then set the minimum voltage
        if(ulnVoltage < sBloodCellStatistics[WBC].m_fMinVolt)
        {
            sBloodCellStatistics[WBC].m_fMinVolt = ulnVoltage;
        }

        //!Peak is detected and next peak has not occurred
        if(usnWBCPeakDetectionCount>=4)
        {
            //!If the peak is detected
            if(sBloodCellStatistics[WBC].m_ucLookForMax == YES )
            {
                //!Var count is to process through the array
                int iCount;

                //!Point to the max peak location in the process array
                cMaxPeakIndex = nTraverseIndex - 4;
                //!If the index is negative, then make it positive since it is
                //! an circular buffer
                if(cMaxPeakIndex < 0)
                {
                    cMaxPeakIndex = PROCESS_ARRAY_SIZE + cMaxPeakIndex;
                }
                //!Get the max value from the processing array using max peak index
                usnMaxValue = usnAdcDataCapture[WBC_ARRAY_SIZE + \
                                                        cMaxPeakIndex];

                //!Get the left minimum value of the peak
                usnLeftMinValue = sBloodCellStatistics[WBC].m_fMinVolt;

                //!Check whether the peak detected is regular peak or consecutive peak
                //! If the next peak interval is less than the limit specified,
                //! then it is consecutive peak
                //! If it is consecutive peak, if the left minimum value is greater
                //! than the previous left minimum  +1000, then use the previous peak's
                //! left minimum value.
                //! If it is consecutive peak and if the notch is very small, then
                //! it is not valid peak.
                if(usnWBCNextPeakInterval <= WBC_NEXT_PEAK_INTERVAL)
                {
                    //if(usnLeftMinValue > (usnWBCPrevLeftMinValue + \
                    //        (usnWBCPrevPeaktoPeak%10)))
                    if(usnLeftMinValue > (usnWBCPrevLeftMinValue + \
                              (1000)))
                    {
                        if((usnMaxValue - usnLeftMinValue) < WBC_NOTCH_LIMIT)
                        {
                            //Donot capture it as peak
                            m_bWBCNotPeak = true;
                        }
                        else
                        {
                            m_bWBCConsecutivePeak = true;
                            m_usnWBCConsecutivePeakCount++;
                        }
                        if((m_usnWBCConsecutivePeakCount <= 4) && (false == m_bWBCNotPeak))
                        {
                            usnLeftMinValue = usnWBCPrevLeftMinValue;
                        }
                        else
                        {
                            m_bWBCConsecutivePeak = false;
                        }
                    }
                }

                //!Update peak to peak value
                usnPeaktoPeakValue = usnMaxValue - usnLeftMinValue;

                //!Check for Peak to peak, if peak to peak amplitude is
                //!valid, then process further.
                if((usnPeaktoPeakValue > \
                        sBloodCellStatistics[WBC].m_fCellLowVolt) && \
                        (usnPeaktoPeakValue < \
                                sBloodCellStatistics[WBC].m_fCellHighVolt) )
                {
                    //!If it is not consecutive peak, then check for the pulse width.
                    //! If it is consecutive peak, pulse width cannot be calculated.
                    //! So the pulse is valid
                    if(false == m_bWBCConsecutivePeak)
                    {
                        m_usnWBCConsecutivePeakCount =0;
                        //!Get the 50% of the max value
                        unsigned int usnTempValue = usnMaxValue - \
                                (usnPeaktoPeakValue >> 1);

                        //!Check for the  50% least value on the left side of the peak
                        //!Assign Max value to the Minimum
                        usnLeftWidthValue = usnMaxValue;
                        //!Assign the max peak pointer to traverse through the array
                        nTraverseIndex = cMaxPeakIndex;

                        //!Check for the 50% least value on the left side of the peak
                        for(iCount=0; iCount<80/*PROCESS_ARRAY_SIZE*/; iCount++)
                        {
                            //!Get the 50% of the max value in the peak
                            if(usnLeftWidthValue > usnTempValue)
                            {
                                usnLeftWidthValue = \
                                        usnAdcDataCapture[WBC_ARRAY_SIZE + \
                                                          nTraverseIndex];
                                nTraverseIndex--;
                                if(nTraverseIndex < 0)
                                {
                                    nTraverseIndex = PROCESS_ARRAY_SIZE-1;
                                }
                            }
                            else
                            {
                                //!If the 50% of the value is determined, store the index
                                cLeftMinPeakIndex = nTraverseIndex+1;
                                if(cLeftMinPeakIndex >= PROCESS_ARRAY_SIZE)
                                {
                                    cLeftMinPeakIndex = PROCESS_ARRAY_SIZE-1;
                                }
                                break;
                            }
                        }

                        //!Get the pulse width based on the indexes identified
                        if(cMaxPeakIndex > cLeftMinPeakIndex)
                        {
                            sBloodCellStatistics[WBC].m_PulseWidth = \
                                    ((cMaxPeakIndex - cLeftMinPeakIndex) << 1);
                        }
                        else
                        {
                            sBloodCellStatistics[WBC].m_PulseWidth = \
                                    (((PROCESS_ARRAY_SIZE - cLeftMinPeakIndex) + \
                                            cMaxPeakIndex) << 1);
                        }
                    }
                    else
                    {
                        //!If it is consecutive peak, then set the pulse with
                        //! as 40 which is valid
                        sBloodCellStatistics[WBC].m_PulseWidth = 40;
                    }

                    //!If it is consecutive peak, then don't check for pulse width,
                    //! else check for pulse width
                    if((true  == m_bWBCConsecutivePeak) || \
                        ((sBloodCellStatistics[WBC].m_PulseWidth > \
                          sBloodCellStatistics[WBC].m_pulseWidthMin) && \
                            (sBloodCellStatistics[WBC].m_PulseWidth < \
                                    sBloodCellStatistics[WBC].m_pulseWidthMax)))
                    {
                        //!If it is considered to be peak and not a notch,
                        //! then update the pulse height
                        if(false == m_bWBCNotPeak)
                        {
                            bool bPeakDetected = false;
                            switch(sBloodCellStatistics[WBC].m_PulseWidth)
                            {
                            case 8:
                            case 10:
                            case 12:
                            case 14:
                            case 16:
                            case 18:
                            case 20:
                                if(usnPeaktoPeakValue > 550)
                                {
                                    bPeakDetected = true;
                                }
                                break;

                            case 22:
                            case 24:
                            case 26:
                            case 28:
                            case 30:
                            case 32:
                            case 34:
                            case 36:
                            case 38:
                            case 40:
                            case 42:
                            case 44:
                            case 46:
                            case 48:
                            case 50:
                            case 52:
                            case 54:
                            case 56:
                            case 58:
                            case 60:
                                if(usnPeaktoPeakValue > 650)
                                {
                                    bPeakDetected = true;
                                }
                                break;

                            default:
                                break;
                            }
                            if(bPeakDetected)
                            {
                                //!Update the pulse height
                                DAStorePulseHeightinSDRAM(WBC,\
                                    sBloodCellStatistics[WBC].m_ulnCellCount,\
                                    usnPeaktoPeakValue);
                                //!Increment the no. of cells counted
                                sBloodCellStatistics[WBC].m_ulnCellCount++;
                                if(sBloodCellStatistics[WBC].m_ulnCellCount>1117000)
                                {
                                    i=1024;
                                    DmaAdcParams.WbcAddrCounter = DmaAdcParams.SampleAnalysisLength;
                                }
                            }
                        }
                    }
                }

                //!Reset all the variables to detect next peak
                sBloodCellStatistics[WBC].m_ucLookForMax = NO;
                sBloodCellStatistics[WBC].m_fMaxVolt = FLOAT_MIN;
                sBloodCellStatistics[WBC].m_fMinVolt = FLOAT_MAX;
                sBloodCellStatistics[WBC].m_PulseWidth = 0;
                m_bWBCNotPeak = false;
                if(false == m_bWBCConsecutivePeak)
                {
                    m_usnWBCConsecutivePeakCount = 0;
                }
                m_bWBCConsecutivePeak = false;

                //!Reset variables with respect to rejected peaks also
                usnWBCPrevLeftMinValue = usnLeftMinValue;
                if(usnWBCPrevLeftMinValue < WBC_MIN_VOLTAGE)
                {
                    usnWBCPrevLeftMinValue = WBC_MIN_VOLTAGE;
                }
                usnWBCPrevPeaktoPeak = usnPeaktoPeakValue;
                if(usnPeaktoPeakValue > sBloodCellStatistics[WBC].m_fCellLowVolt)
                {
                    usnWBCNextPeakInterval = 0;
                }
            }
        }
    }
    //else
    {
        //Do nothing
    }
    //!Update the present processed value to the previous value
    ulnWBCPrevVoltage = ulnVoltage;
    }
}
/******************************************************************************/
/*!
 * \fn void BDARBCPeakDetectionAlgorithm(unsigned int unVoltage)
 * \param unVoltage
 * \brief Peak detection algorithm
 * \brief  Description:
 *      This function takes ADC raw data and processed to detect peaks
 *      peaks shall be counter and stored in m_ulnCellCount variable
 * \brief Modified by Mohan: Added function call to store the pulse height
 *      of the cells
 * \return void
 */
/******************************************************************************/
void BDARBCPeakDetectionAlgorithm(unsigned int unVoltageIn)
{
    //!Var ulnVoltage is used for processing the algorithm
    unsigned long ulnVoltage;

    //!Var nTraverseIndex is used to traverse through the processing array
    int nTraverseIndex = 0;
    //!Get the present pointer in the processing array
    //nTraverseIndex = ucRBCProcessIndex;
    //!Var cMaxPeakIndex to point to the position of the Max peak position
    //!Var cLeftMinPeakIndex to point to the position of the left side lowest
    //! point in the peak
    int cMaxPeakIndex = 0, cLeftMinPeakIndex = 0;
    //!Var to store the Maximum peak value
    unsigned int usnMaxValue = 0;
    //!Var to store the minimum value of the left side of the peak
    unsigned int usnLeftMinValue = 0;
    //!Var to store the half of the peak width (Left side half)
    unsigned int usnLeftWidthValue = 0;

    //!Var to store the peak to peak value of the peak
    unsigned int usnPeaktoPeakValue = 0;

    int i;
    for(i=0; i<1024;i++)
    {

        i++;
        unVoltageIn = usnAdcDataCapture[i+1024];

        //!Get the present pointer in the processing array
        nTraverseIndex = ucRBCProcessIndex;

    //!Divide the input sample by 8 (Left shift by 3)
    //! Moving window average of 8 is considered for averaging
    //! Divide the sample and update to array
    unVoltageIn = unVoltageIn>>1;   //Change on 22/8/2019
    //!Remove the old divided sample from the sum of the array
    ulnSumRBCAvg1 = ulnSumRBCAvg1 - garr_usnMovingWindow1RBC[g_ucRBCAvgPtr1];
    //!Update the array with the latest divided sample
    garr_usnMovingWindow1RBC[g_ucRBCAvgPtr1] = unVoltageIn;
    //!Add the latest divided value to the sum
    ulnSumRBCAvg1 = ulnSumRBCAvg1+garr_usnMovingWindow1RBC[g_ucRBCAvgPtr1];
    //!Move the moving window average pointer to the next location
    g_ucRBCAvgPtr1++;
    //!Reset the moving window average pointer if it reaches maximum array size

    //if(g_ucRBCAvgPtr1>=MOVING_WINDOW)
    if(g_ucRBCAvgPtr1>= RBC_MOVING_WINDOW)   //Change on 22/8/2019
    {
        g_ucRBCAvgPtr1 = 0;
    }

    //!Update the averaged value to the processing variable
    ulnVoltage = ulnSumRBCAvg1;
    //!Update the processing value to the processing array
    usnAdcDataCapture[RBC_ARRAY_SIZE + ucRBCProcessIndex] = ulnVoltage;
    //!Increment the processing array pointer so that the next processed
    //! value shall be updated to the next location
    ucRBCProcessIndex++;
    //!Reset the processing array pointer if it reaches maximum processing array size
    if(ucRBCProcessIndex >= PROCESS_ARRAY_SIZE)
    {
        ucRBCProcessIndex = 0;
    }

    //!RBC next peak interval
    usnRBCNextPeakInterval++;

    //!Sample refers to the processed sample
    //!Process the data sample if it is above the minimum specified voltage
    //! - not used
    //if(ulnVoltage > unRBCMinVoltage)
    {
        //! Check for the maximum value in the sample by comparing with the previous sample
        if(ulnRBCPrevVoltage < ulnVoltage)
        {
            //!If the the signal is increasing slope update the max voltage and
            //! set the peak detection count to zero and set the look for max
            if(ulnVoltage > sBloodCellStatistics[RBC].m_fMaxVolt)
            {
                sBloodCellStatistics[RBC].m_fMaxVolt = ulnVoltage;
                sBloodCellStatistics[RBC].m_ucLookForMax = YES;
                usnRBCPeakDetectionCount = 0;
            }
        }
        else
        {
            //!If it is falling slope and if the peak is detected, then
            //! increment the peak detection count
            if(YES == sBloodCellStatistics[RBC].m_ucLookForMax)
            {
                usnRBCPeakDetectionCount++;
            }
        }

        //!If the signal is below the minimum voltage, then set the minimum voltage
        if(ulnVoltage < sBloodCellStatistics[RBC].m_fMinVolt)
        {
            sBloodCellStatistics[RBC].m_fMinVolt = ulnVoltage;
        }

        //!Peak is detected and next peak has not occurred

        if(usnRBCPeakDetectionCount>=1)//Changed from 4 to 2 (Hari-25-6-2019)
        {
            //!If the peak is detected
            if(sBloodCellStatistics[RBC].m_ucLookForMax == YES )
            {
                //!Var iCount is to process through the array
                int iCount;

                //!Point to the max peak location in the process array
                cMaxPeakIndex = nTraverseIndex - 1;//Changed from 4 to 2 (Hari-25-6-2019)
                //!If the index is negative, then make it positive since it is
                //! an circular buffer
                if(cMaxPeakIndex < 0)
                {
                    cMaxPeakIndex = PROCESS_ARRAY_SIZE + cMaxPeakIndex;
                }
                //!Get the max value from the processing array using max peak index
                usnMaxValue = usnAdcDataCapture[RBC_ARRAY_SIZE + \
                                                        cMaxPeakIndex];

                //!Get the left minimum value of the peak
                usnLeftMinValue = sBloodCellStatistics[RBC].m_fMinVolt;

                //!Check whether the peak detected is regular peak or consecutive peak
                //! If the next peak interval is less than the limit specified,
                //! then it is consecutive peak
                //! If it is consecutive peak, if the left minimum value is greater
                //! than the previous left minimum  +1000, then use the previous peak's
                //! left minimum value.
                //! If it is consecutive peak and if the notch is very small, then
                //! it is not valid peak.
                if(usnRBCNextPeakInterval <= RBC_NEXT_PEAK_INTERVAL)
                {
                    //if(usnLeftMinValue > (usnRBCPrevLeftMinValue + \
                    //        (usnRBCPrevPeaktoPeak%10)))
                    if(usnLeftMinValue > (usnRBCPrevLeftMinValue + \
                              (1000)))
                    {
                        if((usnMaxValue - usnLeftMinValue) < RBC_NOTCH_LIMIT)
                        {
                            //Donot capture it as peak
                            m_bRBCNotPeak = true;
                        }
                        else
                        {
                            m_bRBCConsecutivePeak = true;
                            m_usnRBCConsecutivePeakCount++;
                        }
                        if((m_usnRBCConsecutivePeakCount <= 5) && (false == m_bRBCNotPeak))
                        {
                            usnLeftMinValue = usnRBCPrevLeftMinValue;
                        }
                        else
                        {
                            m_bRBCConsecutivePeak = false;
                        }
                    }
                }

                //!Update peak to peak value
                usnPeaktoPeakValue = usnMaxValue - usnLeftMinValue;

                //!Check for Peak to peak, if peak to peak amplitude is
                //!valid, then process further.
                if((usnPeaktoPeakValue > sBloodCellStatistics[RBC].m_fCellLowVolt) \
                    && (usnPeaktoPeakValue < sBloodCellStatistics[RBC].m_fCellHighVolt))
                {
                    //!If it is not consecutive peak, then check for the pulse width.
                    //! If it is consecutive peak, pulse width cannot be calculated.
                    //! So the pulse is valid
                    if(false == m_bRBCConsecutivePeak)
                    {
                        m_usnRBCConsecutivePeakCount =0;
                        //!Get the 50% of the max value
                        unsigned int usnTempValue = usnMaxValue - \
                                (usnPeaktoPeakValue >> 1);

                        //!Check for the  50% least value on the left side of the peak
                        //!Assign Max value to the Minimum
                        usnLeftWidthValue = usnMaxValue;
                        //!Assign the max peak pointer to traverse through the array
                        nTraverseIndex = cMaxPeakIndex;

                        //!Check for the 50% least value on the left side of the peak
                        for(iCount=0; iCount<80/*PROCESS_ARRAY_SIZE*/; iCount++)
                        {
                            if(usnLeftWidthValue > usnTempValue)
                            {
                                usnLeftWidthValue = usnAdcDataCapture[RBC_ARRAY_SIZE + \
                                                                      nTraverseIndex];
                                nTraverseIndex--;
                                if(nTraverseIndex < 0)
                                {
                                    nTraverseIndex = PROCESS_ARRAY_SIZE-1;
                                }
                            }
                            else
                            {
                                //!If the 50% of the value is determined, store the index
                                cLeftMinPeakIndex = nTraverseIndex+1;
                                if(cLeftMinPeakIndex >= PROCESS_ARRAY_SIZE)
                                {
                                    cLeftMinPeakIndex = PROCESS_ARRAY_SIZE-1;
                                }
                                break;
                            }
                        }

                        //!Get the pulse width based on the indexes identified
                        if(cMaxPeakIndex > cLeftMinPeakIndex)
                        {

                            sBloodCellStatistics[RBC].m_PulseWidth = \
                                    ((cMaxPeakIndex - cLeftMinPeakIndex) << 1);
                        }
                        else
                        {
                            sBloodCellStatistics[RBC].m_PulseWidth = \
                                    (((PROCESS_ARRAY_SIZE - cLeftMinPeakIndex) + \
                                            cMaxPeakIndex) << 1);
                        }

                    }
                    else
                    {
                        //!If it is consecutive peak, then set the pulse with
                        //! as 40 which is valid
                        sBloodCellStatistics[RBC].m_PulseWidth = 40;
                    }

                    //!If it is consecutive peak, then don't check for pulse width,
                    //! else check for pulse width
                    if((true == m_bRBCConsecutivePeak)|| \
                        ((sBloodCellStatistics[RBC].m_PulseWidth > \
                           sBloodCellStatistics[RBC].m_pulseWidthMin) && \
                            (sBloodCellStatistics[RBC].m_PulseWidth < \
                                    sBloodCellStatistics[RBC].m_pulseWidthMax)))
                    {
                        //!If it is considered to be peak and not a notch,
                        //! then update the pulse height
                        if(false == m_bRBCNotPeak)
                        {
                            bool bPeakDetectedRBC=false;
                            {
                                if((usnPeaktoPeakValue > 5200) && (usnPeaktoPeakValue < 6300))
                                {
/*                                    if((sBloodCellStatistics[RBC].m_PulseWidth >= 14) && (sBloodCellStatistics[RBC].m_PulseWidth <= 16))
                                    {
                                        bPeakDetectedRBC = true;
                                    }*/
                                    if(sBloodCellStatistics[RBC].m_PulseWidth >= 12)
                                    {
                                        bPeakDetectedRBC = true;
                                    }
                                }
                                else if((usnPeaktoPeakValue >= 6300) && (usnPeaktoPeakValue < 7000))
                                {
/*                                    if((sBloodCellStatistics[RBC].m_PulseWidth >= 18) && (sBloodCellStatistics[RBC].m_PulseWidth <= 20))
                                    {
                                        bPeakDetectedRBC = true;
                                    }*/
                                    if(sBloodCellStatistics[RBC].m_PulseWidth >= 14)
                                    {
                                        bPeakDetectedRBC = true;
                                    }
                                }
                                else if((usnPeaktoPeakValue >= 7000) && (usnPeaktoPeakValue < 7900))
                                {
                                    if(sBloodCellStatistics[RBC].m_PulseWidth <= 12)
                                    {
                                        bPeakDetectedRBC = true;
                                    }
                                }
                                else if((usnPeaktoPeakValue >= 7900) && (usnPeaktoPeakValue < 10000))
                                {
                                    if(sBloodCellStatistics[RBC].m_PulseWidth > 16)
                                    {
                                        bPeakDetectedRBC = true;
                                    }
                                }
                                else
                                {
                                    bPeakDetectedRBC = true;
                                }


                                if(sBloodCellStatistics[RBC].m_PulseWidth > 22)
                                {
                                    bPeakDetectedRBC = false;
                                }
                            }

                            /*////////////////////////////////////////
                             * -----------------IMP----------------
                             * Counting All pulses(26/12/2019)
                             *
                             ////////////////////////////////////////*/
                            bPeakDetectedRBC = true;


                            //!Update the pulse height
                            if (bPeakDetectedRBC)
                            {
                                DAStorePulseHeightinSDRAM(RBC, \
                                        sBloodCellStatistics[RBC].m_ulnCellCount, \
                                        usnPeaktoPeakValue);
                                //!Increment the no. of cells counted
                                sBloodCellStatistics[RBC].m_ulnCellCount++;

                                if(sBloodCellStatistics[RBC].m_ulnCellCount>100000)
                                {
                                    i = 1024;
                                    DmaAdcParams.unRBCSampleAnalysis = SIX_SECOND_DATA;
                                }
                            }
                        }
                    }
                }

                //!Reset all the variables to detect next peak
                sBloodCellStatistics[RBC].m_ucLookForMax = NO;
                sBloodCellStatistics[RBC].m_fMaxVolt = FLOAT_MIN;
                sBloodCellStatistics[RBC].m_fMinVolt = FLOAT_MAX;
                sBloodCellStatistics[RBC].m_PulseWidth = 0;
                m_bRBCNotPeak = false;
                if(false == m_bRBCConsecutivePeak)
                {
                    m_usnRBCConsecutivePeakCount = 0;
                }
                m_bRBCConsecutivePeak = false;

                //!Reset variables with respect to rejected peaks also
                usnRBCPrevLeftMinValue = usnLeftMinValue;
                if(usnRBCPrevLeftMinValue < RBC_MIN_VOLTAGE)
                {
                    usnRBCPrevLeftMinValue = RBC_MIN_VOLTAGE;
                }
                usnRBCPrevPeaktoPeak = usnPeaktoPeakValue;
                if(usnPeaktoPeakValue > sBloodCellStatistics[RBC].m_fCellLowVolt)
                {
                    usnRBCNextPeakInterval = 0;
                }

            }
        }
    }
    //else
    {
        //Do nothing
    }
    //!Update the present processed value to the previous value
    ulnRBCPrevVoltage  = ulnVoltage;
    }
}
/******************************************************************************/
/*!
 * \fn void BDAPLTPeakDetectionAlgorithm(unsigned int unVoltage)
 * \param unVoltage
 * \brief Peak detection algorithm
 * \brief  Description:
 *      This function takes ADC raw data and processed to detect peaks
 *      peaks shall be counter and stored in m_ulnCellCount variable
 * \brief Modified by Mohan: Added function call to store the pulse height
 *      of the cells
 * \return void
 */
/******************************************************************************/
void BDAPLTPeakDetectionAlgorithm(unsigned int unVoltageIn)
{
    //!Var ulnVoltage is used for processing the algorithm
    unsigned long ulnVoltage;

    //!Var nTraverseIndex is used to traverse through the processing array
    int nTraverseIndex = 0;
    //!Get the present pointer in the processing array
    //nTraverseIndex = ucPLTProcessIndex;
    //!Var cMaxPeakIndex to point to the position of the Max peak position
    //!Var cLeftMinPeakIndex to point to the position of the left side lowest
    //! point in the peak
    int cMaxPeakIndex = 0, cLeftMinPeakIndex = 0;
    //!Var to store the Maximum peak value
    unsigned int usnMaxValue = 0;
    //!Var to store the minimum value of the left side of the peak
    unsigned int usnLeftMinValue = 0;
    //!Var to store the half of the peak width (Left side half)
    unsigned int usnLeftWidthValue = 0;

    //!Var to store the peak to peak value of the peak
    unsigned int usnPeaktoPeakValue = 0;

    int i;
    for(i=0; i<1024; i++)
    {
        unVoltageIn = usnAdcDataCapture[i+2048];

        //!Get the present pointer in the processing array
        nTraverseIndex = ucPLTProcessIndex;

    //!Divide the input sample by 8 (Left shift by 3)
    //! Moving window average of 8 is considered for averaging
    //! Divide the sample and update to array
    unVoltageIn = unVoltageIn>>3;
    //!Remove the old divided sample from the sum of the array
    ulnSumPLTAvg1 = ulnSumPLTAvg1 - garr_usnMovingWindow1PLT[g_ucPLTAvgPtr1];
    //!Update the array with the latest divided sample
    garr_usnMovingWindow1PLT[g_ucPLTAvgPtr1] = unVoltageIn;
    //!Add the latest divided value to the sum
    ulnSumPLTAvg1 = ulnSumPLTAvg1+garr_usnMovingWindow1PLT[g_ucPLTAvgPtr1];
    //!Move the moving window average pointer to the next location
    g_ucPLTAvgPtr1++;
    //!Reset the moving window average pointer if it reaches maximum array size
    if(g_ucPLTAvgPtr1>=MOVING_WINDOW)
    {
        g_ucPLTAvgPtr1 = 0;

    }

    //!Update the averaged value to the processing variable
    ulnVoltage = ulnSumPLTAvg1;
    //!Update the processing value to the processing array
    usnAdcDataCapture[PLT_ARRAY_SIZE + ucPLTProcessIndex] = ulnVoltage;
    //!Increment the processing array pointer so that the next processed
    //! value shall be updated to the next location
    ucPLTProcessIndex++;
    //!Reset the processing array pointer if it reaches maximum processing array size
    if(ucPLTProcessIndex >= PROCESS_ARRAY_SIZE)
    {
        ucPLTProcessIndex = 0;
    }

    //!WBC next peak interval
    usnPLTNextPeakInterval++;

    //!Sample refers to the processed sample
    //!Process the data sample if it is above the minimum specified voltage
    //!  - Presently not used
    //if(ulnVoltage > unPLTMinVoltage)
    {
        //! Check for the maximum value in the sample by comparing with the previous sample
        if(ulnPLTPrevVoltage < ulnVoltage)
        {
            //!If the the signal is increasing slope update the max voltage and
            //! set the peak detection count to zero and set the look for max
            if(ulnVoltage > sBloodCellStatistics[PLT].m_fMaxVolt)
            {
                sBloodCellStatistics[PLT].m_fMaxVolt = ulnVoltage;

                sBloodCellStatistics[PLT].m_ucLookForMax = YES;
                usnPLTPeakDetectionCount = 0;
            }
        }
        else
        {
            //!If it is falling slope and if the peak is detected, then
            //! increment the peak detection count
            if(YES == sBloodCellStatistics[PLT].m_ucLookForMax)
            {
                usnPLTPeakDetectionCount++;
            }
        }

        //!If the signal is below the minimum voltage, then set the minimum voltage
        if(ulnVoltage < sBloodCellStatistics[PLT].m_fMinVolt)
        {
            sBloodCellStatistics[PLT].m_fMinVolt = ulnVoltage;
        }

        //!Peak is detected and next peak has not occurred
        if(sBloodCellStatistics[PLT].m_ucLookForMax == YES )
        {
            //!If the peak is detected
            if(usnPLTPeakDetectionCount>=4)
            {
                //!Var iCount is to process through the array
                int iCount;

                //!Point to the max peak location in the process array
                cMaxPeakIndex = nTraverseIndex - 4;
                //!If the index is negative, then make it positive since it is
                //! an circular buffer
                if(cMaxPeakIndex < 0)
                {
                    cMaxPeakIndex = PROCESS_ARRAY_SIZE + cMaxPeakIndex;
                }
                //!Get the max value from the processing array using max peak index
                usnMaxValue = usnAdcDataCapture[PLT_ARRAY_SIZE + \
                                                        cMaxPeakIndex];

                //!Get the left minimum value of the peak
                usnLeftMinValue = sBloodCellStatistics[PLT].m_fMinVolt;

                //!Check whether the peak detected is regular peak or consecutive peak
                //! If the next peak interval is less than the limit specified,
                //! then it is consecutive peak
                //! If it is consecutive peak, if the left minimum value is greater
                //! than the previous left minimum  +1000, then use the previous peak's
                //! left minimum value.
                //! If it is consecutive peak and if the notch is very small, then
                //! it is not valid peak.
                if(usnPLTNextPeakInterval <= PLT_NEXT_PEAK_INTERVAL)
                {
                    if((usnLeftMinValue > PLT_MIN_VOLTAGE ) && (usnLeftMinValue > (usnPLTPrevLeftMinValue + 1000)))
                    {
                        //check if it is notch
                        if((usnMaxValue - usnLeftMinValue) < PLT_NOTCH_LIMIT)
                        {
                            //Donot capture it as peak
                            m_bPLTNotPeak = true;
                        }
                        else
                        {
                            m_bPLTConsecutivePeak = true;
                            m_usnPLTConsecutivePeakCount++;
                        }
                        if((m_usnPLTConsecutivePeakCount <= 3) && (false == m_bPLTNotPeak))
                        {
                            usnLeftMinValue = usnPLTPrevLeftMinValue;
                        }
                        else
                        {
                            m_bPLTConsecutivePeak = false;
                        }
                    }
                }

                //!Update peak to peak value
                usnPeaktoPeakValue = usnMaxValue - usnLeftMinValue;

                //!Check for Peak to peak, if peak to peak amplitude is
                //!valid, then process further.
                if((usnPeaktoPeakValue > sBloodCellStatistics[PLT].m_fCellLowVolt) \
                     && (usnPeaktoPeakValue < sBloodCellStatistics[PLT].m_fCellHighVolt))
                {
                    //!If it is not consecutive peak, then check for the pulse width.
                    //! If it is consecutive peak, pulse width cannot be calculated.
                    //! So the pulse is valid
                    if(false == m_bPLTConsecutivePeak)
                    {
                        m_usnPLTConsecutivePeakCount =0;
                        //!Get the 50% of the max value
                        unsigned int usnTempValue = usnMaxValue - \
                                (usnPeaktoPeakValue >> 1);

                        //!Check for the  50% least value on the left side of the peak
                        //!Assign Max value to the Minimum
                        usnLeftWidthValue = usnMaxValue;
                        //!Assign the max peak pointer to traverse through the array
                        nTraverseIndex = cMaxPeakIndex;

                        //!Check for the 50% least value on the left side of the peak
                        for(iCount=0; iCount<80/*PROCESS_ARRAY_SIZE*/; iCount++)
                        {
                            //!Get the 50% of the max value in the peak
                            if(usnLeftWidthValue > usnTempValue)
                            {
                                usnLeftWidthValue = usnAdcDataCapture[PLT_ARRAY_SIZE + \
                                                                      nTraverseIndex];
                                nTraverseIndex--;
                                if(nTraverseIndex < 0)
                                {
                                    nTraverseIndex = PROCESS_ARRAY_SIZE-1;
                                }
                            }
                            else
                            {
                                //!If the 50% of the value is determined, store the index
                                cLeftMinPeakIndex = nTraverseIndex+1;
                                if(cLeftMinPeakIndex >= PROCESS_ARRAY_SIZE)
                                {
                                    cLeftMinPeakIndex = PROCESS_ARRAY_SIZE-1;
                                }
                                break;
                            }

                        }

                        //!Get the pulse width based on the indexes identified
                        if(cMaxPeakIndex > cLeftMinPeakIndex)
                        {
                            sBloodCellStatistics[PLT].m_PulseWidth = \
                                    ((cMaxPeakIndex - cLeftMinPeakIndex) << 1);
                        }
                        else
                        {
                            sBloodCellStatistics[PLT].m_PulseWidth = \
                                    (((PROCESS_ARRAY_SIZE - cLeftMinPeakIndex) + \
                                            cMaxPeakIndex) << 1);
                        }
                    }
                    else
                    {
                        //!If it is consecutive peak, then set the pulse with
                        //! as 40 which is valid
                        sBloodCellStatistics[PLT].m_PulseWidth = 20;
                    }

                    //!If it is consecutive peak, then don't check for pulse width,
                    //! else check for pulse width
                    if((true == m_bPLTConsecutivePeak) || \
                         ((sBloodCellStatistics[PLT].m_PulseWidth >= \
                            sBloodCellStatistics[PLT].m_pulseWidthMin) && \
                            (sBloodCellStatistics[PLT].m_PulseWidth < \
                                    sBloodCellStatistics[PLT].m_pulseWidthMax)))
                    {
                        //!If it is considered to be peak and not a notch,
                        //! then update the pulse height
                        if(false == m_bPLTNotPeak)
                        {

                            bool bPeakDetected = false;
                            switch(sBloodCellStatistics[PLT].m_PulseWidth)
                            {
                             /* ADDED NEW ON 11 OCT 2019 BASED ON REGRESSION EQUATION CHANGE*/
                                case 8:
                                  if(usnPeaktoPeakValue > 350)
                                  {
                                      bPeakDetected = true;
                                  }
                                  break;
                              /* ADDED ABOVE TILL HERE*/

                                //case 8:


                                case 10:
                                case 12:
                                case 14:
                                case 16:
                                //case 18:
                                //case 20:
                                    //if(usnPeaktoPeakValue > 550)
                                    if(usnPeaktoPeakValue > 510)
                                    {
                                        bPeakDetected = true;
                                    }
                                    break;

                                //case 16:
                                case 18:
                                case 20:
                                case 22:
                                    //if(usnPeaktoPeakValue > 700)
                                    if(usnPeaktoPeakValue > 600)
                                    {
                                        bPeakDetected = true;
                                    }
                                    break;

                                case 24:
                                    //Added
                                    if(usnPeaktoPeakValue > 650)
                                    {
                                        bPeakDetected = true;
                                    }
                                    break;

                                case 26:
                                    //if(usnPeaktoPeakValue > 850)
                                    if(usnPeaktoPeakValue > 700)
                                    {
                                        bPeakDetected = true;
                                    }
                                    break;

                                case 28:
                                case 30:
                                case 32:
                                case 34:
                                    //if(usnPeaktoPeakValue > 900)
                                    //if(usnPeaktoPeakValue > 800)
                                    if(usnPeaktoPeakValue > 1800)
                                    {
                                        bPeakDetected = true;
                                    }
                                    break;

                                //case 30:
                                ///case 32:
                                //case 34:
                                    //if(usnPeaktoPeakValue > 950)
                                    //{
                                        //bPeakDetected = true;
                                    //}
                                    //break;

                                case 36:
                                case 38:
                                    if(usnPeaktoPeakValue > 2500)
                                    {
                                        bPeakDetected = true;
                                    }
                                    break;

                                case 40:
                                case 42:
                                    if(usnPeaktoPeakValue > 1100)
                                    {
                                        //bPeakDetected = true;
                                    }
                                    break;

                                case 44:
                                case 46:
                                case 48:
                                case 50:
                                case 52:
                                case 54:
                                    if(usnPeaktoPeakValue > 1150)
                                    {
                                        //bPeakDetected = true;
                                    }
                                    break;

                                default:
                                    break;
                            }
                            if(true == bPeakDetected)
                            {
                                //!Update the pulse height
                                DAStorePulseHeightinSDRAM(PLT,\
                                        sBloodCellStatistics[PLT].m_ulnCellCount,\
                                        usnPeaktoPeakValue);
                                //!Increment the no. of cells counted
                                sBloodCellStatistics[PLT].m_ulnCellCount++;

                                if(sBloodCellStatistics[PLT].m_ulnCellCount>20000)
                                {
                                    i = 1024;
                                    DmaAdcParams.unPLTSampleAnalysis = SIX_SECOND_DATA;
                                }
                            }
                        }
                    }
                }
                else
                {
                }

                //!Reset all the variables to detect next peak
                sBloodCellStatistics[PLT].m_ucLookForMax = NO;
                sBloodCellStatistics[PLT].m_fMaxVolt = FLOAT_MIN;
                sBloodCellStatistics[PLT].m_fMinVolt = FLOAT_MAX;
                sBloodCellStatistics[PLT].m_PulseWidth = 0;
                m_bPLTNotPeak = false;
                if(false == m_bPLTConsecutivePeak)
                {
                    m_usnPLTConsecutivePeakCount = 0;
                }
                m_bPLTConsecutivePeak = false;
                //

                //!Reset variables with respect to rejected peaks also
                usnPLTPrevLeftMinValue = usnLeftMinValue;
                usnPLTPrevPeaktoPeak = usnPeaktoPeakValue;
                if(usnPeaktoPeakValue > sBloodCellStatistics[PLT].m_fCellLowVolt)
                {
                    usnPLTNextPeakInterval = 0;
                }
            }
        }
    }
    //else
    {
        //Do nothing
    }
    //!Update the present processed value to the previous value
    ulnPLTPrevVoltage = ulnVoltage;
    }
}
/******************************************************************************/
/*!
 * \fn unsigned int BDAWBCDataAnalysis(Uint32 usnAddress, Uint32 usnLength)
 * \brief process stored ADC raw data
 * \brief  Description:
 *      This function shall process stored ADC raw data and calculate peaks
 *
 * \return
 */
/******************************************************************************/
unsigned int BDAWBCDataAnalysis(Uint32 usnAddress, Uint32 usnLength)
{
    Uint16 usnSdramData = 0;
    Uint32 i = 0;
    for (i = 0; i < usnLength; i++) {
        usnSdramData = CSSdramReadWORD(usnAddress);
        BDAWBCPeakDetectionAlgorithm(usnSdramData);
    }

    return 0;
}
/******************************************************************************/
/*!
 * \fn uint16_t BDAGetBloodCellStatisticsSize(void)
 *
 * \brief  Description:
 *      This function shall get the threshold of RBC, WBC and PLT cells used
 *      for detection of peaks
 * \return sBloodCellStatistics structure address
 */
/******************************************************************************/
uint16_t BDAGetBloodCellStatisticsSize(void)
{
    return sizeof(sBloodCellStatistics[FOUR]);
}

/******************************************************************************/
/*!
 * \fn stBloodCellVoltVal* BDAGetBloodCellStatistics(void)
 *
 * \brief  Description:
 *      This function shall get the threshold of RBC, WBC and PLT cells used
 *      for detection of peaks
 * \return sBloodCellStatistics structure address
 */
/******************************************************************************/
stBloodCellVoltVal* BDAGetBloodCellStatistics(void)
{
    return sBloodCellStatistics;
}

/******************************************************************************/
/*!
 * \fn stBloodCellVoltVal* BDAGetWBCBloodCellStatistics(void)
 *
 * \brief  Description:
 *      This function shall get the threshold of WBC cells used for detection
 *      of WBC peaks
 *
 * \return sBloodCellStatistics structure address
 */
/******************************************************************************/
stBloodCellVoltVal* BDAGetWBCBloodCellStatistics(void)
{
    return &sBloodCellStatistics[eCellWBC];
}
/******************************************************************************/
/*!
 * \fn stBloodCellVoltVal* BDAGetRBCBloodCellStatistics(void)
 *
 * \brief  Description:
 *      This function shall process stored ADC raw data and calculate peaks
 *
 * \return sBloodCellStatistics structure address
 */
/******************************************************************************/
stBloodCellVoltVal* BDAGetRBCBloodCellStatistics(void)
{
    return &sBloodCellStatistics[eCellRBC];
}

/******************************************************************************/
/*!
 * \fn stBloodCellVoltVal* BDAGetPLTBloodCellStatistics(void)
 *
 * \brief  Description:
 *      This function shall process stored ADC raw data and calculate peaks
 *
 * \return sBloodCellStatistics structure address
 */
/******************************************************************************/
stBloodCellVoltVal* BDAGetPLTBloodCellStatistics(void)
{
    return &sBloodCellStatistics[eCellPLT];
}

/******************************************************************************/
/*!
 * \fn     void BDAResetBloodCellStatistics(stBloodCellVoltVal* BloodCellStatistics)
 *
 * \brief  Description:
 *          This function resets the variables associated with peak detection
 * \return void
 */
/******************************************************************************/
void BDAResetBloodCellStatistics(stBloodCellVoltVal* BloodCellStatistics)
{
    BloodCellStatistics->m_Flag = 0;
    BloodCellStatistics->m_PulseWidth = 0;
    BloodCellStatistics->m_fMaxVolt = FLOAT_MIN;
    BloodCellStatistics->m_fMinVolt = FLOAT_MAX;
    BloodCellStatistics->m_ucLookForMax = YES;
    BloodCellStatistics->m_ulnCellCount = 0;
}
/******************************************************************************/
/*!
 * \fn void BDAAlgorithmVarReset(void)
 * \brief To reset all the variables related to the Algorithm
 *
 * \return void
 */
/******************************************************************************/
void BDAAlgorithmVarReset(void)
{
    unsigned char ucarr_Count = 0;

    for(ucarr_Count = 0; ucarr_Count<8; ucarr_Count++)
    {
        garr_usnMovingWindow1WBC[ucarr_Count] = 0;
        garr_usnMovingWindow1RBC[ucarr_Count] = 0;
        garr_usnMovingWindow1PLT[ucarr_Count] = 0;
    }


    g_ucWBCAvgPtr1 = 0;
    g_ucRBCAvgPtr1 = 0;
    g_ucPLTAvgPtr1= 0;

    ucWBCProcessIndex = 0;
    ucRBCProcessIndex = 0;
    ucPLTProcessIndex = 0;

    m_bRBCNotPeak = false;
    m_bRBCConsecutivePeak = false;
    unRBCMinVoltage = RBC_MIN_VOLTAGE;

    //unsigned long ulnSumRBCAvg1 = 0;
    //unsigned long ulnRBCPrevVoltage = 0;
    ulnSumRBCAvg1 = 0;
    ulnRBCPrevVoltage = 0;
    usnRBCPrevPeaktoPeak = 0;
    usnRBCPrevLeftMinValue = RBC_MIN_VOLTAGE;
    usnRBCNextPeakInterval = 0;

    m_bWBCNotPeak = false;
    m_bWBCConsecutivePeak = false;
    unWBCMinVoltage = WBC_MIN_VOLTAGE;

    usnWBCPrevPeaktoPeak = 0;
    usnWBCPrevLeftMinValue = WBC_MIN_VOLTAGE;
    //unsigned long ulnSumWBCAvg1 = 0;
    //unsigned long ulnWBCPrevVoltage = 0;
    ulnSumWBCAvg1 = 0;
    ulnWBCPrevVoltage = 0;
    usnWBCNextPeakInterval = 0;


    m_bPLTNotPeak = false;
    m_bPLTConsecutivePeak = false;
    unPLTMinVoltage = PLT_MIN_VOLTAGE;

    //unsigned long ulnSumPLTAvg1=0;
    //unsigned long ulnPLTPrevVoltage = 0;
    ulnSumPLTAvg1=0;
    ulnPLTPrevVoltage = 0;
    usnPLTPrevPeaktoPeak=0;
    usnPLTPrevLeftMinValue = PLT_MIN_VOLTAGE;
    usnPLTNextPeakInterval = 0;

    usnWBCPeakDetectionCount = 0;
    usnRBCPeakDetectionCount = 0;
    usnPLTPeakDetectionCount = 0;

}

unsigned long GetWBCPulse()
{
    return sBloodCellStatistics[WBC].m_ulnCellCount;
}
unsigned long GetRBCPulse()
{
    return sBloodCellStatistics[RBC].m_ulnCellCount;
}

unsigned long GetPLTPulse()
{
    return sBloodCellStatistics[PLT].m_ulnCellCount;
}
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

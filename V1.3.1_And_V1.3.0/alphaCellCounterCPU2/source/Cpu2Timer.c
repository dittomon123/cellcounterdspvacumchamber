/******************************************************************************/
/*! \file Cpu2Timer.c
 *
 *  \brief Timer initialization for CPU2
 *
 *  \b Description:
 *      Timer configuration and wrapper function for timer operations
 *
 *   $Version: $ 0.2
 *   $Date:    $ Jan-21-2015
 *   $Author:  $ 20040919, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_timer_module
//! @{
//******************************************************************************

#include <Cpu2Timer.h>

volatile uint16_t usnTimer2Flag;
extern Uint32 g_usnRBCPLTAcqtime;
extern Uint32 g_usnWBCAcqtime;
extern Uint32 g_usnWBCAcqStartTime;
extern Uint32 g_usnRBCPLTAcqStartTime;
/******************************************************************************/
/*!
 * \fn void Timer2_Interrupt(void)
 * \brief Timer initialization
 * \brief  Description:
 *      This function initialize timer
 *
 * \return void
 */
/******************************************************************************/
void Timer2_Interrupt(void)
{
	//! Interrupts that are used in this example are re-mapped to
	//! ISR functions found within this file.
	EALLOW;  //! This is needed to write to EALLOW protected registers

	PieVectTable.TIMER2_INT = &cpu_timer2_isr;
	EDIS;    //! This is needed to disable write to EALLOW protected registers

	//! Enable CPU int1 which is connected to CPU-Timer 0, CPU int13
	//! which is connected to CPU-Timer 1, and CPU int 14, which is connected
	//! to CPU-Timer 2:

	IER |= M_INT14;


	//! Enable global Interrupts and higher priority real-time debug events:
	EINT;  //! Enable Global interrupt INTM
	ERTM;  //! Enable Global realtime interrupt DBGM*/
}
/******************************************************************************/
/*!
 * \fn void Start_Timer2(void)
 * \brief timer start
 * \brief Description:
 *      This function starts CPU timers
 * \return void
 */
/******************************************************************************/
void Start_Timer2(void)
{
	CpuTimer2Regs.TCR.all = 0x4000; // Use write-only instruction to set TSS bit = 0
}
/******************************************************************************/
/*!
 * \fn void Configure_Timer2(void)
 * \brief timer configuration
 * \brief Description:
 *      This function configures CPU timers
 * \return void
 */
/******************************************************************************/
void Configure_Timer2(void)
{
	InitCpuTimers();
	Timer2_Interrupt();
	CpuTimer2.InterruptCount = 0;
	// Configure CPU-Timer 0, 1, and 2 to interrupt every second:
	// 200MHz CPU Freq, 1 second Period (in uSeconds)
	ConfigCpuTimer(&CpuTimer2, 200, 20000);
}
/******************************************************************************/
/*!
 * \fn void Stop_Timer2(void)
 * \brief timer stop
 * \brief Description:
 *      This function stops CPU timers
 * \return void
 */
/******************************************************************************/
void Stop_Timer2(void)
{
	CpuTimer2Regs.TCR.all = 0x0010;
}
/******************************************************************************/
/*!
 * \fn __interrupt void cpu_timer2_isr(void)
 * \brief CPU timer 2 interrupt handler
 * \brief  Description:
 *      CPU timer 2 interrupt handler configured in uSec
 *
 * \return void
 */
/******************************************************************************/
__interrupt void cpu_timer2_isr(void)
{
    CpuTimer2.InterruptCount++;
    usnTimer2Flag = 1;

    g_usnRBCPLTAcqtime++;

    g_usnWBCAcqtime++;

    g_usnWBCAcqStartTime++;

    g_usnRBCPLTAcqStartTime++;

   //! Acknowledge this interrupt to receive more interrupts from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

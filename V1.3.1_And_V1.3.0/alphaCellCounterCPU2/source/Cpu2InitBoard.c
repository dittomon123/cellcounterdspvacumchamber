/******************************************************************************/
/*! \file Cpu2InitBoard.c
 *
 *  \brief CPU2 initialization file.
 *
 *  \b Description:
 *      Initializaton of Vector table, Global Interrupts, System Clock,
 *      Timers & Watchdog configuration
 *
 *   $Version: $ 0.1
 *   $Date:    $ Nov-09-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_initialization_module
//! @{
//******************************************************************************
#include <Cpu2InitBoard.h>


#ifdef _FLASH
// These are defined by the linker (see device linker command file)
extern Uint16 RamfuncsLoadStart;
extern Uint16 RamfuncsLoadSize;
extern Uint16 RamfuncsRunStart;
#endif

/******************************************************************************/
/*!
 * \fn void IBInitialize_Board(void)
 * \brief CPU2 initialization
 * \brief  Description:
 *      This function initialize System Clock & Vector table
 *		Calls InitPieCtrl(), InitPieVectTable()
 * \return void
 */
/******************************************************************************/
void IBInitialize_Board(void)
{

	// Copy time critical code and Flash setup code to RAM
	// This includes InitFlash(), Flash API functions and any functions that are
	// assigned to ramfuncs section.
	// The  RamfuncsLoadStart, RamfuncsLoadEnd, and RamfuncsRunStart
	// symbols are created by the linker. Refer to the device .cmd file.
	#ifdef _FLASH
	  memcpy(&RamfuncsRunStart, &RamfuncsLoadStart, (size_t)&RamfuncsLoadSize);
	#endif

	// Step 1. Initialize System Control:
	// PLL, WatchDog, enable Peripheral Clocks
	// This example function is found in the F2837xD_SysCtrl.c file.
	    InitSysCtrl();

	// Call Flash Initialization to setup flash waitstates
	// This function must reside in RAM
	#ifdef _FLASH
	   InitFlash();
	#endif

	DINT;

	//!  Initialize the PIE control registers to their default state.
	//!  The default state is all PIE interrupts disabled and flags
	//!  are cleared.
	//!  This function is found in the F2837xD_PieCtrl.c file.
	InitPieCtrl();

	//! Disable CPU interrupts and clear all CPU interrupt flags:
	EALLOW;
	IER = 0x0000;
	IFR = 0x0000;
	EDIS;

	//! Initialize the PIE vector table with pointers to the shell Interrupt
	//! GService Routines (ISR).
	//! This will populate the entire table, even if the interrupt
	//! is not used in this example.  This is useful for debug purposes.
	//! The shell ISR routines are found in F2837xD_DefaultIsr.c.
	//! This function is found in F2837xD_PieVect.c.
	InitPieVectTable();

	EALLOW;
	EINT;
	EDIS;

}
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

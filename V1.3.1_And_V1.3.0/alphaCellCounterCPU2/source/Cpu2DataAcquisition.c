/******************************************************************************/
/*! \file Cpu2DataAcquisition.c
 *
 *  \brief Data Acquisition using DMA and storing to SDRAM
 *
 *  \b  Description:
 *      In this file DMA & SDRAM initialization.
 *      Aquired ADC channel data shall be stored to SDRAM through DMA engine
 *
 *
 *   $Version: $ 0.1
 *   $Date:    $ Aug-18-2015
 *   $Author:  $ GURUDUTT R, ADITYA B G, MOHAN S K
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_data_acquisition_module
//! @{
//******************************************************************************

#include <Cpu2AdcDriver.h>
#include <Cpu2ConfigSDRAM.h>
#include <Cpu2DataAcquisition.h>
#include "Cpu2IpcModule.h"
#include "Cpu2BloodDataAnalysis.h"
#include "Cpu2InitBoard.h"
#include "Cpu2SystemInitialize.h"
#include "fpu_vector.h"       // FPU headerfile to access memcpy_fast_far()



extern void BDAWBCPeakDetectionAlgorithm(unsigned int unVoltage);
extern void BDARBCPeakDetectionAlgorithm(unsigned int unVoltage);
extern void BDAPLTPeakDetectionAlgorithm(unsigned int unVoltage);

extern void SysIpcSendAnalysisInfo(Uint16 AnalysisCycle);
extern void SysResetCellDataAnalysisParams(Uint16 AnalysisCycle);

//1M Samples per second for 6.5 Seconds
#define WBC_DATA_ANALYSIS_LENGTH        6500000
//1M Samples per second for 8.5 Seconds
#define RBC_PLT_DATA_ANALYSIS_LENGTH    8500000

#define INITIAL_DATA_ACQ_NOISE_TIME     0    //6mSec
#define OVER_FLOW_AVOID                  7000

unsigned int g_usnCurrentModeCPU2 = 0;      //CRH_MODE
//uint16_t cguPulseData;		//RAJKUMARA
extern unsigned int g_usnAlgoExecutionTime;    //CRH_ALGO_TIME
Uint32 g_unWBCNoSamples = 0;    //CRH_CHANGE_20_09
Uint32 g_unRBCPLTNoSamples = 0; //CRH_CHANGE_20_09

extern XINT_FLAGS st_XintFlags; //PH_NOV_02
extern union UT_VOLUMETRIC_ERROR utVolumetricErrorInfo;
extern Uint32 g_usnWBCAnalysetime;
extern Uint32 g_usnRBCPLTAnalysetime;
/******************************************************************************/
/*!
 * \brief DmaAdcParameters DmaAdcParams
 * \brief g_ulSDRAMBufWBC[SDRAM_ADC_BUFFER_SIZE_WBC]
 * \brief g_ulSDRAMBufRBC[SDRAM_ADC_BUFFER_SIZE_RBC]
 * \brief g_ulSDRAMBufPLT[SDRAM_ADC_BUFFER_SIZE_PLT]
 * \brief g_ulSDRAMPulseHeightBufWBC[SDRAM_PULSE_HEIGHT_BUFFER_SIZE_WBC]
 * \brief g_ulSDRAMPulseHeightBufRBC[SDRAM_PULSE_HEIGHT_BUFFER_SIZE_WBC]
 * \brief g_ulSDRAMPulseHeightBufPLT[SDRAM_PULSE_HEIGHT_BUFFER_SIZE_WBC]
 *
 * \brief variable is used to store WBC, RBC, PLT & cells Pulse height -
 *          external SDRAM
 * 		  data samples
 */
/******************************************************************************/
__attribute__((far)) volatile Uint16 g_ulSDRAMBufWBC[SDRAM_ADC_BUFFER_SIZE_WBC]; //!Buffer in far memory

__attribute__((far)) volatile Uint16 g_ulSDRAMBufRBC[SDRAM_ADC_BUFFER_SIZE_RBC]; //!Buffer in far memory

__attribute__((far)) volatile Uint16 g_ulSDRAMBufPLT[SDRAM_ADC_BUFFER_SIZE_PLT]; //!Buffer in far memory

__attribute__((far)) volatile Uint16 g_ulSDRAMPulseHeightBufWBC[SDRAM_PULSE_HEIGHT_BUFFER_SIZE_WBC]; //!Buffer in far memory

__attribute__((far)) volatile Uint16 g_ulSDRAMPulseHeightBufRBC[SDRAM_PULSE_HEIGHT_BUFFER_SIZE_RBC]; //!Buffer in far memory

__attribute__((far)) volatile Uint16 g_ulSDRAMPulseHeightBufPLT[SDRAM_PULSE_HEIGHT_BUFFER_SIZE_PLT]; //!Buffer in far memory

//	#pragma DATA_SECTION(g_ulDstBuf1, "ramgs1");

//Uint16 g_ulSdramBuf[4];
extern Uint16 usnAdcDataCapture[4096];
DmaAdcParameters DmaAdcParams;
Uint16 guCycleFlag;

//memset();
/******************************************************************************/
/*!
 * \fn DmaAdcParameters* DAGetDmaAdcParams(void)
 *
 * \brief  Description: Returns the DMA parameters of the ADC
 *
 *
 * \return DmaAdcParams structure address
 */
/******************************************************************************/
DmaAdcParameters* DAGetDmaAdcParams(void)
{
	return  &DmaAdcParams;

}
/******************************************************************************/
/*!
 * \fn void DAConfigSDRAM_DMA(Uint16 MeasurementCycle)
 * \param MeasurementCycle	Specifies the First count cycle or Second count cycle
 * \brief Configuration function call for EMIF, DMA & SDRAM
 * \brief Description:
 *      This function configures and initiates EMIF , DMA & SDRAM registers
 *      	ConfigureEMIF();
 *			DAConfigureDMA();
 *			DAConfigureDMA_EMIF_WBC();
 *			DAConfigureDMA_EMIF_RBC();
 *			DAConfigureDMA_EMIF_PLT();
 *			configureSDRAMRegisters();
 * \return void
 */
/******************************************************************************/
void DAConfigSDRAM_DMA(Uint16 MeasurementCycle)
{
    //!Fn to initialize all the DMA parameters for All Measurement cycle
    DAInitDMA_Params(MeasurementCycle);
    //!Configure DMA for All the three channels to acquire data
    DAConfigureDMA();
    //!Configure DMA and EMIF for WBC
    DAConfigureDMA_EMIF_WBC();
    //!Configure DMA and EMIF for RBC
    DAConfigureDMA_EMIF_RBC();
    //!Configure DMA and EMIF for PLT
    DAConfigureDMA_EMIF_PLT();
}
/******************************************************************************/
/*!
 * \fn void void DAInitDMA_Params(Uint16 MeasurementCycle)
 * \param MeasurementCycle	Specifies the First count cycle or Second count cycle
 * \brief Initi DMA parameters
 * \brief Description:
 *      This function configures DMA register
 *
 * \return void
 */
/******************************************************************************/
void DAInitDMA_Params(Uint16 MeasurementCycle)
{
    //!Reset the DMA Counters for All the channels (WNC, RBC, PLT)
	DmaAdcParams.DMA_CH1Counter = 1;
	DmaAdcParams.DMA_CH2Counter = 1;
	DmaAdcParams.DMA_CH3Counter = 1;

	//!Get the memory address for WBC, RBC and PLT for all the cycles
	switch(MeasurementCycle)
	{
	case 1:
		DmaAdcParams.SdramMemAddr_WBC = ((Uint32)g_ulSDRAMBufWBC + \
		        FIRST_COUNT_OFFSET);
		DmaAdcParams.SdramMemAddr_RBC = ((Uint32)g_ulSDRAMBufRBC + \
		        FIRST_COUNT_OFFSET);
		DmaAdcParams.SdramMemAddr_PLT = ((Uint32)g_ulSDRAMBufPLT + \
		        FIRST_COUNT_OFFSET);
		break;
	case 2:
		DmaAdcParams.SdramMemAddr_WBC = ((Uint32)g_ulSDRAMBufWBC + \
		        SECOND_COUNT_OFFSET);
		DmaAdcParams.SdramMemAddr_RBC = ((Uint32)g_ulSDRAMBufRBC + \
		        SECOND_COUNT_OFFSET);
		DmaAdcParams.SdramMemAddr_PLT = ((Uint32)g_ulSDRAMBufPLT + \
		        SECOND_COUNT_OFFSET);
		break;
	default:
	    DmaAdcParams.SdramMemAddr_WBC = ((Uint32)g_ulSDRAMBufWBC + \
	            FIRST_COUNT_OFFSET);
        DmaAdcParams.SdramMemAddr_RBC = ((Uint32)g_ulSDRAMBufRBC + \
                FIRST_COUNT_OFFSET);
        DmaAdcParams.SdramMemAddr_PLT = ((Uint32)g_ulSDRAMBufPLT + \
                FIRST_COUNT_OFFSET);
		break;
	}
}
/******************************************************************************/
/*!
 * \fn void DAConfigureDMA(void)
 * \brief Configuration DMA channel
 * \brief Description:
 *      This function configures DMA register
 *
 * \return void
 */
/******************************************************************************/
void DAConfigureDMA(void)
{
    //!Configure the DMA channel
	EALLOW;
    CpuSysRegs.SECMSEL.bit.PF1SEL = 1;
    EDIS;
    DMAInitialize();
}
/******************************************************************************/
/*!
 * \fn void DAConfigureDMA_EMIF_WBC(void)
 * \brief Configuration DMA channel for ADC and SDRAM/RAM
 * \brief Description:
 *      This function configures DMA registers and configure ADC sample to SDRAM
 *  	For WBC
 * \return void
 */
/******************************************************************************/
void DAConfigureDMA_EMIF_WBC(void)
{
	volatile Uint16 *DMADest;
	volatile Uint16 *DMASource;

	//!Configure Channel 1 for WBC
	EALLOW;
	PieVectTable.DMA_CH1_INT = &local_DMACH1_ISR;
    IER |= M_INT7;
	EDIS;

	//!Update the results from ADC channel 0 to the DMA source address
    DMASource = &AdcaResultRegs.ADCRESULT0;
    //!Update the destination address of the SDRAM WBC address
    DMADest = (volatile Uint16 *)DmaAdcParams.SdramMemAddr_WBC; //!DMA destination;

    DMACH1AddrConfig(DMADest,DMASource);
    //! BURST size = 1 | Source step size = 0 | Dest step size += 1
    DMACH1BurstConfig(ONE_WORD_BURST,SOURCE_NO_ADDRESS_CHANGE,\
            DESTINATION_INCREMENT_ONE_ADDRESS);
    //! Transfer size = 0x1000 | Source step size = 0 | Dest step size += 1
    DMACH1TransferConfig((DMA_TRANSFER_SIZE_WBC-1),TRANSFER_STEP_SOURCE_NO_CHANGE,
                                TRANSFER_STEP_DEST_INCREMENT_ONE_ADDRESS);
    DMACH1WrapConfig(0,0,(DMA_TRANSFER_SIZE_WBC),(DMA_TRANSFER_SIZE_WBC-1));

    //!Modify the configuration to acquire data
    DMACH1ModeConfig(DMA_ADCAINT1,PERINT_ENABLE,ONESHOT_DISABLE,CONT_DISABLE,\
            SYNC_DISABLE,SYNC_SRC, OVRFLOW_DISABLE,SIXTEEN_BIT,CHINT_END,\
            CHINT_ENABLE);
    //!Start the DMA channel for acquisition
    StartDMACH1();
}
/******************************************************************************/
/*!
 * \fn void DAConfigureDMA_EMIF_RBC(void)
 * \brief Configuration DMA channel for ADC and SDRAM/RAM
 * \brief Description:
 *      This function configures DMA registers and configure ADC sample to SDRAM
 *		For RBC
 * \return void
 */
/******************************************************************************/
void DAConfigureDMA_EMIF_RBC(void)
{
	volatile Uint16 *DMADest;
	volatile Uint16 *DMASource;

	//!Configure Channel 1 for RBC
	EALLOW;
	PieVectTable.DMA_CH2_INT = &local_DMACH2_ISR;
    IER |= M_INT7;
	EDIS;

	//!Update the results from ADC channel 0 to the DMA source address
    DMASource = &AdcbResultRegs.ADCRESULT0;
    //!Update the destination address of the SDRAM RBC address
    DMADest = (volatile Uint16 *)DmaAdcParams.SdramMemAddr_RBC; //!DMA destination;

    DMACH2AddrConfig(DMADest,DMASource);
    //! BURST size = 1 | Source step size = 0 | Dest step size += 1
    DMACH2BurstConfig(ONE_WORD_BURST,SOURCE_NO_ADDRESS_CHANGE,\
            DESTINATION_INCREMENT_ONE_ADDRESS);
    //! Transfer size = 0x1000 | Source step size = 0 | Dest step size += 1
    DMACH2TransferConfig((DMA_TRANSFER_SIZE_RBC-1),TRANSFER_STEP_SOURCE_NO_CHANGE,
                                TRANSFER_STEP_DEST_INCREMENT_ONE_ADDRESS);
    DMACH2WrapConfig(0,0,(DMA_TRANSFER_SIZE_RBC),(DMA_TRANSFER_SIZE_RBC-1));

    //!Modify the configuration to acquire data
    DMACH2ModeConfig(DMA_ADCBINT1,PERINT_ENABLE,ONESHOT_DISABLE,CONT_DISABLE,\
            SYNC_DISABLE,SYNC_SRC, OVRFLOW_DISABLE,SIXTEEN_BIT,CHINT_END,\
            CHINT_ENABLE);
    //!Start the DMA channel for acquisition
    StartDMACH2();
}
/******************************************************************************/
/*!
 * \fn void DAConfigureDMA_EMIF_PLT(void)
 * \brief Configuration DMA channel for ADC and SDRAM/RAM
 * \brief Description:
 *      This function configures DMA registers and configure ADC sample to SDRAM
 *		For PLT
 * \return void
 */
/******************************************************************************/
void DAConfigureDMA_EMIF_PLT(void){

	volatile Uint16 *DMADest;
	volatile Uint16 *DMASource;

	//!Configure Channel 1 for PLT
	EALLOW;
	PieVectTable.DMA_CH3_INT = &local_DMACH3_ISR;
    IER |= M_INT7;
	EDIS;

	//!Update the results from ADC channel 0 to the DMA source address
    DMASource = &AdccResultRegs.ADCRESULT0;
    //!Update the destination address of the SDRAM PLT address
    DMADest = (volatile Uint16 *)DmaAdcParams.SdramMemAddr_PLT; //!DMA destination;

    DMACH3AddrConfig(DMADest,DMASource);
    //! BURST size = 1 | Source step size = 0 | Dest step size += 1
    DMACH3BurstConfig(ONE_WORD_BURST,SOURCE_NO_ADDRESS_CHANGE,\
            DESTINATION_INCREMENT_ONE_ADDRESS);
    //! Transfer size = 0x1000 | Source step size = 0 | Dest step size += 1
    DMACH3TransferConfig((DMA_TRANSFER_SIZE_PLT-1),TRANSFER_STEP_SOURCE_NO_CHANGE,
                                TRANSFER_STEP_DEST_INCREMENT_ONE_ADDRESS);
    DMACH3WrapConfig(0,0,DMA_TRANSFER_SIZE_PLT,(DMA_TRANSFER_SIZE_PLT-1));

    //!Modify the configuration to acquire data
    DMACH3ModeConfig(DMA_ADCCINT1,PERINT_ENABLE,ONESHOT_DISABLE,CONT_DISABLE,\
            SYNC_DISABLE,SYNC_SRC, OVRFLOW_DISABLE,SIXTEEN_BIT,CHINT_END,\
            CHINT_ENABLE);
    //!Start the DMA channel for acquisition
    StartDMACH3();
}
/******************************************************************************/
/*!
 * \fn void DADMA_DstDataClear(volatile Uint16 *Address, Uint32 Size)
 * \param *Address	Destination address to be passed
 * \param Size	Totao size/lenth of the memory to be initialized
 * \brief Initialize SDRAM/RAM memory location
 * \brief Description:
 *      This function initializes the memory location
 *		Function call:
 *		__addr32_write_uint32(((Uint32)Address, Data);
 * \return void
 */
/******************************************************************************/
void DADMA_DstDataClear(volatile Uint16 *Address, Uint32 Size)
{
	Uint32 i=0;

	//!Loop to write the given data size at particular address
    for(i = 0; i < (Size/2); i++)
    {
    	__addr32_write_uint32(((Uint32)Address + (i*2)), 0x00000000);
    }
}
/******************************************************************************/
/*!
 * \fn Uint32 DAGetDestAddress(Uint16 Type)
 * \param Type	memory allocated for Blood channel type
 * \brief returns the memory address of the variable
 * \brief Description:
 *      This function returns the memory address of the variable
 *		WBC/RBC/PLT buffer address will be returned
 *
 * \return Destination	address of destination
 */
/******************************************************************************/
Uint32 DAGetDestAddress(E_CELL_TYPE eTypeofCell, bool_t bAddofCellHtReq)
{
	Uint32 Destination=0x00000000;

	//!Based on the cell type selected, return the SDRAM address for the destination
	//! Return the address of pulse height or Raw data for each cell type selected
	switch(eTypeofCell)
	{
	case eCellRBC:
		if(bAddofCellHtReq == FALSE)
		{
			Destination = (Uint32)&g_ulSDRAMBufRBC[0];
		}
		else
		{
			Destination = (Uint32)&g_ulSDRAMPulseHeightBufRBC[0];
		}
		break;

	case eCellPLT:

		if(bAddofCellHtReq == FALSE)
		{
			Destination = (Uint32)&g_ulSDRAMBufPLT[0];
		}
		else
		{
			Destination = (Uint32)&g_ulSDRAMPulseHeightBufPLT[0];
		}
		break;

	case eCellWBC:
		if(bAddofCellHtReq == FALSE)
		{
			Destination = (Uint32)&g_ulSDRAMBufWBC[0];
		}
		else
		{
			Destination = (Uint32)&g_ulSDRAMPulseHeightBufWBC[0];
		}
		break;

	default:

		Destination = (Uint32)&g_ulSDRAMBufWBC[0];

		break;
	}

	return Destination;
}
/******************************************************************************/
/*!
 * \fn __interrupt void local_DMACH1_ISR(void)
 * \brief DMA channel 1 interrupt handler
 * \brief Description:
 *      This interrupt shall generate once the configured chunk of ADC data
 *      samples are captured and stored in SDRAM memory location
 *
 *
 * \return void
 */
/******************************************************************************/
__interrupt void local_DMACH1_ISR(void)
{
	EALLOW;
	//! Set up DESTINATION address:
	//! Point to beginning of destination buffer
	DmaRegs.CH1.DST_BEG_ADDR_SHADOW = (Uint32)(DmaAdcParams.SdramMemAddr_WBC + \
	        (DMA_TRANSFER_SIZE_WBC*DmaAdcParams.DMA_CH1Counter));
	DmaRegs.CH1.DST_ADDR_SHADOW =     (Uint32)(DmaAdcParams.SdramMemAddr_WBC + \
	        (DMA_TRANSFER_SIZE_WBC*DmaAdcParams.DMA_CH1Counter++));
	EDIS;

	//!158 = 5177343 samples
	//!If maximum allowable data storage is attained, stop the ADC acquisition
    //! else continue acquiring data and store in SDRAM
	if(DmaAdcParams.DMA_CH1Counter > TOTAL_NO_OF_DMA_CHUNKS_WBC)
	{
		EALLOW;
		PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
		DmaRegs.CH1.CONTROL.bit.TRANSFERSTS = 1;
		DmaRegs.CH1.CONTROL.bit.RUN = 0;
		EDIS;
		ADStopAdcDataAqWBC();
		CSSdramWriteWORD((DmaAdcParams.SdramMemAddr_WBC + \
		        (SDRAM_ADC_BUFFER_SIZE - 2)), 9999);
		CSSdramWriteWORD((DmaAdcParams.SdramMemAddr_WBC + \
		        (SDRAM_ADC_BUFFER_SIZE - 1)), 9999);
	}
	else
	{
		EALLOW;
		PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
		DmaRegs.CH1.CONTROL.bit.TRANSFERSTS = 1;
		DmaRegs.CH1.CONTROL.bit.RUN = 1;
		EDIS;
	}
}
/******************************************************************************/
/*!
 * \fn __interrupt void local_DMACH2_ISR(void)
 * \brief DMA channel 2 interrupt handler
 * \brief Description:
 *      This interrupt shall generate once the configured chunk of ADC data
 *      samples are captured and stored in SDRAM memory location
 * \return void
 */
/******************************************************************************/
__interrupt void local_DMACH2_ISR(void)
{

	EALLOW;
	//! Set up DESTINATION address:
	//! Point to beginning of destination buffer
	DmaRegs.CH2.DST_BEG_ADDR_SHADOW = (Uint32)(DmaAdcParams.SdramMemAddr_RBC + \
	        (DMA_TRANSFER_SIZE_RBC*DmaAdcParams.DMA_CH2Counter));
	DmaRegs.CH2.DST_ADDR_SHADOW =     (Uint32)(DmaAdcParams.SdramMemAddr_RBC + \
	        (DMA_TRANSFER_SIZE_RBC*DmaAdcParams.DMA_CH2Counter++));
	EDIS;

	//!158 = 5177343 samples
	//! If maximum allowable data storage is attained, stop the ADC acquisition
	//! else continue acquiring data and store in SDRAM
	if(DmaAdcParams.DMA_CH2Counter > TOTAL_NO_OF_DMA_CHUNKS_RBC)
	{
		EALLOW;
		PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
		DmaRegs.CH2.CONTROL.bit.TRANSFERSTS = 1;
		DmaRegs.CH2.CONTROL.bit.RUN = 0;
		EDIS;
		ADStopAdcDataAqRBC();
		CSSdramWriteWORD((DmaAdcParams.SdramMemAddr_RBC + \
		        (SDRAM_ADC_BUFFER_SIZE - 2)), 9999);
		CSSdramWriteWORD((DmaAdcParams.SdramMemAddr_RBC + \
		        (SDRAM_ADC_BUFFER_SIZE - 1)), 9999);
	}
	else
	{
		EALLOW;
		PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
		DmaRegs.CH2.CONTROL.bit.TRANSFERSTS = 1;
		DmaRegs.CH2.CONTROL.bit.RUN = 1;
		EDIS;
	}
}
/******************************************************************************/
/*!
 * \fn __interrupt void local_DMACH3_ISR(void)
 * \brief DMA channel 3 interrupt handler
 * \brief Description:
 *      This interrupt shall generate once the configured chunk of ADC data
 *      samples are captured and stored in SDRAM memory location
 *
 *
 * \return void
 */
/******************************************************************************/
__interrupt void local_DMACH3_ISR(void)
{
	EALLOW;
	//! Set up DESTINATION address:
	//! Point to beginning of destination buffer
	DmaRegs.CH3.DST_BEG_ADDR_SHADOW = (Uint32)(DmaAdcParams.SdramMemAddr_PLT + \
	        (DMA_TRANSFER_SIZE_PLT*DmaAdcParams.DMA_CH3Counter));
	DmaRegs.CH3.DST_ADDR_SHADOW =     (Uint32)(DmaAdcParams.SdramMemAddr_PLT + \
	        (DMA_TRANSFER_SIZE_PLT*DmaAdcParams.DMA_CH3Counter++));
	EDIS;

	//!158 = 5177343 samples
	//!If maximum allowable data storage is attained, stop the ADC acquisition
    //! else continue acquiring data and store in SDRAM
	if(DmaAdcParams.DMA_CH3Counter > TOTAL_NO_OF_DMA_CHUNKS_PLT)
	{
		EALLOW;
		PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
		DmaRegs.CH3.CONTROL.bit.TRANSFERSTS = 1;
		DmaRegs.CH3.CONTROL.bit.RUN = 0;
		EDIS;
		ADStopAdcDataAqPLT();
		CSSdramWriteWORD((DmaAdcParams.SdramMemAddr_PLT + \
		        (SDRAM_ADC_BUFFER_SIZE - 2)), 9999);
		CSSdramWriteWORD((DmaAdcParams.SdramMemAddr_PLT + \
		        (SDRAM_ADC_BUFFER_SIZE - 1)), 9999);
	}
	else
	{
		EALLOW;
		PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
		DmaRegs.CH3.CONTROL.bit.TRANSFERSTS = 1;
		DmaRegs.CH3.CONTROL.bit.RUN = 1;
		EDIS;
	}
}
/******************************************************************************/
/*!
 * \fn __interrupt void local_DMACH4_ISR(void)
 * \brief DMA channel 4 interrupt handler
 * \brief Description:
 *      This interrupt shall generate once the configured chunk of ADC data
 *      samples are captured and stored in SDRAM memory location
 *
 *
 * \return void
 */
/******************************************************************************/
__interrupt void local_DMACH4_ISR(void)
{
    //!Handle the DMA channel 4 interrupt
		EALLOW;
		PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
		DmaRegs.CH4.CONTROL.bit.TRANSFERSTS = 1;
		DmaRegs.CH4.CONTROL.bit.RUN = 1;
		EDIS;
}
/******************************************************************************/
/*!
 * \fn void DAMovingAvg(Uint16 fVoltage, Uint16 eType, Uint16 uReset)
 * \param fVoltage : ADC raw data
 * \param eType : Cell type
 * \param uReset : To reset
 * \brief process stored ADC raw data
 * \brief  Description:
 *      This function shall remove any glithces and smoothen the signal and pulses
 *      - Presently not used
 * \return
 */
/******************************************************************************/
Uint16 DAMovingAvg(Uint16 fVoltage, E_CELL_TYPE eTypeofCell, Uint16 uReset)
{
	#define AVERAGE_SAMPLES	5
    //Will hold 10 consecutive samples
    static Uint16 fVoltageBuf[FOUR][AVERAGE_SAMPLES];
    static unsigned long i[FOUR] = {0,0,0};
    Uint32 fVoltagetemp = 0;
    Uint16 j = 0;

    if(uReset == YES)
    {
    	i[eCellWBC] = 0;
    	i[eCellRBC] = 0;
    	i[eCellPLT] = 0;

    	for(j=0; j < AVERAGE_SAMPLES; j++)
		{
			fVoltageBuf[eCellWBC][j] = 0;
		}

    	for(j=0; j < AVERAGE_SAMPLES; j++)
		{
			fVoltageBuf[eCellRBC][j] = 0;
		}

    	for(j=0; j < AVERAGE_SAMPLES; j++)
		{
			fVoltageBuf[eCellPLT][j] = 0;
		}
    	return 0;
    }

    if(i[eTypeofCell] < AVERAGE_SAMPLES)
    {
        fVoltageBuf[eTypeofCell][i[eTypeofCell]++] = fVoltage;
    }
    else
    {
        fVoltageBuf[eTypeofCell][i[eTypeofCell]++ & AVERAGE_SAMPLES] = fVoltage;
    }

    for(j=0; j < AVERAGE_SAMPLES; j++)
    {
        fVoltagetemp = fVoltagetemp + fVoltageBuf[eTypeofCell][j];
    }
    fVoltagetemp = fVoltagetemp/AVERAGE_SAMPLES;

    return (fVoltagetemp);
}
/******************************************************************************/
/*!
 * \fn void DACellDataSampleAnalysis(void)
 * \brief process stored ADC raw data
 * \brief  Description:
 *      This function shall process stored ADC raw data and calculate peaks
 *      - Presently not used
 * \return
 */
/******************************************************************************/
void DACellDataSampleAnalysis(void)
{
	static int i=0;
	//Uint16 uTempData = 0;

	//!If the no. of samples to be analyzed is with in the total captured data,
	//! then process the algorithm
	//! else stop the analysis and send the results to CPU1
	if(DmaAdcParams.WbcAddrCounter >= DmaAdcParams.SampleAnalysisLength)
    {
	    //!If the current state is to analyze the data, then process the data
        if(DmaAdcParams.SampleAnalysisState == FIRST_COUNT_DATA_ANALYSIS_STATE)
        {
            g_usnAlgoExecutionTime = CpuTimer2.InterruptCount - \
                    g_usnAlgoExecutionTime;

            SysIpcSendAnalysisInfo(FIRST_COUNT_CYCLE);
            DmaAdcParams.SampleAnalysisState = DATA_ANALYSIS_COMPLETE_STATE;
        }
    }
    else
    {
        //Process only WBC Algorithm - START
        if(DmaAdcParams.WbcAddrCounter <= g_unWBCNoSamples)
        {
            memcpy_fast_far(&usnAdcDataCapture[0],\
                    (volatile const void *)DmaAdcParams.WbcAddrCounter, 1024);
        }
        for(i=0; i < 1024; i++)
        {
            if(DmaAdcParams.WbcAddrCounter <= g_unWBCNoSamples)
            {
                BDAWBCPeakDetectionAlgorithm(usnAdcDataCapture[i]);
            }
        }
        DmaAdcParams.WbcAddrCounter = DmaAdcParams.WbcAddrCounter+1024;
        //Process only WBC Algorithm - END


        //Process only RBC & PLT Algorithm - START
        if(DmaAdcParams.PltAddrCounter <= g_unRBCPLTNoSamples)
        {
            memcpy_fast_far(&usnAdcDataCapture[1024],\
                    (volatile const void *)DmaAdcParams.RbcAddrCounter,1024);
            memcpy_fast_far(&usnAdcDataCapture[2048],\
                    (volatile const void *)DmaAdcParams.PltAddrCounter,1024);
        }
        for(i=0; i < 1024; i++)
        {
            if((WHOLE_BLOOD_MODE == g_usnCurrentModeCPU2) || \
                    (PRE_DILUENT_MODE == g_usnCurrentModeCPU2)|| \
                    (START_PRE_DILUENT_COUNTING_CMD ==g_usnCurrentModeCPU2))
            {
                if(DmaAdcParams.PltAddrCounter <= g_unRBCPLTNoSamples)
                {
                    BDARBCPeakDetectionAlgorithm(usnAdcDataCapture[i+1024]);
                    BDAPLTPeakDetectionAlgorithm(usnAdcDataCapture[i+2048]);
                }
            }

        }
        DmaAdcParams.RbcAddrCounter = DmaAdcParams.RbcAddrCounter+1024;
        DmaAdcParams.PltAddrCounter = DmaAdcParams.PltAddrCounter+1024;
        //Process only RBC & PLT Algorithm - END
    }
}
/******************************************************************************/
/*!
* \fn 	  void DAStorePulseHeightinSDRAM(Uint16 uiCellType, Uint32 ulCellCnt,
*               Uint16 ulPulseHeight)
* \param  uiCellType
* \param  ulCellCnt
* \param  ulPulseHeight
* \brief  Author- Mohan
* \brief  Description:Function to store pulse height of the cells in SDRAM
*
* \return void
*/
/******************************************************************************/
 void DAStorePulseHeightinSDRAM(Uint16 uiCellType, Uint32 ulCellCnt, \
         Uint16 ulPulseHeight)
 {
     //!Get the WBC pulse height destination address in SDRAM
	 uint32_t usnAddrWbcHeight = DAGetDestAddress(eCellWBC, TRUE);
	 //!Get the RBC pulse height destination address in SDRAM
	 uint32_t usnAddrRbcHeight = DAGetDestAddress(eCellRBC, TRUE);
	 //!Get the PLT pulse height destination address in SDRAM
	 uint32_t usnAddrPltHeight = DAGetDestAddress(eCellPLT, TRUE);

	 if(ulCellCnt < PULSE_HEIGHT_SAMPLES) // Store up to 100000 samples atmost
	 {
		 if(guCycleFlag == FIRST_COUNT_CYCLE)
		 {
			 switch(uiCellType)
			 {
			 case WBC:// WBC sample
			     CSSdramWriteWORD((usnAddrWbcHeight + ulCellCnt),\
				         (Uint16)(ulPulseHeight));
				 break;
			 case RBC: //RBC sample
			     CSSdramWriteWORD((usnAddrRbcHeight + ulCellCnt),\
				         (Uint16)(ulPulseHeight));
				 break;
			 case PLT: //PLT sample
			     CSSdramWriteWORD((usnAddrPltHeight + ulCellCnt),\
				         (Uint16)(ulPulseHeight));
				 break;
			 default:
				 break;
			 }
		 }
		 else
		 {
			 switch(uiCellType)
			 {
			 case WBC:// WBC sample
			     CSSdramWriteWORD((usnAddrWbcHeight + \
				         PULSE_HEIGHT_SAMPLES + ulCellCnt),\
				         (Uint16)(ulPulseHeight));
				 break;
			 case RBC: //RBC sample
			     CSSdramWriteWORD((usnAddrRbcHeight + \
				         PULSE_HEIGHT_SAMPLES + ulCellCnt), \
				         (Uint16)(ulPulseHeight));
				 break;
			 case PLT: //PLT sample
			     CSSdramWriteWORD((usnAddrPltHeight + \
				         PULSE_HEIGHT_SAMPLES + ulCellCnt), \
				         (Uint16)(ulPulseHeight));
				 break;
			 default:
				 break;
			 }
		 }
	 }
 }
 /*****************************************************************************/
 /*!
  * \fn void DAWBCCellDataSampleAnalysis(void)
  * \brief process to analyse and store WBC ADC raw data
  * \brief  Description:
  *      This function is for processing WBC signal based on the current sequence
  * \return
  */
 /*****************************************************************************/
void DAWBCCellDataSampleAnalysis(void)
{
    static int i=0;

    //!If the no. of samples to be analyzed is with in the total captured data,
    //! then process the algorithm
    //! else stop the analysis and send the results to CPU1
    //if(DmaAdcParams.WbcAddrCounter >= DmaAdcParams.SampleAnalysisLength)
    //if(DmaAdcParams.unWBCSampleAnalysis >= SEVEN_SECOND_DATA)
    if(DmaAdcParams.unWBCSampleAnalysis >= (g_usnWBCAnalysetime * 20 * 1000))
    {
        //!If the current state is in data analysis, then stop the analysis and
        //! and send the result to CPU1
        if(DmaAdcParams.SampleAnalysisState == FIRST_COUNT_DATA_ANALYSIS_STATE)
        {
            //!Sample analysis end flag is set to True to indicate that Sample
            //! analysis is finished
            //! Sample analysis start is reset to False
            st_XintFlags.m_bWBC_SampleAnalysisFlagEnd = TRUE;
            st_XintFlags.m_bWBC_SampleAnalysisFlagStart = FALSE;

            DmaAdcParams.unWBCSampleAnalysis = 0;
            g_usnWBCAnalysetime = 0;

            //!Ensure sample analysis end flag is set during volumetric error
            //! condition based on the current mode
            switch (g_usnCurrentModeCPU2)
            {
                case WHOLE_BLOOD_MODE:
                case PRE_DILUENT_MODE:
                case BODY_FLUID_MODE:
                case START_PRE_DILUENT_COUNTING_CMD:
                case START_UP_SEQUENCE_CMD:
                case AUTO_CALIBRATION_WB:
                case QUALITY_CONTROL_WB:
                case QUALITY_CONTROL_BODY_FLUID:
                case PARTIAL_BLOOD_COUNT_RBC_PLT:
                case COMMERCIAL_CALIBRATION_WBC:
                case PARTIAL_BLOOD_COUNT_WBC:
                case STARTUP_FROM_SERVICE_SCREEN_CMD:
                case STARTUP_FAILED_CMD:
                    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted)
                    {
                        st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd = TRUE;
                        st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = FALSE;
                    }
                    break;

                default:
                    break;
            }
        }
    }
    else
    {
        //!Process only WBC Algorithm
        {
            //!Check for the current mode and process only for WBC pulse detection
            switch(g_usnCurrentModeCPU2)
            {
                case WHOLE_BLOOD_MODE:
                case PRE_DILUENT_MODE:
                case BODY_FLUID_MODE:
                case START_PRE_DILUENT_COUNTING_CMD:
                case START_UP_SEQUENCE_CMD:
                case AUTO_CALIBRATION_WB:
                case QUALITY_CONTROL_WB:
                case QUALITY_CONTROL_BODY_FLUID:
                case COMMERCIAL_CALIBRATION_WBC:
                case PARTIAL_BLOOD_COUNT_WBC:
                case STARTUP_FROM_SERVICE_SCREEN_CMD:
                case STARTUP_FAILED_CMD:
                    //!Algorithm is processed from first second data to 5th second data
                    //if((DmaAdcParams.WbcAddrCounter >
                    //    (DmaAdcParams.unWBCSampleAnalysisLength - NINE_SECOND_DATA))
                    //    &&(DmaAdcParams.WbcAddrCounter < (DmaAdcParams.unWBCSampleAnalysisLength
                    //            - FIVE_SECOND_DATA)))
                    {
                        //!Copy 1024 samples from SDRAM to another location to process
                        //! Algorithm is processed for 1024 data
                        memcpy_fast_far(&usnAdcDataCapture[0], \
                             (volatile const void *)DmaAdcParams.WbcAddrCounter, 1024);

                        //!Run peak detection algorithm for WBC raw data
                        //for(i=0; i < 1024; i++)
                        //{
                            BDAWBCPeakDetectionAlgorithm(usnAdcDataCapture[i]);
                            DmaAdcParams.unWBCSampleAnalysis = DmaAdcParams.unWBCSampleAnalysis + 1024;
                        //}
                    }
                    break;
                default:
                    break;
                }
        }
        //!Increment the WBC address counter
        DmaAdcParams.WbcAddrCounter = DmaAdcParams.WbcAddrCounter+1024;

        //if((DmaAdcParams.WbcAddrCounter%1048576)<1024)
        //{
            //SendWBCOneSecondData();
        //}
    }
}
/******************************************************************************/
 /*!
  * \fn void DARBCPLTCellDataSampleAnalysis(void)
  * \brief process to analyse and stores RBC & PLT ADC raw data
  * \brief  Description:
  *  This function is for processing RBC and PLT signal based on the
 *  current sequence
  * \return
  */
/******************************************************************************/
void DARBCPLTCellDataSampleAnalysis(void)
{
    static int i=0;

    //!If the no. of samples to be analyzed is with in the total captured data,
    //! then process the algorithm
    //! else stop the analysis and send the results to CPU1
    /*if((DmaAdcParams.RbcAddrCounter >= DmaAdcParams.unRBC_PLTSampleAnalysisLength) && \
                (DmaAdcParams.PltAddrCounter >= DmaAdcParams.unRBC_PLTSampleAnalysisLength))*/
    //if((DmaAdcParams.unRBCSampleAnalysis >= SIX_SECOND_DATA) && \
            (DmaAdcParams.unPLTSampleAnalysis >= SIX_SECOND_DATA))
    if((DmaAdcParams.unRBCSampleAnalysis >= (g_usnRBCPLTAnalysetime * 20 * 1000)) && \
                (DmaAdcParams.unPLTSampleAnalysis >= (g_usnRBCPLTAnalysetime * 20 * 1000)))
    {
        //!If the current state is in data analysis, then stop the analysis and
        //! and send the result to CPU1
        if(DmaAdcParams.SampleAnalysisState == FIRST_COUNT_DATA_ANALYSIS_STATE)
        {
            //!Sample analysis end flag is set to True to indicate that Sample
            //! analysis is finished
            //! Sample analysis start is reset to False
            st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagEnd = TRUE;
            st_XintFlags.m_bRBC_PLT_SampleAnalysisFlagStart = FALSE;

            DmaAdcParams.unRBCSampleAnalysis = 0;
            DmaAdcParams.unPLTSampleAnalysis = 0;
            g_usnRBCPLTAnalysetime = 0;

            //!Ensure sample analysis end flag is set during volumetric error
            //! condition based on the current mode
            switch (g_usnCurrentModeCPU2)
            {
                case WHOLE_BLOOD_MODE:
                case PRE_DILUENT_MODE:
                case START_PRE_DILUENT_COUNTING_CMD:
                case START_UP_SEQUENCE_CMD:
                case AUTO_CALIBRATION_WB:
                case QUALITY_CONTROL_WB:
                case QUALITY_CONTROL_BODY_FLUID:
                case PARTIAL_BLOOD_COUNT_RBC_PLT:
                case COMMERCIAL_CALIBRATION_RBC:
                case COMMERCIAL_CALIBRATION_PLT:
                case STARTUP_FROM_SERVICE_SCREEN_CMD:
                case STARTUP_FAILED_CMD:
                    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted)
                    {
                        st_XintFlags.m_bWBC_SampleAnalysisFlagEnd = TRUE;
                        st_XintFlags.m_bWBC_SampleAnalysisFlagStart = FALSE;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    else
    {
        //!Process RBC and PLT Algorithm
        {
            //!Check for the current mode and process only for WBC pulse detection
            switch (g_usnCurrentModeCPU2)
            {
                case WHOLE_BLOOD_MODE:
                case PRE_DILUENT_MODE:
                case START_PRE_DILUENT_COUNTING_CMD:
                case START_UP_SEQUENCE_CMD:
                case AUTO_CALIBRATION_WB:
                case QUALITY_CONTROL_WB:
                case QUALITY_CONTROL_BODY_FLUID:
                case PARTIAL_BLOOD_COUNT_RBC_PLT:
                case STARTUP_FROM_SERVICE_SCREEN_CMD:
                case STARTUP_FAILED_CMD:
                    //!Algorithm is processed from first second data to 5th second data
                    /*if((DmaAdcParams.PltAddrCounter > (DmaAdcParams.unRBC_PLTSampleAnalysisLength \
                            - TEN_SECOND_DATA))&&(DmaAdcParams.PltAddrCounter < \
                                    (DmaAdcParams.unRBC_PLTSampleAnalysisLength - FIVE_SECOND_DATA)))*/
                    //if(DmaAdcParams.unRBCSampleAnalysis < SIX_SECOND_DATA)
                    if(DmaAdcParams.unRBCSampleAnalysis < (g_usnRBCPLTAnalysetime * 20 * 1000))
                    // if(DmaAdcParams.RbcAddrCounter < DmaAdcParams.unRBC_PLTSampleAnalysisLength)
                    {
                        //!Copy 1024 samples from SDRAM to another location to process
                        //! Algorithm is processed for 1024 data
                        memcpy_fast_far(&usnAdcDataCapture[1024], \
                              (volatile const void *)DmaAdcParams.RbcAddrCounter,1024);
                        //!Run peak detection algorithm for RBC and PLT raw data
                        //for(i=0; i < 1024; i++)
                        //{
                            BDARBCPeakDetectionAlgorithm(usnAdcDataCapture[i+1024]);
                        //}
                        DmaAdcParams.unRBCSampleAnalysis = DmaAdcParams.unRBCSampleAnalysis + 1024;
                    }

                    //!Algorithm is processed from first second data to 5th second data
                    //if((DmaAdcParams.PltAddrCounter > (DmaAdcParams.unRBC_PLTSampleAnalysisLength \
                            - TEN_SECOND_DATA))&&(DmaAdcParams.PltAddrCounter < \
                                    (DmaAdcParams.unRBC_PLTSampleAnalysisLength - FOUR_SECOND_DATA)))
                    //if(DmaAdcParams.unPLTSampleAnalysis < SIX_SECOND_DATA)
                    if(DmaAdcParams.unPLTSampleAnalysis < (g_usnRBCPLTAnalysetime * 20 * 1000))
                  //  if(DmaAdcParams.PltAddrCounter < DmaAdcParams.unRBC_PLTSampleAnalysisLength)
                    {
                        //!Copy 1024 samples from SDRAM to another location to process
                        //! Algorithm is processed for 1024 data
                        memcpy_fast_far(&usnAdcDataCapture[2048], \
                              (volatile const void *)DmaAdcParams.PltAddrCounter,1024);
                        //!Run peak detection algorithm for RBC and PLT raw data
                        //for(i=0; i < 1024; i++)
                        //{
                            BDAPLTPeakDetectionAlgorithm(usnAdcDataCapture[i+2048]);
                        //}
                        DmaAdcParams.unPLTSampleAnalysis = DmaAdcParams.unPLTSampleAnalysis + 1024;
                    }
                    break;

                case COMMERCIAL_CALIBRATION_RBC:
                    //!Algorithm is processed from first second data to 5th second data
                    //if((DmaAdcParams.PltAddrCounter > (DmaAdcParams.unRBC_PLTSampleAnalysisLength \
                        - NINE_SECOND_DATA))&&(DmaAdcParams.PltAddrCounter < \
                                (DmaAdcParams.unRBC_PLTSampleAnalysisLength - FIVE_SECOND_DATA)))
                    //if(DmaAdcParams.unRBCSampleAnalysis < SIX_SECOND_DATA)
                    if(DmaAdcParams.unRBCSampleAnalysis < (g_usnRBCPLTAnalysetime * 20 * 1000))
                    {
                        //!Copy 1024 samples from SDRAM to another location to process
                        //! Algorithm is processed for 1024 data
                        memcpy_fast_far(&usnAdcDataCapture[1024], \
                        (volatile const void *)DmaAdcParams.RbcAddrCounter,1024);
                        //!Run peak detection algorithm for RBC raw data
                        //for(i=0; i < 1024; i++)
                        {
                            BDARBCPeakDetectionAlgorithm(usnAdcDataCapture[i+1024]);
                        }
                        DmaAdcParams.unRBCSampleAnalysis = DmaAdcParams.unRBCSampleAnalysis + 1024;
                    }
                    else
                    {
                        //DmaAdcParams.RbcAddrCounter = DmaAdcParams.SampleAnalysisLength;
                        DmaAdcParams.unPLTSampleAnalysis = (g_usnRBCPLTAnalysetime * 20 * 1000);
                    }
                    break;

                case COMMERCIAL_CALIBRATION_PLT:
                    //!Algorithm is processed from first second data to 5th second data
                    //if((DmaAdcParams.PltAddrCounter > (DmaAdcParams.unRBC_PLTSampleAnalysisLength \
                            - NINE_SECOND_DATA))&&(DmaAdcParams.PltAddrCounter < \
                                    (DmaAdcParams.unRBC_PLTSampleAnalysisLength - FOUR_SECOND_DATA)))
                    //if(DmaAdcParams.unPLTSampleAnalysis < SIX_SECOND_DATA)
                    if(DmaAdcParams.unPLTSampleAnalysis < (g_usnRBCPLTAnalysetime * 20 * 1000))
                    {
                        //!Copy 1024 samples from SDRAM to another location to process
                        //! Algorithm is processed for 1024 data
                        memcpy_fast_far(&usnAdcDataCapture[2048], \
                        (volatile const void *)DmaAdcParams.PltAddrCounter,1024);
                        //!Run peak detection algorithm for PLT raw data
                        //for(i=0; i < 1024; i++)
                        {
                            BDAPLTPeakDetectionAlgorithm(usnAdcDataCapture[i+2048]);
                        }
                        DmaAdcParams.unPLTSampleAnalysis = DmaAdcParams.unPLTSampleAnalysis + 1024;
                     }
                    else
                    {
                        //DmaAdcParams.PltAddrCounter = DmaAdcParams.SampleAnalysisLength;
                        DmaAdcParams.unRBCSampleAnalysis = (g_usnRBCPLTAnalysetime * 20 * 1000);
                    }
                     break;

                default:
                      break;
            }
        }
        //!Increment the RBC and PLT address counter
        DmaAdcParams.RbcAddrCounter = DmaAdcParams.RbcAddrCounter+1024;
        DmaAdcParams.PltAddrCounter = DmaAdcParams.PltAddrCounter+1024;

        //if((DmaAdcParams.PltAddrCounter%1048576) <1024)
        //{
            //Send RBC and PLT Data
            //SendRBCPLTOneSecondData();
        //}
    }
}
/******************************************************************************/
/*!
 * \fn void ClearSDRAM(void)
 * \brief to clear data in SDRAM
 * \return void
 */
/******************************************************************************/
void ClearSDRAM(void)
{
    //!Clear the SDRAM data before starting any sequence or if any volumetric
    //! error is observed
    for(;DmaAdcParams.WbcAddrCounter < DmaAdcParams.SampleAnalysisLength;)
    {
        CSSdramWriteWORD(DmaAdcParams.WbcAddrCounter, 0);
        CSSdramWriteWORD(DmaAdcParams.RbcAddrCounter, 0);
        CSSdramWriteWORD(DmaAdcParams.PltAddrCounter, 0);
        DmaAdcParams.WbcAddrCounter++;
        DmaAdcParams.RbcAddrCounter++;
        DmaAdcParams.PltAddrCounter++;
    }
}
 //*****************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

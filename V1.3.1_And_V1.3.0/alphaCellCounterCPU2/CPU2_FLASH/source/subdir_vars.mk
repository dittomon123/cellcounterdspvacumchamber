################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/Cpu2AdcDriver.c \
../source/Cpu2BloodDataAnalysis.c \
../source/Cpu2ConfigSDRAM.c \
../source/Cpu2DataAcquisition.c \
../source/Cpu2InitBoard.c \
../source/Cpu2IpcModule.c \
../source/Cpu2SystemInitialize.c \
../source/Cpu2Timer.c \
../source/Cpu2main.c 

C_DEPS += \
./source/Cpu2AdcDriver.d \
./source/Cpu2BloodDataAnalysis.d \
./source/Cpu2ConfigSDRAM.d \
./source/Cpu2DataAcquisition.d \
./source/Cpu2InitBoard.d \
./source/Cpu2IpcModule.d \
./source/Cpu2SystemInitialize.d \
./source/Cpu2Timer.d \
./source/Cpu2main.d 

OBJS += \
./source/Cpu2AdcDriver.obj \
./source/Cpu2BloodDataAnalysis.obj \
./source/Cpu2ConfigSDRAM.obj \
./source/Cpu2DataAcquisition.obj \
./source/Cpu2InitBoard.obj \
./source/Cpu2IpcModule.obj \
./source/Cpu2SystemInitialize.obj \
./source/Cpu2Timer.obj \
./source/Cpu2main.obj 

OBJS__QUOTED += \
"source\Cpu2AdcDriver.obj" \
"source\Cpu2BloodDataAnalysis.obj" \
"source\Cpu2ConfigSDRAM.obj" \
"source\Cpu2DataAcquisition.obj" \
"source\Cpu2InitBoard.obj" \
"source\Cpu2IpcModule.obj" \
"source\Cpu2SystemInitialize.obj" \
"source\Cpu2Timer.obj" \
"source\Cpu2main.obj" 

C_DEPS__QUOTED += \
"source\Cpu2AdcDriver.d" \
"source\Cpu2BloodDataAnalysis.d" \
"source\Cpu2ConfigSDRAM.d" \
"source\Cpu2DataAcquisition.d" \
"source\Cpu2InitBoard.d" \
"source\Cpu2IpcModule.d" \
"source\Cpu2SystemInitialize.d" \
"source\Cpu2Timer.d" \
"source\Cpu2main.d" 

C_SRCS__QUOTED += \
"../source/Cpu2AdcDriver.c" \
"../source/Cpu2BloodDataAnalysis.c" \
"../source/Cpu2ConfigSDRAM.c" \
"../source/Cpu2DataAcquisition.c" \
"../source/Cpu2InitBoard.c" \
"../source/Cpu2IpcModule.c" \
"../source/Cpu2SystemInitialize.c" \
"../source/Cpu2Timer.c" \
"../source/Cpu2main.c" 



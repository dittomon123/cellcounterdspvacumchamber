################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
device/%.obj: ../device/%.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"D:/Softwares/ti-cgt-c2000_16.9.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 --include_path="D:/Softwares/ti-cgt-c2000_16.9.1.LTS/include" --include_path="C:/ti/controlSUITE/device_support/F2837xD/v190/F2837xD_common/utils" --include_path="D:/Cell Counter/V1.3.1_And_V1.3.0/alphaCellCounterCPU2/source" --include_path="D:/Cell Counter/V1.3.1_And_V1.3.0/alphaCellCounterCPU2/include" --include_path="D:/Cell Counter/V1.3.1_And_V1.3.0/alphaCellCounterCPU2/device" --include_path="C:/ti/controlSUITE/device_support/F2837xD/v190/F2837xD_headers/include" --include_path="C:/ti/controlSUITE/device_support/F2837xD/v190/F2837xD_common/include" --include_path="C:/ti/controlSUITE/device_support/F2837xD/v190/F2837xD_common" --advice:performance=all --define=CPU2 --define=_FLASH -g --printf_support=full --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --preproc_with_compile --preproc_dependency="device/$(basename $(<F)).d_raw" --obj_directory="device" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

device/%.obj: ../device/%.asm $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"D:/Softwares/ti-cgt-c2000_16.9.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 --include_path="D:/Softwares/ti-cgt-c2000_16.9.1.LTS/include" --include_path="C:/ti/controlSUITE/device_support/F2837xD/v190/F2837xD_common/utils" --include_path="D:/Cell Counter/V1.3.1_And_V1.3.0/alphaCellCounterCPU2/source" --include_path="D:/Cell Counter/V1.3.1_And_V1.3.0/alphaCellCounterCPU2/include" --include_path="D:/Cell Counter/V1.3.1_And_V1.3.0/alphaCellCounterCPU2/device" --include_path="C:/ti/controlSUITE/device_support/F2837xD/v190/F2837xD_headers/include" --include_path="C:/ti/controlSUITE/device_support/F2837xD/v190/F2837xD_common/include" --include_path="C:/ti/controlSUITE/device_support/F2837xD/v190/F2837xD_common" --advice:performance=all --define=CPU2 --define=_FLASH -g --printf_support=full --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --preproc_with_compile --preproc_dependency="device/$(basename $(<F)).d_raw" --obj_directory="device" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '



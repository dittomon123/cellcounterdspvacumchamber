/*
 * Cpu2Structure.h
 *
 *  Created on: 18-Aug-2016
 *      Author: 20033200
 */

#ifndef INCLUDE_CPU2STRUCTURE_H_
#define INCLUDE_CPU2STRUCTURE_H_


/******************************************************************************/
/*!
 * \brief structure for ADC & DMA parameters
 * \struct DmaAdcParams
 */
/******************************************************************************/
typedef struct DMA_ADC_PARAMETER {
	Uint32 DMA_CH1Counter;
	Uint32 DMA_CH2Counter;
	Uint32 DMA_CH3Counter;
	Uint32 SdramMemAddr_WBC;
	Uint32 SdramMemAddr_RBC;
	Uint32 SdramMemAddr_PLT;
	Uint32 SdramMemLength_AdcBuf;
    Uint16 SampleAnalysisState;
    Uint32 WbcAddrCounter;
    Uint32 RbcAddrCounter;
    Uint32 PltAddrCounter;
    Uint32 SampleAnalysisLength;
    //PH_NOV_02 - START
    Uint32 unWBCSampleAnalysisLength;
    Uint32 unRBC_PLTSampleAnalysisLength;
    Uint32 unRBCPLTAcqtime;
    Uint32 unWBCAcqtime;
    Uint32 unWBCSampleAnalysis;
    Uint32 unRBCSampleAnalysis;
    Uint32 unPLTSampleAnalysis;
    //PH_NOV_02 - END

}DmaAdcParameters;

/******************************************************************************/
/*!
 * \brief structure for ADC parameters for ADC api
 * \typedef DataAqParam
 * \struct StructDataAqParam
 *!  ADCSampleCounter member of StructDataAqParam structure used to count the
 *!  ADC samples
 *!  AquisitionStatus member of StructDataAqParam structure used to update
 *!  the status of the ADC aquisition
 */
/******************************************************************************/
typedef struct DATA_AQUISITION_PARAMETER {
	unsigned long ADCSampleCounter;
    volatile unsigned int AcquisitionStatusFlag;
    Uint16 CountingCycleFlag;
}DataAqParam;

/******************************************************************************/
 /*!
  *
  * \struct stBloodCellVoltVal
  * \brief structure for Blood Data analysis
  * m_fMinVolt - minimum ADC value
  * m_fMaxVolt - maximum ADC value
  * m_fCellLowVolt - Peak detection low threshold
  * m_fCellHighVolt - Peak detection high threshold
  * m_PulseWidth - pulse width in terms of time
  * m_deltaMinMax - peak detection threshold
  * m_pulseWidthMin - pulse width minimum threshold
  * m_pulseWidthMax - pulse width maximum threshold
  */
 /*****************************************************************************/
typedef struct
{
    unsigned int m_fMinVolt;
    unsigned int m_fMaxVolt;
    //unsigned int m_fCellLowVolt;
    unsigned long m_fCellLowVolt;
    //unsigned int m_fCellHighVolt;
    unsigned long m_fCellHighVolt;
    unsigned int m_fApertureLength;
    unsigned int m_fApertureDia;
    unsigned int m_fApertureCorrectedLen;
    unsigned int  m_ucLookForMax;
    unsigned long  m_ulnCellCount;
    unsigned int m_PulseWidth;
    unsigned int m_Flag;
    unsigned int m_deltaMinMax;
    unsigned int m_pulseWidthMin;
    unsigned int m_pulseWidthMax;

	/*
    unsigned long m_fMinVolt;
    unsigned long m_fMaxVolt;
    unsigned long m_fCellLowVolt;
    unsigned long m_fCellHighVolt;
    unsigned long m_fApertureLength;
    unsigned long m_fApertureDia;
    unsigned long m_fApertureCorrectedLen;
    unsigned long  m_ucLookForMax;
    unsigned long  m_ulnCellCount;
    unsigned long m_PulseWidth;
    unsigned int m_Flag;
    unsigned long m_deltaMinMax;
    unsigned long m_pulseWidthMin;
    unsigned long m_pulseWidthMax;
    */

}stBloodCellVoltVal;

/******************************************************************************/
/*!
 * \brief structure for Ipc parameters
 * \struct IpcParams
 */
/******************************************************************************/
typedef struct IPC_PARAMETER {
	uint32_t unCpu1ToCpu2RxBuf[10];
	uint32_t unCpu2ToCpu1TxBuf[10];
	Uint16 RemoteResponseFlag;
	Uint16 LocalReceiveFlag;
	Uint16 ErrFlag;
	Uint16 INT1Flag;
}IpcParameters;



typedef struct
{
    uint32_t	m_SampleStartFlag:1;            //!To initiate the interrupt monitoring for acquisition
    uint32_t	m_SampleEndFlag:1;              //!Presently not used
    uint32_t	m_Xint_RBCPLTStartFlag:1;       //!RBC & PLT start LED interrupt
    uint32_t	m_Xint_WBCStartFlag:1;          //!WBC start LED interrupt
    uint32_t	m_Xint_RBCPLTStopFlag:1;        //!RBC & PLT stop the LED interrupt
    uint32_t	m_Xint_WBCStopFlag:1;           //!WBC stop LET interrupt
    uint32_t	m_RBCPLT_XintFlag:1;            //!RBC & PLT flag after the required acquisition is completed
    uint32_t	m_WBC_XintFlag:1;               //!WBC flag after the required acquisition is completed

	//PH_NOV_02 - START
    uint32_t  m_bWBC_SampleAnalysisFlagStart:1;
    uint32_t  m_bRBC_PLT_SampleAnalysisFlagStart:1;
    uint32_t  m_bWBC_SampleAnalysisFlagEnd:1;
    uint32_t  m_bRBC_PLT_SampleAnalysisFlagEnd:1;
    uint32_t  m_bWBC_StartInterrupt:1;          //!WBC Start interrupt has occured
    uint32_t  m_bRBC_PLT_StartInterrupt:1;
    uint32_t  m_bWBC_BubbleDetected:1;
    uint32_t  m_bRBC_PLT_BubbleDetected:1;
    uint32_t  m_bWBC_Acq_Start_Time_Flag:1;
    uint32_t  m_bRBC_PLT_Acq_Start_Time_Flag:1;
	//PH_NOV_02 - END
}XINT_FLAGS;

typedef union UT_VOLUMETRIC_ERROR
{
    struct ST_VOLUMETRIC_ERROR
    {
        uint32_t unWBCCountingNotStarted:1;
        uint32_t unWBCCountingStartedImproper:1;
        uint32_t unRBCCountingNotStarted:1;
        uint32_t unRBCCountingStartedImproper:1;
        uint32_t unWBCCountingNotCompleted:1;
        uint32_t unWBCCountingCompletedImproper:1;
        uint32_t unRBCCountingNotCompleted:1;
        uint32_t unRBCCountingCompletedImproper:1;
//DS_TESTING
        uint32_t unWBCImproperAbortion:1;
        uint32_t unRBCImproperAbortion:1;
        uint32_t unWBCFlowCalibAbortion:1;
        uint32_t unRBCFlowCalibAbortion:1;
//DS_TESTING
        uint32_t usnReserved:24;
    }stVolumetricError;
    uint32_t unVolumetricInfo;
}utVolumetricInfo;

#endif /* INCLUDE_CPU2STRUCTURE_H_ */

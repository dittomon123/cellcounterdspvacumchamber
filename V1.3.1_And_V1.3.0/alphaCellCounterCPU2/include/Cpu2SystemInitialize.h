/******************************************************************************/
/*! \file Cpu2SystemInitialize.h
 *
 *  \brief Header file for Cpu2SystemInitialize.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Apr-21-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_system_module
//! @{
//******************************************************************************
#ifndef SYSTEM_INITIALIZE_H_
#define SYSTEM_INITIALIZE_H_

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "Cpu2AdcDriver.h"
#include "Cpu2DataAcquisition.h"
#include "Cpu2BloodDataAnalysis.h"
#include "Cpu2InitBoard.h"
#include "Cpu2ConfigSDRAM.h"
#include "Cpu2IpcModule.h"
#include "Cpu2Timer.h"
#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File
#include "Cpu2Enums.h"
#include "Cpu2Structure.h"
#include "Cpu2Defines.h"

/*!Prototype of the function which need to be used for the defining the states
 * in each sequence for the volumetric interrupt, signal processing sending results to CPU1*/

/******************************************************************************/
/*!
 *  \fn     void SysSystemInitialize()
 *  \brief  Not used
 *  \param  This function initialize CPU peripherals and application modules
 *  \return  void
 */
/******************************************************************************/
void SysSystemInitialize(void);

/******************************************************************************/
/*!
 *  \fn     void SysIpcCommModule()
 *  \brief  This function process the tx & rx of ipc communication data
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void SysIpcCommModule(void);

/******************************************************************************/
/*!
 *  \fn     void SysHandleVolumetricTubeStatus()
 *  \brief  This function process the volumetric interrupts and the errors handling
 *      during acquisition
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void SysHandleVolumetricTubeStatus(void);

/******************************************************************************/
/*!
 *  \fn     void xint1_isr()
 *  \brief  interrupt handler of volumetric RBC_PLT Start interrupt
 *  \param  void
 *  \return  interrupt void
 */
/******************************************************************************/
interrupt void xint1_isr(void);

/******************************************************************************/
/*!
 *  \fn     void xint2_isr()
 *  \brief  interrupt handler of volumetric RBC_PLT stop interrupt
 *  \param  void
 *  \return  interrupt void
 */
/******************************************************************************/
interrupt void xint2_isr(void);

/******************************************************************************/
/*!
 *  \fn     void xint3_isr()
 *  \brief  interrupt handler of volumetric WBC start interrupt
 *  \param  void
 *  \return  interrupt void
 */
/******************************************************************************/
interrupt void xint3_isr(void);

/******************************************************************************/
/*!
 *  \fn     void xint4_isr()
 *  \brief  interrupt handler of volumetric WBC stop interrupt
 *  \param  void
 *  \return  interrupt void
 */
/******************************************************************************/
interrupt void xint4_isr(void);

#ifdef _ALPHA_2_BOARD_
/******************************************************************************/
/*!
 *  \fn     void SysInitVolumetricInterrupt()
 *  \brief  This function is for external interrupt configuration
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void SysInitVolumetricInterrupt(void);
#endif

/******************************************************************************/
/*!
 *  \fn     void SysSetWbcBloodCellInfoDefault()
 *  \brief  To update default values for blood cell info structure
 *  \param  uint32_t addrBloodCellInfoDefault
 *  \return  void
 */
/******************************************************************************/
void SysSetWbcBloodCellInfoDefault(uint32_t addrBloodCellInfoDefault);

/******************************************************************************/
/*!
 *  \fn     void SysSetRbcBloodCellInfoDefault()
 *  \brief  To update default values for blood cell info structure
 *  \param  uint32_t addrBloodCellInfoDefault
 *  \return  void
 */
/******************************************************************************/
void SysSetRbcBloodCellInfoDefault(uint32_t addrBloodCellInfoDefault);

/******************************************************************************/
/*!
 *  \fn     void SysSetPltBloodCellInfoDefault()
 *  \brief  To update default values for blood cell info structure
 *  \param  uint32_t addrBloodCellInfoDefault
 *  \return  void
 */
/******************************************************************************/
void SysSetPltBloodCellInfoDefault(uint32_t addrBloodCellInfoDefault);

/******************************************************************************/
/*!
 *  \fn     void SysResetCellDataAnalysisParams()
 *  \brief  To reset the blood cell variables for acquisition
 *  \param  Uint16 AnalysisCycle
 *  \return  void
 */
/******************************************************************************/
void SysResetCellDataAnalysisParams(Uint16 AnalysisCycle);

/******************************************************************************/
/*!
 *  \fn     void SysCellDataAnalysis()
 *  \brief  Not used
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void SysCellDataAnalysis(void);

/******************************************************************************/
/*!
 *  \fn     void SysIpcSendAnalysisInfo()
 *  \brief  This function sends counts to cpu1 after analysis
 *  \param  Uint16 AnalysisCycle
 *  \return  void
 */
/******************************************************************************/
void SysIpcSendAnalysisInfo(Uint16 AnalysisCycle);

void SendWBCOneSecondData(void);
void SendRBCPLTOneSecondData(void);

extern void ClearSDRAM(void);
extern void BDAAlgorithmVarReset(void);

#endif /* SYSTEM_INITIALIZE_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

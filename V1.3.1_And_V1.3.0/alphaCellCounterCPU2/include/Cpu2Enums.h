/*
 * Enums.h
 *
 *  Created on: 18-Aug-2016
 *      Author: 20033200
 */

#ifndef INCLUDE_CPU2ENUMS_H_
#define INCLUDE_CPU2ENUMS_H_

#include "Cpu2Defines.h"

/*! Cell type for the counting */
/******************************************************************************/
/*!
 * \brief union for Blood parameters
 * \enum eCellType
 */
/******************************************************************************/
typedef enum eCellType
{
	ecellInvalid = 0,
    eCellRBC ,
    eCellPLT,
    eCellWBC
}E_CELL_TYPE;

#endif /* INCLUDE_ENUMS_H_ */

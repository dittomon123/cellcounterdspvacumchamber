/******************************************************************************/
/*! \file Cpu2AdcDriver.h
 *
 *  \brief Header file for Cpu2AdcDriver.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Jul-21-2015
 *   $Author:  $ ADITYA B G, GURUDUTT R
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_data_acquisition_module
//! @{
//******************************************************************************
#ifndef ADC_DRIVER_H_
#define ADC_DRIVER_H_

#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File
#include "Cpu2Enums.h"
#include "Cpu2Structure.h"

extern DataAqParam StructDataAqParam;

/*!Prototype of the function which need to be used for the ADC Configuration*/

/******************************************************************************/
/*!
 *  \fn     void ADAdca_Init()
 *  \brief  This function initialize ADCA registers and associated SOC registers
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADAdca_Init(void);

/******************************************************************************/
/*!
 *  \fn     void ADConfigureAdca()
 *  \brief  This function configures ADC resolution, signal mode, clock
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADConfigureAdca(void);

/******************************************************************************/
/*!
 *  \fn     void ADConfigureAdcaEpwm()
 *  \brief  This function configures PWM, to trigger SOC
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADConfigureAdcaEpwm(void);

/******************************************************************************/
/*!
 *  \fn     void ADSetupAdcaEpwm()
 *  \brief  This function configures pwm trigger selection, acquisition window
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADSetupAdcaEpwm(void);

/******************************************************************************/
/*!
 *  \fn     void ADAdcb_Init()
 *  \brief  This function initialize ADCB registers and associated SOC registers
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADAdcb_Init(void);

/******************************************************************************/
/*!
 *  \fn     void ADConfigureAdcb()
 *  \brief  This function configures ADC resolution, signal mode, clock
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADConfigureAdcb(void);

/******************************************************************************/
/*!
 *  \fn     void ADConfigureAdcbEpwm()
 *  \brief  This function configures PWM, to trigger SOC
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADConfigureAdcbEpwm(void);

/******************************************************************************/
/*!
 *  \fn     void ADSetupAdcbEpwm()
 *  \brief  This function configures PWM trigger selection, acquisition window
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADSetupAdcbEpwm(void);

/******************************************************************************/
/*!
 *  \fn     void ADAdcc_Init()
 *  \brief  This function initialize ADCC registers and associated SOC registers
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADAdcc_Init(void);

/******************************************************************************/
/*!
 *  \fn     void ADConfigureAdcc()
 *  \brief  This function configures ADC resolution, signal mode, clock
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADConfigureAdcc(void);

/******************************************************************************/
/*!
 *  \fn     void ADConfigureAdccEpwm()
 *  \brief  This function configures PWM, to trigger SOC
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADConfigureAdccEpwm(void);

/******************************************************************************/
/*!
 *  \fn     void ADSetupAdccEpwm()
 *  \brief  This function configures PWM trigger selection, acquisition window
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADSetupAdccEpwm(void);

/******************************************************************************/
/*!
 *  \fn     void ADStartAdcDataAcquisition()
 *  \brief  This function configures and initiates the PWM trigger to start
 *      Acquisition
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADStartAdcDataAcquisition(void);

/******************************************************************************/
/*!
 *  \fn     void ADStartAdcDataAqWBC()
 *  \brief  This function configures and initiates the PWM trigger to start
 *      Acquisition of WBC channel
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADStartAdcDataAqWBC(void);

/******************************************************************************/
/*!
 *  \fn     void ADStopAdcDataAqWBC()
 *  \brief  This function configures and initiates the PWM trigger to stop
 *      Acquisition of WBC channel
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADStopAdcDataAqWBC(void);

/******************************************************************************/
/*!
 *  \fn     void ADStartAdcDataAqRBC()
 *  \brief  This function configures and initiates the PWM trigger to start
 *      Acquisition of RBC channel
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADStartAdcDataAqRBC(void);

/******************************************************************************/
/*!
 *  \fn     void ADStopAdcDataAqRBC()
 *  \brief  This function configures and initiates the PWM trigger to stop
 *      Acquisition of RBC channel
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADStopAdcDataAqRBC(void);

/******************************************************************************/
/*!
 *  \fn     void ADStartAdcDataAqPLT()
 *  \brief  This function configures and initiates the PWM trigger to start
 *      Acquisition of PLT channel
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADStartAdcDataAqPLT(void);

/******************************************************************************/
/*!
 *  \fn     void ADStopAdcDataAqPLT()
 *  \brief  This function configures and initiates the PWM trigger to stop
 *      Acquisition of PLT channel
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADStopAdcDataAqPLT(void);

/******************************************************************************/
/*!
 *  \fn     void ADStopAdcDataAcquisition()
 *  \brief  This function configures and initiates the PWM trigger to stop
 *      Acquisition
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ADStopAdcDataAcquisition(void);

/******************************************************************************/
/*!
 *  \fn     void ADGetAdcBufAddr()
 *  \brief  Not used
 *  \param  void
 *  \return  unsigned int*
 */
/******************************************************************************/
unsigned int* ADGetAdcBufAddr(void);

/******************************************************************************/
/*!
 *  \fn     void ADGetStructDataAqParam()
 *  \brief  Function to get the data praramter of StructDataAqParam
 *          instance to access ADCSampleCounter & Aquisition status
 *  \param  void
 *  \return  DataAqParam*
 */
/******************************************************************************/
DataAqParam* ADGetStructDataAqParam(void);

/******************************************************************************/
/*!
 *  \fn     void ADBloodDataAcquisition()
 *  \brief  Function to configure data acquisition of WBC, RBC & PLTfor First & Second
 *      counting cycles
 *  \param  Uint16 CountingCycle
 *  \return  void
 */
/******************************************************************************/
void ADBloodDataAcquisition(Uint16 CountingCycle);

/******************************************************************************/
/*!
 *  \fn     void adca1_isr()
 *  \brief  Not used
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
interrupt void adca1_isr(void);

/******************************************************************************/
/*!
 *  \fn     void adcb1_isr()
 *  \brief  Not used
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
interrupt void adcb1_isr(void);

/******************************************************************************/
/*!
 *  \fn     void adcc1_isr()
 *  \brief  Not used
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
interrupt void adcc1_isr(void);

extern Uint32 DAGetDestAddress(E_CELL_TYPE eTypeofCell, bool_t bAddofCellHtReq);
#endif /* ADC_DRIVER_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

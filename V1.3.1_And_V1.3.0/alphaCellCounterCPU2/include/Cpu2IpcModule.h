/******************************************************************************/
/*! \file Cpu2IpcModule.h
 *
 *  \brief Header file for Cpu2IpcModule.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-04-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_ipc_communication_module
//! @{
//******************************************************************************
#ifndef IPC_MODULE_H_
#define IPC_MODULE_H_
#include "F2837xD_device.h"     // Headerfile Include File
#include "F2837xD_Examples.h"   // Examples Include File
#include "F2837xD_Ipc_drivers.h"
#include "Cpu2Enums.h"
#include "Cpu2Structure.h"
//******************** IPC contents ********************************************

/*!Prototype of the function which need to be used for IPC Communication */

/******************************************************************************/
/*!
 *  \fn     uint16_t IPCCpu2toCpu1IpcCmdData()
 *  \brief  CPU2 shall transmit data packet to CPU1 using IPCLtoRBlockWrite call
 *  \param  IpcParameters *Cpu2IpcPacket, uint16_t dataLength
 *  \return  uint16_t
 */
/******************************************************************************/
uint16_t IPCCpu2toCpu1IpcCmdData(IpcParameters *Cpu2IpcPacket, uint16_t dataLength);

/******************************************************************************/
/*!
 *  \fn     uint16_t IPCCpu2toCpu1IpcCmd()
 *  \brief  CPU2 shall transmit data packet to CPU1 using IPCLtoRBlockWrite call
 *  \param  IpcParameters *Cpu2IpcPacket
 *  \return  uint16_t
 */
/******************************************************************************/
uint16_t IPCCpu2toCpu1IpcCmd(IpcParameters *Cpu2IpcPacket);

/******************************************************************************/
/*!
 *  \fn     uint16_t IPCCpu2IpcReqMemAccess()
 *  \brief  CPU1 or CPU2 requests memory access for RW
 *  \param  void
 *  \return  uint16_t
 */
/******************************************************************************/
uint16_t IPCCpu2IpcReqMemAccess(void);

/******************************************************************************/
/*!
 *  \fn     IpcParameters* IPCGetCpu2IpcParam()
 *  \brief  Responds to 16/32-bit data word write command the remote CPU system
 *
 *  CPU system which includes the 16/32-bit data word to write to the local CPU
 *  address.
 *
 *  This function will allow the local CPU system to update all ipc related
 *  variables and status flags
 *  \param  void
 *  \return  IpcParameters*
 */
/******************************************************************************/
IpcParameters* IPCGetCpu2IpcParam(void);

/******************************************************************************/
/*!
 *  \fn     void IPCInitialize_IPC()
 *  \brief  This function configures IPC interrupts registers of CPU1
 *      initializes peripheral & enables interrupts
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void IPCInitialize_IPC(void);

/******************************************************************************/
/*!
 *  \fn     void IPCCpu2toCpu1IpcCommSync()
 *  \brief  CPU1 shall initiates communication by polling STS bit to ensure CPU2
 *      is ready for the handshaking signals.
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void IPCCpu2toCpu1IpcCommSync(void);

/******************************************************************************/
/*!
 *  \fn     void IPCClearCpu2IpcPacketTxBuf()
 *  \brief  Clears the Tx buffer of IpcParameters
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void IPCClearCpu2IpcPacketTxBuf(IpcParameters *Cpu2IpcPacket);

/******************************************************************************/
/*!
 *  \fn     void IPCClearCpu2IpcPacketRxBuf()
 *  \brief  Clears the Rx buffer of IpcParameters
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void IPCClearCpu2IpcPacketRxBuf(IpcParameters *Cpu2IpcPacket);
//void Error(void);

/******************************************************************************/
/*!
 *  \fn     void CPU01toCPU02IPC0IntHandler()
 *  \brief  CPU1 shall read/write to the shared memory based on the command
 *      received from CPU2
 *  \param  void
 *  \return  __interrupt void
 */
/******************************************************************************/
__interrupt void CPU01toCPU02IPC0IntHandler (void);

/******************************************************************************/
/*!
 *  \fn     void CPU01toCPU02IPC1IntHandler()
 *  \brief  CPU1 shall read/write to the shared memory based on the
 *      command  received from CPU2
 *  \param  void
 *  \return __interrupt void
 */
/******************************************************************************/
__interrupt void CPU01toCPU02IPC1IntHandler (void);

//******************** IPC contents ends ***************************************


#endif /* IPC_MODULE_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

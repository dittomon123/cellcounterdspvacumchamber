/******************************************************************************/
/*! \file Cpu2InitBoard.h
 *
 *  \brief Header file for Cpu2InitBoard.c
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Nov-09-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_initialization_module
//! @{
//******************************************************************************
#ifndef POC2V2CELLCOUNTERCPU2_INCLUDE_CPU2INITBOARD_H_
#define POC2V2CELLCOUNTERCPU2_INCLUDE_CPU2INITBOARD_H_
#include "F28x_Project.h"     // Device Headerfile and Examples Include File

#define _ALPHA_2_BOARD_

/*!Prototype of the function which need to be used for initializing the board */

/******************************************************************************/
/*!
 *  \fn     void IBInitialize_Board()
 *  \brief  This function initialize System Clock & Vector table
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void IBInitialize_Board(void);

#endif /* POC2V2CELLCOUNTERCPU2_INCLUDE_CPU2INITBOARD_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

/******************************************************************************/
/*! \file Cpu2DataAcquisition.h
 *
 *  \brief Header file for Cpu2DataAcquisition.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Aug-18-2015
 *   $Author:  $ GURUDUTT R, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_data_acquisition_module
//! @{
//******************************************************************************

#ifndef INCLUDE_DATAACQUISTION_H_
#define INCLUDE_DATAACQUISTION_H_
#include "F28x_Project.h"
#include "Cpu2InitBoard.h"
#include "Cpu2Enums.h"
#include "Cpu2Structure.h"


/******************************************************************************/
/*!
 * \brief Function prototypes:
 * \fn void DAConfigSDRAM_DMA(Uint16 MeasurementCycle)
 * \fn void DAInitDMA_Params(Uint16 MeasurementCycle);
 * \fn DAConfigureDMA(void)
 * \fn DAConfigureDMA_EMIF_WBC(void)
 * \fn DAConfigureDMA_EMIF_RBC(void)
 * \fn DAConfigureDMA_EMIF_PLT(void)
 *
 *
 * \fn void DADMA_DstDataClear(volatile unsigned int *Address, unsigned long Size)
 * \fn __interrupt void local_DMACH1_ISR(void);
 * \fn __interrupt void local_DMACH2_ISR(void);
 * \fn __interrupt void local_DMACH3_ISR(void);
 * \fn __interrupt void local_DMACH4_ISR(void);
 * \fn Uint16 DAGetDestAddress(Uint16 Type);
 * \fn int16 ConfigEmif1CPU(Uint16 CpuConfig);
 * \fn Uint16 CSGetEmif1Status(void);
 */
/******************************************************************************/

/******************************************************************************/
/*!
 *  \fn     void DAConfigSDRAM_DMA()
 *  \brief  This function configures and initiates EMIF , DMA & SDRAM registers
 *  \param  Uint16 MeasurementCycle
 *  \return  void
 */
/******************************************************************************/
void DAConfigSDRAM_DMA(Uint16 MeasurementCycle);

/******************************************************************************/
/*!
 *  \fn     void DAInitDMA_Params()
 *  \brief  This function configures DMA register
 *  \param  Uint16 MeasurementCycle
 *  \return  void
 */
/******************************************************************************/
void DAInitDMA_Params(Uint16 MeasurementCycle);

/******************************************************************************/
/*!
 *  \fn     void DAConfigureDMA()
 *  \brief  This function configures DMA register
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void DAConfigureDMA(void);

/******************************************************************************/
/*!
 *  \fn     void DAConfigureDMA_EMIF_WBC()
 *  \brief  This function configures DMA registers and configure ADC sample to SDRAM
 *      For WBC
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void DAConfigureDMA_EMIF_WBC(void);

/******************************************************************************/
/*!
 *  \fn     void DAConfigureDMA_EMIF_RBC()
 *  \brief  This function configures DMA registers and configure ADC sample to SDRAM
 *      For RBC
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void DAConfigureDMA_EMIF_RBC(void);

/******************************************************************************/
/*!
 *  \fn     void DAConfigureDMA_EMIF_PLT()
 *  \brief  This function configures DMA registers and configure ADC sample to SDRAM
 *      For PLT
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void DAConfigureDMA_EMIF_PLT(void);

/******************************************************************************/
/*!
 *  \fn     void DAGetDestAddress()
 *  \brief  This function returns the memory address of the variable
 *      WBC/RBC/PLT buffer address will be returned
 *  \param  E_CELL_TYPE eTypeofCell, bool_t bAddofCellHtReq
 *  \return  void
 */
/******************************************************************************/
Uint32 DAGetDestAddress(E_CELL_TYPE eTypeofCell, bool_t bAddofCellHtReq);

/******************************************************************************/
/*!
 *  \fn     void DADMA_DstDataClear()
 *  \brief  Initialize SDRAM/RAM memory location
 *  \param  volatile unsigned int *Address, unsigned long Size
 *  \return  void
 */
/******************************************************************************/
void DADMA_DstDataClear(volatile unsigned int *Address, unsigned long Size);

/******************************************************************************/
/*!
 *  \fn     void local_DMACH1_ISR()
 *  \brief  This interrupt shall generate once the configured chunk of ADC data
 *      samples are captured and stored in SDRAM memory location
 *  \param  void
 *  \return  __interrupt void
 */
/******************************************************************************/
__interrupt void local_DMACH1_ISR(void);

/******************************************************************************/
/*!
 *  \fn     void local_DMACH2_ISR()
 *  \brief  This interrupt shall generate once the configured chunk of ADC data
 *      samples are captured and stored in SDRAM memory location
 *  \param  void
 *  \return  __interrupt void
 */
/******************************************************************************/
__interrupt void local_DMACH2_ISR(void);

/******************************************************************************/
/*!
 *  \fn     void local_DMACH3_ISR()
 *  \brief  This interrupt shall generate once the configured chunk of ADC data
 *      samples are captured and stored in SDRAM memory location
 *  \param  void
 *  \return  __interrupt void
 */
/******************************************************************************/
__interrupt void local_DMACH3_ISR(void);

/******************************************************************************/
/*!
 *  \fn     void local_DMACH4_ISR()
 *  \brief  This interrupt shall generate once the configured chunk of ADC data
 *      samples are captured and stored in SDRAM memory location
 *  \param  void
 *  \return  __interrupt void
 */
/******************************************************************************/
__interrupt void local_DMACH4_ISR(void);

/******************************************************************************/
/*!
 *  \fn     void DAGetDmaAdcParams()
 *  \brief  Returns the DMA parameters of the ADC
 *  \param  void
 *  \return  DmaAdcParameters*
 */
/******************************************************************************/
DmaAdcParameters* DAGetDmaAdcParams(void);

/******************************************************************************/
/*!
 *  \fn     void DACellDataSampleAnalysis()
 *  \brief  This function shall process stored ADC raw data and calculate peaks
 *      - Presently not used
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void DACellDataSampleAnalysis(void);

/******************************************************************************/
/*!
 *  \fn     void DAMovingAvg()
 *  \brief  This function shall remove any glithces and smoothen the signal and pulses
 *      - Presently not used
 *  \param  Uint16 fVoltage, E_CELL_TYPE eTypeofCell, Uint16 uReset
 *  \return  Uint16
 */
/******************************************************************************/
Uint16 DAMovingAvg(Uint16 fVoltage, E_CELL_TYPE eTypeofCell, Uint16 uReset);

/******************************************************************************/
/*!
 *  \fn     void DAStorePulseHeightinSDRAM()
 *  \brief  Function to store pulse height of the cells in SDRAM
 *  \param  Uint16 uiCellType, Uint32 ulCellCnt, Uint16 ulPulseHeight
 *  \return  void
 */
/******************************************************************************/
void DAStorePulseHeightinSDRAM(Uint16 uiCellType, Uint32 ulCellCnt,
                            Uint16 ulPulseHeight);

/******************************************************************************/
/*!
 *  \fn     void DAWBCCellDataSampleAnalysis()
 *  \brief  This function is for processing WBC signal based on the current sequence
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void DAWBCCellDataSampleAnalysis(void);

/******************************************************************************/
/*!
 *  \fn     void DARBCPLTCellDataSampleAnalysis()
 *  \brief   This function is for processing RBC and PLT signal based on the
 *  current sequence
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void DARBCPLTCellDataSampleAnalysis(void);

/******************************************************************************/
/*!
 *  \fn     void ClearSDRAM()
 *  \brief  Not used
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void ClearSDRAM(void);

#endif /* INCLUDE_DATAACQUISTION_H_ */

//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

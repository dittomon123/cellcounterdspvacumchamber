/******************************************************************************/
/*! \file Cpu2ConfigSDRAM.h
 *
 *  \brief Header file for Cpu2ConfigSDRAM.c
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Nov-09-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//! \addtogroup cpu2_sdram_config_module
//! @{
//******************************************************************************
#ifndef INCLUDE_CPU2INITSDRAM_H_
#define INCLUDE_CPU2INITSDRAM_H_
#include "F28x_Project.h"     // Device Headerfile and Examples Include File
//#include "fpu_vector.h"       // FPU headerfile to access memcpy_fast_far()

/*!Prototype of the function which need to be used for SDRAM Configuration */

/******************************************************************************/
/*!
 *  \fn     void CSConfigEmif1CPU()
 *  \brief  This function provided R/W access to the CPU2 core
 *  \param  Uint16 CpuConfig
 *  \return  int16
 */
/******************************************************************************/
int16 CSConfigEmif1CPU(Uint16 CpuConfig);

/******************************************************************************/
/*!
 *  \fn     void CSGetEmif1Status()
 *  \brief  This function read EMIF1MSEL bit and returns the value
 *  \param  void
 *  \return  Uint16
 */
/******************************************************************************/
Uint16 CSGetEmif1Status(void);

/******************************************************************************/
/*!
 *  \fn     void CSSdramWriteWORD()
 *  \brief  To write word to the memory address
 *  \param  Uint32 MemAddr, Uint16 Data
 *  \return  void
 */
/******************************************************************************/
void CSSdramWriteWORD(Uint32 MemAddr, Uint16 Data);

/******************************************************************************/
/*!
 *  \fn     void CSSdramWriteDWORD()
 *  \brief  To write word to the memory address
 *  \param  Uint32 MemAddr,Uint32 Data
 *  \return  void
 */
/******************************************************************************/
void CSSdramWriteDWORD(Uint32 MemAddr,Uint32 Data);

/******************************************************************************/
/*!
 *  \fn     void CSSdramReadWORD()
 *  \brief  To read word to the memory address
 *  \param  Uint32 MemAddr
 *  \return  Uint16
 */
/******************************************************************************/
Uint16 CSSdramReadWORD(Uint32 MemAddr);

/******************************************************************************/
/*!
 *  \fn     void CSSdramReadDWORD()
 *  \brief  To read word to the memory address
 *  \param  Uint32 MemAddr
 *  \return  Uint32
 */
/******************************************************************************/
Uint32 CSSdramReadDWORD(Uint32 MemAddr);

#endif /* INCLUDE_CPU2INITSDRAM_H_ */

//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

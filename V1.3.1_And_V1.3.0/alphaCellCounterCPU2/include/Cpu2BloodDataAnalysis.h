/******************************************************************************/
/*! \file Cpu2BloodDataAnalysis.h
 *
 *  \brief Header file for Cpu2BloodDataAnalysis.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Sep-07-2015
 *   $Author:  $ GURUDUTT R, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_data_analysis_module
//! @{
//******************************************************************************
#include "F28x_Project.h"
#include "Cpu2Enums.h"
#include "Cpu2Structure.h"

#ifndef INCLUDE_BLOODDATAANALYSIS_H_
#define INCLUDE_BLOODDATAANALYSIS_H_



/*!Prototype of the function which need to be used for the blood cell counting */

/******************************************************************************/
/*!
 *  \fn     void BDAPeakDetectionAlgorithm()
 *  \brief  This function takes ADC raw data and processed to detect peaks
 *      peaks shall be counter and stored in m_ulnCellCount variable
 *  \param  unsigned int unVoltage
 *  \return  void
 */
/******************************************************************************/
void BDAPeakDetectionAlgorithm(unsigned int unVoltage);

/******************************************************************************/
/*!
 *  \fn     void BDAGetBloodCellStatistics()
 *  \brief  This function shall get the threshold of RBC, WBC and PLT cells used
 *      for detection of peaks
 *  \param  void
 *  \return  stBloodCellVoltVal*
 */
/******************************************************************************/
stBloodCellVoltVal* BDAGetBloodCellStatistics(void);

/******************************************************************************/
/*!
 *  \fn     void BDAGetBloodCellStatisticsSize()
 *  \brief  This function shall get the threshold of RBC, WBC and PLT cells used
 *      for detection of peaks
 *  \param  void
 *  \return  uint16_t
 */
/******************************************************************************/
uint16_t BDAGetBloodCellStatisticsSize(void);

/******************************************************************************/
/*!
 *  \fn     void BDAGetWBCBloodCellStatistics()
 *  \brief  This function shall get the threshold of WBC cells used for detection
 *      of WBC peaks
 *  \param  void
 *  \return  stBloodCellVoltVal*
 */
/******************************************************************************/
stBloodCellVoltVal* BDAGetWBCBloodCellStatistics(void);

/******************************************************************************/
/*!
 *  \fn     void BDAGetRBCBloodCellStatistics()
 *  \brief  This function shall get the threshold of RBC cells used for detection
 *      of RBC peaks
 *  \param  void
 *  \return  stBloodCellVoltVal*
 */
/******************************************************************************/
stBloodCellVoltVal* BDAGetRBCBloodCellStatistics(void);

/******************************************************************************/
/*!
 *  \fn     void BDAGetPLTBloodCellStatistics()
 *  \brief  This function shall get the threshold of PLT cells used for detection
 *      of PLT peaks
 *  \param  void
 *  \return  stBloodCellVoltVal*
 */
/******************************************************************************/
stBloodCellVoltVal* BDAGetPLTBloodCellStatistics(void);

/******************************************************************************/
/*!
 *  \fn     void BDAResetBloodCellStatistics()
 *  \brief  This function resets the variables associated with peak detection
 *  \param  stBloodCellVoltVal* BloodCellStatistics
 *  \return  void
 */
/******************************************************************************/
void BDAResetBloodCellStatistics(stBloodCellVoltVal* BloodCellStatistics);

/******************************************************************************/
/*!
 *  \fn     void BDACellPeakDetectionAlgorithm()
 *  \brief  Not used
 *  \param  unsigned int unVoltage, enum eCellType enumCellType
 *  \return  void
 */
/******************************************************************************/
void BDACellPeakDetectionAlgorithm(unsigned int unVoltage, enum eCellType enumCellType);

/******************************************************************************/
/*!
 *  \fn     void BDAWBCPeakDetectionAlgorithm()
 *  \brief  This function takes WBC ADC raw data and processed to detect peaks
 *      peaks shall be counter and stored in m_ulnCellCount variable
 *  \param  unsigned int unVoltageIn
 *  \return  void
 */
/******************************************************************************/
void BDAWBCPeakDetectionAlgorithm(unsigned int unVoltageIn);

/******************************************************************************/
/*!
 *  \fn     void BDARBCPeakDetectionAlgorithm()
 *  \brief  This function takes RBC ADC raw data and processed to detect peaks
 *      peaks shall be counter and stored in m_ulnCellCount variable
 *  \param  unsigned int unVoltageIn
 *  \return  void
 */
/******************************************************************************/
void BDARBCPeakDetectionAlgorithm(unsigned int unVoltageIn);

/******************************************************************************/
/*!
 *  \fn     void BDAPLTPeakDetectionAlgorithm()
 *  \brief  This function takes PLT ADC raw data and processed to detect peaks
 *      peaks shall be counter and stored in m_ulnCellCount variable
 *  \param  unsigned int unVoltageIn
 *  \return  void
 */
/******************************************************************************/
void BDAPLTPeakDetectionAlgorithm(unsigned int unVoltageIn);

/******************************************************************************/
/*!
 *  \fn     void BDAWBCDataAnalysis()
 *  \brief  This function shall process stored WBC ADC raw data and calculate peaks
 *  \param  Uint32 usnAddress, Uint32 usnLength
 *  \return  unsigned int
 */
/******************************************************************************/
unsigned int BDAWBCDataAnalysis(Uint32 usnAddress, Uint32 usnLength);

/******************************************************************************/
/*!
 *  \fn     void BDAAlgorithmVarReset()
 *  \brief  This function is to reset all the variables related to the Algorithm
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void BDAAlgorithmVarReset(void);

unsigned long GetWBCPulse();
unsigned long GetRBCPulse();
unsigned long GetPLTPulse();

extern Uint16 CSSdramReadWORD(Uint32 MemAddr);
extern  void DAStorePulseHeightinSDRAM(Uint16 uiCellType, Uint32 ulCellCnt, \
        Uint16 ulPulseHeight);
#endif /* INCLUDE_BLOODDATAANALYSIS_H_ */
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

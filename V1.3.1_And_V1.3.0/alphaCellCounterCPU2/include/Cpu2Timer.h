/******************************************************************************/
/*! \file Cpu2Timer.h
 *
 *  \brief Header file for Cpu2Timer.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.2
 *   $Date:    $ Jan-21-2015
 *   $Author:  $ 20040919, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu2_timer_module
//! @{
//******************************************************************************
#ifndef TIMER_H_
#define TIMER_H_

#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File

/*!Prototype of the function which need to be used for the Timer configuration */

/******************************************************************************/
/*!
 *  \fn     void Timer2_Interrupt()
 *  \brief  Timer2 interrupt enable
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void Timer2_Interrupt(void);

/******************************************************************************/
/*!
 *  \fn     void Start_Timer2()
 *  \brief  To start timer 2
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void Start_Timer2(void);

/******************************************************************************/
/*!
 *  \fn     void Stop_Timer2()
 *  \brief  To stop timer 2
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void Stop_Timer2(void);

/******************************************************************************/
/*!
 *  \fn     void Configure_Timer2()
 *  \brief  To configure Timer 2
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void Configure_Timer2(void);

/******************************************************************************/
/*!
 *  \fn     void cpu_timer2_isr()
 *  \brief  Timer 2 interrupt service routine
 *  \param  void
 *  \return  interrupt void
 */
/******************************************************************************/
__interrupt void cpu_timer2_isr(void);

/******************************************************************************/
/*!
 * \brief Preprocessor definitions
 * \def TIMER1_CYCLE_TIME_uS - not used
 * \def TIMER2_CYCLE_TIME_uS - not used
 */
/******************************************************************************/
#define TIMER1_CYCLE_TIME_uS	2000	//2 mSec cycle time
#define TIMER2_CYCLE_TIME_uS	1000000	//1 Sec cycle time

#endif /* TIMER_H_ */

//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

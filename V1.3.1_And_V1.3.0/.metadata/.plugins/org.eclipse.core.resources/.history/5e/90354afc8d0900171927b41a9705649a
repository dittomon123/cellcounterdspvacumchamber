/******************************************************************************/
/*! \file InitGpio.h
 *
 *  \brief Header file for InitGpio.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-05-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_gpio_module
//! @{
//******************************************************************************
#ifndef INIT_GPIO_H_
#define INIT_GPIO_H_

#include "F2837xD_device.h"     // Headerfile Include File
#include "F2837xD_Examples.h"   // Examples Include File
#include "Pwm.h"
#include "Structure.h"
#define	_ALPHA_2_BOARD_
#define _ALPHA_5_BOARD_
#define _X_AXIS_HELICAL_

/******************************************************************************/
/*!
 * \def SPI_SIMO_MP	SPI Delfino Slave In Sitara Master Out pin GPIO58
 * \def SPI_SOMI_MP	SPI Delfino Slave Out Sitara Master In pin GPIO59
 * \def SPI_CLK_MP SPI Sitara Clock pin GPIO60
 * \def SPI_CS_MP SPI Sitara Chip Select pin GPIO61
 */
/******************************************************************************/
#define	SPI_SIMO_MP													58
#define	SPI_SOMI_MP													59
#define	SPI_CLK_MP													60
#define	SPI_CS_MP													61

#define SPI_PERIPHERAL_MP											15


/******************************************************************************/
/*!
 * \def SPI_COMM_DELFINO_TXR_INT_REQ	SPI Delfino Transfer interrupt
 *                                      request pin GPIO165
 * \def SPI_COMM_DELFINO_BUSY_STATUS	SPI Delfino Busy state pin GPIO166
 * \def SPI_COMM_SITARA_BUSY_STATUS		SPI Sitara Busy state pin GPIO167
 * \def SPI_COMM_SITARA_ABORT_REQ		SPI Sitara Abort Request pin GPIO168
 *
 */
/******************************************************************************/
#define	SPI_COMM_CTRL_DELFINO_TXR_INT_REQ							165
#define	SPI_COMM_CTRL_DELFINO_BUSY_STATUS							166
#define	SPI_COMM_CTRL_SITARA_BUSY_STATUS							167
#define	SPI_COMM_CTRL_SITARA_ABORT_REQ								168
#define GPIO_PERIPHERAL_MP											0

/******************************************************************************/
/*!
 * \brief SPI interface and pin configuration of valve control
 * \def SPI_SIMO_VALVE    Pin no of Slave input and Master output pin
 * \def SPI_SOMI_VALVE    Pin no of Slave output and Master input pin
 * \def SPI_CLK_VALVE     Pin no of SPI clock pin
 * \def SPI_CS_VALVE      Pin no of SPI chip select
 * \def SPI_RESET_VALVE   Pin no of SPI reset
 * \def SPI_PERIPHERAL_VALVE   Pin no of SPI peripheral
 */
/******************************************************************************/
#define	SPI_SIMO_VALVE												122
#define	SPI_SOMI_VALVE												123
#define	SPI_CLK_VALVE												124
#define	SPI_CS_VALVE												125
#define	SPI_RESET_VALVE												5
#define SPI_PERIPHERAL_VALVE										6

/******************************************************************************/
/*!
 * \brief SPI interface and pin configuration of Home sensor
 * \def SPI_SIMO_HOME_SENSE    Pin no of Slave input and Master output pin
 * \def SPI_SOMI_HOME_SENSE    Pin no of Slave output and Master input pin
 * \def SPI_CLK_HOME_SENSE     Pin no of SPI clock pin
 * \def SPI_CS_HOME_SENSE      Pin no of SPI chip select
 * \def SPI_PERIPHERAL_HOME_SENSE   Pin no of SPI peripheral
 */
/******************************************************************************/
#define	SPI_SIMO_HOME_SENSE											63
#define	SPI_SOMI_HOME_SENSE											64
#define	SPI_CLK_HOME_SENSE											65
#define	SPI_CS_HOME_SENSE											118//66
#define SPI_PERIPHERAL_HOME_SENSE									15

/******************************************************************************/
/*!
 * \brief Pin configuration of uart port for debug messages
 * \def DEBUG_PORT_UART_TX for transmit
 * \def DEBUG_PORT_UART_RX for receive from PC
 *
 */
/******************************************************************************/
#define	DEBUG_PORT_UART_TX											8
#define	DEBUG_PORT_UART_RX											9
#define DEBUG_PORT_UART_PERIPHERAL									6

/******************************************************************************/
/*!
 * \brief definitions of Home Position Sensor detection pins
 * \def GPIO_HOME_POS1  Pin no. of home position sensor: X-Axis
 * \def GPIO_HOME_POS2  Pin no. of home position sensor: Diluent
 * \def GPIO_HOME_POS3  Pin no. of home position sensor: Sample
 * \def GPIO_HOME_POS4  Pin no. of home position sensor: Waste
 * \def GPIO_HOME_POS5  Pin no. of home position sensor: Y-Axis
 * \def GPIO_HOME_POS_INT    Pin no. of home position interrupt
 */
/******************************************************************************/
#ifdef _ALPHA_2_BOARD_
#ifdef _ALPHA_5_BOARD_
#define     GPIO_HOME_POS1                                          114 //X-axis
#define     GPIO_HOME_POS2                                          112 //Diluent
#define     GPIO_HOME_POS3                                          115 //Sample
#define     GPIO_HOME_POS4                                          111 //Waste
#define     GPIO_HOME_POS5                                          113 //Y-Axis
#else
#define 	GPIO_HOME_POS1											111
#define 	GPIO_HOME_POS2											112
#define 	GPIO_HOME_POS3											113
#define 	GPIO_HOME_POS4											114
#define 	GPIO_HOME_POS5											115
#endif
#define 	GPIO_HOME_POS_INT										106
#else
#define 	GPIO_HOME_POS1											107
#define 	GPIO_HOME_POS2											66
#define 	GPIO_HOME_POS3											109
#define 	GPIO_HOME_POS4											110
#define 	GPIO_HOME_POS5											111
#endif

/******************************************************************************/
/*!
 * \brief definitions of Volumetric Sensor detection pins
 * \def GPIO_VOL1  Pin no. of volumetric interrupt: RBC starting
 * \def GPIO_VOL2  Pin no. of volumetric interrupt: RBC ending
 * \def GPIO_VOL3  Pin no. of volumetric interrupt: WBC starting
 * \def GPIO_VOL4  Pin no. of volumetric interrupt: WBC ending
 */
/******************************************************************************/
#ifdef _ALPHA_2_BOARD_
#define 	GPIO_VOL1												107
#define 	GPIO_VOL2												108
#define 	GPIO_VOL3												109
#define 	GPIO_VOL4												110
#endif

/******************************************************************************/
/*!
 * \brief definitions of PWM pins for Solenoid & HGB
 * \def S_PWM_VALVE  Pin no. of pwm control of solinoid valve
 * \def GPIO_VOL2  Pin no. of volumetric interrupt: RBC ending
 */
/******************************************************************************/
#define	S_PWM_VALVE													151
#define	HB_PWM_HGB													157

#define S_PWM_VALVE_PERIPHERAL										1
#define HB_PWM_HGB_PERIPHERAL										1

/******************************************************************************/

#define	TEST_GPIO													116			//CRH_GPIO
#define	LED1_GPIO													118//116	//CRH_GPIO
#define	LED2_GPIO													117

#define	SAMPLE_START_BUTTON_GPIO									126

#define CYCLE_COUNTER_GPIO_OUTPUT1									53
#define CYCLE_COUNTER_GPIO_OUTPUT2									54

#define	HIGH_VOLTAGE_SELECT_GPIO_OUTPUT								161
#define	ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT								162

#define WASTE_DETECT_GPIO                                           100

#define 	M_RESET													10


//#define 	M1_STEP													14		//PWM8A defined in Pwm.c
#define 	M1_MODE0												4
#define 	M1_MODE1												3
#define 	M1_MODE2												2
#define 	M1_ENBL													6
#define 	M1_DIR													7
#define 	M1_DECAY												16
#define 	M1_FAULT												17

//#define 	M2_STEP													155		//PWM6A defined in Pwm.c
#define 	M2_MODE0												13
#define 	M2_MODE1												12
#define 	M2_MODE2												11
#define 	M2_ENBL													15
#define 	M2_DIR													20
#define 	M2_DECAY												21
#define 	M2_FAULT												22

//#define 	M3_STEP													147		//PWM2A defined in Pwm.c
#define 	M3_MODE0												25
#define 	M3_MODE1												24
#define 	M3_MODE2												23
#define 	M3_ENBL													27
#define 	M3_DIR													28
#define 	M3_DECAY												33
#define 	M3_FAULT												19

//#define 	M4_STEP													153		//PWM5A defined in Pwm.c
#define 	M4_MODE0												37
#define 	M4_MODE1												35
#define 	M4_MODE2												34
#define 	M4_ENBL													91
#define 	M4_DIR													150
#define 	M4_DECAY												95
#define 	M4_FAULT												96

//#define 	M5_STEP													0		//PWM1A defined in Pwm.c
#define 	M5_MODE0												99
#define 	M5_MODE1												98
#define 	M5_MODE2												97
#define 	M5_ENBL													101
#define 	M5_DIR													102
#define 	M5_DECAY												103
#define 	M5_FAULT												94

#define     DILUENT_PRIME_CHECK                                     128
#define     LYSE_PRIME_CHECK                                        132
#define     RINSE_PRIME_CHECK                                       134

#define		SAMPLE_START_BTN_PRESS		(GpioDataRegs.GPDDAT.bit.GPIO126)




/******************************************************************************/
/*!
 *
 * \fn void IGInitialize_Gpio(void);
 * \brief function prototype definition
 * \fn void IGBlinkLED( unsigned int  mSecLEDBlink);
 * \brief function prototype definition
 * \fn void IGConfigLED( unsigned int  Pin,  unsigned int  Status);
 * \fn void IGGpioConfig_LED(void);
 * \fn void IGGpioConfig_HighVoltPin(void);
 * \fn void IGConfigGpioPin(GpioPinConfig SetGpioPin);
 * \brief function prototype definition
 */
/******************************************************************************/
void IGInitialize_Gpio(void);
void IGBlinkLED( unsigned int  mSecLEDBlink);
void IGConfigLED( unsigned int  Pin,  unsigned int  State);
void IGConfigHighVoltageModule( unsigned int  Pin,  unsigned int  State);
void IGGpioConfig_DefaultState(void);
void IGGpioConfig_LED(void);
void IGGpioConfig_HighVoltPin(void);
void IGConfigGpioPin(GpioPinConfig SetGpioPin);

void IGGpioConfig_Motor1(void);
void IGGpioConfig_Motor2(void);
void IGGpioConfig_Motor3(void);
void IGGpioConfig_Motor4(void);
void IGGpioConfig_Motor5(void);
void IGGpioConfig_MotorReset(void);
void IGGpioConfig_SampleStartButton(void);

int IGWriteMotorGpio( unsigned int  Pin,  unsigned int  State);
Uint16 IGReadMotorGpio( unsigned int  Pin);
void IGGpioConfig_CycleCounterPin(void);
void IGWriteCycleCounterGpio( unsigned int  Pin,  unsigned int  State);

void IGGpioConfig_SpiComm(void);
void IGGpioConfig_SpiCommControlPin(void);
void IGWriteSpiCommControlPin( unsigned int  Pin,  unsigned int  State);

void IGGpioConfig_UartComm(void);

void IGGpioConfig_SpiValve(void);
void IGWriteSpiValveResetPin( unsigned int  Pin,  unsigned int  State);

void IGGpioConfig_SpiHomeSense(void);

void IGGpioConfig_ValvePwm(void);
void IGGpioConfig_HgbPwm(void);

void IGGpioConfig_WasteDetectPin(void);//SKM_WASTE_DETECT_CHANGE

void IGConfigHomeSensCS( unsigned int  Pin,  unsigned int  State);
void IGInit_GPIO_HomePositionSensor(void);

void IGGpioConfig_ReagentPrimingCheck(void);
#endif /* INIT_GPIO_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

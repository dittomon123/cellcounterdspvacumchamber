/******************************************************************************/
/*! \file InitBoard.h
 *
 *  \brief Header file for InitBoard.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-23-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_cpu_initialization_module
//! @{
//*****************************************************************************
#ifndef INIT_BOARD_H_
#define INIT_BOARD_H_

#include "F2837xD_device.h"     // Headerfile Include File
#include "F2837xD_Examples.h"   // Examples Include File


/******************************************************************************/
/*!
 * \brief Function prototypes:
 * \fn void IBInitialize_Board();
 * \fn void IBTimer_Interrupt();
 * \fn void IBWatchDog_Timer_Interrupt();
 * \fn Uint16 IBGetInterruptRegStatus();
 * \fn void IBSetInterruptRegStatus(Uint16 InterruptReg);
 * \fn __interrupt void wakeint_isr(void);
 * \fn __interrupt void cpu_timer0_isr(void);
 * \fn __interrupt void cpu_timer1_isr(void);
 */
/******************************************************************************/
void IBInitialize_Board();
void IBTimer_Interrupt();
void IBWatchDog_Timer_Interrupt();
Uint16 IBGetInterruptRegStatus();

void IBSetInterruptRegStatus(Uint16 InterruptReg);

__interrupt void wakeint_isr(void);
__interrupt void cpu_timer0_isr(void);
__interrupt void cpu_timer1_isr(void);

#define	JTAG_ENABLE		TRUE
#define	JTAG_DISABLE	FALSE


#endif /* INIT_BOARD_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

25/7/2018:-->V2.4.43 & V0.0.33 is released.

26/7/2018: V0.0.44
1.In Shutdown sequence aspiration of cleaner to needle is added.
2.In WBM,HgBSampleAcq taking before drainig the bath.
3.In WBM,displacement for 1st & 2nd bubbling of WBC bath is reduced to 5mm from 8mm.
 
 27/7/2018:
 1.Starting cmd for wake up sequence is added.
 2.Watchdog timer is enabled for 60ms.(But we are not reseting the CPU currently).
 1/8/2018:
 1.g_usWholeBloodZapTime variable is added to take zap time from UI.
 
 7/8/2018:
 #define PULSEWIDTH_MAX_WBC                      60 is changed to 44.
 #define PULSEWIDTH_MAX_RBC                      60 is chanegd to 46.
 CPU1-1.1.44
 28/08/2018:
 1.HGB blank value in startUp sequence is added.
 5/9/2018:
 CPU1-2.2.45
 1.For counting, Vacuum creation is fixed to 4.5 psi.
 
 31/11/2018:
 1.Pressure sensor line presence check is added.()
 
 6/11/2018: (1.0.46)
 1.Bubble count time is added for all bubbling times.
 13/11/2018:
 1.Drain all Pop_up issue solved.
 2.Zap sequence is modified.(Count line Rinsing).
 3.Waste bin sensor testing is completed.
 
 (Version 0.0.48)  ->
 2/12/2018:
 1.In whole blood mode,WBC disp is changed  to 200 microL from 400 micrL.Remaining 200microL is adding through hose. 
 3/12/2018:
 1. In whole blood mode, RBC mixing timer is commented.(Extra waste movement)
 2. Lyse Prime Sequence changed.
 4/12/2018:
 1.EZ_Cleaning, Asp for each bath is changed to 1.5ml from 1ml.
-------->Released on 4th dec 2018. 
 6/12/2018:
 1.g_bCountTimeVacuumMoni variable is disable if the motor is stopping normal condition.
 7/12/2018:
 1.Pressure sensore line check is disable in prime all and drain all sequences.
 12/14/2019:
 1.Stattic analysis is done in this code and the modifications for the same is also there.
 
--------------------------<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>--------------------------------------------------
24/12/2019:
1.In WBM,WBC disp is changed  to 200 microL from 400 micrL.Remaining 200microL is adding through hose.(***Reverted***).
2,Zap is added for flush aperture sequence.(As customer asked).

 ---->Released on 24th dec 2018 (1.1.48)_new home position sensore
26/12/2018:
1.Pressure senore calibration 
1. Extra zap is added at the end of main sequence.(For Testing)
-->  Removed
 ------------>Released on 28th jan 2019 (0.0.51)_new home position sensore
 1.Pressure senore calibration changed
 2.Abort the start-up sequence if the waste float sensor is detected at start-up.-->added
 
 4/2/2019:
 1.RBC lower count time increased to 1sec from 0.5 sec and  upper time is as is 0.5sec.(Done By divya)
 14/2/2019:
 1.RBC lower thresold is inc to 1sec from 0.5sec.
 2.Waste syringe speed is not reducing in the WBC RBC count time and Flow calibration sequence.
 
 25/2/2019:
 1.Pressure calibration is modification:
 	whenever we are calibrating we need to take actual pressure from pressure sensor.This one is added.
 	(before it was like calibrating the pressure on the calibrated value.)
 
 27/2/2019:
 1.ADC Pressure average for 32 is removed and evry 20ms once data acq added.
 
 28/2/2019:
 1.The main 20ms timer is changed to 5ms timer for reading system health data(Pressure,Temp,hgb..etc)
 	 but the Main loop is still at every 20ms once only.(To get the instance pressure,tem ect values )
 2. Smooth stoping of waste motor is added at the time of count vacuum creation. 

1/3/2019:
1.Pressure and timeStamp at every state is added.
2.Aqc start pressure ,aqc end pressure and motor stop pressure is added in the packet while sending results. 
 	 
8/3/2019:
1.Pressure calibration sign data variable is deleted. and directly taking the index 10 value when we save the bubble frequency from set up.

11/3/2019:
1.Pressure printing on teraterm is configuard to test parameter index 1.

22/3/2019:
1.We have taken a jan 11th released MBD (With back flow) to resolve the back flow issue.
	Modifications done accordingly in the model.  *** BACK FLOW ISSUE SOLVED  **
25/3/2019:
1.Testing did for backflow, drain tub,bubbles in glass tube, bubbles at bath back side and liquid in pressure senore line.
(above all are fine in this date for more then 100 runs).

 8/4/2019:
 1.Full data analysis is added for RBC PLT.(CV testing )(CPU 2)
 2.Lower acq time changed to 3sec from 5sec.

9/4/2019:
1.Tune volumetric sequence is changed to shutdown rinse fill sequnce.(Shutdown testing).
 -->Released on 10th april 2019
 
 22/4/2019:
 1.Zap change in introduced.

7/5/2019:
1.HGB LED is keeping it ON for all the time.
2.Default Pressure value istaking from processor.
 
 8/5/2019:
 1.sequence started cmd is sending to processore for whole blood mode.(Commented on 9/5/2019 as it is handling in application side)


4/6/2019:
1.In ZAP sequence, After completion of zap 2sec Delay is added.(Reset issue)

5/6/2019: 
1.mixing time is configuared in UI.

6/6/2019:
While moving home direction,sCurve is applyed for sample motor.

7/6/2019:
1.Tube voluemetric sequence is reverted back.
2.In shutdown sequence, bath filling volum is changed to 4ml from 2ml.
  -->Released on Jun 7th 2019.
 
 10/6/2019:
1.Error set and clear command for WASTE BIN FULL i now sending to sitara.

14/6/2019:
	In ZAP section, Turn Off the ZAP immediatly but turn off the WBC and RBC line after 2sec.

 20/6/2019:
 1. Avg is removed for HGB data.l
 26/6/2019:
 1.HGB light OFF and ON is added for counting sequence.
 
 2/8/2019:
 1.Drop let sucking is added after dispensing diluent for pre-diluent in pre-diluent mode.
 
--------->Released on 6th Aug 2019:
 8/8/2019:
 in Shutdown sequence:
 1.aspiration will happen only once(2ml) for both baths.
 2.extra 2ml is added to each bath to fill the bath completly. 
 
 19/8/2019:
 In CPU2:
 in RBC algo moving window avg is changed to 3 from 8.
 and considering all the pulses not alternative pulses.
 
 -------->Released on 21st Aug 2019:
 
	
11/9/2019:
To Reduce the reagent concumption following sequence modified:
	1.wake up
	2.back flush
	3.In all start up sequences backflush sequence is removed.
	

31/10/2019:
1.Pressure error limits changed to (-12 -- +12) from (-10 -- +13).


21/11/2019:
1.Pressure sensore tube presence check.(Logic changed)

22/11/2019:
1.In pre-diluent mode, Automatic dispensation error resolved.
------------------------------
16/12/2019:
1.In CPU2, if the first sensor is not enabled for 10sec system should continue the sequence.

17/12/2019:
1.In start up,sequence abortion is added if the waste bin is full.(Bug ID:409)

18/12/2019:
1.flush aperture sequence changed.(Bug ID:407)
	
 	 
 23/12/2019:
 1.Sequence change for Whole blood mode to reduce sequence time to 62sec from 64.3sec.
 -
 -
 -
 Done.
 
 26/12/2019:
 CPU2:
 In RBC algorithm, switch case is removed(Counting all pulses with in the range)
 -------------------->Released Test Binary
 
 3/1/2020:
 1.In CPU2:
 	WBC,RBC Early and Late counting error added by using flow calibration values.()
 2.Min. Acquisition time changed to 5sec from 3sec.
   
 8/1/2020:
 1.Diluation factors are fine tuned for pre-dilute mode and now results are same in pre-dilute and WBM.
 2.RBC bath getting drain before count start in strat-up.(Resolved)
 3.In Flush aperture sequence, high pressure was observed at the end of the sequence.(Resolved)
 
 25/2/2020:
 1. Whenever waste motor moving home for disposal,open V7 without checking pressure.
 
 
/*
 * File: BloodCellCounter.h
 *
 * Code generated for Simulink model 'BloodCellCounter'.
 *
 * Model version                  : 1.5294
 * Simulink Coder version         : 8.4 (R2013a) 13-Feb-2013
 * TLC version                    : 8.4 (Jan 19 2013)
 * C/C++ source code generated on : Thu Nov 26 15:27:02 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. ROM efficiency
 *    3. RAM efficiency
 *    4. Traceability
 *    5. Safety precaution
 *    6. Debugging
 *    7. MISRA-C:2004 guidelines
 * Validation result: Passed (12), Warnings (5), Error (0)
 */

#ifndef RTW_HEADER_BloodCellCounter_h_
#define RTW_HEADER_BloodCellCounter_h_
#ifndef BloodCellCounter_COMMON_INCLUDES_
# define BloodCellCounter_COMMON_INCLUDES_
#include <stddef.h>
#include <string.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* BloodCellCounter_COMMON_INCLUDES_ */

#include "BloodCellCounter_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals and states (auto storage) for system '<Root>' */
typedef struct {
  real_T Count_Lyse;                   /* '<S3>/Chart' */
  real_T SleepCounter;                 /* '<S3>/Chart' */
  real_T Clean_Count;                  /* '<S3>/Chart' */
  real_T CountTime_Delay;              /* '<S3>/Chart' */
  real_T CleanCheckEn;                 /* '<S3>/Chart' */
  real_T PulseCount;                   /* '<S3>/Chart' */
  uint32_T FrequencyWasteSyringe;      /* '<S3>/Chart' */
  uint32_T FrequencyDiluentSyringe;    /* '<S3>/Chart' */
  uint32_T FrequencyYaxis;             /* '<S3>/Chart' */
  uint32_T FrequencyLyseSyringe;       /* '<S3>/Chart' */
  uint32_T FrequencyXaxis;             /* '<S3>/Chart' */
  uint32_T CurrPosStatus;              /* '<S3>/Chart' */
  uint32_T Delay_time;                 /* '<S3>/Chart' */
  uint32_T WasteTime;                  /* '<S3>/Chart' */
  uint32_T HomeZoneFlag;               /* '<S3>/Chart' */
  uint32_T PressureMon;                /* '<S3>/Chart' */
  struct {
    uint_T V1Flag:1;                   /* '<S3>/Chart' */
  } bitsForTID0;

  int16_T DisplacementYaxis;           /* '<S3>/Chart' */
  int16_T DisplacementWasteSyringe;    /* '<S3>/Chart' */
  int16_T DisplacementLyseSyringe;     /* '<S3>/Chart' */
  int16_T DisplacementXaxis;           /* '<S3>/Chart' */
  int16_T DisplacementDiluentSyringe;  /* '<S3>/Chart' */
  uint16_T Drain_time;                 /* '<S3>/Chart' */
  uint8_T is_active_c1_BloodCellCounter;/* '<S3>/Chart' */
  uint8_T is_c1_BloodCellCounter;      /* '<S3>/Chart' */
  uint8_T is_HOMEMotors;               /* '<S3>/Chart' */
  uint8_T is_QUALITY_CONTROL;          /* '<S3>/Chart' */
  uint8_T is_PREDILUENTMODE;           /* '<S3>/Chart' */
  uint8_T is_SERVICE_HANDLING;         /* '<S3>/Chart' */
  uint8_T is_STARTUP_HMODE;            /* '<S3>/Chart' */
  uint8_T is_PRIME_LYSE;               /* '<S3>/Chart' */
  uint8_T is_EZ_CLEANING;              /* '<S3>/Chart' */
  uint8_T is_VALUMETRIC_CHECK;         /* '<S3>/Chart' */
  uint8_T is_PRIME_RINSE;              /* '<S3>/Chart' */
  uint8_T is_HOME_YAXIS;               /* '<S3>/Chart' */
  uint8_T is_PRIME_ALL_NEW;            /* '<S3>/Chart' */
  uint8_T is_PRIME_DILUENT;            /* '<S3>/Chart' */
  uint8_T is_HOME_XAXIS;               /* '<S3>/Chart' */
  uint8_T is_WBCRBC_COUNTING;          /* '<S3>/Chart' */
  uint8_T is_HOME_DILUENT;             /* '<S3>/Chart' */
  uint8_T is_BACKFLUSH;                /* '<S3>/Chart' */
  uint8_T is_HOME_SAMPLE;              /* '<S3>/Chart' */
  uint8_T is_DRAIN_BATHS;              /* '<S3>/Chart' */
  uint8_T is_HOME_WASTE;               /* '<S3>/Chart' */
  uint8_T is_SleepSequence;            /* '<S3>/Chart' */
  uint8_T is_GlasstubeCleaning;        /* '<S3>/Chart' */
  uint8_T is_CLEAN_BATHS;              /* '<S3>/Chart' */
  uint8_T is_BATH_FILL;                /* '<S3>/Chart' */
  uint8_T is_CountFail;                /* '<S3>/Chart' */
  uint8_T is_CALIBRATION_HANDLING;     /* '<S3>/Chart' */
  uint8_T is_PATIENT_HANDLING;         /* '<S3>/Chart' */
  uint8_T is_WHOLE_BLOOD_MODE;         /* '<S3>/Chart' */
  uint8_T is_WHOLE_BLOOD_MODE_k;       /* '<S3>/Chart' */
  uint8_T is_PREDILUENTMODE_o;         /* '<S3>/Chart' */
  boolean_T Valve_4;                   /* '<S3>/Chart' */
  boolean_T Valve_5;                   /* '<S3>/Chart' */
  boolean_T Valve_1;                   /* '<S3>/Chart' */
  boolean_T Valve_7;                   /* '<S3>/Chart' */
  boolean_T Valve_2;                   /* '<S3>/Chart' */
  boolean_T Valve_9;                   /* '<S3>/Chart' */
  boolean_T Valve_10;                  /* '<S3>/Chart' */
  boolean_T Valve_11;                  /* '<S3>/Chart' */
  boolean_T Valve_3;                   /* '<S3>/Chart' */
  boolean_T Valve_6;                   /* '<S3>/Chart' */
  boolean_T Valve_8;                   /* '<S3>/Chart' */
  boolean_T Valve_12;                  /* '<S3>/Chart' */
  boolean_T Valve_13;                  /* '<S3>/Chart' */
  boolean_T Valve_14;                  /* '<S3>/Chart' */
  boolean_T Valve_15;                  /* '<S3>/Chart' */
  boolean_T Valve_16;                  /* '<S3>/Chart' */
} D_Work_BloodCellCounter_T;

/* External inputs (root inport signals with auto storage) */
typedef struct {
  boolean_T LogicEnable;               /* '<Root>/LogicEnable' */
  uint16_T StopMotorYaxis;             /* '<Root>/StopMotorYaxis' */
  uint16_T StopMotorXaxis;             /* '<Root>/StopMotorXaxis' */
  uint16_T StopMotorLyseSyringe;       /* '<Root>/StopMotorLyseSyringe' */
  uint16_T StopMotorWasteSyringe;      /* '<Root>/StopMotorWasteSyringe' */
  uint16_T StopMotorDiluentSyringe;    /* '<Root>/StopMotorDiluentSyringe' */
  uint8_T CountStop;                   /* '<Root>/CountStop' */
  boolean_T ValveOpenStart;            /* '<Root>/ValveOpenStart' */
  boolean_T StartAspiration;           /* '<Root>/StartAspiration' */
  boolean_T ZapComplete;               /* '<Root>/ZapComplete' */
  uint16_T PopUpCmd;                   /* '<Root>/PopUpCmd' */
  uint16_T MajorCmd;                   /* '<Root>/MajorCmd' */
  uint16_T MinorCmd;                   /* '<Root>/MinorCmd' */
  uint16_T InBubblingFrequency[3];     /* '<Root>/InBubblingFrequency' */
  boolean_T HomeStatus[5];             /* '<Root>/HomeStatus' */
  uint32_T Pressure;                   /* '<Root>/Pressure' */
  int16_T Delay_BeforeCount;           /* '<Root>/Delay_BeforeCount' */
} ExternalInputs_BloodCellCount_T;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  boolean_T Direction[5];              /* '<Root>/Direction' */
  int16_T Displacement[5];             /* '<Root>/Displacement  ' */
  uint8_T ValveStatus_1[8];            /* '<Root>/ValveStatus_1' */
  uint8_T ValveStatus_2[8];            /* '<Root>/ValveStatus_2' */
  uint16_T CycleFlag;                  /* '<Root>/CycleFlag' */
  eMBDstates STATE;                    /* '<Root>/STATE' */
  uint32_T Frequency[5];               /* '<Root>/Frequency' */
  uint8_T CountStart;                  /* '<Root>/CountStart' */
  boolean_T X_AxisHP;                  /* '<Root>/X_AxisHP' */
  boolean_T PowerOn_Count;             /* '<Root>/PowerOn_Count' */
  boolean_T StartPollAspirationGpio;   /* '<Root>/StartPollAspirationGpio' */
  boolean_T HPMotorCheck[5];           /* '<Root>/HPMotorCheck' */
  uint8_T XaxisPosition;               /* '<Root>/XaxisPosition' */
} ExternalOutputs_BloodCellCoun_T;

/* Real-time Model Data Structure */
struct tag_RTM_BloodCellCounter_T {
  const char_T * volatile errorStatus;
};

/* Block signals and states (auto storage) */
extern D_Work_BloodCellCounter_T BloodCellCounter_DWork;

/* External inputs (root inport signals with auto storage) */
extern ExternalInputs_BloodCellCount_T BloodCellCounter_U;

/* External outputs (root outports fed by signals with auto storage) */
extern ExternalOutputs_BloodCellCoun_T BloodCellCounter_Y;

/*
 * Exported Global Parameters
 *
 * Note: Exported global parameters are tunable parameters with an exported
 * global storage class designation.  Code generation will declare the memory for
 * these parameters and exports their symbols.
 *
 */
extern const uint32_T BUBBLE_ONT;      /* Variable: BUBBLE_ONT
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T BUBBLE_PRD;      /* Variable: BUBBLE_PRD
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Bubble_Delay;    /* Variable: Bubble_Delay
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_FIFTYSIX;      /* Variable: C_FIFTYSIX
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_FIVE;          /* Variable: C_FIVE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_FOUR;          /* Variable: C_FOUR
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_ONE;           /* Variable: C_ONE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_ONESIXTYSIX;   /* Variable: C_ONESIXTYSIX
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_SEVEN;         /* Variable: C_SEVEN
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_SIX;           /* Variable: C_SIX
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_THREE;         /* Variable: C_THREE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_TWO;           /* Variable: C_TWO
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_VALVETIME;     /* Variable: C_VALVETIME
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_ZERO;          /* Variable: C_ZERO
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T C_ZEROPressure;  /* Variable: C_ZEROPressure
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_BUBBLE_WBC;/* Variable: Delay_BUBBLE_WBC
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_Dispensation_RBC;/* Variable: Delay_Dispensation_RBC
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const uint32_T Delay_Dispensation_WBC;/* Variable: Delay_Dispensation_WBC
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const uint32_T Delay_Drain;     /* Variable: Delay_Drain
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_EZ;        /* Variable: Delay_EZ
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_EZDisp;    /* Variable: Delay_EZDisp
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_PreDilDisp;/* Variable: Delay_PreDilDisp
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_Pressure;  /* Variable: Delay_Pressure
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_Prime;     /* Variable: Delay_Prime
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_TubeClean; /* Variable: Delay_TubeClean
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_V1;        /* Variable: Delay_V1
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_V2;        /* Variable: Delay_V2
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_V3;        /* Variable: Delay_V3
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_V7;        /* Variable: Delay_V7
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Delay_V8;        /* Variable: Delay_V8
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T K_DelayV7_Backflush;/* Variable: K_DelayV7_Backflush
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint32_T K_Delay_B4Count; /* Variable: K_Delay_B4Count
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T K_Delay_Firstphase;/* Variable: K_Delay_Firstphase
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint32_T K_Delay_HGBACQ;  /* Variable: K_Delay_HGBACQ
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T K_Delay_RBC;     /* Variable: K_Delay_RBC
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T K_Delay_Secondphase;/* Variable: K_Delay_Secondphase
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint32_T K_Delay_WBC;     /* Variable: K_Delay_WBC
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T K_Drain_Time;    /* Variable: K_Drain_Time
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T K_SLEEP_LIMIT;   /* Variable: K_SLEEP_LIMIT
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Valve_12_Delay;  /* Variable: Valve_12_Delay
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Valve_1_Delay;   /* Variable: Valve_1_Delay
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Valve_2_Delay;   /* Variable: Valve_2_Delay
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Valve_4_6_Delay; /* Variable: Valve_4_6_Delay
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint32_T Valve_7_Delay;   /* Variable: Valve_7_Delay
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T C_STATE38;        /* Variable: C_STATE38
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_100per_down_Diluent;/* Variable: K_100per_down_Diluent
                                            * Referenced by: '<S3>/Chart'
                                            */
extern const int16_T K_100per_down_LyseSample;/* Variable: K_100per_down_LyseSample
                                               * Referenced by: '<S3>/Chart'
                                               */
extern const int16_T K_100per_down_Y;  /* Variable: K_100per_down_Y
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_100per_down_waste;/* Variable: K_100per_down_waste
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_100per_left_X;  /* Variable: K_100per_left_X
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_100per_right_X; /* Variable: K_100per_right_X
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_100per_up_LyseSample;/* Variable: K_100per_up_LyseSample
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const int16_T K_100per_up_Y;    /* Variable: K_100per_up_Y
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_100per_up_waste;/* Variable: K_100per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_10mm_Down_Diluent;/* Variable: K_10mm_Down_Diluent
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_10per_down_Diluent;/* Variable: K_10per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_10per_down_waste;/* Variable: K_10per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_10per_up_Diluent;/* Variable: K_10per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_10per_up_waste; /* Variable: K_10per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_12per_up_Diluent;/* Variable: K_12per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_15per_down_Diluent;/* Variable: K_15per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_15per_down_waste;/* Variable: K_15per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_15per_right_X;  /* Variable: K_15per_right_X
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_16mm_Diluent_up;/* Variable: K_16mm_Diluent_up
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_16per_down_Diluent;/* Variable: K_16per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_17per_up_Diluent;/* Variable: K_17per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_18per_down_Diluent;/* Variable: K_18per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_18per_down_waste;/* Variable: K_18per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_18per_up_Diluent;/* Variable: K_18per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_18per_up_waste; /* Variable: K_18per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_19per_down_Diluent;/* Variable: K_19per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_20per_down_Diluent;/* Variable: K_20per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_20per_down_waste;/* Variable: K_20per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_20per_up_Diluent;/* Variable: K_20per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_22per_down_Diluent;/* Variable: K_22per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_22per_up_Diluent;/* Variable: K_22per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_24per_up_Diluent;/* Variable: K_24per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_25per_down_Diluent;/* Variable: K_25per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_25per_down_waste;/* Variable: K_25per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_25per_up_Diluent;/* Variable: K_25per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_26per_up_Diluent;/* Variable: K_26per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_28per_up_waste; /* Variable: K_28per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_2per_down_Diluent;/* Variable: K_2per_down_Diluent
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_2per_up_Diluent;/* Variable: K_2per_up_Diluent
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_30mm_down_Diluent;/* Variable: K_30mm_down_Diluent
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_30per_down_Y;   /* Variable: K_30per_down_Y
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_30per_up_Y;     /* Variable: K_30per_up_Y
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_30per_up_waste; /* Variable: K_30per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_32per_down_waste;/* Variable: K_32per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_33per_up_Diluent;/* Variable: K_33per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_39per_down_waste;/* Variable: K_39per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_39per_up_waste; /* Variable: K_39per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_3mm_Extra_down_Diluent;/* Variable: K_3mm_Extra_down_Diluent
                                               * Referenced by: '<S3>/Chart'
                                               */
extern const int16_T K_3per_down_waste;/* Variable: K_3per_down_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_3per_up_Diluent;/* Variable: K_3per_up_Diluent
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_3per_up_waste;  /* Variable: K_3per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_40per_down_Diluent;/* Variable: K_40per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_40per_down_LyseSample;/* Variable: K_40per_down_LyseSample
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const int16_T K_40per_down_waste;/* Variable: K_40per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_40per_up_LyseSample;/* Variable: K_40per_up_LyseSample
                                            * Referenced by: '<S3>/Chart'
                                            */
extern const int16_T K_41per_down_waste;/* Variable: K_41per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_41per_up_waste; /* Variable: K_41per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_42per_up_waste; /* Variable: K_42per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_43per_up_waste; /* Variable: K_43per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_4per_up_Diluent;/* Variable: K_4per_up_Diluent
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_50per_down_waste;/* Variable: K_50per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_50per_left_X;   /* Variable: K_50per_left_X
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_50per_right_X;  /* Variable: K_50per_right_X
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_50per_up_Diluent;/* Variable: K_50per_up_Diluent
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_50per_up_waste; /* Variable: K_50per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_55mm_up_waste;  /* Variable: K_55mm_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_5mm_Down_Waste; /* Variable: K_5mm_Down_Waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_5mm_Down_Y;     /* Variable: K_5mm_Down_Y
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_5mm_up_Waste;   /* Variable: K_5mm_up_Waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_5per_up_Diluent;/* Variable: K_5per_up_Diluent
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_5per_up_waste;  /* Variable: K_5per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_66per_up_waste; /* Variable: K_66per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_6per_up_waste;  /* Variable: K_6per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_75per_down_Diluent;/* Variable: K_75per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_75per_down_Y;   /* Variable: K_75per_down_Y
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_76per_down_Diluent;/* Variable: K_76per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_80per_down_Diluent;/* Variable: K_80per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_80per_down_waste;/* Variable: K_80per_down_waste
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_80per_up_waste; /* Variable: K_80per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_84per_up_waste; /* Variable: K_84per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_86per_up_waste; /* Variable: K_86per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_88per_down_Diluent;/* Variable: K_88per_down_Diluent
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_88per_up_waste; /* Variable: K_88per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_8per_up_Diluent;/* Variable: K_8per_up_Diluent
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_8per_up_waste;  /* Variable: K_8per_up_waste
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_9per_up_Diluent;/* Variable: K_9per_up_Diluent
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_Add_Diluent_ToRBC;/* Variable: K_Add_Diluent_ToRBC
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_AspToRBC_X;     /* Variable: K_AspToRBC_X
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_Aspiration_down_Y;/* Variable: K_Aspiration_down_Y
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_Bubbling_up_Waste;/* Variable: K_Bubbling_up_Waste
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_Cleanser_Asp_down_Diluent;/* Variable: K_Cleanser_Asp_down_Diluent
                                                  * Referenced by: '<S3>/Chart'
                                                  */
extern const int16_T K_Count_Time_Vacuum_waste;/* Variable: K_Count_Time_Vacuum_waste
                                                * Referenced by: '<S3>/Chart'
                                                */
extern const int16_T K_Diluent_up_For_WBCRatio;/* Variable: K_Diluent_up_For_WBCRatio
                                                * Referenced by: '<S3>/Chart'
                                                */
extern const int16_T K_DisplacementYaxis_Home;/* Variable: K_DisplacementYaxis_Home
                                               * Referenced by: '<S3>/Chart'
                                               */
extern const int16_T K_Displacement_Home;/* Variable: K_Displacement_Home
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_FlangeOut_right_X;/* Variable: K_FlangeOut_right_X
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_FullStroke_down_Diluent;/* Variable: K_FullStroke_down_Diluent
                                                * Referenced by: '<S3>/Chart'
                                                */
extern const int16_T K_FullStroke_down_LyseSample;/* Variable: K_FullStroke_down_LyseSample
                                                   * Referenced by: '<S3>/Chart'
                                                   */
extern const int16_T K_FullStroke_up_Diluent;/* Variable: K_FullStroke_up_Diluent
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const int16_T K_FullStroke_up_LyseSample;/* Variable: K_FullStroke_up_LyseSample
                                                 * Referenced by: '<S3>/Chart'
                                                 */
extern const int16_T K_Pre_Dil_Asp_down;/* Variable: K_Pre_Dil_Asp_down
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_Pre_Dil_WBC_Probe;/* Variable: K_Pre_Dil_WBC_Probe
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const int16_T K_Pre_Dil_WBC_hose;/* Variable: K_Pre_Dil_WBC_hose
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const int16_T K_Pre_Diluent_Dis;/* Variable: K_Pre_Diluent_Dis
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_RBCToAsp_X;     /* Variable: K_RBCToAsp_X
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_RBCToWBC_X;     /* Variable: K_RBCToWBC_X
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_TubeCleaning_waste;/* Variable: K_TubeCleaning_waste
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_WBC_Hose_up_Diluent;/* Variable: K_WBC_Hose_up_Diluent
                                            * Referenced by: '<S3>/Chart'
                                            */
extern const int16_T K_WBC_Probe_dis_up_Diluent;/* Variable: K_WBC_Probe_dis_up_Diluent
                                                 * Referenced by: '<S3>/Chart'
                                                 */
extern const int16_T K_backFlush_up_waste;/* Variable: K_backFlush_up_waste
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const int16_T K_down_Y_To_WBC;  /* Variable: K_down_Y_To_WBC
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int16_T K_extramm_right_X;/* Variable: K_extramm_right_X
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T ASP_COMPLETE;    /* Variable: ASP_COMPLETE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T ASP_READY;       /* Variable: ASP_READY
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T C_COMPLETE;      /* Variable: C_COMPLETE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T C_IDLE;          /* Variable: C_IDLE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T C_ZEROuint16;    /* Variable: C_ZEROuint16
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T DISP_COMPLETE;   /* Variable: DISP_COMPLETE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T EZ_WAIT;         /* Variable: EZ_WAIT
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T HGB_BLANK_ACQSTART;/* Variable: HGB_BLANK_ACQSTART
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T HGB_SAMPLE_ACQSTART;/* Variable: HGB_SAMPLE_ACQSTART
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_ABORT_BY_USER; /* Variable: K_ABORT_BY_USER
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_ABORT_BY_USER_COMPLETE;/* Variable: K_ABORT_BY_USER_COMPLETE
                                                * Referenced by: '<S3>/Chart'
                                                */
extern const uint16_T K_ABORT_MAJORCMD;/* Variable: K_ABORT_MAJORCMD
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_ALL_MOTOR_DIL_CHECK;/* Variable: K_ALL_MOTOR_DIL_CHECK
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_ALL_MOTOR_DIL_CHECK_COMPLETE;/* Variable: K_ALL_MOTOR_DIL_CHECK_COMPLETE
                                                      * Referenced by: '<S3>/Chart'
                                                      */
extern const uint16_T K_ALL_MOTOR_LYSE_CHECK;/* Variable: K_ALL_MOTOR_LYSE_CHECK
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const uint16_T K_ALL_MOTOR_LYSE_CHECK_COMPLETE;/* Variable: K_ALL_MOTOR_LYSE_CHECK_COMPLETE
                                                       * Referenced by: '<S3>/Chart'
                                                       */
extern const uint16_T K_ALL_MOTOR_WASTE_CHECK;/* Variable: K_ALL_MOTOR_WASTE_CHECK
                                               * Referenced by: '<S3>/Chart'
                                               */
extern const uint16_T K_ALL_MOTOR_WASTE_CHECK_COMPLETE;/* Variable: K_ALL_MOTOR_WASTE_CHECK_COMPLETE
                                                        * Referenced by: '<S3>/Chart'
                                                        */
extern const uint16_T K_ALL_MOTOR_X_CHECK;/* Variable: K_ALL_MOTOR_X_CHECK
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_ALL_MOTOR_X_CHECK_COMPLETE;/* Variable: K_ALL_MOTOR_X_CHECK_COMPLETE
                                                    * Referenced by: '<S3>/Chart'
                                                    */
extern const uint16_T K_ALL_MOTOR_Y_CHECK;/* Variable: K_ALL_MOTOR_Y_CHECK
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_ALL_MOTOR_Y_CHECK_COMPLETE;/* Variable: K_ALL_MOTOR_Y_CHECK_COMPLETE
                                                    * Referenced by: '<S3>/Chart'
                                                    */
extern const uint16_T K_AUTO_CALIBRATION;/* Variable: K_AUTO_CALIBRATION
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_AUTO_CALIBRATION_COMPLETE;/* Variable: K_AUTO_CALIBRATION_COMPLETE
                                                   * Referenced by: '<S3>/Chart'
                                                   */
extern const uint16_T K_BACKFLUSH;     /* Variable: K_BACKFLUSH
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_BACKFLUSH_COMPLETE;/* Variable: K_BACKFLUSH_COMPLETE
                                            * Referenced by: '<S3>/Chart'
                                            */
extern const uint16_T K_BATH_FILLING;  /* Variable: K_BATH_FILLING
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_BATH_FILLING_COMPLETE;/* Variable: K_BATH_FILLING_COMPLETE
                                               * Referenced by: '<S3>/Chart'
                                               */
extern const uint16_T K_BODY_FLUID_COMPLETE;/* Variable: K_BODY_FLUID_COMPLETE
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_BODY_FLUID_MODE;/* Variable: K_BODY_FLUID_MODE
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const uint16_T K_CALIBRATION_HANDLING;/* Variable: K_CALIBRATION_HANDLING
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const uint16_T K_CLEAN_BATHS;   /* Variable: K_CLEAN_BATHS
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_CLEAN_BATHS_COMPLETE;/* Variable: K_CLEAN_BATHS_COMPLETE
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const uint16_T K_COM_CALIBRATION_PLT;/* Variable: K_COM_CALIBRATION_PLT
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_COM_CALIBRATION_PLT_COMPLETE;/* Variable: K_COM_CALIBRATION_PLT_COMPLETE
                                                      * Referenced by: '<S3>/Chart'
                                                      */
extern const uint16_T K_COM_CALIBRATION_RBC;/* Variable: K_COM_CALIBRATION_RBC
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_COM_CALIBRATION_RBC_COMPLETE;/* Variable: K_COM_CALIBRATION_RBC_COMPLETE
                                                      * Referenced by: '<S3>/Chart'
                                                      */
extern const uint16_T K_COM_CALIBRATION_WBC;/* Variable: K_COM_CALIBRATION_WBC
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_COM_CALIBRATION_WBC_COMPLETE;/* Variable: K_COM_CALIBRATION_WBC_COMPLETE
                                                      * Referenced by: '<S3>/Chart'
                                                      */
extern const uint16_T K_COUNTING_MODE; /* Variable: K_COUNTING_MODE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_COUNT_COMPLETE;/* Variable: K_COUNT_COMPLETE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_COUNT_FAILED;  /* Variable: K_COUNT_FAILED
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_COUNT_FAILED_COMPLTE;/* Variable: K_COUNT_FAILED_COMPLTE
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const uint16_T K_CYCLE_DEFAULT; /* Variable: K_CYCLE_DEFAULT
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_DEACTIVATE_COUNT;/* Variable: K_DEACTIVATE_COUNT
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_DEACTIVATE_COUNT_COMPLETE;/* Variable: K_DEACTIVATE_COUNT_COMPLETE
                                                   * Referenced by: '<S3>/Chart'
                                                   */
extern const uint16_T K_DISP_START;    /* Variable: K_DISP_START
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_DRAINBATHS_COMPLETE;/* Variable: K_DRAINBATHS_COMPLETE
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_DRAIN_ALL;     /* Variable: K_DRAIN_ALL
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_DRAIN_ALL_COMPLETE;/* Variable: K_DRAIN_ALL_COMPLETE
                                            * Referenced by: '<S3>/Chart'
                                            */
extern const uint16_T K_DRAIN_BATHS;   /* Variable: K_DRAIN_BATHS
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_ENTER_INTO_SLEEP;/* Variable: K_ENTER_INTO_SLEEP
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_ENTER_INTO_SLEEP_COMPLETE;/* Variable: K_ENTER_INTO_SLEEP_COMPLETE
                                                   * Referenced by: '<S3>/Chart'
                                                   */
extern const uint16_T K_EZ_CLEANER;    /* Variable: K_EZ_CLEANER
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_EZ_COMPLETE;   /* Variable: K_EZ_COMPLETE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_FACTORY_WBCRBC_COUNT;/* Variable: K_FACTORY_WBCRBC_COUNT
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const uint16_T K_FACTORY_WBCRBC_COUNT_COMPLETE;/* Variable: K_FACTORY_WBCRBC_COUNT_COMPLETE
                                                       * Referenced by: '<S3>/Chart'
                                                       */
extern const uint16_T K_FLOW_RINSE_PRIME;/* Variable: K_FLOW_RINSE_PRIME
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_FLOW_RINSE_PRIME_COMPLETE;/* Variable: K_FLOW_RINSE_PRIME_COMPLETE
                                                   * Referenced by: '<S3>/Chart'
                                                   */
extern const uint16_T K_GLASSTUBE_CLEANING;/* Variable: K_GLASSTUBE_CLEANING
                                            * Referenced by: '<S3>/Chart'
                                            */
extern const uint16_T K_GLASSTUBE_CLEANING_COMPLETE;/* Variable: K_GLASSTUBE_CLEANING_COMPLETE
                                                     * Referenced by: '<S3>/Chart'
                                                     */
extern const uint16_T K_GLASSTUBE_FILLING;/* Variable: K_GLASSTUBE_FILLING
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_GLASSTUBE_FILLING_COMPLTE;/* Variable: K_GLASSTUBE_FILLING_COMPLTE
                                                   * Referenced by: '<S3>/Chart'
                                                   */
extern const uint16_T K_HOME_COMPLETE; /* Variable: K_HOME_COMPLETE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_HOME_DILUENT_COMPLETE;/* Variable: K_HOME_DILUENT_COMPLETE
                                               * Referenced by: '<S3>/Chart'
                                               */
extern const uint16_T K_HOME_DILUENT_SYRINGE;/* Variable: K_HOME_DILUENT_SYRINGE
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const uint16_T K_HOME_SAMPLE_COMPLETE;/* Variable: K_HOME_SAMPLE_COMPLETE
                                              * Referenced by: '<S3>/Chart'
                                              */
extern const uint16_T K_HOME_SAMPLE_SYRINGE;/* Variable: K_HOME_SAMPLE_SYRINGE
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_HOME_WASTE_COMPLETE;/* Variable: K_HOME_WASTE_COMPLETE
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_HOME_WASTE_SYRINGE;/* Variable: K_HOME_WASTE_SYRINGE
                                            * Referenced by: '<S3>/Chart'
                                            */
extern const uint16_T K_HOME_XAXIS;    /* Variable: K_HOME_XAXIS
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_HOME_XAXIS_COMPLETE;/* Variable: K_HOME_XAXIS_COMPLETE
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_HOME_YAXIS;    /* Variable: K_HOME_YAXIS
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_HOME_YAXIS_COMPLETE;/* Variable: K_HOME_YAXIS_COMPLETE
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_HYPO_CLEANER_FAIL;/* Variable: K_HYPO_CLEANER_FAIL
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_HYPO_CLEANER_FAIL_COMPLTE;/* Variable: K_HYPO_CLEANER_FAIL_COMPLTE
                                                   * Referenced by: '<S3>/Chart'
                                                   */
extern const uint16_T K_PATIENT_HANDLING;/* Variable: K_PATIENT_HANDLING
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_PREDIL_START;  /* Variable: K_PREDIL_START
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_PRE_DILUENT_MODE;/* Variable: K_PRE_DILUENT_MODE
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_PRE_DILUENT_MODE_COMPLETE;/* Variable: K_PRE_DILUENT_MODE_COMPLETE
                                                   * Referenced by: '<S3>/Chart'
                                                   */
extern const uint16_T K_PRIMEDIL_COMPLETE;/* Variable: K_PRIMEDIL_COMPLETE
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_PRIMELYS_COMPLETE;/* Variable: K_PRIMELYS_COMPLETE
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_PRIMERIN_COMPLETE;/* Variable: K_PRIMERIN_COMPLETE
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_PRIME_ALL;     /* Variable: K_PRIME_ALL
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_PRIME_ALL_COMPLETE;/* Variable: K_PRIME_ALL_COMPLETE
                                            * Referenced by: '<S3>/Chart'
                                            */
extern const uint16_T K_PRIME_DILUENT; /* Variable: K_PRIME_DILUENT
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_PRIME_LYSE;    /* Variable: K_PRIME_LYSE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_PRIME_RINSE;   /* Variable: K_PRIME_RINSE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_QUALITY_CONTROL;/* Variable: K_QUALITY_CONTROL
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const uint16_T K_QUAL_CONTROL_BF;/* Variable: K_QUAL_CONTROL_BF
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const uint16_T K_QUAL_CONTROL_BF_COMPLETE;/* Variable: K_QUAL_CONTROL_BF_COMPLETE
                                                  * Referenced by: '<S3>/Chart'
                                                  */
extern const uint16_T K_QUAL_CONTROL_WB;/* Variable: K_QUAL_CONTROL_WB
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const uint16_T K_QUAL_CONTROL_WB_COMPLETE;/* Variable: K_QUAL_CONTROL_WB_COMPLETE
                                                  * Referenced by: '<S3>/Chart'
                                                  */
extern const uint16_T K_SERVICE_HANDLING;/* Variable: K_SERVICE_HANDLING
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_SETTING_MAJORCMD;/* Variable: K_SETTING_MAJORCMD
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_SHUTDOWN;      /* Variable: K_SHUTDOWN
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_SHUTDOWN_COMPLETE;/* Variable: K_SHUTDOWN_COMPLETE
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_SHUTDOWN_HANDLER;/* Variable: K_SHUTDOWN_HANDLER
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_SLEEP;         /* Variable: K_SLEEP
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_SLEEP_MODE_COMPLETE;/* Variable: K_SLEEP_MODE_COMPLETE
                                             * Referenced by: '<S3>/Chart'
                                             */
extern const uint16_T K_STARTUP_BACKFLUSH;/* Variable: K_STARTUP_BACKFLUSH
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_STARTUP_COMPLETE;/* Variable: K_STARTUP_COMPLETE
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_STARTUP_FAILED;/* Variable: K_STARTUP_FAILED
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_STARTUP_FROM_SERVICE_SCREEN;/* Variable: K_STARTUP_FROM_SERVICE_SCREEN
                                                     * Referenced by: '<S3>/Chart'
                                                     */
extern const uint16_T K_STARTUP_HANDLER;/* Variable: K_STARTUP_HANDLER
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const uint16_T K_STARTUP_MODE;  /* Variable: K_STARTUP_MODE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_TOSHIP;        /* Variable: K_TOSHIP
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_TOSHIP_COMPLETE;/* Variable: K_TOSHIP_COMPLETE
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const uint16_T K_VOLUMETRIC_CHECK;/* Variable: K_VOLUMETRIC_CHECK
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_VOLUMETRIC_CHECK_COMPLETE;/* Variable: K_VOLUMETRIC_CHECK_COMPLETE
                                                   * Referenced by: '<S3>/Chart'
                                                   */
extern const uint16_T K_WAKEUP;        /* Variable: K_WAKEUP
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_WAKEUP_COMPLETE;/* Variable: K_WAKEUP_COMPLETE
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const uint16_T K_WBCRBC_COUNT_TIME;/* Variable: K_WBCRBC_COUNT_TIME
                                           * Referenced by: '<S3>/Chart'
                                           */
extern const uint16_T K_WBCRBC_COUNT_TIME_COMPLETE;/* Variable: K_WBCRBC_COUNT_TIME_COMPLETE
                                                    * Referenced by: '<S3>/Chart'
                                                    */
extern const uint16_T K_ZAPPING;       /* Variable: K_ZAPPING
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T K_ZAPPING_COMPLETE;/* Variable: K_ZAPPING_COMPLETE
                                          * Referenced by: '<S3>/Chart'
                                          */
extern const uint16_T K_ZAP_INTIATE;   /* Variable: K_ZAP_INTIATE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T NEEDLE_READY;    /* Variable: NEEDLE_READY
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint16_T ZAP_ACTIVE;      /* Variable: ZAP_ACTIVE
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const int8_T C_ZEROint8;        /* Variable: C_ZEROint8
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T Aspiration;       /* Variable: Aspiration
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T C_FOUR_uint8;     /* Variable: C_FOUR_uint8
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T C_ONE_uint8;      /* Variable: C_ONE_uint8
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T C_TWO_uint8;      /* Variable: C_TWO_uint8
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T C_ZERO_uint8;     /* Variable: C_ZERO_uint8
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T Invalid;          /* Variable: Invalid
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T K_CleanRepeat;    /* Variable: K_CleanRepeat
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T K_CountLyseRepeat;/* Variable: K_CountLyseRepeat
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T ProbeCleanDone;   /* Variable: ProbeCleanDone
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T RinseCount;       /* Variable: RinseCount
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T ToRBC;            /* Variable: ToRBC
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T ToWBC;            /* Variable: ToWBC
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const uint8_T Xreset;           /* Variable: Xreset
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const boolean_T HOME_CHECK_RESET;/* Variable: HOME_CHECK_RESET
                                         * Referenced by: '<S3>/Chart'
                                         */
extern const boolean_T HOME_CHECK_SET; /* Variable: HOME_CHECK_SET
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const boolean_T HP_DIS;         /* Variable: HP_DIS
                                        * Referenced by: '<S3>/Chart'
                                        */
extern const boolean_T HP_ENA;         /* Variable: HP_ENA
                                        * Referenced by: '<S3>/Chart'
                                        */

/* Model entry point functions */
extern void BloodCellCounter_initialize(void);
extern void BloodCellCounter_step(void);
extern void BloodCellCounter_terminate(void);

/* Real-time Model object */
extern RT_MODEL_BloodCellCounter_T *const BloodCellCounter_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'BloodCellCounter'
 * '<S1>'   : 'BloodCellCounter/ControlLogic'
 * '<S2>'   : 'BloodCellCounter/Model Info'
 * '<S3>'   : 'BloodCellCounter/ControlLogic/ControlLogicChart'
 * '<S4>'   : 'BloodCellCounter/ControlLogic/MotorControl'
 * '<S5>'   : 'BloodCellCounter/ControlLogic/ValveControl'
 * '<S6>'   : 'BloodCellCounter/ControlLogic/ControlLogicChart/Chart'
 * '<S7>'   : 'BloodCellCounter/ControlLogic/MotorControl/Compare To Zero'
 * '<S8>'   : 'BloodCellCounter/ControlLogic/MotorControl/Compare To Zero1'
 * '<S9>'   : 'BloodCellCounter/ControlLogic/MotorControl/Compare To Zero2'
 * '<S10>'  : 'BloodCellCounter/ControlLogic/MotorControl/Compare To Zero3'
 * '<S11>'  : 'BloodCellCounter/ControlLogic/MotorControl/Compare To Zero4'
 * '<S12>'  : 'BloodCellCounter/ControlLogic/ValveControl/ValveStatusALHA5'
 */

/*-
 * Requirements for '<Root>': BloodCellCounter
 */
#endif                                 /* RTW_HEADER_BloodCellCounter_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

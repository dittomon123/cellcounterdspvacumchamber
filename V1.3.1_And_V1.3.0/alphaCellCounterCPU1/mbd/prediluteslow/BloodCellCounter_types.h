/*
 * File: BloodCellCounter_types.h
 *
 * Code generated for Simulink model 'BloodCellCounter'.
 *
 * Model version                  : 1.5294
 * Simulink Coder version         : 8.4 (R2013a) 13-Feb-2013
 * TLC version                    : 8.4 (Jan 19 2013)
 * C/C++ source code generated on : Thu Nov 26 15:01:11 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. ROM efficiency
 *    3. RAM efficiency
 *    4. Traceability
 *    5. Safety precaution
 *    6. Debugging
 *    7. MISRA-C:2004 guidelines
 * Validation result: Passed (12), Warnings (5), Error (0)
 */

#ifndef RTW_HEADER_BloodCellCounter_types_h_
#define RTW_HEADER_BloodCellCounter_types_h_
#include "rtwtypes.h"
#ifndef _DEFINED_TYPEDEF_FOR_eMBDstates_
#define _DEFINED_TYPEDEF_FOR_eMBDstates_

typedef enum {
  eSTATE_INVALID = 0,                  /* Default value */
  eSEQUENCE_STARTED = 1,
  eREADY_FOR_DISPENSING = 2,
  eDISPENSE_COMPLETED = 3,
  eREADY_FOR_ASPIRATION = 4,
  eASPIRATION_COMPLETED_WB = 5,
  eASPIRATION_COMPLETED_MONITORING = 6,
  eDRAINING_BC_STARTED = 7,
  eDRAINING_BC_INTER_STARTED = 8,
  eDRAINING_BC_COMPLETED = 9,
  eWBC_BUBBLING_STARTED = 10,
  eWBC_BUBBLING_COMPLETED = 11,
  eFIRST_DILUTION_COMPLETED = 12,
  eRBC_DISPENSATION_COMPLETED = 13,
  eWBC_RBC_BUBBLING_STARTED = 14,
  eWBC_RBC_BUBBLING_INTER_STARTED = 15,
  eWBC_RBC_BUBBLING_COMPLETED = 16,
  eCOUNTING_STARTED = 17,
  eCOUNTING_COMPLETED = 18,
  eDRAINING_AC_STARTED = 19,
  eDRAINING_AC_INTER_STARTED = 20,
  eDRAINING_AC_COMPLETED = 21,
  eBACKFLUSH_STARTED = 22,
  eBACKFLUSH_COMPLETED = 23,
  eDRAINING_FINAL_STARTED = 24,
  eDRAINING_FINAL_INTER_STARTED = 25,
  eDRAINING_FINAL_COMPLETED = 26,
  eBATH_FILLING_COMPLETED = 27,
  eHGB_BLANK_ACQUISITION_START = 101,
  eHGB_SAMPLE_ACQUISITION_START = 102,
  eASPIRATION_KEY_PRESSED = 103,
  eMONITOR_COUNT_TIME_VACCUM = 104,
  eZAP_START = 201,
  eEZ_WAIT = 202,
  eSLEEP = 203,
  eABORT_PROCESS = 1000,
  eVOLUMETRIC_CHECK_START = 28,
  eVOLUMETRIC_CHECK_COMPLETE = 29,
  eDILUENT_BUBBLE_STARTED = 30,
  eLYSE_BUBBLE_STARTED = 31,
  ePREDIL_BUBBLE_STARTED = 32
} eMBDstates;

#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM_BloodCellCounter_T RT_MODEL_BloodCellCounter_T;

#endif                                 /* RTW_HEADER_BloodCellCounter_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

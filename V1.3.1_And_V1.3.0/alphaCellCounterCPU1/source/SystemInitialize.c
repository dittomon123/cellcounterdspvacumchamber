/******************************************************************************/
/*! \file SystemInitialize.c
 *  \brief System initialization
 *
 *  \b Description:
 *      System initializarion and main controlled logic from mbd module
 *      called here
 *      Also test standalone code for individual peripherals modules
 *
 *   $Version: $ 0.1
 *   $Date:    $ Apr-21-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_system_module
//! @{
//*****************************************************************************
#include "F28x_Project.h"
#include "MotorConfig.h"
#include "SystemInitialize.h"
#include "fpu_vector.h"
#include "InitGpio.h"


#include <time.h>	//SAM_SPI

/******************************************************************************/
/*!
 *
 * \def STANDALONE_MODULE_TEST
 * \def MOTOR_MOVEMENT_DEBUG_ENABLE
 * \def ALL_MOTORS
 * \brief Preprocessor definitions
 */
/******************************************************************************/
#define STANDALONE_MODULE_TEST				0
#define	MOTOR_MOVEMENT_DEBUG_ENABLE			0
#define ALL_MOTORS							5
//Enable this to enable Start Button function   //CRH_TEST
#define START_BUTTON_ENABLE					0
#define SDRAM_ADC_BUFFER_SIZE				(1048576 * 5)
//#define _ADC_RAW_DATA_IN_ASCII_       //QA_C
#define FOUR								4
#define Y_AXIS_ERROR_CHECK                  1
#define WASTE_ERROR_CHECK                   1
#define SAMPLE_ERROR_CHECK                  1
#define DILUENT_ERROR_CHECK                 1
#define X_AXIS_ERROR_CHECK                  1

bool_t bPressureMonitor = FALSE;    //PH_PRESSURE_TEST
uint16_t bStartADCPressure = 0;
uint16_t bEndADCPressure = 0;
uint16_t bMStopADCPressure = 0;

bool_t XPosMonReset = FALSE;
//Hari_satish_bubble test
bool_t g_bFirstMixingState  = FALSE;
bool_t g_bLyseMixingState  = FALSE;
bool_t g_bPreDilMixingState  = FALSE;
uint16_t MixingTime = 0;
uint16_t prePresssureValue = 0;
uint16_t u_n8ContinousRun = 0;
uint16_t u_n8ContinousRunDelay = 0;
extern uint16_t Counter;
extern uint16_t g_unSPIErrorCheckCounter;
extern bool_t g_bPressureCaliStart;
extern bool_t m_bSystemStatusFlag;
extern bool_t m_bZapInitiateFlag;
extern bool_t g_bLowVolumeDisp;
/******************************************************************************/
/*!
 *
 * \brief variable is used to store FW version control string
 * \var char Cpu1FirmwareVersion[]
 * \var char Cpu2FirmwareVersion[]
 */
/******************************************************************************/
#pragma DATA_SECTION(Cpu1FirmwareVersion,".RAMGS10_Cpu1IpcParam");
int Cpu1FirmwareVersion[03] = {1,3,2};

#pragma DATA_SECTION(Cpu2FirmwareVersion,".RAMGS10_Cpu1IpcParam");
int Cpu2FirmwareVersion[03] = {1,3,0};


uint32_t ulnActualPressureAdc = ZERO;

/******************************************************************************/
/*!
 * \var m_ucValveControlTime
 * \brief  This variable is to hold the valve in the excited state for
 * particular amount of time (VALVE_CONTROL_TIME)
 */
/******************************************************************************/
unsigned char m_ucValveControlTime = ZERO;

/******************************************************************************/
/*!
* \var PressureCheckCounter
* \brief  This variable is used as counter for pressure line check status
*/
/******************************************************************************/
unsigned char PressureCheckCounter = ZERO;

/******************************************************************************/
/*!
 * \var m_bVlaveCheckStatus
 * \brief  This variable is to check whether valve check command is received
 * from the processor
 */
/******************************************************************************/
bool_t m_bVlaveCheckStatus = FALSE;

/******************************************************************************/
/*!
 * \var m_ucValveNumber
 * \brief  This variable is stores the valve number during valve test
 * from the processor
 */
/******************************************************************************/
unsigned char m_ucValveNumber= ZERO;

/******************************************************************************/
/*!
 * \var gucWasteCounter
 * \brief  Waste bin full need to be checked at specific interval. This variable
 * holds and compares the time with WASTE_SESNE_INTERVAL
 */
/******************************************************************************/
unsigned char gucWasteCounter = ZERO;

/******************************************************************************/
/*!
 * \var gucWasteDetectCounter
 * \brief  Waste bin full need to be detected for 1 second continously.
 */
/******************************************************************************/
unsigned char gucWasteDetectCounter = ZERO;

/******************************************************************************/
/*!
 * \var valveStatus1
 * \brief  To Send first 8 Valve status to be controlled
 */
/******************************************************************************/
unsigned int valveStatus1 = ZERO;

/******************************************************************************/
/*!
 * \var valveStatus1
 * \brief  To Send first 8 Valve status to be controlled
 */
/******************************************************************************/
unsigned int valveStatus2 = ZERO;

/******************************************************************************/
/*!
 * \var g_bWasteDetectCheck
 * \brief  Waste bin monitoring need to be enabled by processor.
 * This varaible is enabled if the processor enables waste bin monitoring.
 */
/******************************************************************************/
bool_t g_bWasteDetectCheck = FALSE;

/******************************************************************************/
/*!
 * \var m_bWasteBinFull
 * \brief  Waste bin full stores the status whether the waste bin is full or empty
 */
/******************************************************************************/
bool_t m_bWasteBinFull = FALSE;

/******************************************************************************/
/*!
 * \var m_usnPressureAdc
 * \brief  variable stores the averaged pressure value from the ADC
 */
/******************************************************************************/
uint32_t m_usnPressureAdc = ZERO;

/******************************************************************************/
/*!
 * \var ADCcalZeroPressure
 * \brief  variable stores the pressure diff after pressure calibration
 */
/******************************************************************************/

int32_t sncalZeroPSI = ZERO;

/******************************************************************************/
/*!
 * \var m_usnTemperatureAdc
 * \brief  variable stores the averaged temperature value from the ADC
 */
/******************************************************************************/
uint32_t m_usnTemperatureAdc = ZERO;

/******************************************************************************/
/*!
 * \var m_usnHgbAdc
 * \brief  variable stores the averaged HgB value from the ADC
 */
/******************************************************************************/
uint32_t m_usnHgbAdc = ZERO;

/******************************************************************************/
/*!
 * \var m_usnHgbNewAdc
 * \brief  variable stores the averaged HgB value of current iteration from the ADC
 */
/******************************************************************************/
uint32_t m_usnHgbNewAdc = ZERO;

/******************************************************************************/
/*!
 * \var m_usnSense24Voltage
 * \brief  variable stores the averaged 24V value from the ADC
 */
/******************************************************************************/
uint32_t m_usnSense24Voltage = ZERO;

/******************************************************************************/
/*!
 * \var m_usnSense5Voltage
 * \brief  variable stores the averaged 5V value from the ADC
 */
/******************************************************************************/
uint32_t m_usnSense5Voltage = ZERO;

/******************************************************************************/
/*!
 * \var m_bPressureMonitor
 * \brief  variable to enable whether pressure to be monitored in any particular instance
 */
/******************************************************************************/
bool_t m_bPressureMonitor = FALSE;

/******************************************************************************/
/*!
 * \var m_bHgBBlankAcq
 * \brief  variable to enable to acquire the HgB blank data from ADC
 */
/******************************************************************************/
bool_t m_bHgBBlankAcq = FALSE;
/******************************************************************************/
/*!
 * \var m_bHgBBlankAcqRepeat
 * \brief  variable to enable to acquire the HgB blank data from ADC
 */
/******************************************************************************/
bool_t m_bHgBBlankAcqRepeat = FALSE;


/******************************************************************************/
/*!
 * \var m_bHgBSampleAcq
 * \brief  variable to enable to acquire the HgB blood sample data from ADC
 */
/******************************************************************************/
bool_t m_bHgBSampleAcq = FALSE;

/******************************************************************************/
/*!
 * \var m_bHgBBlankAcq
 * \brief  variable to store averaged HgB blank data acquired from ADC
 */
/******************************************************************************/
uint16_t m_usnHgbBlank = ZERO;
/******************************************************************************/
/*!
 * \var m_usnHgbBlankRepeat
 * \brief  variable to store averaged HgB blank data acquired from ADC
 */
/******************************************************************************/
uint16_t m_usnHgbBlankRepeat = ZERO;
/******************************************************************************/
/*!
 * \var m_usnHgbSample
 * \brief  variable to store averaged HgB blood sampled data acquired from ADC
 */
/******************************************************************************/
uint16_t m_usnHgbSample = ZERO;

/******************************************************************************/
/*!
 * \var g_bMotorChkStatus
 * \brief  variable to update motor check status success/failure to preocessor
 */
/******************************************************************************/
bool_t g_bMotorChkStatus = FALSE;

/******************************************************************************/
/*!
 * \var m_ucHgBBlankAcqCount
 * \brief  update final averaged HgB blank data only after acquireing 32 samples.
 * This variable maintains the count of 32 HgB blank samples
 */
/******************************************************************************/
unsigned char m_ucHgBBlankAcqCount = ZERO;

/******************************************************************************/
/*!
 * \var m_ucHgBSampleAcqCount
 * \brief  update final averaged HgB blood sample data only after acquireing 32 samples.
 * This variable maintains the count of 32 HgB blank samples
 */
/******************************************************************************/
unsigned char m_ucHgBSampleAcqCount = ZERO;


bool_t m_bCountingTimeSeq = FALSE;  //CRH_TBD - to check whether this variable is required

/******************************************************************************/
/*!
 * \var m_snMonDiluentCount
 * \brief  Diluent prime error to be set only if the error is detected continuously
 * for a minimum time of REAGENT_PRIME_MON_TIME. This variable is used to increment
 * and maintain the error detection time.
 */
/******************************************************************************/
uint16_t m_snMonDiluentCount = ZERO;

/******************************************************************************/
/*!
 * \var g_bMonDiluentPriming
 * \brief  Variable to enable or disable the diluent line monitoring, by defualt
 * the line will not be monitored. After completion of diluent priming, the line
 * will be monitored
 */
/******************************************************************************/
bool_t g_bMonDiluentPriming = FALSE;

/******************************************************************************/
/*!
 * \var m_snMonLyseCount
 * \brief  Lyse prime error to be set only if the error is detected continuously
 * for a minimum time of REAGENT_PRIME_MON_TIME. This variable is used to increment
 * and maintain the error detection time.
 */
/******************************************************************************/
uint16_t m_snMonLyseCount = ZERO;

/******************************************************************************/
/*!
 * \var g_bMonLysePriming
 * \brief  Variable to enable or disable the Lyse line monitoring, by defualt
 * the line will not be monitored. After completion of Lyse priming, the line
 * will be monitored
 */
/******************************************************************************/
bool_t g_bMonLysePriming = FALSE;

/******************************************************************************/
/*!
 * \var m_snMonRinseCount
 * \brief  Rinse prime error to be set only if the error is detected continuously
 * for a minimum time of REAGENT_PRIME_MON_TIME. This variable is used to increment
 * and maintain the error detection time.
 */
/******************************************************************************/
uint16_t m_snMonRinseCount = ZERO;

/******************************************************************************/
/*!
 * \var g_bMonRinsePriming
 * \brief  Variable to enable or disable the Rinse line monitoring, by defualt
 * the line will not be monitored. After completion of Rinse priming, the line
 * will be monitored
 */
/******************************************************************************/
bool_t g_bMonRinsePriming = FALSE;

/******************************************************************************/
/*!
 * \var utErrorInfoCheck
 * \brief  Variable to store the error info
 */
/******************************************************************************/
union UT_ERROR_INFO utErrorInfoCheck;

/******************************************************************************/
/*!
 * \var g_usnDataMode
 * \brief  Variable to send the data depending upon the mode/sequence selected
 */
/******************************************************************************/
uint16_t g_usnDataMode = 0;

/******************************************************************************/
/*!
 * \var m_bXAxisMonEnable
 * \brief  Variable to enable to monitor the X-Axis motor in any given sequence.
 * It shall be disabled when X-Axis need not to be monitored
 */
/******************************************************************************/
bool_t m_bXAxisMonEnable = FALSE;

/******************************************************************************/
/*!
 * \var m_bDiluentSyringeMonEnable
 * \brief  Variable to enable to monitor the Diluent Syringe in any given sequence.
 * It shall be disabled when Diluent Syringe need not to be monitored
 */
/******************************************************************************/
bool_t m_bDiluentSyringeMonEnable = FALSE;

/******************************************************************************/
/*!
 * \var m_bSampleSyringeMonEnable
 * \brief  Variable to enable to monitor the Sample Syringe in any given sequence.
 * It shall be disabled when Sample Syringe need not to be monitored
 */
/******************************************************************************/
bool_t m_bSampleSyringeMonEnable = FALSE;

/******************************************************************************/
/*!
 * \var m_bWasteSyringeMonEnable
 * \brief  Variable to enable to monitor the Waste Syringe in any given sequence.
 * It shall be disabled when Waste Syringe need not to be monitored
 */
/******************************************************************************/
bool_t m_bWasteSyringeMonEnable = FALSE;
bool_t m_bBubblingMovEnFlag = FALSE;
/******************************************************************************/
/*!
 * \var m_bYAxisMonEnable
 * \brief  Variable to enable to monitor the Y-Axis in any given sequence.
 * It shall be disabled when Y-Axis Syringe need not to be monitored
 */
/******************************************************************************/
bool_t m_bYAxisMonEnable = FALSE;

/******************************************************************************/
/*!
 * \var m_eXPosition
 * \brief  Variable to store the X-Axis position, by default it is eXPositionInvalid,
 * X-Axis for aspiration = eASPIRATION, X-Axis above RBC bath = eRBC_BATH,
 * X-Axis above WBC bath = eWBC_BATH
 */
/******************************************************************************/
E_XPOSITION m_eXPosition = eXPositionInvalid;

/******************************************************************************/
/*!
 * \var m_eCycleCompletion
 * \brief  Variable to store the states during completion of the cycle and data
 * transmission to Processor, by default it is eDefault (Zero),
 */
/******************************************************************************/
E_CYCLE_COMPLETION m_eCycleCompletion = eDefault;

/******************************************************************************/
/*!
 * \var g_stSeqMonTime
 * \brief  Variable to store Sequence monitoring time
 */
/******************************************************************************/
struct ST_SEQ_MON_TIME g_stSeqMonTime;

/******************************************************************************/
/*!
 * \var utVolumetricErrorInfo
 * \brief  Variable to store the volumetric error information
 */
/******************************************************************************/
union UT_VOLUMETRIC_ERROR utVolumetricErrorInfo;

/******************************************************************************/
/*!
 * \var utVolumetricErrorInfoReAcq
 * \brief  Variable to store the volumetric error information caused for
 *      re-acquisition
 */
/******************************************************************************/
union UT_VOLUMETRIC_ERROR_RE_ACQ utVolumetricErrorInfoReAcq;

/******************************************************************************/
/*!
 * \var m_bRBCStartSensor
 * \brief  Variable to store the RBC Flow start liquid sensing status
 */
/******************************************************************************/
bool_t m_bRBCStartSensor;

/******************************************************************************/
/*!
 * \var m_bRBCStopSensor
 * \brief  Variable to store the RBC Flow stop liquid sensing status
 */
/******************************************************************************/
bool_t m_bRBCStopSensor;

/******************************************************************************/
/*!
 * \var m_bWBCStartSensor
 * \brief  Variable to store the WBC Flow start liquid sensing status
 */
/******************************************************************************/
bool_t m_bWBCStartSensor;

/******************************************************************************/
/*!
 * \var m_bWBCStopSensor
 * \brief  Variable to store the RBC Flow stop liquid sensing status
 */
/******************************************************************************/
bool_t m_bWBCStopSensor;

/******************************************************************************/
/*!
 * \var utSysStatusMonInfo
 * \brief  Variable to store the system status monitoring enable information
 */
/******************************************************************************/
/*!
 * \var g_bYMotorFaultMonEn
 * \brief  Variable to store the system status monitoring enable information
 */
/******************************************************************************/
bool_t   g_bYMotorFaultMonEn = FALSE;
/******************************************************************************/
/*!
 * \var g_bXMotorFaultMonEn
 * \brief  Variable to store the system status monitoring enable information
 */
/******************************************************************************/
bool_t   g_bXMotorFaultMonEn = FALSE;
/******************************************************************************/
/*!
 * \var g_bDilMotorFaultMonEn
 * \brief  Variable to store the system status monitoring enable information
 */
/******************************************************************************/
bool_t   g_bDilMotorFaultMonEn = FALSE;
/******************************************************************************/
/*!
 * \var g_bWasteMotorFaultMonEn
 * \brief  Variable to store the system status monitoring enable information
 */
/******************************************************************************/
bool_t   g_bWasteMotorFaultMonEn = FALSE;
/******************************************************************************/
/*!
 * \var g_bSampleMotorFaultMonEn
 * \brief  Variable to store the system status monitoring enable information
 */
/******************************************************************************/
bool_t   g_bSampleMotorFaultMonEn = FALSE;

/******************************************************************************/
/*!
 * \var g_bCountTimeVacuumMoni
 * \brief  Variable to store the Count time vacuum monitoring
 */
/******************************************************************************/
bool_t   g_bCountTimeVacuumMoni = FALSE;
/******************************************************************************/
/*!
 * \var g_bCountTimeVacuumMoni
 * \brief  Variable to store the Count time vacuum monitoring
 */
/******************************************************************************/
bool_t   g_bTubeCleanVacuumMoni = FALSE;
/******************************************************************************/
/*!
 * \var cZapTime
 * \brief  Variable to store the ZapTime
 */
/******************************************************************************/
static int cZapTime = 0;    //QA_C

/******************************************************************************/
/*!
 * \var m_bReadyForAspirationState
 * \brief  To indicate the present state is in ready for aspiration
 */
/******************************************************************************/
bool_t m_bReadyForAspirationState = FALSE;

/******************************************************************************/
/*!
 * \var m_arrusnTimeStamp
 * \brief  To indicate the present state is in ready for aspiration
 */
/******************************************************************************/
//uint16_t m_arrusnTimeStamp[100];

/******************************************************************************/
/*!
 * \var m_arrusnTimeStamp
 * \brief  To indicate the present state is in ready for aspiration
 */
/******************************************************************************/
//uint16_t m_usnTimeStampLen = 0;

/******************************************************************************/
union UT_SYS_STATUS_MON_EN utSysStatusMonEnInfo;

bool_t g_bDataTransmitted = FALSE;
#pragma DATA_SECTION(DebugPrintBuf,".RAMGS10_Cpu1IpcParam");
char DebugPrintBuf[50];

/******************************************************************************/
/*!
 * \struct stPwmParameter:
 * \brief stPwmParameter object creation
 * 		  Object: *pMotor1Pwm8
 * 		  Object: *pMotor2Pwm6
 * 		  Object: *pMotor3Pwm2
 * 		  Object: *pMotor4Pwm5
 * 		  Object: *pMotor5Pwm1
 *
 */
/******************************************************************************/
static stPwmParameter *pMotor1Pwm8;
static stPwmParameter *pMotor2Pwm6;
static stPwmParameter *pMotor3Pwm2;
static stPwmParameter *pMotor4Pwm5;
static stPwmParameter *pMotor5Pwm1;

/******************************************************************************/
/*!
 * \var stCpu1IpcCommPtr
 * \brief  pointer to Structure to refer to IPC parameters which communicates
 * between the CPU1 and CPU2
 */
/******************************************************************************/
static stIpcParameters *stCpu1IpcCommPtr;

/******************************************************************************/
/*!
 * \var uiSampleStartBtnPress
 * \brief  Variable to indicate MBD to that sample start button was pressed and
 * start aspiration
 */
/******************************************************************************/
static Uint16 uiSampleStartBtnPress = FALSE;

/******************************************************************************/
/*!
 * \var usnSdramAdcDataAddr
 * \brief  Array to store the address of the RBC, WBC, PLT count & pulse heights
 */
/******************************************************************************/
Uint32 usnSdramAdcDataAddr[13];

/******************************************************************************/
/*!
 * \var m_pSdramADCWBCCnt
 * \brief  Pointer to WBC count which is used to put data to the packet that is
 * transmitted to processor
 */
/******************************************************************************/
Uint32* m_pSdramADCWBCCnt = NULL;

/******************************************************************************/
/*!
 * \var m_pSdramADCRBCCnt
 * \brief  Pointer to RBC count which is used to put data to the packet that is
 * transmitted to processor
 */
/******************************************************************************/
Uint32* m_pSdramADCRBCCnt = NULL;

/******************************************************************************/
/*!
 * \var m_pSdramADCPLTCnt
 * \brief  Pointer to PLT count which is used to put data to the packet that is
 * transmitted to processor
 */
/******************************************************************************/
Uint32* m_pSdramADCPLTCnt = NULL;

/******************************************************************************/
/*!
 * \var g_unZappingTime
 * \brief  variable maintains the time till which Zapping to be performed
 */
/******************************************************************************/
uint32_t g_unZappingTime;

/******************************************************************************/
/*!
 * \var m_usnWBCCountTime
 * \brief  variable holds the WBC acquisition time
 */
/******************************************************************************/
uint16_t m_usnWBCCountTime = ZERO;

/******************************************************************************/
/*!
 * \var m_usnRBCCountTime
 * \brief  variable holds the RBC & PLT acquisition time
 */
/******************************************************************************/
uint16_t m_usnRBCCountTime = ZERO;

/******************************************************************************/
/*!
 * \var m_bSequenceCompleted
 * \brief  variable indicates whether the sequence is completed or not
 */
/******************************************************************************/
bool_t m_bSequenceCompleted = FALSE;

/******************************************************************************/
/*!
 * \var m_bVolumetricError
 * \brief  variable indicates whether error has occured in the volumetric tubes
 * Errors indicate bubble, block pressure release in the volumetric tube
 */
/******************************************************************************/
bool_t m_bVolumetricError = FALSE;

/******************************************************************************/
/*!
 * \var m_bInvalidData
 * \brief  variable indicates whether the measurement is valid or not
 */
/******************************************************************************/
bool_t m_bInvalidData = FALSE;

/******************************************************************************/
/*!
 * \var stPressureSensePtr
 * \brief  Pointer to structure of pressure parameters
 */
/******************************************************************************/
volatile PressureParameter *stPressureSensePtr;

/******************************************************************************/
/*!
 * \var pstStatusSense
 * \brief  Pointer to structure of system health status parameters
 */
/******************************************************************************/
volatile  ST_HEALTH_STATUS *pstStatusSense;

/******************************************************************************/
/*!
 * \var g_usnFirstDilutionFreq
 * \brief  Variable to store first dilution frequency, by default is is set to
 * FIRST_DILUTION_FREQ_DEFAULT
 */
/******************************************************************************/
uint16_t g_usnFirstDilutionFreq = FIRST_DILUTION_FREQ_DEFAULT;

/******************************************************************************/
/*!
 * \var g_usnLyseMixingFreq
 * \brief  Variable to store Lyse Mixing frequency, by default is is set to
 * LYSE_MIXING_FREQ_DEFAULT
 */
/******************************************************************************/
uint16_t g_usnLyseMixingFreq = LYSE_MIXING_FREQ_DEFAULT;

/******************************************************************************/
/*!
 * \var g_usnFinalRBCMixingFreq
 * \brief  Variable to store Final RBC Mixing frequency, by default is is set to
 * FINAL_RBC_MIXING_FREQ_DEFAULT
 */
/******************************************************************************/
uint16_t g_usnFinalRBCMixingFreq = 100; //FINAL_RBC_MIXING_FREQ_DEFAULT;

/******************************************************************************/
/*!
 * \var g_usnWholeBloodZapTime
 * \brief  Variable to store Final zap time in whole blood mode, by default is is set to
 * g_usnWholeBloodZapTime
 */
/******************************************************************************/
uint16_t g_usnWholeBloodZapTime = 0;  //HN_added

/******************************************************************************/

uint16_t g_DiluentMixTime = 0;  //HN_added
uint16_t g_RBCMixTime = 0;  //HN_added
uint16_t g_LyseMixTime = 0;  //HN_added
//Test data
uint16_t g_TestDataI0 = 0;  //HN_added
uint16_t g_TestDataI1 = 0;  //HN_added
uint16_t g_TestDataI2 = 0;  //HN_added
/******************************************************************************/
/*!
 * \var g_usnWholeBloodZapTime
 * \brief  Variable to store calibrated WBC counting time done at flow calibration sequence, by default is is set to
 * g_usnWholeBloodZapTime
 */
/******************************************************************************/
uint16_t g_usnWbcCalibratedTime = 330;  //DS_added
/******************************************************************************/
/*!
 * \var g_usnWholeBloodZapTime
 * \brief  Variable to store calibrated RBC counting time done at flow calibration sequence, by default is is set to
 * g_usnWholeBloodZapTime
 */
/******************************************************************************/
uint16_t g_usnRbcCalibratedTime = 440;  //DS_added

/******************************************************************************/
/*!
 * \var m_bReadyforAspCommand
 * \brief  Variable to send Ready for aspiration command to Sitara only once
 * when the probe is ready for aspiration
 */
/******************************************************************************/
bool_t m_bReadyforAspCommand = TRUE;

bool_t m_bPressureValveOn = FALSE;
unsigned char m_ucPressureValveCounter = 250;
bool_t m_bValveExcited = FALSE;

extern uint16_t g_usnCurrentModeMajor;
extern Uint16 usnSensor1_HomeState;
extern void BloodCellCounter_initialize(void);
extern volatile bool_t g_bStartButtonStatus;
extern uint16_t g_usnCurrentMode;
extern uint16_t g_usnCurrentModeTemp;
extern volatile bool_t g_bHealthStatusCheck;
extern bool_t g_bBusyBitStatus;
extern bool_t m_bMutiplePackets;
extern bool_t m_bLastMultiplePacket;
extern Uint32 g_unSequenceStateTime;
extern Uint32 g_unSequenceStateTime1;
extern uint16_t g_usnHealthindexCounter;
extern bool_t m_bSPIWriteControlStatus;
extern bool_t m_bTxPendingFlag;
extern uint16_t g_unSysHealthMonCount;
extern  uint16_t   SMDGetMotorFault();
/*! Buffer for algorithm and cell signal processing */
/******************************************************************************/
/*!
 * \brief structure for Blood cell statistics
 * \struct stBloodCellVoltVal
 *
 */
/******************************************************************************/
#if 1
#pragma DATA_SECTION(sstBloodCellStatisticsDefault,".RAMGS10_Cpu1IpcParam");
static ST_BLOOD_CELL_VOLT_VAL	sstBloodCellStatisticsDefault[FOUR] =
    {
    		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    		NULL, NULL, NULL, FLOAT_MAX, FLOAT_MIN, CELL_LOW_V_RBC,
    		CELL_HIGH_V_RBC, APERTURE_LENGTH_RBC_PLT, APERTURE_DIA_RBC_PLT,
    		APERTURE_LENGTH_ADJUSTED_RBC_PLT, YES, ZERO,  ZERO, ZERO,
    		DELTA_MIN_MAX_RBC, PULSEWIDTH_MIN_RBC, PULSEWIDTH_MAX_RBC,
            FLOAT_MAX, FLOAT_MIN, CELL_LOW_V_PLT, CELL_HIGH_V_PLT,
            APERTURE_LENGTH_RBC_PLT, APERTURE_DIA_RBC_PLT,
            APERTURE_LENGTH_ADJUSTED_RBC_PLT, YES, ZERO, ZERO, ZERO,
            DELTA_MIN_MAX_PLT, PULSEWIDTH_MIN_PLT, PULSEWIDTH_MAX_PLT,
            FLOAT_MAX, FLOAT_MIN, CELL_LOW_V_WBC, CELL_HIGH_V_WBC,
            APERTURE_LENGTH_WBC, APERTURE_DIA_WBC, APERTURE_LENGTH_ADJUSTED_WBC,
            YES, ZERO,  ZERO, ZERO, DELTA_MIN_MAX_WBC, PULSEWIDTH_MIN_WBC,
            PULSEWIDTH_MAX_WBC
    };
#endif
/******************************************************************************/



/******************************************************************************/
/*!
 * \fn void SysSystem_Initalize(void)
 * \brief system initialization
 * \brief Description:
 *      This function initialize CPU peripherals and application modules
 * \return void
 */
/******************************************************************************/
void SysSystem_Initalize(void)
{
	Uint16 Emif1Status=ZERO;
	printf("\n HW selected:  Alpha Cell Counter v1 \n");

	//memset(m_arrusnTimeStamp, 0, sizeof(m_arrusnTimeStamp));


	//!Get the pointer related to Motor 1 structure
  	pMotor1Pwm8	  = PWMGetMotor1Pwm8();

  	//!Get the pointer related to Motor 2 structure
  	pMotor2Pwm6	  = PWMGetMotor2Pwm6();

  	//!Get the pointer related to Motor 3 structure
  	pMotor3Pwm2	  = PWMGetMotor3Pwm2();

  	//!Get the pointer related to Motor 4 structure
  	pMotor4Pwm5	  = PWMGetMotor4Pwm5();

  	//!Get the pointer related to Motor 5 structure
  	pMotor5Pwm1	  = PWMGetMotor5Pwm1();

  	//!To get the CPU1 IPC parameter pointers
  	stCpu1IpcCommPtr = IPCGetCpu1IpcParam();

  	//!To get the pressure sensor structure pointer
  	stPressureSensePtr = PSGetPressureSense();

  	//!To get the pressure sensor structure pointer
  	pstStatusSense = ADGetHealthStatus();

	//!Initialize Board - Flash, Interept PIE Control, Interrupt Flag enable
	//!Interrupt clear flag, Interrupt pie vector table initialization
  	IBInitialize_Board();

	//!Initialize all the GPIOs
  	IGInitialize_Gpio();

	//!Initialize IPC
  	IPCInitialize_IPC();

	//!Initialize the timer
	IBTimer_Interrupt();

	//!Watchdog Timer Function
	IBWatchDog_Timer_Interrupt();
	//! enabling before while loop
	//Enable_WatchdogTimer();       //DO NOT ENABLE CURRENTLY

	//!SPI Initalization for Valve control
	SIInitialize_SpiValve();

	//!PWM Initalization for all motor operation
	PWMInitialize();

	//!HGB pwm  Initalization for HGB operation
	PWMInitHgbPwm();

	//!Initialize the GPIOs used for Motors
	SMDInitialize_MotorGpio();

	//!UART configuration for debug messages
	configureUART(115200);

	//!Home Position Sensor Configuration and setup
#ifdef IO_EXPANDER_USED
	OIInitSpiHomeSensor();
#endif
	//!Initialize the GPIOs used for Voulemtric tubes
	OIInit_GPIO_EXTINT();

	//! Cpu2 ownership for ADC
	ADCpu2Ownership();
	DELAY_US(100000);

	//! ADCD engine initialization
	ADAdcd_Init();

	//! Pressure Sensor Configuration and setup
	PSInitPressureModule();

	//! SDRAM Initalization
	ISInitializeSDRAM();

	/*!To initialse the pointers to the usnSdramAdcDataAddr */
	SysInitSDRamAdd();

	//! SPI communication (DSP-SITARA) Initalization
	SPIConfigCommPortAndDMA();

#if	STANDALONE_MODULE_TEST
	SysPeripheralModuleTest();
	SysTestWasteMotor();
#endif
	//SPIActivateCommPort();

	//! IPC sync b/w cpu1 & cpu2
	SysInitCpu1Cpu2IpcComm();

	//! variable initialization
	SysSetParametersDefaultValue();


	//! emif1 ownership transfer
	ISConfigEmif1CPU(0x00);
	while(!(0x02 == Emif1Status))
	{
		DELAY_US(500000);
		Emif1Status = ISGetEmif1Status();
	}
	//!Start the DMA Channels
	SPIActivateCommPort();

/*
#if DEBUG_PRINT_ENABLE
	printf("Initialization of MBD code \n");
#endif
*/     //QA_C

	//!Switch on the PWM of the HGB
	PWMHgbPwmControl(START, HGB_PWM_FREQUENCY); //Switch on the HgB LED
}

/******************************************************************************/
/*!
 * \fn void SysRT_OneStep(void)
 * \brief Sequence of operation for Blood analysis
 * \brief Description:
 *      This function call shall do the sequence of operation for
 *      blood data analysis and motor control
 * \return void
 */
/******************************************************************************/
void SysRT_OneStep(void)
{
    /*! This is auto generated code from MBD. Please do not modify*/
	static boolean_T OverrunFlag = ZERO;

    /*! Disable interrupts here */

    /*! Check for overrun - This is auto generated code from MBD.
    * Please do not modify*/
    if (OverrunFlag)
    {
    rtmSetErrorStatus(BloodCellCounter_M, "Overrun");
    return;
    }

    //!Overrun flag is set to true. This is auto generated code from MBD.
    //!Please do not modify
    OverrunFlag = TRUE;

    //!IPC Communication between CPU1 and CPU2
    SysCpu1IpcCommModule();

    /*! Save FPU context here (if necessary)
    * Re-enable timer or interrupt here
    * Set model inputs here
    * */
    //!To update the status and variables from DSP to MBD
    SysUpdateDspToMbd();

    //!Execution of MBD
    BloodCellCounter_step();

    //!To update the status and variables from MBD to DSP
    SysUpdateMbdToDsp();

    //! Wait for Flag from mbd and then poll for Sample start button press
    SysUpdateSampleStartButton();

    //!Check for Syringe home position check after motor stopped condition
    //!SysMotorHomePositionCheck should be called before SysMotor_Valve_Control
    //!becuase moter completed status is checked.
    SysMotorHomePositionCheck();

    //!Output from the MBD is verifeid for Motor and Valve
    SysMotor_Valve_Control();

    //!ADC Aquisition to monitor the Health parameter of the system
    //!24V, 5V, Temperature, Pressure and HgB
    SysDataAquisition();

    //!Checking each valve with respect to command from Sitara
    SysValveStatusTest();

    //!To check whether the waste bin is full or not
    SysWasteOverflowCheck();

    //!To check whether any of the sequence is completed and what action
    //!to be taken based on the specific sequence
    SysCycleCompletion();

    //!Indicate task complete - Auto generated from MBD please donot modify
    OverrunFlag = FALSE;

    //!To monitor the whether the reagent lines are primed or not
    //!If any of the line is empty the sequence is aboted and
    //!error message is posted to Sitara to take any further actions
    SysMonitorReagentPriming();

    /*! Disable interrupts here
    * Restore FPU context here (if necessary)
    * Enable interrupts here */

    //int test  = (unsigned int)OIHomeSensor5State();
    //printf("\n test: %d \n", test );

    //!GPIO to be toggled every 3 seconds to indicate processor that
    //!controller is not hanged.
    SysControllerStatusMon();

    //!Get the System health status from ADC
    //SysInitGetSystemStatus();   //we are calling this fun from 5ms loop

    //!Monitor system health status every 60ms
    if(0==g_unSysHealthMonCount%3)
    {
        //!To monitor the system health from the ADC output
        SysInitHealthStatusCheck();
    }

    //! To monitor the pressure sensore tube connection
    SysPressureLineCheck();

    //! To calibrate the pressure
    SysPressureCalibration();

    //PressureValveControl();

    //!Send SPI error check command if the controller is idle
    if((0 == g_usnCurrentModeMajor) || (TRUE == m_bReadyForAspirationState))
    {
        //!check if it is 1 minute interval, then send SPI error check command
        if(0 == (g_unSPIErrorCheckCounter % 3000))
        {
            //!Frame SPI error check packet
            CIFormServiceHandlingPacket(SPI_ERROR_CHECK, FALSE);
        }
    }
}

/******************************************************************************/
/*!
 * \fn void SysInitCpu1Cpu2IpcComm(void)
 * \brief Function is to sync b/w cpu1 & cpu2
 * \brief Description:
 *      Function is to sync b/w cpu1 & cpu2. Initial data handshaking b/w the
 *      cores and firm ware verion request
 * \return void
 */
/******************************************************************************/
void SysInitCpu1Cpu2IpcComm(void)
{
	uint16_t status = ZERO;
	//!CPU1 and CPU2 IPC syncronization
	IPCCpu1toCpu2IpcCommSync();

	//!Delay should be provided after syncronization is done. Donot modify the delay
	DELAY_US(10000);

	sprintf(DebugPrintBuf, "\r\n Cpu1 & Cpu2 Ipc communication sync-done");
	UartDebugPrint((int *)DebugPrintBuf, 50);
	//!Request for the firmware version of CPU2
	//! Pass the WBC, RBC and PLT threshold and cell parameter values to CPU2
	/*------------------Initial IPC commm data ----------------------------*/
	stCpu1IpcCommPtr->LocalReceiveFlag = 0;
	stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[0] = CPU1_IPC_CPU2_FW_VERSION_REQ;
	stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[1] = \
	        (uint32_t)&stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[0];
	stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[2] = \
	        (uint32_t)&sstBloodCellStatisticsDefault[eCellWBC];
	stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[3] = \
	        (uint32_t)&sstBloodCellStatisticsDefault[eCellRBC];
	stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[4] = \
	        (uint32_t)&sstBloodCellStatisticsDefault[eCellPLT];
	/*------------------Initial IPC commm data ----------------------------*/
	//!Send the commnad and data to CPU2
	status =  IPCCpu1toCpu2IpcCmdData(stCpu1IpcCommPtr,5);
	printf("\n CPU1_IPC_CPU2_FW_VERSION_REQ status %d \n",status);
}
/******************************************************************************/
/*!
 * \fn void SysUartPrintCpuFirmwareVer(Uint16 Cpu)
 * \brief will print the FW verion
 * \brief Description:
 *      This function call will print FW verion of cpu
 * \return void
 */
/******************************************************************************/
void SysUartPrintCpuFirmwareVer(Uint16 Cpu)
{
	switch(Cpu)
	{
	case 1:
	    //!Print the CPU1 Firmware Version
		sprintf(DebugPrintBuf, "\r\n ");
		UartDebugPrint((int *)DebugPrintBuf, 50);
		UartDebugPrint((int *)Cpu1FirmwareVersion, 50);
		break;
	case 2:
	    //!Print the CPU2 Firmware Version
		sprintf(DebugPrintBuf, "\r\n ");
		UartDebugPrint((int *)DebugPrintBuf, 50);
		UartDebugPrint((int *)Cpu2FirmwareVersion, 50);
		break;

	default:
	    //!Print the CPU1 Firmware Version
		sprintf(DebugPrintBuf, "\r\n ");
		UartDebugPrint((int *)DebugPrintBuf, 50);
		UartDebugPrint((int *)Cpu1FirmwareVersion, 50);
		break;
	}
}
/******************************************************************************/
/*!
 * \fn void SysCpu1IpcCommModule(void)
 * \brief Ipc communication Module
 * \brief Description:
 *      This function call shall process ipc communication module
 * \return void
 */
/******************************************************************************/
void SysCpu1IpcCommModule(void)
{

    static unsigned int usnReAcquisition = 1;
    //!Check the flag whether any data is received from CPU2
	if(stCpu1IpcCommPtr->LocalReceiveFlag == 1)
	{
	    //!Check the command received from the CPU2
		switch(stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0])
		{
		//!If the command received is CPU2 firmware version then update the
		//! below mentioned parameters
		case CPU2_IPC_CPU2_FW_VERSION_RESPONSE:
			//printf("CPU2_IPC_CPU2_FW_VERSION_RESPONSE %ld \n", \
			//        stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0]);
			//!Reset the flag after reading the data from CPU2
			stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
			//!Reset the command received from CPU 2 to Zero
			stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;

		    sprintf(DebugPrintBuf, "\r\nVer Response");
		    UartDebugPrint((int *)DebugPrintBuf, 40);

		    sprintf(DebugPrintBuf, "\r Add: %ld", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2]);
		    UartDebugPrint((int *)DebugPrintBuf, 40);
			//!Update the CPU 2 version number received from CPU2 to the array
			SysGetCpu2FwVer(stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2]);
			//!Update the WBC threshold values and Cell information
			SysGetWbcBloodCellInfo(stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[3]);
			//!Update the RBC threshold values and Cell information
			SysGetRbcBloodCellInfo(stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[4]);
			//!Update the PLT threshold values and Cell information
			SysGetPltBloodCellInfo(stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[5]);
			break;

	        //!If the command received is first Count Data Acquisition started
	        //! then update the below mentioned parameters
		case CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_STARTED:
		    bStartADCPressure = m_usnPressureAdc;    //Hari_Pressure Test

			//printf("---Cpu1 Rx: %ld from CPU2--\n", \
			//        stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0]);
			//!Reset the flag after reading the data from CPU2
			stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
			//!Reset the command received from CPU 2 to Zero
			stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;

			//!Update the SDRAM address of WBC RAW address
			usnSdramAdcDataAddr[0] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1];
			//!Update the SDRAM address of RBC RAW address
			usnSdramAdcDataAddr[1] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2];
			//!Update the SDRAM address of PLT RAW address
			usnSdramAdcDataAddr[2] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[3];
			//!Update the second count offset - presently not used
			usnSdramAdcDataAddr[3] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[4];
			//!Update the SDRAM address of WBC pulse height address
			usnSdramAdcDataAddr[4] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[5];
			//!Update the SDRAM address of RBC pulse height address
			usnSdramAdcDataAddr[5] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[6];
			//!Update the SDRAM address of PLT pulse height address
			usnSdramAdcDataAddr[6] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[7];

			//printf("First Measurement Started \n");
            sprintf(DebugPrintBuf, "\r\n First Meas Started: %ld\n", g_unSequenceStateTime);
            UartDebugPrint((int *)DebugPrintBuf, 50);

			bPressureMonitor = TRUE;

#ifdef _ALPHA_2_BOARD_
			//!Set Valve open status - it is hand to to indicate MBD to release
			//! vaccum to start the flow through the volumetric tube
			BloodCellCounter_U.ValveOpenStart = TRUE;
#endif
			break;

            //!If the command received is first Count Data Acquisition completed
            //! then update the below mentioned parameters
		case CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED:

		    //!Start HGB pwm to take sample HGB value
		    PWMHgbPwmControl(START, HGB_PWM_FREQUENCY); //HN_Added_4/4/19

		    bEndADCPressure = m_usnPressureAdc;    //Hari_Pressure Test

			//!Reset the flag after reading the data from CPU2
			stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
			//!Reset the command received from CPU 2 to Zero
			stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;
			sprintf(DebugPrintBuf, "\r\n DATA_ACQ_COMPLETED: %ld\n", g_unSequenceStateTime);
			UartDebugPrint((int *)DebugPrintBuf, 50);

			//bPressureMonitor = FALSE;

			//CRH_RBC_WBC_TIME  - START
			sprintf(DebugPrintBuf, "\r\nWBC Count Time: %ld\n", \
			        stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1]);
            UartDebugPrint((int *)DebugPrintBuf, 40);
            sprintf(DebugPrintBuf, "\r\nRBC_PLT Count Time:%ld\n", \
                    stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2]);

            //!WBC acquisition time is obtained from CPU2 and updated
            m_usnWBCCountTime =(uint16_t)(20 *(stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1]));
            //!RBC acquisition time is obtained from CPU2 and updated
            m_usnRBCCountTime =(uint16_t)(20 *(stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2]));

            //!Volumetric error info is acquired and updated
            utVolumetricErrorInfo.unVolumetricError = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[3];

            if((utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted) && \
            (utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted))
            {
               // m_eCycleCompletion = eRESULT_TRANSMISSION;eERROR_CHECK

            }
            //!Need to check whether this variable can be removed
            m_bCountingTimeSeq = TRUE;//SAM_COUNT_TIME  //CRH_TBD - to check whether this variable is required

            //!Re-acquisition shall be enabled for the first time in the sequence
            //! if the error occurs. It shall be disabled if the error occurs,
            //! so that re-acquisition happens only once. If the error occurs again,
            //! error information will be sent to Sitara
            //!Check if the re-acquisition is enabled
            if(ENABLE_RE_ACQ == usnReAcquisition)
            {
                sprintf(DebugPrintBuf, "\r\nCount Time Completed\n");
                UartDebugPrint((int *)DebugPrintBuf, 40);
                //!Check if any of the volumetric error has occured, input MBD
                //! for re-acquisition
                if((utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted) || \
                        (utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted) || \
                        (utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper) || \
                        (utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper) || \
                        (utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotCompleted) || \
                        (utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotCompleted) || \
                        (utVolumetricErrorInfo.stVolumetricError.unWBCCountingStartedImproper) || \
                        (utVolumetricErrorInfo.stVolumetricError.unRBCCountingStartedImproper))
                {
                    if((g_usnCurrentMode ==FLOW_CALIBRATION) || (g_usnCurrentMode == COUNTING_TIME_SEQUENCE))
                    {
                        sprintf(DebugPrintBuf, "\r\n Re-Aqu is not req in COUNTING_TIME_SEQUENCE\n");
                        UartDebugPrint((int *)DebugPrintBuf, 40);


                        //!Set the status to error check and then error status is
                        //! sent to Sitara
                        m_eCycleCompletion = eERROR_CHECK;

                        //!Send error packet
                        //SysErrorCommand(g_usnDataMode);

                        //!Set busy state to high
                        if(g_bBusyBitStatus == FALSE)
                        {
                            SPISetCommCtrlBusyInt();
                        }


                        //!Set the count stop to 1 and disable the re-acquisition and
                        //! proceed for further draining
                        BloodCellCounter_U.CountStop = 1;
                    }
                    else

                    {
                        //!Set count stop to 2 so that Re-Acquisition happens at MBD
                        //BloodCellCounter_U.CountStop = 2;
                        BloodCellCounter_U.CountStop = 1;   //HN_added
                        sprintf(DebugPrintBuf, "\r\n2 RE-AC\n");
                        UartDebugPrint((int *)DebugPrintBuf, 40);

                        //!Set Reacquisition error
                        utErrorInfoCheck.stErrorInfo.ulnReAcq = TRUE;

                        if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper)
                        {
                            if( ((20 * WBC_ACQ_TIME_LOW)> m_usnWBCCountTime) && (0 != m_usnWBCCountTime)){
                                utVolumetricErrorInfo.stVolumetricError.unWBCEarlyCompletion = TRUE;
                            }
                            else //HN_Added on 10_5_2019
                            {
                                utVolumetricErrorInfo.stVolumetricError.unWBCEarlyCompletion = FALSE;
                            }
                            if( ((20 * WBC_ACQ_TIME_HIGH) <= m_usnWBCCountTime)||( 10000 < m_usnWBCCountTime) )
                            {
                                //jojo edited 1.5 sec tolarence
                                utVolumetricErrorInfo.stVolumetricError.unWBCLateCompletion = TRUE;
                            }
                            else //HN_Added on 10_5_2019
                            {
                                utVolumetricErrorInfo.stVolumetricError.unWBCLateCompletion = FALSE;
                            }
                            //!Either of the error will be sent, make unWBCCountingCompletedImproper to FALSE
                            utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE;
                        }

                        if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper)
                        {
                            if( ((20 * RBC_ACQ_TIME_LOW) > m_usnRBCCountTime) && (0 != m_usnRBCCountTime))
                            {
                                utVolumetricErrorInfo.stVolumetricError.unRBCEarlyCompletion = TRUE;
                            }
                            else
                            {
                                utVolumetricErrorInfo.stVolumetricError.unRBCEarlyCompletion = FALSE;
                            }
                            if(( (20 * RBC_ACQ_TIME_HIGH) <= m_usnRBCCountTime) || (10000 < m_usnRBCCountTime))
                            {
                                //jojo edited 1.5 sec tolarence
                                utVolumetricErrorInfo.stVolumetricError.unRBCLateCompletion = TRUE;
                            }
                            else
                            {
                                utVolumetricErrorInfo.stVolumetricError.unRBCLateCompletion = FALSE;
                            }
                            //!Either of the error will be sent, make unWBCCountingCompletedImproper to FALSE
                            utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE;
                        }

                        utVolumetricErrorInfoReAcq.unVolumetricError = utVolumetricErrorInfo.unVolumetricError;

                        //!Since the re-acquisition is enabled, clear the volumetric error.
                        utVolumetricErrorInfo.unVolumetricError = ZERO;
                        //!Disable the re-acquisition for the second time
                        usnReAcquisition = DISABLE_RE_ACQ;
                        //!Reset the parameter of the CPU2 for re-acquisition.
                        //stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[0] = CPU1_IPC_RESET_FOR_ACQ;
                        //IPCCpu1toCpu2IpcCmd(stCpu1IpcCommPtr);

                        //!Set the status to error check and then error status is
                        //! sent to Sitara
                        m_eCycleCompletion = eERROR_CHECK;
                    }
                }
                else
                {
					//!If no volumetric error then set count stop to 1. So the
					//! input to MBD for re-acquisition is disabled
                    BloodCellCounter_U.CountStop = 1;
                    //sprintf(DebugPrintBuf, "\r\n1 RE-AC\n");
                    //UartDebugPrint((int *)DebugPrintBuf, 40);
                }
            }
            else
            {
                //!Set the error code base on the error
                if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper)
                {
                    if( ((0 != m_usnWBCCountTime) && (20 * WBC_ACQ_TIME_LOW)> m_usnWBCCountTime))
                    {
                        utVolumetricErrorInfo.stVolumetricError.unWBCEarlyCompletion = TRUE;
                    }
                    if(( (20 * WBC_ACQ_TIME_HIGH) <= m_usnWBCCountTime)||( 10000 < m_usnWBCCountTime) )
                    {
                        //jojo edited 1.5 sec tolarence
                        utVolumetricErrorInfo.stVolumetricError.unWBCLateCompletion = TRUE;
                    }
                    utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE; //HN_Added on 10_5_2019
                }

                if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper)
                {
                    if( (0 !=  m_usnRBCCountTime) && (20 * RBC_ACQ_TIME_LOW) > m_usnRBCCountTime)
                    {
                        utVolumetricErrorInfo.stVolumetricError.unRBCEarlyCompletion = TRUE;
                    }
                    if(( (20 * RBC_ACQ_TIME_HIGH) <= m_usnRBCCountTime)||( 10000 < m_usnRBCCountTime) )
                    {
                        //jojo edited 1.5 sec tolarence
                        utVolumetricErrorInfo.stVolumetricError.unRBCLateCompletion = TRUE;
                    }
                    utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE; //HN_Added on 10_5_2019
                }
				//!Re-Acquisition is disabled for the second time and if the any
				//! of the acquisition is not started, then the data transmission
				//! is disabled and error is transmitted to Sitara
                if((utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted) || \
                        (utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted) || \
                        (utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper) || \
                        (utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper) || \
                        (utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotCompleted) || \
                        (utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotCompleted) || \
                        (utVolumetricErrorInfo.stVolumetricError.unWBCCountingStartedImproper) || \
                        (utVolumetricErrorInfo.stVolumetricError.unRBCCountingStartedImproper))

                {
					//!Set the status to error check and then error status is
					//! sent to Sitara
                    m_eCycleCompletion = eERROR_CHECK;

                    //!Send error packet
                    //SysErrorCommand(g_usnDataMode);

                    //!Set busy state to high
                    if(g_bBusyBitStatus == FALSE)
                    {
                        SPISetCommCtrlBusyInt();
                    }

                    sprintf(DebugPrintBuf, "\r\n eERROR_CHECK1\n");
                    UartDebugPrint((int *)DebugPrintBuf, 40);

                    sprintf(DebugPrintBuf, "\r\n2nd time error\n");
                    UartDebugPrint((int *)DebugPrintBuf, 40);
                    /*
                    //!Reset the CPU2 for next cycle acquisition
                    stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[0] = CPU1_IPC_RESET_FOR_ACQ;
                    IPCCpu1toCpu2IpcCmd(stCpu1IpcCommPtr);
                    */
                    if((utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted) && \
                         (utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted))
                    {
                        //m_eCycleCompletion = eERROR_TRANSMISSION;
                        sprintf(DebugPrintBuf, "\r\nSet to eERROR_TRANSMISSION\n");
                        UartDebugPrint((int *)DebugPrintBuf, 40);
                    }
                }
                else
                {
                    m_eCycleCompletion = eSEQ_COMPLETION;
                }
                sprintf(DebugPrintBuf, "\r\n3 RE-AC\n");
                UartDebugPrint((int *)DebugPrintBuf, 40);

                //utVolumetricErrorInfo.unVolumetricInfo = ZERO;

				//!Set the count stop to 1 and disable the re-acquisition and
				//! proceed for further draining
                BloodCellCounter_U.CountStop = 1;

				//!Enable re-acquisition for next sequence
                usnReAcquisition = ENABLE_RE_ACQ;
            }
            if(1 == BloodCellCounter_U.CountStop)
            {
				//!Enable re-acquisition for the next sequence
                usnReAcquisition = ENABLE_RE_ACQ;
            }
			//!Set Valve open start to False so that (TBD)
            BloodCellCounter_U.ValveOpenStart = FALSE;
            //CRH_RBC_WBC_TIME  - END

           // m_bPressureMonitor = FALSE; //CRH_PRESSURE_pressure end
			break;

			//!If the command received is Second data acquisition starated,
			//! then perform the below operation
		case CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_STARTED:
			//printf("---Cpu1 Rx: %ld from CPU2--\n", \
			//        stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0]);
			//!Reset the flag after reading the data from CPU2
			stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
			//!Reset the command received from CPU 2 to Zero
			stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;
			break;

			//!If the command received is Second data acquisition completed,
			//! then perform the below operation
		case CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_COMPLETED:
			//!Reset the flag after reading the data from CPU2
			stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
			//!Reset the command received from CPU 2 to Zero
			stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0] = ZERO;

			//!Set the Count stop to 2, which gives input to MBD to stop
			//! acquistion  (TBD)
			BloodCellCounter_U.CountStop = 2;

			break;

			//!If the command received is First data Analysis completed, then
			//! perform the below operation
		case CPU2_IPC_FIRST_COUNT_DATA_ANALYSIS_COMPLETED:
			//!Reset the flag after reading the data from CPU2
			stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
		    
			//!Reset the command received from CPU 2 to Zero
			stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;
			
			//!Update the WBC count result from CPU2 to SDRAM
			usnSdramAdcDataAddr[7] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1];
			//!Update the RBC count result from CPU2 to SDRAM
			usnSdramAdcDataAddr[8] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2];
			//!Update the PLT count result from CPU2 to SDRAM
			usnSdramAdcDataAddr[9] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[3];

			sprintf(DebugPrintBuf, "\r\n WbcCellCount:");
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, ":%ld ", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1]);
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, "\r\n RbcCellCount:");
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, ":%ld ", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2]);
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, "\r\n PltCellCount:");
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, ":%ld ", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[3]);
			UartDebugPrint((int *)DebugPrintBuf, 40);

			sprintf(DebugPrintBuf, "\r\n Algo Time:");
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, ":%ld ", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[4]);
			UartDebugPrint((int *)DebugPrintBuf, 40);


			//!If the re-acquisition is disabled, then set the state to
			//! transmission of the data.
			//!In case results are acuired from CPU2 during re-acuisition error,
			//! don't transmit data to Sitara
			if(DISABLE_RE_ACQ != usnReAcquisition)
			{
				//!Set the state to Results (WBC, RBC and PLT count to Sitara
			    m_eCycleCompletion = eRESULT_TRANSMISSION;
			}
			break;

			//!If the command received is Second data Analysis completed, then
			//! perform the below operation
		case CPU2_IPC_SECOND_COUNT_DATA_ANALYSIS_COMPLETED:
			//!Reset the flag after reading the data from CPU2
			stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
			
			//!Reset the command received from CPU 2 to Zero
			stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0] = ZERO;
			
			//!Update the WBC count result from CPU2 to SDRAM
			usnSdramAdcDataAddr[10] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1];
			//!Update the RBC count result from CPU2 to SDRAM
			usnSdramAdcDataAddr[11] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2];
			//!Update the PLT count result from CPU2 to SDRAM
			usnSdramAdcDataAddr[12] = stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[3];

			printf("\n CPU2_IPC_SECOND_COUNT_DATA_ANALYSIS_COMPLETED");
			printf("\n WBC Counts: %ld ",stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1]);
			printf("\n RBC Counts: %ld ",stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2]);
			printf("\n PLT Counts: %ld ",stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[3]);
			sprintf(DebugPrintBuf, "\r\n Second Count Data Analysis Completed ");
			UartDebugPrint((int *)DebugPrintBuf, 50);
			sprintf(DebugPrintBuf, "\r\n WbcCellCount:");
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, ":%ld ", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1]);
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, "\r\n RbcCellCount:");
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, ":%ld ", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2]);
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, "\r\n PltCellCount:");
			UartDebugPrint((int *)DebugPrintBuf, 40);
			sprintf(DebugPrintBuf, ":%ld ", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[3]);
			UartDebugPrint((int *)DebugPrintBuf, 40);
			break;

			//!If the command received is volumetric interrupt error, then
			//! perform the below operation
		case CPU2_IPC_VOLUMETRIC_INT_ERROR:
		    sprintf(DebugPrintBuf, "\r\nCPU2_IPC_VOLUMETRIC_INT_ERROR ");
		    UartDebugPrint((int *)DebugPrintBuf, 40);
			//!Reset the flag after reading the data from CPU2, if the flag is
			//! not reset, then the interrupt is continuously generated
            stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
			//!Reset the command received from CPU 2 to Zero
            stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;
			//!Set count stop to true and input is given to MBD (TBD)
            BloodCellCounter_U.CountStop = 1;
			//!Set volumetric error to true - Presently not used
            m_bVolumetricError = TRUE;
			break;

			//!If the command received is IPC reset done, then perform the
			//! below operation
		case CPU2_IPC_RESET_DONE:
            sprintf(DebugPrintBuf, "\r\n CPU2_IPC_RESET_DONE ");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            if(usnReAcquisition == DISABLE_RE_ACQ)
            {
                BloodCellCounter_U.CountStop = 2;
                //BloodCellCounter_U.CountStop = 1;
            }
		    //!Set the error if the reset is not done - Presently not used
			//!Reset the flag after reading the data from CPU2, if the flag is
			//! not reset, then the interrupt is continuously generated
			stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;//reset the receiver
			//!Reset the command received from CPU 2 to Zero
            stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;
		    break;

			//!If the command received is WBC Analysis start, then perform the
			//! below operation
		case CPU2_WBC_ANALYSIS_START:
		    sprintf(DebugPrintBuf, "\r\nCPU2_WBC_ANALYSIS_START\n ");
            UartDebugPrint((int *)DebugPrintBuf, 40);
			//!Reset the flag after reading the data from CPU2, if the flag is
			//! not reset, then the interrupt is continuously generated
            stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;//reset the receiver
			//!Reset the command received from CPU 2 to Zero
            stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;
		    break;

			//!If the command received is WBC Analysis end, then perform the
			//! below operation
        case CPU2_WBC_ANALYSIS_END:
            sprintf(DebugPrintBuf, "\r\nCPU2_WBC_ANALYSIS_END\n ");
            UartDebugPrint((int *)DebugPrintBuf, 40);
			//!Reset the flag after reading the data from CPU2, if the flag is
			//! not reset, then the interrupt is continuously generated
            stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;//reset the receiver
			//!Reset the command received from CPU 2 to Zero
            stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;
            break;

			//!If the command received is RBC_PLT Analysis start, then perform
			//! the below operation
		case CPU2_RBC_PLT_ANALYSIS_START:
            sprintf(DebugPrintBuf, "\r\nCPU2_RBC_PLT_ANALYSIS_START\n ");
            UartDebugPrint((int *)DebugPrintBuf, 40);
			//!Reset the flag after reading the data from CPU2, if the flag is
			//! not reset, then the interrupt is continuously generated
            stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;//reset the receiver
			//!Reset the command received from CPU 2 to Zero
            stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[ZERO] = ZERO;
		    break;

			//!If the command received is RBC_PLT Analysis end, then perform the
			//! below operation
		case CPU2_RBC_PLT_ANALYSIS_END:
            sprintf(DebugPrintBuf, "\r\nCPU2_RBC_PLT_ANALYSIS_END\n ");
            UartDebugPrint((int *)DebugPrintBuf, 40);
			//!Reset the flag after reading the data from CPU2, if the flag is
			//! not reset, then the interrupt is continuously generated
            stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;//reset the receiver
			//!Reset the command received from CPU 2 to Zero
            stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0] = ZERO;
		    break;

		case CPU2_VOLUMETRIC_SENSOR_DETECTION:
            stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;//reset the receiver
            //!Reset the command received from CPU 2 to Zero
            stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0] = ZERO;

            switch(stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1])
            {
            case 1:
                sprintf(DebugPrintBuf, "\r\nWBC Start@%ld\r\n ",g_unSequenceStateTime);
                UartDebugPrint((int *)DebugPrintBuf, 40);
                break;

            case 2:
                sprintf(DebugPrintBuf, "\r\nRBC Start@%ld\r\n ",g_unSequenceStateTime);
                UartDebugPrint((int *)DebugPrintBuf, 40);
                break;

            case 3:
                sprintf(DebugPrintBuf, "\r\nRBC Stop@%ld\r\n ",g_unSequenceStateTime);
                UartDebugPrint((int *)DebugPrintBuf, 40);
                break;

            case 4:
                sprintf(DebugPrintBuf, "\r\nWBC Stop@%ld\r\n ",g_unSequenceStateTime);
                UartDebugPrint((int *)DebugPrintBuf, 40);
                break;
            default:
                break;

            }
		    break;

		case CPU2_ONE_SECOND_DATA:
            stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;//reset the receiver
            //!Reset the command received from CPU 2 to Zero
            stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0] = ZERO;

            //sprintf(DebugPrintBuf, "\r\nCPU2_ONE_SECOND_DATA\n ");
            //UartDebugPrint((int *)DebugPrintBuf, 40);
            switch(stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[1])
            {
            case 1:
                //sprintf(DebugPrintBuf, "\r\n WBC_1Sec:");
                //UartDebugPrint((int *)DebugPrintBuf, 40);
                //sprintf(DebugPrintBuf, ":%ld \n", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2]);
                //UartDebugPrint((int *)DebugPrintBuf, 40);
                break;

            case 2:
                //sprintf(DebugPrintBuf, "\r\n RBC_1Sec:");
                //UartDebugPrint((int *)DebugPrintBuf, 40);
                //sprintf(DebugPrintBuf, ":%ld ", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[2]);
                //UartDebugPrint((int *)DebugPrintBuf, 40);
                //sprintf(DebugPrintBuf, "\r\n PLT_1Sec:");
                //UartDebugPrint((int *)DebugPrintBuf, 40);
                //sprintf(DebugPrintBuf, ":%ld \n", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[3]);
                //UartDebugPrint((int *)DebugPrintBuf, 40);
                break;
            }
		    break;

		default:

            sprintf(DebugPrintBuf, "\r\nCPU2 invalid command:%ld\n ", stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0]);
            UartDebugPrint((int *)DebugPrintBuf, 40);

			printf("\n---CPU2 invalid command %ld--\n",\
			        stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0]);
			//!Reset the flag after reading the data from CPU2, if the flag is
			//! not reset, then the interrupt is continuously generated
			stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0] = ZERO;
			//!Reset the command received from CPU 2 to Zero
			stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
			break;

		}
	}

	//!If error flag in IPC communication is observed, then clear the error flag
	//! and send command to Sitara indicating error in IPC communication
	if(stCpu1IpcCommPtr->ErrFlag == 1)
	{
		//printf("\n---Cpu1 Communication Error--\n");
		//!Reset the flag after reading the data from CPU2, if the flag is not
		//! reset, then the interrupt is continuously generated
		stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
		//!Clear the error flag for IPC communication
		stCpu1IpcCommPtr->ErrFlag = ZERO;
		//!Reset the command received from CPU 2 to Zero
		stCpu1IpcCommPtr->unCpu2ToCpu1RxBuf[0] = ZERO;
	}
}
/******************************************************************************/
/*!
 * \fn void SysGetWbcBloodCellInfo(uint16_t addrBloodCellInfoDefault)
 * \param addrBloodCellInfoDefault
 * \brief To copy default values for blood cell info structure
 * \brief Description:
 *      This function copy default values for blood cell info structure
 * \return void
 */
/******************************************************************************/
void SysGetWbcBloodCellInfo(uint32_t addrBloodCellInfoDefault)
{
	//!Blood cell structure Pointer
	ST_BLOOD_CELL_VOLT_VAL *stCpu2BloodCellInfoPtr;
	stCpu2BloodCellInfoPtr = (void *) addrBloodCellInfoDefault;
	//!Set the WBC blood cell threshold and parameters to WBC cell structure
	memcpy(&sstBloodCellStatisticsDefault[eCellWBC], stCpu2BloodCellInfoPtr, \
	        sizeof(ST_BLOOD_CELL_VOLT_VAL));

	sprintf(DebugPrintBuf, "\r\n WBC Thresholds:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, "\r\n CellHighVolt:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, ":%ld ", \
	        (uint32_t)sstBloodCellStatisticsDefault[eCellWBC].m_fCellHighVolt);
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, "\r\n CellLowVolt:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, ":%ld ", \
	        (uint32_t)sstBloodCellStatisticsDefault[eCellWBC].m_fCellLowVolt);
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, "\r\n deltaMinMax:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, ":%ld \r\n", \
	        (uint32_t)sstBloodCellStatisticsDefault[eCellWBC].m_deltaMinMax);
	UartDebugPrint((int *)DebugPrintBuf, 40);
}
/******************************************************************************/
/*!
 * \fn void SysGetRbcBloodCellInfo(uint16_t addrBloodCellInfoDefault)
 * \param addrBloodCellInfoDefault
 * \brief To copy default values for blood cell info structure
 * \brief Description:
 *      This function copy default values for blood cell info structure
 * \return void
 */
/******************************************************************************/
void SysGetRbcBloodCellInfo(uint32_t addrBloodCellInfoDefault)
{
	//!Blood cell structure pointer
	ST_BLOOD_CELL_VOLT_VAL *stCpu2BloodCellInfoPtr;
	stCpu2BloodCellInfoPtr = (void *) addrBloodCellInfoDefault;
	//!Set the WBC blood cell threshold and parameters to WBC cell structure
	memcpy(&sstBloodCellStatisticsDefault[eCellRBC], stCpu2BloodCellInfoPtr, \
	        sizeof(ST_BLOOD_CELL_VOLT_VAL));

	sprintf(DebugPrintBuf, "\r\n RBC Thresholds:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, "\r\n CellHighVolt:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, ":%ld ", \
	        (uint32_t)sstBloodCellStatisticsDefault[eCellRBC].m_fCellHighVolt);
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, "\r\n CellLowVolt:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, ":%ld ", \
	        (uint32_t)sstBloodCellStatisticsDefault[eCellRBC].m_fCellLowVolt);
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, "\r\n deltaMinMax:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, ":%ld \r\n", \
	        (uint32_t)sstBloodCellStatisticsDefault[eCellRBC].m_deltaMinMax);
	UartDebugPrint((int *)DebugPrintBuf, 40);
}
/******************************************************************************/
/*!
 * \fn void SysGetPltBloodCellInfo(uint16_t addrBloodCellInfoDefault)
 * \param addrBloodCellInfoDefault
 * \brief To copy default values for blood cell info structure
 * \brief Description:
 *      This function copy default values for blood cell info structure
 * \return void
 */
/******************************************************************************/
void SysGetPltBloodCellInfo(uint32_t addrBloodCellInfoDefault)
{
	//!Blood cell structure pointer
	ST_BLOOD_CELL_VOLT_VAL *stCpu2BloodCellInfoPtr;
	stCpu2BloodCellInfoPtr = (void *) addrBloodCellInfoDefault;
	//!Set the WBC blood cell threshold and parameters to WBC cell structure
	memcpy(&sstBloodCellStatisticsDefault[eCellPLT], stCpu2BloodCellInfoPtr, \
	        sizeof(ST_BLOOD_CELL_VOLT_VAL));

	sprintf(DebugPrintBuf, "\r\n PLT Thresholds:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, "\r\n CellHighVolt:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, ":%ld ", \
	        (uint32_t)sstBloodCellStatisticsDefault[eCellPLT].m_fCellHighVolt);
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, "\r\n CellLowVolt:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, ":%ld ", \
	        (uint32_t)sstBloodCellStatisticsDefault[eCellPLT].m_fCellLowVolt);
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, "\r\n deltaMinMax:");
	UartDebugPrint((int *)DebugPrintBuf, 40);
	sprintf(DebugPrintBuf, ":%ld \r\n", \
	        (uint32_t)sstBloodCellStatisticsDefault[eCellPLT].m_deltaMinMax);
	UartDebugPrint((int *)DebugPrintBuf, 40);
}
/******************************************************************************/
/*!
 * \fn void SysGetCpu2FwVer(uint16_t addrCpu2FwVer)
 * \param addrCpu2FwVer
 * \brief copy Cpu2 Fwver
 * \brief Description:
 *      This function to copy cpu2 firmware version
 * \return void
 */
/******************************************************************************/
void SysGetCpu2FwVer(uint32_t addrCpu2FwVer)
{
    uint16_t *Cpu2FwVer;
	Cpu2FwVer = (void *) addrCpu2FwVer;

	DELAY_US(10000);
	//memcpy(Cpu2FirmwareVersion, Cpu2FwVer, sizeof(Cpu2FirmwareVersion));
	//!Assign CPU2 version no.
	Cpu2FirmwareVersion[0] = Cpu2FwVer[0];
	Cpu2FirmwareVersion[1] = Cpu2FwVer[1];
	Cpu2FirmwareVersion[2] = Cpu2FwVer[2];

    sprintf(DebugPrintBuf, "\r\nVer:");
    UartDebugPrint((int *)DebugPrintBuf, 40);
    sprintf(DebugPrintBuf, " %d .", (int)Cpu2FwVer[0]);
    UartDebugPrint((int *)DebugPrintBuf, 40);
    sprintf(DebugPrintBuf, " %d .", (int)Cpu2FwVer[1]);
    UartDebugPrint((int *)DebugPrintBuf, 40);
    sprintf(DebugPrintBuf, " %d ", (int)Cpu2FwVer[2]);
    UartDebugPrint((int *)DebugPrintBuf, 40);

    printf("\n Updated CPU2 FW: ");
	printf("%s \n",Cpu2FirmwareVersion);
	//!Function to print CPU1 firmware version
	//SysUartPrintCpuFirmwareVer(1);
	//!Function to print CPU2 firmware version
	//SysUartPrintCpuFirmwareVer(2);
}
/******************************************************************************/
/*!
 * \fn void SysCycleCompletion(void)
 * \brief cycle completion task
 * \brief Description:
 *      Function to handle action on any cycle completion
 * \return void
 */
/******************************************************************************/
void SysCycleCompletion(void)
{
	//!This function is called to check of any of the sequence is completed
	//! from MBD, if any of the sequence is completed, then the corresponding
	//! action is taken accordingly
    SysMBDSequenceCompletion();
   	//!Check the staus of cycle completion
    switch(m_eCycleCompletion)
    {
		//!If the cycle completion is set to transmit the results, then send
		//! the WBC, RBC, PLT count results of the seqeunce to Sitara
        case eRESULT_TRANSMISSION:
			//!Function to transmit the results to Sitara based on the
			//! sequence running
            SysResultsTransmitToSitara();
			//!Set the cycle completion to default state, so that it doesn't
			//! go to any other state to perform any other unnecessary operations
            m_eCycleCompletion = eSEQ_COMPLETION;

            break;

		//!If the cycle completion is set to error check, check if any error
		//! is available, if any error is available, then frome the error
		//! packet ans send to Sitara. Error will be set in any state of the
		//! sequence. After transmission of pulse heights, cycle completion will
		//! be set error check, so that after results are sent, error should be
		//! transmitted immediately and do not weight for the completion of the
		//! sequence. Sequence may complete after some time of transmission of data.
		//! So error to be transmitted immediately after transmission of results
		//! and pulse heights.
        case eERROR_CHECK:
			//!Check if any error is present
            //if(ZERO != utErrorInfoCheck.ulnErrorInfo)
            {
                //!if any of the error is set, then frame the error packet
                //! and send error
                //! to Sitara. After sending error, send data to the processor
                //!Depending upon the data mode send error command to Sitara
                SysErrorCommand(g_usnDataMode);
            }

			//!Set the cycle completion state to seqence completion. The next
			//! step is to check for sequence completion
			m_eCycleCompletion = eDefault;
            break;

        case eSEQ_COMPLETION:
            //!Check for pending errors between data transmission and MBD
            //! sequence completion. If any of the error is present, then send it.
            /*
            if(ZERO != utErrorInfoCheck.ulnErrorInfo)
            {
                //!if any of the error is set, then frame the error packet and send error
                //! to Sitara. After sending error, send data to the processor
                CIFormErrorHandlingPacket(DELFINO_ERROR_MINOR_CMD);
            }
            else
            {
                SysSequenceCompletion();
                m_eCycleCompletion = eDefault;
            }*/
			//!Function to send sequence completion to Sitara after completion
			//! of the any counting sequence. If this command is not sent in
			//! whole blood mode or in any calibration mode, next sequence will
			//! not continue
            SysSequenceCompletion();
            break;

        case eERROR_TRANSMISSION:
            //!if any of the error is set, then frame the error packet
            //! and send error
            //! to Sitara. After sending error, send data to the processor
            //!Depending upon the data mode send error command to Sitara
            SysErrorCommand(g_usnDataMode);
            //!Set the cycle completion state to seqence completion. The next
            //! step is to check for sequence completion
            //m_eCycleCompletion = eERROR_COMPLETION;
            m_eCycleCompletion = eDELAY_CASE;
            break;

        case eDELAY_CASE:
            //!Set the cycle completion state to eERROR_COMPLETION. The next
            //! step is to check for sequence completion
            m_eCycleCompletion = eERROR_COMPLETION;
            break;

        case eERROR_COMPLETION:
            //!Function to transmit the results to Sitara based on the
            //! sequence running
            SysResultsTransmitToSitara();
            //!Set the cycle completion state to seqence completion. The next
            //! step is to check for sequence completion
            m_eCycleCompletion = eSEQ_COMPLETION;
            break;

        default:
            break;
    }
}
/******************************************************************************/
/*!
 * \fn void SysUpdateDspToMbd(void)
 * \brief To update inputs to MBD module
 * \brief Description:
 *      Here all motor related input parameters to MBD shall be updated
 * \return void
 */
/******************************************************************************/
void SysUpdateDspToMbd(void)
{
	#ifdef _ALPHA_5_BOARD_
	//!Update Motor 1 (X-Axis) status to MBD
	BloodCellCounter_U.StopMotorXaxis = pMotor4Pwm5->Status;
	//!Update Motor 2 (Diluent Syringe) status to MBD
	BloodCellCounter_U.StopMotorDiluentSyringe = pMotor2Pwm6->Status;
	//!Update Motor 3 (Lyse Syringe) status to MBD
	BloodCellCounter_U.StopMotorLyseSyringe = pMotor3Pwm2->Status;
	//!Update Motor 4 (Waste Syringe) status to MBD
	BloodCellCounter_U.StopMotorWasteSyringe = pMotor1Pwm8->Status;
	//!Update Motor 5 (Y-Axis) status to MBD
	BloodCellCounter_U.StopMotorYaxis = pMotor5Pwm1->Status;
	//!Update sample start button status to MBD
	BloodCellCounter_U.StartAspiration = uiSampleStartBtnPress;

	//!Uppdate X-Axis home position status
	BloodCellCounter_U.HomeStatus[WASTE_MOTOR] = OIHomeSensor1State();
	//!Uppdate Diluent syringe home position status
	BloodCellCounter_U.HomeStatus[DILUENT_MOTOR] = OIHomeSensor2State();
	//!Uppdate Sample Syringe home position status
	BloodCellCounter_U.HomeStatus[SAMPLE_MOTOR] = OIHomeSensor3State();
	//!Uppdate Waste Syringe home position status
	BloodCellCounter_U.HomeStatus[X_AXIS_MOTOR] = OIHomeSensor4State();
	//!Uppdate Y-Axis home position status
	BloodCellCounter_U.HomeStatus[Y_AXIS_MOTOR] = OIHomeSensor5State();
	//!Uppdate pressure status
	BloodCellCounter_U.Pressure = m_usnPressureAdc;  //HN_Added

	//!Update First dilution frequency value
	BloodCellCounter_U.InBubblingFrequency[FIRST_DILUTION_FREQ] = g_usnFirstDilutionFreq;//g_usnFirstDilutionFreq;
	//!Update Lyse Mixing frequency value
	BloodCellCounter_U.InBubblingFrequency[LYSE_MIXING_FREQ] = g_usnLyseMixingFreq;
	//!Update Final RBC Mixing frequency value
	BloodCellCounter_U.InBubblingFrequency[FINAL_RBC_MIXING_FREQ] = g_usnFinalRBCMixingFreq;
	//BloodCellCounter_U.InBubblingFrequency[FINAL_RBC_MIXING_FREQ] = MIN_MIXING_FREQ;


#if 0
	BloodCellCounter_U.HP_XAxis = OIHomeSensor1State();
	BloodCellCounter_U.HP_DiluentSyringe = OIHomeSensor2State();
	BloodCellCounter_U.HP_SampleLyseSyringe = OIHomeSensor3State();
	BloodCellCounter_U.HP_WasteSyinge = OIHomeSensor4State();
	BloodCellCounter_U.HP_YAxis = OIHomeSensor5State();
#endif
	#else
	//!Update Motor 1 (X-Axis) status to MBD
	BloodCellCounter_U.StopMotorXaxis = pMotor1Pwm8->Status;
	BloodCellCounter_U.StopMotorYaxis = pMotor2Pwm6->Status;
	BloodCellCounter_U.StopMotorLyseSyringe = pMotor3Pwm2->Status;
	BloodCellCounter_U.StopMotorWasteSyringe = pMotor4Pwm5->Status;
	BloodCellCounter_U.StopMotorDiluentSyringe = pMotor5Pwm1->Status;
	BloodCellCounter_U.StartAspiration = uiSampleStartBtnPress;
	BloodCellCounter_U.HP_XAxis = usnSensor1_HomeState;
	BloodCellCounter_U.HP_YAxis = OIHomeSensor2State();
	BloodCellCounter_U.HP_SampleLyseSyringe = OIHomeSensor3State();
	BloodCellCounter_U.HP_WasteSyinge = OIHomeSensor4State();
	BloodCellCounter_U.HP_DiluentSyringe = OIHomeSensor5State();
	#endif

//	SysCheckHomePosition();
	BloodCellCounter_U.LogicEnable = 1;
    //BloodCellCounter_U.Mode = g_usnCurrentMode ;
	//g_usnDataMode = g_usnCurrentMode;

	//!Dont change this delay time it will effect the count
	BloodCellCounter_U.Delay_BeforeCount = 25;
	//BloodCellCounter_U.Delay_BeforeCount = g_TestDataI2;
#if 1
	//!Update Major and minor command to MBD to start sequence.
	//!Update only if major and minor command is not zero
	//!Otherwise, the major command and minor command shall mix with any other
	//! running sequence
    if((0!=g_usnCurrentModeMajor)&&(0!=g_usnCurrentMode))
    {
		//!Update the major command to MBD
        BloodCellCounter_U.MajorCmd = g_usnCurrentModeMajor;
		//!Update the minor command to MBD
        BloodCellCounter_U.MinorCmd = g_usnCurrentMode;
    }
#endif
	//!Update the intermediate command to MBD dispense the diluent in pre-dilute mode.
    if(g_usnCurrentModeTemp != 0)
    {
        BloodCellCounter_U.PopUpCmd  = g_usnCurrentModeTemp;
    }
    //!if pressure releas valve is ON close it when startAsp is TRUE
    if(BloodCellCounter_U.StartAspiration == TRUE)
    {
        m_bVlaveCheckStatus = false;
        m_ucValveControlTime = ZERO;
    }
}
/******************************************************************************/
/*!
 * \fn void SysUpdateSampleStartButton(void)
 * \brief To update Sample start button polling status
 * \brief Description:
 *      Function to handle Sample start button switch
 * \return void
 */
/******************************************************************************/
void SysUpdateSampleStartButton(void)
{
	//!Input is given from MBD to check for polling the start button when the
	//! probe out for aspiration
	//!
    u_n8ContinousRunDelay++;
	if(BloodCellCounter_Y.StartPollAspirationGpio == TRUE)
	{
		//!Set the motors in idle mode when the probe is out for aspiration
	    SysMotorControlIdleMode();

	    // To enable Auto run Uncomment below line
	    //!If the  g_DiluentMixTime value is 957 then systme will go for auto run
	    //! this is for testing only
	    //! u_n8ContinousRunDelay increase every 20mSec so this will exicute on every 100 seconds
	    if((g_TestDataI1 == 957)&&(u_n8ContinousRunDelay>4000))
	    {
	        u_n8ContinousRunDelay=0;
	        g_bStartButtonStatus = TRUE;
	        u_n8ContinousRun++;
	        if(u_n8ContinousRun>300)
	        {
	            u_n8ContinousRun=0;
	            g_TestDataI1 =0;
	            g_bStartButtonStatus = FALSE;
	        }
	    }

#if START_BUTTON_ENABLE

	    //!Check whether the sample start button is enabled by Sitara
        if(g_bStartButtonStatus == TRUE)
        {
            //!If the Sample start button in enabled, clear the busy pin so that
            //! Sitara can sent any other command to Controller
            if(g_bBusyBitStatus == TRUE)
            {
                SPIClearCommCtrlBusyInt();  //CRH_TBD
            }
            //!Read the GPIO pin to check whether it is pressed or not. If it is
            //! pressed, then start aspiration
            if(!(Uint16)GPIO_ReadPin(SAMPLE_START_BUTTON_GPIO))
            {
                //!Set Sample start button to ture which is an input to MBD to indicate
                //! start aspirate
                uiSampleStartBtnPress = TRUE;

                //!As soon as the sample start button press is detected,
                //!disable the sample start button polling.
                g_bStartButtonStatus = FALSE;

                //!Since the sequence has started, pulll the busy pin high to
                //! to indicate Sitara that controller is busy in executing sequence
                if(g_bBusyBitStatus == FALSE)
                {
                    SPISetCommCtrlBusyInt();
                }
                //!Reset the sequence state time to Zero when the start button
                //! is pressed.
                //! So the sequence monitoring time starts from the beginneing
                //! which shall be used to monitor the seqeunce
                g_unSequenceStateTime = 0;
            }
            else
            {
                //!If the sample start button is not pressed, Set Sample
                //! start button to false which is an input to MBD to indicate
                //! do not aspirate since the key is not rpessed
                uiSampleStartBtnPress = FALSE;
            }
        }
#else
        //!Check whether the sample start button is pressed
		if(g_bStartButtonStatus == TRUE)
	    {

		    IGConfigLED(LED1_GPIO, 0);

		    //!Set Sample start button to ture which is an input to MBD to indicate
		    //! start aspirate
	        uiSampleStartBtnPress = TRUE;
            //!As soon as the sample start button press is detected,
            //!disable the sample start button press flag.
	        g_bStartButtonStatus = FALSE;
	        //! set the busy pin so that Sitara can not sent any other command to Controller
	        if(g_bBusyBitStatus == FALSE)
            {
                SPISetCommCtrlBusyInt();
            }

	        //!Enable the flag for ready for aspiration as soon as the trigger
	        //! button is pressed
	        m_bReadyforAspCommand = TRUE;

	        m_bPressureValveOn = FALSE;
	        m_ucPressureValveCounter = 250;
	        m_bValveExcited = FALSE;

            //!Reset the sequence state time to Zero when the start button
            //! is pressed.
            //! So the sequence monitoring time starts from the beginneing
            //! which shall be used to monitor the seqeunce
            g_unSequenceStateTime = 0;

            g_unSequenceStateTime1 = 0;

            //!Close the V7 valve if it is open
            SIUpdateValveStatus(ZERO, ZERO);
	    }
	    else
	    {
	        //! Set Sample start button to false which is an input to MBD to indicate
	        //! do not aspirate since the key is not pressed
	        uiSampleStartBtnPress = FALSE;
	    }
#endif  
	}
	else
	{
	    //!If the sample start button is not enabled, Set Sample
        //! start button to false which is an input to MBD to indicate
        //! do not aspirate since the key is not rpessed
		uiSampleStartBtnPress = FALSE;

		g_bStartButtonStatus = FALSE;
	}
}
/******************************************************************************/
/*!
 * \fn void SysUpdateMbdToDsp(void)
 * \brief To update outputs to Dsp module
 * \brief Description:
 *      Here all motor related ouput parameters to Dsp shall be updated
 * \return void
 */
/******************************************************************************/
void SysUpdateMbdToDsp(void)
{
	static Uint16 HiVoltageSwitch = ZERO;
	static bool b_TestVariable = FALSE;

	//!-----> Do not change anything  <-------//
	//!**************************************//
	int unCount;
    unCount = g_usnHealthindexCounter-1;
    if(unCount<0)
    {
        unCount = HEALTH_STATUS_ARRAY_SIZE-1;
    }
    //!---------------------------------------//
    //! **************************************//

    if(1 == g_TestDataI1)
    {
        if(0 == CpuTimer0.InterruptCount %5)
        {
            sprintf(DebugPrintBuf, "\r\nP: %ld", m_usnPressureAdc);
            UartDebugPrint((int *)DebugPrintBuf, 50);
        }
    }
    if(b_TestVariable)
    {
        //sprintf(DebugPrintBuf, "\r\n H: %ld", m_usnHgbAdc);
        //UartDebugPrint((int *)DebugPrintBuf, 50);
    }
    /*if(0 == CpuTimer0.InterruptCount %5)
    {
    }*/
    static bool bZappingOn;
    static bool bZappingOff;

    switch(BloodCellCounter_Y.XaxisPosition)
    {
    case 1:
            m_eXPosition = eASPIRATION;
            break;
    case 2:
            m_eXPosition = eRBC_BATH;
            break;
    case 3:
            m_eXPosition = eWBC_BATH;
            break;
    case 0:
            //m_eXPosition = eXPositionInvalid;
            break;
    case 4:
            //!if  Xmotor is moving out from Asp position through Homesensore
            m_eXPosition = eXPositionInvalid;
            break;
    default:
        break;
    }

//
/*    if(m_bZapInitiateFlag)
    {
        m_bZapInitiateFlag =  false;
        //!Perform only zap for 5sec
        cZapTime = ZAP_SEQUENCE_ZAPPING_TIME;

        //!GPIO used for toggling the LED1 is used for Controlling resistor
        //! R150 bypassing
        //! This has to be reverted after revising the board
        IGConfigHighVoltageModule(LED1_GPIO, 1);

        //Switch On for specific time
        IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_GPIO_OUTPUT, \
                HIGH_VOLTAGE_SWITCH_ON);
        IGConfigHighVoltageModule(ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT, \
                ZAP_VOLTAGE_ON);
        IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT, \
                HIGH_VOLTAGE_SWITCH_ON);

        g_unZappingTime = ZERO;
        bZappingOn = TRUE;

        //!Frame sequence Started command and send to Sitara
        CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);
    }*/

	pMotor4Pwm5->HomeSensorControl = BloodCellCounter_Y.X_AxisHP;//SKM_CHANGE_XAXIS

	//!Switch on the high voltage which is used during counting sequence.
	//! Input is given from MBD on as Power On count.
	//! When the power on count is set to Zero, switch off the high voltage
	if(BloodCellCounter_Y.PowerOn_Count == TRUE && HiVoltageSwitch == 0)
	{
		HiVoltageSwitch = 1;
		IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_GPIO_OUTPUT, \
		        HIGH_VOLTAGE_SWITCH_ON);

        IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT, \
                HIGH_VOLTAGE_SWITCH_ON);
		//printf("Switch ON High Voltage  \n");

	}
	else if(BloodCellCounter_Y.PowerOn_Count == FALSE && HiVoltageSwitch == 1)
	{
		HiVoltageSwitch = ZERO;
		IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_GPIO_OUTPUT, \
		        HIGH_VOLTAGE_SWITCH_OFF);

        IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT, \
                HIGH_VOLTAGE_SWITCH_OFF);
		//printf("Switch OFF High Voltage  \n");
	}

    //static unsigned char ucZapCount = 0;

    //!Perform the below operations when different states are received from MBD
    switch (BloodCellCounter_Y.STATE)
    {
/*        case eREADY_FOR_ASPIRATION:
            {
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
            }
            break;*/
        case eASPIRATION_KEY_PRESSED:
            {
                b_TestVariable = true;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
                //!Switch on the PWM of the HGB
                PWMHgbPwmControl(START, HGB_PWM_FREQUENCY);
            }
            break;
        //!If aspiration completed state is received from MBD, send aspiration
        //! completed command to Sitara based on the sequence running
        //! This command is sent to Sitara so that beep shall be given to
        //! indicate user that aspiration completed
        case eASPIRATION_COMPLETED_WB:
        {
            //!As soon as the aspiration is completed, reset the parameters of
            //! data acquisition in CPU2
            stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[0] = CPU1_IPC_RESET_FOR_ACQ;
            IPCCpu1toCpu2IpcCmd(stCpu1IpcCommPtr);
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
            BloodCellCounter_Y.STATE = eSTATE_INVALID;
            switch(g_usnCurrentMode)
            {

                case WHOLE_BLOOD_MODE:
                case PRE_DILUENT_MODE:
                case DISPENSE_THE_DILUENT:
                case START_PRE_DILUENT_COUNTING_CMD:
                case BODY_FLUID_MODE:
                case PARTIAL_BLOOD_COUNT_WBC:
                case PARTIAL_BLOOD_COUNT_RBC_PLT:
                    //!Frame aspiration completed command in patient handling Mode
                    CIFormPatientHandlingPacket(DELFINO_ASPIRATION_COMPLETED);
/*                    //!Set blank acquisition state to true to Acquire Blank hgb adc values
                    m_bHgBBlankAcq = TRUE;*/
                     break;
                case START_UP_SEQUENCE_MODE:   //HN_Added
                case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
                   // m_bHgBBlankAcq = TRUE;
                    break;
                case AUTO_CALIBRATION_WB:
                case COMMERCIAL_CALIBRATION_WBC:
                case COMMERCIAL_CALIBRATION_RBC:
                case COMMERCIAL_CALIBRATION_PLT:
                    //!Frame aspiration completed command in Calibration handling Mode
                     CIFormCalibrationHandlingPacket(DELFINO_ASPIRATION_COMPLETED);
                     //!Set blank acquisition state to true to Acquire Blank hgb adc values
                   //  m_bHgBBlankAcq = TRUE;
                     break;

                case QUALITY_CONTROL_WB:
                case QUALITY_CONTROL_BODY_FLUID:
                    //!Frame aspiration completed command in QC handling Mode
                    CIFormQualityHandlingPacket(DELFINO_ASPIRATION_COMPLETED);
                    //!Set blank acquisition state to true to Acquire Blank hgb adc values
                   // m_bHgBBlankAcq = TRUE;
                     break;
                case PRIME_WITH_EZ_CLEANSER_CMD:
                    //!Send the EZ_Cleanser is started to master
                    CIFormServiceHandlingPacket(PRIME_WITH_EZ_CLEANSER_CMD, true);
                    break;
                case SHUTDOWN_SEQUENCE:
                    //! Frame sequence started command and send to sitara
                    CIFormShutDownSequencePacket(DELFINO_SHUTDOWN_SEQUENCE_STARTED);
                    //!Set X-Axis position to invalid position
                    sprintf(DebugPrintBuf, "\r\n DELFINO_SHUTDOWN_SEQUENCE_STARTED \n");
                    UartDebugPrint((int *)DebugPrintBuf, 50);
                    //m_eXPosition = eXPositionInvalid;
                    break;

                default:
                       break;
            }
            //CIFormPatientHandlingPacket(DELFINO_ASPIRATION_COMPLETED);
            //CRH_HGB - Start the HGB PWM to swithc On the LED
            //PWMHgbPwmControl(START, HGB_PWM_FREQUENCY);
        }
            break;
        case eASPIRATION_COMPLETED_MONITORING:
            //!Set blank acquisition state to true to Acquire Blank hgb adc values
            m_bHgBBlankAcq = TRUE;
            sprintf(statDebugPrintBuf, "\n eASPIRATION_COMPLETED_MONITORING\n");
            UartDebugPrint((int *)statDebugPrintBuf, 50);
            break;

        case eDRAINING_BC_STARTED:
            {
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
            BloodCellCounter_Y.STATE = eSTATE_INVALID;
            }
            break;
        case eDRAINING_BC_COMPLETED:
            {
                sprintf(DebugPrintBuf, "\r\n eDRAINING_BC_COMPLETED: %ld\n", g_unSequenceStateTime);
                UartDebugPrint((int *)DebugPrintBuf, 50);
                BloodCellCounter_Y.STATE = eSTATE_INVALID;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
            }
            break;
/*
        case eWBC_BUBBLING_STARTED:
            {
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
            }
        break;
*/
        case eFIRST_DILUTION_COMPLETED:
            {
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
            }
            break;
        case eRBC_DISPENSATION_COMPLETED:
            {
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
            }
            break;
        case eWBC_RBC_BUBBLING_STARTED:
            {
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
            //g_bLyseMixingState = true;   //Testing
             sprintf(DebugPrintBuf, "\r\n eWBC_RBC_BUBBLING_STARTED: %ld\n", g_unSequenceStateTime);
             UartDebugPrint((int *)DebugPrintBuf, 50);
            g_bFirstMixingState = true;
            MixingTime = g_LyseMixTime;
            Counter = 0;
            }
            break;
        case eWBC_RBC_BUBBLING_INTER_STARTED:
            {
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
            //g_bPreDilMixingState = true;    //Testing
            sprintf(DebugPrintBuf, "\r\n eWBC_RBC_BUBBLING_INTER_STARTED: %ld\n", g_unSequenceStateTime);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            g_bFirstMixingState = true;
            MixingTime = g_RBCMixTime;
            Counter = 0;
            b_TestVariable = true;
            }
            break;
        /*case eTUBE_CLEANING_STATE:
             {
             //g_bTubeCleanVacuumMoni = true;

             //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
             //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
             }
             break;*/
        case eMONITOR_COUNT_TIME_VACCUM:
             {
             g_bCountTimeVacuumMoni = true;
             //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
             //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
             }
             break;
        case eCOUNTING_STARTED:
            {
             //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
             //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
                //!Switch on the PWM of the HGB
                PWMHgbPwmControl(START, HGB_PWM_FREQUENCY); //Switch on the HgB LED
            }
            break;
        case eCOUNTING_COMPLETED:
            {
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
            //!Start HGB pwm to take sample HGB value
            //PWMHgbPwmControl(START, HGB_PWM_FREQUENCY);
            }
            break;
        //!If the ZAP start command is received from MBD, perform ZAP operation.
        //! Switch on the High Voltage and zapping
    	case eZAP_START:
    	    {
            sprintf(statDebugPrintBuf, "eZAP_START: %ld \n",g_unSequenceStateTime);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;

            BloodCellCounter_Y.STATE = eSTATE_INVALID;

            if(g_usnCurrentMode == MBD_ZAP_INITIATE)
    	    {
    	        cZapTime = ZAP_SEQUENCE_ZAPPING_TIME;
    	        //!Frame sequence Started command and send to Sitara
    	        CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);
    	    }
    	    else
    	    {
    	        cZapTime = g_usnWholeBloodZapTime;
    	    }
            //!GPIO used for toggling the LED1 is used for Controlling resistor
            //! R150 bypassing
            //! This has to be reverted after revising the board
    	    IGConfigHighVoltageModule(LED1_GPIO, 1);

    		//Switch On for specific time

            IGConfigHighVoltageModule(ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT, \
                    ZAP_VOLTAGE_ON);

            IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT, \
                    HIGH_VOLTAGE_SWITCH_ON);
            IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_GPIO_OUTPUT, \
                    HIGH_VOLTAGE_SWITCH_ON);

            g_unZappingTime = ZERO;
            bZappingOn = TRUE;
            /*
            if(ucZapCount)
            {
                g_unZappingTime = 48;
                if(ucZapCount > 5)
                {
                    ucZapCount = 0;
                }
            }
            else
            {
                IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_GPIO_OUTPUT, \
                        HIGH_VOLTAGE_SWITCH_ON);
                IGConfigHighVoltageModule(ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT, \
                        ZAP_VOLTAGE_ON);
            }
            ucZapCount++;
            */
    	    }
    	    break;
        case eHGB_SAMPLE_ACQUISITION_START:
            {
            //sprintf(DebugPrintBuf,"\n\r eHGB_SAMPLE_ACQUISITION_START\n\r");
                        //UartDebugPrint((int *)DebugPrintBuf, 50);
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
            m_bHgBSampleAcq = TRUE;
            }
            break;
        case eDRAINING_AC_STARTED:
            {
            //sprintf(DebugPrintBuf,"\n\r eDRAINING_AC_STARTED\n\r");
             //           UartDebugPrint((int *)DebugPrintBuf, 50);
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
            }
            break;
        case eDRAINING_AC_COMPLETED:
            {
            //sprintf(DebugPrintBuf,"\n\r eDRAINING_AC_COMPLETED\n\r");
            //            UartDebugPrint((int *)DebugPrintBuf, 50);
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
            }
            break;
        case eDRAINING_FINAL_STARTED:
            {
            //sprintf(DebugPrintBuf,"\n\r eDRAINING_FINAL_STARTED\n\r");
            //            UartDebugPrint((int *)DebugPrintBuf, 50);
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
            }
            break;
        case eDRAINING_FINAL_COMPLETED:
            {
            //sprintf(DebugPrintBuf,"\n\r eDRAINING_FINAL_COMPLETED\n\r");
             //           UartDebugPrint((int *)DebugPrintBuf, 50);
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
            }
            break;
        case eBATH_FILLING_COMPLETED:
            {
            //sprintf(DebugPrintBuf,"\n\r eBATH_FILLING_COMPLETED\n\r");
             //           UartDebugPrint((int *)DebugPrintBuf, 50);
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
            }
            break;

        //!If the Ready for Dispensing command is received from MBD,
        //! Send command to Sitara stating needle is out for dispensing. This action
        //! is performed only in patient handling: Pre-dilute mode
    	case eREADY_FOR_DISPENSING:
    	{

    	    if(g_bBusyBitStatus == TRUE)
            {
                SPIClearCommCtrlBusyInt();
            }
    	    g_bLowVolumeDisp = TRUE;
            sprintf(DebugPrintBuf, "\n g_bLowVolumeDisp \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
    		CIFormPatientHandlingPacket(DELFINO_NEEDLE_READY_FOR_DISPENSING_CMD);
            //!Set all the motors to idle mode after completion of MBD sequence
    		SysMotorControlIdleMode();
    		g_usnCurrentModeTemp = ZERO;
            m_bReadyforAspCommand = TRUE;
    	}
    	break;

        //!If the Diluent Dispensing completed command is received from MBD,
        //! Send command to Sitara stating diluent dispensing is completed. This action
        //! is performed only in patient handling: Pre-dilute mode
    	case eDISPENSE_COMPLETED:
    	{
            if(g_bBusyBitStatus == TRUE)
            {
                SPIClearCommCtrlBusyInt();
            }
            g_bLowVolumeDisp = TRUE;
            sprintf(DebugPrintBuf, "\n g_bLowVolumeDisp \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
    		CIFormPatientHandlingPacket(DELFINO_DISPENSE_COMPLETED_CMD);
    		m_bReadyforAspCommand = TRUE;
            //!Set all the motors to idle mode after completion of MBD sequence
    		SysMotorControlIdleMode();
    	}
    	break;

    	//!If the Abort the current process is received from MBD, then perform
    	//! the below operations
    	case eABORT_PROCESS:
    	{
            //sprintf(statDebugPrintBuf, "eABORT_PROCESS: %ld \n",g_unSequenceStateTime);
           // UartDebugPrint((int *)DebugPrintBuf, 50);

    		//SysInitAbortProcess();
    		//CRH_CHANGE_13_09 - START
            /*if(g_bBusyBitStatus == TRUE)
            {
                SPIClearCommCtrlBusyInt();
            }*/
            //CRH_CHANGE_13_09
    	}
    	break;

#if 0
    	//!Ready for aspiration shall be sent to Sitara based on the input from
    	//!Aspiration polling and not based on the state with respect to ready
    	//! for aspiration from MBD. So the case is commented.
    	//!If ready for aspiration is received from MBD, perform the below operations
    	case eREADY_FOR_ASPIRATION:
    	{
    	    sprintf(DebugPrintBuf,"\n\r eREADY_FOR_ASPIRATION\n\r");
    	    UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Switch off the PWM of the HGB
            PWMHgbPwmControl(STOP, HGB_PWM_FREQUENCY);

    	    //!Clear the busy pin if it is set when the needle is out for ready
    	    //! for aspiration
            if(g_bBusyBitStatus == TRUE)
            {
                SPIClearCommCtrlBusyInt();
            }
            //!Based on the current mode, send Ready for aspiration command to
            //! Sitara based on the current mode
            switch(g_usnCurrentMode)
            {

            case WHOLE_BLOOD_MODE:
            case PRE_DILUENT_MODE:
            case DISPENSE_THE_DILUENT:
            case START_PRE_DILUENT_COUNTING_CMD:
            case BODY_FLUID_MODE:
            case PARTIAL_BLOOD_COUNT_WBC:
            case PARTIAL_BLOOD_COUNT_RBC_PLT:
                sprintf(statDebugPrintBuf, "DELFINO_READY_FOR_ASPIRATION\n");
                UartDebugPrint((int *)statDebugPrintBuf, 50);
                CIFormPatientHandlingPacket(DELFINO_SEQUENCE_COMPLETION); //Hari Change
                break;

            case AUTO_CALIBRATION_WB:
            case COMMERCIAL_CALIBRATION_WBC:
            case COMMERCIAL_CALIBRATION_RBC:
            case COMMERCIAL_CALIBRATION_PLT:
                CIFormCalibrationHandlingPacket(DELFINO_READY_FOR_ASPIRATION);
                 break;

            case QUALITY_CONTROL_WB:
            case QUALITY_CONTROL_BODY_FLUID:
                CIFormQualityHandlingPacket(DELFINO_READY_FOR_ASPIRATION);
                break;

                //PH_CHANGE_SAMPLE_START_BUTTON - START
            case PRIME_WITH_EZ_CLEANSER_CMD:
                CIFormServiceHandlingPacket(DELFINO_READY_FOR_ASPIRATION, false);
                break;

            case SHUTDOWN_SEQUENCE:
                CIFormShutDownSequencePacket(DELFINO_READY_FOR_ASPIRATION);
                break;
                //PH_CHANGE_SAMPLE_START_BUTTON - END
            default:
                break;
            }
    	}
    	break;

#endif
    	//!If the motor check is success, set motor check status to true (Success)
    	case MBD_MOTOR_CHK_SUCCESS:
    	{
    		g_bMotorChkStatus = TRUE;
    	}
    	break;

    	//!If the motor check is success, set motor check status to false (Failure)
    	case MBD_MOTOR_CHK_FALIURE:
		{
    		g_bMotorChkStatus = FALSE;
		}
		break;
		//!During EZ mode, the hypo-cleanser solution is dipensed to Both RBC
		//! and WBC bath and the sequence wiats for 15 min. During that scenario,
		//! set all the motors to idle mode
    	case eEZ_WAIT:
    	    SysMotorControlIdleMode();
    	break;
/*    	case eASPIRATION_COMPLETED_MONITORING:
    	    sprintf(DebugPrintBuf,"\n\r eASPIRATION_COMPLETED_MONITORING\n\r");
    	                UartDebugPrint((int *)DebugPrintBuf, 50);
             break;*/
    	case eVOLUMETRIC_CHECK_START:
            m_bRBCStartSensor = GPIO_ReadPin(GPIO_VOL1);

            m_bRBCStopSensor = GPIO_ReadPin(GPIO_VOL2);

            m_bWBCStartSensor = GPIO_ReadPin(GPIO_VOL3);

            m_bWBCStopSensor = GPIO_ReadPin(GPIO_VOL4);
            if(LIQUID_SENSOR_OFF == m_bRBCStartSensor)
            {
                //!Clear the RBC start LED error
                utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor = FALSE;
            }
            else
            {
                //!Set the RBC start LED error
                utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor = TRUE;
            }
            if(LIQUID_SENSOR_OFF == m_bRBCStopSensor)
            {
                //!Clear the RBC stop LED error
                utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor = FALSE;
            }
            else
            {
                //!Set the RBC stop LED error
                utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor = TRUE;
            }
            if(LIQUID_SENSOR_OFF == m_bWBCStartSensor)
            {
                //!Clear the WBC start LED error
                utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor = FALSE;
            }
            else
            {
                //!Set the WBC start LED error
                utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor = TRUE;
            }
            if(LIQUID_SENSOR_OFF == m_bWBCStopSensor)
            {
                //!Clear the WBC stop LED error
                utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor = FALSE;
            }
            else
            {
                //!Set the WBC stop LED error
                utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor = TRUE;
            }
    	    if((TRUE == utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor) ||
    	            (TRUE == utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor) ||
    	            (TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor) ||
    	            (TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor))
    	    {
    	        //!Abort the current process
    	        SysInitAbortProcess(TRUE);

    	        //!Frame the error command and abort the sequence
    	        SysErrorCommand(g_usnCurrentMode);
    	    }
    	    break;
    	default:
    	    break;
    }

    //CRH_CHANGE_13_09  - Hardcoded to success always -TBD
    //!Presently Motor check status is set to true by default - This had to be
    //! removed in future
    g_bMotorChkStatus = TRUE;

    //!If Zaping is on, high voltage should be on only for ZAPPING_TIME (2 seconds)
    //! After that is has to be switched off
    if(TRUE == bZappingOn)
    {
        //!Increment the ZAP interval
        g_unZappingTime++;

        //!When the Zapping time reaches the required time,
        //!switch off the high voltage and Zapping voltage and give input to MBD
        if(g_unZappingTime >= cZapTime)
        {
            IGConfigHighVoltageModule(ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT, \
                    ZAP_VOLTAGE_OFF);
            bZappingOff =  true;
/*            if(g_usnCurrentMode == MBD_ZAP_INITIATE)
            {
                DELAY_US(2000000); //2sec
            }*/
            //!GPIO used for toggling the LED1 is used for Controlling resistor
            //! R150 bypassing
            //! This has to be reverted after revising the board
            IGConfigHighVoltageModule(LED1_GPIO, 0);

            //!Reset the variable to false when Zapping sequence is comepleted
            g_unZappingTime = ZERO;
            bZappingOn = FALSE;
            //!If the sequence is MBD_ZAP_INITIATE then dont set the ZAP completion
            if(g_usnCurrentMode != MBD_ZAP_INITIATE)
            {
                //!Update Zapping completed to MBD
                BloodCellCounter_U.ZapComplete  = TRUE;
            }
        }
    }
    //! If ZAP time is completed, then after 2sec turn off the
    //!  WBC and RBC high voltage line  (solution for DSP-reset)
    if(bZappingOff)
    {
        //!Increment the ZAP interval
        g_unZappingTime++;

        if(100 < g_unZappingTime)
        {
            IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT, \
                    HIGH_VOLTAGE_SWITCH_OFF);
            IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_GPIO_OUTPUT, \
                    HIGH_VOLTAGE_SWITCH_OFF);

            sprintf(DebugPrintBuf, "\r\n bZappingOff :  \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            g_unZappingTime = ZERO;
            bZappingOff = false;
            if(g_usnCurrentMode == MBD_ZAP_INITIATE)
            {
                //!Update Zapping completed to MBD
                BloodCellCounter_U.ZapComplete  = TRUE;
            }
        }
    }

    //!If HGB data acquisition state is received from MBD for blank HGB data
    //! acquisition, then get the averaged data from the ADC
    if(TRUE == m_bHgBBlankAcq)
    {
        //!Initially 32 ADC samples were considered for averaging calculating
        //! the final HGB result. Presently the same is done at ADC section.
        //! It is tested and verified. So do not modify any logic for optimization
        m_ucHgBBlankAcqCount++;
        if(HGB_NO_SAMPLE_ACQ <= m_ucHgBBlankAcqCount)
        {
            m_usnHgbBlank = (uint16_t)m_usnHgbAdc;
            m_ucHgBBlankAcqCount = ZERO;
            m_bHgBBlankAcq = FALSE;
            b_TestVariable =  false;
//            PWMHgbPwmControl(STOP, HGB_PWM_FREQUENCY);
        }
    }
    else
    {
        //Do nothing
    }

    //!If HGB data acquisition state is received from MBD for blank HGB data
    //! acquisition, then get the averaged data from the ADC
//    if(TRUE == m_bHgBBlankAcqRepeat)
//    {
//        //!Initially 32 ADC samples were considered for averaging calculating
//        //! the final HGB result. Presently the same is done at ADC section.
//        //! It is tested and verified. So do not modify any logic for optimization
//        m_ucHgBBlankAcqCount++;
//        if(HGB_NO_SAMPLE_ACQ <= m_ucHgBBlankAcqCount)
//        {
//            m_usnHgbBlankRepeat = (uint16_t)m_usnHgbAdc;
////            if(m_usnHgbBlankRepeat>m_usnHgbBlank)
////            {
////                m_usnHgbBlank=m_usnHgbBlankRepeat
////            }
//            m_ucHgBBlankAcqCount = ZERO;
//            m_bHgBBlankAcqRepeat = FALSE;
//            b_TestVariable =  false;
//            sprintf(DebugPrintBuf, "\r\n m_usnHgbBlankRepeat %d m_usnHgbBlank%d\n", m_usnHgbBlankRepeat,m_usnHgbBlank);
//            UartDebugPrint((int *)DebugPrintBuf, 50);
//            PWMHgbPwmControl(STOP, HGB_PWM_FREQUENCY);
//        }
//
//    }
//    else
//    {
//        //Do nothing
//    }


    //!If HGB data acquisition state is received from MBD for sample HGB data
    //! acquisition, then get the averaged data from the ADC
    if(TRUE == m_bHgBSampleAcq)
    {
        //!Initially 32 ADC samples were considered for averaging calculating
        //! the final HGB result. Presently the same is done at ADC section.
        //! It is tested and verified. So do not modify any logic for optimization
        m_ucHgBSampleAcqCount++;
        if(HGB_NO_SAMPLE_ACQ <= m_ucHgBSampleAcqCount)
        {
            m_usnHgbSample = (uint16_t)m_usnHgbAdc;
            m_ucHgBSampleAcqCount = ZERO;
            m_bHgBSampleAcq = FALSE;
            sprintf(DebugPrintBuf, "\r\n m_usnHgbSample %d\n", m_usnHgbSample);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            b_TestVariable =  false;
            PWMHgbPwmControl(STOP, HGB_PWM_FREQUENCY);
        }
    }
    else
    {
        //Do nothing
    }


    //!Get the sequence state from MBD
    uint16_t usnSequenceState = BloodCellCounter_Y.STATE;

    //!Perform sequence Monitoring based on the sequence currently running
    switch(g_usnCurrentMode)
    {
        case WHOLE_BLOOD_MODE:
        //!Do not check for START_PRE_DILUENT_COUNTING_CMD during cycle completion
        //!Cycle completion is mainly for the main mode (pre-dilute mode) and
        //!not the intermediate mode of starting the pre-dilute sequence after
        //!diluting the blood externally in pre-dilute mode
        case START_PRE_DILUENT_COUNTING_CMD:
        case PRE_DILUENT_MODE:
        case BODY_FLUID_MODE:
        case START_UP_SEQUENCE_MODE:
        case AUTO_CALIBRATION_WB:
        case COMMERCIAL_CALIBRATION_WBC:
        case COMMERCIAL_CALIBRATION_RBC:
        case COMMERCIAL_CALIBRATION_PLT:
        case QUALITY_CONTROL_WB:
        case QUALITY_CONTROL_BODY_FLUID:
        case PARTIAL_BLOOD_COUNT_WBC:
        case PARTIAL_BLOOD_COUNT_RBC_PLT:
        case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
        case MBD_STARTUP_FAILED_CMD:
            //!Sequence monitoring check function is called
            SysStateIntervalCheck(usnSequenceState);
            break;

        case MBD_WAKEUP_START:
        case PRIME_WITH_RINSE_SERVICE_HANDLING:
        case PRIME_WITH_LYSE_SERVICE_HANDLING:
        case PRIME_WITH_DILUENT_SERVICE_HANDLING:
        case PRIME_ALL_SERVICE_HANDLING:
        case BACK_FLUSH_SERVICE_HANDLING:
        case ZAP_APERTURE_SERVICE_HANDLING:
        case DRAIN_BATH_SERVICE_HANDLING:
        case DRAIN_ALL_SERVICE_HANDLING:
        case CLEAN_BATH_SERVICE_HANDLING:
        case HEAD_RINSING_SERVICE_HANDLING:
        case PROBE_CLEANING_SERVICE_HANDLING:
        case MBD_TO_SHIP_SEQUENCE_START:
        case SHUTDOWN_SEQUENCE:
        case COUNTING_TIME_SEQUENCE:
        case FLOW_CALIBRATION:
        case PRIME_WITH_EZ_CLEANSER_CMD:
        case MBD_USER_ABORT_PROCESS:
        case RINSE_PRIME_FLOW_CALIBRATION:
        case MBD_USER_ABORT_Y_AXIS_PROCESS:
        case MBD_VOLUMETRIC_BOARD_CHECK:
        case MBD_SLEEP_PROCESS:
        case MBD_VOL_TUBE_EMPTY:
        case MBD_ALL_MOTOR_Y_AXIS_CHECK:
        case X_AXIS_MOTOR_CHECK:
        case Y_AXIS_MOTOR_CHECK:
        case DILUENT_SYRINGE_MOTOR_CHECK:
        case WASTE_SYRINGE_MOTOR_CHECK:
        case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:
        case MBD_BATH_FILL_CMD:
        //case COUNTING_TIME_SEQUENCE://SKM_COUNTSTART_CHANGE
            //!Sequence monitoring check function is only for sending sequence
            //! started packet in any of the service handling
            if(eSEQUENCE_STARTED == usnSequenceState)
            {
                //m_eXPosition = eXPositionInvalid;
                SysStateIntervalCheck(usnSequenceState);
            }
            break;
        default:
            break;
    }

    //!Ready for aspiration to be sent to sitara based on the sample start button polling
    //! and it has to be sent only once
    if((TRUE == m_bReadyforAspCommand) && (TRUE == BloodCellCounter_Y.StartPollAspirationGpio))
    {
        //!Clear the busy pin if it is set when the needle is out for ready
        //! for aspiration
        if(g_bBusyBitStatus == TRUE)
        {
            SPIClearCommCtrlBusyInt();
        }

        m_bReadyforAspCommand = FALSE;

        //!Based on the current mode, send Ready for aspiration command to
        //! Sitara based on the current mode
        switch(g_usnCurrentMode)
        {

        case WHOLE_BLOOD_MODE:
        case PRE_DILUENT_MODE:
        case DISPENSE_THE_DILUENT:
        case START_PRE_DILUENT_COUNTING_CMD:
        case BODY_FLUID_MODE:
        case PARTIAL_BLOOD_COUNT_WBC:
        case PARTIAL_BLOOD_COUNT_RBC_PLT:
             CIFormPatientHandlingPacket(DELFINO_READY_FOR_ASPIRATION);
             sprintf(DebugPrintBuf, "\r\n DELFINO_READY_FOR_ASPIRATION \n");
             UartDebugPrint((int *)DebugPrintBuf, 50);
            break;

        case AUTO_CALIBRATION_WB:
        case COMMERCIAL_CALIBRATION_WBC:
        case COMMERCIAL_CALIBRATION_RBC:
        case COMMERCIAL_CALIBRATION_PLT:
            CIFormCalibrationHandlingPacket(DELFINO_READY_FOR_ASPIRATION);
             break;

        case QUALITY_CONTROL_WB:
        case QUALITY_CONTROL_BODY_FLUID:
            CIFormQualityHandlingPacket(DELFINO_READY_FOR_ASPIRATION);
            break;

            //PH_CHANGE_SAMPLE_START_BUTTON - START
        case PRIME_WITH_EZ_CLEANSER_CMD:
            CIFormServiceHandlingPacket(DELFINO_READY_FOR_ASPIRATION, false);
            //!Set X-Axis position to invalid position
            //m_eXPosition = eXPositionInvalid;
            break;

        case SHUTDOWN_SEQUENCE:
            CIFormShutDownSequencePacket(DELFINO_READY_FOR_ASPIRATION);
            //!Set X-Axis position to invalid position
            //m_eXPosition = eXPositionInvalid;
            break;
            //PH_CHANGE_SAMPLE_START_BUTTON - END
        default:
            break;
        }
    }

    //!Aspiration key press recieved to be sent to sitara based on the sample start button press
    if(eASPIRATION_KEY_PRESSED == BloodCellCounter_Y.STATE)
    {
        //!Set the busy pin if it is not set when the aspiration key press is
        //! recieved from Sitara
        if(g_bBusyBitStatus == FALSE)
        {
            SPISetCommCtrlBusyInt();
        }

        //!Based on the current mode, send Ready for aspiration command to
        //! Sitara based on the current mode
        switch(g_usnCurrentMode)
        {

        case WHOLE_BLOOD_MODE:
        case PRE_DILUENT_MODE:
        case DISPENSE_THE_DILUENT:
        case START_PRE_DILUENT_COUNTING_CMD:
        case BODY_FLUID_MODE:
        case PARTIAL_BLOOD_COUNT_WBC:
        case PARTIAL_BLOOD_COUNT_RBC_PLT:
            CIFormPatientHandlingPacket(DELFINO_ASPIRATION_KEY_PRESS_RECIEVED);
            break;

        case AUTO_CALIBRATION_WB:
        case COMMERCIAL_CALIBRATION_WBC:
        case COMMERCIAL_CALIBRATION_RBC:
        case COMMERCIAL_CALIBRATION_PLT:
            CIFormCalibrationHandlingPacket(DELFINO_ASPIRATION_KEY_PRESS_RECIEVED);
             break;

        case QUALITY_CONTROL_WB:
        case QUALITY_CONTROL_BODY_FLUID:
            CIFormQualityHandlingPacket(DELFINO_ASPIRATION_KEY_PRESS_RECIEVED);
            break;

            //PH_CHANGE_SAMPLE_START_BUTTON - START
        case PRIME_WITH_EZ_CLEANSER_CMD:
            CIFormServiceHandlingPacket(DELFINO_ASPIRATION_KEY_PRESS_RECIEVED, false);
            //!Set X-Axis position to invalid position
            //m_eXPosition = eXPositionInvalid;;
            break;

        case SHUTDOWN_SEQUENCE:
            CIFormShutDownSequencePacket(DELFINO_ASPIRATION_KEY_PRESS_RECIEVED);
            //!Set X-Axis position to invalid position
            //m_eXPosition = eXPositionInvalid;
            break;
            //PH_CHANGE_SAMPLE_START_BUTTON - END
        default:
            break;
        }
    }
}
/******************************************************************************/
/*!
 * \fn void SysSetParametersDefaultValue(void)
 * \brief Toset parameter defaults value
 * \brief Description:
 *      Function to set Pameter default values for LED1 and Home sensor control
 * \return void
 */
/******************************************************************************/
void SysSetParametersDefaultValue(void)
{
    //!Turn off the LED1
	//IGConfigLED(LED1_GPIO, ZERO);
	//pMotor1Pwm8->HomeSensorControl = SENSOR_INTERRUPT_ENABLE;
	pMotor4Pwm5->HomeSensorControl = SENSOR_INTERRUPT_ENABLE;//SKM_CHANGE_XAXIS
}
/******************************************************************************/
/*!
 * \fn void SysMotor_Valve_Control(void)
 * \brief Motor & cvalve control
 * \brief Description:
 *      This function controls the motor and valves based in mbd input
 * \return void
 */
/******************************************************************************/
void SysMotor_Valve_Control(void)
{
	  static boolean_T OneTimeFlag=ZERO;

#if 1

	  //!If the motor 1 status is completed and the displacement is reached to Zero,
	  //! Set the motor to idle state and disable the motor
  if(pMotor1Pwm8->Status == COMPLETED && \
          BloodCellCounter_Y.Displacement[WASTE_MOTOR] == ZERO)
  {
	  pMotor1Pwm8->Status = IDLE;
	  //SMDSetMotorDisable(MOTOR1_SELECT);
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf(" M1 steps done \n");
#endif
  }
#ifdef _ALPHA_5_BOARD_
  //!If the motor 2 status is completed and the displacement is reached to Zero,
  //! Set the motor to idle state and disable the motor
  if(pMotor2Pwm6->Status == COMPLETED && \
          BloodCellCounter_Y.Displacement[DILUENT_MOTOR] == ZERO)
#else
  if(pMotor2Pwm6->Status == COMPLETED && \
          BloodCellCounter_Y.Displacement[Y_AXIS_MOTOR] == ZERO)
  #endif
  {
  	#ifdef _ALPHA_5_BOARD_
  	#else
	  SMDSetMotorDisable(MOTOR2_SELECT);
	  #endif
	  pMotor2Pwm6->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf(" M2 step done \n");
#endif
  }
#endif

    //!If the motor 3 status is completed and the displacement is reached to Zero,
    //! Set the motor to idle state and disable the motor
  if(pMotor3Pwm2->Status == COMPLETED && \
          BloodCellCounter_Y.Displacement[SAMPLE_MOTOR] == ZERO)
  {
	  pMotor3Pwm2->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf(" M3 step done \n");
#endif
  }
    //!If the motor 4 status is completed and the displacement is reached to Zero,
    //! Set the motor to idle state and disable the motor
  if(pMotor4Pwm5->Status == COMPLETED && \
          BloodCellCounter_Y.Displacement[X_AXIS_MOTOR] == ZERO)
  {
	  pMotor4Pwm5->Status = IDLE;
	  SMDSetMotorDisable(MOTOR4_SELECT);
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf(" M4 step done \n");
#endif

  }
  //!If the motor 5 status is completed and the displacement is reached to Zero,
  //! Set the motor to idle state and disable the motor
  #ifdef _ALPHA_5_BOARD_
  if(pMotor5Pwm1->Status == COMPLETED && \
          BloodCellCounter_Y.Displacement[Y_AXIS_MOTOR] == ZERO)
  #else
  if(pMotor5Pwm1->Status == COMPLETED && \
          BloodCellCounter_Y.Displacement[DILUENT_MOTOR] == ZERO)
  #endif
  {
	  pMotor5Pwm1->Status = IDLE;
	  #ifdef _ALPHA_5_BOARD_
	  SMDSetMotorDisable(MOTOR5_SELECT);
	  #else
	  #endif
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf(" M5 step done \n");
#endif
  }

  //if(BloodCellCounter_Y.CycleFlag == FALSE)
  //Default value of g_usnCurrentMode is Zero
  //Default and intial state of cycle flag need to be changed to High Value.
  //If both the values are zero by default, then Service handling
  //status will be sent coninuously
  //if(BloodCellCounter_Y.CycleFlag == K_CYCLE_DEFAULT)

  //!If the Cycle flag is set to K_CYCLE_DEFAULT and valve check status is set to False (TBD)
  if((BloodCellCounter_Y.CycleFlag == K_CYCLE_DEFAULT) && \
          (FALSE == m_bVlaveCheckStatus))
  {

      //Diluent syringe performance testing
      SysBinArrayToInt();//SKM_CHANGE_VALVE
      SIUpdateValveStatus(valveStatus1, valveStatus2);
	  OneTimeFlag=ZERO;

#ifdef _ALPHA_5_BOARD_
	  //!If some motor displacement is given from MBD and presently if the
	  //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[WASTE_MOTOR] > ZERO && \
	          pMotor1Pwm8->Status == IDLE)
	  {
	      //!PH_CHANGE_07_MAR_17 - START

	      //!During home position verification, move the motros irrespective of
	      //!any condition (Maximum displacement, direction, current position)
	      {
	          SMDStart_Motor1(BloodCellCounter_Y.Displacement[WASTE_MOTOR], \
	                            BloodCellCounter_Y.Direction[WASTE_MOTOR], \
	                            BloodCellCounter_Y.Frequency[WASTE_MOTOR]);
	      }
	      //!Validate the X-Axis for the conditions mentioned
	      //!If the displacment provided goes beyond the limit of the X-Asis
	      //!supproted distance
	      //!If the X-Axis is at home position and home position is not detected
	      //!If the X-Axis is not at home position and home position is detected
	      //if(((pMotor1Pwm8->iMotorPosition + BloodCellCounter_Y.Displacement[WASTE_MOTOR]) <= MAX_X_DISTANCE)||
	      if(((pMotor1Pwm8->iMotorPosition) <= MAX_WASTE_DISTANCE)||
	        ((ZERO == pMotor1Pwm8->iMotorPosition) && (true == OIHomeSensor1State()))||
	        ((ZERO != pMotor1Pwm8->iMotorPosition) && (false == OIHomeSensor1State())))
	      {
	          SMDStart_Motor1(BloodCellCounter_Y.Displacement[WASTE_MOTOR], \
		          BloodCellCounter_Y.Direction[WASTE_MOTOR], \
		          BloodCellCounter_Y.Frequency[WASTE_MOTOR]);
	      }
	      else
	      {
	          //!Send an error to UI indicating X-Axis issue
	      }
	      //!PH_CHANGE_07_MAR_17 - END
	  }

    //!If some motor displacement is given from MBD and presently if the
    //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[DILUENT_MOTOR] > ZERO && \
	          pMotor2Pwm6->Status == IDLE)
	  {
	      //!PH_CHANGE_07_MAR_17 - START

          //!During home position verification, move the motros irrespective of
          //!any condition (Maximum displacement, direction, current position)
          //if()
	      {
	          SMDStart_Motor2(BloodCellCounter_Y.Displacement[DILUENT_MOTOR], \
                    BloodCellCounter_Y.Direction[DILUENT_MOTOR], \
                    BloodCellCounter_Y.Frequency[DILUENT_MOTOR]);
	      }
	      //!Validate the Diluent for the conditions mentioned
          //!If the displacment provided goes beyond the limit of the Diluent
          //!supproted distance
          //!If the Diluent is at home position and home position is not detected
          //!If the Diluent is not at home position and home position is detected
	      if((pMotor2Pwm6->iMotorPosition <= MAX_DILUENT_DISTANCE)||
          ((ZERO == pMotor2Pwm6->iMotorPosition) && (true == OIHomeSensor2State()))||
          ((ZERO != pMotor2Pwm6->iMotorPosition) && (false == OIHomeSensor2State())))
	      {
	          SMDStart_Motor2(BloodCellCounter_Y.Displacement[DILUENT_MOTOR], \
	                  BloodCellCounter_Y.Direction[DILUENT_MOTOR], \
	                  BloodCellCounter_Y.Frequency[DILUENT_MOTOR]);
	      }
	      else
	      {
	          //!Through an error to UI indicating Diluent syringe issue
	      }
	      //!PH_CHANGE_07_MAR_17 - END
	  }

	    //!If some motor displacement is given from MBD and presently if the
	    //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[SAMPLE_MOTOR] > ZERO && \
	          pMotor3Pwm2->Status == IDLE)
	  {
	      //!PH_CHANGE_07_MAR_17 - START
          //!During home position verification, move the motros irrespective of
          //!any condition (Maximum displacement, direction, current position)
          //if()
	      {
	          SMDStart_Motor3(BloodCellCounter_Y.Displacement[SAMPLE_MOTOR], \
                    BloodCellCounter_Y.Direction[SAMPLE_MOTOR], \
                    BloodCellCounter_Y.Frequency[SAMPLE_MOTOR]);
	      }

          //!Validate the Sample syringe for the conditions mentioned
          //!If the displacment provided goes beyond the limit of the sample syringe
          //!supproted distance
          //!If the sample is at home position and home position is not detected
          //!If the sample is not at home position and home position is detected
          if((pMotor3Pwm2->iMotorPosition <= MAX_SAMPLE_DISTANCE)||
          ((ZERO == pMotor3Pwm2->iMotorPosition) && (true == OIHomeSensor3State()))||
          ((ZERO != pMotor3Pwm2->iMotorPosition) && (false == OIHomeSensor3State())))
          {
              SMDStart_Motor3(BloodCellCounter_Y.Displacement[SAMPLE_MOTOR], \
		          BloodCellCounter_Y.Direction[SAMPLE_MOTOR], \
		          BloodCellCounter_Y.Frequency[SAMPLE_MOTOR]);
          }
          else
          {
              //!Through an error to UI indicating sample syringe issue
          }
          //!PH_CHANGE_07_MAR_17 - END
	  }

	    //!If some motor displacement is given from MBD and presently if the
	    //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[X_AXIS_MOTOR] > ZERO && \
	          pMotor4Pwm5->Status == IDLE)
	  {
	      //!PH_CHANGE_07_MAR_17 - START
          //!During home position verification, move the motros irrespective of
          //!any condition (Maximum displacement, direction, current position)
          //if()
	      {
              SMDStart_Motor4(BloodCellCounter_Y.Displacement[X_AXIS_MOTOR], \
                      BloodCellCounter_Y.Direction[X_AXIS_MOTOR], \
                      BloodCellCounter_Y.Frequency[X_AXIS_MOTOR]);
	      }

          //!Validate the waste syringe for the conditions mentioned
          //!If the displacment provided goes beyond the limit of the sample syringe
          //!supproted distance
          //!If the waste syringe is at home position and home position is not detected
          //!If the waste syringe is not at home position and home position is detected
          //if((pMotor4Pwm5->iMotorPosition <= MAX_WASTE_DISTANCE)||
	      if(((pMotor4Pwm5->iMotorPosition + BloodCellCounter_Y.Displacement[X_AXIS_MOTOR]) <= MAX_X_DISTANCE)||
          ((ZERO == pMotor4Pwm5->iMotorPosition) && (true == OIHomeSensor4State()))||
          ((ZERO != pMotor4Pwm5->iMotorPosition) && (false == OIHomeSensor4State())))
          {
              SMDStart_Motor4(BloodCellCounter_Y.Displacement[X_AXIS_MOTOR], \
                      BloodCellCounter_Y.Direction[X_AXIS_MOTOR], \
                      BloodCellCounter_Y.Frequency[X_AXIS_MOTOR]);
          }
          else
          {
              //!Through an error to UI indicating waste syringe issue
          }
          //!PH_CHANGE_07_MAR_17 - END
	  }

	    //!If some motor displacement is given from MBD and presently if the
	    //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[Y_AXIS_MOTOR] > ZERO && \
	          pMotor5Pwm1->Status == IDLE)
	  {
	      //!PH_CHANGE_07_MAR_17 - START

          //!During home position verification, move the motros irrespective of
          //!any condition (Maximum displacement, direction, current position)
          //if()
          {
              SMDStart_Motor5(BloodCellCounter_Y.Displacement[Y_AXIS_MOTOR], \
                   BloodCellCounter_Y.Direction[Y_AXIS_MOTOR], \
                   BloodCellCounter_Y.Frequency[Y_AXIS_MOTOR]);
          }

	      //!Validate the waste syringe for the conditions mentioned
	      //!If the displacment provided goes beyond the limit of the sample syringe
	      //!supproted distance
	      //!If the waste syringe is at home position and home position is not detected
	      //!If the waste syringe is not at home position and home position is detected
	      if((pMotor5Pwm1->iMotorPosition <= MAX_Y_AXIS_DISTANCE)||
	      ((ZERO == pMotor5Pwm1->iMotorPosition) && (true == OIHomeSensor5State()))||
          ((ZERO != pMotor5Pwm1->iMotorPosition) && (false == OIHomeSensor5State())))
          {
	          SMDStart_Motor5(BloodCellCounter_Y.Displacement[Y_AXIS_MOTOR], \
		          BloodCellCounter_Y.Direction[Y_AXIS_MOTOR], \
		          BloodCellCounter_Y.Frequency[Y_AXIS_MOTOR]);
          }
	      else
	      {
	          //!Through an error to UI indicating Y-Axis issue
	      }
	      //!PH_CHANGE_07_MAR_17 - END
	  }
#else
	    //!If some motor displacement is given from MBD and presently if the
	    //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[X_AXIS_MOTOR] > ZERO && \
	          pMotor1Pwm8->Status == IDLE)
	  {
		  SMDStart_Motor1(BloodCellCounter_Y.Displacement[X_AXIS_MOTOR], \
		          BloodCellCounter_Y.Direction[X_AXIS_MOTOR], \
		          BloodCellCounter_Y.Frequency[X_AXIS_MOTOR]);
	  }

	    //!If some motor displacement is given from MBD and presently if the
	    //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[Y_AXIS_MOTOR] > ZERO && \
	          pMotor2Pwm6->Status == IDLE)
	  {
		  SMDStart_Motor2(BloodCellCounter_Y.Displacement[Y_AXIS_MOTOR], \
		          BloodCellCounter_Y.Direction[Y_AXIS_MOTOR], \
		          BloodCellCounter_Y.Frequency[Y_AXIS_MOTOR]);
	  }

	    //!If some motor displacement is given from MBD and presently if the
	    //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[SAMPLE_MOTOR] > ZERO && \
	          pMotor3Pwm2->Status == IDLE)
	  {
		  SMDStart_Motor3(BloodCellCounter_Y.Displacement[SAMPLE_MOTOR], \
		          BloodCellCounter_Y.Direction[SAMPLE_MOTOR], \
		          BloodCellCounter_Y.Frequency[SAMPLE_MOTOR]);
	  }

	    //!If some motor displacement is given from MBD and presently if the
	    //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[WASTE_MOTOR] > ZERO && \
	          pMotor4Pwm5->Status == IDLE)
	  {
		  SMDStart_Motor4(BloodCellCounter_Y.Displacement[WASTE_MOTOR], \
		          BloodCellCounter_Y.Direction[WASTE_MOTOR], \
		          BloodCellCounter_Y.Frequency[WASTE_MOTOR]);

	  }

	    //!If some motor displacement is given from MBD and presently if the
	    //! motor is in idle state, then start the motor
	  if(BloodCellCounter_Y.Displacement[DILUENT_MOTOR] > ZERO && \
	          pMotor5Pwm1->Status == IDLE)
	  {
		  SMDStart_Motor5(BloodCellCounter_Y.Displacement[DILUENT_MOTOR], \
		          BloodCellCounter_Y.Direction[DILUENT_MOTOR], \
		          BloodCellCounter_Y.Frequency[DILUENT_MOTOR]);
	  }

#endif
	 /* SysBinArrayToInt();
	  SIUpdateValveStatus(valveStatus1, valveStatus2);
      */
  }
  else
  {
#if INIT_DEBUG_PRINT_ENABLE
      //!Clear the interrupt counter
	  CpuTimer0.InterruptCount = ZERO;
#endif
	  //!If one time Flag is set, then the sequence is end and press the restart key
	  if(OneTimeFlag == ZERO)
	  {
#if INIT_DEBUG_PRINT_ENABLE
		  printf("Sequence Ends & Press Key to restart \n");
#endif
		  //!One time flag is enabled for next sequence
		  OneTimeFlag = 1;
	  }
  }
  //!Blink LED for 2 seconds
  IGBlinkLED(2000);
}
/******************************************************************************/
/*!
 * \fn void SysMotorControlIdleMode(void)
 * \brief Idle motor control
 * \brief Description:
 *      Function to set motors in idle mode
 * \return void
 */
/******************************************************************************/
void SysMotorControlIdleMode(void)
{
    //!Disable motor 1
	SMDSetMotorDisable(MOTOR1_SELECT);
	//!Disable motor 2
	SMDSetMotorDisable(MOTOR2_SELECT);
	//!Disable motor 3
	SMDSetMotorDisable(MOTOR3_SELECT);
	//!Disable motor 4
	SMDSetMotorDisable(MOTOR4_SELECT);
	//!Disable motor 5
	SMDSetMotorDisable(MOTOR5_SELECT);

	//IGConfigLED(LED1_GPIO, ZERO);
	IGConfigLED(LED2_GPIO, ZERO);
}
/******************************************************************************/
/*!
 * \fn void SysDataAquisition(void)
 * \brief ADC measurement module
 * \brief Description:
 *      First & Second counting modules for Blood data and Pressure Data
 * \return void
 */
/******************************************************************************/
void SysDataAquisition(void)
{
    static unsigned int CountOneFlag = 1;
	uint16_t status = ZERO;

    	//!Check if the count start command is received from MBD.
	//! If it is received, disable all the motors.
	//! During data acquisition, all the motors should be in idle mode.
	//! If the motors are on, it affects the data acquisition through ADC during
	//! counting sequence.
	if(BloodCellCounter_Y.CountStart == 1  && CountOneFlag == 1)
	{
	    SysMotorControlIdleMode();

		stCpu1IpcCommPtr->LocalReceiveFlag = ZERO;
		//!Set the count stop to Zero
		BloodCellCounter_U.CountStop = ZERO;
//		//! If cuurent sequence is Startup from service screen then set current mode to start up
//		if(g_usnCurrentMode == MBD_STARTUP_FROM_SERVICE_SCREEN_CMD)
//		{
//
//	        sprintf(DebugPrintBuf, "\r\n CountStart: srtup frm service %d ",g_usnCurrentMode);
//	        UartDebugPrint((int *)DebugPrintBuf, 50);
//		    g_usnCurrentMode = WHOLE_BLOOD_MODE;
//		}

		//!Send command to CPU to start acquiring data from the flow cell
		stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[0] = \
		        CPU1_IPC_FIRST_COUNT_DATA_ACQUISITION_CMD;


        m_bRBCStartSensor = GPIO_ReadPin(GPIO_VOL1);

        m_bRBCStopSensor = GPIO_ReadPin(GPIO_VOL2);

        m_bWBCStartSensor = GPIO_ReadPin(GPIO_VOL3);

        m_bWBCStopSensor = GPIO_ReadPin(GPIO_VOL4);

        if(LIQUID_SENSOR_OFF == m_bRBCStartSensor)
        {
            //!Clear the RBC start LED error
            utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor = FALSE;
        }
        else
        {
            //!Set the RBC start LED error
            utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor = TRUE;
        }
        if(LIQUID_SENSOR_OFF == m_bRBCStopSensor)
        {
            //!Clear the RBC stop LED error
            utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor = FALSE;
        }
        else
        {
            //!Set the RBC stop LED error
            utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor = TRUE;
        }
        if(LIQUID_SENSOR_OFF == m_bWBCStartSensor)
        {
            //!Clear the WBC start LED error
            utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor = FALSE;
        }
        else
        {
            //!Set the WBC start LED error
            utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor = TRUE;
        }
        if(LIQUID_SENSOR_OFF == m_bWBCStopSensor)
        {
            //!Clear the WBC stop LED error
            utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor = FALSE;
        }
        else
        {
            //!Set the WBC stop LED error
            utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor = TRUE;
        }
        if((TRUE == utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor) || \
                        (TRUE == utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor) || \
                        (TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor) ||\
                        (TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor))
        {
            //!Abort the current sequence and set the error
            //SysInitAbortProcess(TRUE);

            //!Frame the error command and abort the sequence
            //SysErrorCommand(g_usnCurrentMode);
        }

		//!Set the data as the current mode, this shall help for volumetric tube
		//! monitroing and data processing the data depending upon the modes selected.
		stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[1] = g_usnCurrentMode;
		stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[2] = g_usnWbcCalibratedTime;         //DS_added
		stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[3] = g_usnRbcCalibratedTime;         //DS_added

        sprintf(DebugPrintBuf, "\r\n g_usnWbcCalibratedTime : %d\n", g_usnWbcCalibratedTime);
        UartDebugPrint((int *)DebugPrintBuf, 50);
        sprintf(DebugPrintBuf, "\r\n g_usnRbcCalibratedTime : %d\n", g_usnRbcCalibratedTime);
        UartDebugPrint((int *)DebugPrintBuf, 50);

		//!Set the data mode. This data mode used to send data sitara depending
		//! upon the current mode running.
		//! Current mode variable cannot be used for data transferring.
		//! Current mode vairable is giving input to MBD and shall be reset
		//! after completion of the sequence. Data may be sent after completion
		//! of the seqeunce also. So cuurent mode variable cannot be used.
		g_usnDataMode = g_usnCurrentMode;
		//!Reset the interrupt counter
		CpuTimer0.InterruptCount = ZERO;
		//!Send Start acquisition command to CPU2
		status =  IPCCpu1toCpu2IpcCmdData(stCpu1IpcCommPtr,4);
		if(status == ZERO)
		{
			//STATUS_FAIL has occured retry code should be written
		}
		m_bPressureMonitor = TRUE;     //CRH_PRESSURE_Pressure check

		//!Count start from MBD is not reset untill the counting finishes.
		//! So Count one flag is reset to Zero, so that the condition doesn't repeat
		CountOneFlag = ZERO;

	}
	//!Set the count flag to 1 when the count start is reset from MBD.
	//! This shall intiate counting and data acquisition for the next seqeunce
	if(ZERO == BloodCellCounter_Y.CountStart)
    {
        CountOneFlag = 1;
    }

	//!Acquire the ADC health status
	//ADAdcAcquireHealthStatus();
}
/******************************************************************************/
/*!
 * \fn void SysCheckHomePosition(void)
 * \brief Home position check
 * \brief Description:
 *      checks the X & Y home position states and provided to mbd module
 * \return void
 */
/******************************************************************************/
void SysCheckHomePosition(void)
{
#if 1
#if HOME_SENSORS_ENABLE
    BloodCellCounter_U.HomeStatus[0] = IDLE;
	//BloodCellCounter_U.HP_YAxis = IDLE;
#else
	BloodCellCounter_U.HP_YAxis = COMPLETED;

#endif
	if(SENSOR2_HOME_STATE == OIHomeSensor2State())
	{
	    BloodCellCounter_U.HomeStatus[0] = COMPLETED;
	    //BloodCellCounter_U.HP_YAxis = COMPLETED;
	}
#endif
}
/******************************************************************************/
/*!
 * \fn void SysTestMotors(void)
 * \brief Test motors function
 * \brief Description:
 *      continuous run for all 5 motor UP & DOWN direction
 * \return void
 */
/******************************************************************************/
void SysTestMotors(void)
{

	static unsigned int st_Toggle=ZERO;
	static volatile unsigned int st_Temp = ZERO;

	  if(pMotor1Pwm8->Status == COMPLETED)
	  {
		  pMotor1Pwm8->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf("\n M1 step done \n");
#endif
	  }
	  if(pMotor2Pwm6->Status == COMPLETED)
	  {
		  pMotor2Pwm6->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf("\n M2 step done \n");
#endif
	  }
	  if(pMotor3Pwm2->Status == COMPLETED )
	  {
		  pMotor3Pwm2->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf("\n M3 step done \n");
#endif
	  }
	  if(pMotor4Pwm5->Status == COMPLETED)
	  {
		  pMotor4Pwm5->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf("\n M4 step done \n");
#endif
	  }
	  if(pMotor5Pwm1->Status == COMPLETED)
	  {
		  pMotor5Pwm1->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
			printf("\n M5 step done \n");
#endif
	  }


	  if(st_Toggle == ZERO)
	  {
		  if(pMotor5Pwm1->Status == IDLE && \
		          pMotor4Pwm5->Status == IDLE && \
		          pMotor3Pwm2->Status == IDLE && \
		          pMotor1Pwm8->Status == IDLE && \
		          pMotor2Pwm6->Status == IDLE)
		  if(pMotor2Pwm6->Status == IDLE )
		  {
		      SysInitValveTest(VALVE_7);
		      BloodCellCounter_DWork.Valve_7 = TRUE;
			  SMDStart_Motor1(3000, MOTOR_UP_DIRECTION, 1000);
			  SMDStart_Motor2(6000, MOTOR_UP_DIRECTION, 1000);
			  SMDStart_Motor3(2000, MOTOR_UP_DIRECTION, 1000);
		      if(TRUE == OIHomeSensor4State())
		      {
		          SMDStart_Motor4(3000, MOTOR_UP_DIRECTION, 1000);
		      }
		      else
            {
                SMDStart_Motor4(3000, MOTOR_DOWN_DIRECTION, 1000);
            }
			  SMDStart_Motor5(3000, MOTOR_UP_DIRECTION, 1000);

			  st_Toggle = 1;

			  IGConfigLED(LED1_GPIO, 1);
//			  IGConfigLED(LED2_GPIO, 1);

		  }
		  while(!(pMotor5Pwm1->Status == COMPLETED && \
		          pMotor4Pwm5->Status == COMPLETED && \
		          pMotor3Pwm2->Status == COMPLETED && \
		          pMotor1Pwm8->Status == COMPLETED && \
		          pMotor2Pwm6->Status == COMPLETED));
		  while(!( pMotor2Pwm6->Status == COMPLETED ));
		  DELAY_US(4000000);
		  IGConfigLED(LED1_GPIO, 1);
//		  IGConfigLED(LED2_GPIO, 0);
	  }
	  else
	  {
		  if(pMotor5Pwm1->Status == IDLE && \
		          pMotor4Pwm5->Status == IDLE && \
		          pMotor3Pwm2->Status == IDLE && \
		          pMotor1Pwm8->Status == IDLE && \
		          pMotor2Pwm6->Status == IDLE)
		  if(pMotor2Pwm6->Status == IDLE )
		  {
		      SysInitValveTest(VALVE_7);
			  SMDStart_Motor1(3000, MOTOR_DOWN_DIRECTION, 1000);
			  SMDStart_Motor2(6000, MOTOR_DOWN_DIRECTION, 1000);
			  SMDStart_Motor3(2000, MOTOR_DOWN_DIRECTION, 1000);
			  SMDStart_Motor4(3000, MOTOR_DOWN_DIRECTION, 1000);
			  SMDStart_Motor5(3000, MOTOR_DOWN_DIRECTION, 1000);
			  st_Toggle = ZERO;

			  IGConfigLED(LED1_GPIO, ZERO);
//			  IGConfigLED(LED2_GPIO, ZERO);

		  }

		 while(!(pMotor5Pwm1->Status == COMPLETED && \
		          pMotor4Pwm5->Status == COMPLETED && \
		          pMotor3Pwm2->Status == COMPLETED && \
		          pMotor1Pwm8->Status == COMPLETED && \
		          pMotor2Pwm6->Status == COMPLETED));
	  while(!( pMotor2Pwm6->Status == COMPLETED ));
		  DELAY_US(4000000);
		  IGConfigLED(LED1_GPIO, ZERO);
//		  IGConfigLED(LED2_GPIO, 1);
	  }

#if 0
	    static unsigned int Step = 0, bootStep = 2;
	    static volatile unsigned int st_Temp = 0;

	    static unsigned long SequenceNo = 0;
	    static int firstTime = 1;
	    static unsigned long NoHome = 0;

	      if(pMotor1Pwm8->Status == COMPLETED)
	      {
	          pMotor1Pwm8->Status = IDLE;
	#if MOTOR_PWM_DEBUG_PRINT_ENABLE
	            printf("\n M1 step done \n");
	#endif
	      }
	      if(pMotor2Pwm6->Status == COMPLETED)
	      {
	          pMotor2Pwm6->Status = IDLE;
	#if MOTOR_PWM_DEBUG_PRINT_ENABLE
	            printf("\n M2 step done \n");
	#endif
	      }
	      if(pMotor3Pwm2->Status == COMPLETED )
	      {
	          pMotor3Pwm2->Status = IDLE;
	#if MOTOR_PWM_DEBUG_PRINT_ENABLE
	            printf("\n M3 step done \n");
	#endif
	      }
	      if(pMotor4Pwm5->Status == COMPLETED)
	      {
	          pMotor4Pwm5->Status = IDLE;
	#if MOTOR_PWM_DEBUG_PRINT_ENABLE
	            printf("\n M4 step done \n");
	#endif
	      }
	      if(pMotor5Pwm1->Status == COMPLETED)
	      {
	          pMotor5Pwm1->Status = IDLE;
	#if MOTOR_PWM_DEBUG_PRINT_ENABLE
	            printf("\n M5 step done \n");
	#endif
	      }
	#if 0
	      unsigned int Home1 = HomeSensor1State();
	      unsigned int Home2 = HomeSensor2State();
	      unsigned int Home3 = HomeSensor3State();
	      unsigned int Home4 = HomeSensor4State();
	      unsigned int Home5 = HomeSensor5State();

	      printf("\n Home1:%d \n", Home1);
	      printf("\n Home2:%d \n", Home2);
	      printf("\n Home3:%d \n", Home3);
	      printf("\n Home4:%d \n", Home4);
	      printf("\n Home5:%d \n", Home5);
	      DELAY_US(1000000);
	#endif
	#if 1
	      //For the first time

	      if(TRUE == firstTime)
	      {
	          switch(bootStep)
	          {
	          case 1:
	              //Move Y axis to home position
	              if(pMotor5Pwm1->Status == IDLE && SENSOR2_HOME_STATE != OIHomeSensor5State())//If idle
	              {
	                  SMDStart_Motor5(10000/*3000*/, MOTOR_DOWN_DIRECTION, 1000);
	                  while(!(pMotor5Pwm1->Status == COMPLETED));
	              }
	              /*//while(!(pMotor2Pwm6->Status == COMPLETED));
	              if(SENSOR2_HOME_STATE != OIHomeSensor5State())
	              {
	                  NoHome++;
	              }*/
	              SysMotorControlIdleMode();
	              DELAY_US(2000);
	              bootStep = 2;
	              break;
	              //Move X Axis to left end and then to RBC bath
	          case 2:
	              if ((SENSOR1_HOME_STATE != OIHomeSensor4State()) && (pMotor4Pwm5->Status == IDLE))
	              {
	                  pMotor4Pwm5->HomeSensorControl = SENSOR_INTERRUPT_ENABLE;
	                  SMDStart_Motor4(8275/*3000*/, MOTOR_UP_DIRECTION, 1000);//Start_Motor1(8275/*3000*/, MOTOR_DOWN_DIRECTION, 1000);
	              }
	              if(SENSOR2_HOME_STATE == OIHomeSensor4State() && (pMotor4Pwm5->Status == IDLE))
	              {
	                  SMDStart_Motor4(2800/*3000*/, MOTOR_DOWN_DIRECTION, 1000);
	              }
	              while(!(pMotor4Pwm5->Status == COMPLETED));
	              SysMotorControlIdleMode();
	              DELAY_US(2000);
	              bootStep = 3;
	              break;

	          case 3:
	              if(pMotor4Pwm5->Status == IDLE && pMotor4Pwm5->HomeSensorControl == SENSOR_INTERRUPT_ENABLE)//If idle
	              {
	                  SMDStart_Motor4(1250/*3000*/, MOTOR_DOWN_DIRECTION, 1000);//Start_Motor1(3775/*3000*/, MOTOR_UP_DIRECTION, 1000);
	                  pMotor4Pwm5->HomeSensorControl = SENSOR_INTERRUPT_DISABLE;
	              }
	              if ((SENSOR2_HOME_STATE != OIHomeSensor4State()) && (pMotor4Pwm5->Status == IDLE))
	              {
	                  pMotor4Pwm5->HomeSensorControl = SENSOR_INTERRUPT_ENABLE;
	                  SMDStart_Motor4(8275/*3000*/, MOTOR_UP_DIRECTION, 1000);//Start_Motor1(8275/*3000*/, MOTOR_DOWN_DIRECTION, 1000);
	              }
	              while(!(pMotor4Pwm5->Status == COMPLETED));
	              SysMotorControlIdleMode();
	              DELAY_US(2000);
	              bootStep = 4;
	              //Step = 1;
	              //firstTime = FALSE;
	              //bootStep = 0;
	              break;
	          case 4:
	              if(pMotor4Pwm5->Status == IDLE && pMotor4Pwm5->HomeSensorControl == SENSOR_INTERRUPT_ENABLE)//If idle
	               {
	                  SMDStart_Motor4(1250/*3000*/, MOTOR_DOWN_DIRECTION, 1000);//Start_Motor1(3775/*3000*/, MOTOR_UP_DIRECTION, 1000);
	                  pMotor4Pwm5->HomeSensorControl = SENSOR_INTERRUPT_DISABLE;
	                     while(!(pMotor4Pwm5->Status == COMPLETED));
	               }
	              //while(!(pMotor1Pwm8->Status == COMPLETED));
	              SysMotorControlIdleMode();
	              DELAY_US(2000);
	              Step = 4;
	              firstTime = FALSE;
	              bootStep = 0;
	              break;

	           default:
	                break;
	          }
	      }

	      //Sequence - Starts



	      switch(Step)
	      {
	      case 1:
	          SequenceNo++;

	              sprintf(DebugPrintBuf, "\r\n SequenceNo: %ld ",SequenceNo);
	              UartDebugPrint((int *)DebugPrintBuf, 50);
	              sprintf(DebugPrintBuf, "\r\n Non home Iterations of Y axis: %ld ",NoHome);
	              UartDebugPrint((int *)DebugPrintBuf, 50);
	              printf("\r\n SequenceNo: %ld ",SequenceNo);
	              printf("\r\n No.of homing failures of Y axis: %ld ",NoHome);
	          //Move X axis left to Aspiration position
	          if(pMotor4Pwm5->Status == IDLE)//If idle
	          {
	              SMDStart_Motor4(3775/*3000*/, MOTOR_UP_DIRECTION, 1000);//Start_Motor1(4500/*3000*/, MOTOR_DOWN_DIRECTION, 1000);
	          }
	          while(!(pMotor4Pwm5->Status == COMPLETED));
	          SysMotorControlIdleMode();
	          DELAY_US(2000);
	          Step = 4;
	          break;

	      case 2:
	          //Y-Axis down
	          if(pMotor5Pwm1->Status == IDLE)//If idle
	          {
	              SMDStart_Motor5(9000/*7450*//*3000*/, MOTOR_UP_DIRECTION, 1000);//Start_Motor2(7450/*7450*//*3000*/, MOTOR_UP_DIRECTION, 1000);
	          }
	          while(!(pMotor5Pwm1->Status == COMPLETED));
	          SysMotorControlIdleMode();
	          DELAY_US(2000);
	          Step = 3;
	          break;

	      case 3:
	          //Y-Axis Up
	          if(pMotor5Pwm1->Status == IDLE)//If idle
	          {
	              SMDStart_Motor5(10000/*7450*//*3000*/, MOTOR_DOWN_DIRECTION, 1000);
	          }
	          while(!(pMotor5Pwm1->Status == COMPLETED));
	          /*if(SENSOR2_HOME_STATE != OIHomeSensor5State())
	          {
	               NoHome++;
	          }*/
	          SysMotorControlIdleMode();
	          DELAY_US(2000);
	          Step = 4;
	          break;

	      case 4:
	          //X Axis to WBC Bath
	          if(pMotor4Pwm5->Status == IDLE)//If idle
	          {
	              SMDStart_Motor4(8275, MOTOR_DOWN_DIRECTION, 1000);     //CHANGE
	              //Start_Motor1(3775, MOTOR_DOWN_DIRECTION, 1000);     //CHANGE
	          }
	          while(!(pMotor4Pwm5->Status == COMPLETED));
	          SysMotorControlIdleMode();
	          DELAY_US(1000);
	          Step = 7;
	          break;

	      case 5:
	          //Y Axis down
	          if(pMotor5Pwm1->Status == IDLE)//If idle
	          {
	              SMDStart_Motor5(7450/*9500*//*9000*//*3000*/, MOTOR_UP_DIRECTION, 1000);
	          }
	          while(!(pMotor5Pwm1->Status == COMPLETED));
	          SysMotorControlIdleMode();
	          DELAY_US(1000);
	          Step = 6;
	          break;

	      case 6:
	          //Y axis Up
	          if(pMotor5Pwm1->Status == IDLE)//If idle
	          {
	              SMDStart_Motor5(10000/*9500*//*9000*//*3000*/, MOTOR_DOWN_DIRECTION, 1000);
	          }
	          while(!(pMotor5Pwm1->Status == COMPLETED));
	          /*if(SENSOR2_HOME_STATE != OIHomeSensor5State())
	          {
	               NoHome++;
	          }*/
	          SysMotorControlIdleMode();
	          DELAY_US(1000);
	          Step = 7;
	          break;

	      case 7:
	          //X Axis to RBC bath position
	          if(pMotor4Pwm5->Status == IDLE)//If idle
	          {
	              //SMDStart_Motor4(4500, MOTOR_UP_DIRECTION, 1000);
	              SMDStart_Motor4(8275, MOTOR_UP_DIRECTION, 1000);
	          }
	          while(!(pMotor4Pwm5->Status == COMPLETED));
	          SysMotorControlIdleMode();
	          DELAY_US(1000);
	          Step = 4;
	          break;

	      case 8:
	          //Y Axis Down
	          if(pMotor5Pwm1->Status == IDLE)//If idle
	          {
	              SMDStart_Motor5(7450/*7950*//*7450*//*3000*/, MOTOR_UP_DIRECTION, 1000);
	          }
	          while(!(pMotor5Pwm1->Status == COMPLETED));
	          SysMotorControlIdleMode();
	          DELAY_US(1000);
	          Step = 9;
	          break;

	      case 9:
	          //Y Axis Up
	          if(pMotor5Pwm1->Status == IDLE)//If idle
	          {
	              SMDStart_Motor5(10000/*7950*//*7450*//*3000*/, MOTOR_DOWN_DIRECTION, 1000);
	          }
	          while(!(pMotor5Pwm1->Status == COMPLETED));
	          /*if(SENSOR2_HOME_STATE != OIHomeSensor5State())
	          {
	               NoHome++;
	          }*/
	          SysMotorControlIdleMode();
	          DELAY_US(1000);
	          Step = 1;
	          break;

	      default:
	          break;
	      }

#endif

#endif
}

/******************************************************************************/
/*!
 * \fn void SysTestValves(void)
 * \brief Test valve output
 * \brief Description:
 *      valve output toggle in loop
 * \return void
 */
/******************************************************************************/
void SysTestValves(void)
{
	static Uint16 st_ValveStatus = 0xFFFF;

	Uint16 ValveStatus1 = (0x00FF & st_ValveStatus);
	Uint16 ValveStatus2 = (Uint16)(st_ValveStatus >> 8);

	SIUpdateValveStatus((unsigned int)ValveStatus1, (unsigned int)ValveStatus2);
	printf(" ValveStatus2: %x \t ValveStatus1: %x \n", ValveStatus2, ValveStatus1);
//	st_ValveStatus = st_ValveStatus << 1;
	if(st_ValveStatus == 0xFFFF)
	{
		st_ValveStatus = 0x0000;
	}
	else
	{
		st_ValveStatus = 0xFFFF;
	}
}
/******************************************************************************/
/*!
 * \fn void SysTestGpioPin(void)
 * \brief Test gpio pin toggle
 * \brief Description:
 *      gpio pin toggle in loop
 * \return void
 */
/******************************************************************************/
void SysTestGpioPin(void)
{
	static Uint16 stToggle = ZERO, stHighVoltageSelection=1;
	if(stToggle == ZERO)
	{
		stHighVoltageSelection = (int)(!stHighVoltageSelection);  //QA_C

		IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_GPIO_OUTPUT, \
		        stHighVoltageSelection);
		IGConfigHighVoltageModule(ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT, \
		        ZAP_VOLTAGE_OFF);

		IGWriteCycleCounterGpio(CYCLE_COUNTER_GPIO_OUTPUT1, ZERO);
		IGWriteCycleCounterGpio(CYCLE_COUNTER_GPIO_OUTPUT2, ZERO);

		PWMHgbPwmControl(STOP, HGB_PWM_FREQUENCY);

		stToggle = 1;
	}
	else
	{
		IGConfigHighVoltageModule(ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT, ZAP_VOLTAGE_ON);

		IGWriteCycleCounterGpio(CYCLE_COUNTER_GPIO_OUTPUT1, 1);
		IGWriteCycleCounterGpio(CYCLE_COUNTER_GPIO_OUTPUT2, 1);

		PWMHgbPwmControl(START, HGB_PWM_FREQUENCY);

		stToggle = ZERO;
	}
}
/******************************************************************************/
/*!
 * \fn void SysPeripheralModuleTest(void)
 * \brief Peripheral test
 * \brief Description:
 *      Function performs the individual/standalone test for requested module
 *      or peripheral
 *      Request is from UART command based
 * \return void
 */
/******************************************************************************/
void SysPeripheralModuleTest(void)
{
	//Uint32 RxData=ZERO;
	Uint16 Emif1Status=ZERO;

	SysTestUartModule();

	//! IPC sync b/w cpu1 & cpu2
	SysInitCpu1Cpu2IpcComm();
	//! variable initialization
	SysSetParametersDefaultValue();
	//! emif1 ownership transfer
	ISConfigEmif1CPU(0x00);
	while(!(0x02 == Emif1Status))
	{
		DELAY_US(500000);
		Emif1Status = ISGetEmif1Status();
	}

	SPIClearCommTxStatus();
	printf(" Uart test: \n");

	//RxData = OIWriteDataSpiHomeSensorReg(WRITE_CMD_IODIR, \
	SPI_HOME_POS_CONFIG_IODIR);

	sprintf(DebugPrintBuf, "\r\n WRITE_CMD_IODIR 0x%04x ",WRITE_CMD_IODIR);
	UartDebugPrint((int *)DebugPrintBuf, 50);
	sprintf(DebugPrintBuf, " SPI_HOME_POS_CONFIG_IODIR 0x%04x ",\
	        SPI_HOME_POS_CONFIG_IODIR);
	UartDebugPrint((int *)DebugPrintBuf, 50);

	pMotor1Pwm8->HomePosSenseStatus = ZERO;

	SPIActivateCommPort();//SAM_SPI

	sprintf(statDebugPrintBuf, " \n WAITING FOR ONE MIN \n ");
					UartDebugPrint((int *)statDebugPrintBuf, 50);

	printf(statDebugPrintBuf, " \n  DONE WAITING FOR ONE MIN \n ");
						UartDebugPrint((int *)statDebugPrintBuf, 50);

    //!Switch on the PWM of the HGB
    PWMHgbPwmControl(START, HGB_PWM_FREQUENCY); //Switch on the HgB LED
    /*while(1)
    {
        // HGB check
        SysDataAquisition();
        SysInitGetSystemStatus();
    }*/

	while(1)
	{
#if ONE
		//printf(" Motor test: \n");
	    SysTestMotors();
		printf(" Valve test: \n");
		SysTestValves();

		printf(" Gpio Pin test: \n");
		//SysTestGpioPin();
		printf(" Home Pos sensor test: \n");
		//OIConfigSpiHomeSensor();
#endif
		DELAY_US(1000000);	//10ms

		//Read waste syringe Home postion sensors
		//int test = OIHomeSensor3State();

		//printf(" test: %d\n", test);
		//SPICommTest();

		//SPICommRead();


		/*static int test=1,count=0;
		test =1;


        switch(test)
        {
        case 1:
                while(count<50)
                {
                    test=OIHomeSensor1State();
                    printf(" Waste test: %d\n", test);
                    count++;
                }
                count=0;
                break;
        case 2:
            while(count<50)
            {
                test=OIHomeSensor2State();
                printf("Diluent  test: %d\n", test);
                count++;
            }
            count=0;
                break;
        case 3:
            while(count<50)
            {
                test=OIHomeSensor3State();
                printf(" Sample test: %d\n", test);
                count++;
            }
            count=0;
                break;
        case 4:
            while(count<50)
                    {
                test=OIHomeSensor4State();
                printf(" X-Axis test: %d\n", test);
                count++;
                    }
            count=0;
                break;
        case 5:
            while(count<50)
                    {
                test=OIHomeSensor5State();
                printf(" Y-Axis test: %d\n", test);
                count++;
                    }
            count=0;
                break;
        default:
            break;
        }
        */

		IGBlinkLED(1);
	}
//	IGBlinkLED(200);
//	SysCpu1IpcCommModule();
}
/******************************************************************************/
/*!
 * \fn void SysTestUartModule(void)
 * \brief uart test
 * \brief Description:
 *      Function to performs the uart test
 * \return void
 */
/******************************************************************************/
void SysTestUartModule(void)
{
	int16 Counter=ZERO;
	char RxBuf[25];

	Counter = getUartRxData((int*)RxBuf);
	if(Counter > ZERO)
	{
		UartDebugPrint((int *)RxBuf, 40);
	}
	sprintf(DebugPrintBuf, "\r\n alpha Hw: Uart test \n");
	UartDebugPrint((int *)DebugPrintBuf, 40);

}
/******************************************************************************/
/*!
 * \fn void SysTestMotors(void)
 * \brief Test motors function
 * \brief Description:
 *      continuous run for all 5 motor UP & DOWN direction
 * \return void
 */
/******************************************************************************/
void SysTestWasteMotor(void)
{

    static unsigned int st_Toggle=ONE;

      if(pMotor1Pwm8->Status == COMPLETED)
      {
          pMotor1Pwm8->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
            printf("\n M1 step done \n");
#endif
      }
      if(pMotor2Pwm6->Status == COMPLETED)
      {
          pMotor2Pwm6->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
            printf("\n M2 step done \n");
#endif
      }
      if(pMotor3Pwm2->Status == COMPLETED )
      {
          pMotor3Pwm2->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
            printf("\n M3 step done \n");
#endif
      }
      if(pMotor4Pwm5->Status == COMPLETED)
      {
          pMotor4Pwm5->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
            printf("\n M4 step done \n");
#endif
      }
      if(pMotor5Pwm1->Status == COMPLETED)
      {
          pMotor5Pwm1->Status = IDLE;
#if MOTOR_PWM_DEBUG_PRINT_ENABLE
            printf("\n M5 step done \n");
#endif
      }


      if(pMotor1Pwm8->Status == IDLE )
      {
          SMDStart_Motor1(5500, MOTOR_UP_DIRECTION, 1000);
          while(!( pMotor2Pwm6->Status == COMPLETED ));
          DELAY_US(1000000);
          SMDStart_Motor1(5500, MOTOR_DOWN_DIRECTION, 1000);
          while(!( pMotor2Pwm6->Status == COMPLETED ));
          DELAY_US(4000000);
      }
      sprintf(DebugPrintBuf, "\r\n Pressure when the motor is down: %ld", m_usnPressureAdc);
      UartDebugPrint((int *)DebugPrintBuf, 50);
      while(1)
      {
          SysInitGetSystemStatus();
          switch(st_Toggle)
          {
              case 1:
                  if(pMotor1Pwm8->Status == IDLE )
                  {
                      SMDStart_Motor1(2700, MOTOR_UP_DIRECTION, 1000);
                      st_Toggle = 2;
                      while(!( pMotor2Pwm6->Status == COMPLETED ));
                      DELAY_US(4000000);
                  }
                  sprintf(DebugPrintBuf, "\r\n Pressure when the motor is the middle while moving up: %ld", m_usnPressureAdc);
                  UartDebugPrint((int *)DebugPrintBuf, 50);
                  break;
              case 2:
                  if(pMotor1Pwm8->Status == IDLE )
                  {
                      SMDStart_Motor1(2700, MOTOR_UP_DIRECTION, 1000);
                      st_Toggle = 3;
                      while(!( pMotor2Pwm6->Status == COMPLETED ));
                      DELAY_US(4000000);
                  }
                  sprintf(DebugPrintBuf, "\r\n Pressure when the motor is up: %ld", m_usnPressureAdc);
                  UartDebugPrint((int *)DebugPrintBuf, 50);
                  break;
              case 3:
                  if(pMotor1Pwm8->Status == IDLE )
                  {
                      SMDStart_Motor1(2700, MOTOR_DOWN_DIRECTION, 1000);
                      st_Toggle = 4;
                      while(!( pMotor2Pwm6->Status == COMPLETED ));
                      DELAY_US(4000000);
                  }
                  sprintf(DebugPrintBuf, "\r\n Pressure when the motor is the middle while moving down: %ld", m_usnPressureAdc);
                  UartDebugPrint((int *)DebugPrintBuf, 50);
                  break;
              case 4:
                  if(pMotor1Pwm8->Status == IDLE )
                  {
                      SMDStart_Motor1(2700, MOTOR_DOWN_DIRECTION, 1000);
                      st_Toggle = 1;
                      while(!( pMotor2Pwm6->Status == COMPLETED ));
                      DELAY_US(4000000);
                  }
                  sprintf(DebugPrintBuf, "\r\n Pressure when the motor is down: %ld", m_usnPressureAdc);
                  UartDebugPrint((int *)DebugPrintBuf, 50);
                  break;
          }
      }
}
//SAM_SPI - START
/******************************************************************************/
/*!
 * \fn uint32_t SysGetADCDataLength(uint16_t usnSubCmd)
 * \brief ADC Data Length for Pulse Height
 * \brief Description:
 *     	Function is called to get the length of the pulse Height data
 *     	obtained from ADC
 * \param usnSubCmd SubCommand to be sent to Sitara
 * \return uint16_t
 */
/******************************************************************************/
uint32_t SysGetADCDataLength(uint16_t usnSubCmd)
{
    uint32_t uslDataLength  = ZERO;
	switch (usnSubCmd)
	{
		case DELFINO_WBC_PULSE_HEIGHT_DATA:

		    uslDataLength = *m_pSdramADCWBCCnt;
			break;

		case DELFINO_RBC_PULSE_HEIGHT_DATA:

		    uslDataLength = *m_pSdramADCRBCCnt;
			break;

		case DELFINO_PLT_PULSE_HEIGHT_DATA:
		    uslDataLength = *m_pSdramADCPLTCnt;
			break;
	}
	return uslDataLength;
}
/******************************************************************************/
/*!
 * \fn uint16_t SysGetAdcCountData(uint16_t* pData)
 * \brief ADC Count Data
 * \brief Description:
 *     	Function is called to get the pulse count data obtained from for WBC,
 *     	RBC and Platelets
 * \param pData pointer variable to store the data
 * \return the datalength
 */
/******************************************************************************/
uint16_t SysGetAdcCountData(uint16_t* pData)
{
    uint16_t usnWBC_LSW = ZERO;
    uint16_t usnWBC_MSW = ZERO;
    uint16_t usnRBC_LSW = ZERO;
    uint16_t usnRBC_MSW = ZERO;
    uint16_t usnPLT_LSW = ZERO;
    uint16_t usnPLT_MSW = ZERO;

    //!This mode is used to get the WBC, RBC and PLT count data and frame the
    //! packet to send to Sitara
    //! Based on the data mode, set the datainto the packet
	switch(g_usnDataMode)
	{
		case WHOLE_BLOOD_MODE:
		case START_PRE_DILUENT_COUNTING_CMD:
		case PRE_DILUENT_MODE:
		case AUTO_CALIBRATION_MODE:
		case COMMERCIAL_CALIBRATION_MODE:
		case START_UP_SEQUENCE_MODE:
		case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
		case MBD_STARTUP_FAILED_CMD:
		case AUTO_CALIBRATION_WB:
		case QUALITY_CONTROL_WB:
		{
		    //!If the sequence is invalid, then set all the data to Zero.
		    if(TRUE == m_bInvalidData)
		    {
                pData[0] = ZERO;
                pData[1] = ZERO;
                pData[2] = ZERO;
                pData[3] = ZERO;
                pData[4] = ZERO;
                pData[5] = ZERO;
                m_bInvalidData = FALSE;
		    }
		    else
		    {

		        usnWBC_LSW = (uint16_t)(*m_pSdramADCWBCCnt);
		        usnWBC_MSW = (*m_pSdramADCWBCCnt)>>16;

                usnRBC_LSW = (uint16_t)(*m_pSdramADCRBCCnt);
                usnRBC_MSW = (*m_pSdramADCRBCCnt)>>16;

                usnPLT_LSW = (uint16_t)(*m_pSdramADCPLTCnt);
                usnPLT_MSW = (*m_pSdramADCPLTCnt)>>16;

                pData[0] = usnWBC_LSW;
                pData[1] = usnWBC_MSW;
                pData[2] = usnRBC_LSW;
                pData[3] = usnRBC_MSW;
                pData[4] = usnPLT_LSW;
                pData[5] = usnPLT_MSW;
		    }

		    //!Set the HGB blank value in the 4th position of the data
			pData[6] = 	m_usnHgbBlank;

			//!Set the HGB sample value in the 5th position of the data
			pData[7] =  m_usnHgbSample;

			//!WBC volumetric counting time is palced in the 6th position. Presently it is hardcoded
			pData[8] =  m_usnWBCCountTime; //6580;//7000;//m_usnWBCCountTime;
			//!RBC volumetric counting time is palced in the 7th position. Presently it is hardcoded
			pData[9] =  m_usnRBCCountTime;//9160;//8240;//m_usnRBCCountTime;
			//!WBC Algorithm Calculation time is for complete data
			pData[10] =  m_usnWBCCountTime;
			//!RBC Algorithm Calculation time is for 5 seconds so it is fixed
			pData[11] =  m_usnRBCCountTime;//RBC_PROCESSING_TIME;
			//!PLT Algorithm Calculation time is for 6 seconds so it is fixed
			pData[12] =  m_usnRBCCountTime;
			//TBD_Comment
			/*if(m_usnRBCCountTime < PLT_PROCESSING_TIME)
			{
			    pData[12] =  m_usnRBCCountTime;
			}
			else
			{
			    pData[12] =  PLT_PROCESSING_TIME;
			}*/
			pData[13] = bStartADCPressure;  //Hari pressure check-before aqc start
			pData[14] = bEndADCPressure;    //Hari pressure check-aqc stop
			pData[15] = bMStopADCPressure;    //Hari pressure check-Motor stop
		}
		break;

		case BODY_FLUID_MODE:
		case COMMERCIAL_CALIBRATION_WBC:
		case PARTIAL_BLOOD_COUNT_WBC:
		case QUALITY_CONTROL_BODY_FLUID:
		{
            usnWBC_LSW = (uint16_t)(*m_pSdramADCWBCCnt);
            usnWBC_MSW = (*m_pSdramADCWBCCnt)>>16;

		    //!Update the WBC count data
            pData[0] = usnWBC_LSW;
            pData[1] = usnWBC_MSW;
			//!No RBC count for the above modes so set to Zero
			pData[2] =  ZERO;
			pData[3] =  ZERO;
			//!No PLT count for the above modes so set to Zero
			pData[4] =  ZERO;
			pData[5] =  ZERO;
			//!Set the HGB blank value in the 4th position of the data
			pData[6] =  m_usnHgbBlank;
			//!Set the HGB sample value in the 5th position of the data
			pData[7] =  m_usnHgbSample;
			//!WBC volumetric counting time is palced in the 6th position. Presently it is hardcoded
			pData[8] = m_usnWBCCountTime;
			//!RBC volumetric counting time is palced in the 7th position. Presently it is hardcoded
			pData[9] = m_usnRBCCountTime;
            //!WBC Algorithm Calculation time is for complete data
            pData[10] =  m_usnWBCCountTime;
            //!RBC Algorithm Calculation time is for 5 seconds so it is fixed
            pData[11] =  RBC_PROCESSING_TIME;
            //!PLT Algorithm Calculation time is for 6 seconds so it is fixed
            if(m_usnRBCCountTime < PLT_PROCESSING_TIME)
            {
                pData[12] =  m_usnRBCCountTime;
            }
            else
            {
                pData[12] =  PLT_PROCESSING_TIME;
            }
		}
		break;

		case COMMERCIAL_CALIBRATION_RBC:
		case COMMERCIAL_CALIBRATION_PLT:
		case PARTIAL_BLOOD_COUNT_RBC_PLT:

            usnRBC_LSW = (uint16_t)(*m_pSdramADCRBCCnt);
            usnRBC_MSW = (*m_pSdramADCRBCCnt)>>16;

            usnPLT_LSW = (uint16_t)(*m_pSdramADCPLTCnt);
            usnPLT_MSW = (*m_pSdramADCPLTCnt)>>16;

		    //!No WBC count for the above modes so set to Zero
		    pData[0] = ZERO;
		    pData[1] = ZERO;
		    //!Update the RBC count data
            pData[2] = usnRBC_LSW;
            pData[3] = usnRBC_MSW;
            //!Update the PLT count data
            pData[4] = usnPLT_LSW;
            pData[5] = usnPLT_MSW;
            //!Set the HGB blank value in the 4th position of the data
            pData[6] =  m_usnHgbBlank;
            //!Set the HGB sample value in the 5th position of the data
            pData[7] =  m_usnHgbSample;
            //!WBC volumetric counting time is palced in the 6th position. Presently it is hardcoded
            pData[8] = m_usnWBCCountTime;
            //!RBC volumetric counting time is palced in the 7th position. Presently it is hardcoded
            pData[9] = m_usnRBCCountTime;
            //!WBC Algorithm Calculation time is for complete data
            pData[10] =  m_usnWBCCountTime;
            //!RBC Algorithm Calculation time is for 5 seconds so it is fixed
            pData[11] =  RBC_PROCESSING_TIME;
            //!PLT Algorithm Calculation time is for 6 seconds so it is fixed
            if(m_usnRBCCountTime < PLT_PROCESSING_TIME)
            {
                pData[12] =  m_usnRBCCountTime;
            }
            else
            {
                pData[12] =  PLT_PROCESSING_TIME;
            }
		    break;

		default:
		{
			sprintf(statDebugPrintBuf, " CRH_TEST \n");
			UartDebugPrint((int *)statDebugPrintBuf, 50);

            usnWBC_LSW = (uint16_t)(*m_pSdramADCWBCCnt);
            usnWBC_MSW = (*m_pSdramADCWBCCnt)>>16;

            usnRBC_LSW = (uint16_t)(*m_pSdramADCRBCCnt);
            usnRBC_MSW = (*m_pSdramADCRBCCnt)>>16;

            usnPLT_LSW = (uint16_t)(*m_pSdramADCPLTCnt);
            usnPLT_MSW = (*m_pSdramADCPLTCnt)>>16;

			//!Update the WBC count data
			pData[0] = usnWBC_LSW;
			pData[1] = usnWBC_MSW;
			//!Update the RBC count data
            pData[2] = usnRBC_LSW;
            pData[3] = usnRBC_MSW;
			//!Update the PLT count data
            pData[4] = usnPLT_LSW;
            pData[5] = usnPLT_MSW;
			//!Set the HGB blank value in the 4th position of the data
			pData[6] =  m_usnHgbBlank;
			//!Set the HGB sample value in the 5th position of the data
			pData[7] =  m_usnHgbSample;
			//!WBC volumetric counting time is palced in the 6th position. Presently it is hardcoded
			pData[8] =  m_usnWBCCountTime;
			//!RBC volumetric counting time is palced in the 7th position. Presently it is hardcoded
			pData[9] =  m_usnRBCCountTime;
            //!WBC Algorithm Calculation time is for complete data
            pData[10] =  m_usnWBCCountTime;
            //!RBC Algorithm Calculation time is for 5 seconds so it is fixed
            pData[11] =  m_usnRBCCountTime;//RBC_PROCESSING_TIME;
            //!PLT Algorithm Calculation time is for 6 seconds so it is fixed
            if(m_usnRBCCountTime < PLT_PROCESSING_TIME)
            {
                pData[12] =  m_usnRBCCountTime;
            }
            else
            {
                pData[12] =  PLT_PROCESSING_TIME;
            }
		}
		break;
	}
	//!Return the count data length
	return COUNT_DATA_LENGTH;
}
/******************************************************************************/
/*!
 * \fn void SysInitGetHGBData(uint16_t* pData)
 * \brief HGB Data
 * \brief Description:
 *     	Function is called to get the HGB data obtained from ADC - Presently not used
 * \param pusnData pointer variable to store the data
 * \param usnDataLength datalength
 * \return void
 */
/******************************************************************************/
void SysInitGetHGBData(uint16_t* pusnData, uint16_t usnDataLength)
{
	uint16_t usnCount = ZERO;

	for(usnCount = ZERO; usnCount< usnDataLength; usnDataLength++)
	{
		//!presently not used
	}
}
/******************************************************************************/
/*!
 * \fn uint32_t SysGetPulseHeightDataFrmSDRAM(E_CELL_TYPE eTypeofCell,
 * 			uint16_t* parrusnHeightData,uint16_t rusnPrevHeightIndex,
 * 								uint16_t rusnCount,const uint16_t kusnMaxSize)
 * \brief Pulse Height Data
 * \brief Description:
 *     	Function is called to get the pulse height data obtained from ADC for
 *     	WBC, RBC and Platelets
 * \param eTypeofCell Cell type enum
 * \param parrusnHeightData pointer array to store the Pulse height
 *          data obtained from ADC
 * \param rusnPrevHeightIndex variable indicating the index till which data was
 * 			 written previously
 * \param kusnMaxSize variable indicating the number of bytes to be written to
 * 			during this function call
 * \return uint16_t
 */
/******************************************************************************/
uint32_t SysGetPulseHeightDataFrmSDRAM(E_CELL_TYPE eTypeofCell,\
        uint16_t* parrusnHeightData,uint32_t rusnPrevHeightIndex,\
        const uint16_t kusnMaxSize)
{
	uint32_t usnNewDataIndex,usnIndex=ZERO;
	uint32_t usnwbchtdata = usnSdramAdcDataAddr[4];
	uint32_t usnrbchtdata = usnSdramAdcDataAddr[5];
	uint32_t usnplthtdata = usnSdramAdcDataAddr[6];


	//!No. of bytes of data to be wriiten to the array
	usnNewDataIndex = rusnPrevHeightIndex + kusnMaxSize;

	//!Check the type of cell for the pulse height data to be sent
	switch (eTypeofCell)
	{
	    //!If the cell type is RBC, update the index and copy the pulse height data
	    //! from source to destination based on the index
		case eCellRBC:
			usnIndex = usnrbchtdata+rusnPrevHeightIndex;
			memcpy_fast_far(&parrusnHeightData[0],\
			        (volatile const void *)usnIndex, kusnMaxSize);
		break;

        //!If the cell type is PLT, update the index and copy the pulse height data
        //! from source to destination based on the index
		case eCellPLT:
			usnIndex = usnplthtdata+rusnPrevHeightIndex;
			memcpy_fast_far(&parrusnHeightData[0],\
			        (volatile const void *)usnIndex, kusnMaxSize);
		break;

        //!If the cell type is WBC, update the index and copy the pulse height data
        //! from source to destination based on the index
		case eCellWBC:
			usnIndex = usnwbchtdata+rusnPrevHeightIndex;
			memcpy_fast_far(&parrusnHeightData[0],\
			        (volatile const void *)usnIndex, kusnMaxSize);
		break;

		default:
			break;
	}
	//!Return the updated index to store to copy data for the next packet transfer
	return usnNewDataIndex;
}
/******************************************************************************/
/*!
 * \fn void SysInitSDRamAdd()
 * \brief SD RAM Address initialisation
 * \brief Description:
 *     	Function is called to initialise the SDRam address to member pointers
 * \return void
 */
/******************************************************************************/
void SysInitSDRamAdd()
{
    //!Get the WBC count address
	m_pSdramADCWBCCnt = &usnSdramAdcDataAddr[7];
	//!Get the RBC count address
	m_pSdramADCRBCCnt = &usnSdramAdcDataAddr[8];
	//!Get the PLT count address
	m_pSdramADCPLTCnt = &usnSdramAdcDataAddr[9];
}
/******************************************************************************/
/*!
 * \fn uint32_t SysGetADCRawDataFrmSDRAM()
 * \brief RAW ADC Data
 * \brief Description:
 *     	Function is called to get the raw ADC data obtained for WBC,
 *     	RBC and Platelets
 * \param eTypeofCell Cell type enum
 * \param parrusnHeightData pointer array to store the Pulse height
 *          data obtained from ADC
 * \param rusnPrevHeightIndex variable indicating the index till which data was
 * 			 written previously
 * \param kusnMaxSize variable indicating the number of bytes to be written to
 * 			during this function call
 * \return uint32_t
 */
/******************************************************************************/
uint32_t SysGetADCRawDataFrmSDRAM(E_CELL_TYPE eTypeofCell,\
        uint16_t* parrusnADCData,uint32_t rusnPrevADCIndex,\
        const uint16_t kusnMaxSize)
{
	uint32_t usnNewDataIndex, usnIndex;
	usnIndex = ZERO;

	//!Assign the WBC RAW data address
	uint32_t usnAdcWbcRawdata = usnSdramAdcDataAddr[0];
	//!Assign the RBC RAW data address
	uint32_t usnAdcRbcRawdata = usnSdramAdcDataAddr[1];
	//!Assign the PLT RAW data address
	uint32_t usnAdcPltRawdata = usnSdramAdcDataAddr[2];

	//!No. of bytes of data to be written to the array
	usnNewDataIndex = rusnPrevADCIndex + kusnMaxSize;

	//!check for the cell type to copy the ADC RAW data
	switch (eTypeofCell)
	{
        //!If the cell type is RBC, update the index and copy the RAW data
        //! from source to destination based on the index
		case eCellRBC:
			usnIndex = usnAdcRbcRawdata+rusnPrevADCIndex;
			memcpy_fast_far(&parrusnADCData[0],\
			        (volatile const void *)usnIndex, kusnMaxSize);
		break;

        //!If the cell type is PLT, update the index and copy the RAW data
        //! from source to destination based on the index
		case eCellPLT:
			usnIndex = usnAdcPltRawdata+rusnPrevADCIndex;
			memcpy_fast_far(&parrusnADCData[0],\
			        (volatile const void *)usnIndex, kusnMaxSize);
		break;

        //!If the cell type is WBC, update the index and copy the RAW data
        //! from source to destination based on the index
		case eCellWBC:
			usnIndex = usnAdcWbcRawdata+rusnPrevADCIndex;
			memcpy_fast_far(&parrusnADCData[0],\
			        (volatile const void *)usnIndex, kusnMaxSize);
		break;

		default:
			break;
	}
	//!Return the updated index to store to copy data for the next packet transfer
	return usnNewDataIndex;
}
/******************************************************************************/
/*!
 * \fn void SysInitGetFirmwareVer()
 * \brief Fimware Version
 * \param uint16_t* pData
 * \brief Description:
 *     	Function is called to get the version of CPU1 and CPU2
 * \return void
 */
/******************************************************************************/
void SysInitGetFirmwareVer(uint16_t* pData)
{
	uint16_t usncount = ZERO;
	//!Set the data lenth to 6, It is the length for firmware packet
	uint16_t usnDataLength = 6;
	//!Copy CPU1 version number to data packet
	for(usncount = ZERO; usncount <3;usncount ++)
	{
		pData[usncount] = Cpu1FirmwareVersion[usncount];
        sprintf(statDebugPrintBuf, "\r\n%d", Cpu1FirmwareVersion[usncount]);
        UartDebugPrint((int *)statDebugPrintBuf, 50);
	}

    sprintf(statDebugPrintBuf, "\r\n%d", Cpu2FirmwareVersion[0]);
    UartDebugPrint((int *)statDebugPrintBuf, 50);
    sprintf(statDebugPrintBuf, "\r\n%d", Cpu2FirmwareVersion[1]);
    UartDebugPrint((int *)statDebugPrintBuf, 50);
    sprintf(statDebugPrintBuf, "\r\n%d", Cpu2FirmwareVersion[2]);
    UartDebugPrint((int *)statDebugPrintBuf, 50);
	//!Copy CPU2 version number to data packet
	for(usncount=3;usncount <usnDataLength;usncount++)
	{
		pData[usncount] = Cpu2FirmwareVersion[usncount-3];
	}
}
/******************************************************************************/
/*!
 * \fn  void SysInitGetSystemStatus()
 * \brief Description: Function to get the System status
 * \param void
 * \return void
 */
/******************************************************************************/
void SysInitGetSystemStatus(void)
{
	uint16_t usnCount = ZERO;

	//!Initialize pressure, Temperature, HGB, 24V and 5V to zero
    static uint32_t stat_ulnPressureAdc = ZERO;
    static uint32_t stat_ulnTemperatureAdc = ZERO;
    static uint32_t stat_ulnHgbAdc = ZERO;
    static uint32_t stat_ulnSense24Voltage = ZERO;
    static uint32_t stat_ulnSense5Voltage = ZERO;

    static bool stat_bOnce = TRUE;
    //! For the first time, add all the values in the array, from next interrupt
    //! remove the old element and add the latest ADC value and average by 32.
    if(TRUE == stat_bOnce)
    {
        for(usnCount = 0; usnCount < HEALTH_STATUS_ARRAY_SIZE; usnCount++)
        {
            stat_ulnPressureAdc = stat_ulnPressureAdc + \
                       (pstStatusSense->m_arrusnPressureAdc[usnCount]);
            stat_ulnTemperatureAdc = stat_ulnTemperatureAdc + \
                        (pstStatusSense->m_arrusnTemperatureAdc[usnCount]);
            stat_ulnHgbAdc = stat_ulnHgbAdc + \
                        (pstStatusSense->m_arrusnHgbAdc[usnCount]);
            stat_ulnSense24Voltage = stat_ulnSense24Voltage + \
                        (pstStatusSense->m_arrusnSense24Voltage[usnCount]);
            stat_ulnSense5Voltage  = stat_ulnSense5Voltage + \
                        (pstStatusSense->m_arrusnSense5Voltage[usnCount]);
        }
        stat_bOnce = FALSE;
    }
    int unCount;
    unCount = g_usnHealthindexCounter-1;
    if(unCount<0)
    {
        unCount = HEALTH_STATUS_ARRAY_SIZE-1;
    }

    stat_ulnPressureAdc = stat_ulnPressureAdc - \
            (pstStatusSense->m_arrusnPressureAdc[g_usnHealthindexCounter]) +
            (pstStatusSense->m_arrusnPressureAdc[unCount]);

    stat_ulnTemperatureAdc = stat_ulnTemperatureAdc - \
            (pstStatusSense->m_arrusnTemperatureAdc[g_usnHealthindexCounter]) +
            (pstStatusSense->m_arrusnTemperatureAdc[unCount]);

    stat_ulnHgbAdc = stat_ulnHgbAdc - \
            (pstStatusSense->m_arrusnHgbAdc[g_usnHealthindexCounter]) +
            (pstStatusSense->m_arrusnHgbAdc[unCount]);

    stat_ulnSense24Voltage = stat_ulnSense24Voltage - \
            (pstStatusSense->m_arrusnSense24Voltage[g_usnHealthindexCounter]) +
            (pstStatusSense->m_arrusnSense24Voltage[unCount]);

    stat_ulnSense5Voltage  = stat_ulnSense5Voltage - \
            (pstStatusSense->m_arrusnSense5Voltage[g_usnHealthindexCounter]) +
            (pstStatusSense->m_arrusnSense5Voltage[unCount]);

    //! Performing Shift Operator instade of deviding by 32 for faster performance.
	//m_usnPressureAdc = ulnActualPressureAdc = stat_ulnPressureAdc >> 5;

	m_usnPressureAdc = ulnActualPressureAdc = pstStatusSense->m_arrusnPressureAdc[unCount];
	m_usnTemperatureAdc = stat_ulnTemperatureAdc >> 5;
	//m_usnHgbNewAdc = stat_ulnHgbAdc >> 5;

	m_usnHgbNewAdc = pstStatusSense->m_arrusnHgbAdc[unCount];
	m_usnSense24Voltage = stat_ulnSense24Voltage >> 5;
	m_usnSense5Voltage = stat_ulnSense5Voltage >> 5;


	/*
	pstStatusSense->m_arrusnPressureAdc[g_usnHealthindexCounter] = m_usnPressureAdc;
	pstStatusSense->m_arrusnTemperatureAdc[g_usnHealthindexCounter] = m_usnTemperatureAdc;
	pstStatusSense->m_arrusnHgbAdc[g_usnHealthindexCounter] = m_usnHgbAdc;
	pstStatusSense->m_arrusnSense24Voltage[g_usnHealthindexCounter] = m_usnSense24Voltage;
	pstStatusSense->m_arrusnSense5Voltage[g_usnHealthindexCounter] = m_usnSense5Voltage;
    */
	// SAM - To be uncommented after fixing the ranges
	//SysInitCheckSystemHealthRanges();

	m_usnPressureAdc = (m_usnPressureAdc + sncalZeroPSI);

	/*//! Hari pressure calibration-Start
	if(g_usnZeroPressureCalSign == ZERO)
	{
	    m_usnPressureAdc = (m_usnPressureAdc - sncalZeroPSI);
	}
	else
	{
	    m_usnPressureAdc = (m_usnPressureAdc + sncalZeroPSI);
	}
	//! Hari pressure calibration-Ends*/

	if(m_bSystemStatusFlag)
    {
	    if(Counter>=2)
	    {
	        CIFormServiceHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);
	        m_bSystemStatusFlag = FALSE;
	    }
	    Counter++;
    }
    m_usnHgbAdc = m_usnHgbNewAdc;
}
/******************************************************************************/
/*!
 * \fn void SysInitValveTest(unsigned char ucValveNumber)
 * \brief Valve Check Status
 * \brief Description:
 *      To excite the valve when the corresponding Valve number is received.
 *      This assist in testing the valve functioning
 * \param ucValveNumber to store the valve number received from Sitara
 * \return void
 */
/******************************************************************************/
void SysInitValveTest(unsigned char ucValveNumber)
{
    //!Set the valve number which has to be excited
    m_ucValveNumber = ucValveNumber;
    //!Set the counter to excite the valve to particular time
    /*if()
        m_ucValveControlTime = VALVE_CONTROL_TIME;
    }
    else
    {
        m_ucValveControlTime = VALVE_DELAY_TIME;
    }*/
    //!Set valve check status to true
    m_bVlaveCheckStatus = true;
    //!Reset all the valves to zero.
    Uint16 usnValveStatus1 = ZERO;
    Uint16 usnValveStatus2 = ZERO;
    //!This condition is to map the valve number given to the corresponding SPI data to be sent to valve
    //! Do not change any of the number. Do not hardocde the valve no.s also.
    #ifdef _ALPHA_5_BOARD_
    switch (ucValveNumber)
       {
           case VALVE_1:
                usnValveStatus1 = 0x0080;
                usnValveStatus2 = 0x0000;
                break;
           case VALVE_2:
                usnValveStatus1 = 0x0040;
                usnValveStatus2 = 0x0000;
                break;
           case VALVE_3:
                usnValveStatus1 = 0x0008;
                usnValveStatus2 = 0x0000;
                break;
           case VALVE_4:
                usnValveStatus1 = 0x0000;
                usnValveStatus2 = 0x0020;
                break;
           case VALVE_5:
                usnValveStatus1 = 0x0000;
                usnValveStatus2 = 0x0010;
                break;
           case VALVE_6:
                usnValveStatus1 = 0x0000;
                usnValveStatus2 = 0x0008;
                break;
           case VALVE_7:
                usnValveStatus1 = 0x0001;
                usnValveStatus2 = 0x0000;
                break;
           case VALVE_8:
                usnValveStatus1 = 0x0004;
                usnValveStatus2 = 0x0000;
                break;
           case VALVE_9:
                usnValveStatus1 = 0x0010;
                usnValveStatus2 = 0x0000;
                break;
           case VALVE_10:
                usnValveStatus1 = 0x0002;
                usnValveStatus2 = 0x0000;
                break;
           case VALVE_11:
                usnValveStatus1 = 0x0000;
                usnValveStatus2 = 0x0001;
                break;
           case VALVE_12:
                usnValveStatus1 = 0x0000;
                usnValveStatus2 = 0x0004;
                break;
           case VALVE_13:
                usnValveStatus1 = 0x0000;
                usnValveStatus2 = 0x0002;
                break;
           case VALVE_14:
                usnValveStatus1 = 0x0000;
                usnValveStatus2 = 0x0080;
                break;
           case VALVE_15:
                usnValveStatus1 = 0x0000;
                usnValveStatus2 = 0x0040;
                break;
           case VALVE_16:
                usnValveStatus1 = 0x0020;
                usnValveStatus2 = 0x0000;
                break;
           default:
               break;
       }
       #else
    switch (ucValveNumber)
    {
        case VALVE_1:
        	 usnValveStatus1 = 0x0080;
             usnValveStatus2 = 0x0000;
             break;
        case VALVE_2:
        	 usnValveStatus1 = 0x0040;
             usnValveStatus2 = 0x0000;
             break;
        case VALVE_3:
             usnValveStatus1 = 0x0004;
             usnValveStatus2 = 0x0000;
             break;
        case VALVE_4:
             usnValveStatus1 = 0x0000;
             usnValveStatus2 = 0x0020;
             break;
        case VALVE_5:
             usnValveStatus1 = 0x0000;
             usnValveStatus2 = 0x0001;
             break;
        case VALVE_6:
             usnValveStatus1 = 0x0020;
             usnValveStatus2 = 0x0000;
             break;
        case VALVE_7:
             usnValveStatus1 = 0x0001;
             usnValveStatus2 = 0x0000;
             break;
        case VALVE_8:
             usnValveStatus1 = 0x0000;
             usnValveStatus2 = 0x0002;
             break;
        case VALVE_9:
             usnValveStatus1 = 0x0000;
             usnValveStatus2 = 0x0004;
             break;
        case VALVE_10:
             usnValveStatus1 = 0x0000;
             usnValveStatus2 = 0x0008;
             break;
        case VALVE_11:
             usnValveStatus1 = 0x0000;
             usnValveStatus2 = 0x0010;
             break;
        case VALVE_12:
             usnValveStatus1 = 0x0010;
             usnValveStatus2 = 0x0000;
             break;
        case VALVE_13:
             usnValveStatus1 = 0x0008;
             usnValveStatus2 = 0x0000;
             break;
        case VALVE_14:
             usnValveStatus1 = 0x0000;
             usnValveStatus2 = 0x0040;
             break;
        case VALVE_15:
             usnValveStatus1 = 0x0002;
             usnValveStatus2 = 0x0000;
             break;
        case VALVE_16:
             usnValveStatus1 = 0x0000;
             usnValveStatus2 = 0x0080;
             break;
        default:
            break;
    }
    #endif
    //!Excite the Corresponding Valve by sending the value to the valve SPI driver
    SIUpdateValveStatus(usnValveStatus1, usnValveStatus2);
}
/******************************************************************************/
/*!
 * \fn unsigned int SysWasteDetectGpioRead(void)
  *
 * \brief Reads Waste detect sense
 * \brief  Description:
 *      This function returns the float sensor status
 *
 * \return unsigned int of SensorStatus
 */
/******************************************************************************/
unsigned int SysWasteDetectGpioRead(void)
{
    //!Read the status of Waste detect GPIO and return
    return ((unsigned int)GPIO_ReadPin(WASTE_DETECT_GPIO));
}
/******************************************************************************/
/*!
 * \fn void SysValveStatusTest(void)
  *
 * \brief Valve test operation
 * \brief  To switch off the valve when the corresponding Valve is excited.
 *      This assist in testing the valve functioning
 * \return void
 */
/******************************************************************************/
void SysValveStatusTest(void)
{
  //!Check whether Valve check command received
  if(true == m_bVlaveCheckStatus)
  {

      //!Valvue test Operation
      if(ZERO == m_ucValveControlTime)
      {
          //sprintf(statDebugPrintBuf, "\r\n ValveTest false \n");
          //UartDebugPrint((int *)statDebugPrintBuf, 50);
          //!Reset the Valve check command received
          m_bVlaveCheckStatus = false;

          //!Disable the excitation of Valve
          SIUpdateValveStatus(ZERO, ZERO);

          //!Update the Valve Check completed status to Sitara
          CIValveTestCompleted(m_ucValveNumber);

          //!Clear the busy pin
          SPIClearCommCtrlBusyInt();

          //!Reset the Valve number
          m_ucValveNumber = ZERO;
      }
      //!If received, then decrement counter
      m_ucValveControlTime--;
  }
}
/******************************************************************************/
/*!
 * \fn void SysWasteOverflowCheck(void)
  *
 * \brief Waste detect sensor status check
 * \brief  Function to checks status of the waste detect sensor every second.
 * \return void
 */
/******************************************************************************/
void SysWasteOverflowCheck(void)
{
    //if(ZERO == g_usnCurrentMode)
    {
        //!check whether status check is initiated by sitara
        if(TRUE == g_bWasteDetectCheck)
        {
           //!check  whether the counter gets 1sec
            if(gucWasteCounter == WASTE_SESNE_INTERVAL)
            {
               //! if counter gets 1sec checks status of the sensor
               if(WASTE_DETECTED == SysWasteDetectGpioRead())
               {
                    gucWasteDetectCounter++;
                    if(gucWasteDetectCounter >=2)
                    {
                        //!Set wsate bin is full
                        utErrorInfoCheck.stErrorInfo.ulnWasteBinFull = TRUE;

                        if((g_usnCurrentModeMajor == SITARA_STARTUP_CMD) || ((g_usnCurrentMode ==START_UP_SEQUENCE_MODE)||\
                                (g_usnCurrentMode == MBD_STARTUP_FROM_SERVICE_SCREEN_CMD)))
                        {
                            sprintf(statDebugPrintBuf, "\r\nWasteBinFull in Start-Up\n");
                            UartDebugPrint((int *)statDebugPrintBuf, 50);
                            SysInitAbortProcess(TRUE);

                            //!at the time of startup is the waste bin is full then the wast bin error is not reporting
                            //! because of packet framing problem b/w waste packet and system status packet
                            //! so this dealy added to resolve this problem.
                            DELAY_US(3000000);    //30ms
                        }
                        //!Send command to Sitara indicating waste bin is full
                        //!Frame the error command and abort the sequence
                         //SysErrorCommand(g_usnCurrentMode);
                        //!Report the error only once
                        if(!(m_bWasteBinFull))
                        {
                            CIFormServiceHandlingPacket(WASTE_BIN_FULL_CMD, TRUE);
                        }
                        //!Set waste bin is full
                        m_bWasteBinFull = TRUE;

                        //CIFormServiceHandlingPacket(DELFINO_ERROR_MINOR_CMD, FALSE);
                        gucWasteDetectCounter = 0;
                        gucWasteCounter = 0;
                    }
               }
               else
               {
                   //!ReSet wsate bin is full
                   utErrorInfoCheck.stErrorInfo.ulnWasteBinFull = FALSE;

                   if(m_bWasteBinFull)
                   {
                       CIFormServiceHandlingPacket(WASTE_BIN_FULL_CMD, FALSE);
                   }
                   //!ReSet wsate bin is full
                   m_bWasteBinFull = FALSE;
               }
                //!reset the counter
                gucWasteCounter = ZERO;
            }
            //!increment the counter
            gucWasteCounter++;
        }
    }
}
/******************************************************************************/
/*!
 * \fn void SysPressureLineCheck(void)
 * \brief   monitor pressure sensor line tube is connected or not
 * \brief   Function to moitor pressure sensor line
 * \return  void
 */
/******************************************************************************/
void SysPressureLineCheck()     //Hari
{
    //!If the reagent is there than only start monitoring
    //! pressure sensore line presence
    //if(m_bWasteSyringeMonEnable == TRUE)

    if((g_usnCurrentMode == DRAIN_ALL_SERVICE_HANDLING) || \
            (g_usnCurrentMode == PRIME_ALL_SERVICE_HANDLING))
    {
        PressureCheckCounter = 0;
        utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail = FALSE;

    }
    else
    {
        if((FALSE == GPIO_ReadPin(DILUENT_PRIME_CHECK)))
        {
            //!If Waste motor is Running then check for pressure sensore
            //! line is connected or not
            if(START == PWMGetMotor1Status())
            {
                if(m_usnPressureAdc == prePresssureValue)
                //if((m_usnPressureAdc == prePresssureValue) || (((m_usnPressureAdc) <= prePresssureValue+2)&&(m_usnPressureAdc) >= prePresssureValue-2))
                {
                    PressureCheckCounter++;
                    if(PressureCheckCounter >= 25)
                    //if((m_usnPressureAdc == prePresssureValue) || (((m_usnPressureAdc) <= prePresssureValue+2)&&(m_usnPressureAdc) >= prePresssureValue-2))
                    {
                        //!SET the error
                        utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail = TRUE;

                        //!Arrest the current sequence and set the error
                        SysInitArrestSequence(TRUE);

                        //!Frame the error command and abort the sequence
                        SysErrorCommand(g_usnCurrentMode);

                        PressureCheckCounter = 0;
                    }
                }
                else
                {
                    PressureCheckCounter = 0;
                    utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail = FALSE;
                }
                //!Update the present Pressure value to prepressureValue
                prePresssureValue = m_usnPressureAdc;
            }
            else
            {
                PressureCheckCounter = 0;
            }

        }
        else
        {
            PressureCheckCounter = 0;
        }
    }
}
/******************************************************************************/
/*!
 * \fn void SysPressureCalibration(void)
 * \brief   pressure calibration method
 * \brief   Function to calibrate the ADC pressure value
 * \return  void
 */
/******************************************************************************/
void SysPressureCalibration()
{
    static unsigned char LocalCounter = 0;

    if(g_bPressureCaliStart == TRUE)
       {
           if(LocalCounter >= 5)
           {
               //!After removing pressure line if the pressure value is greater
               //! than +- 1 PSI then report the error
               //! (2380 -> +1psi and 2132 -> -1psi)
               //! for 15 psi sensor
               if((m_usnPressureAdc > 2380) || (m_usnPressureAdc < 2132))
               {
               //! for 30 psi sensor
//               if((m_usnPressureAdc > 2313) || (m_usnPressureAdc < 2190))
//               {
                   //! Report Pressures sensore failure error to Master
                   //!SET the error
                   utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail = TRUE;
                   //!Frame the error command and abort the sequence
                   SysErrorCommand(g_usnCurrentMode);
                   g_bPressureCaliStart = FALSE;

               }
               else
               {
                   //! Take the new ADC value which is equal to 0 PSI
                   //sncalZeroPSI = (2253 - m_usnPressureAdc);
                   sncalZeroPSI = (2253 - (ulnActualPressureAdc));

                   sprintf(statDebugPrintBuf, "\r\n sncalZeroPSI :%ld  \n",sncalZeroPSI);
                   UartDebugPrint((int *)statDebugPrintBuf, 50);

                   //!Reset the g_bPressureCaliStart so that calibartion will stop
                   g_bPressureCaliStart = FALSE;
                   LocalCounter = ZERO;
                   //! Clear the busy state
                   SPIClearCommCtrlBusyInt();

                   CPFrameDataPacket(DELFINO_SYSTEM_SETTING_CMD, \
                                              DELFINO_PRESSURE_CALIBRATED_DATA, FALSE);

                   /*//!Send Pressure calibartion completed to sitara as an error
                   utErrorInfoCheck.stErrorInfo.PressureCalCmpltd = TRUE;
                   //!Frame the error command
                   SysErrorCommand(g_usnCurrentMode);*/

               }
           }
           LocalCounter++;
       }
}
/******************************************************************************/
/*!
 * \fn void SysInitWasteBinReplaced(void)
 * \brief  	Called when  Waste Bin replaced command is received from SITARA
 * \brief  	Function to update Waste detect check variable
 * \return 	void
 */
/******************************************************************************/
void SysInitWasteBinReplaced()
{
	//!set the WasteDetectCheck variable to TRUE
	g_bWasteDetectCheck = TRUE;
	//!Clear waste bin full error
	m_bWasteBinFull = FALSE;
	//!Clear waste bin full error
	utErrorInfoCheck.stErrorInfo.ulnWasteBinFull = FALSE;
}
/******************************************************************************/
/*!
 * \fn void SysInitHealthStatusCheck(void)
 * \brief  	Function to check the system health status
 * \return 	void
 */
/******************************************************************************/
void SysInitHealthStatusCheck()
{
    //!Check if the health check status is set to true and no multiple packets
    //! are transmitted during this check
	if(((m_bMutiplePackets == FALSE) || \
	        (m_bLastMultiplePacket == FALSE)) || \
	        ((m_bSPIWriteControlStatus == FALSE ) && \
	                (FALSE == m_bTxPendingFlag)))
	{
	    //!Acquire the health status every 20mSec, do not make it false
	    SysInitCheckSystemHealthRanges();
	}
}
/******************************************************************************/
/*!
 * \fn void SysInitSystemHealthData(void)
 * \brief  	Function to update the system health status to data array
 * \return 	void
 * \Author	SBS
 */
/******************************************************************************/
void SysInitSystemHealthData(uint16_t* pData)
{
    //!Update the data packet of the system status command
    //QA_C.
	pData[0] = m_usnPressureAdc;
	pData[1] = m_usnTemperatureAdc;
	pData[2] = m_usnHgbAdc;
	pData[3] = m_usnSense5Voltage;
	pData[4] = m_usnSense24Voltage;
#if 0
	//!If the waste bin is full set the status of the waste bin
	if(m_bWasteBinFull == TRUE)
	{
	    sprintf(statDebugPrintBuf, " m_bWasteBinFull set \n");
	    UartDebugPrint((int *)statDebugPrintBuf, 50);

		pData[5] = SET;
	}
	else
	{
		pData[5] = RESET;
	}
#endif
	pData[5] = m_usnHgbNewAdc;

}
/******************************************************************************/
/*!
 * \fn void SysInitCheckSystemHealthRanges(void)
 * \brief  	Function to check the system health values against the ranges
 * \return 	void
 */
/******************************************************************************/
void SysInitCheckSystemHealthRanges(void)
{
    static unsigned char ucSysHealthMonState = 0;

    static uint16_t usnTempValue = 0;
    static uint16_t usnPressureTempValue = 0;
    static uint16_t usn24VTempValue = 0;
    static uint16_t usn5VTempValue = 0;
    static uint16_t usnMotor1FaultCounter = 0;
    static uint16_t usnMotor2FaultCounter = 0;
    static uint16_t usnMotor3FaultCounter = 0;
    static uint16_t usnMotor4FaultCounter = 0;
    static uint16_t usnMotor5FaultCounter = 0;

    static bool incTemp = true;
    static bool incPres = true;
    static bool inc24V = true;
    static bool inc5V = true;

    if(incTemp)
    {
        usnTempValue++;
        if(usnTempValue > (DEFAULT_SYSTEM_TEMPERATURE_UPPER_LIMIT+500))
        {
            incTemp = false;
        }
    }
    else
    {
        usnTempValue--;
        if(usnTempValue < (DEFAULT_SYSTEM_TEMPERATURE_LOWER_LIMIT - 500))
        {
            incTemp = true;
        }
    }

    if(incPres)
    {
        usnPressureTempValue++;
        usnPressureTempValue++;
        if(usnPressureTempValue > (DEFAULT_SYSTEM_PRESSURE_UPPER_LIMIT+500))
        {
            incPres = false;
        }
    }
    else
    {
        usnPressureTempValue--;
        usnPressureTempValue--;
        if(usnPressureTempValue < (DEFAULT_SYSTEM_PRESSURE_LOWER_LIMIT - 500))
        {
            incPres = true;
        }
    }


    if(inc24V)
    {
        usn24VTempValue++;
        if(usn24VTempValue > (DEFAULT_SYSTEM_24_VOLTS_UPPER_LIMIT+500))
        {
            inc24V = false;
        }
    }
    else
    {
        usn24VTempValue--;
        if(usn24VTempValue < (DEFAULT_SYSTEM_24_VOLTS_LOWER_LIMIT - 500))
        {
            inc24V = true;
        }
    }


    if(inc5V)
    {
        usn5VTempValue++;
        if(usn5VTempValue > (DEFAULT_SYSTEM_5_VOLTS_UPPER_LIMIT+500))
        {
            inc5V = false;
        }
    }
    else
    {
        usn5VTempValue--;
        if(usn5VTempValue < (DEFAULT_SYSTEM_5_VOLTS_LOWER_LIMIT - 500))
        {
            inc5V = true;
        }
    }
    /*Motor1 Fault check */
    if(g_bYMotorFaultMonEn == TRUE)
    {
        if(FALSE == SMDGetMotorFault(MOTOR5_SELECT))
        {
            usnMotor1FaultCounter++;
            if(usnMotor1FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                usnMotor1FaultCounter = 0;
                //!Set Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnY_MotorFault = TRUE;
                sprintf(DebugPrintBuf, "\n Y_Motor_Fault \n");
                UartDebugPrint((int *)DebugPrintBuf, 50);

                //!Arresting the sequence when error occur
                //SysInitArrestSequence(TRUE);
                g_bYMotorFaultMonEn = FALSE;
            }

        }
        else
        {
            usnMotor1FaultCounter = 0;
        }
    }
    else
    {
        if(TRUE == SMDGetMotorFault(MOTOR5_SELECT))
        {
            usnMotor1FaultCounter++;
            if(usnMotor1FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                g_bYMotorFaultMonEn = true;
                usnMotor1FaultCounter = 0;
                //!ReSet Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnY_MotorFault = FALSE;
            }
            else
            {
                usnMotor1FaultCounter = 0;
            }

        }
    }

    if(g_bXMotorFaultMonEn == TRUE)
    {
        if(FALSE == SMDGetMotorFault(MOTOR4_SELECT))
        {
            usnMotor2FaultCounter++;
            if(usnMotor2FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                usnMotor2FaultCounter = 0;
                //!Set Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnX_MotorFault = TRUE;
                sprintf(DebugPrintBuf, "\n Y_Motor_Fault \n");
                UartDebugPrint((int *)DebugPrintBuf, 50);

                //!Arresting the sequence when error occur
                //SysInitArrestSequence(TRUE);
                g_bXMotorFaultMonEn = FALSE;
            }

        }
        else
        {
            usnMotor2FaultCounter = 0;
        }
    }
    else
    {
        if(TRUE == SMDGetMotorFault(MOTOR4_SELECT))
        {
            usnMotor2FaultCounter++;
            if(usnMotor2FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                g_bXMotorFaultMonEn = true;
                usnMotor2FaultCounter = 0;
                //!ReSet Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnX_MotorFault = FALSE;
            }
            else
            {
                usnMotor2FaultCounter = 0;
            }

        }
    }

    if(g_bDilMotorFaultMonEn == TRUE)
    {
        if(FALSE == SMDGetMotorFault(MOTOR2_SELECT))
        {
            usnMotor3FaultCounter++;
            if(usnMotor3FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                usnMotor3FaultCounter = 0;
                //!Set Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnDiluent_MotorFault = TRUE;
                sprintf(DebugPrintBuf, "\n Y_Motor_Fault \n");
                UartDebugPrint((int *)DebugPrintBuf, 50);

                //!Arresting the sequence when error occur
                SysInitArrestSequence(TRUE);
                g_bDilMotorFaultMonEn = FALSE;
            }

        }
        else
        {
            usnMotor3FaultCounter = 0;
        }
    }
    else
    {
        if(TRUE == SMDGetMotorFault(MOTOR2_SELECT))
        {
            usnMotor3FaultCounter++;
            if(usnMotor3FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                g_bDilMotorFaultMonEn = true;
                usnMotor3FaultCounter = 0;
                //!ReSet Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnDiluent_MotorFault = FALSE;
            }
            else
            {
                usnMotor3FaultCounter = 0;
            }

        }
    }

    if(g_bWasteMotorFaultMonEn == TRUE)
    {
        if(FALSE == SMDGetMotorFault(MOTOR1_SELECT))
        {
            usnMotor4FaultCounter++;
            if(usnMotor4FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                usnMotor4FaultCounter = 0;
                //!Set Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnWaste_MotorFault = TRUE;
                sprintf(DebugPrintBuf, "\n Y_Motor_Fault \n");
                UartDebugPrint((int *)DebugPrintBuf, 50);

                //!Arresting the sequence when error occur
                SysInitArrestSequence(TRUE);
                g_bWasteMotorFaultMonEn = FALSE;
            }

        }
        else
        {
            usnMotor4FaultCounter = 0;
        }
    }
    else
    {
        if(TRUE == SMDGetMotorFault(MOTOR1_SELECT))
        {
            usnMotor4FaultCounter++;
            if(usnMotor4FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                g_bWasteMotorFaultMonEn = true;
                usnMotor4FaultCounter = 0;
                //!ReSet Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnWaste_MotorFault = FALSE;
            }
            else
            {
                usnMotor4FaultCounter = 0;
            }

        }
    }
    if(g_bSampleMotorFaultMonEn == TRUE)
    {
        if(FALSE == SMDGetMotorFault(MOTOR3_SELECT))
        {
            usnMotor5FaultCounter++;
            if(usnMotor5FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                usnMotor5FaultCounter = 0;
                //!Set Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnSample_MotorFault = TRUE;
                sprintf(DebugPrintBuf, "\n Y_Motor_Fault \n");
                UartDebugPrint((int *)DebugPrintBuf, 50);

                //!Arresting the sequence when error occur
                SysInitArrestSequence(TRUE);
                g_bSampleMotorFaultMonEn = FALSE;
            }

        }
        else
        {
            usnMotor5FaultCounter = 0;
        }
    }
    else
    {
        if(TRUE == SMDGetMotorFault(MOTOR3_SELECT))
        {
            usnMotor5FaultCounter++;
            if(usnMotor5FaultCounter > MOTOR_FAULT_CONDITION_TIME)
            {
                g_bSampleMotorFaultMonEn = true;
                usnMotor1FaultCounter = 0;
                //!ReSet Motor Fault Error
                utErrorInfoCheck.stErrorInfo.ulnSample_MotorFault = FALSE;
            }
            else
            {
                usnMotor5FaultCounter = 0;
            }

        }
    }
    //m_usnTemperatureAdc = usnTempValue;
    //m_usnPressureAdc = usnPressureTempValue;
    //m_usnSense24Voltage = usn24VTempValue;
    //m_usnSense5Voltage = usn5VTempValue;

    ucSysHealthMonState++;

    //SysCheck_5V
    if(ucSysHealthMonState > 6)
    {
        ucSysHealthMonState = 1;
    }

    switch(ucSysHealthMonState)
    {
    case 1:
        SysCheckPressure();
        break;

    case 2:
        SysCheckTemperature();
        break;

    case 3:
        SysCheck_24V();
        break;

    case 4:
        SysCheck_5V();
        break;
    case 5:
        MotorFaultCheck();
        break;
    case 6:
        //SysWasteOverflowCheck();
        break;

    default:
        ucSysHealthMonState = 1;
        break;
    }
}
/******************************************************************************/
/*!
 * \fn void SysInitAbortProcess(void)
 * \brief  	Function to abort the process
 * \return 	void
 * \Author	SBS
 */
/******************************************************************************/
void SysInitAbortProcess(bool_t bSendCommandToSitara)
{

    if(bSendCommandToSitara)
    {
        sprintf(DebugPrintBuf, "\r\nSysInitAbortProcess %d \n", BloodCellCounter_Y.STATE);
        UartDebugPrint((int *)DebugPrintBuf, 50);

        utErrorInfoCheck.stErrorInfo.ulnSequenceAborted = TRUE;
        //!Disable the start button for next measurement
        g_bStartButtonStatus = FALSE;

        //!Clear the busy pin so that Processor can send the next command
        SPIClearCommCtrlBusyInt();

        //!Re-initialize all the variables of the MBD sequence immediately after
        //!aborting the sequence
        BloodCellCounter_initialize();
        
        //!Set all the motors to Idle mode - No Motor shall shall run after
        //!any process is aborted
        SysMotorControlIdleMode();

        PWMParamDefaultState(); //HN_add

        //!Starting of Abort sequence xposition should be invalid
        m_eXPosition = eXPositionInvalid;


        //!The current execution mode shall be set to no mode
        g_usnCurrentModeMajor = SITARA_ABORT_MAIN_PROCESS_CMD;
        //!The current execution mode shall be set to no mode
        g_usnCurrentMode = MBD_USER_ABORT_PROCESS;
        //!The current temperory execution mode (for pre-diluent)
        //!shall be set to no mode
        g_usnCurrentModeTemp = ZERO;

        m_eCycleCompletion = eDefault;

        //!Enable the flag for ready for aspiration as soon as the trigger
        //! button is pressed
        m_bReadyforAspCommand = TRUE;

        //!Reset the volumetric error
        utVolumetricErrorInfo.unVolumetricError = ZERO;
        utVolumetricErrorInfoReAcq.unVolumetricError = ZERO;

    }
}
//SAM_ABORT - END
/******************************************************************************/
/*!
 * \fn uint16_t SysInitGetCountingTime(uint16_t* pData)
 * \brief  Counting Time
 * \brief Description:
 *     	Function is called to get the counting time for WBC and RBC
 * \param pData pointer variable to store the data
 * \return the datalength
 */
/******************************************************************************/
uint16_t SysInitGetCountingTime(uint16_t* pData)
{
    //!Set the WBC and RBC counting time to the data packet
	pData[0] = m_usnWBCCountTime;//SAM_COUNT_TIME
	pData[1] = m_usnRBCCountTime;//SAM_COUNT_TIME

	//!Return the counting data length
	return COUNTING_TIME_DATA_LENGTH;

}
/******************************************************************************/
/*!
 * \fn void SysStateIntervalCheck(void)
 * \brief Toset parameter defaults value
 * \brief Description:
 *      To verify whether the interval between the states are within the range
 * \return void
 */
/******************************************************************************/
void SysStateIntervalCheck(uint16_t usnSequenceState)
{
    static uint16_t usnExpectedState = ZERO;
    //!Check if current state is matching to next expected state
    if((ZERO != usnExpectedState) && (usnSequenceState != usnExpectedState))
    {
        //!Abort the sequence and don't send any command to Sitara
        //!Sitara shall abort the Sequence on the UI end based on the
        //!time out and Send sequence aborted information to Delfino
        //SysInitAbortProcess(FALSE);
        //return;
    }


    switch(usnExpectedState)
    {
        case eREADY_FOR_DISPENSING:
            //sprintf(DebugPrintBuf, "\nExp: eREADY_FOR_DISPENSING\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            break;

        case eDISPENSE_COMPLETED:
            //sprintf(DebugPrintBuf, "\nExp: eDISPENSE_COMPLETED\n");
            //                UartDebugPrint((int *)DebugPrintBuf, 50);
            break;

        case eREADY_FOR_ASPIRATION:
            //sprintf(DebugPrintBuf, "\nExp: eREADY_FOR_ASPIRATION\n");
            //                UartDebugPrint((int *)DebugPrintBuf, 50);
            break;

        case eASPIRATION_COMPLETED_MONITORING:

            //!Check whether the state time interval is within the range
            if(g_unSequenceStateTime > ASPIRATION_COMPLETED_TIME_HIGH)
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                //SysInitAbortProcess(FALSE);
                //!Set the error code for Aspiration completed
            }
            break;
/*        case eWBC_BUBBLING_STARTED:
            //sprintf(DebugPrintBuf,"\n\r eWBC_BUBBLING_STARTED\n\r");
            //UartDebugPrint((int *)DebugPrintBuf, 50);
             break;*/

        case eDRAINING_BC_STARTED:
            break;

        case eDRAINING_BC_INTER_STARTED:
            break;

        case eDRAINING_BC_COMPLETED:
            break;

        case eWBC_BUBBLING_COMPLETED:
            break;

        case eFIRST_DILUTION_COMPLETED:
            //!Check whether the state time interval is within the range
            if(g_unSequenceStateTime > FIRST_DILUTION_COMPLETED_TIME_HIGH)
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            break;

        case eRBC_DISPENSATION_COMPLETED:
            //!Check whether the state time interval is within the range
            if(g_unSequenceStateTime > RBC_DISPENSATION_COMPLETED_TIME_HIGH)
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }

            break;

        case eWBC_RBC_BUBBLING_INTER_STARTED:
            break;

        case eWBC_RBC_BUBBLING_COMPLETED:
            //!Check whether the state time interval is within the range
            if(g_unSequenceStateTime > BUBBLING_COMPLETED_TIME_HIGH)
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            break;

        case eCOUNTING_STARTED:
            //PH_CHANGE_07_MAR_17 - START
            //!Check for the vacuum for Counting
            //!If the vacuum for counting is not with in the range
            //!Indicate vacuum error
            //PH_CHANGE_07_MAR_17 - END

            //!Check whether the state time interval is within the range
            if(g_unSequenceStateTime > COUNTING_STARTED_TIME_HIGH)
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            break;

        case eCOUNTING_COMPLETED:

            //!Check whether the state time interval is within the range
            if(g_unSequenceStateTime > COUNTING_COMPLETED_TIME_HIGH)
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            break;

        case eDRAINING_AC_STARTED:
            break;

        case eDRAINING_AC_INTER_STARTED:
            break;

        case eDRAINING_AC_COMPLETED:
            break;

        case eBACKFLUSH_STARTED:

            //PH_CHANGE_07_MAR_17 - START
            //!Check for the pressure for Backflush
            //!If the pressure for Backflush is not with in the range
            //!Indicate Backflush pressure error
            //PH_CHANGE_07_MAR_17 - END
            break;

        case eBACKFLUSH_COMPLETED:
            //!Check whether the state time interval is within the range
            if(g_unSequenceStateTime > BACKFLUSH_COMPLETED_TIME_HIGH)
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            break;

        case eDRAINING_FINAL_STARTED:
            break;

        case eDRAINING_FINAL_INTER_STARTED:
            break;

        case eDRAINING_FINAL_COMPLETED:

            break;

        case eBATH_FILLING_COMPLETED:

            //Acquire the HgB blank data
            //m_bHgBBlankAcq = TRUE;

            //!Check whether the state time interval is within the range
            if(g_unSequenceStateTime > BATH_FILLING_COMPLETED_TIME_HIGH)
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            break;

        default:
            break;
    }
    switch(usnSequenceState)
    {
        case eSEQUENCE_STARTED:

            if(g_bBusyBitStatus == FALSE)
            {
                SPISetCommCtrlBusyInt();
            }
            //!Initialize all the home position status
            //SysHomePositionInit();
            sprintf(DebugPrintBuf, "\r\n eSEQUENCE_STARTED %ld\n", g_unSequenceStateTime);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            //!If the current mode is pre-diluent, then the expected state is
            //! Ready for dispensing, else if the current mode is whole blood
            //! the expected state is ready for aspiration
            if(PRE_DILUENT_MODE == g_usnCurrentMode)
            {
                usnExpectedState = eREADY_FOR_DISPENSING;

            }
            else
            {
                if(WHOLE_BLOOD_MODE == g_usnCurrentMode)
                {
                    usnExpectedState = eREADY_FOR_ASPIRATION;
                }
            }
            //!Frame and sequence started packet to Sitara
            CIFormSequneceStartedCmd();
            //!Set X-Axis position to Aspiration position
            //m_eXPosition = eASPIRATION;
            break;

        case eREADY_FOR_DISPENSING:
            //!Set expected state to Dispensing completed
            usnExpectedState = eDISPENSE_COMPLETED;
            //!Set X-Axis position to Aspiration position
           // m_eXPosition = eASPIRATION;
            break;

        case eDISPENSE_COMPLETED:
            //!Set expected state to ready for aspiration
            usnExpectedState = eREADY_FOR_ASPIRATION;
            //!Set X-Axis position to Aspiration position
            //m_eXPosition = eASPIRATION;
            break;

        case eREADY_FOR_ASPIRATION:
            //!Check if the sequence time is with in the expected limit for the specified state
            if((g_unSequenceStateTime >= READY_FOR_ASPIRATION_LOW) && \
                    (g_unSequenceStateTime <= READY_FOR_ASPIRATION_HIGH))
            {
                //Do nothing
                //Send the sequence status to Sitara
                //CIFormSequenceStatusPacket(usnSequenceState);
                //usnExpectedState = eDRAINING_BC_STARTED;
                //sprintf(DebugPrintBuf, "\n eREADY_FOR_ASPIRATION %ld\n", g_unSequenceStateTime);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);

            }
            //!Set expected state to aspiration completed monitoring
            usnExpectedState = eASPIRATION_COMPLETED_MONITORING;
            //!Set X-Axis position to Aspiration position
            //m_eXPosition = eASPIRATION;
            break;

        case eASPIRATION_COMPLETED_WB:
            //!Set X-Axis position to invalid
            //m_eXPosition = eXPositionInvalid;
            break;

        case eASPIRATION_COMPLETED_MONITORING:
            //!Check if the sequence time is with in the expected limit for the specified state
            if((g_unSequenceStateTime >= ASPIRATION_COMPLETED_TIME_LOW) && \
                    (g_unSequenceStateTime <= ASPIRATION_COMPLETED_TIME_HIGH))
            {
                //Do nothing
                //Send the sequence status to Sitara
                //usnExpectedState = eDRAINING_BC_STARTED;
            }
            else
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            //!Form seqeunce state packet and send to Sitara
            CIFormSequenceStatusPacket(usnSequenceState);
            //!Set expected state to Draining before counting started
            usnExpectedState = eDRAINING_BC_STARTED;
            //m_eXPosition = eWBC_BATH;
            break;

        case eDRAINING_BC_STARTED:
            //Enable Pressure Monitoring
            m_bPressureMonitor = TRUE;
            //!Check for the starting pressure
            //!If the vacuum for WBC draining is less than the expected vacuum
            //!Indicate draining error
            if(m_usnPressureAdc <= DRAINING_BC_STARTED_PRESSURE)
            {
                //!Clear error - WBC Draining before counting
            }
            else
            {
                //!Set error - WBC Draining before counting
            }
            //!Set expected state to intermediate draining started before counting
            usnExpectedState = eDRAINING_BC_INTER_STARTED;
           // m_eXPosition = eXPositionInvalid;
            break;

        case eDRAINING_BC_INTER_STARTED:
            //Enable Pressure Monitoring
            m_bPressureMonitor = TRUE;
            //!Check for the starting pressure
            //!If the vacuum for RBC draining is less than the expected vacuum
            //!Indicate draining error
            if(m_usnPressureAdc <= DRAINING_BC_INTER_STARTED_PRESSURE)
            {
                //!Clear error - RBC Draining before counting
            }
            else
            {
                //!Set error - RBC Draining before counting
            }
            //!Set expected state to draining completed before counting
            usnExpectedState = eDRAINING_BC_COMPLETED;
            break;

        case eDRAINING_BC_COMPLETED:
            //!Enable Pressure Monitoring
            m_bPressureMonitor = TRUE;
            //!Check for the starting pressure
            //!If the vacuum for after draing is not with in the range
            //!Indicate draining error
            if(m_usnPressureAdc <= DRAINING_BC_COMPLETED_PRESSURE)
            {
                //!Clear error - RBC Draining before counting
            }
            else
            {
                //!Set error - RBC Draining before counting
            }
            //!Set expected state to WBC Bubbling started
            usnExpectedState = eWBC_BUBBLING_STARTED;
            break;

        case eWBC_BUBBLING_STARTED:
            sprintf(DebugPrintBuf, "\r\n eWBC_BUBBLING_STARTED: %ld\n", g_unSequenceStateTime);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            BloodCellCounter_Y.STATE = eSTATE_INVALID;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;  //HN_added
            g_bFirstMixingState = true;    //Testing
            MixingTime = g_DiluentMixTime;
            Counter = 0;


            //!Enable Pressure Monitoring
            m_bPressureMonitor = TRUE;
            //!Check for the starting pressure
            //!If the pressure for WBC Bubbling is not with in the range
            //!Indicate WBC Bubbling error
            if(m_usnPressureAdc >= WBC_BUBBLING_STARTED_PRESSURE)
            {
                //!Clear error - WBC bubbling started
            }
            else
            {
                //!Set error - WBC bubbling started
            }
            //!Set expected state to WBC bubbling completed
            usnExpectedState = eWBC_BUBBLING_COMPLETED;
            break;

        case eWBC_BUBBLING_COMPLETED:
            //!Disable Pressure Monitoring
            m_bPressureMonitor = TRUE;
            //!Check for the starting pressure
            //!If the pressure for WBC Bubbling is not with in the range
            //!Indicate WBC Bubbling error
            if(m_usnPressureAdc >= WBC_BUBBLING_COMPLETED_PRESSURE)
            {
                //!Clear error - WBC bubbling completed
            }
            else
            {
                //!Set error - WBC bubbling completed
            }
            //!Set expected state to first dilution completed
            usnExpectedState = eFIRST_DILUTION_COMPLETED;
            break;

        case eFIRST_DILUTION_COMPLETED:
            //!Disable Pressure Monitoring
            //m_bPressureMonitor = FALSE;
            //!Check if the sequence time is with in the expected limit for the specified state
            if((g_unSequenceStateTime >= FIRST_DILUTION_COMPLETED_TIME_LOW) \
             && (g_unSequenceStateTime <= FIRST_DILUTION_COMPLETED_TIME_HIGH))
            {
                //Do nothing
                //Send the sequence status to Sitara
                //usnExpectedState = eRBC_DISPENSATION_COMPLETED;
            }
            else
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            //!Form seqeunce state packet and send to Sitara
            CIFormSequenceStatusPacket(usnSequenceState);
            //!Set expected state to RBC dispensation completed
            usnExpectedState = eRBC_DISPENSATION_COMPLETED;
            break;

        case eRBC_DISPENSATION_COMPLETED:
            //!Check whether the state time interval is within the range
            if((g_unSequenceStateTime >= \
                    RBC_DISPENSATION_COMPLETED_TIME_LOW) && \
                    (g_unSequenceStateTime <= \
                            RBC_DISPENSATION_COMPLETED_TIME_HIGH))
            {
                //!Send the sequence status to Sitara
                //usnExpectedState = eWBC_RBC_BUBBLING_STARTED;
            }
            else
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            //!Form seqeunce state packet and send to Sitara
            CIFormSequenceStatusPacket(usnSequenceState);
            //!Set expected state to RBC bubbling started
            usnExpectedState = eWBC_RBC_BUBBLING_STARTED;
            //m_eXPosition = eRBC_BATH;
            break;

        case eWBC_RBC_BUBBLING_STARTED:
            //!Start Monitoring the pressure
            m_bPressureMonitor = TRUE;
            //!Check for the starting pressure
            //!If the pressure for WBC Bubbling is not with in the range
            //!Indicate WBC Bubbling error
            if(m_usnPressureAdc >= WBC_RBC_BUBBLING_STARTED_PRESSURE)
            {
                //!Clear error - WBC bubbling started
            }
            else
            {
                //!Set error - WBC bubbling started
            }
            //!Set expected state to WBC RBC intermediate state started
            usnExpectedState = eWBC_RBC_BUBBLING_INTER_STARTED;
            break;

        case eWBC_RBC_BUBBLING_INTER_STARTED:
            //!Start Monitoring the pressure
            m_bPressureMonitor = TRUE;
            //!Check for the starting pressure
            //!If the pressure for RBC Bubbling is not with in the range
            //!Indicate RBC Bubbling error
            if(m_usnPressureAdc >= WBC_RBC_BUBBLING_INTER_STARTED_PRESSURE)
            {
                //!Clear error - RBC bubbling started
            }
            else
            {
                //!Set error - RBC bubbling started
            }
            //!Set expected state to WBC RBC bubbling completed
            usnExpectedState = eWBC_RBC_BUBBLING_COMPLETED;
            break;

        case eWBC_RBC_BUBBLING_COMPLETED:
            //!Stop Monitoring the pressure
            //m_bPressureMonitor = FALSE;
            sprintf(DebugPrintBuf, "\r\n eWBC_RBC_BUBBLING_INTER_STARTED: %ld\n", g_unSequenceStateTime);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            //!If the pressure for RBC Bubbling completed is not with in the range
            //!Indicate RBC Bubbling error
            if(m_usnPressureAdc >= WBC_RBC_BUBBLING_COMPLETED_PRESSURE)
            {
                //!Clear error - RBC bubbling completed
            }
            else
            {
                //!Set error - RBC bubbling completed
            }

            //!Check whether the state time interval is within the range
            if((g_unSequenceStateTime >= BUBBLING_COMPLETED_TIME_LOW) && \
                    (g_unSequenceStateTime <= BUBBLING_COMPLETED_TIME_HIGH))
            {
                //Do nothing
                //!Send the sequence status to Sitara
                //DELAY_US(200000);
                //CIFormSequenceStatusPacket(usnSequenceState);
                //usnExpectedState = eCOUNTING_STARTED;
            }
            else
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            //!Form seqeunce state packet and send to Sitara
            CIFormSequenceStatusPacket(usnSequenceState);
            //!Set expected state to counting started
            usnExpectedState = eCOUNTING_STARTED;
            break;

        case eCOUNTING_STARTED:
            //Start Monitoring the pressure
            m_bPressureMonitor = TRUE;
            //!Check for the starting pressure
            //!If the pressure for counting is not with in the range
            //!Indicate Counting pressure error
            if((m_usnPressureAdc >= COUNTING_STARTED_PRESSURE_LOW) && \
                    (m_usnPressureAdc <= COUNTING_STARTED_PRESSURE_HIGH))
            {
                //!Clear error - Counting starting pressure
            }
            else
            {
                //!Set error - Counting starting Pressure
            }
            //!Check whether the state time interval is within the range
            if((g_unSequenceStateTime >= COUNTING_STARTED_TIME_LOW) && \
                    (g_unSequenceStateTime <= COUNTING_STARTED_TIME_HIGH))
            {
                //!Do nothing
                //!Send the sequence status to Sitara
                //usnExpectedState = eCOUNTING_COMPLETED;
            }
            else
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            //!Form seqeunce state packet and send to Sitara
            CIFormSequenceStatusPacket(usnSequenceState);
            //!Set expected state to counting completed
            usnExpectedState = eCOUNTING_COMPLETED;
            break;

        case eCOUNTING_COMPLETED:
            //!start Monitoring the pressure
            //m_bPressureMonitor = FALSE;
            //!Check for the ending pressure
            //!If the pressure for counting is not with in the range
            //!Indicate Counting pressure error
            if((m_usnPressureAdc >= COUNTING_COMPLETED_PRESSURE_LOW) && \
                    (m_usnPressureAdc <= COUNTING_COMPLETED_PRESSURE_HIGH))
            {
                //!Clear error - Counting starting pressure
            }
            else
            {
                //!Set error - Counting starting Pressure
            }
            //check whether the state time interval is within the range
            if((g_unSequenceStateTime >= COUNTING_COMPLETED_TIME_LOW) && \
                    (g_unSequenceStateTime <= COUNTING_COMPLETED_TIME_HIGH))
            {
                //!Do nothing
                //!Send the sequence status to Sitara
                //usnExpectedState = eDRAINING_AC_STARTED;
            }
            else
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            //!Form seqeunce state packet and send to Sitara
            CIFormSequenceStatusPacket(usnSequenceState);
            //!Set expected state to first draing after counting started
            usnExpectedState = eDRAINING_AC_STARTED;
            break;

        case eDRAINING_AC_STARTED:
            //!start Monitoring the pressure
            //m_bPressureMonitor = TRUE;
            //!Check for the ending pressure
            //!If the pressure for draining is not with in the range
            //!Indicate Counting pressure error
            if(m_usnPressureAdc >= DRAINING_AC_STARTED_PRESSURE)
            {
                //!Clear error - WBC draining pressure
            }
            else
            {
                //!Set error - WBC draining pressure
            }
            //!Set expected state to first intermediate state of draining after
            //! counting started
            usnExpectedState = eDRAINING_AC_INTER_STARTED;
            break;

        case eDRAINING_AC_INTER_STARTED:
            //!start Monitoring the pressure
            //m_bPressureMonitor = TRUE;
            //!Check for the ending pressure
            //!If the pressure for draining is not with in the range
            //!Indicate Counting pressure error
            if(m_usnPressureAdc >= DRAINING_AC_INTER_STARTED_PRESSURE)
            {
                //!Clear error - RBC draining pressure
            }
            else
            {
                //!Set error - RBC draining pressure
            }
            //!Set expected state to first draining completed
            usnExpectedState = eDRAINING_AC_COMPLETED;
            break;

        case eDRAINING_AC_COMPLETED:
            //!start Monitoring the pressure
            //m_bPressureMonitor = TRUE;
            //!Check for the ending pressure
            //!If the pressure for draining is not with in the range
            //!Indicate Counting pressure error
            if(m_usnPressureAdc >= DRAINING_AC_COMPLETED_PRESSURE)
            {
                //!Clear error - Draining completed pressure
            }
            else
            {
                //!Set error - Draining completed pressure
            }
            //!Set expected state to backflush started
            usnExpectedState = eBACKFLUSH_STARTED;
            break;

        case eBACKFLUSH_STARTED:
            //!Start Monitoring the pressure
            //m_bPressureMonitor = TRUE;
            //!Check for the ending pressure
            //!If the pressure for backflush is not with in the range
            //!Indicate Counting pressure error
            if(m_usnPressureAdc >= BACKFLUSH_STARTED_PRESSURE)
            {
                //!Clear error - Backflush started pressure
            }
            else
            {
                //!Set error - Backflush started pressure
            }
            //!Set expected state to backflush completed
            usnExpectedState = eBACKFLUSH_COMPLETED;
            break;

        case eBACKFLUSH_COMPLETED:
            //!Start Monitoring the pressure
            //m_bPressureMonitor = TRUE;
            //!Check for the ending pressure
            //!If the pressure for backflush is not with in the range
            //!Indicate Counting pressure error
            if(m_usnPressureAdc >= BACKFLUSH_COMPLETED_PRESSURE)
            {
                //!Clear error - Backflush completed pressure
            }
            else
            {
                //!Set error - Backflush completed pressure
            }

            //!Check whether the state time interval is within the range
            if((g_unSequenceStateTime >= BACKFLUSH_COMPLETED_TIME_LOW) && \
                    (g_unSequenceStateTime <= BACKFLUSH_COMPLETED_TIME_HIGH))
            {
                //Do nothing
                //!Send the sequence status to Sitara
                //usnExpectedState = eDRAINING_FINAL_STARTED;
            }
            else
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            //!Form seqeunce state packet and send to Sitara
            CIFormSequenceStatusPacket(usnSequenceState);
            //!Set expected state to final draining started
            usnExpectedState = eDRAINING_FINAL_STARTED;
            break;

        case eDRAINING_FINAL_STARTED:
            //!Start Monitoring the pressure
           // m_bPressureMonitor = TRUE;
            //!Check for the ending pressure
            //!If the pressure for WBC is not with in the range
            //!Indicate Counting pressure error
            if(m_usnPressureAdc >= DRAINING_FINAL_STARTED_PRESSURE)
            {
                //!Clear error - WBC Draining started
            }
            else
            {
                //!Set error - WBC Draining started
            }
            //!Set expected state to intermediate state of final draining
            usnExpectedState = eDRAINING_FINAL_INTER_STARTED;
            break;

        case eDRAINING_FINAL_INTER_STARTED:
            //!Start Monitoring the pressure
            //m_bPressureMonitor = TRUE;
            //!Check for the ending pressure
            //!If the pressure for WBC is not with in the range
            //!Indicate Counting pressure error
            if(m_usnPressureAdc >= DRAINING_FINAL_INTER_STARTED_PRESSURE)
            {
                //!Clear error - RBC Draining started
            }
            else
            {
                //!Set error - RBC Draining started
            }
            //!Set expected state to final draining completed
            usnExpectedState = eDRAINING_FINAL_COMPLETED;
            break;

        case eDRAINING_FINAL_COMPLETED:
            //!Start Monitoring the pressure
            //m_bPressureMonitor = TRUE;
            //!Check for the ending pressure
            //!If the pressure for WBC is not with in the range
            //!Indicate Counting pressure error
            if(m_usnPressureAdc >= DRAINING_FINAL_COMPLETED_PRESSURE)
            {
                //!Clear error - RBC Draining started
            }
            else
            {
                //!Set error - RBC Draining started
            }
            //!Set expected state to Bath filling completed
            usnExpectedState = eBATH_FILLING_COMPLETED;
            break;

        case eBATH_FILLING_COMPLETED:
            //!Stop Monitoring the pressure
            //m_bPressureMonitor = FALSE;
            //!Check whether the state time interval is within the range
            if((g_unSequenceStateTime >= BATH_FILLING_COMPLETED_TIME_LOW) && \
                    (g_unSequenceStateTime <= BATH_FILLING_COMPLETED_TIME_HIGH))
            {
                //!Do nothing
                //!Send the sequence status to Sitara
                //usnExpectedState = eSTATE_INVALID;
            }
            else
            {
                //!Abort the sequence and don't send any command to Sitara
                //!Sitara shall abort the Sequence on the UI end based on the
                //!time out and Send sequence aborted information to Delfino
                SysInitAbortProcess(FALSE);
            }
            //!Form seqeunce state packet and send to Sitara
            CIFormSequenceStatusPacket(usnSequenceState);
            //!Set expected state to invalid after completion of the sequence
            usnExpectedState = eSTATE_INVALID;

            //!Set X-Axis position to invalid position
           // m_eXPosition = eXPositionInvalid;
            break;

        default:
            break;
    }

}
/******************************************************************************/
/*!
 * \fn void SysMonitorReagentPriming()
 * \brief   Function to check whether Diluent, Lyse and Rinse lines are primed
 * \return  void
 */
/******************************************************************************/
void SysMonitorReagentPriming(void)
{
    //!Monitor the liquid flow for Diluent, Lyse and Rinse
    //!If any of the liquid is empty for specified duration then post the
    //!error to Sitara
    //!On bootup, the liquid is to be monitoed for Diluent, Lyse and Rinse
    //!Liquid to be monitored only in any of the sequence where liquid is
    //!dependent. Dont monitor the liquid during Motor and Valve test

    SysCheckDiluentPriming();
    SysCheckLysePriming();
    SysCheckRinsePriming();
    if((TRUE == utErrorInfoCheck.stErrorInfo.ulnLyseEmpty) ||\
            (TRUE == utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty) ||\
            (TRUE == utErrorInfoCheck.stErrorInfo.ulnRinseEmpty))
    {
        switch(g_usnCurrentMode)
        {
            case WHOLE_BLOOD_MODE:
            case PRE_DILUENT_MODE:
            case DISPENSE_THE_DILUENT:
            case START_PRE_DILUENT_COUNTING_CMD:
            case BODY_FLUID_MODE:
            case PARTIAL_BLOOD_COUNT_WBC:
            case PARTIAL_BLOOD_COUNT_RBC_PLT:
                SysInitAbortProcess(TRUE);
                //CIFormPatientHandlingPacket(DELFINO_ERROR_MINOR_CMD);
                CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);
                break;

            case AUTO_CALIBRATION_WB:
            case COMMERCIAL_CALIBRATION_WBC:
            case COMMERCIAL_CALIBRATION_RBC:
            case COMMERCIAL_CALIBRATION_PLT:
                SysInitAbortProcess(TRUE);
                //CIFormCalibrationHandlingPacket(DELFINO_ERROR_MINOR_CMD);
                CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);
                 break;

            case QUALITY_CONTROL_WB:
            case QUALITY_CONTROL_BODY_FLUID:
                SysInitAbortProcess(TRUE);
                //CIFormQualityHandlingPacket(DELFINO_ERROR_MINOR_CMD);
                CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);
                break;

            case PRIME_WITH_EZ_CLEANSER_CMD:
            case BACK_FLUSH_SERVICE_HANDLING:
            case ZAP_APERTURE_SERVICE_HANDLING:
            case CLEAN_BATH_SERVICE_HANDLING:
            case HEAD_RINSING_SERVICE_HANDLING:
            case PROBE_CLEANING_SERVICE_HANDLING:
            case MBD_WAKEUP_START:
            case COUNTING_TIME_SEQUENCE:
            case FLOW_CALIBRATION:
            case RINSE_PRIME_FLOW_CALIBRATION:
            case MBD_TO_SHIP_SEQUENCE_START:
            case MBD_VOLUMETRIC_BOARD_CHECK:
                //!Set the error code before aborting so that the error code
                //! shall be set before sending the error command based  on the
                //! major mode running
                //Set the major command as start up only for this condition
                SysInitAbortProcess(TRUE);
                /*if(SITARA_STARTUP_CMD == g_usnCurrentModeMajor)
                {
                    //CIFormStartupHandlingPacket(DELFINO_ERROR_MINOR_CMD, FALSE);
                }
                else
                {
                    CIFormServiceHandlingPacket(DELFINO_ERROR_MINOR_CMD, FALSE);
                }*/
                CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);

                break;

            case SHUTDOWN_SEQUENCE:
                SysInitAbortProcess(TRUE);
                //CIFormShutDownSequencePacket(DELFINO_ERROR_MINOR_CMD);
                CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);
                break;

            case ZERO:
                sprintf(DebugPrintBuf, "\r\n Idle Abort \n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
                SysInitAbortProcess(TRUE);
                //CIFormServiceHandlingPacket(DELFINO_ERROR_MINOR_CMD, false);
                CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);
                if(TRUE == utErrorInfoCheck.stErrorInfo.ulnLyseEmpty)
                {
                    utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;
                }

                if(TRUE == utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty)
                {
                    utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;
                }
                if(TRUE == utErrorInfoCheck.stErrorInfo.ulnRinseEmpty)
                {
                    utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;
                }
                break;

            case PRIME_ALL_SERVICE_HANDLING:
            case PRIME_WITH_DILUENT_SERVICE_HANDLING:
            case PRIME_WITH_LYSE_SERVICE_HANDLING:
            case PRIME_WITH_RINSE_SERVICE_HANDLING:
            case DRAIN_ALL_SERVICE_HANDLING:
            case SITARA_ABORT_MAIN_PROCESS_CMD:
            case DRAIN_BATH_SERVICE_HANDLING:
            case X_AXIS_MOTOR_CHECK:
            case Y_AXIS_MOTOR_CHECK:
            case DILUENT_SYRINGE_MOTOR_CHECK:
            case WASTE_SYRINGE_MOTOR_CHECK:
            case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:
            case MBD_SLEEP_PROCESS:
            case MBD_ABORT_PROCESS_COMPLETED:
            case MBD_USER_ABORT_PROCESS:
            case MBD_USER_ABORT_Y_AXIS_PROCESS:
                //!Do not monitor the reagent priming
                break;

            default:
                break;
        }
    }
}
/******************************************************************************/
/*!
 * \fn void SysCheckDiluentPriming()
 * \brief   Function to check whether Diluent line is primed
 * \return  void
 */
/******************************************************************************/
void SysCheckDiluentPriming(void)
{
    if(TRUE == g_bMonDiluentPriming)
    {
        //!Read the GPIO to Monitor the Diluent Line
        if(TRUE == GPIO_ReadPin(DILUENT_PRIME_CHECK))
        {
            //!Increment the Counter to continuously monitor the Diluent line
            //!is empty
            m_snMonDiluentCount++;

        }
        else
        {
            //!Reset the Counter to if the Diluent line is detected with liquid
            m_snMonDiluentCount = ZERO;
        }

        //!If the reagent monitoring time is croosed then set the error
        if(REAGENT_PRIME_MON_TIME <= m_snMonDiluentCount)
        {
            sprintf(DebugPrintBuf, "\r\n ulnDiluentEmpty\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Set the Diluent prime error
            utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = TRUE;
            //!Reset the counter to Zero and send the status to Sitara
            m_snMonDiluentCount = ZERO;

            //!Abort Sequence by Sending the error information to the Sitara
            //SysInitAbortProcess(TRUE);

            //!Don't monitor the line
            g_bMonDiluentPriming = FALSE;
        }
    }
}
/******************************************************************************/
/*!
 * \fn void SysCheckLysePriming()
 * \brief   Function to check whether Lyse line is primed
 * \return  void
 */
/******************************************************************************/
void SysCheckLysePriming(void)
{
    if(TRUE == g_bMonLysePriming)
    {
        //!Read the GPIO to Monitor the Lyse Line
        if(TRUE == GPIO_ReadPin(LYSE_PRIME_CHECK))
        {
            //!Increment the Counter to continuously monitor the Lyse line is
            //!empty
            m_snMonLyseCount++;
        }
        else
        {
            //!Reset the Counter to if the Lyse line is detected with liquid
            m_snMonLyseCount = ZERO;
        }
        if(REAGENT_PRIME_MON_TIME <= m_snMonLyseCount)
        {
            sprintf(DebugPrintBuf, "\r\n ulnLyseEmpty\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Set the Lyse prime error
            utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = TRUE;
            //!Reset the counter to Zero and send the status to Sitara
            m_snMonLyseCount = ZERO;

            g_bMonLysePriming = FALSE;

            //!Abort Sequence by Sending the error information to the Sitara
            //SysInitAbortProcess(TRUE);
        }
    }
}
/******************************************************************************/
/*!
 * \fn void SysCheckRinsePriming()
 * \brief   Function to check whether Rinse line is primed
 * \return  void
 */
/******************************************************************************/
void SysCheckRinsePriming(void)
{

    if(TRUE == g_bMonRinsePriming)
    {
        //!Read the GPIO to Monitor the Rinse Line
        if(TRUE == GPIO_ReadPin(RINSE_PRIME_CHECK))
        {
            //!Increment the Counter to continuously monitor the Rinse line is
            //!empty
            m_snMonRinseCount++;
        }
        else
        {
            //!Reset the Counter to if the Rinse line is detected with liquid
            m_snMonRinseCount = ZERO;
        }
        if(REAGENT_PRIME_MON_TIME <= m_snMonRinseCount)
        {
            sprintf(DebugPrintBuf, "\r\n ulnRinseEmpty\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Set the Rinse prime error
            utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = TRUE;
            //!Reset the counter to Zero and send the status to Sitara
            m_snMonRinseCount = ZERO;

            g_bMonRinsePriming = FALSE;

            //!Abort Sequence by Sending the error information to the Sitara
            //SysInitAbortProcess(TRUE);
        }
    }
}
//PH_CHANGE_07_MAR_17 - START
/******************************************************************************/
/*!
 * \fn void SysMotorHomePositionCheck(void)
 * \brief   Called to check whether the motors are at home position with respect
 *           to the current displacement and postion of the motor calulated
 *           If it is not as expected set the error code and indicate UI with
 *           respect to the error
 * \return  void
 */
/******************************************************************************/
void SysMotorHomePositionCheck(void)
{
#if X_AXIS_ERROR_CHECK
    //!Error check for X Motor Assembly
    SysXMotorErrorCheck();
#endif


#if DILUENT_ERROR_CHECK
    //!Error check for Diluent motor Assembly
    SysDiluentMotorErrorCheck();
#endif

#if SAMPLE_ERROR_CHECK
    //!Error check for Sample motor Assembly
    SysSampleMotorErrorCheck();
#endif

#if WASTE_ERROR_CHECK
    //!Error check for Waste motor Assembly
    SysWasteMotorErrorCheck();
#endif

#if Y_AXIS_ERROR_CHECK
    //!Error check for Y motor Assembly
    SysYMotorErrorCheck();
#endif
}

/******************************************************************************/
/*!
* \fn void ControllerStatus(void)
* \brief Function is to toggle the GPIO every 3 seconds to indicate processor
*       that controller main loop is running
* \return void
*/
/******************************************************************************/
void SysControllerStatusMon(void)
{
    static Uint16 stat_usnControllerMonCount = ZERO;
    //!Toggle GPIO high and low with a frequent cof 3 sec and 50%duty cycle
    if(stat_usnControllerMonCount == CONTROLLER_MON_TIME_HIGH)
    {
        //Set the GPIO High
        GPIO_WritePin(SPI_COMM_CTRL_SITARA_BUSY_STATUS, ONE);
    }
    if(stat_usnControllerMonCount == CONTROLLER_MON_TIME_LOW)
    {
        //Set the GPIO Low
        GPIO_WritePin(SPI_COMM_CTRL_SITARA_BUSY_STATUS, ZERO);
    }
    stat_usnControllerMonCount++;
    if(stat_usnControllerMonCount > CONTROLLER_MON_TIME_LOW)
    {
        stat_usnControllerMonCount = 0;
    }
}
/******************************************************************************/
/*!
* \fn void SysHomePositionInit(void)
* \brief Function to initialize the current position of motors before feedback
* monitoring of motors
* \return void
*/
/******************************************************************************/
void SysHomePositionInit(void)
{
    //!Dilunet syringe home position verification and initializing the position to Zero
    if(true == OIHomeSensor2State())
    {
        pMotor2Pwm6->iMotorPosition = ZERO;
    }
    else
    {
        //!Enable to monitor the Diluent syringe motor movement
        m_bDiluentSyringeMonEnable = TRUE;
    }

    //!Sample syringe home position verification and initializing the position to Zero
    if(true == OIHomeSensor3State())
    {
        pMotor3Pwm2->iMotorPosition = ZERO;
    }
    else
    {
        //!Enable to monitor the Sample syringe motor movement
        m_bSampleSyringeMonEnable = TRUE;
    }

    //!Waste syringe home position verification and initializing the position to Zero
    if(true == OIHomeSensor4State())
    {
        pMotor4Pwm5->iMotorPosition = ZERO;
    }
    else
    {
        //!Enable to monitor the Waste syringe motor movement
        m_bWasteSyringeMonEnable = TRUE;
    }

    //!Y-Axis home position verification and initializing the position to Zero
    if(true == OIHomeSensor5State())
    {
        pMotor5Pwm1->iMotorPosition = ZERO;
    }
    else
    {
        //!Enable to monitor the Waste syringe motor movement
        m_bYAxisMonEnable = TRUE;
    }

    switch(g_usnCurrentMode)
    {
        case WHOLE_BLOOD_MODE:
        //!Do not check for START_PRE_DILUENT_COUNTING_CMD during cycle completion
        //!Cycle completion is mainly for the main mode (pre-dilute mode) and
        //!not the intermediate mode of starting the pre-dilute sequence after
        //!diluting the blood externally in pre-dilute mode
        //case START_PRE_DILUENT_COUNTING_CMD:
        case PRE_DILUENT_MODE:
        case BODY_FLUID_MODE:
        case START_UP_SEQUENCE_MODE:
        case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
        case MBD_STARTUP_FAILED_CMD:
        case AUTO_CALIBRATION_WB:
        case COMMERCIAL_CALIBRATION_WBC:
        case COMMERCIAL_CALIBRATION_RBC:
        case COMMERCIAL_CALIBRATION_PLT:
        case QUALITY_CONTROL_WB:
        case QUALITY_CONTROL_BODY_FLUID:
        case PARTIAL_BLOOD_COUNT_WBC:
        case PARTIAL_BLOOD_COUNT_RBC_PLT:
        case COUNTING_TIME_SEQUENCE:
        case PRIME_WITH_EZ_CLEANSER_CMD:
        case SHUTDOWN_SEQUENCE:
        case FLOW_CALIBRATION:
            //!Enable to monitor the X-Axis motor movement
            m_bXAxisMonEnable = TRUE;
            //!Enable to monitor the Diluent syringe motor movement
            m_bDiluentSyringeMonEnable = TRUE;
            //!Enable to monitor the Sample syringe motor movement
            m_bSampleSyringeMonEnable = TRUE;
            //!Enable to monitor the Waste syringe motor movement
            m_bWasteSyringeMonEnable = TRUE;
            //!Enable to monitor the Y-Axis motor movement
            m_bYAxisMonEnable = TRUE;
            break;

        case MBD_WAKEUP_START:
            break;

        case PRIME_WITH_RINSE_SERVICE_HANDLING:
        case BACK_FLUSH_SERVICE_HANDLING:
        case ZAP_APERTURE_SERVICE_HANDLING:
        case DRAIN_BATH_SERVICE_HANDLING:
        case CLEAN_BATH_SERVICE_HANDLING:
            //!Enable to monitor the Diluent syringe motor movement
            m_bDiluentSyringeMonEnable = TRUE;
            //!Enable to monitor the Waste syringe motor movement
            m_bWasteSyringeMonEnable = TRUE;
            break;

        case PRIME_WITH_DILUENT_SERVICE_HANDLING:
            //!Enable to monitor the Diluent syringe motor movement
            m_bDiluentSyringeMonEnable = TRUE;
            //!Enable to monitor the Waste syringe motor movement
            m_bWasteSyringeMonEnable = TRUE;
            //!Enable to monitor the Y-Axis motor movement
            m_bYAxisMonEnable = TRUE;
            break;


        case PRIME_WITH_LYSE_SERVICE_HANDLING:
            //!Enable to monitor the Diluent syringe motor movement
            m_bDiluentSyringeMonEnable = TRUE;
            //!Enable to monitor the Sample syringe motor movement
            m_bSampleSyringeMonEnable = TRUE;
            //!Enable to monitor the Waste syringe motor movement
            m_bWasteSyringeMonEnable = TRUE;
            break;

        case PRIME_ALL_SERVICE_HANDLING:
        case DRAIN_ALL_SERVICE_HANDLING:
            //!Enable to monitor the Diluent syringe motor movement
            m_bDiluentSyringeMonEnable = TRUE;
            //!Enable to monitor the Sample syringe motor movement
            m_bSampleSyringeMonEnable = TRUE;
            //!Enable to monitor the Waste syringe motor movement
            m_bWasteSyringeMonEnable = TRUE;
            //!Enable to monitor the Y-Axis motor movement
            m_bYAxisMonEnable = TRUE;
            break;

        case HEAD_RINSING_SERVICE_HANDLING:
        case PROBE_CLEANING_SERVICE_HANDLING:
            break;

        default:
            break;
    }

}
/******************************************************************************/
/*!
* \fn void SysDisableMotorMon(void)
* \brief Function to initialize the current position of motors before feedback
* monitoring of motors
* \return void
*/
/******************************************************************************/
void SysDisableMotorMon(void)
{
    //!Disable the motor monitoring of all the motors.
    m_bXAxisMonEnable = FALSE;
    m_bDiluentSyringeMonEnable = FALSE;
    m_bSampleSyringeMonEnable = FALSE;
    m_bWasteSyringeMonEnable = FALSE;
    m_bYAxisMonEnable = FALSE;
    m_eXPosition = eXPositionInvalid;
}
/******************************************************************************/
/*!
* \fn void SysXMotorErrorCheck(void)
* \brief Function to check error in X-Axis Motor and its positions
* \return void
*/
/******************************************************************************/
void SysXMotorErrorCheck(void)
{
    //!If X-Axis motor check is enabled, perform error check for X-Axis
    if(TRUE == m_bXAxisMonEnable)
    {
        //! Motor Fault condition check
        if(FALSE == SMDGetMotorFault(MOTOR4_SELECT))
        {
            //!Set the error Code for X-Axis
            utErrorInfoCheck.stErrorInfo.ulnX_MotorFault = TRUE;
            sprintf(DebugPrintBuf, "\nX Motor_Fault \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Arresting the sequence when error occur
            SysInitArrestSequence(TRUE);
            //!Send Error Message to Processor
            SysErrorCommand(g_usnCurrentMode);
        }

        static bool stat_bXHomeCheck  = FALSE;
        //!If home position check
         //static bool_t stat_bSetXPosition = FALSE;

        //!If the MBD is checking for Home position of X-Axis, then set the X-Axis
        //! position to invalid else set X-Axis position to MBD checking position
        if(TRUE == BloodCellCounter_Y.HPMotorCheck[X_AXIS_MOTOR])
        {
            stat_bXHomeCheck = TRUE;
            BloodCellCounter_Y.HPMotorCheck[X_AXIS_MOTOR] = FALSE;
        }

        //!If the X-Motor displacement is completed
        if(pMotor4Pwm5->Status == COMPLETED)
        {
            //!If it is home check
            if(TRUE == stat_bXHomeCheck)
            {

                //!If the home position is not detected,set the X-Position error
                if(TRUE == OIHomeSensor4State())
                {
                    //!Set the error Code for X-Axis
                    utErrorInfoCheck.stErrorInfo.ulnX_Motor = FALSE;
                    //!Reset the homecheck
                    stat_bXHomeCheck = FALSE;
                }
                else
                {
                    //!Set the error Code for X-Axis
                    utErrorInfoCheck.stErrorInfo.ulnX_Motor = TRUE;
                    sprintf(DebugPrintBuf, "\n X Motor error 1\n");
                    UartDebugPrint((int *)DebugPrintBuf, 50);
                    sprintf(DebugPrintBuf, "X Set 1 :%d %d \n", m_eXPosition, OIHomeSensor4State());
                    UartDebugPrint((int *)DebugPrintBuf, 50);

                    //!Arresting the sequence when error occur
                    SysInitArrestSequence(TRUE);

                    //!Send Error Message to Processor
                    SysErrorCommand(g_usnCurrentMode);

                }
                stat_bXHomeCheck = FALSE;

            }
            //!Set the motor to idle state after completion
            pMotor4Pwm5->Direction = IDLE;

            m_bXAxisMonEnable = FALSE;
            //!Check the current position of the X-Motor
            switch(m_eXPosition)
            {
                    case eASPIRATION:
                    //!In aspiration position if home position is detected,
                    //! then clear the error. If not then abort the sequence
                    //! and set the error
                    if(TRUE == OIHomeSensor4State())
                    {
                        utErrorInfoCheck.stErrorInfo.ulnX_Motor = FALSE;
                    }
                    else
                    {
                        //!Set the error Code for X-Axis
                        utErrorInfoCheck.stErrorInfo.ulnX_Motor = TRUE;
                        sprintf(DebugPrintBuf, "\n X Motor Error 2\n");
                        UartDebugPrint((int *)DebugPrintBuf, 50);
                        sprintf(DebugPrintBuf, "X Set 2 :%d %d \n", m_eXPosition, OIHomeSensor4State());
                        UartDebugPrint((int *)DebugPrintBuf, 50);

                        //!Arresting the sequence when error occur
                        SysInitArrestSequence(TRUE);

                        //!Send Error Message to Processor
                        SysErrorCommand(g_usnCurrentMode);

                    }
                    break;

                case eRBC_BATH:
                case eWBC_BATH:
                    //!If the X-axis position is aspiration, then home should not be detected.
                    //! If home is detected, then set the error and abor tthe seqeunce.
                    if(FALSE == OIHomeSensor4State())
                    {
                        utErrorInfoCheck.stErrorInfo.ulnX_Motor = FALSE;

                    }
                    else
                    {
                        //!Set the error Code for X-Axis
                        utErrorInfoCheck.stErrorInfo.ulnX_Motor = TRUE;
                        sprintf(DebugPrintBuf, "\n X Motor Error 3\n");
                        UartDebugPrint((int *)DebugPrintBuf, 50);
                        sprintf(DebugPrintBuf, "X Set 3 :%d %d \n", m_eXPosition, OIHomeSensor4State());
                        UartDebugPrint((int *)DebugPrintBuf, 50);

                        //!Arresting the sequence when error occur
                        SysInitArrestSequence(TRUE);

                        //!Send Error Message to Processor
                        SysErrorCommand(g_usnCurrentMode);
                    }
                    break;

                default:
                    break;
            }
        }
    }
    else
    {
        //!If the motor status is completed, then set it to idle state
        if(pMotor4Pwm5->Status == COMPLETED)
        {
            pMotor4Pwm5->Direction = IDLE;
            m_bXAxisMonEnable = FALSE;
        }
    }
}
/******************************************************************************/
/*!
* \fn void SysDiluentMotorErrorCheck(void)
* \brief Function to check error in Diluent Motor assembly
* \return void
*/
/******************************************************************************/
void SysDiluentMotorErrorCheck(void)
{
    //!If Diluent motor check is enabled, perform error check for Diluent
    if(TRUE == m_bDiluentSyringeMonEnable)
    {
        static bool stat_bDiluentHomeCheck  = FALSE;

        //! Motor Fault condition check
        if(FALSE == SMDGetMotorFault(MOTOR2_SELECT))
        {
            //!Set the error Code for X-Axis
            utErrorInfoCheck.stErrorInfo.ulnDiluent_MotorFault = TRUE;
            sprintf(DebugPrintBuf, "\nDil Motor_Fault \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Arresting the sequence when error occur
            SysInitArrestSequence(TRUE);

            //!Send Error Message to Processor
            SysErrorCommand(g_usnCurrentMode);
        }



        //!If home position check
        if(true == BloodCellCounter_Y.HPMotorCheck[DILUENT_MOTOR])
        {
            //stat_bDiluentHomeCheck = TRUE;
            //sprintf(DebugPrintBuf, "\n Diluent Home Set\n");
            //UartDebugPrint((int *)DebugPrintBuf, 50);
        }
        //!If the Diluent syringe displacement is completed
        if(pMotor2Pwm6->Status == COMPLETED)
        {
            if(stat_bDiluentHomeCheck)
            {
                if(true == OIHomeSensor2State())
                {

                    pMotor2Pwm6->iMotorPosition = ZERO;
                    //!CLear the error Code for Diluent Syringe
                    utErrorInfoCheck.stErrorInfo.ulnDiluentSyringe = FALSE;
                    stat_bDiluentHomeCheck = FALSE;

                }
                else
                {
                    //!Set the error Code for Diluent Syringe
                    utErrorInfoCheck.stErrorInfo.ulnDiluentSyringe = TRUE;

                   sprintf(DebugPrintBuf, "D :%ld %d \n", pMotor2Pwm6->iMotorPosition, OIHomeSensor2State());
                    UartDebugPrint((int *)DebugPrintBuf, 50);
                   sprintf(DebugPrintBuf, "\n Diluent Motor error 1 \n\r");
                   UartDebugPrint((int *)DebugPrintBuf, 50);


                    //!Arresting the sequence when error occur
                    SysInitArrestSequence(TRUE);

                    //!Send Error Message to Processor
                    SysErrorCommand(g_usnCurrentMode);
                }
            }

            //!Calculate the distance of the Diluent based on the Direction movement
            //!Check for the home position check condition
            if(MOTOR2_HOME_DIRECTION == pMotor2Pwm6->Direction)
            {
              //!If the motor is moving in up direction
              //!Subtract the distance to the current motor position
              pMotor2Pwm6->iMotorPosition = pMotor2Pwm6->iMotorPosition - pMotor2Pwm6->FBTimerCounter;

              if(pMotor2Pwm6->iMotorPosition < ZERO)
              {
                  pMotor2Pwm6->iMotorPosition = ZERO;
              }

            }
            else if((1) == pMotor2Pwm6->Direction)
            {
              //!If the motor is moving in down direction
              //!Add the distance to the current motor position
              pMotor2Pwm6->iMotorPosition = pMotor2Pwm6->iMotorPosition + pMotor2Pwm6->TimerCounter;

            }

            //!Set the motor to idle state
            pMotor2Pwm6->Direction = IDLE;

            m_bDiluentSyringeMonEnable = FALSE;

            //!Check for the following conditions
            //!If the current position is zero and Home is detected
            //!If the current position is not zero and home is not detected
            if(((320 >= pMotor2Pwm6->iMotorPosition) && (true == OIHomeSensor2State()))||
            ((320 < pMotor2Pwm6->iMotorPosition) && (false == OIHomeSensor2State())))
            {
                //!Clear the error code for Diluent Syringe
                utErrorInfoCheck.stErrorInfo.ulnDiluentSyringe = FALSE;

                if(true == OIHomeSensor2State())
                {
                    pMotor2Pwm6->iMotorPosition = 0;

                }
            }
            else
            {
                //!Set the error Code for Diluent Syringe
                utErrorInfoCheck.stErrorInfo.ulnDiluentSyringe = TRUE;

                sprintf(DebugPrintBuf, "\n Diluent Motor error 2\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
                sprintf(DebugPrintBuf, "\rD Set :%ld %d \n", pMotor2Pwm6->iMotorPosition, OIHomeSensor2State());
                UartDebugPrint((int *)DebugPrintBuf, 50);

                //!Arresting the sequence when error occur
                SysInitArrestSequence(TRUE);
                //!Send Error Message to Processor
                SysErrorCommand(g_usnCurrentMode);
            }
        }
    }
    else
    {
        //!If the motor status is completed, then set it to idle state
        if(pMotor2Pwm6->Status == COMPLETED)
        {
            pMotor2Pwm6->Direction = IDLE;
            m_bDiluentSyringeMonEnable = FALSE;
        }
    }
}
/******************************************************************************/
/*!
* \fn void SysSampleMotorErrorCheck(void)
* \brief Function to check error in Sample Motor assembly
* \return void
*/
/******************************************************************************/
void SysSampleMotorErrorCheck(void)
{
    //!If Sample motor check is enabled, perform error check for Sample syringe
    if(TRUE == m_bSampleSyringeMonEnable)
    {
        //! Motor Fault condition check
        if(FALSE == SMDGetMotorFault(MOTOR3_SELECT))
        {
            //!Set the error Code for X-Axis
            utErrorInfoCheck.stErrorInfo.ulnSample_MotorFault = TRUE;
            sprintf(DebugPrintBuf, "\nSample Motor_Fault \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Arresting the sequence when error occur
            SysInitArrestSequence(TRUE);

            //!Send Error Message to Processor
            SysErrorCommand(g_usnCurrentMode);
        }

        static bool_t stat_bSampleHomeCheck = FALSE;
        if(true == BloodCellCounter_Y.HPMotorCheck[SAMPLE_MOTOR])
        {
            stat_bSampleHomeCheck = TRUE;
        }
        //!If the Sample Syringe displacement is completed
        if(pMotor3Pwm2->Status == COMPLETED)
        {
            //!If home position check
            if(TRUE == stat_bSampleHomeCheck)
            {
                if(true == OIHomeSensor3State())
                {
                    pMotor3Pwm2->iMotorPosition = ZERO;
                    //!Clear the error Code for Sample Syringe
                    utErrorInfoCheck.stErrorInfo.ulnSampleSyringe = FALSE;
                    stat_bSampleHomeCheck = FALSE;
                }
                else
                {
                    //!Set the error Code for Sample Syringe
                    utErrorInfoCheck.stErrorInfo.ulnSampleSyringe = TRUE;
                    sprintf(DebugPrintBuf, "S Set : %ld %d\n\r", pMotor3Pwm2->iMotorPosition, OIHomeSensor3State());
                    UartDebugPrint((int *)DebugPrintBuf, 50);
                    sprintf(DebugPrintBuf, "\n Sample Motor error 1\n");
                    UartDebugPrint((int *)DebugPrintBuf, 50);

                    //!Arresting the sequence when error occur
                    SysInitArrestSequence(TRUE);
                    //!Send Error Message to Processor
                   SysErrorCommand(g_usnCurrentMode);
                }
            }

            //!Calculate the distance of the Sample syringe based on the Direction movement
            //!Check for the home position check condition
            if(MOTOR3_HOME_DIRECTION == pMotor3Pwm2->Direction)
            {
                //!If the motor is moving in up direction
                //!Subtract the distance to the current motor position
                pMotor3Pwm2->iMotorPosition = pMotor3Pwm2->iMotorPosition - pMotor3Pwm2->FBTimerCounter;
                if(pMotor3Pwm2->iMotorPosition < ZERO)
                {
                    pMotor3Pwm2->iMotorPosition = ZERO;
                }
            }
            else if((1) == pMotor3Pwm2->Direction)
            {
                //!If the motor is moving in down direction
                //!Add the distance to the current motor position
                pMotor3Pwm2->iMotorPosition = pMotor3Pwm2->iMotorPosition + pMotor3Pwm2->TimerCounter;
            }

            //!Set the motor to idle state
            pMotor3Pwm2->Direction = IDLE;

            m_bSampleSyringeMonEnable = FALSE;

            //!Check for the following conditions
            //!If the current position is zero and Home is detected
            //!If the current position is not zero and home is not detected
            if(((270 >= pMotor3Pwm2->iMotorPosition) && (true == OIHomeSensor3State()))||
            ((270 < pMotor3Pwm2->iMotorPosition) && (false == OIHomeSensor3State())))
            {
                //!Clear the error code for Sample Syringe
                utErrorInfoCheck.stErrorInfo.ulnSampleSyringe = FALSE;
                if(true == OIHomeSensor3State())
                {
                    pMotor3Pwm2->iMotorPosition = 0;
                }
            }
            else
            {
                //!Set the error Code for Sample Syringe
                utErrorInfoCheck.stErrorInfo.ulnSampleSyringe = TRUE;
                sprintf(DebugPrintBuf, "\n Sample Motor error 2\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
                sprintf(DebugPrintBuf, "S Set: %ld %d\n", pMotor3Pwm2->iMotorPosition, OIHomeSensor3State());
                UartDebugPrint((int *)DebugPrintBuf, 50);

                //!Arresting the sequence when error occur
                SysInitArrestSequence(TRUE);

                //!Send Error Message to Processor
                SysErrorCommand(g_usnCurrentMode);
            }
        }
    }
    else
    {
        //!If the motor status is completed, then set it to idle state
        if(pMotor3Pwm2->Status == COMPLETED)
        {
            pMotor3Pwm2->Direction = IDLE;
            m_bSampleSyringeMonEnable = FALSE;
        }
    }
}
/******************************************************************************/
/*!
* \fn void SysWasteMotorErrorCheck(void)
* \brief Function to check error in Waste Motor assembly
* \return void
*/
/******************************************************************************/
void SysWasteMotorErrorCheck(void)
{
    //! Motor Fault condition check
    if(FALSE == SMDGetMotorFault(MOTOR4_SELECT))
    {
        //!Set the error Code for Waste Motor
        utErrorInfoCheck.stErrorInfo.ulnWaste_MotorFault = TRUE;
        sprintf(DebugPrintBuf, "\n Waste Motor_Fault \n");
        UartDebugPrint((int *)DebugPrintBuf, 50);

        //!Arresting the sequence when error occur
         SysInitArrestSequence(TRUE);
        //!Send Error Message to Processor
        SysErrorCommand(g_usnCurrentMode);
    }
    //m_bWasteSyringeMonEnable = FALSE; //For Testing
    //!If Waste motor check is enabled, perform error check for waste syringe
    if(TRUE == m_bWasteSyringeMonEnable)
    {
        //! Motor Fault condition check
        if(FALSE == SMDGetMotorFault(WASTE_MOTOR))
        {
            //!Set the error Code for Waste Motor
            utErrorInfoCheck.stErrorInfo.ulnWasteSyringe = TRUE;
            sprintf(DebugPrintBuf, "\n Waste Motor_Fault \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Send Error Message to Processor
            SysErrorCommand(g_usnCurrentMode);
        }
        static bool_t stat_bWasteHomeCheck = FALSE;
        if(true == BloodCellCounter_Y.HPMotorCheck[WASTE_MOTOR])
        {
            //Set the variable
            stat_bWasteHomeCheck = TRUE;
        }
        //!If the Waste Syringe displacement is completed
        if(pMotor1Pwm8->Status == COMPLETED)
        {
            //!If home position check
            if(stat_bWasteHomeCheck)
            {
                if(true == OIHomeSensor1State())
                {
                    pMotor1Pwm8->iMotorPosition = ZERO;
                    //!Clear the error Code for Waste Syringe
                    utErrorInfoCheck.stErrorInfo.ulnWasteSyringe = FALSE;
                    stat_bWasteHomeCheck = FALSE;

                }
                else
                {
                    //!Set the error Code for Waste Syringe
                    utErrorInfoCheck.stErrorInfo.ulnWasteSyringe = TRUE;

                    sprintf(DebugPrintBuf, "\rWaste Motor Error 1\n\r");
                    UartDebugPrint((int *)DebugPrintBuf, 50);
                    sprintf(DebugPrintBuf, "\rW Set : %ld %ld %d\n\r", pMotor1Pwm8->TimerCounter, pMotor1Pwm8->iMotorPosition, OIHomeSensor1State());
                    UartDebugPrint((int *)DebugPrintBuf, 50);

                    //!Arresting the sequence when error occur
                    SysInitArrestSequence(TRUE);

                    //!Send Error Message to Processor
                    SysErrorCommand(g_usnCurrentMode);
                }
            }

            //!Calculate the distance of the Waste based on the Direction movement
            //!Check for the home position check condition
            if(MOTOR4_HOME_DIRECTION == pMotor1Pwm8->Direction)
            {
              //!If the motor is moving in up direction
              //!Subtract the distance to the current motor position

              pMotor1Pwm8->iMotorPosition = pMotor1Pwm8->iMotorPosition - pMotor1Pwm8->FBTimerCounter;

              if(pMotor1Pwm8->iMotorPosition < ZERO)
              {
                  pMotor1Pwm8->iMotorPosition = ZERO;
              }
            }
            else if((1) == pMotor1Pwm8->Direction)
            {
              //!If the motor is moving in down direction
              //!Add the distance to the current motor position
              //pMotor1Pwm8->iMotorPosition = pMotor1Pwm8->iMotorPosition + pMotor1Pwm8->TimerCounter;
                pMotor1Pwm8->iMotorPosition = pMotor1Pwm8->iMotorPosition + pMotor1Pwm8->FBTimerCounter;
            }

            //!Set the motor to idle state
            pMotor1Pwm8->Direction = IDLE;

            m_bWasteSyringeMonEnable = FALSE;

            //!Check for the following conditions
            //!If the current position is zero and Home is detected
            //!If the current position is not zero and home is not detected
            //!Initially the threshold was 150, then changed to 200 then changed to 300
            if(((380 >= pMotor1Pwm8->iMotorPosition)&& (true == OIHomeSensor1State()))||
             ((380  < pMotor1Pwm8->iMotorPosition)  && (false == OIHomeSensor1State())))
            {
                //!Clear the error code for Waste Syringe
                utErrorInfoCheck.stErrorInfo.ulnWasteSyringe = FALSE;
                if(true == OIHomeSensor1State())
                {
                    pMotor1Pwm8->iMotorPosition = 0;
                }
            }
            else
            {
                //!Set the error Code for Waste Syringe
                utErrorInfoCheck.stErrorInfo.ulnWasteSyringe = TRUE;
                sprintf(DebugPrintBuf, "\rW Set : %ld %ld %d\n\r", pMotor1Pwm8->TimerCounter, pMotor1Pwm8->iMotorPosition, OIHomeSensor1State());
                UartDebugPrint((int *)DebugPrintBuf, 50);
                sprintf(DebugPrintBuf, "\n Waste Motor Error 2\n\r");
                UartDebugPrint((int *)DebugPrintBuf, 50);

                //!Arresting the sequence when error occur
                SysInitArrestSequence(TRUE);

                //!Send Error Message to Processor
                SysErrorCommand(g_usnCurrentMode);
            }
        }
    }
    else
    {
        //!If the motor status is completed, then set it to idle state
        if(pMotor1Pwm8->Status == COMPLETED)
        {
            pMotor1Pwm8->Direction = IDLE;
            m_bWasteSyringeMonEnable = FALSE;
            Counter = 0;
        }
    }
}
/******************************************************************************/
/*!
* \fn void SysYMotorErrorCheck(void)
* \brief Function to check error in Y Motor assembly
* \return void
*/
/******************************************************************************/
void SysYMotorErrorCheck(void)
{
    //!If Y-Axis motor check is enabled, perform error check for Y-Axis
    if(TRUE == m_bYAxisMonEnable)
    {
        //! Motor Fault condition check
        if(FALSE == SMDGetMotorFault(MOTOR5_SELECT))
        {
            //!Set the error Code for Y motor
            utErrorInfoCheck.stErrorInfo.ulnY_MotorFault = TRUE;
            sprintf(DebugPrintBuf, "\n Y Motor_Fault \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            SysInitArrestSequence(TRUE);

            //!Send Error Message to Processor
            SysErrorCommand(g_usnCurrentMode);
        }
        static bool_t stat_bYHomeCheck = FALSE;
        if(true == BloodCellCounter_Y.HPMotorCheck[Y_AXIS_MOTOR])
        {
            //Set the variable
            stat_bYHomeCheck = TRUE;
        }
        //!If the Y-Axis Motor displacement is completed
        if(pMotor5Pwm1->Status == COMPLETED)
        {
            //!If home position check
            if(stat_bYHomeCheck)
            {
                if(true == OIHomeSensor5State())
                {
                    pMotor5Pwm1->iMotorPosition = ZERO;
                    //!Clear the error Code for Y-Axis
                    utErrorInfoCheck.stErrorInfo.ulnY_Motor = FALSE;
                    stat_bYHomeCheck = FALSE;
                }
                else
                {
                    //!Set the error Code for Y-Axis
                    utErrorInfoCheck.stErrorInfo.ulnY_Motor = TRUE;
                    sprintf(DebugPrintBuf, "Y Set : %ld %d\n\r", pMotor5Pwm1->iMotorPosition, OIHomeSensor5State());
                    UartDebugPrint((int *)DebugPrintBuf, 50);
                    sprintf(DebugPrintBuf, "\n Y Motor error 1\n");
                    UartDebugPrint((int *)DebugPrintBuf, 50);

                    SysInitArrestSequence(TRUE);

                    SysErrorCommand(g_usnCurrentMode);
                }
            }

            //!Calculate the distance of the Y-Axis based on the Direction movement
            //!Check for the home position check condition
            if(MOTOR5_HOME_DIRECTION == pMotor5Pwm1->Direction)
            {
                //!If the motor is moving in up direction
                //!Subtract the distance to the current motor position
                pMotor5Pwm1->iMotorPosition = pMotor5Pwm1->iMotorPosition - pMotor5Pwm1->FBTimerCounter;
                if(pMotor5Pwm1->iMotorPosition < ZERO)
                {
                    pMotor5Pwm1->iMotorPosition = ZERO;
                }
            }

            else if((!MOTOR5_HOME_DIRECTION) == pMotor5Pwm1->Direction)
            {
                //!If the motor is moving in down direction
                //!Add the distance to the current motor position
                pMotor5Pwm1->iMotorPosition = pMotor5Pwm1->iMotorPosition + pMotor5Pwm1->TimerCounter;
            }

            //!Set the motor to idle state
            pMotor5Pwm1->Direction = IDLE;

            m_bYAxisMonEnable = FALSE;

            //!Check for the following conditions
            //!If the current position is zero and Home is detected
            //!If the current position is not zero and home is not detected
            if(((250 >= pMotor5Pwm1->iMotorPosition) && (true == OIHomeSensor5State()))||
            ((250 < pMotor5Pwm1->iMotorPosition) && (false == OIHomeSensor5State())))
            {
                if(true == OIHomeSensor5State())
                {
                    pMotor5Pwm1->iMotorPosition = 0;
                    //!Clear the error code for Y-Axis
                    utErrorInfoCheck.stErrorInfo.ulnY_Motor = FALSE;

                }

            }
            else
            {
                //!Set the error Code for Y-Axis
                utErrorInfoCheck.stErrorInfo.ulnY_Motor = TRUE;
                sprintf(DebugPrintBuf, "Y Set : %ld %d\n\r", pMotor5Pwm1->iMotorPosition, OIHomeSensor5State());
                UartDebugPrint((int *)DebugPrintBuf, 50);
                sprintf(DebugPrintBuf, "\n Y Motor error 2\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);

                SysInitArrestSequence(TRUE);

                SysErrorCommand(g_usnCurrentMode);
            }
        }
    }
    else
    {
        //!If the motor status is completed, then set it to idle state
        if(pMotor5Pwm1->Status == COMPLETED)
        {
            pMotor5Pwm1->Direction = IDLE;
            m_bYAxisMonEnable = FALSE;
        }
    }
}
/******************************************************************************/
/*!
* \fn void SysMBDSequenceCompletion(void)
* \brief Function to relinitalize the variables and set motor to idle after
* completion of the sequence
* \return void
*/
/******************************************************************************/
void SysMBDSequenceCompletion(void)
{
//    sprintf(DebugPrintBuf, "\r\nSysMBDSequenceCompletion %d \n\n",BloodCellCounter_Y.CycleFlag);
//    UartDebugPrint((int *)DebugPrintBuf, 70);

    //!Check if the cycle flag is set from MBD for the sequence running
    switch(BloodCellCounter_Y.CycleFlag)
    {
         case WHOLE_BLOOD_MODE:
         case BODY_FLUID_MODE:
         //!Do not check for START_PRE_DILUENT_COUNTING_CMD during cycle completion
         //!Cycle completion is mainly for the main mode (pre-dilute mode) and
         //!not the intermediate mode of starting the pre-dilute sequence after
         //!diluting the blood externally in pre-dilute mode
         //case START_PRE_DILUENT_COUNTING_CMD:
         case PRE_DILUENT_MODE:
         case START_UP_SEQUENCE_MODE:
         case AUTO_CALIBRATION_WB:         //SKM_CHANGE_CAL
         case COMMERCIAL_CALIBRATION_WBC:  //SKM_CHANGE_CAL
         case COMMERCIAL_CALIBRATION_RBC:  //SKM_CHANGE_CAL
         case COMMERCIAL_CALIBRATION_PLT:  //SKM_CHANGE_CAL
         case QUALITY_CONTROL_WB:          //SKM_CHANGE_QC
         case QUALITY_CONTROL_BODY_FLUID:  //SKM_CHANGE_QC
         case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
         case MBD_STARTUP_FAILED_CMD:
             //!Set all the motors to idle mode after completion of MBD sequence
            SysMotorControlIdleMode();
            //!Reset the current mode variables
            g_usnCurrentModeTemp = ZERO;
            g_usnCurrentModeMajor = ZERO;
            g_usnCurrentMode = ZERO;
            //!Initialize the MBD related varialbes
            BloodCellCounter_initialize();
            //printf("Sequence Ends & Press Key to restart \n");
            //!Reset CPU interrupt counter
            CpuTimer0.InterruptCount = ZERO;

            //!Set valve open state and count stop to Zero
            BloodCellCounter_U.ValveOpenStart = FALSE;
            BloodCellCounter_U.CountStop = ZERO;


            //!Indicate sequence from MBD is completed. This is used to indicate
            //! Sitara to tell the seqeunce is completed and next sequence can be initiated
            m_bSequenceCompleted = TRUE;

            sprintf(DebugPrintBuf, "\r\n MBD_Seq_Comp: %ld\n\n", g_unSequenceStateTime1);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;

            //!Enable the flag for ready for aspiration as soon as the trigger
            //! button is pressed
            m_bReadyforAspCommand = TRUE;

            m_bPressureValveOn = TRUE;
            bPressureMonitor = FALSE;

            //!Set the counter to excite the valve to 5sec time
            m_ucValveControlTime = VALVE_DELAY_TIME;
            SysInitValveTest(VALVE_7);

            /*! Set the m_bTxPendingFlag to False*/
            m_bTxPendingFlag = FALSE;
            break;

         case PRIME_WITH_RINSE_SERVICE_HANDLING:
         case PRIME_WITH_LYSE_SERVICE_HANDLING:
         case PRIME_WITH_DILUENT_SERVICE_HANDLING:
         case PRIME_WITH_EZ_CLEANSER_CMD:
         case PRIME_ALL_SERVICE_HANDLING:
         case BACK_FLUSH_SERVICE_HANDLING:
         case ZAP_APERTURE_SERVICE_HANDLING:
         case DRAIN_BATH_SERVICE_HANDLING:
         case DRAIN_ALL_SERVICE_HANDLING:
         case CLEAN_BATH_SERVICE_HANDLING:
         case SYSTEM_STATUS_SERVICE_HANDLING:
         case VALVE_TEST_COMPLETION:
         case WASTE_BIN_FULL_CMD:
         case HEAD_RINSING_SERVICE_HANDLING:
         case PROBE_CLEANING_SERVICE_HANDLING:
         case X_AXIS_MOTOR_CHECK:
         case Y_AXIS_MOTOR_CHECK:
         case DILUENT_SYRINGE_MOTOR_CHECK:
         case WASTE_SYRINGE_MOTOR_CHECK:
         case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:
         case MBD_WAKEUP_START:
         case MBD_SLEEP_PROCESS:
         case MBD_ALL_MOTOR_Y_AXIS_CHECK:
         case MBD_ALL_MOTOR_X_AXIS_CHECK:
         case MBD_ALL_MOTOR_SAMPLE_CHECK:
         case MBD_ALL_MOTOR_DILUENT_CHECK:
         case MBD_ALL_MOTOR_WASTE_CHECK:
         case MBD_VOL_TUBE_EMPTY:
         case MBD_BATH_FILL_CMD:
         case MBD_STARTUP_BACKFLUSH_CMD:
         case MBD_ZAP_INITIATE:
         case GLASS_TUBE_FILLING:
         case COUNT_FAIL:
         case HYPO_CLEANER_FAIL:
         //case MBD_USER_ABORT_PROCESS:

//             sprintf(DebugPrintBuf, "\r\nSysMBDSequenceCompletion %ld %ld\n\n", BloodCellCounter_Y.CycleFlag,g_usnCurrentMode);
//             UartDebugPrint((int *)DebugPrintBuf, 50);
             if(BloodCellCounter_Y.CycleFlag == g_usnCurrentMode)
             {
                 //!Initialize the MBD related varialbes
                 BloodCellCounter_initialize();

                 //!Send sequence completed commant to sitara in any of the
                 //!service handling seqeunce
                 if(SITARA_STARTUP_CMD == g_usnCurrentModeMajor)
                 {
                     CIFormStartupHandlingPacket(g_usnCurrentMode, FALSE);
                 }
                 else
                 {
                     CIFormServiceHandlingPacket(g_usnCurrentMode, FALSE);
                 }

                 sprintf(DebugPrintBuf, "\r\n MBD_Seq_Comp:  Service\n\n");
                 UartDebugPrint((int *)DebugPrintBuf, 50);

                 //!Clear the busy pin
                 SPIClearCommCtrlBusyInt();
                 //!Reset the current mode variables
                 g_usnCurrentModeMajor = ZERO;
                 g_usnCurrentMode = ZERO;
                 g_usnCurrentModeTemp = ZERO;
                 //!Set all the motors to idle mode after completion of MBD sequence
                 SysMotorControlIdleMode();

                 //!Enable the flag for ready for aspiration as soon as the trigger
                 //! button is pressed
                 m_bReadyforAspCommand = TRUE;
                 //!Set Xaxis position to invalid position
                // m_eXPosition = eXPositionInvalid;
                 bPressureMonitor = FALSE;

                 m_bPressureValveOn = TRUE;
                 /*! Set the m_bTxPendingFlag to False*/
                 m_bTxPendingFlag = FALSE;
             }
         break;

         case MBD_VOLUMETRIC_BOARD_CHECK:
             if(BloodCellCounter_Y.CycleFlag == g_usnCurrentMode)
              {
                  //!Send sequence completed commant to sitara in any of the
                  //!service handling seqeunce

                 if(FALSE == utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor)
                 {
                     if(LIQUID_SENSOR_ON == m_bRBCStartSensor)
                     {
                         //!Clear the RBC start LED error
                         utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor = FALSE;
                     }
                     else
                     {
                         //!Set the RBC start LED error
                         utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor = TRUE;
                     }
                 }
                 if(FALSE == utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor)
                 {
                     if(LIQUID_SENSOR_ON == m_bRBCStopSensor)
                     {
                         //!Clear the RBC stop LED error
                         utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor = FALSE;
                     }
                     else
                     {
                         //!Set the RBC stop LED error
                         utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor = TRUE;
                     }
                 }
                 if(FALSE == utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor)
                 {
                     if(LIQUID_SENSOR_ON == m_bWBCStartSensor)
                     {
                         //!Clear the WBC start LED error
                         utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor = FALSE;
                     }
                     else
                     {
                         //!Set the WBC start LED error
                         utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor = TRUE;
                     }
                 }
                 if(FALSE == utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor)
                 {
                     if(LIQUID_SENSOR_ON == m_bWBCStopSensor)
                     {
                         //!Clear the WBC stop LED error
                         utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor = FALSE;
                     }
                     else
                     {
                         //!Set the WBC stop LED error
                         utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor = TRUE;
                     }
                 }
                 if((utErrorInfoCheck.stErrorInfo.ulnRBCStartSensor) || \
                         (utErrorInfoCheck.stErrorInfo.ulnRBCStopSensor) || \
                         (utErrorInfoCheck.stErrorInfo.ulnWBCStartSensor) || \
                         (utErrorInfoCheck.stErrorInfo.ulnWBCStopSensor))
                 {
                     CIFormStartupHandlingPacket(DELFINO_ERROR_MINOR_CMD, FALSE);
                 }
                 else
                 {
                     CIFormStartupHandlingPacket(g_usnCurrentMode, FALSE);
                 }

                  //!Clear the busy pin
                  SPIClearCommCtrlBusyInt();
                  //!Reset the current mode variables
                  g_usnCurrentModeMajor = ZERO;
                  g_usnCurrentMode = ZERO;
                  g_usnCurrentModeTemp = ZERO;
                  //!Set all the motors to idle mode after completion of MBD sequence
                  SysMotorControlIdleMode();
                  //!Initialize the MBD related varialbes
                  BloodCellCounter_initialize();

                  //!Enable the flag for ready for aspiration as soon as the trigger
                  //! button is pressed
                  m_bReadyforAspCommand = TRUE;

                  m_bPressureValveOn = TRUE;
              }
             break;

         case SHUTDOWN_SEQUENCE:
              if(BloodCellCounter_Y.CycleFlag == g_usnCurrentMode)
              {
                  //Implemetation to Send status to Sitara - TBD
                  //!Frame and send Shutdown sequence completed comand to Sitara
                  CIFormShutDownSequencePacket(DELFINO_SHUTDOWN_SEQUENCE_COMPLETED_CMD);
                  //!Clear the busy pin
                  SPIClearCommCtrlBusyInt();
                  //!Reset the current mode variables
                  g_usnCurrentModeMajor = ZERO;//SKM_CHANGE_MBD_ARC
                  g_usnCurrentMode = ZERO;
                  g_usnCurrentModeTemp = ZERO;
                  //!Set all the motors to idle mode after completion of MBD sequence
                  SysMotorControlIdleMode();
                  //!Initialize the MBD related varialbes
                  BloodCellCounter_initialize();

                  //!Enable the flag for ready for aspiration as soon as the trigger
                  //! button is pressed
                  m_bReadyforAspCommand = TRUE;

                  m_bPressureValveOn = TRUE;
              }
         break;

         case MBD_ABORT_PROCESS_COMPLETED:
             //!Set all the motors to idle mode after completion of MBD sequence
             SysMotorControlIdleMode();
             //!Reset the current mode variables
             g_usnCurrentModeMajor = ZERO;
             g_usnCurrentMode = ZERO;
             g_usnCurrentModeTemp = ZERO;
             //!Initialize the MBD related varialbes
             BloodCellCounter_initialize();
             //Commented temperorily to avoid undesired scenario
             //during raw data transfer - SITARA PROBLEM
             //CIFormAbortProcessPacket(DELFINO_ABORT_PROCESS_SUB_CMD);
             sprintf(DebugPrintBuf, "\r\n MBD_ABORT_PROCESS_COMPLETED \n\n");
             UartDebugPrint((int *)DebugPrintBuf, 50);
             CIFormServiceHandlingPacket(g_usnCurrentMode, FALSE);
             //!Clear the busy pin
             if(g_bBusyBitStatus == TRUE)
             {
                 SPIClearCommCtrlBusyInt();
             }

             //!Enable the flag for ready for aspiration as soon as the trigger
             //! button is pressed
             m_bReadyforAspCommand = TRUE;

             //!Set Xaxis position to RBC position
             //m_eXPosition = eRBC_BATH;

             m_bPressureValveOn = TRUE;
         break;

         case COUNTING_TIME_SEQUENCE:

             if((BloodCellCounter_Y.CycleFlag == g_usnCurrentMode) && \
                     (m_bCountingTimeSeq == TRUE))  //CRH_TBD - to check whether this variable is required
             {
                 m_bCountingTimeSeq = FALSE;    //CRH_TBD - to check whether this variable is required
                 //!Frame counting sequence completed and send to Sitara
                 CIFormServiceHandlingPacket(g_usnCurrentMode, FALSE);
                 //!Clear the busy pin
                 SPIClearCommCtrlBusyInt();
                 //!Reset the current mode variables
                 g_usnCurrentModeMajor = ZERO;
                 g_usnCurrentMode = ZERO;
                 g_usnCurrentModeTemp = ZERO;
                 //!Set all the motors to idle mode after completion of MBD sequence
                 SysMotorControlIdleMode();
                 //!Initialize the MBD related varialbes
                 BloodCellCounter_initialize();

                 //!Enable the flag for ready for aspiration as soon as the trigger
                 //! button is pressed
                 m_bReadyforAspCommand = TRUE;

                 m_bPressureValveOn = TRUE;
             }
         break;

         case FLOW_CALIBRATION:
             if((BloodCellCounter_Y.CycleFlag == g_usnCurrentMode) && \
                     (m_bCountingTimeSeq == TRUE))  //CRH_TBD - to check whether this variable is required
             {
                 sprintf(DebugPrintBuf, "\r\n FLOW_CALIBRATION Completed \n");
                 UartDebugPrint((int *)DebugPrintBuf, 50);
                 m_bCountingTimeSeq = FALSE;    //CRH_TBD - to check whether this variable is required
                 //!Frame counting sequence completed and send to Sitara
                 CIFormSystemSettingPacket(DELFINO_FLOW_CALIBRATION_CMD);
                 //!Clear the busy pin
                 SPIClearCommCtrlBusyInt();
                 //!Reset the current mode variables
                 g_usnCurrentModeMajor = ZERO;
                 g_usnCurrentMode = ZERO;
                 g_usnCurrentModeTemp = ZERO;
                 //!Set all the motors to idle mode after completion of MBD sequence
                 SysMotorControlIdleMode();
                 //!Initialize the MBD related varialbes
                 BloodCellCounter_initialize();

                 //!Enable the flag for ready for aspiration as soon as the trigger
                 //! button is pressed
                 m_bReadyforAspCommand = TRUE;

                 m_bPressureValveOn = TRUE;
             }
             break;

         case RINSE_PRIME_FLOW_CALIBRATION:
                //!Set all the motors to idle mode after completion of MBD sequence
                SysMotorControlIdleMode();
                //!Initialize the MBD related varialbes
                BloodCellCounter_initialize();
                sprintf(DebugPrintBuf, "\r\n FLOW_CALIBRATION Completed \n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
                //!Frame sequence completed packet ans send to Sitara
                CIFormSystemSettingPacket(DELFINO_RINSE_PRIME_COMPLETED);
                //!Clear the busy pin
                SPIClearCommCtrlBusyInt();
                //!Reset the current mode variables
                g_usnCurrentModeMajor = ZERO;
                g_usnCurrentMode = ZERO;
                g_usnCurrentModeTemp = ZERO;

                //!Enable the flag for ready for aspiration as soon as the trigger
                //! button is pressed
                m_bReadyforAspCommand = TRUE;

                m_bPressureValveOn = TRUE;
                break;

         case MBD_TO_SHIP_SEQUENCE_START:
                //!Set all the motors to idle mode after completion of MBD sequence
                SysMotorControlIdleMode();
                //!Initialize the MBD related varialbes
                BloodCellCounter_initialize();
                //!Frame sequence completed packet ans send to Sitara
                //CIFormSystemSettingPacket(DELFINO_RINSE_PRIME_COMPLETED);
                CIFormServiceHandlingPacket(g_usnCurrentMode, FALSE);
                //!Clear the busy pin
                SPIClearCommCtrlBusyInt();
                //!Reset the current mode variables
                 g_usnCurrentModeMajor = ZERO;
                 g_usnCurrentMode = ZERO;
                 g_usnCurrentModeTemp = ZERO;

                 //!Enable the flag for ready for aspiration as soon as the trigger
                 //! button is pressed
                 m_bReadyforAspCommand = TRUE;

                 m_bPressureValveOn = TRUE;
                 break;

         case MBD_USER_ABORT_PROCESS:
             if(BloodCellCounter_Y.CycleFlag == g_usnCurrentMode)
             {
                 CIFormServiceHandlingPacket(g_usnCurrentMode, FALSE);

                 //!Reset the current mode variables
                 g_usnCurrentModeMajor = ZERO;
                 g_usnCurrentMode = ZERO;
                 g_usnCurrentModeTemp = ZERO;
                 //!Set all the motors to idle mode after completion of MBD sequence
                 SysMotorControlIdleMode();
                 //!Initialize the MBD related varialbes
                 BloodCellCounter_initialize();
                 //!Frame sequence completed packet ans send to Sitara
                 //CIFormAbortProcessPacket(DELFINO_ABORT_SEQUENCE_COMPLETED_CMD);
                 sprintf(DebugPrintBuf, "\r\n MBD_USER_ABORT_PROCESS 2 \n\n");
                 UartDebugPrint((int *)DebugPrintBuf, 50);

                 //CIFormServiceHandlingPacket(g_usnCurrentMode, FALSE);
                 //!Clear the busy pin
                 SPIClearCommCtrlBusyInt();
                 //!Reset all the motor positions to Zero
                 pMotor1Pwm8->iMotorPosition = ZERO;
                 pMotor2Pwm6->iMotorPosition = ZERO;
                 pMotor3Pwm2->iMotorPosition = ZERO;
                 pMotor4Pwm5->iMotorPosition = ZERO;
                 pMotor5Pwm1->iMotorPosition = ZERO;
                 //!Enable the flag for ready for aspiration as soon as the trigger
                 //! button is pressed
                 m_bReadyforAspCommand = TRUE;

                 m_bPressureValveOn = TRUE;
             }
          break;


         case MBD_USER_ABORT_Y_AXIS_PROCESS:
             if(BloodCellCounter_Y.CycleFlag == g_usnCurrentMode)
             {
                 //!Reset the current mode variables
                 g_usnCurrentModeMajor = ZERO;
                 //g_usnCurrentMode = ZERO;
                 g_usnCurrentModeTemp = ZERO;
                 //!Set all the motors to idle mode after completion of MBD sequence
                 SysMotorControlIdleMode();
                 //!Initialize the MBD related varialbes
                 BloodCellCounter_initialize();
                 //!Frame sequence completed packet ans send to Sitara
                 //CIFormAbortProcessPacket(DELFINO_Y_AXIS_ABORT_SEQUENCE_COMPLETED_CMD);         /*DS Testing*/
                 CIFormServiceHandlingPacket(g_usnCurrentMode, FALSE);                           /*DS Testing*/
                 g_usnCurrentMode = ZERO;
                 //!Clear the busy pin
                 SPIClearCommCtrlBusyInt();

                 //!Enable the flag for ready for aspiration as soon as the trigger
                 //! button is pressed
                 m_bReadyforAspCommand = TRUE;

                 //m_bPressureValveOn = TRUE;
             }
          break;

         default:
         break;
    }
}
/******************************************************************************/
/*!
* \fn void SysResultsTransmitToSitara(void)
* \brief Function to transmit count results to Sitara
* \return void
*/
/******************************************************************************/
void SysResultsTransmitToSitara(void)
{
    {
        //!Clear the busy pin
        SPIClearCommCtrlBusyInt();//SAM_SPI_BUSY_INTR
        //printf("g_usnCurrentModeTemp: %d \n", g_usnCurrentModeTemp);
        switch(g_usnDataMode)
        {
            case WHOLE_BLOOD_MODE:
            //!Do not check for START_PRE_DILUENT_COUNTING_CMD during cycle completion
            //!Cycle completion is mainly for the main mode (pre-dilute mode) and
            //!not the intermediate mode of starting the pre-dilute sequence after
            //!diluting the blood externally in pre-dilute mode
            //case START_PRE_DILUENT_COUNTING_CMD:
            case PRE_DILUENT_MODE:
            case BODY_FLUID_MODE:
            case START_UP_SEQUENCE_MODE:
            case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
            case MBD_STARTUP_FAILED_CMD:
            case PARTIAL_BLOOD_COUNT_WBC:
            case PARTIAL_BLOOD_COUNT_RBC_PLT:
            {
                //!Frame results packet for patient handling mode and send to Sitara
                CIFormPatientHandlingPacket(DELFINO_WBC_RBC_PLT_HGB_COUNT);
                //!Clear the busy pin
                SPIClearCommCtrlBusyInt();
                //!Reset the HGB values after sending data to Sitara
                m_usnHgbBlank = ZERO;
                m_usnHgbSample = ZERO;
            }
            break;

            //CRH_DEC_21 - START
            case AUTO_CALIBRATION_WB:
            case COMMERCIAL_CALIBRATION_WBC:
            case COMMERCIAL_CALIBRATION_RBC:
            case COMMERCIAL_CALIBRATION_PLT:
            {
                //!Frame results packet for calibration handling mode and send to Sitara
                CIFormCalibrationHandlingPacket(DELFINO_WBC_RBC_PLT_HGB_COUNT);
                //!Clear the busy pin
                SPIClearCommCtrlBusyInt();
                //!Reset the HGB values after sending data to Sitara
                m_usnHgbBlank = ZERO;
                m_usnHgbSample = ZERO;
            }
            break;

            case QUALITY_CONTROL_WB:
            case QUALITY_CONTROL_BODY_FLUID:
                //!Frame results packet for QC handling mode and send to Sitara
                CIFormQualityHandlingPacket(DELFINO_WBC_RBC_PLT_HGB_COUNT);
                //!Clear the busy pin
                SPIClearCommCtrlBusyInt();
                //!Reset the HGB values after sending data to Sitara
                m_usnHgbBlank = ZERO;
                m_usnHgbSample = ZERO;
                break;
            //CRH_DEC_21 - END

            default:
            break;
        }
    }
}

/******************************************************************************/
/*!
* \fn void SysSequenceCompletion(void)
* \brief Function to send sequence completed status to Sitara
* \return void
*/
/******************************************************************************/
void SysSequenceCompletion(void)
{
    //!If the sequence is completed, then send sequence completed command to Sitara
    if((TRUE == m_bSequenceCompleted) && (TRUE == g_bDataTransmitted))
    {
        if((m_bSPIWriteControlStatus == FALSE ) && (FALSE == m_bTxPendingFlag))
        {
            //!Set the cycle completion state to Default state
            m_eCycleCompletion = eDefault;

            g_bDataTransmitted = FALSE;

            //!Reset the sequence completed flag
            m_bSequenceCompleted = FALSE;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = g_unSequenceStateTime;
            //m_arrusnTimeStamp[m_usnTimeStampLen++] = m_usnPressureAdc;
            //!Depending upon the data mode send sequence completed command to Sitara
            switch(g_usnDataMode)
            {
            case WHOLE_BLOOD_MODE:
            //case START_PRE_DILUENT_COUNTING_CMD:
            case PRE_DILUENT_MODE:
            case BODY_FLUID_MODE:
            case START_UP_SEQUENCE_MODE:
            case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
            case MBD_STARTUP_FAILED_CMD:
            case PARTIAL_BLOOD_COUNT_WBC:
            case PARTIAL_BLOOD_COUNT_RBC_PLT:
                sprintf(DebugPrintBuf, "\r\n DEL_SEQ_COMP: %ld\n", g_unSequenceStateTime1);
                UartDebugPrint((int *)DebugPrintBuf, 50);
                //!Frame the sequence completed packet for patient handling and send to Sitara
                CIFormPatientHandlingPacket(DELFINO_SEQUENCE_COMPLETION);
                //!Reset the data mode
                g_usnDataMode = 0;
                //!Clear the busy pin so that next seqeunce can be initiated from processor
                SPIClearCommCtrlBusyInt();
                /*! Set the m_bTxPendingFlag to False*/
                m_bTxPendingFlag = FALSE;
                break;

            case AUTO_CALIBRATION_WB:
            case COMMERCIAL_CALIBRATION_WBC:
            case COMMERCIAL_CALIBRATION_RBC:
            case COMMERCIAL_CALIBRATION_PLT:
                sprintf(DebugPrintBuf, "\n Calib Handling DELFINO_SEQUENCE_COMPLETION\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
                //!Frame the sequence completed packet for Calibration handling and send to Sitara
                CIFormCalibrationHandlingPacket(DELFINO_SEQUENCE_COMPLETION);
                //!Reset the data mode
                g_usnDataMode = 0;
                //!Clear the busy pin so that next seqeunce can be initiated from processor
                SPIClearCommCtrlBusyInt();
                /*! Set the m_bTxPendingFlag to False*/
                m_bTxPendingFlag = FALSE;
                break;

            case QUALITY_CONTROL_WB:
            case QUALITY_CONTROL_BODY_FLUID:
                sprintf(DebugPrintBuf, "\n Quality Handling DELFINO_SEQUENCE_COMPLETION\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
                //!Frame the sequence completed packet for QC handling and send to Sitara
                CIFormQualityHandlingPacket(DELFINO_SEQUENCE_COMPLETION);
                //!Reset the data mode
                g_usnDataMode = 0;
                //!Clear the busy pin so that next seqeunce can be initiated from processor
                SPIClearCommCtrlBusyInt();
                break;

            default:
                break;
            }
            //!Don't reset the CPU2, it will clear all the data present in SDRAM
            //stCpu1IpcCommPtr->unCpu1ToCpu2TxBuf[0] = CPU1_IPC_RESET_FLAG;
            //IPCCpu1toCpu2IpcCmd(stCpu1IpcCommPtr);

            //!Enable the flag for ready for aspiration as soon as the cycle is completed
            m_bReadyforAspCommand = TRUE;
        }
    }
}

/******************************************************************************/
/*!
* \fn void SysReInitOnNewMeasurement(void)
* \brief Function to re-initialize on new measurmeent
* \return void
*/
/******************************************************************************/
void SysReInit()
{
    //!Set Cycle completion to default state
    m_eCycleCompletion = eDefault;


    //!Clear the Data mode to Zero
    g_usnDataMode = 0;

    //!Reset the volumeteric error on next cycle
    utVolumetricErrorInfo.unVolumetricError = ZERO;
    utVolumetricErrorInfoReAcq.unVolumetricError = ZERO;
}

/******************************************************************************/
/*!
 *  \fn     SysBinArrayToInt()
 *  \brief  Function to convert the array of binary value o Integer
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysBinArrayToInt()
{
    int index = 0;
    valveStatus1 = 0;
    valveStatus2 = 0;
    for(index=0;index<8;index++)
    {
         if((BloodCellCounter_Y.ValveStatus_1[index]) == 1)
         {
             valveStatus1 = valveStatus1 | (1 << index);
         }
         if((BloodCellCounter_Y.ValveStatus_2[index]) == 1)
         {
             valveStatus2 = valveStatus2 | (1 << index);
         }
     }
}

/******************************************************************************/
/*!
 *  \fn     SysErrorCommand()
 *  \brief  Function to frame the error command to be sent to processor
 *  \param  uint16_t usnCurrentMode
 *  \return void
 */
/******************************************************************************/
void SysErrorCommand(uint16_t usnCurrentMode)
{
    CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);

    /*switch(usnCurrentMode)
    {
        case WHOLE_BLOOD_MODE:
        //case START_PRE_DILUENT_COUNTING_CMD:
        case PRE_DILUENT_MODE:
        case BODY_FLUID_MODE:
        case START_UP_SEQUENCE_MODE:
        case PARTIAL_BLOOD_COUNT_WBC:
        case PARTIAL_BLOOD_COUNT_RBC_PLT:
            //!Frame the sequence completed packet for patient handling and send to Sitara
            CIFormPatientHandlingPacket(DELFINO_ERROR_MINOR_CMD);
            break;

        case AUTO_CALIBRATION_WB:
        case COMMERCIAL_CALIBRATION_WBC:
        case COMMERCIAL_CALIBRATION_RBC:
        case COMMERCIAL_CALIBRATION_PLT:
            //!Frame the sequence completed packet for Calibration handling and send to Sitara
            CIFormCalibrationHandlingPacket(DELFINO_ERROR_MINOR_CMD);
            break;

        case QUALITY_CONTROL_WB:
        case QUALITY_CONTROL_BODY_FLUID:
            //!Frame the sequence completed packet for QC handling and send to Sitara
            CIFormQualityHandlingPacket(DELFINO_ERROR_MINOR_CMD);
            break;

        case PRIME_WITH_RINSE_SERVICE_HANDLING:
        case PRIME_WITH_LYSE_SERVICE_HANDLING:
        case PRIME_WITH_DILUENT_SERVICE_HANDLING:
        case PRIME_WITH_EZ_CLEANSER_CMD:
        case PRIME_ALL_SERVICE_HANDLING:
        case BACK_FLUSH_SERVICE_HANDLING:
        case ZAP_APERTURE_SERVICE_HANDLING:
        case DRAIN_BATH_SERVICE_HANDLING:
        case DRAIN_ALL_SERVICE_HANDLING:
        case CLEAN_BATH_SERVICE_HANDLING:
        case SYSTEM_STATUS_SERVICE_HANDLING:
        case VALVE_TEST_COMPLETION:
        case WASTE_BIN_FULL_CMD:
        case HEAD_RINSING_SERVICE_HANDLING:
        case PROBE_CLEANING_SERVICE_HANDLING:
        case X_AXIS_MOTOR_CHECK:
        case Y_AXIS_MOTOR_CHECK:
        case DILUENT_SYRINGE_MOTOR_CHECK:
        case WASTE_SYRINGE_MOTOR_CHECK:
        case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:
        case MBD_WAKEUP_START:
        case MBD_SLEEP_PROCESS:
        case MBD_VOLUMETRIC_BOARD_CHECK:
            if(SITARA_STARTUP_CMD == g_usnCurrentModeMajor)
            {
                CIFormStartupHandlingPacket(DELFINO_ERROR_MINOR_CMD, FALSE);
            }
            else
            {
                CIFormServiceHandlingPacket(DELFINO_ERROR_MINOR_CMD, FALSE);
            }
        break;

        case ZERO:
            CIFormServiceHandlingPacket(DELFINO_ERROR_MINOR_CMD, FALSE);
            break;

        case SHUTDOWN_SEQUENCE:
            break;

        default:
            break;

    }
    */

}
/******************************************************************************/
/*!
 * \fn void SysInitArrestSequence(void)
 * \brief   Function to arrest the current sequence
 * \return  void
 * \Author  SBS
 */
/******************************************************************************/
void SysInitArrestSequence(bool_t bSendCommandToSitara)
{
    if(bSendCommandToSitara)
    {
//        sprintf(DebugPrintBuf, "\r\nSysInitArrestProcess %d \n", BloodCellCounter_Y.STATE);
//        UartDebugPrint((int *)DebugPrintBuf, 50);

        utErrorInfoCheck.stErrorInfo.ulnSequenceArrested = TRUE;
        //!Disable the start button for next measurement
        g_bStartButtonStatus = FALSE;

        //!Clear the busy pin so that Processor can send the next command
        SPIClearCommCtrlBusyInt();

        //!The current execution mode shall be set to no mode
        g_usnCurrentModeMajor = ZERO;
        //!The current execution mode shall be set to no mode
        g_usnCurrentMode = ZERO;
        //!The current temperory execution mode (for pre-diluent)
        //!shall be set to no mode
        g_usnCurrentModeTemp = ZERO;
        //!Set all the motors to Idle mode - No Motor shall run after
        //!any process is aborted
        SysMotorControlIdleMode();

        PWMParamDefaultState();

        //!Re-initialize all the variables of the MBD sequence immediately after
        //!aborting the sequence
        BloodCellCounter_initialize();

        m_eCycleCompletion = eDefault;

        //!Enable the flag for ready for aspiration as soon as the trigger
        //! button is pressed
        m_bReadyforAspCommand = TRUE;

        //!Set Xmotor position to invalid
        m_eXPosition = eXPositionInvalid;


        //!Reset the volumetric error
        utVolumetricErrorInfo.unVolumetricError = ZERO;
        utVolumetricErrorInfoReAcq.unVolumetricError = ZERO;

        //!Reset all the motor positions to Zero
        /*pMotor1Pwm8->iMotorPosition = ZERO;
        pMotor2Pwm6->iMotorPosition = ZERO;
        pMotor3Pwm2->iMotorPosition = ZERO;
        pMotor4Pwm5->iMotorPosition = ZERO;
        pMotor5Pwm1->iMotorPosition = ZERO;
        */

        //!Disable all the motor monitor
        m_bYAxisMonEnable = FALSE;
        m_bWasteSyringeMonEnable = FALSE;
        m_bSampleSyringeMonEnable = FALSE;
        m_bDiluentSyringeMonEnable = FALSE;
        m_bXAxisMonEnable = FALSE;
    }

}

/******************************************************************************/
/*!
 * \fn void SysInitProbeCleanArrestSequence(void)
 * \brief   Function to arrest the current sequence
 * \return  void
 * \Author  SBS
 */
/******************************************************************************/
void SysInitProbeCleanArrestSequence(bool_t bSendCommandToSitara)
{
    if(bSendCommandToSitara)
    {
//        utErrorInfoCheck.stErrorInfo.ulnSequenceArrested = TRUE;
        //!Disable the start button for next measurement
        g_bStartButtonStatus = FALSE;

        //!Clear the busy pin so that Processor can send the next command
        SPIClearCommCtrlBusyInt();

        //!The current execution mode shall be set to no mode
        g_usnCurrentModeMajor = ZERO;
        //!The current execution mode shall be set to no mode
        g_usnCurrentMode = ZERO;
        //!The current temperory execution mode (for pre-diluent)
        //!shall be set to no mode
        g_usnCurrentModeTemp = ZERO;
        //!Set all the motors to Idle mode - No Motor shall run after
        //!any process is aborted
        SysMotorControlIdleMode();

        PWMParamDefaultState();

        //!Re-initialize all the variables of the MBD sequence immediately after
        //!aborting the sequence
        BloodCellCounter_initialize();

        m_eCycleCompletion = eDefault;

        //!Enable the flag for ready for aspiration as soon as the trigger
        //! button is pressed
        m_bReadyforAspCommand = TRUE;

        //!Set Xmotor position to invalid
        m_eXPosition = eXPositionInvalid;


        //!Reset the volumetric error
        utVolumetricErrorInfo.unVolumetricError = ZERO;
        utVolumetricErrorInfoReAcq.unVolumetricError = ZERO;

        //!Reset all the motor positions to Zero
        /*pMotor1Pwm8->iMotorPosition = ZERO;
        pMotor2Pwm6->iMotorPosition = ZERO;
        pMotor3Pwm2->iMotorPosition = ZERO;
        pMotor4Pwm5->iMotorPosition = ZERO;
        pMotor5Pwm1->iMotorPosition = ZERO;
        */

        //!Disable all the motor monitor
        m_bYAxisMonEnable = FALSE;
        m_bWasteSyringeMonEnable = FALSE;
        m_bSampleSyringeMonEnable = FALSE;
        m_bDiluentSyringeMonEnable = FALSE;
        m_bXAxisMonEnable = FALSE;
    }

}


/******************************************************************************/
/*!
 * \fn void SysCheckPressure(void)
 * \brief   Function to check, is the pressure within the expected range
 * \return  void
 */
/******************************************************************************/
void SysCheckPressure(void)
{
//    sprintf(DebugPrintBuf, "\rPressure: %ld %f\n", m_usnPressureAdc,((((float)m_usnPressureAdc*3.0/4096-(0.1*3.3))/(0.8*3.3/(30-(-30))))+-30));
//    UartDebugPrint((int *)DebugPrintBuf, 50);
    if(TRUE == utSysStatusMonEnInfo.stSysStatusMonEn.ucPressureMon)
    {
        //!Check if the pressure is with in the specified limit
        if(m_usnPressureAdc < DEFAULT_SYSTEM_PRESSURE_SENSOR_FAIL)
        {
            sprintf(DebugPrintBuf, "\r\n Pressure sensor fail \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            //!Set the error code and abort the sequence
            utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail = TRUE;
        }
        else
        {
            //!Clear the error code
            utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail = FALSE;
        }

        if(m_usnPressureAdc < DEFAULT_SYSTEM_PRESSURE_LOWER_LIMIT)
        {
            sprintf(DebugPrintBuf, "\r\n Low Vacuum \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            //!Set the error code and abort the sequence
            utErrorInfoCheck.stErrorInfo.ulnLowVacuum = TRUE;
        }
        else
        {
            //!Clear the error code
            utErrorInfoCheck.stErrorInfo.ulnLowVacuum = FALSE;
        }

        if(m_usnPressureAdc >= DEFAULT_SYSTEM_PRESSURE_UPPER_LIMIT)
        {
            sprintf(DebugPrintBuf, "\r\n High Pressure: %ld\n", m_usnPressureAdc);
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Set the error code and abort the sequence
            utErrorInfoCheck.stErrorInfo.ulnHighPressure = TRUE;

            m_bPressureValveOn = TRUE;
        }
        else
        {
            //!Clear the error code
            utErrorInfoCheck.stErrorInfo.ulnHighPressure = FALSE;
        }

        //!Check for the error and send the command to Sitara.
        //! If any of the pressure error, then arrest the sequence and send the
        //! command to Sitara
        if((TRUE == utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail) || \
                (TRUE == utErrorInfoCheck.stErrorInfo.ulnLowVacuum) || \
                (TRUE == utErrorInfoCheck.stErrorInfo.ulnHighPressure))
        {
            //!Arrest the current sequence and set the error
            SysInitArrestSequence(TRUE);

            //!Disable pressure monitoring
            utSysStatusMonEnInfo.stSysStatusMonEn.ucPressureMon = FALSE;

            //!Frame the sequence completed packet for patient handling and send to Sitara
            //CIFormPatientHandlingPacket(DELFINO_SEQUENCE_COMPLETION);//Hn_added for testing pressure

            //!Frame the error command and abort the sequence
            SysErrorCommand(g_usnCurrentMode);
        }
    }
    else
    {
        if((m_usnPressureAdc > DEFAULT_SYSTEM_PRESSURE_LOWER_HYST_LIMIT) && \
                            (m_usnPressureAdc < DEFAULT_SYSTEM_PRESSURE_UPPER_HYST_LIMIT))
        {
            sprintf(DebugPrintBuf, "\r\n Pressure within limit: %ld\n", m_usnPressureAdc);
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Clear the error code
            utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail = FALSE;

            //!Clear the error code
            utErrorInfoCheck.stErrorInfo.ulnLowVacuum = FALSE;

            //!Clear the error code
            utErrorInfoCheck.stErrorInfo.ulnHighPressure = FALSE;

            //!Send system status command to Sitara
            CIFormServiceHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);

            //!Enable pressure monitoring
            utSysStatusMonEnInfo.stSysStatusMonEn.ucPressureMon = TRUE;
        }
    }

}

/******************************************************************************/
/*!
 * \fn void SysCheckTemperature(void)
 * \brief   Function to check, is the temperature within the expected range
 * \return  void
 */
/******************************************************************************/
void SysCheckTemperature(void)
{
    if(TRUE == utSysStatusMonEnInfo.stSysStatusMonEn.ucTemperatureMon)
    {
        //!Check if the temperature is within the specified limit, if not set the
        //!temperature error
        if(m_usnTemperatureAdc < DEFAULT_SYSTEM_TEMPERATURE_LOWER_LIMIT || \
                m_usnTemperatureAdc >= DEFAULT_SYSTEM_TEMPERATURE_UPPER_LIMIT)
        {
            sprintf(DebugPrintBuf, "\r\n Temp Error: %ld\n", m_usnTemperatureAdc);
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!If the temperature is with in the specified limit, then clear the error
            utErrorInfoCheck.stErrorInfo.ulnTemperature = TRUE;

            //!Disable pressure monitoring
            utSysStatusMonEnInfo.stSysStatusMonEn.ucTemperatureMon = FALSE;

            //!Frame the error command and abort the sequence
            SysErrorCommand(g_usnCurrentMode);
        }
        else
        {
            //!If the temperature is with in the specified limit, then clear the error
            utErrorInfoCheck.stErrorInfo.ulnTemperature = FALSE;
        }
    }
    else
    {
        if(m_usnTemperatureAdc > DEFAULT_SYSTEM_TEMP_HYST_LOWER_LIMIT && \
                m_usnTemperatureAdc < DEFAULT_SYSTEM_TEMP_HYST_UPPER_LIMIT)
        {
            sprintf(DebugPrintBuf, "\r\n Temp Within limit: %ld\n", m_usnTemperatureAdc);
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!If the temperature is with in the specified limit, then clear the error
            utErrorInfoCheck.stErrorInfo.ulnTemperature = FALSE;

            //!Send system status command to Sitara
            CIFormServiceHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);

            //!Enable temperature monitoring
            utSysStatusMonEnInfo.stSysStatusMonEn.ucTemperatureMon = TRUE;
        }
    }
}

/******************************************************************************/
/*!
 * \fn void SysCheck_24V(void)
 * \brief   Function to check, is the 24V within the expected range
 * \return  void
 */
/******************************************************************************/
void SysCheck_24V(void)
{
    if(TRUE == utSysStatusMonEnInfo.stSysStatusMonEn.uc24VMon)
    {
        //!Check if the 24V is not within the specified limit, set the
        //!24V error
        if(m_usnSense24Voltage < DEFAULT_SYSTEM_24_VOLTS_LOWER_LIMIT || \
                m_usnSense24Voltage >= DEFAULT_SYSTEM_24_VOLTS_UPPER_LIMIT)
        {
            sprintf(DebugPrintBuf, "\r\n 24V Error: %ld\n", m_usnSense24Voltage);
            UartDebugPrint((int *)DebugPrintBuf, 50);

            utErrorInfoCheck.stErrorInfo.uln24V_Check = TRUE;

            //!Disable pressure monitoring
            utSysStatusMonEnInfo.stSysStatusMonEn.uc24VMon = FALSE;

            //!Frame the error command and abort the sequence
            SysErrorCommand(g_usnCurrentMode);
        }
        else
        {
            //!If the 24V is with in the specified limit, then clear the error
            utErrorInfoCheck.stErrorInfo.uln24V_Check = FALSE;
        }
    }
    else
    {
        if(m_usnSense24Voltage > DEFAULT_SYSTEM_24_VOLTS_HYST_LOWER_LIMIT && \
                m_usnSense24Voltage < DEFAULT_SYSTEM_24_VOLTS_HYST_UPPER_LIMIT)
        {
            sprintf(DebugPrintBuf, "\r\n 24V within limit: %ld\n", m_usnSense24Voltage);
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!If the temperature is with in the specified limit, then clear the error
            utErrorInfoCheck.stErrorInfo.uln24V_Check = FALSE;

            //!Send system status command to Sitara
            CIFormServiceHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);

            //!Enable 24V monitoring
            utSysStatusMonEnInfo.stSysStatusMonEn.uc24VMon = TRUE;
        }
    }
}

/******************************************************************************/
/*!
 * \fn void SysCheck_5V(void)
 * \brief   Function to check, is the 5V within the expected range
 * \return  void
 */
/******************************************************************************/
void SysCheck_5V(void)
{

    if(TRUE == utSysStatusMonEnInfo.stSysStatusMonEn.uc5VMon)
    {
        //!Check if the 5V is within the specified limit, if not set the
        //!5V error
        if(m_usnSense5Voltage <  DEFAULT_SYSTEM_5_VOLTS_LOWER_LIMIT || \
                m_usnSense5Voltage >= DEFAULT_SYSTEM_5_VOLTS_UPPER_LIMIT)
        {

            sprintf(DebugPrintBuf, "\r\n 5V Error: %ld\n", m_usnSense5Voltage);
            UartDebugPrint((int *)DebugPrintBuf, 50);

            utErrorInfoCheck.stErrorInfo.uln5V_Check = TRUE;

            //!Disable pressure monitoring
            utSysStatusMonEnInfo.stSysStatusMonEn.uc5VMon = FALSE;

            //!Frame the error command and abort the sequence
            SysErrorCommand(g_usnCurrentMode);
        }
        else
        {
            //!If the 5V is with in the specified limit, then clear the error
            utErrorInfoCheck.stErrorInfo.uln5V_Check = FALSE;
        }
    }
    else
    {
        if(m_usnSense5Voltage >  DEFAULT_SYSTEM_5_VOLTS_HYST_LOWER_LIMIT && \
                m_usnSense5Voltage < DEFAULT_SYSTEM_5_VOLTS_HYST_UPPER_LIMIT)
        {

            sprintf(DebugPrintBuf, "\r\n 5V within limit: %ld\n", m_usnSense5Voltage);
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!If the temperature is with in the specified limit, then clear the error
            utErrorInfoCheck.stErrorInfo.uln5V_Check = FALSE;

            //!Send system status command to Sitara
            CIFormServiceHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);

            //!Enable 24V monitoring
            utSysStatusMonEnInfo.stSysStatusMonEn.uc5VMon = TRUE;
        }

    }
}

/******************************************************************************/
/*!
 * \fn void MotorFaultCheck(void)
 * \brief   Function to check, Motor fault condition
 * \return  void
 */
/******************************************************************************/
void  MotorFaultCheck(void)
{
    if((TRUE == utErrorInfoCheck.stErrorInfo.ulnY_MotorFault) || \
            (TRUE == utErrorInfoCheck.stErrorInfo.ulnX_MotorFault) || \
            (TRUE == utErrorInfoCheck.stErrorInfo.ulnDiluent_MotorFault) || \
            (TRUE == utErrorInfoCheck.stErrorInfo.ulnSample_MotorFault) || \
            (TRUE == utErrorInfoCheck.stErrorInfo.ulnWaste_MotorFault))
    {
        //!Send Error Message to Processor
        SysErrorCommand(g_usnCurrentMode);
    }
}

/******************************************************************************/
/*!
 * \fn void PressureValveControl(void)
 * \brief   Function to control the value at the end of the sequence
 * \return  void
 */
/******************************************************************************/
void PressureValveControl(void)
{
    if(TRUE == m_bPressureValveOn)
    {
        //!Excite the V7 valve

        Uint16 usnValveStatus1 = ZERO;
        Uint16 usnValveStatus2 = ZERO;

        usnValveStatus1 = 0x0001;
        usnValveStatus2 = 0x0000;

        //!Excite the Corresponding Valve by sending the value to the valve SPI driver
        SIUpdateValveStatus(usnValveStatus1, usnValveStatus2);

        sprintf(DebugPrintBuf, "\rV7 Valve On\n");
        UartDebugPrint((int *)DebugPrintBuf, 50);

        m_bPressureValveOn = FALSE;
        m_ucPressureValveCounter = 250;

        m_bValveExcited = TRUE;
    }

    if(TRUE == m_bValveExcited)
    {
        if((WHOLE_BLOOD_MODE == g_usnCurrentMode) || (ZERO == g_usnCurrentMode))
        {
            m_ucPressureValveCounter--;
            if(0 >= m_ucPressureValveCounter)
            {
                //!Close the V7 valve
                SIUpdateValveStatus(ZERO, ZERO);
                m_bValveExcited = FALSE;
                sprintf(DebugPrintBuf, "\rV7 Valve Off\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        }
        else
        {
            //!Close the V7 valve
            SIUpdateValveStatus(ZERO, ZERO);

            m_bPressureValveOn = FALSE;
            m_ucPressureValveCounter = 250;

            m_bValveExcited = FALSE;
            sprintf(DebugPrintBuf, "\rV7 Valve Off\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
        }
    }
}
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************


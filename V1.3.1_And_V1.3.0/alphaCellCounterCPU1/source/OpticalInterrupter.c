/******************************************************************************/
/*! \file OpticalInterrupter.c
 *
 *  \brief GPIO interrupt for Home position sensor module
 *
 *  \b Description:
 *      Home position sensors are generating external interrupt
 *      when syring piston reaches the home position mark. Here, Home position
 *      interrupts are used to stop the motor operation
 *
 *   $Version: $ 0.1
 *   $Date:    $ May-12-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_optical_sensor_module
//! @{
//******************************************************************************
#include"OpticalInterrupter.h"
#include "MotorConfig.h"
#include"UartDebug.h"
#include<stdio.h>
#include<string.h>
#include"initGpio.h"

#ifdef IO_EXPANDER_USED
extern void PWMSpiHomePosSense(Uint16 );


/******************************************************************************/
/*!
 * \fn void OIInitSpiHomeSensor(void)
 * \brief SPI periheral initialization
 * \b Description:
 *      This function initializes SPI periheral registers
 *
 * \return void
 */
/******************************************************************************/
void OIInitSpiHomeSensor(void)
{
    IGConfigHomeSensCS(SPI_CS_HOME_SENSE, 1);
    DELAY_US(10000);

    //! Reset on, falling edge with delay, 8-bit char bits
    SpibRegs.SPICCR.all =0x0007;
    // Enable master mode, normal phase,
	SpibRegs.SPICTL.all =0x0006;

	// enable talk, and SPI int disabled.
	SpibRegs.SPISTS.all=0x0000;
	// LSPCLK/50    //1 MHz SCLK
	SpibRegs.SPIBRR.all = 0x0031;
	// LoopBack Disabled // Clock = 0
	SpibRegs.SPICCR.all =0x0087;
	// Set so breakpoints don't disturb xmission
	SpibRegs.SPIPRI.bit.FREE = 1;

	// Initialize the Spi FIFO
	OIInitFifoSpiHomeSensor();

	OIConfigSpiHomeSensor();
}
/******************************************************************************/
/*!
 * \fn void OIInitFifoSpiHomeSensor(void)
 * \brief SPI periheral FIFO initialization
 * \brief  Description:
 *      This function initializes SPI periheral FIFO registers
 *
 * \return void
 */
/******************************************************************************/
void OIInitFifoSpiHomeSensor(void)
{
// Initialize SPI FIFO registers
	SpibRegs.SPIFFTX.all=0xE040;
//	SpibRegs.SPIFFRX.all=0x2044;
	//data is transferred to SPITXBUF immediately upon completion
	//of transmission of the previous data
	SpibRegs.SPIFFCT.all=0x0000;
}
/******************************************************************************/
/*!
 * \fn Uint32 OIWriteDataSpiHomeSensorReg(Uint16 writeCmdData, Uint16 writeRegData)
 * \param writeData specifies the data to be transmitted to peripheral
 *
 * \brief SPI Write function
 * \brief Description:
 *      This function writes data to SPI transmit buffer
 *
 * \return void
 */
/******************************************************************************/
Uint32 OIWriteDataSpiHomeSensorReg(Uint16 writeCmdData, Uint16 writeRegData)
{

	Uint32 RxData=0x00;
	IGConfigHomeSensCS(SPI_CS_HOME_SENSE, 0);
	DELAY_US(5);

	SpibRegs.SPITXBUF = 0xFF00 & writeCmdData;
	while((SpibRegs.SPIFFRX.bit.RXFFST != 1));
	RxData = SpibRegs.SPIRXBUF;

	SpibRegs.SPITXBUF = (0xFF00 & (writeCmdData << 8));
	while((SpibRegs.SPIFFRX.bit.RXFFST != 1));
	RxData = SpibRegs.SPIRXBUF;

	SpibRegs.SPITXBUF = 0xFF00 & (writeRegData);
	while((SpibRegs.SPIFFRX.bit.RXFFST != 1));
	RxData = SpibRegs.SPIRXBUF;

	DELAY_US(5);
	IGConfigHomeSensCS(SPI_CS_HOME_SENSE, 1);
	DELAY_US(100);
	return RxData;
}
/******************************************************************************/
/*!
 * \fn Uint16 OIReadSpiHomeSensorReg(void)
 * \brief SPI Write function
 * \brief Description:
 *      This function read data to SPI receive buffer
 *
 * \return received data
 */
/******************************************************************************/
Uint16 OIReadSpiHomeSensorReg(Uint16 writeData)
{
	Uint16 RxData=0x00;

	IGConfigHomeSensCS(SPI_CS_HOME_SENSE, 0);
	DELAY_US(1);

	SpibRegs.SPITXBUF = 0xFF00 & (writeData);
	while((SpibRegs.SPIFFRX.bit.RXFFST != 1));
	RxData = SpibRegs.SPIRXBUF;

	SpibRegs.SPITXBUF = 0xFF00 & (writeData << 8);
	while((SpibRegs.SPIFFRX.bit.RXFFST != 1));
	RxData = SpibRegs.SPIRXBUF;

	SpibRegs.SPITXBUF = 0x0000;
	while((SpibRegs.SPIFFRX.bit.RXFFST != 1));
	RxData = SpibRegs.SPIRXBUF;

	DELAY_US(1);
	IGConfigHomeSensCS(SPI_CS_HOME_SENSE, 1);
	return (0xFFE0 | RxData);
}
/******************************************************************************/
/*!
 * \fn void OIConfigSpiHomeSensor(void)
 * \brief SPI slave device configuration
 * \brief Description:
 *      This function configures the spi slave device
 *
 * \return void
 */
/******************************************************************************/
static char statDebugPrintBuf[50];
void OIConfigSpiHomeSensor(void)
{
	Uint32 RxData=0;

#if 1

	RxData = OIReadSpiHomeSensorReg(READ_CMD_IOCON);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_IOCON 0x%04x 00 ",READ_CMD_IOCON);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Initial RxData 0x%04x ",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf("\r\n READ_CMD_IOCON 0x%04x 00 ",READ_CMD_IOCON);
	printf("  Initial RxData 0x%04x ",(Uint16)(RxData));

	RxData = OIWriteDataSpiHomeSensorReg(WRITE_CMD_IOCON, SPI_HOME_POS_CONFIG_IOCON);

	sprintf(statDebugPrintBuf, "\r\n WRITE_CMD_IOCON 0x%04x ",WRITE_CMD_IOCON);
	UartDebugPrint((int *)statDebugPrintBuf, 50);
	sprintf(statDebugPrintBuf, " SPI_HOME_POS_CONFIG_IOCON 0x%04x ",\
	        SPI_HOME_POS_CONFIG_IOCON);
	UartDebugPrint((int *)statDebugPrintBuf, 50);

	printf("\r\n WRITE_CMD_IOCON 0x%04x ",WRITE_CMD_IOCON);
	printf(" SPI_HOME_POS_CONFIG_IOCON 0x%04x ",SPI_HOME_POS_CONFIG_IOCON);

	RxData = OIReadSpiHomeSensorReg(READ_CMD_IOCON);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_IOCON 0x%04x 00 ",READ_CMD_IOCON);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Updated RxData 0x%04x \r\n",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf(" Updated RxData 0x%04x ",(Uint16)(RxData));
#endif

	RxData = OIReadSpiHomeSensorReg(READ_CMD_IODIR);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_IODIR 0x%04x 00 ",READ_CMD_IODIR);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Initial RxData 0x%04x ",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf("\n READ_CMD_IODIR 0x%04x 00 ",READ_CMD_IODIR);
	printf(" Initial RxData 0x%04x  ",(Uint16)(RxData));

	RxData = OIWriteDataSpiHomeSensorReg(WRITE_CMD_IODIR, SPI_HOME_POS_CONFIG_IODIR);

	sprintf(statDebugPrintBuf, "\r\n WRITE_CMD_IODIR 0x%04x ",WRITE_CMD_IODIR);
	UartDebugPrint((int *)statDebugPrintBuf, 50);
	sprintf(statDebugPrintBuf, " SPI_HOME_POS_CONFIG_IODIR 0x%04x ",\
	        SPI_HOME_POS_CONFIG_IODIR);
	UartDebugPrint((int *)statDebugPrintBuf, 50);

	printf("\r\n WRITE_CMD_IODIR 0x%04x ",WRITE_CMD_IODIR);
	printf(" SPI_HOME_POS_CONFIG_IODIR 0x%04x ",SPI_HOME_POS_CONFIG_IODIR);

	RxData = OIReadSpiHomeSensorReg(READ_CMD_IODIR);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_IODIR 0x%04x 00 ",READ_CMD_IODIR);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Updated RxData 0x%04x  \r\n",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf(" Updated RxData 0x%04x \n",(Uint16)(RxData));

//----------------------------------------------------------------------------//
	RxData = OIReadSpiHomeSensorReg(READ_CMD_IPOL);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_IPOL 0x%04x 00 ",READ_CMD_IPOL);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Initial RxData 0x%04x ",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf("\n READ_CMD_IPOL 0x%04x 00 ",READ_CMD_IPOL);
	printf(" Initial RxData 0x%04x ",(Uint16)(RxData));

	RxData = OIWriteDataSpiHomeSensorReg(WRITE_CMD_IPOL, SPI_HOME_POS_CONFIG_IPOL);

	sprintf(statDebugPrintBuf, "\r\n WRITE_CMD_IPOL 0x%04x ",WRITE_CMD_IPOL);
	UartDebugPrint((int *)statDebugPrintBuf, 50);
	sprintf(statDebugPrintBuf, " SPI_HOME_POS_CONFIG_IPOL 0x%04x ",\
	        SPI_HOME_POS_CONFIG_IPOL);
	UartDebugPrint((int *)statDebugPrintBuf, 50);

	printf("\r\n WRITE_CMD_IPOL 0x%04x ",WRITE_CMD_IPOL);
	printf(" SPI_HOME_POS_CONFIG_IPOL 0x%04x ",SPI_HOME_POS_CONFIG_IPOL);

	RxData = OIReadSpiHomeSensorReg(READ_CMD_IPOL);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_IPOL 0x%04x 00 ",READ_CMD_IPOL);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Updated RxData 0x%04x \r\n",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf(" Updated RxData 0x%04x  \r\n",(Uint16)(RxData));
//-----------------------------------------------------------------------------------------------

	RxData = OIReadSpiHomeSensorReg(READ_CMD_GPINTEN);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_GPINTEN 0x%04x 00 ",READ_CMD_GPINTEN);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Initial RxData 0x%04x ",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf("\r\n READ_CMD_GPINTEN 0x%04x 00 ",READ_CMD_GPINTEN);
	printf(" Initial RxData 0x%04x  ",(Uint16)(RxData));

	RxData = OIWriteDataSpiHomeSensorReg(WRITE_CMD_GPINTEN, SPI_HOME_POS_CONFIG_GPINTEN);

	sprintf(statDebugPrintBuf, "\r\n WRITE_CMD_GPINTEN 0x%04x ",WRITE_CMD_GPINTEN);
	UartDebugPrint((int *)statDebugPrintBuf, 50);
	sprintf(statDebugPrintBuf, " SPI_HOME_POS_CONFIG_GPINTEN 0x%04x ",\
	        SPI_HOME_POS_CONFIG_GPINTEN);
	UartDebugPrint((int *)statDebugPrintBuf, 50);

	printf("\r\n WRITE_CMD_GPINTEN 0x%04x ",WRITE_CMD_GPINTEN);
	printf(" SPI_HOME_POS_CONFIG_GPINTEN 0x%04x ",SPI_HOME_POS_CONFIG_GPINTEN);
	RxData = OIReadSpiHomeSensorReg(READ_CMD_GPINTEN);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_GPINTEN 0x%04x 00 ",READ_CMD_GPINTEN);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Updated RxData 0x%04x \r\n ",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf(" Updated RxData 0x%04x \r\n",(Uint16)(RxData));

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//

	RxData = OIReadSpiHomeSensorReg(READ_CMD_DEFVAL);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_DEFVAL 0x%04x 00 ",READ_CMD_DEFVAL);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Inititial RxData 0x%04x  ",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf("\n READ_CMD_DEFVAL 0x%04x 00 ",READ_CMD_DEFVAL);
	printf(" Initial RxData 0x%04x ",(Uint16)(RxData));

	RxData = OIWriteDataSpiHomeSensorReg(WRITE_CMD_DEFVAL, SPI_HOME_POS_CONFIG_DEFVAL);

	sprintf(statDebugPrintBuf, "\r\n WRITE_CMD_DEFVAL 0x%04x ",WRITE_CMD_DEFVAL);
	UartDebugPrint((int *)statDebugPrintBuf, 50);
	sprintf(statDebugPrintBuf, " SPI_HOME_POS_CONFIG_DEFVAL 0x%04x ",\
	        SPI_HOME_POS_CONFIG_DEFVAL);
	UartDebugPrint((int *)statDebugPrintBuf, 50);

	printf("\r\n WRITE_CMD_DEFVAL 0x%04x ",WRITE_CMD_DEFVAL);
	printf(" SPI_HOME_POS_CONFIG_DEFVAL 0x%04x ",SPI_HOME_POS_CONFIG_DEFVAL);
	RxData = OIReadSpiHomeSensorReg(READ_CMD_DEFVAL);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_DEFVAL 0x%04x 00 ",READ_CMD_DEFVAL);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Updated RxData 0x%04x  \r\n",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf(" Updated RxData 0x%04x  \r\n",(Uint16)(RxData));
//-------------------------------------------------------------------------------------------------//

	RxData = OIReadSpiHomeSensorReg(READ_CMD_INTCON);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_INTCON 0x%04x 00 ",READ_CMD_INTCON);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Initial RxData 0x%04x ",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf("\n READ_CMD_INTCON 0x%04x 00 ",READ_CMD_INTCON);
	printf(" Initial RxData 0x%04x ",(Uint16)(RxData));

	RxData = OIWriteDataSpiHomeSensorReg(WRITE_CMD_INTCON, SPI_HOME_POS_CONFIG_INTCON);

	sprintf(statDebugPrintBuf, "\r\n WRITE_CMD_INTCON 0x%04x ",WRITE_CMD_INTCON);
	UartDebugPrint((int *)statDebugPrintBuf, 50);
	sprintf(statDebugPrintBuf, " SPI_HOME_POS_CONFIG_INTCON 0x%04x ",\
	        SPI_HOME_POS_CONFIG_INTCON);
	UartDebugPrint((int *)statDebugPrintBuf, 50);

	printf("\r\n WRITE_CMD_INTCON 0x%04x ",WRITE_CMD_INTCON);
	printf(" SPI_HOME_POS_CONFIG_INTCON 0x%04x ",SPI_HOME_POS_CONFIG_INTCON);
	RxData = OIReadSpiHomeSensorReg(READ_CMD_INTCON);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_INTCON 0x%04x 00 ",READ_CMD_INTCON);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Updated RxData 0x%04x \r\n ",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf(" RxData 0x%04x  \n",(Uint16)(RxData));
//----------------------------------------------------------------------------//
	RxData = OIReadSpiHomeSensorReg(READ_CMD_GPPU);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_GPPU 0x%04x 00 ",READ_CMD_GPPU);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " Initial RxData 0x%04x  ",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf("\n READ_CMD_GPPU 0x%04x 00 ",READ_CMD_GPPU);
	printf(" Initial RxData 0x%04x ",(Uint16)(RxData));

	RxData = OIWriteDataSpiHomeSensorReg(WRITE_CMD_GPPU, SPI_HOME_POS_CONFIG_GPPU);

	sprintf(statDebugPrintBuf, "\r\n WRITE_CMD_GPPU 0x%04x ",WRITE_CMD_GPPU);
	UartDebugPrint((int *)statDebugPrintBuf, 50);
	sprintf(statDebugPrintBuf, " SPI_HOME_POS_CONFIG_GPPU 0x%04x ",\
	        SPI_HOME_POS_CONFIG_GPPU);
	UartDebugPrint((int *)statDebugPrintBuf, 50);

	printf("\r\n WRITE_CMD_GPPU 0x%04x ",WRITE_CMD_GPPU);
	printf(" SPI_HOME_POS_CONFIG_GPPU 0x%04x ",SPI_HOME_POS_CONFIG_GPPU);

	RxData = OIReadSpiHomeSensorReg(READ_CMD_GPPU);

	sprintf(statDebugPrintBuf, "\r\n READ_CMD_GPPU 0x%04x 00 ",READ_CMD_GPPU);
	UartDebugPrint((int *)statDebugPrintBuf, 40);
	sprintf(statDebugPrintBuf, " RxData 0x%04x \r\n",(Uint16)(RxData));
	UartDebugPrint((int *)statDebugPrintBuf, 40);

	printf(" Updated RxData 0x%04x \r\n",(Uint16)(RxData));
}
/******************************************************************************/
/*!
 * \fn void OIInitialize_EXTINT(void)
 * \brief Configure GPIO as external interrupt
 * \b Description:
 *      This function configures GPIO's as external interrupts
 *
 * \return void
 */
/******************************************************************************/
void OIInitialize_EXTINT(void)
{
	// Interrupts that are used in this example are re-mapped to
	// ISR functions found within this file.
	EALLOW;	// This is needed to write to EALLOW protected registers
	PieVectTable.XINT1_INT = &xint1_isr;	//For Volumetric RBC-A sensor
	PieVectTable.XINT2_INT = &xint2_isr;	//For Volumetric RBC-A sensor
	PieVectTable.XINT3_INT = &xint3_isr;	//For Volumetric RBC-A sensor
	PieVectTable.XINT4_INT = &xint4_isr;	//For Volumetric RBC-A sensor
//	PieVectTable.XINT5_INT = &xint5_isr;	// To start communicate with SITARA..tbd
	EDIS;   // This is needed to disable write to EALLOW protected registers

	// Enable XINT1 and XINT2 in the PIE: Group 1 interrupt 4 & 5
	// Enable INT1 which is connected to WAKEINT:
	PieCtrlRegs.PIECTRL.bit.ENPIE = 1;          // Enable the PIE block
	PieCtrlRegs.PIEIER12.bit.INTx3 = 1;          // Enable PIE Gropu 14 INT5

	IER |= M_INT12;                              // Enable CPU INT12
	EINT;                                       // Enable Global Interrupts


}
/******************************************************************************/
/*!
 * \fn interrupt void xint1_isr(void)
 * \brief external interrupt 1
 * \brief  Description:
 *      interrupt handler of Motor1 Sensor
 *
 * \return void
 */
/******************************************************************************/
interrupt void xint1_isr(void)
{
//	Motor1SensorISR();
    // Acknowledge this interrupt to receive more interrupts from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
/******************************************************************************/
/*!
 * \fn interrupt void xint2_isr(void)
 * \brief external interrupt 2
 * \brief  Description:
 *      interrupt handler of Motor2 Sensor
 *
 * \return void
 */
/******************************************************************************/
interrupt void xint2_isr(void)
{
//	Motor2SensorISR();
    // Acknowledge this interrupt to receive more interrupts from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
/******************************************************************************/
/*!
 * \fn interrupt void xint3_isr(void)
 * \brief external interrupt 3
 * \brief  Description:
 *      interrupt handler of Motor3 Sensor
 *
 * \return void
 */
/******************************************************************************/
interrupt void xint3_isr(void)
{
//	Motor3SensorISR();
    // Acknowledge this interrupt to receive more interrupts from group 12
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
}
/******************************************************************************/
/*!
 * \fn interrupt void xint4_isr(void)
 * \brief external interrupt 4
 * \brief  Description:
 *      interrupt handler of Motor4 Sensor
 *
 * \return void
 */
/******************************************************************************/
interrupt void xint4_isr(void)
{
//	Motor4SensorISR();
	// Acknowledge this interrupt to receive more interrupts from group 12
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
}
/******************************************************************************/
/*!
 * \fn interrupt void xint5_isr(void)
 * \brief external interrupt 5
 * \brief  Description:
 *      interrupt handler of Motor5 Sensor
 *
 * \return void
 */
/******************************************************************************/
interrupt void xint5_isr(void)
{
//	Motor5SensorISR();
	PWMSpiHomePosSense((OIReadSpiHomeSensorReg(READ_CMD_GPIO)));
	// Acknowledge this interrupt to receive more interrupts from group 12
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
}
#endif
/******************************************************************************/
/*!
 * \fn void OIDisableCpu1_EXTINT(void)
 * \brief Configure to disable external interrupt
 * \b Description:
 *      This function configures GPIO's as external interrupts
 *
 * \return void
 */
/******************************************************************************/
void OIDisableCpu1_EXTINT(void)
{
	// Enable INT1 which is connected to WAKEINT:
    //! Disable the PIE block
	PieCtrlRegs.PIECTRL.bit.ENPIE = 0;
	//! Disable PIE Gropu 14 INT5
	PieCtrlRegs.PIEIER12.bit.INTx3 = 0;

	//! Disable CPU INT12
	IER |= (IER & ~M_INT12);

	//! Disable XINT5
	XintRegs.XINT5CR.bit.ENABLE = 0;
}
/******************************************************************************/
/*!
 * \fn void OIInit_GPIO_EXTINT(void)
 * \brief Configure GPIO's pinmux
 * \brief  Description:
 *      This function configures GPIO's direction & values in pinpux
 *
 * \return void
 */
/******************************************************************************/
void OIInit_GPIO_EXTINT(void)
{
#ifdef _ALPHA_2_BOARD_
	GpioPinConfig GpioSetupVolumetricSensorGpio[4];

    //!Set the following parameters of the volumetric interrupt GPIO pin:
    //!GPIO pin no. to GPIO_VOL1
    //!GPIO MUX select to GPIO_MUX_CPU2 to control from CPU2
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as low
	GpioSetupVolumetricSensorGpio[0].GpioPin = GPIO_VOL1;
	GpioSetupVolumetricSensorGpio[0].GpioCpuMuxSel = GPIO_MUX_CPU2;
	GpioSetupVolumetricSensorGpio[0].GpioInputOutput = GPIO_INPUT;
	GpioSetupVolumetricSensorGpio[0].GpioFlags = GPIO_QUAL6;
	GpioSetupVolumetricSensorGpio[0].GpioPeripheral = 0;

    //!Set the following parameters of the volumetric interrupt GPIO pin:
    //!GPIO pin no. to GPIO_VOL2
    //!GPIO MUX select to GPIO_MUX_CPU2 to control from CPU2
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as low
	GpioSetupVolumetricSensorGpio[1].GpioPin = GPIO_VOL2;
	GpioSetupVolumetricSensorGpio[1].GpioCpuMuxSel = GPIO_MUX_CPU2;
	GpioSetupVolumetricSensorGpio[1].GpioInputOutput = GPIO_INPUT;
	GpioSetupVolumetricSensorGpio[1].GpioFlags = GPIO_QUAL6;
	GpioSetupVolumetricSensorGpio[1].GpioPeripheral = 0;

    //!Set the following parameters of the volumetric interrupt GPIO pin:
    //!GPIO pin no. to GPIO_VOL3
    //!GPIO MUX select to GPIO_MUX_CPU2 to control from CPU2
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as low
	GpioSetupVolumetricSensorGpio[2].GpioPin = GPIO_VOL3;
	GpioSetupVolumetricSensorGpio[2].GpioCpuMuxSel = GPIO_MUX_CPU2;
	GpioSetupVolumetricSensorGpio[2].GpioInputOutput = GPIO_INPUT;
	GpioSetupVolumetricSensorGpio[2].GpioFlags = GPIO_QUAL6;
	GpioSetupVolumetricSensorGpio[2].GpioPeripheral = 0;

    //!Set the following parameters of the volumetric interrupt GPIO pin:
    //!GPIO pin no. to GPIO_VOL4
    //!GPIO MUX select to GPIO_MUX_CPU2 to control from CPU2
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as low
	GpioSetupVolumetricSensorGpio[3].GpioPin = GPIO_VOL4;
	GpioSetupVolumetricSensorGpio[3].GpioCpuMuxSel = GPIO_MUX_CPU2;
	GpioSetupVolumetricSensorGpio[3].GpioInputOutput = GPIO_INPUT;
	GpioSetupVolumetricSensorGpio[3].GpioFlags = GPIO_QUAL6;
	GpioSetupVolumetricSensorGpio[3].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the volumetric interrupt GPIO
	IGConfigGpioPin(GpioSetupVolumetricSensorGpio[0]);
	IGConfigGpioPin(GpioSetupVolumetricSensorGpio[1]);
	IGConfigGpioPin(GpioSetupVolumetricSensorGpio[2]);
	IGConfigGpioPin(GpioSetupVolumetricSensorGpio[3]);

	EALLOW;
	//! Each sampling window is 510*SYSCLKOUT
	GpioCtrlRegs.GPDCTRL.bit.QUALPRD1 = 0xFF;
	EDIS;

	//!External interrupt for the volumetric tubes for WBC Start and STOP,
	//! RBC_PLT Start and STOP
	GPIO_SetupXINT1Gpio(GPIO_VOL1);
	GPIO_SetupXINT2Gpio(GPIO_VOL2);
	GPIO_SetupXINT3Gpio(GPIO_VOL3);
	GPIO_SetupXINT4Gpio(GPIO_VOL4);
	//GPIO_SetupXINT4Gpio(GPIO_VOL5);

	//GPIO_WritePin(GPIO_VOL1,1);
	//GPIO_WritePin(GPIO_VOL2,1);
	//GPIO_WritePin(GPIO_VOL3,1);
	//GPIO_WritePin(GPIO_VOL4,1);
	//GPIO_WritePin(GPIO_VOL4,1);
#endif
}
/******************************************************************************/
/*!
 * \fn unsigned int OIHomeSensor1State(void)
 *
 * \brief Reads Sensor 1 state
 * \brief  Description:
 *      This function returns the sensor 1 status
 *
 * \return unsigned int of Sensor1State
 */
/******************************************************************************/
unsigned int OIHomeSensor1State(void)
{
#if HOME_SENSORS_ENABLE
    //!Read the GPIO pin status of Home position 1
       return (!((unsigned int)GPIO_ReadPin(GPIO_HOME_POS1)));   //-->For New HPS
#else
       return ((unsigned int)GPIO_ReadPin(GPIO_HOME_POS1));
		//return !SENSOR1_HOME_STATE;
#endif
}
/******************************************************************************/
/*!
 * \fn unsigned int OIHomeSensor2State(void)
  *
 * \brief Reads Sensor 2 state
 * \brief  Description:
 *      This function returns the sensor 2 status
 *
 * \return unsigned int of Sensor2State
 */
/******************************************************************************/
unsigned int OIHomeSensor2State(void)
{
#if HOME_SENSORS_ENABLE
    //!Read the GPIO pin status of Home position 2
       return (!((unsigned int)GPIO_ReadPin(GPIO_HOME_POS2)));    //-->For New HPS
#else
       return ((unsigned int)GPIO_ReadPin(GPIO_HOME_POS2));
		//return !SENSOR2_HOME_STATE;
#endif
}
/******************************************************************************/
/*!
 * \fn unsigned int OIHomeSensor3State(void)
  *
 * \brief Reads Sensor 3 state
 * \brief  Description:
 *      This function returns the sensor 3 status
 *
 * \return unsigned int of Sensor3State
 */
/******************************************************************************/
unsigned int OIHomeSensor3State(void)
{
#if HOME_SENSORS_ENABLE
    //!Read the GPIO pin status of Home position 3
        return (!((unsigned int)GPIO_ReadPin(GPIO_HOME_POS3))); //-->For New HPS
#else
        return ((unsigned int)GPIO_ReadPin(GPIO_HOME_POS3));
		//return !SENSOR3_HOME_STATE;
#endif
}
/******************************************************************************/
/*!
 * \fn unsigned int OIHomeSensor4State(void)
  * \brief Reads Sensor 4 state
 * \brief  Description:
 *      This function returns the sensor 4 status
 *
 * \return unsigned int of Sensor4State
 */
/******************************************************************************/
unsigned int OIHomeSensor4State(void)
{
#if HOME_SENSORS_ENABLE
    //!Read the GPIO pin status of Home position 4
        return (!((unsigned int)GPIO_ReadPin(GPIO_HOME_POS4)));  //-->For New HPS
#else
        return ((unsigned int)GPIO_ReadPin(GPIO_HOME_POS4));
		//return !SENSOR4_HOME_STATE;
#endif
}
/******************************************************************************/
/*!
 * \fn unsigned int OIHomeSensor5State(void)
  * \brief Reads Sensor 5 state
 * \brief  Description:
 *      This function returns the sensor 5 status
 *
 * \return unsigned int of Sensor5State
 */
/******************************************************************************/
unsigned int OIHomeSensor5State(void)
{
#if HOME_SENSORS_ENABLE
    //!Read the GPIO pin status of Home position 5
       return (!((unsigned int)GPIO_ReadPin(GPIO_HOME_POS5)));   //-->For New HPS
#else
       return ((unsigned int)GPIO_ReadPin(GPIO_HOME_POS5));
		//return !SENSOR5_HOME_STATE;
#endif
}
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

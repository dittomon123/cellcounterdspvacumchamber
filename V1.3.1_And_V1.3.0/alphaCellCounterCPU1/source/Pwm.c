/********************************************************************************/
/*! \file Pwm.c
 *
 *  \brief Pwm initialization and control
 *
 *  \b Description:
 *      PWM is used to operate motor. For motorn Steps are generated from PWM
 *      and start & Stop shall be done using PWM operation.
 *      Pwm frequency is variable input from motor control module and s-curve
 *      algorith is implemented during START & STOP of motor operation
 *
 *   $Version: $ 0.1
 *   $Date:    $ Apr-09-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_pwm_module
//! @{
//*****************************************************************************

#include "Pwm.h"
#include "MotorConfig.h"
#include "math.h"
#include "InitGpio.h"
#include "UartDebug.h"
#include <stdio.h>
#include<stdlib.h>
#include<string.h>

#define ELEVEN          11;
/******************************************************************************/
/*!
 * \struct EPWM_REGS
 * \brief *ePWM[5] = {&EPwm1Regs,&EPwm2Regs,&EPwm5Regs,&EPwm6Regs,&EPwm8Regs}
 */
/******************************************************************************/
volatile struct EPWM_REGS *ePWM[5] =
             { &EPwm1Regs, &EPwm2Regs, &EPwm5Regs, &EPwm6Regs, &EPwm8Regs};


extern bool_t g_bYMotorFaultMonEn;
extern bool_t g_bXMotorFaultMonEn;
extern bool_t g_bDilMotorFaultMonEn;
extern bool_t g_bWasteMotorFaultMonEn;
extern bool_t g_bSampleMotorFaultMonEn;
extern char DebugPrintBuf[50];
extern Uint32 g_unSequenceStateTime;
extern uint32_t m_usnPressureAdc;
extern bool_t  g_bCountTimeVacuumMoni;
extern bool_t  g_bTubeCleanVacuumMoni;
extern bool_t m_bBubblingMovEnFlag;
extern bool_t g_bFirstMixingState;   //HN_added
extern bool_t g_bLyseMixingState;    //HN_added
extern bool_t g_bPreDilMixingState;  //HN_added
extern uint16_t g_DiluentMixTime;  //HN_added
extern uint16_t g_LyseMixTime;     //HN_added
extern uint16_t g_RBCMixTime;
extern uint16_t MixingTime;
extern uint32_t sncalZeroPSI;
extern bool_t g_bLowVolumeDisp;
extern uint16_t g_TestDataI0;
extern uint16_t bMStopADCPressure;
/******************************************************************************/
/*!
 *
 * \var Motor1Pwm8
 * \var Motor2Pwm6
 * \var Motor3Pwm2
 * \var Motor4Pwm5
 * \var Motor5Pwm1
 * \brief RAM DATA_SECTION memroy is mapped to variables
 */
/******************************************************************************/
#pragma DATA_SECTION(Motor1Pwm8,".RAMGS6_PwmParams");
#pragma DATA_SECTION(Motor2Pwm6,".RAMGS6_PwmParams");
#pragma DATA_SECTION(Motor3Pwm2,".RAMGS6_PwmParams");
#pragma DATA_SECTION(Motor4Pwm5,".RAMGS6_PwmParams");
#pragma DATA_SECTION(Motor5Pwm1,".RAMGS6_PwmParams");

/******************************************************************************/
/*!
 * \struct Motor1Pwm8
 * \struct Motor2Pwm6
 * \struct Motor3Pwm2
 * \struct Motor4Pwm5
 * \struct Motor5Pwm1
 * \brief structure stPwmParameter
 */
/******************************************************************************/
static stPwmParameter Motor1Pwm8;
static stPwmParameter Motor2Pwm6;
static stPwmParameter Motor3Pwm2;
static stPwmParameter Motor4Pwm5;
static stPwmParameter Motor5Pwm1;

bool_t m_bX_Home_Movement = false;
bool_t m_bY_Home_Movement = false;
bool_t m_bDiluent_Home_Movement = false;
bool_t m_bWaste_Home_Movement = false;
bool_t m_bSample_Home_Movement = false;
uint16_t Counter = 0;   //HN added
#if 1

/******************************************************************************/
/*!
 * \fn stPwmParameter* PWMGetMotor1Pwm8(void)
 * \brief To get Structure address of Motor1Pwm8
 * \brief  Description:
 *      This function returns the address of Motor1Pwm8
 *
 * \return &Motor1Pwm8
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor1Pwm8(void)
{
    //!Returns the Motor 1 PWM 8 parameter address
	return &Motor1Pwm8;
}
/******************************************************************************/
/*!
 * \fn stPwmParameter* PWMGetMotor2Pwm6(void)
 * \brief To get Structure address of Motor2Pwm6
 * \brief  Description:
 *      This function returns the address of Motor2Pwm6
 *
 * \return &Motor2Pwm6
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor2Pwm6(void)
{
    //!Returns the Motor 2 PWM 6 parameter address
	return &Motor2Pwm6;
}
/******************************************************************************/
/*!
 * \fn stPwmParameter* PWMGetMotor3Pwm2(void)
 * \brief To get Structure address of Motor3Pwm2
 * \brief  Description:
 *      This function returns the address of Motor3Pwm2
 *
 * \return &Motor3Pwm2
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor3Pwm2(void)
{
    //!Returns the Motor 3 PWM 2 parameter address
	return &Motor3Pwm2;
}
/******************************************************************************/
/*!
 * \fn stPwmParameter* PWMGetMotor4Pwm5(void)
 * \brief To get Structure address of Motor4Pwm5
 * \brief  Description:
 *      This function returns the address of Motor4Pwm5
 *
 * \return &Motor4Pwm5
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor4Pwm5(void)
{
    //!Returns the Motor 4 PWM 5 parameter address
	return &Motor4Pwm5;
}
/******************************************************************************/
/*!
 * \fn stPwmParameter* PWMGetMotor5Pwm1(void)
 * \brief To get Structure address of Motor5Pwm1
 * \brief  Description:
 *      This function returns the address of Motor5Pwm1
 *
 * \return &Motor5Pwm1
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor5Pwm1(void)
{
    //!Returns the Motor 5 PWM 1 parameter address
	return &Motor5Pwm1;
}
#endif
/******************************************************************************/
/*!
 * \fn void PWMInitialize(void)
 * \brief Initialize 5 pwm engines for 5 stepper motor
 * \brief  Description:
 *      This function initialize the pwm isr and intervector table
 *      Configures pwm engine and period
 *		Configures all ePWM channels and sets up PWM
 * \return void
 */
/******************************************************************************/
void PWMInitialize(void)
{
    //!Initialize PWM1 GPIOs
	InitEPwm1Gpio();
    //!Initialize PWM2 GPIOs
	InitEPwm2Gpio();
    //!Initialize PWM5 GPIOs
	InitEPwm5Gpio();
    //!Initialize PWM6 GPIOs
	InitEPwm6Gpio();
    //!Initialize PWM8 GPIOs
	InitEPwm8Gpio();

	//!Interrupts that are used in this example are re-mapped to
	//!ISR functions found within this file.
	EALLOW; //!This is needed to write to EALLOW protected registers
	PieVectTable.EPWM1_INT = &epwm1_isr;
	PieVectTable.EPWM2_INT = &epwm2_isr;
	PieVectTable.EPWM5_INT = &epwm5_isr;

	PieVectTable.EPWM6_INT = &epwm6_isr;
	PieVectTable.EPWM8_INT = &epwm8_isr;
	EDIS;   // This is needed to disable write to EALLOW protected registers
	EALLOW;
	CpuSysRegs.PCLKCR0.bit.TBCLKSYNC =0;

	EDIS;

    //!Configure PWM6 for the perios MOTOR_PWM_PERIOD
	PWMConfig(PWM6A, MOTOR_PWM_PERIOD);
	//!Configure PWM8 for the perios MOTOR_PWM_PERIOD
	PWMConfig(PWM8A, MOTOR_PWM_PERIOD);
	//!Configure PWM1 for the perios MOTOR_PWM_PERIOD
	PWMConfig(PWM1A, MOTOR_PWM_PERIOD);
	//!Configure PWM2 for the perios MOTOR_PWM_PERIOD
	PWMConfig(PWM2A, MOTOR_PWM_PERIOD);
	//!Configure PWM5 for the perios MOTOR_PWM_PERIOD
	PWMConfig(PWM5A, MOTOR_PWM_PERIOD);

	EALLOW;
	CpuSysRegs.PCLKCR0.bit.TBCLKSYNC =1;
	EDIS;

	//!Set the PWM Default parameters
	PWMParamDefaultState();

	//!Enable CPU INT3 which is connected to EPWM1-3 INT:
	IER |= M_INT3;

	//!Enable EPWM INTn in the PIE: Group 3 interrupt 1-3
	PieCtrlRegs.PIEIER3.bit.INTx1 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx2 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx5 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx6 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx8 = 1;

	//!Enable global Interrupts and higher priority real-time debug events:
	EINT;  //!Enable Global interrupt INTM
	ERTM;  //!Enable Global realtime interrupt DBGM
}
/******************************************************************************/
/*!
 * \fn void PWMParamDefaultState(void)
 * \brief Initialize pwm variables
 * \brief  Description:
 *      This function initialize the pwm variables to default state
 *      Configures pwm engine and period
 *		Configures all ePWM channels and sets up PWM
 * \return void
 */
/******************************************************************************/
void PWMParamDefaultState(void)
{
    //!Set all the motor direction to IDLE
	Motor5Pwm1.Direction = IDLE;
	Motor4Pwm5.Direction = IDLE;
	Motor3Pwm2.Direction = IDLE;
	Motor2Pwm6.Direction = IDLE;
	Motor1Pwm8.Direction = IDLE;

	//!Set all the motor status to IDLE
	Motor5Pwm1.Status = IDLE;
	Motor4Pwm5.Status = IDLE;
	Motor3Pwm2.Status = IDLE;
	Motor2Pwm6.Status = IDLE;
	Motor1Pwm8.Status = IDLE;

	//!Set the timer counter of all the motors to Zero
	Motor5Pwm1.TimerCounter = 0;
	Motor4Pwm5.TimerCounter = 0;
	Motor3Pwm2.TimerCounter = 0;
	Motor2Pwm6.TimerCounter = 0;
	Motor1Pwm8.TimerCounter = 0;

	//!Set the time stamp 0 of all the motors to Zero
	Motor5Pwm1.TimeStamp[0] = 0;
	Motor4Pwm5.TimeStamp[0] = 0;
	Motor3Pwm2.TimeStamp[0] = 0;
	Motor2Pwm6.TimeStamp[0] = 0;
	Motor1Pwm8.TimeStamp[0] = 0;

	//!Set the time stamp 1 of all the motors to Zero
	Motor5Pwm1.TimeStamp[1] = 0;
	Motor4Pwm5.TimeStamp[1] = 0;
	Motor3Pwm2.TimeStamp[1] = 0;
	Motor2Pwm6.TimeStamp[1] = 0;
	Motor1Pwm8.TimeStamp[1] = 0;

	//!Set the frequency of all the motors
	Motor5Pwm1.uiMotorMinFreq = MIN_MOTOR5_FREQUENCY;
	Motor4Pwm5.uiMotorMinFreq = MIN_MOTOR4_FREQUENCY;
	Motor3Pwm2.uiMotorMinFreq = MIN_MOTOR3_FREQUENCY;
	Motor2Pwm6.uiMotorMinFreq = MIN_MOTOR2_FREQUENCY;
	Motor1Pwm8.uiMotorMinFreq = MIN_MOTOR1_FREQUENCY;

	//!Set the motor position of all the motors
    Motor5Pwm1.iMotorPosition = 0;
    Motor4Pwm5.iMotorPosition = 0;
    Motor3Pwm2.iMotorPosition = 0;
    Motor2Pwm6.iMotorPosition = 0;
    Motor1Pwm8.iMotorPosition = 0;
}
/******************************************************************************/
/*!
 * \fn void PWMInitHgbPwm(void)
 *
 * \brief Initialize individual pwm channels
 * \brief  Description:
 *      This function initialize the pwm channel registers
 *      Configures pwm engine and period
 *		Configures all ePWM channels and sets up PWM
 * \return void
 */
/******************************************************************************/
void PWMInitHgbPwm(void)
{
	Uint16 period = HGB_PWM_PERIOD_FACTOR/HGB_PWM_FREQUENCY;
	EALLOW;
	CpuSysRegs.PCLKCR0.bit.TBCLKSYNC =0;
	EDIS;
	//!set Immediate load TB_IMMEDIATE
	EPwm7Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	//!PWM frequency = 1 / period
	EPwm7Regs.TBPRD = (period-1);
	//!set duty 10% initially
	EPwm7Regs.CMPA.bit.CMPA = (period/10);

	//!Set PWM 7 Phase register
	EPwm7Regs.TBPHS.all = 0;
	//!Set PWM 7 Time base control register
	EPwm7Regs.TBCTR = 0;

	//!Set PWM 7 Time base control register
	EPwm7Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;//TB_COUNT_UP;
	EPwm7Regs.TBCTL.bit.PHSEN = TB_DISABLE;
	EPwm7Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;

	EPwm7Regs.TBCTL.bit.HSPCLKDIV = PWM_TB_DIV10;
	EPwm7Regs.TBCTL.bit.CLKDIV =  PWM_TB_DIV8;
	//EPwm7Regs.TBCTL.bit.FREE_SOFT = 11;
	EPwm7Regs.TBCTL.bit.FREE_SOFT = ELEVEN;   //QA_C


	//!Set PWM 7 Counter Compare control register
	EPwm7Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	EPwm7Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;
	EPwm7Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm7Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

	//!Set PWM 7 Action Qualifier control register
	EPwm7Regs.AQCTLA.bit.ZRO = AQ_SET;
	EPwm7Regs.AQCTLA.bit.CAU = AQ_CLEAR;
	EALLOW;
	CpuSysRegs.PCLKCR0.bit.TBCLKSYNC =1;
	EDIS;
	//!Stop the PWM of the HGB
	PWMHgbPwmControl(STOP, HGB_PWM_FREQUENCY);
}
/******************************************************************************/
/*!
 * \fn void PWMHgbPwmControl(Uint16 State, Uint16 frequency)
 * \param State pwm state whether RUN or STOP
 * \param frequency pwm period to be provided
 *
 * \brief Controls individual pwm channels
 * \brief  Description:
 *      This function control the pwm channel
 *      Configures pwm period & duty cycle
 *
 * \return void
 */
/******************************************************************************/
void PWMHgbPwmControl(Uint16 State, Uint16 frequency)
{
	Uint16 period = HGB_PWM_PERIOD_FACTOR/HGB_PWM_FREQUENCY;
	//!Set the period of the HGB
	if(frequency > 50 && frequency < 1000)
	{
		period = HGB_PWM_PERIOD_FACTOR/frequency;
	}

	//!If the state is start, set the duty cycle of the PWM and start the PWM,
	//!else reset the duty cycle to zero and stop the PWM
    if(State == START)
    {
        EPwm7Regs.TBPRD = period-1;
        EPwm7Regs.CMPA.bit.CMPA = period/2;       // set duty 50%
        EPwm7Regs.AQCTLA.bit.ZRO = AQ_SET;        // PWM toggle high/low
        EPwm7Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    }
    else
    {
        EPwm7Regs.CMPA.bit.CMPA = DUTY_PERCENT_0;    // set duty 0%
        EPwm7Regs.AQCTLA.bit.ZRO = AQ_CLEAR;         // PWM toggle high/low
        EPwm7Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    }
}
/******************************************************************************/
/*!
 * \fn void PWMConfig(Uint16 Channel, Uint16 period)
 * \param Channel pwm channel to be configured
 * \param  period specifies the pwm period
 *
 * \brief Initialize individual pwm channels
 * \brief  Description:
 *      This function initialize the pwm channel registers
 *      Configures pwm engine and period
 *		Configures all ePWM channels and sets up PWM
 * \return void
 */
/******************************************************************************/
void PWMConfig(Uint16 Channel, Uint16 period)
{
    //!set Immediate load
    //(*ePWM[Channel]).TBCTL.bit.PRDLD = TB_SHADOW;
    //!set Immediate load TB_IMMEDIATE
	(*ePWM[Channel]).TBCTL.bit.PRDLD = TB_SHADOW;
	//!PWM frequency = 1 / period
	(*ePWM[Channel]).TBPRD = period-1;
	//!set duty 0% initially
	//(*ePWM[Channel]).CMPA.bit.CMPA = period / 2;
	//set duty 0% initially
	(*ePWM[Channel]).CMPA.bit.CMPA = DUTY_PERCENT_0;
	//!Set PWM Phase register
	(*ePWM[Channel]).TBPHS.all = 0;
	//!Set PWM Time base control register
	(*ePWM[Channel]).TBCTR = 0;

	//!Set PWM Time base control register
	(*ePWM[Channel]).TBCTL.bit.CTRMODE = TB_COUNT_UP;//TB_COUNT_UP;
	(*ePWM[Channel]).TBCTL.bit.PHSEN = TB_DISABLE;
	(*ePWM[Channel]).TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;

	(*ePWM[Channel]).TBCTL.bit.HSPCLKDIV = PWM_TB_DIV10;
	(*ePWM[Channel]).TBCTL.bit.CLKDIV =  PWM_TB_DIV8;
	//(*ePWM[Channel]).TBCTL.bit.FREE_SOFT = 11;
	(*ePWM[Channel]).TBCTL.bit.FREE_SOFT = ELEVEN;   //QA_C

	//!Set PWM Counter Compare control register
	(*ePWM[Channel]).CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
	(*ePWM[Channel]).CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;
	(*ePWM[Channel]).CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	(*ePWM[Channel]).CMPCTL.bit.SHDWBMODE = CC_SHADOW;

	// PWM toggle high/low
	//!Set PWM 7 Action Qualifier control register
	(*ePWM[Channel]).AQCTLA.bit.ZRO = AQ_SET;
	(*ePWM[Channel]).AQCTLA.bit.CAU = AQ_CLEAR;

	//!Interrupt where we will change the Compare Values
	//!Select INT on Zero event
	(*ePWM[Channel]).ETSEL.bit.INTSEL = ET_CTR_ZERO;
	(*ePWM[Channel]).ETSEL.bit.INTEN = 1;                // Enable INT
	//ET_3RD;           // Generate INT on 3rd event
	(*ePWM[Channel]).ETPS.bit.INTPRD = ET_1ST;

}
/******************************************************************************/
/*!
 * \fn void PWMControl(Uint16 Channel, Uint16 State, Uint16 period)
 * \param Channel pwm channel to be configured
 * \param State pwm state whether RUN or STOP
 * \param period pwm period to be provided
 *
 * \brief Controls individual pwm channels
 * \brief  Description:
 *      This function control the pwm channel
 *      Configures pwm period & duty cycle
 *
 * \return void
 */
/******************************************************************************/
void PWMControl(Uint16 Channel, Uint16 State, Uint16 period)
{
    //!If the state is start, set the duty cycle of the PWM and start the PWM,
    //!else reset the duty cycle to zero and stop the PWM
    if(State == START)
    {
        (*ePWM[Channel]).TBPRD = period-1;
        //!set duty 50%
        (*ePWM[Channel]).CMPA.bit.CMPA = (Uint16)(period*0.7);
        //!PWM toggle high/low
        (*ePWM[Channel]).AQCTLA.bit.ZRO = AQ_SET;
        (*ePWM[Channel]).AQCTLA.bit.CAU = AQ_CLEAR;
        //!Enable INT
        (*ePWM[Channel]).ETSEL.bit.INTEN = 1;
    }
    else
    {
        //!Disable INT
        (*ePWM[Channel]).ETSEL.bit.INTEN = 0;
        //!set duty 0%
        (*ePWM[Channel]).CMPA.bit.CMPA = DUTY_PERCENT_0;
        //!PWM toggle high/low
        (*ePWM[Channel]).AQCTLA.bit.ZRO = AQ_CLEAR;
        (*ePWM[Channel]).AQCTLA.bit.CAU = AQ_CLEAR;
    }
}
/******************************************************************************/
/*!
 * \fn void PWM1FreqControl(Uint16 period)
 * \param period pwm period to be provided
 *
 * \brief Controls individual pwm frequency
 * \brief  Description:
 *      This function control the pwm channel
 *      Configures pwm period & duty cycle
 *
 * \return void
 */
/******************************************************************************/
void PWM1FreqControl(Uint16 period)
{
    //!Control the PWM 1 by setting the Time period and the duty cycle
	(*ePWM[0]).TBPRD = period-1;
	(*ePWM[0]).CMPA.bit.CMPA = (Uint16)(period*0.7);       // set duty 70%
	(*ePWM[0]).AQCTLA.bit.ZRO = AQ_SET;               // PWM toggle high/low
	(*ePWM[0]).AQCTLA.bit.CAU = AQ_CLEAR;
}
/******************************************************************************/
/*!
 * \fn void PWM2FreqControl(Uint16 period)
 * \param period pwm period to be provided
 *
 * \brief Controls individual pwm frequency
 * \brief  Description:
 *      This function control the pwm channel
 *      Configures pwm period & duty cycle
 *
 * \return void
 */
/******************************************************************************/
void PWM2FreqControl(Uint16 period)
{
    //!Control the PWM 2 by setting the Time period and the duty cycle
	(*ePWM[1]).TBPRD = period-1;
	(*ePWM[1]).CMPA.bit.CMPA = (Uint16)(period*0.7);       // set duty 70%
	(*ePWM[1]).AQCTLA.bit.ZRO = AQ_SET;               // PWM toggle high/low
	(*ePWM[1]).AQCTLA.bit.CAU = AQ_CLEAR;
}
/******************************************************************************/
/*!
 * \fn void PWM5FreqControl(Uint16 period)
 * \param period pwm period to be provided
 *
 * \brief Controls individual pwm frequency
 * \brief  Description:
 *      This function control the pwm channel
 *      Configures pwm period & duty cycle
 *
 * \return void
 */
/******************************************************************************/
void PWM5FreqControl(Uint16 period)
{
    //!Control the PWM 5 by setting the Time period and the duty cycle
	(*ePWM[2]).TBPRD = period-1;
	(*ePWM[2]).CMPA.bit.CMPA = (Uint16)(period*0.7);       // set duty 70%
	(*ePWM[2]).AQCTLA.bit.ZRO = AQ_SET;               // PWM toggle high/low
	(*ePWM[2]).AQCTLA.bit.CAU = AQ_CLEAR;
}
/******************************************************************************/
/*!
 * \fn void PWM6FreqControl(Uint16 period)
 * \param period pwm period to be provided
 *
 * \brief Controls individual pwm frequency
 * \brief  Description:
 *      This function control the pwm channel
 *      Configures pwm period & duty cycle
 *
 * \return void
 */
/******************************************************************************/
void PWM6FreqControl(Uint16 period)
{
    //!Control the PWM 6 by setting the Time period and the duty cycle
	(*ePWM[3]).TBPRD = period-1;
	(*ePWM[3]).CMPA.bit.CMPA = (Uint16)(period*0.7);       // set duty 70%
	(*ePWM[3]).AQCTLA.bit.ZRO = AQ_SET;               // PWM toggle high/low
	(*ePWM[3]).AQCTLA.bit.CAU = AQ_CLEAR;
}
/******************************************************************************/
/*!
 * \fn void PWM8FreqControl(Uint16 period)
 * \param period pwm period to be provided
 *
 * \brief Controls individual pwm frequency
 * \brief  Description:
 *      This function control the pwm channel
 *      Configures pwm period & duty cycle
 *
 * \return void
 */
/******************************************************************************/
void PWM8FreqControl(Uint16 period)
{
    //!Control the PWM 8 by setting the Time period and the duty cycle
	(*ePWM[4]).TBPRD = period-1;
	(*ePWM[4]).CMPA.bit.CMPA = (Uint16)(period*0.7);       // set duty 70%
	(*ePWM[4]).AQCTLA.bit.ZRO = AQ_SET;               // PWM toggle high/low
	(*ePWM[4]).AQCTLA.bit.CAU = AQ_CLEAR;
}
/******************************************************************************/
/*!
 * \fn __interrupt void epwm1_isr(void)
 * \brief PWM interrupt handler for epwm1
 * \brief  Description:
 *      PWM interrupt handler call on every frequency steps
 *      s-curve and pwm cotrol shall be done here
 *
 * \return void
 */
/******************************************************************************/
__interrupt void epwm1_isr(void)
{
    /*Ymotor ISR*/
    //!This section is added to resolve Y-Axis motor getting stuck at
    //!home postion and the sequnce gets blocked
    //!Check if the motor movement is towards home position
    //!If the home poisition is detected, set the distance to be moved to 2mm
    //!So that S-Curve will be enabled and the motor moves further up above the
    //!home position detected location and the sequence continues
    //!This loop shall run only once for setting the distance of 2mm after
    //!reaching home position

/*    if((true == m_bY_Home_Movement) && (1 == PWMMotor5SensorPoll()) \
            && (Motor5Pwm1.Status == START))
    {
        m_bY_Home_Movement = false;
        Motor5Pwm1.FBTimerCounter = Motor5Pwm1.TimerCounter;
        Motor5Pwm1.TimerCounter = Motor5Pwm1.NumberOfSteps - \
                (Uint32)((200/MOTOR5_STEP_SIZE));
    }

    if(((true == m_bY_Home_Movement)&&(Motor5Pwm1.Status == START)) \
           && (Motor5Pwm1.TimerCounter >= Motor5Pwm1.NumberOfSteps))
    {
        m_bY_Home_Movement = false;
        Motor5Pwm1.FBTimerCounter = Motor5Pwm1.TimerCounter;
    }*/

    if(((true == m_bY_Home_Movement) && (Motor5Pwm1.Status == START)) &&
            ((1 == PWMMotor5SensorPoll()) || (Motor5Pwm1.TimerCounter >= Motor5Pwm1.NumberOfSteps)))
    {
        m_bY_Home_Movement = false;
        Motor5Pwm1.FBTimerCounter = Motor5Pwm1.TimerCounter;
        if(1 == PWMMotor5SensorPoll())
        {
            Motor5Pwm1.TimerCounter = Motor5Pwm1.NumberOfSteps - \
                (Uint32)((200/MOTOR5_STEP_SIZE));
        }
    }

    //!Check whether the pulse given to the motor has reached to the no. of steps
    //!to be given when the motor is in start condition.
    //!If the above condition is met, stop the motor and set the status to
    //!completed and direction to IDLE, else continue running the Motor

	if((Motor5Pwm1.TimerCounter >= Motor5Pwm1.NumberOfSteps) && \
	        (Motor5Pwm1.Status == START))
	{
		PWMControl(PWM1A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor5Pwm1.Status = COMPLETED;
		//Motor5Pwm1.Direction = IDLE;

#if PWM_DEBUG_PRINT_ENABLE
		Motor5Pwm1.TimeStamp[1] = CpuTimer0.InterruptCount;
		Motor5Pwm1.TimeStamp[2] = Motor5Pwm1.TimerCounter;
#endif
//		Disable_Motor5();
	}
#if MOTOR5_SCURVE_ENABLE
	//!If the motor is starting, it has to start from low frequency to run the motor
	//!smoothly, so S-Curve is enabled for particular start band period
	//!Frequency of the motor is slowly increased for the start band period as
	//!specified in the SCurve look-up
    //!If the motor is starting, then apply the S-Curve
	else if(Motor5Pwm1.TimerCounter < Motor5Pwm1.StartBandPeriod)
	{
		PWM1FreqControl(Motor5Pwm1.SCurveLookUp[Motor5Pwm1.SCurveLookUpCounter++]);
//		PWMControl(PWM1A, START, \
		Motor5Pwm1.SCurveLookUp[Motor5Pwm1.SCurveLookUpCounter++]);
	}
    //!If the motor is stopping, it has to stop with the frequency gradually falling
    //!from high frequency to low frequency rather than abruply stopping
    //!So S-Curve is enabled for particular stop band period
    //!Frequency of the motor is slowly decresed for the stop band period as
    //!specified in the SCurve look-up
    //!If the distance of the motor moved is close to the speified distance to be
    //!moved, then apply the S-Curve
	else if(Motor5Pwm1.TimerCounter >= Motor5Pwm1.StopBandPeriod)
	{
		PWM1FreqControl(Motor5Pwm1.SCurveLookUp[--Motor5Pwm1.SCurveLookUpCounter]);
//		PWMControl(PWM1A, START, \
		Motor5Pwm1.SCurveLookUp[--Motor5Pwm1.SCurveLookUpCounter]);
	}
#endif

	//!Increment the timer counter which refers to the no. of pulses applied to
	//!move the motor
    Motor5Pwm1.TimerCounter++;

    //!Clear INT flag for this timer
    EPwm1Regs.ETCLR.bit.INT = 1;

    //!Acknowledge this interrupt to receive more interrupts from group 3
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
/******************************************************************************/
/*!
 * \fn __interrupt void epwm2_isr(void)
 * \brief PWM interrupt handler for epwm2
 * \brief  Description:
 *      PWM interrupt handler call on every frequency steps
 *      s-curve and pwm cotrol shall be done here
 * \return void
 */
/******************************************************************************/
__interrupt void epwm2_isr(void)
{
    /* Sample ISR*/
    //!This section is added to resolve Sample motor Coming down from
    //!home position and the Home sensor is not detecting.
    //!Check if the motor movement is towards home position
    //!If the home position is detected, set the distance to be moved to 1mm
    //!So that S-Curve will be enabled and the motor moves further up above the
    //!home position detected location and the sequence continues
    //!This loop shall run only once for setting the distance of 1mm after
    //!reaching home position
    if(((true == m_bSample_Home_Movement) && (Motor3Pwm2.Status == START)) &&
      ((1 == PWMMotor3SensorPoll()) || (Motor3Pwm2.TimerCounter >= Motor3Pwm2.NumberOfSteps)))
        {
            m_bSample_Home_Movement = false;
            Motor3Pwm2.FBTimerCounter = Motor3Pwm2.TimerCounter;
            if(1 == PWMMotor3SensorPoll())
            {
                Motor3Pwm2.TimerCounter = Motor3Pwm2.NumberOfSteps - \
                    (Uint32)((100/MOTOR3_STEP_SIZE));
            }
        }
    //!Check whether the pulse given to the motor has reached to the no. of steps
    //!to be given when the motor is in start condition
    //!If the above condition is met, stop the motor and set the status to
    //!completed and direction to IDLE, else continue running the Motor
	if((Motor3Pwm2.TimerCounter >= Motor3Pwm2.NumberOfSteps) && \
	        (Motor3Pwm2.Status == START))
	{
		PWMControl(PWM2A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor3Pwm2.Status = COMPLETED;
		//Motor3Pwm2.Direction = IDLE;

#if PWM_DEBUG_PRINT_ENABLE
		Motor3Pwm2.TimeStamp[1] = CpuTimer0.InterruptCount;
		Motor3Pwm2.TimeStamp[2] = Motor3Pwm2.TimerCounter;
#endif
//		Disable_Motor3();
	}
	else if((m_bSample_Home_Movement))
	{
	    //!If the motor is starting, it has to start from low frequency to run the motor
	    //!smoothly, so S-Curve is enabled for particular start band period
	    //!Frequency of the motor is slowly increased for the start band period as
	    //!specified in the SCurve look-up
	    //!If the motor is starting, then apply the S-Curve
	    if(Motor3Pwm2.TimerCounter < Motor3Pwm2.StartBandPeriod)
	    {
	        PWM2FreqControl(Motor3Pwm2.SCurveLookUp[Motor3Pwm2.SCurveLookUpCounter++]);
	        //		PWMControl(PWM2A, START, \
	        Motor3Pwm2.SCurveLookUp[Motor3Pwm2.SCurveLookUpCounter++]);
	    }
	    //!If the motor is stopping, it has to stop with the frequency gradually falling
	    //!from high frequency to low frequency rather than abruply stopping
	    //!So S-Curve is enabled for particular stop band period
	    //!Frequency of the motor is slowly decresed for the stop band period as
	    //!specified in the SCurve look-up
	    //!If the distance of the motor moved is close to the speified distance to be
	    //!moved, then apply the S-Curve
	    else if(Motor3Pwm2.TimerCounter >= Motor3Pwm2.StopBandPeriod)
	     {
	        PWM2FreqControl(Motor3Pwm2.SCurveLookUp[--Motor3Pwm2.SCurveLookUpCounter]);
	        //		PWMControl(PWM2A, START, \
	        Motor3Pwm2.SCurveLookUp[--Motor3Pwm2.SCurveLookUpCounter]);
	      }
	}
    //!Increment the timer counter which refers to the no. of pulses applied to
    //!move the motor
	Motor3Pwm2.TimerCounter++;

    //!Clear INT flag for this timer
    EPwm2Regs.ETCLR.bit.INT = 1;

    //!Acknowledge this interrupt to receive more interrupts from group 3
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
/******************************************************************************/
/*!
 * \fn __interrupt void epwm5_isr(void)
 * \brief PWM interrupt handler for epwm5
 * \brief  Description:
 *      PWM interrupt handler call on every frequency steps
 *      s-curve and pwm cotrol shall be done here
 * \return void
 */
/******************************************************************************/
__interrupt void epwm5_isr(void)
{
    /*X_axis ISR */
#if 0
    //!Check whether the pulse given to the motor has reached to the no. of steps
    //!to be given when the motor is in start condition or the home position is detected
    //!If the above condition is met, stop the motor and set the status to
    //!completed and direction to IDLE, else continue running the Motor
	if((1 == PWMMotor4SensorPoll() && (Motor4Pwm5.Status == START)) || \
	        (Motor4Pwm5.TimerCounter >= Motor4Pwm5.NumberOfSteps) && \
	        (Motor4Pwm5.Status == START))
	{
		PWMControl(PWM5A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor4Pwm5.Status = COMPLETED;
		Motor4Pwm5.Direction = IDLE;

#if PWM_DEBUG_PRINT_ENABLE
		Motor4Pwm5.TimeStamp[1] = CpuTimer0.InterruptCount;
		Motor4Pwm5.TimeStamp[2] = Motor4Pwm5.TimerCounter;
#endif

//		Disable_Motor4();
	}
#endif
	//!Check whether the pulse given to the motor has reached to the no. of steps
    //!to be given when the motor is in start condition or the home position is detected
    //!If the above condition is met, stop the motor and set the status to
    //!completed and direction to IDLE, else continue running the Motor
    if((1 == PWMMotor4SensorPoll() && (Motor4Pwm5.Status == START) && \
            (Motor4Pwm5.HomeSensorControl == SENSOR_INTERRUPT_ENABLE)) || \
            (Motor4Pwm5.TimerCounter >= Motor4Pwm5.NumberOfSteps) && \
            (Motor4Pwm5.Status == START))
    {
        PWMControl(PWM5A, STOP, STOP_MOTOR_PWM_PERIOD);
        Motor4Pwm5.Status = COMPLETED;
        //Motor1Pwm8.Direction = IDLE;

#if PWM_DEBUG_PRINT_ENABLE
        Motor4Pwm5.TimeStamp[1] = CpuTimer0.InterruptCount;
        Motor4Pwm5.TimeStamp[2] = Motor1Pwm8.TimerCounter;
#endif
//      Disable_Motor4();
    }
    //!this loop is for after reaching the home move 23mm to reach aspiration position.
   if((1 == PWMMotor4SensorPoll() && (Motor4Pwm5.Status == START) && \
           (Motor4Pwm5.HomeSensorControl == SENSOR_INTERRUPT_DISABLE) \
           && (m_bX_Home_Movement == true)))
   {
       //Motor4Pwm5.TimerCounter = Motor4Pwm5.NumberOfSteps - (Uint32)((2511/MOTOR4_STEP_SIZE));
       Motor4Pwm5.TimerCounter = Motor4Pwm5.NumberOfSteps - (Uint32)((2311/MOTOR4_STEP_SIZE)); //HN_Change
        m_bX_Home_Movement = false;
   }
#if MOTOR4_SCURVE_ENABLE
    //!If the motor is starting, it has to start from low frequency to run the motor
    //!smoothly, so S-Curve is enabled for particular start band period
    //!Frequency of the motor is slowly increased for the start band period as
    //!specified in the SCurve look-up
    //!If the motor is starting, then apply the S-Curve
	else if(Motor4Pwm5.TimerCounter < Motor4Pwm5.StartBandPeriod)
	{
		PWM5FreqControl(Motor4Pwm5.SCurveLookUp[Motor4Pwm5.SCurveLookUpCounter++]);
//		PWMControl(PWM5A, START, \
		Motor4Pwm5.SCurveLookUp[Motor4Pwm5.SCurveLookUpCounter++]);
	}
    //!If the motor is stopping, it has to stop with the frequency gradually falling
    //!from high frequency to low frequency rather than abruply stopping
    //!So S-Curve is enabled for particular stop band period
    //!Frequency of the motor is slowly decresed for the stop band period as
    //!specified in the SCurve look-up
    //!If the distance of the motor moved is close to the speified distance to be
    //!moved, then apply the S-Curve
	else if(Motor4Pwm5.TimerCounter >= Motor4Pwm5.StopBandPeriod)
	{
		PWM5FreqControl(Motor4Pwm5.SCurveLookUp[--Motor4Pwm5.SCurveLookUpCounter]);
//		PWMControl(PWM5A, START, \
		Motor4Pwm5.SCurveLookUp[--Motor4Pwm5.SCurveLookUpCounter]);

	}
#endif
    //!Increment the timer counter which refers to the no. of pulses applied to
    //!move the motor
	Motor4Pwm5.TimerCounter++;

    //!Clear INT flag for this timer
    EPwm5Regs.ETCLR.bit.INT = 1;

    //!Acknowledge this interrupt to receive more interrupts from group 3
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
/******************************************************************************/
/*!
 * \fn __interrupt void epwm6_isr(void)
 * \brief PWM interrupt handler for epwm6
 * \brief  Description:
 *      PWM interrupt handler call on every frequency steps
 *      s-curve and pwm cotrol shall be done here
 * \return void
 */
/******************************************************************************/
__interrupt void epwm6_isr(void)
{
    /*DILUENT MOTOR ISR */
    //!This section is added to resolve Diluent motor Coming down from
    //!home position and the Home sensor is not detecting.
    //!Check if the motor movement is towards home position
    //!If the home position is detected, set the distance to be moved to 1mm
    //!So that S-Curve will be enabled and the motor moves further up above the
    //!home position detected location and the sequence continues
    //!This loop shall run only once for setting the distance of 1mm after
    //!reaching home position
    if(((true == m_bDiluent_Home_Movement) && (Motor2Pwm6.Status == START)) &&
      ((1 == PWMMotor2SensorPoll()) || (Motor2Pwm6.TimerCounter >= Motor2Pwm6.NumberOfSteps)))
        {
            m_bDiluent_Home_Movement = false;
            Motor2Pwm6.FBTimerCounter = Motor2Pwm6.TimerCounter;
            if(1 == PWMMotor2SensorPoll())
            {
                Motor2Pwm6.TimerCounter = Motor2Pwm6.NumberOfSteps - \
                    (Uint32)((100/MOTOR2_STEP_SIZE));
            }
        }

    //!Check whether the pulse given to the motor has reached to the no. of steps
    //!to be given when the motor is in start condition,stop the motor and set the status to
    //!completed and direction to IDLE, else continue running the Motor
    if((Motor2Pwm6.TimerCounter >= Motor2Pwm6.NumberOfSteps) && \
             (Motor2Pwm6.Status == START))
    {
        PWMControl(PWM6A, STOP, STOP_MOTOR_PWM_PERIOD);
        Motor2Pwm6.Status = COMPLETED;
        g_bLowVolumeDisp = false;
        //Motor2Pwm6.Direction = IDLE;

#if PWM_DEBUG_PRINT_ENABLE
        Motor2Pwm6.TimeStamp[1] = CpuTimer0.InterruptCount;
        Motor2Pwm6.TimeStamp[2] = Motor2Pwm6.TimerCounter;
#endif

//      Disable_Motor4();
    }
    //!Don't Apply sCurve for low volume dispensation.
    else if(!(g_bLowVolumeDisp))
    {
        //!If the motor is starting, it has to start from low frequency to run the motor
        //!smoothly, so S-Curve is enabled for particular start band period
        //!Frequency of the motor is slowly increased for the start band period as
        //!specified in the SCurve look-up
        //!If the motor is starting, then apply the S-Curve
        if(Motor2Pwm6.TimerCounter < Motor2Pwm6.StartBandPeriod)
        {
            PWM6FreqControl(Motor2Pwm6.SCurveLookUp[Motor2Pwm6.SCurveLookUpCounter++]);
            //		PWMControl(PWM6A, START, \
            Motor2Pwm6.SCurveLookUp[Motor2Pwm6.SCurveLookUpCounter++]);
        }
        //!If the motor is stopping, it has to stop with the frequency gradually falling
        //!from high frequency to low frequency rather than abruply stopping
        //!So S-Curve is enabled for particular stop band period
        //!Frequency of the motor is slowly decresed for the stop band period as
        //!specified in the SCurve look-up
        //!If the distance of the motor moved is close to the speified distance to be
        //!moved, then apply the S-Curve
        else if(Motor2Pwm6.TimerCounter >= Motor2Pwm6.StopBandPeriod)
        {
            PWM6FreqControl(Motor2Pwm6.SCurveLookUp[--Motor2Pwm6.SCurveLookUpCounter]);
            //		PWMControl(PWM6A, START, \
            Motor2Pwm6.SCurveLookUp[--Motor2Pwm6.SCurveLookUpCounter]);
        }
    }

    //!Increment the timer counter which refers to the no. of pulses applied to
    //!move the motor
	Motor2Pwm6.TimerCounter++;

    //!Clear INT flag for this timer
    EPwm6Regs.ETCLR.bit.INT = 1;

    //!Acknowledge this interrupt to receive more interrupts from group 3
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}
/******************************************************************************/
/*!
 * \fn __interrupt void epwm8_isr(void)
 * \brief PWM interrupt handler for epwm8
 * \brief  Description:
 *      PWM interrupt handler call on every frequency steps
 *      s-curve and pwm control shall be done here
 * \return void
 */
/******************************************************************************/
__interrupt void epwm8_isr(void)
{
    /*Waste Motor ISR*/

    if(g_bFirstMixingState)
    {
        if(Counter >= MixingTime)
        {
            g_bFirstMixingState = false;
            MixingTime = ZERO;
            Counter = ZERO;

            //!Update the completed pulses to FBTimerCounter for error checking
            Motor1Pwm8.FBTimerCounter = Motor1Pwm8.TimerCounter;

            PWMControl(PWM8A, STOP, STOP_MOTOR_PWM_PERIOD);
            Motor1Pwm8.Status = COMPLETED;

        }
        //Counter++;
    }

    if(((true == m_bWaste_Home_Movement) && (Motor1Pwm8.Status == START)) &&
      ((1 == PWMMotor1SensorPoll()) || (Motor1Pwm8.TimerCounter >= Motor1Pwm8.NumberOfSteps)))
    {
        m_bWaste_Home_Movement = false;
        //!Update the completed pulses to FBTimerCounter for error checking
        Motor1Pwm8.FBTimerCounter = Motor1Pwm8.TimerCounter;
        if(1 == PWMMotor1SensorPoll())
        {
            Motor1Pwm8.TimerCounter = Motor1Pwm8.NumberOfSteps - \
                (Uint32)((100/MOTOR1_STEP_SIZE));
        }
    }
    //!If the state from MBD is vacuum monitor than stop the motor
    //! if the vacuum is >= 3.7 PSI
    if(g_bCountTimeVacuumMoni == true)
    {
        if(m_usnPressureAdc <= g_TestDataI0  )
        {
            //PWMControl(PWM8A, STOP, STOP_MOTOR_PWM_PERIOD);
            //Motor1Pwm8.Status = COMPLETED;
            //!Update the completed pulses to FBTimerCounter for error checking
            Motor1Pwm8.FBTimerCounter = Motor1Pwm8.TimerCounter;

            //!Don't stop the motor abruptly. Apply the S-cur by giving
            //! 100 pulses(Full Step)   //HN_change (28/2/2019)
            Motor1Pwm8.TimerCounter = (Motor1Pwm8.NumberOfSteps - 400);

            g_bCountTimeVacuumMoni = false;
            bMStopADCPressure = m_usnPressureAdc;
        }
    }
	if((Motor1Pwm8.TimerCounter >= Motor1Pwm8.NumberOfSteps) && \
	        (Motor1Pwm8.Status == START))
	{
	    if(0 == Motor1Pwm8.FBTimerCounter)
	    {
	        //!Update the completed pulses to FBTimerCounter for error checking
	        Motor1Pwm8.FBTimerCounter = Motor1Pwm8.TimerCounter;
	    }
	    PWMControl(PWM8A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor1Pwm8.Status = COMPLETED;
		g_bCountTimeVacuumMoni = false; //Added 6/12/2018
		g_bTubeCleanVacuumMoni = false;
		g_bFirstMixingState = false;

#if PWM_DEBUG_PRINT_ENABLE
		Motor1Pwm8.TimeStamp[1] = CpuTimer0.InterruptCount;
		Motor1Pwm8.TimeStamp[2] = Motor1Pwm8.TimerCounter;
#endif
//		Disable_Motor4();
	}

#if MOTOR1_SCURVE_ENABLE
   //!If the motor is starting, it has to start from low frequency to run the motor
   //!smoothly, so S-Curve is enabled for particular start band period
   //!Frequency of the motor is slowly increased for the start band period as
   //!specified in the SCurve look-up
   //!If the motor is starting, then apply the S-Curve
   else if(Motor1Pwm8.TimerCounter < Motor1Pwm8.StartBandPeriod)
   {
       PWM8FreqControl(Motor1Pwm8.SCurveLookUp[Motor1Pwm8.SCurveLookUpCounter++]);
//	   PWMControl(PWM8A, START, \
       Motor1Pwm8.SCurveLookUp[Motor1Pwm8.SCurveLookUpCounter++]);
   }
   //!If the motor is stopping, it has to stop with the frequency gradually falling
   //!from high frequency to low frequency rather than abruply stopping
   //!So S-Curve is enabled for particular stop band period
   //!Frequency of the motor is slowly decresed for the stop band period as
   //!specified in the SCurve look-up
   //!If the distance of the motor moved is close to the speified distance to be
   //!moved, then apply the S-Curve
	else if(Motor1Pwm8.TimerCounter >= Motor1Pwm8.StopBandPeriod)
	{
		PWM8FreqControl(Motor1Pwm8.SCurveLookUp[--Motor1Pwm8.SCurveLookUpCounter]);
//		PWMControl(PWM8A, START, \
		Motor1Pwm8.SCurveLookUp[--Motor1Pwm8.SCurveLookUpCounter]);
	}
#endif
    //!Increment the timer counter which refers to the no. of pulses applied to
    //!move the motor
	Motor1Pwm8.TimerCounter++;

    //!Clear INT flag for this timer
    EPwm8Regs.ETCLR.bit.INT = 1;

    //!Acknowledge this interrupt to receive more interrupts from group 3
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}


#if 0//No More Needed. Refer MotorSencePoll functions

/******************************************************************************/
/*!
 * \fn void Motor1SensorISR(void)
 * \brief Optical sensor 1 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 1
 *
 * \return void
 */
/******************************************************************************/
void Motor1SensorISR(void)
{
    //!If the motor is moving towards home direction and if the home position
    //!stop the PWM control of the motor and set the motor status to complete and idle
	if((SENSOR1_HOME_STATE == OIHomeSensor1State(READ_DATA)) && \
	        Motor1Pwm8.Direction == MOTOR1_HOME_DIRECTION && \
	        Motor1Pwm8.HomeSensorControl == SENSOR_INTERRUPT_ENABLE)
	{

		PWMControl(PWM8A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor1Pwm8.Status = COMPLETED;
		Motor1Pwm8.Direction = IDLE;
	//		BloodCellCounter_U.HP_YAxis = COMPLETED;
	}

	//	   GpioDataRegs.GPFDAT.bit.GPIO161 = 1;
//		OIHomeSensor1State(WRITE_DATA);
}
/******************************************************************************/
/*!
 * \fn void Motor2SensorISR(void)
 * \brief Optical sensor 2 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 2
 *
 * \return void
 */
/******************************************************************************/
void Motor2SensorISR(void)
{
    //!If the motor is moving towards home direction and if the home position
    //!stop the PWM control of the motor and set the motor status to complete and idle
	if((SENSOR2_HOME_STATE == OIHomeSensor2State(READ_DATA)) && \
	        Motor2Pwm6.Direction == MOTOR2_HOME_DIRECTION)
	{
		PWMControl(PWM6A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor2Pwm6.Status = COMPLETED;
		Motor2Pwm6.Direction = IDLE;
	//		BloodCellCounter_U.HP_XAxis = COMPLETED;
	}
	//	   GpioDataRegs.GPFDAT.bit.GPIO163 = 1;
//	OIHomeSensor2State(WRITE_DATA);

}
/******************************************************************************/
/*!
 * \fn void Motor3SensorISR(void)
 * \brief Optical sensor 3 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 3
 *
 * \return void
 */
/******************************************************************************/
void Motor3SensorISR(void)
{
    //!If the motor is moving towards home direction and if the home position
    //!stop the PWM control of the motor and set the motor status to complete and idle
	if((SENSOR3_HOME_STATE == OIHomeSensor3State(READ_DATA))&& \
	        Motor3Pwm2.Direction == MOTOR3_HOME_DIRECTION)
	{
		PWMControl(PWM2A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor3Pwm2.Status = COMPLETED;
		Motor3Pwm2.Direction = IDLE;
	}
//	   GpioDataRegs.GPFDAT.bit.GPIO164 = 1;
//	OIHomeSensor3State(WRITE_DATA);
}
/******************************************************************************/
/*!
 * \fn void Motor4SensorISR(void)
 * \brief Optical sensor 4 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 4
 *
 * \return void
 */
/******************************************************************************/
void Motor4SensorISR(void)
{
    //!If the motor is moving towards home direction and if the home position
    //!stop the PWM control of the motor and set the motor status to complete and idle
	if((SENSOR4_HOME_STATE == OIHomeSensor4State(READ_DATA)) && \
	        Motor4Pwm5.Direction == MOTOR4_HOME_DIRECTION)
	{
		PWMControl(PWM5A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor4Pwm5.Status = COMPLETED;
		Motor4Pwm5.Direction = IDLE;
	}
//	   GpioDataRegs.GPFDAT.bit.GPIO162 = 1;
//	OIHomeSensor4State(WRITE_DATA);

}
/******************************************************************************/
/*!
 * \fn void Motor5SensorISR(void)
 * \brief Optical sensor 5 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 5
 *
 * \return void
 */
/******************************************************************************/
void Motor5SensorISR(void)
{
    //!If the motor is moving towards home direction and if the home position
    //!stop the PWM control of the motor and set the motor status to complete and idle
	if((SENSOR5_HOME_STATE == OIHomeSensor5State(READ_DATA)) && \
	        Motor5Pwm1.Direction == MOTOR5_HOME_DIRECTION)
	{
		PWMControl(PWM1A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor5Pwm1.Status = COMPLETED;
		Motor5Pwm1.Direction = IDLE;
	}
//	   GpioDataRegs.GPEDAT.bit.GPIO133 = 1;
//	   OIHomeSensor5State(WRITE_DATA);

}

#endif
/******************************************************************************/
/*!
 * \fn void PWMConfigMotor1Param(float  fDisplacement, Uint16 usnDirection,
 *          Uint16 StepFrequency)
 * \param  fDisplacement motor displacement to be provided
 * \param  usnDirection specifies the direction of the motor
 * \param  StepFrequency specifies the step frequency for the motor
 * \brief Configure Motor 1 PWM parameters
 * \brief  Description:
 *      Motor 1 related pwm parameters shall be configured here.
 *      PWM frequency, Steps & S-curve related parameters are calculated here
 *      PWM channel 8 for motor 1 configuration
 *
 * \return void
 */
/******************************************************************************/
void PWMConfigMotor1Param(float  fDisplacement, Uint16 usnDirection, \
        Uint16 StepFrequency)
{
    //!Set the frequency of the Motor based on the step frequency recived.
    //!If it is less than the 32500, set the MOTOR freq. factor
    //!else set the PWM frequency for the motor movement
	if((StepFrequency * MOTOR1_FREQUENCY_FACTOR1) < 32500)
	{
		Motor1Pwm8.Fequency = (StepFrequency * MOTOR1_FREQUENCY_FACTOR1);
	}
	else
	{
		Motor1Pwm8.Fequency = MOTOR_PWM_FREQUENCY;
	}

	//!Set the following configurations of the motor:
	//!1. Direction
	//!2. No. of steps to be moved
	//!3. Step Size configuration
	Motor1Pwm8.Direction = usnDirection;
	Motor1Pwm8.NumberOfSteps = (Uint32)((fDisplacement/MOTOR1_STEP_SIZE));//4724 steps
	if(Motor1Pwm8.NumberOfSteps < 1)
	{
		Motor1Pwm8.NumberOfSteps = 1;
	}
	Motor1Pwm8.StepSize = MOTOR1_STEP_CONFIGURATION;

#if MOTOR_PWM_DEBUG_PRINT_ENABLE
//#if 1
//    printf(" M1 *Freq: %d *Steps: %ld \n",Motor1Pwm8.Fequency,Motor1Pwm8.NumberOfSteps);
//    printf("M1 *Freq:%d *Steps:%ld disp:%ld\n",Motor1Pwm8.Fequency,Motor1Pwm8.NumberOfSteps,fDisplacement);

    sprintf(DebugPrintBuf,"M1 *Frq:%d *Stp:%ld di:%lf,%lf\n",Motor1Pwm8.Fequency,Motor1Pwm8.NumberOfSteps,fDisplacement,MOTOR1_STEP_SIZE);
    UartDebugPrint((int *)DebugPrintBuf, 75);
#endif

#if	MOTOR1_SCURVE_ENABLE
	//!S-Curve look-up function for the motor start band and stop band of the motor
	PWMGetSCurveLookUp(&Motor1Pwm8);
	Motor1Pwm8.SCurveLookUpCounter = 0;
#else
	//!Set the S-Curve look-up with the period and freq. configuration
	Motor1Pwm8.SCurveLookUp[0] = (Uint16)(MOTOR_PWM_PERIOD_FACTOR/Motor1Pwm8.Fequency);
#endif

	//!Set the status of the motor to start and intialize the timer counter to zero
	Motor1Pwm8.Status = START;
	Motor1Pwm8.TimerCounter = 0;
	Motor1Pwm8.FBTimerCounter = 0;
	Motor1Pwm8.TimeStamp[0] = CpuTimer0.InterruptCount;
	//m_bX_Home_Movement = true;//SKM_CHANGE_X_AXIS_HOME

	if(Motor1Pwm8.Direction == MOTOR1_HOME_DIRECTION)
	{
	    m_bWaste_Home_Movement = true;
	}

	 Counter = 0;

	if((SENSOR1_HOME_STATE == OIHomeSensor1State()) && \
    Motor1Pwm8.Direction == MOTOR1_HOME_DIRECTION)
	{

		PWMControl(PWM8A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor1Pwm8.Status = COMPLETED;
		//Motor1Pwm8.Direction = IDLE;
	}
	else
	{
		PWMControl(PWM8A, START, Motor1Pwm8.SCurveLookUp[0]);
	}
	//!Start the PWM to start the motor
	//PWMControl(PWM8A, START, Motor1Pwm8.SCurveLookUp[0]);

}
/******************************************************************************/
/*!
 * \fn void PWMConfigMotor2Param(float  fDisplacement, Uint16 usnDirection,
 *          Uint16 StepFrequency)
 * \param fDisplacement motor displacement to be provided
 * \param usnDirection specifies the direction of the motor
 * \param StepFrequency specifies the step frequency for the motor
 *
 * \brief Configure Motor 2 PWM 6 parameters
 * \brief Description:
 *      Motor 2 related pwm parameters shall be configured here.
 *      PWM frequency, Steps & S-curve related parameters are calculated here
 *      PWM channel 6 for motor 2 configuration
 *
 * \return void
 */
/******************************************************************************/
void PWMConfigMotor2Param(float fDisplacement, Uint16 usnDirection,
        Uint16 StepFrequency)
{
    //!Set the frequency of the Motor based on the step frequency recived.
    //!If it is less than the 32500, set the MOTOR freq. factor
    //!else set the PWM frequency for the motor movement
	if((StepFrequency * MOTOR2_FREQUENCY_FACTOR1) < 32500)
	{
		Motor2Pwm6.Fequency = (StepFrequency * MOTOR2_FREQUENCY_FACTOR1);
	}
	else
	{
		Motor2Pwm6.Fequency = MOTOR_PWM_FREQUENCY;
	}

    //!Set the following configurations of the motor:
    //!1. Direction
    //!2. No. of steps to be moved
    //!3. Step Size configuration
	Motor2Pwm6.Direction = usnDirection;
	Motor2Pwm6.NumberOfSteps = (Uint32)((fDisplacement/MOTOR2_STEP_SIZE));//4724 steps
	if(Motor2Pwm6.NumberOfSteps < 1)
	{
		Motor2Pwm6.NumberOfSteps = 1;
	}
	Motor2Pwm6.StepSize = MOTOR2_STEP_CONFIGURATION;

#if MOTOR_PWM_DEBUG_PRINT_ENABLE
	printf(" M2 *Freq: %d *Steps: %ld \n",Motor2Pwm6.Fequency,Motor2Pwm6.NumberOfSteps);
#endif

	if(!(g_bLowVolumeDisp))
	{
	    //!S-Curve look-up function for the motor start band and stop band of the motor
	    PWMGetSCurveLookUp(&Motor2Pwm6);
	    Motor2Pwm6.SCurveLookUpCounter = 0;
	}
	else
	{
	    //!Set the S-Curve look-up with the period and freq. configuration
	    Motor2Pwm6.SCurveLookUp[0] = (Uint16)(MOTOR_PWM_PERIOD_FACTOR/Motor2Pwm6.Fequency);
	}
	//!Set the status of the motor to start and intialize the timer counter to zero
	Motor2Pwm6.Status = START;
	Motor2Pwm6.TimerCounter = 0;
	Motor2Pwm6.FBTimerCounter = 0;
	Motor2Pwm6.TimeStamp[0] = CpuTimer0.InterruptCount;

    //!If motor is moving home direction then set the diluent home movement variable
    if(Motor2Pwm6.Direction == MOTOR2_HOME_DIRECTION)
    {
         m_bDiluent_Home_Movement = true;
    }

	//!If the motor is moving towards home direction and if home position is
	//!detected stop the motor, else start the motor.
	if((SENSOR2_HOME_STATE == OIHomeSensor2State()) && \
	        Motor2Pwm6.Direction == MOTOR2_HOME_DIRECTION)
	{
	    //!Disable the PWM to stop the motor movement and set the status to completed
	    //!and direction to IDLE.
		PWMControl(PWM6A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor2Pwm6.Status = COMPLETED;
		//Motor2Pwm6.Direction = IDLE;

	}
	else
	{
	    //!Start the PWM to start the motor
		PWMControl(PWM6A, START, Motor2Pwm6.SCurveLookUp[0]);
	}
}
/******************************************************************************/
/*!
 * \fn void PWMConfigMotor3Param(float fDisplacement, Uint16 usnDirection,
 *          Uint16 StepFrequency)
 * \param fDisplacement motor displacement to be provided
 * \param usnDirection specifies the direction of the motor
 * \param StepFrequency specifies the step frequency for the motor
 *
 * \brief Configure Motor 3 PWM 4 parameters
 * \brief Description:
 *      Motor 3 related pwm parameters shall be configured here.
 *      PWM frequency, Steps & S-curve related parameters are calculated here
 *      PWM channel 2for motor 3 configuration
 *
 * \return void
 */
/******************************************************************************/
void PWMConfigMotor3Param(float fDisplacement, Uint16 usnDirection,
        Uint16 StepFrequency)
{
    //!Set the frequency of the Motor based on the step frequency recived.
    //!If it is less than the 32500, set the MOTOR freq. factor
    //!else set the PWM frequency for the motor movement
	if((StepFrequency * MOTOR3_FREQUENCY_FACTOR1) < 32500)
	{
		Motor3Pwm2.Fequency = (StepFrequency * MOTOR3_FREQUENCY_FACTOR1);
	}
	else
	{
		Motor3Pwm2.Fequency = MOTOR_PWM_FREQUENCY;
	}

    //!Set the following configurations of the motor:
    //!1. Direction
    //!2. No. of steps to be moved
    //!3. Step Size configuration
	Motor3Pwm2.Direction = usnDirection;
	Motor3Pwm2.NumberOfSteps = (Uint32)((fDisplacement/MOTOR3_STEP_SIZE));//4724 steps
	if(Motor3Pwm2.NumberOfSteps < 1)
	{
		Motor3Pwm2.NumberOfSteps = 1;
	}
	Motor3Pwm2.StepSize = MOTOR3_STEP_CONFIGURATION;

    //!If motor is moving home direction then set the Sample home movement variable
    if(Motor3Pwm2.Direction == MOTOR3_HOME_DIRECTION)
    {
        m_bSample_Home_Movement = true;
    }

#if MOTOR_PWM_DEBUG_PRINT_ENABLE
	printf(" M3 *Freq: %d *Steps: %ld \n",Motor3Pwm2.Fequency,Motor3Pwm2.NumberOfSteps);
#endif

	if(m_bSample_Home_Movement)
	{
	    //!S-Curve look-up function for the motor start band and stop band of the motor
	    PWMGetSCurveLookUp(&Motor3Pwm2);
	    Motor3Pwm2.SCurveLookUpCounter = 0;
	}
	else
	{
	    //!Set the S-Curve look-up with the period and freq. configuration
	    Motor3Pwm2.SCurveLookUp[0] = (Uint16)(MOTOR_PWM_PERIOD_FACTOR/Motor3Pwm2.Fequency);
	}
	//!Set the status of the motor to start and intialize the timer counter to zero
	Motor3Pwm2.Status = START;
	Motor3Pwm2.TimerCounter = 0;
	Motor3Pwm2.TimeStamp[0] = CpuTimer0.InterruptCount;



    //!If the motor is moving towards home direction and if home position is
    //!detected stop the motor, else start the motor.
	if((SENSOR3_HOME_STATE == OIHomeSensor3State()) && \
	        Motor3Pwm2.Direction == MOTOR3_HOME_DIRECTION)
	{
        //!Disable the PWM to stop the motor movement and set the status to completed
        //!and direction to IDLE.
		PWMControl(PWM2A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor3Pwm2.Status = COMPLETED;
		//Motor3Pwm2.Direction = IDLE;
	}
	else
	{
	    //!Start the PWM to start the motor
		PWMControl(PWM2A, START, Motor3Pwm2.SCurveLookUp[0]);


	}

}
/******************************************************************************/
/*!
 * \fn void PWMConfigMotor4Param(float fDisplacement, Uint16 usnDirection,
 *          Uint16 StepFrequency)
 * \param fDisplacement motor displacement to be provided
 * \param usnDirection specifies the direction of the motor
 * \param StepFrequency specifies the step frequency for the motor
 *
 * \brief Configure Motor 4 PWM 5 parameters
 * \brief Description:
 *      Motor 4 related pwm parameters shall be configured here.
 *      PWM frequency, Steps & S-curve related parameters are calculated here
 *      PWM channel 5 for motor 4 configuration
 *
 * \return void
 */
/******************************************************************************/
void PWMConfigMotor4Param(float fDisplacement, Uint16 usnDirection,\
        Uint16 StepFrequency)
{
    //!Set the frequency of the Motor based on the step frequency recived.
    //!If it is less than the 32500, set the MOTOR freq. factor
    //!else set the PWM frequency for the motor movement
	if((StepFrequency * MOTOR4_FREQUENCY_FACTOR1) < 32500)
	{
		Motor4Pwm5.Fequency = (StepFrequency * MOTOR4_FREQUENCY_FACTOR1);
	}
	else
	{
		Motor4Pwm5.Fequency = MOTOR_PWM_FREQUENCY;
	}

    //!Set the following configurations of the motor:
    //!1. Direction
    //!2. No. of steps to be moved
    //!3. Step Size configuration
	Motor4Pwm5.Direction = usnDirection;
	Motor4Pwm5.NumberOfSteps = (Uint32)((fDisplacement/MOTOR4_STEP_SIZE));//4724 steps
	if(Motor4Pwm5.NumberOfSteps < 1)
	{
		Motor4Pwm5.NumberOfSteps = 1;
	}
	Motor4Pwm5.StepSize = MOTOR4_STEP_CONFIGURATION;

#if MOTOR_PWM_DEBUG_PRINT_ENABLE
	printf(" M4 *Freq: %d *Steps: %ld \n",Motor4Pwm5.Fequency,Motor4Pwm5.NumberOfSteps);
#endif

#if	MOTOR4_SCURVE_ENABLE
	//!S-Curve look-up function for the motor start band and stop band of the motor
	PWMGetSCurveLookUp(&Motor4Pwm5);
	Motor4Pwm5.SCurveLookUpCounter = 0;
#else
	//!Set the S-Curve look-up with the period and freq. configuration
	Motor4Pwm5.SCurveLookUp[0] = (Uint16)(MOTOR_PWM_PERIOD_FACTOR/Motor4Pwm5.Fequency);
#endif
    //!If the motor is moving towards home direction and if home position is
    //!detected stop the motor, else start the motor.
	Motor4Pwm5.Status = START;
	Motor4Pwm5.TimerCounter = 0;
	Motor4Pwm5.TimeStamp[0] = CpuTimer0.InterruptCount;
	m_bX_Home_Movement = true;//SKM_CHANGE_X_AXIS_HOME

    //!If the motor is moving towards home direction and if home position is
    //!detected stop the motor, else start the motor.
/*	if((SENSOR4_HOME_STATE == OIHomeSensor4State()) && \
	        Motor4Pwm5.Direction == MOTOR4_HOME_DIRECTION)
	{
        //!Disable the PWM to stop the motor movement and set the status to completed
        //!and direction to IDLE.
		PWMControl(PWM5A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor4Pwm5.Status = COMPLETED;
		Motor4Pwm5.Direction = IDLE;
	}
	else
	{
	    //!Start the PWM to start the motor
		PWMControl(PWM5A, START, Motor4Pwm5.SCurveLookUp[0]);
	}
*/
    //!Start the PWM to start the motor
    PWMControl(PWM5A, START, Motor4Pwm5.SCurveLookUp[0]);
}
/******************************************************************************/
/*!
 * \fn void PWMConfigMotor5Param(float fDisplacement, Uint16 usnDirection,
 *          Uint16 StepFrequency)
 * \param fDisplacement motor displacement to be provided
 * \param usnDirection specifies the direction of the motor
 * \param StepFrequency specifies the step frequency for the motor
 *
 * \brief Configure Motor 5 PWM 1 parameters
 * \brief Description:
 *      Motor 5 related pwm parameters shall be configured here.
 *      PWM frequency, Steps & S-curve related parameters are calculated here
 *      PWM channel 1 for motor 5 configuration
 *
 * \return void
 */
/******************************************************************************/
void PWMConfigMotor5Param(float fDisplacement, Uint16 usnDirection, \
           Uint16 StepFrequency)
{
    //!Set the frequency of the Motor based on the step frequency recived.
    //!If it is less than the 32500, set the MOTOR freq. factor
    //!else set the PWM frequency for the motor movement
	if((StepFrequency * MOTOR5_FREQUENCY_FACTOR1) < 32500)
	{
		Motor5Pwm1.Fequency = (StepFrequency * MOTOR5_FREQUENCY_FACTOR1);
	}
	else
	{
		Motor5Pwm1.Fequency = MOTOR_PWM_FREQUENCY;
	}

    //!Set the following configurations of the motor:
    //!1. Direction
    //!2. No. of steps to be moved
    //!3. Step Size configuration
	Motor5Pwm1.Direction = usnDirection;
	Motor5Pwm1.NumberOfSteps = (Uint32)((fDisplacement/MOTOR5_STEP_SIZE));//4724 steps
	if(Motor5Pwm1.NumberOfSteps < 1)
	{
		Motor5Pwm1.NumberOfSteps = 1;
	}

	Motor5Pwm1.StepSize = MOTOR5_STEP_CONFIGURATION;

#if MOTOR_PWM_DEBUG_PRINT_ENABLE
	printf(" M5 *Freq: %d *Steps: %ld \n",Motor5Pwm1.Fequency,Motor5Pwm1.NumberOfSteps);
#endif

#if	MOTOR5_SCURVE_ENABLE
	//!S-Curve look-up function for the motor start band and stop band of the motor
	PWMGetSCurveLookUp(&Motor5Pwm1);
	Motor5Pwm1.SCurveLookUpCounter = 0;
#else
	Motor5Pwm1.SCurveLookUp[0] = (Uint32)(MOTOR_PWM_PERIOD_FACTOR/Motor5Pwm1.Fequency);
#endif

    //!If the motor is moving towards home direction and if home position is
    //!detected stop the motor, else start the motor.
	Motor5Pwm1.Status = START;
	Motor5Pwm1.TimerCounter = 0;
	Motor5Pwm1.FBTimerCounter = 0;
	Motor5Pwm1.TimeStamp[0] = CpuTimer0.InterruptCount;

	if(Motor5Pwm1.Direction == MOTOR5_HOME_DIRECTION)
	{
	    m_bY_Home_Movement = true;//Y_AXIS_HOME_CHANGE
	}
	g_bYMotorFaultMonEn = true;

    //!If the motor is moving towards home direction and if home position is
    //!detected stop the motor, else start the motor.
	if((SENSOR5_HOME_STATE == OIHomeSensor5State())&& \
	        Motor5Pwm1.Direction == MOTOR5_HOME_DIRECTION)
	{
        //!Disable the PWM to stop the motor movement and set the status to completed
        //!and direction to IDLE.
		PWMControl(PWM1A, STOP, STOP_MOTOR_PWM_PERIOD);
		Motor5Pwm1.Status = COMPLETED;
		//Motor5Pwm1.Direction = IDLE;
	}
	else
	{
	    //!Start the PWM to start the motor
		PWMControl(PWM1A, START,Motor5Pwm1.SCurveLookUp[0]);
	}

}
/******************************************************************************/
/*!-----------------------------------------------------------------------------
 S-Curve Formula for 61 steps:
 Acceleration 	= 	Frequency/(1+exp((Steps/10-3)*(-1)))
 Deacceleration = 	Frequency/(1+exp(Steps/10-3))

 Acceleration 	= 	Frequency/(1+pow(2.71828, (Steps/10-3)*(-1)))
 Deacceleration = 	Frequency/(1+pow(2.71828, (Steps/10-3)))
--------------------------------------------------------------------------------
 S-Curve Formula for 36 steps:
 Acceleration 	= 	Frequency/(1+exp((Steps/5-3.5)*-1))
 Deacceleration = 	Frequency/(1+exp(Steps/5-3.5))

 Acceleration 	= 	Frequency/(1+pow(2.71828, (Steps/5-3.5)*(-1)))
 Deacceleration = 	Frequency/(1+pow(2.71828, (Steps/5-3.5)))
--------------------------------------------------------------------------------
 S-Curve Formula for 31 steps:
 Acceleration 	= 	Frequency/(1+exp((Steps/5-3)*-1))
 Deacceleration = 	Frequency/(1+exp(Steps/5-3))

 Acceleration 	= 	Frequency/(1+pow(2.71828, (Steps/5-3)*(-1)))
 Deacceleration = 	Frequency/(1+pow(2.71828, (Steps/5-3)))
 -------------------------------------------------------------------------------
 S-Curve Formula for 8 steps:
 Acceleration 	= 	Frequency/(1+exp((Steps/2-3.5)*-1))
 Deacceleration = 	Frequency/(1+exp(Steps/2-3.5))

 Acceleration 	= 	Frequency/(1+pow(2.71828, (Steps/2-3.5)*(-1)))
 Deacceleration = 	Frequency/(1+pow(2.71828, (Steps/2-3.5)))
------------------------------------------------------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*!
 * \fn void PWMGetSCurveLookUp(stPwmParameter *PwmParam)
 * \param *PwmParam pwm parameters to calculate S-Curve lookup table
 * \brief S-Curve lookup table generation
 * \brief Description:
 *      S-curve lookup table generation in PwmParam->SCurveLookUp[i]
 *      Based on step size & frequency, S-curve frequency looku[p table
 *      shall be created
 *
 * \return void
 */
/******************************************************************************/
#if 1
void PWMGetSCurveLookUp(stPwmParameter *PwmParam)
{
	double SCurveConst1 = 5.0, SCurveConst2 = 3.0;
	Uint16 i=0,Frequency=1000;
	//Uint16 SCurveFreqMin = (MIN_MOTOR_FREQUENCY*PwmParam->StepSize);
	//!Set S-Curve minimum freq
	Uint16 SCurveFreqMin = (PwmParam->uiMotorMinFreq*PwmParam->StepSize);
	//!set the S-Curve frequency
	Uint16 SCurveFreq = (PwmParam->Fequency-SCurveFreqMin);
	double expPower=0.0;

	//!Set the start band period, curve constant 1 and curve constant 2 based
	//!on step size
	switch(PwmParam->StepSize)
	{
	case FULL_STEP:
	    //!If the step size is FULL_STEP, StartBandPeriod = 31,
	    //!SCurveConst1 = 5.0, SCurveConst2 = 3.0
		PwmParam->StartBandPeriod = 31;
		SCurveConst1 = 5.0;
		SCurveConst2 = 3.0;
		break;

	case TWO_MICRO_STEP:
        //!If the step size is TWO_MICRO_STEP, StartBandPeriod = 65,
        //!SCurveConst1 = 8.0, SCurveConst2 = 4.0
		PwmParam->StartBandPeriod = 65;
		SCurveConst1 = 8.0;
		SCurveConst2 = 4.0;
		break;

	case FOUR_MICRO_STEP:
        //!If the step size is FOUR_MICRO_STEP, StartBandPeriod = 101,
        //!SCurveConst1 = 10.0, SCurveConst2 = 5.0
		PwmParam->StartBandPeriod = 101;
		SCurveConst1 = 10.0;
		SCurveConst2 = 5.0;
		break;

	case EIGHT_MICRO_STEP: case SIXTEEN_MICRO_STEP:
        //!If the step size is EIGHT_MICRO_STEP or SIXTEEN_MICRO_STEP,
        //!StartBandPeriod = 151, SCurveConst1 = 15.0, SCurveConst2 = 5.0
		PwmParam->StartBandPeriod = 151;
		SCurveConst1 = 15.0;
		SCurveConst2 = 5.0;
		break;


	default:
        //!By default set the StartBandPeriod = 41, SCurveConst1 = 15.0,
        //!SCurveConst2 = 5.0
		PwmParam->StartBandPeriod = 41;
		SCurveConst1 = 5.0;
		SCurveConst2 = 4.0;
		break;
	}

	//!Set the stop band period based on the no. of steps and start band period
	if(PwmParam->NumberOfSteps > (PwmParam->StartBandPeriod*2))
	{
		PwmParam->StopBandPeriod = PwmParam->NumberOfSteps - \
		        PwmParam->StartBandPeriod;
	}
	else
	{
	    //!Set the start band period, curve constant and stopband period
		PwmParam->StartBandPeriod = 9;
		SCurveConst1 = 4.0;
		SCurveConst2 = 2.0;
		PwmParam->StopBandPeriod = PwmParam->NumberOfSteps;
	}

	//!Set the frequency and S-Curve look-up table
	for(i=0;i<PwmParam->StartBandPeriod;i++)
	{
		expPower = (double)i/SCurveConst1 - SCurveConst2;
		Frequency =(Uint16)( ((SCurveFreq)/(1+pow(2.71828,\
		        (expPower*(-1.0))))) + SCurveFreqMin);
		PwmParam->SCurveLookUp[i] = (Uint16)(MOTOR_PWM_PERIOD_FACTOR/Frequency);
	}
}
#endif
/******************************************************************************/
/*!
 * \fn Uint16 PWMGetMotor1Status(void)
 * \brief Motor 1 pwm status
 * \b Description:
 *      Motor  1 pwm status shall be returned
 *
 * \return unsigned int Motor1Pwm8.Status whether motor is in RUN or IDLE state
 */
/******************************************************************************/
Uint16 PWMGetMotor1Status(void)
{
    //!Return the Motor 1 status
	return Motor1Pwm8.Status;
}
/******************************************************************************/
/*!
 * \fn Uint16 PWMGetMotor2Status(void)
 * \brief Motor 2 pwm status
 * \b Description:
 *      Motor  2 pwm status shall be returned
 *
 * \return unsigned int Motor2Pwm6.Status whether motor is in RUN or IDLE state
 */
/******************************************************************************/
Uint16 PWMGetMotor2Status(void)
{
    //!Return the Motor 2 status
	return Motor2Pwm6.Status;
}
/******************************************************************************/
/*!
 * \fn Uint16 PWMGetMotor3Status(void)
 * \brief Motor 3 pwm status
 * \b Description:
 *      Motor  3 pwm status shall be returned
 *
 * \return unsigned int Motor3Pwm2.Status whether motor is in RUN or IDLE state
 */
/******************************************************************************/
Uint16 PWMGetMotor3Status(void)
{
    //!Return the Motor 3 status
	return Motor3Pwm2.Status;
}
/******************************************************************************/
/*!
 * \fn Uint16 PWMGetMotor4Status(void)
 * \brief Motor 4 pwm status
 * \b Description:
 *      Motor  4 pwm status shall be returned
 *
 * \return unsigned int Motor4Pwm5.Status whether motor is in RUN or IDLE state
 */
/******************************************************************************/
Uint16 PWMGetMotor4Status(void)
{
    //!Return the Motor 4 status
	return Motor4Pwm5.Status;
}
/******************************************************************************/
/*!
 * \fn Uint16 PWMGetMotor5Status(void)
 * \brief Motor 5 pwm status
 * \b Description:
 *      Motor  5 pwm status shall be returned
 *
 * \return unsigned int Motor5Pwm1.Status whether motor is in RUN or IDLE state
 */
/******************************************************************************/
Uint16 PWMGetMotor5Status(void)
{
    //!Return the Motor 5 status
	return Motor5Pwm1.Status;
}
/******************************************************************************/
/*!
 * \fn void PWMSpiHomePosSense(Uint16 u16_RxData)
 * \brief Reads Sensor state
 * \brief  Description:
 *      This function returns the sensor  status
 *
 * \return void
 */
/******************************************************************************/

void PWMSpiHomePosSense(volatile Uint16 u16_RxData)
{

	Motor1Pwm8.HomePosSenseStatus = 1;
//	u16_prevRxData = u16_RxData;

}
/******************************************************************************/
/*!
 * \fn int16 PWMMotor1SensorPoll(void)
 * \brief Optical sensor 1 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 1
 *
 * \return return 1 if sensor sensed else 0
 */
/******************************************************************************/
int16 PWMMotor1SensorPoll(void)
{
    //!If the motor is moving towards home direction and if the home position
    //!stop the PWM control of the motor and set the motor status to complete and idle
#if 0
	if((SENSOR1_HOME_STATE == ((unsigned int)OIHomeSensor1State())) && \
	        (Motor1Pwm8.Direction == MOTOR1_HOME_DIRECTION) && \
	        (Motor1Pwm8.HomeSensorControl == SENSOR_INTERRUPT_ENABLE))
#endif
	    if((SENSOR1_HOME_STATE == ((unsigned int)OIHomeSensor1State())) && \
	            (Motor1Pwm8.Direction == MOTOR1_HOME_DIRECTION))
	{
		return 1;
	}
	return 0;
}
/******************************************************************************/
/*!
 * \fn int16 PWMMotor2SensorPoll(void)
 * \brief Optical sensor 2 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 2
 *
 * \return return 1 if sensor sensed else 0
 */
/******************************************************************************/
int16 PWMMotor2SensorPoll(void)
{
    //!If the motor is moving towards home direction and if the home position
    //! is detected then return 1
	if((SENSOR2_HOME_STATE == ((unsigned int)OIHomeSensor2State())) && \
	        Motor2Pwm6.Direction == MOTOR2_HOME_DIRECTION)
	{
		return 1;
	}
	return 0;
}
/******************************************************************************/
/*!
 * \fn int16 PWMMotor3SensorPoll(void)
 * \brief Optical sensor 3 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 3
 *
 * \return return 1 if sensor sensed else 0
 */
/******************************************************************************/
int16 PWMMotor3SensorPoll(void)
{
    //!If the motor is moving towards home direction and if the home position
    //! is detected then return 1
	if((SENSOR3_HOME_STATE == ((unsigned int)OIHomeSensor3State())) && \
	        Motor3Pwm2.Direction == MOTOR3_HOME_DIRECTION)
	{
		return 1;
	}
	return 0;
}
/******************************************************************************/
/*!
 * \fn int16 PWMMotor4SensorPoll(void)
 * \brief Optical sensor 4 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 4
 *
 * \return return 1 if sensor sensed else 0
 */
/******************************************************************************/
int16 PWMMotor4SensorPoll(void)
{
    //!If the motor is moving towards home direction and if the home position
    //! is detected then return 1
	if((SENSOR4_HOME_STATE == ((unsigned int)OIHomeSensor4State())) && \
	        Motor4Pwm5.Direction == MOTOR4_HOME_DIRECTION)
	{
		return 1;
	}
	return 0;
}
/******************************************************************************/
/*!
 * \fn int16 PWMMotor5SensorPoll(void)
 * \brief Optical sensor 5 state
 * \brief  Description:
 *      PWM control based on optical sensor or home position sensor input
 *      Home sensor 5
 *
 * \return return 1 if sensor sensed else 0
 */
/******************************************************************************/
int16 PWMMotor5SensorPoll(void)
{
    //!If the motor is moving towards home direction and if the home position
    //! is detected then return 1
	if((SENSOR5_HOME_STATE == ((unsigned int)OIHomeSensor5State())) && \
	        Motor5Pwm1.Direction == MOTOR5_HOME_DIRECTION)
	{
		return 1;
	}
	return 0;
}
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

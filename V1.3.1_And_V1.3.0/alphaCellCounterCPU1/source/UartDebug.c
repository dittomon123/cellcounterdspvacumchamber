/******************************************************************************/
/*! \file UartDebug.c
 *
 *  \brief UART initialization
 *
 *  \b Description:
 *      Timer configuration and wrapper function for timer operations
 *
 *   $Version: $ 0.2
 *   $Date:    $ Aug-18-2015
 *   $Author:  $ GURUDUTT R
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_uart_module
//! @{
//*****************************************************************************

#include"UartDebug.h"

/******************************************************************************/
/*!
 * \fn int configureUART(uint32_t ui32Baud)
 * \param ui32Baud
 * \brief UART periheral initialization
 * \brief  Description:
 *      This function initializes UART periheral registers
 *
 * \return int
 */
/******************************************************************************/
int configureUART(uint32_t ui32Baud)
{

//	configureUART0();
	scia_fifo_init();	   //!Initialize the SCI FIFO
	scia_init(ui32Baud);

	//! uart test message
	testUART();
	return 0;
}
/******************************************************************************/
/*!
 * \fn void configureUART0(void)
 * \brief UART periheral initialization
 * \brief  Description:
 *      This function initializes UART periheral registers
 * \return void
 */
/******************************************************************************/
void configureUART0(void){

	//  SysCtlPeripheralEnable(SYSCTL_PERIPH_SCI1);
#if 0
	  EALLOW;
	  GpioCtrlRegs.GPAMUX1.bit.GPIO9 = 0x2;
	  GpioCtrlRegs.GPAGMUX1.bit.GPIO9 = 0x1;

	  GpioCtrlRegs.GPAPUD.bit.GPIO9 = 0;
	  GpioCtrlRegs.GPAQSEL1.bit.GPIO9 = 3;
	  GpioCtrlRegs.GPADIR.bit.GPIO9 = 0;

	  GpioCtrlRegs.GPAMUX1.bit.GPIO8 = 0x2;
	  GpioCtrlRegs.GPAGMUX1.bit.GPIO8 = 0x1;

	  GpioCtrlRegs.GPAPUD.bit.GPIO8 = 0;
	  GpioCtrlRegs.GPADIR.bit.GPIO8 = 1;
	  EDIS;
#else

#endif
}
/******************************************************************************/
/*!
 * \fn void scia_init(uint32_t ui32Baud)
 * \param ui32Baud specifies the baud rate
 *
 * \brief UART periheral initialization
 * \brief  Description:
 *      This function initializes UART periheral registers
 *      115200 baud, 1 stop bit,  No loopback, async mode, idle-line protocol
 *		enable TX, RX, internal SCICLK, isable RX ERR, SLEEP, TXWAKE
 * \return void
 */
/******************************************************************************/
void scia_init(uint32_t ui32Baud)
{


    //!Note: Clocks were turned on to the SCIA peripheral
    //!in the InitSysCtrl() function

 	SciaRegs.SCICCR.all =0x0007;   // 1 stop bit,  No loopback
                                   // No parity,8 char bits,
                                  // async mode, idle-line protocol
	SciaRegs.SCICTL1.all =0x0003;  // enable TX, RX, internal SCICLK,
                                   // Disable RX ERR, SLEEP, TXWAKE
	SciaRegs.SCICTL2.all =0x0003;
	SciaRegs.SCICTL2.bit.TXINTENA =1;
	SciaRegs.SCICTL2.bit.RXBKINTENA =1;

	//********************************************Baud Rate 115200**************

	 uint32_t ui32UARTClk;
	 uint32_t ui32Div;

	 ui32UARTClk = SysCtlLowSpeedClockGet(SYSTEM_CLOCK_SPEED);

	 ui32Div = ((ui32UARTClk  / (ui32Baud * 8)) - 1);

	 SciaRegs.SCIHBAUD.all = (ui32Div & 0xFF00) >> 8;
	 SciaRegs.SCILBAUD.all = ui32Div & 0x00FF;

	//**************************************************************

//	SciaRegs.SCICCR.bit.LOOPBKENA =1; // Enable loop back
	SciaRegs.SCICTL1.all =0x0023;     // Relinquish SCI from Reset
}
// Transmit a character from the SCI'
/******************************************************************************/
/*!
 * \fn void scia_xmit(int nData)
 * \param nData specifies the data to be transmit
 *
 * \brief UART periheral transmit
 * \brief  Description:
 *      This function transmits data using configured baud
 * \return void
 */
/******************************************************************************/
void scia_xmit(int nData)
{
	while(!SciaRegs.SCICTL2.bit.TXEMPTY);
    SciaRegs.SCITXBUF.all=nData;
    while(!SciaRegs.SCICTL2.bit.TXEMPTY);
//    DELAY_US(100);
}
// Receive a character to SCI'
/******************************************************************************/
/*!
 * \fn Uint16 UARTReceiveDataBytes16bit(volatile int *str)
 * \param *str data to be received
 *
 * \brief UART receive
 * \brief  Description:
 *      This function receives data from external device/PC using
 *      configured baud
 * \return Uint16
 */
/******************************************************************************/
Uint16 UARTReceiveDataBytes16bit(volatile int *str)
{
	Uint16 i=0;

#if 0
	do
	{
		str[i++] = SciaRegs.SCIRXBUF.all;
		if(i >= 10)
		{
			break;
		}
	}
	// wait for RRDY/RXFFST =1 for 1 data available in FIFO
	while(SciaRegs.SCIFFRX.bit.RXFFST != 1);

	if(i >= 10)
	{
	    return 1;
	}
	else
	{
		return 0;
	}

#endif

	for(i=0;i<10;i++)
	{
		str[i] = ('a' + i );
	}
	return 1;
}
// Initialize the SCI FIFO
/******************************************************************************/
/*!
 * \fn void scia_fifo_init(void)
 *
 * \brief UART FIFO config
 * \brief  Description:
 *      This function configures FIFO
 * \return void
 */
/******************************************************************************/
void scia_fifo_init(void)
{
    SciaRegs.SCIFFTX.all=0xE040;
    SciaRegs.SCIFFRX.all=0x2044;
    SciaRegs.SCIFFCT.all=0x0;
}
/******************************************************************************/
/*!
 * \fn void testUART(void)
 *
 * \brief UART test function
 * \brief  Description:
 *      This function tests UART peripheral by sending initial
 *      string with device info
 * \return void
 */
/******************************************************************************/
void testUART(void)
{

	char DebugPrintBuf[50];
	sprintf(DebugPrintBuf, "\r\n Cell Counter:alpha HW rev1.0 \n");
	UartDebugPrint((int*)DebugPrintBuf, 50);

	sprintf(DebugPrintBuf,"\r\n DEVICE REVID: %ld ",DevCfgRegs.REVID);
	UartDebugPrint((int*)DebugPrintBuf,40);
	sprintf(DebugPrintBuf," PARTIDH: %ld ",DevCfgRegs.PARTIDH.all);
	UartDebugPrint((int*)DebugPrintBuf,40);
	sprintf(DebugPrintBuf," PARTIDL: %ld \n",DevCfgRegs.PARTIDL.all);
	UartDebugPrint((int*)DebugPrintBuf,40);
}
/******************************************************************************/
/*!
 * \fn void UARTSendDataBytes16bit(int *DataBuffer, int NoOfBytes)
 * \param *DataBuffer specifies the data to to be sent
 * \param NoOfBytes specifies the number of bytes to be sent
 *
 * \brief Send data wrapper function
 * \brief  Description:
 *      This function sends data from buffer and number of byts parameters
 *      passed to the function
 * \return void
 */
/******************************************************************************/
void UARTSendDataBytes16bit(int *DataBuffer, int NoOfBytes)
{
	int count = 0;
	scia_xmit('\r');
	scia_xmit('\n');
	for(count = 0; count < NoOfBytes ; count++)
	{
		scia_xmit(DataBuffer[count]);
	}
}
/******************************************************************************/
/*!
 * \fn void UARTSendADCRawData16bit(int Data)
 * \param Data specifies the data to to be sent
 *
 * \brief Send data wrapper function
 * \brief  Description:
 *      This function sends data passed to the function
 * \return void
 */
/******************************************************************************/
void UARTSendADCRawData16bit(int Data)
{
	scia_xmit(Data>>8);
	scia_xmit(0x0F & Data);
}
/******************************************************************************/
/*!
 * \fn void UartDebugPrint(int *DataBuffer, int NoOfBytes)
 * \param *DataBuffer specifies the data to to be sent
 * \param NoOfBytes specifies the number of bytes to be sent
 *
 * \brief Send data wrapper function
 * \brief  Description:
 *      This function sends data from buffer and number of byts parameters
 *      passed to the function
 * \return void
 */
/******************************************************************************/
void UartDebugPrint(int *DataBuffer, int NoOfBytes)
{
	int count = 0;
	for(count = 0; count < NoOfBytes ; count++)
	{
		scia_xmit((0x00FF & DataBuffer[count]));
		if(DataBuffer[count] == '\0')
			break;
	}
}
/******************************************************************************/
/*!
 * \fn int16 getUartRxData(int *DataBuffer)
 * \param *DataBuffer specifies the data to to be received
 * \brief Send data wrapper function
 * \brief  Description:
 *      This function receives the data from PC
 *      passed to the function
 * \return void
 */
/******************************************************************************/
int16 getUartRxData(int *DataBuffer)
{
	int16 Counter=0;
	while(SciaRegs.SCIFFRX.bit.RXFFST != 0)
	{
		*DataBuffer++ = SciaRegs.SCIRXBUF.all;
		Counter++;
		if(Counter >= 19)
			break;
	}
    SciaRegs.SCIFFRX.bit.RXFFOVRCLR=1;   // Clear Overflow flag
	*DataBuffer++ = (int)'\0';
	return (Counter);

}
#if 0

interrupt void sciaTxFifoIsr(void)
{
    Uint16 i;
    for(i=0; i< 2; i++)
    {
       SciaRegs.SCITXBUF.all=sdataA[i];     // Send data
    }

    SciaRegs.SCIFFTX.bit.TXFFINTCLR=1;  // Clear SCI Interrupt flag
    PieCtrlRegs.PIEACK.all|=0x100;      // Issue PIE ACK
}

interrupt void sciaRxFifoIsr(void)
{
    Uint16 i;
    for(i=0;i<2;i++)
    {
       rdataA[i]=SciaRegs.SCIRXBUF.all;  // Read data
    }

    SciaRegs.SCIFFRX.bit.RXFFOVRCLR=1;   // Clear Overflow flag
    SciaRegs.SCIFFRX.bit.RXFFINTCLR=1;   // Clear Interrupt flag

    PieCtrlRegs.PIEACK.all|=0x100;       // Issue PIE ack
}

#endif

//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

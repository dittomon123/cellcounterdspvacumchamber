/******************************************************************************/
/*! \file Timer.c
 *
 *  \brief Timer initialization
 *
 *  \b Description:
 *      Timer configuration and wrapper function for timer operations
 *
 *   $Version: $ 0.2
 *   $Date:    $ Jan-21-2015
 *   $Author:  $ 20040919, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_timer_module
//! @{
//*****************************************************************************

#include "Timer.h"

/******************************************************************************/
/*!
 * \fn void Initialize_Timer(void)
 * \brief timer initialization
 * \brief Description:
 *      This function initialize CPU timers
 * \return void
 */
/******************************************************************************/
void Initialize_Timer(void)
{
    //!Initilaze CPU timers
	InitCpuTimers();

	//!Configure timer 0
	Configure_Timer0();

	//!Configure timer 1
	Configure_Timer1();

	//!Start timer 0
	Start_Timer0();

	//!Start timer 1
	Stop_Timer1();
}

/******************************************************************************/
/*!
 * \fn void Start_Timer0(void)
 * \brief timer start
 * \brief Description:
 *      This function starts CPU timers 0
 * \return void
 */
/******************************************************************************/
void Start_Timer0(void)
{
	//CpuTimer0Regs.TCR.all = 0x4000;
    //Use write-only instruction to set TSS bit = 0
    //!Start CPU timer 0
	StartCpuTimer0();
}
/******************************************************************************/
/*!
 * \fn void Start_Timer1(void)
 * \brief timer start
 * \brief Description:
 *      This function starts CPU timer 1
 * \return void
 */
/******************************************************************************/
void Start_Timer1(void)
{
    //CpuTimer1Regs.TCR.all = 0x4000;
    // Use write-only instruction to set TSS bit = 0
    //!Start CPU timer 1
	StartCpuTimer1();
}
/******************************************************************************/
/*!
 * \fn void Configure_Timer0(void)
 * \brief timer configuration
 * \brief Description:
 *      This function configures CPU timers 0
 * \return void
 */
/******************************************************************************/
void Configure_Timer0(void)
{
	//!Configure CPU-Timer 0 to interrupt every second:
	// 200MHz CPU Freq, 1 second Period (in uSeconds)
	ConfigCpuTimer(&CpuTimer0, 200, TIMER1_CYCLE_TIME_uS);

}
/******************************************************************************/
/*!
 * \fn void Configure_Timer1(void)
 * \brief timer configuration
 * \brief Description:
 *      This function configures CPU timers 1
 * \return void
 */
/******************************************************************************/
void Configure_Timer1(void)
{
	//!Configure CPU-Timer 1 to interrupt every second:
	// 200MHz CPU Freq, 1 second Period (in uSeconds)
	ConfigCpuTimer(&CpuTimer1, 200, TIMER2_CYCLE_TIME_uS);
}
/******************************************************************************/
/*!
 * \fn void Stop_Timer0(void)
 * \brief timer stop
 * \brief Description:
 *      This function stops CPU timers 0
 * \return void
 */
/******************************************************************************/
void Stop_Timer0(void)
{
    //!Use write-only instruction to set TSS bit = 0 to stop CPU timer 0
	CpuTimer0Regs.TCR.all = 0x0010;
}
/******************************************************************************/
/*!
 * \fn void Stop_Timer1(void)
 * \brief timer stop
 * \brief Description:
 *      This function stops CPU timers 1
 * \return void
 */
/******************************************************************************/
void Stop_Timer1(void)
{
    //!Use write-only instruction to set TSS bit = 0 to stop CPU timer 0
	CpuTimer1Regs.TCR.all = 0x0010;
}


//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

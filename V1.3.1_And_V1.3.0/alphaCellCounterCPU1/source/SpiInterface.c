/******************************************************************************/
/*! \file SpiInterface.c
 *
 *  \brief SPI interface for solenoid valve driver
 *
 *  \b Description:
 *      Solenoid Valves are controlled through SPI interface
 *      Here, SPI intialization and solenoid valve control through
 *      wrapper functions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Apr-09-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_spi_module
//! @{
//*****************************************************************************
#include "SpiInterface.h"

#define SPI_INTERFACE_VALVE_OPERATION			1

#include<stdio.h>
#include<stdlib.h>
#include"UartDebug.h"
#ifdef SPI_DEBUG_PRINT_ENABLE
//#include<stdio.h>
//#include<stdlib.h>
#endif

/******************************************************************************/
/*!
 *
 * \var Uint16 sdata1=0;
 * \var Uint16 sdata2=0;
 * \var Uint16 sdata3=0;
 * \brief variables are used operate solenoid valves through SPI
 * 		  total 13 valves are operated using thress variables
 */
/******************************************************************************/
//Uint16 a,b;
Uint16 sdata1=0;
Uint16 sdata2=0;
Uint16 sdata3=0;

/******************************************************************************/
/*!
 * \fn void SIInitialize_SpiValve(void)
 * \brief SPI periheral initialization
 * \b Description:
 *      This function initializes SPI periheral registers
 *
 * \return void
 */
/******************************************************************************/
void SIInitialize_SpiValve(void)
{
#if SPI_INTERFACE_VALVE_OPERATION

    //!Reset on, falling edge with delay, 8-bit char bits
	SpicRegs.SPICCR.all =0x0007;
	//!Enable master mode, normal phase,
	//!Enable talk, and SPI int disabled.
	SpicRegs.SPICTL.all =0x0006;

	SpicRegs.SPISTS.all=0x0000;
	// LSPCLK/50
	SpicRegs.SPIBRR.all = 0x0031;
	//!LoopBack Disabled
    SpicRegs.SPICCR.all =0x0087;
    //!Set so breakpoints don't disturb xmission
    SpicRegs.SPIPRI.bit.FREE = 1;

    //!Initialize the Spi FIFO
    SISPI_Fifo_Initialize();
#endif
}
/******************************************************************************/
/*!
 * \fn void SISPI_Fifo_Initialize(void)
 * \brief SPI periheral FIFO initialization
 * \brief  Description:
 *      This function initializes SPI periheral FIFO registers
 *
 * \return void
 */
/******************************************************************************/
void SISPI_Fifo_Initialize(void)
{
    //!Initialize SPI FIFO registers
    SpicRegs.SPIFFTX.all=0xE040;
    //!Data is transferred to SPITXBUF immediately upon completion
    //!of transmission of the previous data
    SpicRegs.SPIFFCT.all=0x0000;

    //SpicRegs.SPIFFRX.all=0x2044;
}
/******************************************************************************/
/*!
 * \fn void SISPI_WriteData(Uint16 x)
 * \param x specifies the data to be transmitted to peripheral
 *
 * \brief SPI Write function
 * \brief Description:
 *      This function writes data to SPI transmit buffer
 *
 * \return void
 */
/******************************************************************************/
void SISPI_WriteData(Uint16 x)
{
	SpicRegs.SPITXBUF=x<<8;
}
/******************************************************************************/
/*!
 * \fn SISPI_ReadData(Uint16 rdata1)
 * \param rdata1 specifies the received data from SPI peripheral
 * \brief SPI Write function
 * \brief Description:
 *      This function read data to SPI receive buffer
 * \return Uint16 rdata1
 */
/******************************************************************************/
Uint16 SISPI_ReadData(Uint16 rdata1)
{
	rdata1=SpicRegs.SPIRXBUF;
	return rdata1;
}
/******************************************************************************/
/*!
 * \fn void SIDelay_Loop(void)
 * \brief Delay function
 * \brief  Description:
 *      Wrapper function for Delay function
 *
 * \return void
 */
/******************************************************************************/
void SIDelay_Loop(void)
{
	DELAY_US(800);
}
/******************************************************************************/
/*!
 * \fn void SIUpdateValveStatus(Uint16 ValveStatus_1, Uint16 ValveStatus_2)
 * \param ValveStatus_1	specifies the valve output of V1 to V8
 * \param ValveStatus_2 specifies the valve output of V9 to V16
 *
 * \brief Write Valve state
 * \brief  Description:
 *      This function writes valve state to Valve driver
 *
 * \return void
 */
/******************************************************************************/
void SIUpdateValveStatus(Uint16 ValveStatus_1, Uint16 ValveStatus_2)
{

#if SPI_INTERFACE_VALVE_OPERATION
	if(sdata2 != ValveStatus_2 || sdata3 != ValveStatus_1)
	{
		//!Read data from model
		sdata3 = ValveStatus_1;
		sdata2 = ValveStatus_2;

		//!Shift data by 8 bits
		sdata1 = 0x0000;

		//!Write sdata1 to SPI
		SISPI_WriteData(sdata1);
		//!Write sdata2 to SPI
		SISPI_WriteData(sdata2);
		//!Write sdata3 to SPI
		SISPI_WriteData(sdata3);

/*
#if SPI_DEBUG_PRINT_ENABLE
	//printf("V:\t%ld\t%x\t%x \n",(CpuTimer0.InterruptCount*2), \
		(Uint16)ValveStatus_1,(Uint16)ValveStatus_1);
#endif
*/
	}
//	DELAY_US(100);
#endif
}
/******************************************************************************/
/*!
 * \fn void SIErrorSpiValve(void)
 * \brief error function
 * \brief  Description:
 *      Wrapper function for Valve driver error check
 *
 * \return void
 */
/******************************************************************************/
void SIErrorSpiValve(void)
{


}
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

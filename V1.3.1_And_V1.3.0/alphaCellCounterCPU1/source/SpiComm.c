/******************************************************************************/
 /*!
 *File Name 	: SpiComm.c
 *Project ID	: PRJA2802
 *Pupose		: SPI Communication Class for Configuration of
 *                  SPI required for Communication between Sitara and Delfino.
 *Created Date	: 09-Dec-2015
 *Created by 	: R Gurudutt / SamyukthaBS
 *Version		: 1.0
 ******************************************************************************/

#include "F28x_Project.h"
#include "SpiComm.h"
#include "SpiInterface.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "CommProtocol.h"
#include <string.h>
#include "InitGpio.h"
#include "Timer.h"
#include"UartDebug.h"


extern char DebugPrintBuf[50];

/******************************************************************************/
/*!
 * \var SpiCommBuf
 * \brief  Structure to store the  data to be transmitted and received data from DMA
 */
/******************************************************************************/
SpiCommBuffer SpiCommBuf;

/******************************************************************************/
/*!
 * \brief DATA_SECTION();
 * \brief  map the RX & TX data of DMA to memory
 */
/******************************************************************************/
#pragma DATA_SECTION(SpiCommBuf, ".RAMGS8_SpiCommRxTx");

/******************************************************************************/
/*!
 * \var SpiComm
 * \brief  Structure to store the transmit and recieve status
 */
/******************************************************************************/
SpiCommParam SpiComm;

/******************************************************************************/
/*!
 * \var DMADest
 * \brief  pointer of the DMA destination to transmit and Recieve data
 */
/******************************************************************************/
volatile Uint16 *DMADest;

/******************************************************************************/
/*!
 * \var DMADest
 * \brief  pointer of the DMA source to transmit and Recieve data
 */
/******************************************************************************/
volatile Uint16 *DMASource;

/******************************************************************************/
/*!
 * \var m_bDataReceived
 * \brief  If data is received from DMA buffer, the variable is set if valid
 * data is received from Processor
 */
/******************************************************************************/
bool_t m_bDataReceived = 0;

/******************************************************************************/
/*!
 * \var m_usnSPIRxData
 * \brief  Array to store the received data from the processor
 */
/******************************************************************************/
uint16_t m_usnSPIRxData[MEM_BUFFER_SIZE * COMM_QUEUE_LENGTH];

/******************************************************************************/
/*!
 * \var m_bTxPendingFlag
 * \brief  This variable is set if any data is pending to be transmitted to processor
 */
/******************************************************************************/
bool_t m_bTxPendingFlag = FALSE;

/******************************************************************************/
/*!
 * \var g_bBusyBitStatus
 * \brief  This variable is set if controller is busy in performing any sequences
 */
/******************************************************************************/
bool_t g_bBusyBitStatus = FALSE;

extern volatile bool_t m_bTxInterruptStatus;
extern bool_t m_bTxToDMAFlag;
extern bool_t m_bMutiplePackets;
extern bool_t m_bLastMultiplePacket;
extern bool_t m_bSPIWriteControlStatus;
extern ST_SPI_PACKET_FRAME m_stPrevTxPacket;

/******************************************************************************/
/*!
 * \fn void SPIConfigCommPortAndDMA(void)
 * \brief  This function initializes the SPI GPIO Registers and the DMA Registers
 *
 * \return void
 */
/******************************************************************************/
void SPIConfigCommPortAndDMA()
{
	/*! Initialise the SPI A GPIO Registers*/
	SPIInitCommGpioReg();
	/*! Configure the DMA Registers*/
	SPIConfigCommDmaReg();
}
/******************************************************************************/
/*!
 * \fn void SPIInitCommGpioReg()
 * \brief  This function initializes the SPIA engine
 * \param void
 * \return void
 */
/******************************************************************************/
void SPIInitCommGpioReg(void)
{
	/*! Configure the SPIA GPIO's*/
    IGGpioConfig_SpiComm();
}
/******************************************************************************/
/*!
 * \fn void SPIConfigCommDmaReg()
 * \brief This function configures Spi DMA register
 * \param void
 * \return void
 */
/******************************************************************************/
void SPIConfigCommDmaReg(void)
{
	/*! Clear the SPI Tx And Rx Status*/
	SPIClearCommTxStatus();
	SPIClearCommRxStatus();

	/*!Initialise the Interrupt service routine*/
	EALLOW;  // This is needed to write to EALLOW protected registers
	PieVectTable.DMA_CH5_INT= &local_DMACH5_ISR;
	PieVectTable.DMA_CH6_INT= &local_DMACH6_ISR;
	EDIS;   // This is needed to disable write to EALLOW protected registers

	/*! Initialise the DMA*/
	DMAInitialize();

	/*! Configure the DMA Rx Registers*/
	SPIConfigCommDmaRx();
	/*! Configure the DMA Tx Registers*/
	SPIConfigCommDmaTx();
	/*! Configure the SPI FIFO Registers*/
	SPIConfigCommFifoReg();
	/*! Configure the SPI registers*/
	SPIConfigCommReg();

	EALLOW;
	CpuSysRegs.SECMSEL.bit.PF2SEL = 1;
	EDIS;
	/*! Enable the required interrupts*/
	PieCtrlRegs.PIECTRL.bit.ENPIE = 1;   // Enable the PIE block
	PieCtrlRegs.PIEIER7.bit.INTx5 = 1;	// Enable PIE Group 7, INT 1 (DMA CH5)
	PieCtrlRegs.PIEIER7.bit.INTx6 = 1;	// Enable PIE Group 7, INT 2 (DMA CH6)
	IER |= M_INT7;						// Enable CPU INT6
	EINT;

	/*! Clear the DMA Tx and Rx Buffers*/
	SPIClearCommBuffer(SpiCommBuf.SpiDmaTxData, (MEM_BUFFER_SIZE * COMM_QUEUE_LENGTH));
	SPIClearCommBuffer(SpiCommBuf.SpiDmaRxData, (MEM_BUFFER_SIZE * COMM_QUEUE_LENGTH));
}
/******************************************************************************/
/*!
 * \fn void SPIResetCommDmaAddr(void)
 * \brief Configuration Spi DMA address
 * \brief Description:
 *      This function configures Spi DMA address
 *
 * \return void
 */
/******************************************************************************/
void SPIResetCommDmaAddr(void)
{
	/*! Configure DMA Channel 5 and 6 Address*/
	DMACH5AddrConfig(&SpiaRegs.SPITXBUF,(volatile Uint16 *)SpiCommBuf.SpiDmaTxData);
	DMACH6AddrConfig((volatile Uint16 *)SpiCommBuf.SpiDmaRxData,&SpiaRegs.SPIRXBUF);
}
/******************************************************************************/
/*!
 * \fn void SPIResetCh5DmaAdd(void)
 * \brief Configuration Spi DMA address
 * \brief Description:
 *      This function configures Spi DMA Channel5 address
 * \return void
 */
/******************************************************************************/
void SPIResetCh5DmaAdd(void)
{
	/*!Configure only DMA Channel 5 Address*/
	DMACH5AddrConfig(&SpiaRegs.SPITXBUF,(volatile Uint16 *)SpiCommBuf.SpiDmaTxData);

}
/******************************************************************************/
/*!
 * \fn void SPIResetCh6DmaAdd(void)
 * \brief Configuration Spi DMA address
 * \brief Description:
 *      This function configures Spi DMA Channel6 address
 * \return void
 */
/******************************************************************************/
void SPIResetCh6DmaAdd(void)
{
	/*! Configure only DMA Channel 6 Address*/
	DMACH6AddrConfig((volatile Uint16 *)SpiCommBuf.SpiDmaRxData,&SpiaRegs.SPIRXBUF);
}
/******************************************************************************/
/*!
 * \fn bool_t SPIGetCommTxStatus(void)
 * \brief Transmission status
 * \brief Description:
 *      This function returns the state of transmission
 *
 * \return SpiComm.TxStatus == 1 if Transmission completed
 */
/******************************************************************************/
bool_t SPIGetCommTxStatus(void)
{
	/*!return the SPI Tx Status*/
	return SpiComm.TxStatus;
}
/******************************************************************************/
/*!
 * \fn bool_t SPIGetCommRxStatus(void)
 * \brief Reception status
 * \brief Description:
 *      This function returns the state of Reception
 *
 * \return SpiComm.RxStatus == 1 if  Reception completed
 */
/******************************************************************************/
bool_t SPIGetCommRxStatus(void)
{
	/*!return the SPI Rx Status*/
	return SpiComm.RxStatus;
}
/******************************************************************************/
/*!
 * \fn void SPIClearCommTxStatus(void)
 * \brief Transmission status
 * \brief Description:
 *      This function Clear the state of transmission
 * \return void
 */
/******************************************************************************/
void SPIClearCommTxStatus(void)
{
	/*!Clear the SPI Tx Status*/
	SpiComm.TxStatus = 0;
}
/******************************************************************************/
/*!
 * \fn void SPISetCommTxStatus(void)
 * \brief Transmission status
 * \brief Description:
 *      This function sets the state of transmission when Channel 5 ISR is serviced
 * \return void
 */
/******************************************************************************/
void SPISetCommTxStatus(void)
{
	/*! Set the SPI Tx Status*/
	SpiComm.TxStatus = 1;
}
/******************************************************************************/
/*!
 * \fn void SPISetCommRxStatus()
 * \brief Receive status
 * \brief Description:
 *       This function sets the state of Reception when Channel 6 ISR is serviced
 * \return void
 */
/******************************************************************************/
void SPISetCommRxStatus(void)
{
	/*! Set the SPI Rx Status*/
	SpiComm.RxStatus = 1;
}
/******************************************************************************/
/*!
 * \fn void SPIClearCommRxStatus(void)
 * \brief Reception status
 * \brief Description:
 *      This function Clear the state of Reception
 * \return void
 */
/******************************************************************************/
void SPIClearCommRxStatus(void)
{
	/*!Clear the SPI Rx Status*/
	SpiComm.RxStatus = 0;
}
/******************************************************************************/
/*!
 * \fn void SPIClearCommBuffer()
 * \param1 Uint16* unOrigin
 * \param2 Uint16 unLength
 * \brief Description:
 *      This function Clear the SPI Buffer
 * \return void
 */
/******************************************************************************/
void SPIClearCommBuffer(Uint16* unOrigin, Uint16 unLength)
{
	Uint16 i=0;

	for(i=0;i<unLength;i++)
	{
		*(unOrigin+i) = 0x0000;
	}
}
/******************************************************************************/
/*!
 * \fn void SPIActivateCommPort()
 * \brief Description:
 *     Start DMA Channel 6 and Channel 5
 * \return void
 */
/******************************************************************************/
void SPIActivateCommPort()
{
    //!Start the DMA channel 5 and DMA channel 6
	StartDMACH6();
	StartDMACH5();
}
/******************************************************************************/
/*!
 * \fn void SPIDeactivateCommPort()
 * \brief Description:
 *     Stop the running of DMA Channel 6 and Channel 5
 * \return void
 */
/******************************************************************************/
void SPIDeactivateCommPort()
{
    //!Stop DMA channel 5 and DMA channel 6
	SPIStopDMACH5();
	SPIStopDMACH6();
}
/******************************************************************************/
/*!
 * \fn void SPISetCommCtrlTxrIntReq()
 * \brief Description:
 *     Set the SPI Communication Transmit Interrupt GPIO to HIGH
 * \return void
 */
/******************************************************************************/
void SPISetCommCtrlTxrIntReq()
{
	EALLOW;
	//!Set the SPI interrupt to Sitara to high
	GpioDataRegs.GPFDAT.bit.GPIO165 = 1;
	EDIS;
}
/******************************************************************************/
/*!
 * \fn void SPIClearCommCtrlTxrIntReq()
 * \brief Description:
 *     Clear the SPI Communication Transmit Interrupt GPIO to Low
 * \return void
 */
/******************************************************************************/
void SPIClearCommCtrlTxrIntReq()
{
	EALLOW;
	//!Clear the SPI interrupt to Sitara to low
	GpioDataRegs.GPFCLEAR.bit.GPIO165 = 1;
	EDIS;
}
/******************************************************************************/
/*!
 * \fn void SPISetCommCtrlBusyInt()
 * \brief Description:
 *     Set the SPI Communication BUSY Interrupt GPIO to HIGH
 * \return void
 */
/******************************************************************************/
void SPISetCommCtrlBusyInt()
{
	EALLOW;
	//!Set the busy control to Sitara to high
	GpioDataRegs.GPFDAT.bit.GPIO166 = 1;
	EDIS;
	g_bBusyBitStatus = TRUE;
}
/******************************************************************************/
/*!
 * \fn void SPIClearCommCtrlBusyInt()
 * \brief Description:
 *     Clear the SPI Communication BUSY Interrupt GPIO to Low
 * \return void
 */
/******************************************************************************/
void SPIClearCommCtrlBusyInt()
{
	EALLOW;
	//!Clear the busy control to Sitara to low
	GpioDataRegs.GPFCLEAR.bit.GPIO166 = 1;
	EDIS;
	g_bBusyBitStatus = FALSE;
}
/******************************************************************************/
/*!
 * \fn void SPIConfigCommFifoReg()
 * \brief Description:
 *     Configuration of SPI A FIFO registers
 * \return void
 */
/******************************************************************************/
void SPIConfigCommFifoReg()
{
	uint16_t m;

	//FIFO configuration
	//!FIFO Transmit Delay is made 0
	SpiaRegs.SPIFFCT.all=0x0;
	for(m=0;m<3;m++);

	//!Set SPI in reset
	SpiaRegs.SPIFFTX.bit.SPIRST = 0;
	SpiaRegs.SPIFFRX.all = 0x0;
	//!RX FIFO enabled, clear FIFO int
	SpiaRegs.SPIFFRX.all=0x2040;
	//!Set RX FIFO level
	SpiaRegs.SPIFFRX.bit.RXFFIL = RX_FIFO_LVL;

	SpiaRegs.SPIFFTX.all = 0x0;
	//!FIFOs enabled, TX FIFO released,
	SpiaRegs.SPIFFTX.all=0xE040;
	//!Set TX FIFO level
	SpiaRegs.SPIFFTX.bit.TXFFIL = TX_FIFO_LVL;
	SpiaRegs.SPIFFTX.bit.SPIRST = 1;
}
/******************************************************************************/
/*!
 * \fn void SPIConfigCommReg()
 * \brief Description:
 *     Configuration of SPI A Communication egisters
 * \return void
 */
/******************************************************************************/
void SPIConfigCommReg()
{
    //!Reset on, rising edge, 16-bit char bits
	SpiaRegs.SPICCR.all =0x000F;
	//!Delayed phase = 1
	SpiaRegs.SPICTL.all =0x000E;
	//!Slave mode
	SpiaRegs.SPICTL.bit.MASTER_SLAVE = 0;
	//!Baud Rate
	SpiaRegs.SPIBRR.all = 0x0002;
	//!Relinquish SPI from Reset
	SpiaRegs.SPICCR.all = 0x008F;
	//!High Speed Mode
	SpiaRegs.SPICCR.bit.HS_MODE = 0;
	//!Set so breakpoints don't disturb xmissin
	SpiaRegs.SPIPRI.bit.FREE = 1;
}
/******************************************************************************/
/*!
 * \fn void SPIConfigCommDmaRx()
 * \brief Description:
 *     Configuration of SPI DMA Receive registers
 * \return void
 */
/******************************************************************************/
void SPIConfigCommDmaRx(void)
{
	DMADest = (volatile Uint16 *)SpiCommBuf.SpiDmaRxData;
	/*!Configure the DMA Channel 6 Address*/
	DMACH6AddrConfig(DMADest,&SpiaRegs.SPIRXBUF);
	/*!Configure the DMA Channel 6 Burst registers*/
	DMACH6BurstConfig(RX_BURST,0,1);
	/*!Configure the DMA Channel 6 Transfer registers*/
	DMACH6TransferConfig(RX_TRANSFER,0,1);
	/*!Configure the DMA Channel 6 Mode registers*/
	DMACH6ModeConfig(DMA_SPIARX,PERINT_ENABLE,ONESHOT_DISABLE,CONT_DISABLE,\
				SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,SIXTEEN_BIT,CHINT_END,\
				CHINT_ENABLE);
}
/******************************************************************************/
/*!
 * \fn void SPIConfigCommDmaTx()
 * \brief Description:
 *     Configuration of SPI DMA Transmission registers
 * \return void
 */
/******************************************************************************/
void SPIConfigCommDmaTx(void)
{
	DMASource = (volatile Uint16 *)SpiCommBuf.SpiDmaTxData;
	/*!Configure the DMA Channel 5 Address*/
	DMACH5AddrConfig(&SpiaRegs.SPITXBUF,DMASource);
	/*!Configure the DMA Channel 5 Burst registers*/
	// Burst size, src step, dest step
	DMACH5BurstConfig(BURST,1,0);
	/*!Configure the DMA Channel 5 Transfer registers*/
	// transfer size, src step, dest step
	DMACH5TransferConfig(TX_TRANSFER,1,0);
	/*!Configure the DMA Channel 5 Mode registers*/
	DMACH5ModeConfig(DMA_SPIATX,PERINT_ENABLE,ONESHOT_DISABLE,CONT_DISABLE,\
				SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,SIXTEEN_BIT,CHINT_END,\
				CHINT_ENABLE);
}
/******************************************************************************/
/*!
 * \fn void SPIStopDMACH5()
 * \brief Description:
 *    Stop the running of Channel 5 DMA
 * \return void
 */
/******************************************************************************/
void SPIStopDMACH5()
{
	 EALLOW;
	 //!Stop the DMA channel 5
	 DmaRegs.CH5.CONTROL.bit.RUN = 0;
	 EDIS;
}
/******************************************************************************/
/*!
 * \fn void SPIStopDMACH6()
 * \brief Description:
 *    Stop the running of Channel 6 DMA
 * \return void
 */
/******************************************************************************/
void SPIStopDMACH6()
{
	 EALLOW;
	 //!Stop the DMA channel 6
	 DmaRegs.CH6.CONTROL.bit.RUN = 0;
	 EDIS;
}
/******************************************************************************/
/*!
 * \fn void SPISetTxTestDataPacket()
 * \param1 Uint16* DataBuf
 * \param2 Uint16 unCmd
 * \param3 Uint16 unLength
 * \brief Description:
 *   Set teh Test Data Packet
 * \return void
 */
/******************************************************************************/
void SPISetTxTestDataPacket(Uint16* DataBuf, Uint16 unCmd, Uint16 unLength)
{
	Uint16 i=0;
	*(DataBuf+0) = 0xAAAA;
	*(DataBuf+1) = 0x5555;
	*(DataBuf+2) = 0x1b;
	*(DataBuf+3) = 0x0003;
	*(DataBuf+4) = 0x2001;
	*(DataBuf+5) = 0x2004;

	for(i=6;i<unLength;i++)
	{
		*(DataBuf+i) = unCmd;
	}
	for(i=9;i<508; i++)
	{
		*(DataBuf+i) = 0;
	}
	*(DataBuf+unLength-3) = 0xFC2A;
	*(DataBuf+unLength-2) = 0x5555;
	*(DataBuf+unLength-1) = 0xAAAA;
}
/******************************************************************************/
/*!
 * \fn bool_t SPICommRead(void)
 * \brief Description:
 *      This function returns the state of Reception
 *
 * \return 0 if  Rxn / Txn successfull
 */
/******************************************************************************/
bool_t SPICommRead(void)
{
	int i =0;
	/*! Enter the if loop only when Channel 6 Interrupt is received*/
	if(SET == SPIGetCommRxStatus())
	{
	    //!If the starting word is not dummy packet and it is start of packet,
	    //!Store the data received to the recieve buffer
		if((0xffff != SpiCommBuf.SpiDmaRxData[0]) && \
		        (0x5555 == SpiCommBuf.SpiDmaRxData[0]))
		{
			/*!Store only when Valid data  is received*/
			for(i =0;i<512;i++)
			{
				m_usnSPIRxData[i] = SpiCommBuf.SpiDmaRxData[i];
			}
			/*! Set the m_bDataReceived flag to true*/
			m_bDataReceived = TRUE;
		}
		//!If the starting word is dummy word, then ignore the packet
		else if((0xffff == SpiCommBuf.SpiDmaRxData[0]))
		{
			/*!Do Nothing when Dummy Packet is received */
			m_bDataReceived = FALSE;
		}
		else
		{
			//!When Data received is shifted,
			//!SPI DMA Registers have to be reconfigured
			m_bDataReceived = FALSE;
			SPIReConfigCommDmaReg();
			CIFormACKPacket(SET);
		}
		/*!Clear the SPI Rx Status*/
		SPIClearCommRxStatus();
		/*!return TRUE*/
		return TRUE;
	}
	/*! return FALSE*/
	return 0;
}
/******************************************************************************/
/*!
 * \fn int16 SPICommTest(void)
 * \brief Description:
 *      This function returns the state of Reception
 *
 * \return 0 if  Rxn / Txn successfull
 */
/******************************************************************************/
int16 SPICommTest(void)
{
	static Uint16 Cmd = 0xFFFF, OneTimeFlag=0; //once =0;
	Uint32 i=0;

	/*sprintf(statDebugPrintBuf, " \n TEST 1 \n ");
	UartDebugPrint((int *)statDebugPrintBuf, 50);*/

	//To Test Writing only Once and ReadingContinuously
	if((RESET == SPIGetCommTxStatus()) && (OneTimeFlag == RESET) && (Cmd > RESET) )
	{
		//SPISetCommCtrlBusyInt();

		//once  = 1;
		sprintf(statDebugPrintBuf, "\r\n DSP SPI command 0x%04x ",Cmd);
		UartDebugPrint((int *)statDebugPrintBuf, 50);
		sprintf(statDebugPrintBuf, " ready for txn.. Waiting for Sitara Txn ");
		UartDebugPrint((int *)statDebugPrintBuf, 50);

		OneTimeFlag = 1;
		SPISetTxTestDataPacket(SpiCommBuf.SpiDmaTxData, Cmd--, 512);
		SPIActivateCommPort();
		//SPISetCommCtrlBusyInt();
					sprintf(statDebugPrintBuf, " \n busy interrupt high \n ");
					UartDebugPrint((int *)statDebugPrintBuf, 50);

					SPISetCommCtrlTxrIntReq();

		DELAY_US(2000);	//2ms

		//Pull the busy interrupt low
		SPIClearCommCtrlTxrIntReq();


		SpiComm.TxStatus = 1;
	}
	else if(SET == SPIGetCommRxStatus())
	{
		sprintf(statDebugPrintBuf, " ready for RX.. Waiting for Sitara Txn ");
		UartDebugPrint((int *)statDebugPrintBuf, 50);

		//SPISetCommCtrlBusyInt();
		if(0xffff != SpiCommBuf.SpiDmaRxData[0])
		{
			for(i = 0; i < 512; i++)
			{
				/*if(!(i%6))
				{
					sprintf(statDebugPrintBuf, " \n");
					UartDebugPrint((int *)statDebugPrintBuf, 50);
				}*/
			}
		}
		SPIResetCommDmaAddr();
		SPIClearCommTxStatus();
		SPIClearCommRxStatus();
		SPIActivateCommPort();//Added to read continuously - to START the DMA

	}
	return 0;
}
/******************************************************************************/
/*!
 * \fn __interrupt void local_DMACH5_ISR(void)
 * \brief Description:
 *      Interrupt routine function for DMA Channel5
 * \return from the interrupt
 */
/******************************************************************************/
__interrupt void local_DMACH5_ISR(void)
{

    // NEED TO EXECUTE EALLOW INSIDE ISR !!!
	EALLOW;
	//!HALT the DMA channel 5
	DmaRegs.CH5.CONTROL.bit.HALT = 1;
	//!ACK to receive more interrupts from this PIE group
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
	EDIS;

	/*! Set the SPI Tx Status Flag to 1*/
	SPISetCommTxStatus();

	/*! Set the m_bTxPendingFlag to False*/
	m_bTxPendingFlag = FALSE;

	return;
}
/******************************************************************************/
/*!
 * \fn __interrupt void local_DMACH6_ISR(void)
 * \brief Description:
 *      Interrupt routine function for DMA Channel6
 * \return from the interrupt
 */
/******************************************************************************/
__interrupt void local_DMACH6_ISR(void)
{
    // NEED TO EXECUTE EALLOW INSIDE ISR !!!
	EALLOW;
	//!HALT the DMA channel 6
	DmaRegs.CH6.CONTROL.bit.HALT = 1;
	//!ACK to receive more interrupts from this PIE group
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
	EDIS;

	/*! Set the SPI Rx Status Flag to 1*/
	SPISetCommRxStatus();
	/*!Start the Channel 6 DMA*/
	StartDMACH6();

	return;
}
/******************************************************************************/
/*!
 * \fn bool_t SPICommWrite(void)
 * \brief Writing to SPI
 * \brief Description:
 *      This function is used to send data to Sitara through SPI
 * \return true if Write is successful
 */
/******************************************************************************/
bool_t SPICommWrite()
{
	/*! Enter the if loop only when Channel 5 interrupt is obtained*/
	if(SET == SPIGetCommTxStatus())
	{
		//!If Multiple packets are present Form teh Packet,
		//!write the DMA and START the DMA
		if(m_bMutiplePackets == TRUE || m_bLastMultiplePacket == TRUE)
		{
		    //!Frame the data packet to be transmitted to Sitara
			CPFrameDataPacket(m_stPrevTxPacket.usnMajorCommand, \
			        m_stPrevTxPacket.usnMinorCommand, TRUE);
			//!When multiple packets are transmitted,
			//!if it is last multiple packet, then clear the multiple packet and
			//!last multiple packet flag
			if(m_bLastMultiplePacket == TRUE)
			{
				m_bLastMultiplePacket = FALSE;
				m_bMutiplePackets = FALSE;
			}
		}
		else
		{
			/*! DMA channel 5 is not started until Next packet is formed
			 *  and written to the DMA Buffer
			 */
		}
		/*!Clear the SPI Rx and Tx status*/
		SPIClearCommRxStatus();
		SPIClearCommTxStatus();
		/*!return TRUE*/
		return TRUE;
	}
	else
		/*!return FALSE*/
		return FALSE;
}
/******************************************************************************/
/*!
 * \fn bool_t SPIDmaTxRx()
 * \brief Send Command to Sitara and Check if Data is received and
 *          Upsarse the packet.
 * \brief Description:
 *      This function to decode the received data and to start the recieve DMA
 * \return uint16_t
 */
/******************************************************************************/
bool_t SPIDmaTxRx(void)
{

	/*!Check if Valid data is received,only then decode the data*/
	if(m_bDataReceived == TRUE)
	{

		CPDecodeReceivedDataPacket(m_usnSPIRxData);
		//!Data received flag is cleared, so that busy pin can be pulled low to
		//!indicate Sitara can transmit next packet
		m_bDataReceived =FALSE;

	}

	//!Start DAM channel 6 to recieve the next packet from Sitara
	StartDMACH6();
	/*! return TRUE always*/
	return TRUE;
}
/******************************************************************************/
/*!
 * \fn bool_t SPIDmaWrite()
 * \param uint16_t *data to be sent to Sitara
 * \brief Description:
 *      This function is called to write the data to DMA
 * \return bool_t
 */
/******************************************************************************/
bool_t SPIDmaWrite(uint16_t *data)
{
	uint16_t uncount =0;
	/*!Enter the if loop only if Data is ready to be written to DMA buffer
	 * and there is no Pending DMA transfer*/
	if(m_bTxToDMAFlag == TRUE && m_bTxPendingFlag == FALSE)
	 {
		/*sprintf(statDebugPrintBuf, "TX INT \n");
		UartDebugPrint((int *)statDebugPrintBuf, 50);*/

		/*!DO a Channel 5 Soft reset to complete the pending write operation*/
		EALLOW;
		DmaRegs.CH5.CONTROL.bit.SOFTRESET = 1;
		EDIS;
		/*! load the data to the DMA Tx Buffer*/
		for(uncount = 0;uncount<512;uncount++)
		{
			SpiCommBuf.SpiDmaTxData[uncount] = *(data+ uncount);
		}
		/*!Reset teh Channel5 DMA Address*/
		SPIResetCh5DmaAdd();
		/*!Start the Channel 5 DMA*/
		StartDMACH5();

		/*!Set the m_bTxPendingFlag to TRUE*/
		m_bTxPendingFlag = TRUE;

		/*!Set the m_bTxToDMAFlag to FALSE*/
		m_bTxToDMAFlag = FALSE;

		/*!Halt the DMA Channel 6 so that CH6 Interrupt is not
		 * obtained during the Transfer*/
		// NEED TO EXECUTE EALLOW INSIDE ISR !!!
		EALLOW;
		DmaRegs.CH6.CONTROL.bit.HALT = 1;
		EDIS;

		/*!Set the SPI Tx INT flag to True only once in the Starting during
		 * Multiple packet transmission*/
		if(m_bTxInterruptStatus == TRUE)
		{
		    //!Set the interrupt pin to high
			SPISetCommCtrlTxrIntReq();

			DELAY_US(100);

			//!Clear the interrupt pin to low
			SPIClearCommCtrlTxrIntReq();

			m_bTxInterruptStatus = FALSE;
		}
		/*!Clear the m_bSPIWriteControlStatus falg to FALSE*/
		//m_bSPIWriteControlStatus  = FALSE;

		/*!return TRUE as data is written */
		return TRUE;
	}
	else
	{
        sprintf(statDebugPrintBuf, "\r\n Tx=%d, Pen = %d\n", m_bTxToDMAFlag, m_bTxPendingFlag);
        UartDebugPrint((int *)statDebugPrintBuf, 50);
		/*!return FALSE*/
		return FALSE;
	}
}
/******************************************************************************/
/*!
 * \fn 		void SPIReConfigCommDmaReg()
 * \param 	void
 * \brief   Description: This function is called to reinitialise DMA
 * \return void
 */
/******************************************************************************/
void SPIReConfigCommDmaReg(void)
{
    //!Configure the DMA register
	SPIConfigCommDmaReg();
}

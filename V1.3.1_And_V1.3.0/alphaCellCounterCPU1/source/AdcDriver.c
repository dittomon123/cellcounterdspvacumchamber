/******************************************************************************/
/*! \file AdcDriver.c
 *
 *  \brief Analog input channel configuration for sensor data Aquisition.
 *	\brief
 *  \b Description:
 *      In this file the comple Analog input channel Drivers configuration
 *      and it's sub-sequent
 *      functions definition has been implemented.
 *
 *   $Version: $ 0.1
 *   $Date:    $ Jul-21-2015
 *   $Author:  $ ADITYA B G, GURUDUTT R
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_data_acquisition_module
//! @{
//*****************************************************************************
#include <AdcDriver.h>
#include "PressureSense.h"
#include "SystemInitialize.h"



#pragma DATA_SECTION(unAdcBuf,".RAMGS7_unAdcBuf");

/******************************************************************************/
/*!
 *
 * \var g_bHealthStatusCheck
 * \brief  to enable and disable the monitoring of
 * controller health status (i.e., monitoring the 24V, 5V, tempressure, pressure
 * and blank HgB data
 */
/******************************************************************************/
volatile bool_t g_bHealthStatusCheck = FALSE;

/******************************************************************************/
/*!
 *
 * \var g_usnHealthindexCounter
 * \brief  Health monitoring is done through ADC. Single ADC value cannot be
 * used as the final health value. Atleast 32 averaged samples to be taken for
 * for ariving at final health values. This variable is the counter which traverse
 * through the ADC sampled data array.
 */
/******************************************************************************/
uint16_t g_usnHealthindexCounter = 0;
/******************************************************************************/
/*!
 *
 * \var unAdcBuf[4000]	specifies the buffer for Pressure, Voltage &
 *	heamoglobin ADC data samples
 * \brief  unAdcBuf variable is used to store Pressure, Heamoglobin
 * 	and Voltage sensors data samples
 */
/******************************************************************************/
unsigned int unAdcBuf[4000];

/******************************************************************************/
/*!
 * \typedef ST_DATA_AQUISITION_PARAMETER
 * \brief ST_DATA_AQUISITION_PARAMETER structure that defines the ADC data
 * aquisition instance
 *  these fields are used by ADC wrapper functions
 * \struct StructST_DATA_AQUISITION_PARAMETER
 * \brief StructST_DATA_AQUISITION_PARAMETER instance creation to acess
 * ADCSampleCounter & Aquisition status
 */
/******************************************************************************/
ST_DATA_AQUISITION_PARAMETER StructDataAqParam;

/******************************************************************************/
/*!
 *
 * \typedef PressureParameter
 * \struct PressureSense
 * \brief PressureSensestructure that defines the Pressure parameters.
 *         These fields are used by Pressure module
 */
/******************************************************************************/
extern PressureParameter PressureSense;

/******************************************************************************/
/*!
 * \struct ParameterStatus:
 * \brief ParameterStatusSense instance creation
 *        Object: ParameterStatusSense
 *
 */
/******************************************************************************/
ST_HEALTH_STATUS m_stHealthStatusSense;

/******************************************************************************/
/*!
 * \fn ST_HEALTH_STATUS* ADGetHealthStatus(void)
 * \brief To get Structure address of ST_HEALTH_STATUS
 * \brief  Description:
 *      This function returns the address of ST_HEALTH_STATUS
 *
 * \return &m_stHealthStatusSense
 */
/******************************************************************************/
ST_HEALTH_STATUS* ADGetHealthStatus(void)
{
    return &m_stHealthStatusSense;
}

/******************************************************************************/
/*!
 *
 * \fn void ADPeakDetectionAlgorithm(unsigned int fVoltage)
 * \brief External function definition
 */
/******************************************************************************/
extern void ADPeakDetectionAlgorithm(unsigned int fVoltage);

/******************************************************************************/
/*!
 * \fn unsigned int* ADGetAdcBufAddr(void)
 * \brief Get ADC Buffer Address for ADC api
 *  Description:
 *  This function returns address of unAdcBuf variable
 * \return unAdcBuf (Address of unAdcBuf variable)
 */
/******************************************************************************/
unsigned int* ADGetAdcBufAddr(void)
{
	return (unAdcBuf);
}
/******************************************************************************/
/*!
 * \fn void ADCpu2Ownership(void)
 * \brief CPU2 ownership of ADC modules
 * 		Description:
 *      This function provides ownership of ADCA, ADCB, ADCC modules to CPU2
 * \return void
 */
/******************************************************************************/
void ADCpu2Ownership(void)
{

	EALLOW;
	DevCfgRegs.CPUSEL0.bit.EPWM3 = 1;
	DevCfgRegs.CPUSEL0.bit.EPWM9 = 1;
	DevCfgRegs.CPUSEL11.bit.ADC_A = 1;
	DevCfgRegs.CPUSEL11.bit.ADC_B = 1;
	DevCfgRegs.CPUSEL11.bit.ADC_C = 1;
	EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADAdcd_Init(void)
 * \brief ADCD block initialization
 * 		Description:
 *      This function initialize ADCD registers and associated SOC registers
 *		Calls ADConfigureAdcd(), ADConfigureAdcdEpwm(), ADSetupAdcdEpwm()
 * \return void
 */
/******************************************************************************/
void ADAdcd_Init(void)
{
    ADConfigureAdcd();
    ADConfigureAdcdEpwm();
    ADSetupAdcdEpwm();
//!  Map ISR functions
	EALLOW;
	PieVectTable.ADCD1_INT = &adcd1_isr;
	IER |= M_INT1;                      //! Enable CPU Interrupt 1

	EINT;                               //! Enable Global interrupt INTM
	ERTM;
	EDIS;

	EALLOW;
	  //! Sync ePWM
	CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;
	EDIS;

	PieCtrlRegs.PIEIER1.bit.INTx6 = 1;	//! Enable INT 6 in the PIE

	PressureSense.AdcCounter = 0;
	PressureSense.AcquireStatus = ACQUIRE_COMPLETE;
	PressureSense.DataSize = 20;
}
/******************************************************************************/
/*!
 * \fn void ADConfigureAdcd(void)
 * \brief To configure ADCD block registers
 * 		Description:
 *      This function configures adc resolution, signal mode, clock
 * \return void
 */
/******************************************************************************/
void ADConfigureAdcd(void)
{
    //Device_cal();
    EALLOW;
    //! set ADCCLK divider to /4
    AdcdRegs.ADCCTL2.bit.PRESCALE = 6;
    //! 12-bit resolution
    AdcdRegs.ADCCTL2.bit.RESOLUTION = 0;
    //! single-ended channel conversions (12-bit mode only)
    AdcdRegs.ADCCTL2.bit.SIGNALMODE = 0;
    //! Set pulse positions to late
    AdcdRegs.ADCCTL1.bit.INTPULSEPOS = 1;
    //! power up the ADC
    AdcdRegs.ADCCTL1.bit.ADCPWDNZ = 1;
    //! delay for 1ms to allow ADC time to power up
    DELAY_US(1000);
    EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADConfigureAdcdEpwm(void)
 * \brief To configure ADCD block for pwm trigger
 * 		Description:
 *      This function configures pwm soc to trigger SOC
 *
 */
/******************************************************************************/
void ADConfigureAdcdEpwm(void)
{
    EALLOW;
    //! Assumes ePWM clock is already enabled
    //! freeze counter
    EPwm4Regs.TBCTL.bit.CTRMODE = 3;
    //! TBCLK p`    -0987654re-scaler = /1
    EPwm4Regs.TBCTL.bit.HSPCLKDIV = 0;
#if 0 //abg
    //! Set period to 4000 counts (50kHz)
    EPwm4Regs.TBPRD = 0x66;
    EPwm4Regs.CMPA.bit.CMPA = 0x33;
#else
    //! Set compare A value to 2048 counts
	EPwm1Regs.CMPA.bit.CMPA = 0x0800;
	//! Set period to 4096 counts
	EPwm1Regs.TBPRD = 0x1000;
#endif
	//! Disable SOC on A group
    EPwm4Regs.ETSEL.bit.SOCAEN  = 0;
    //! Select SOC on up-count
    EPwm4Regs.ETSEL.bit.SOCASEL = 4;
    //! enable SOCA
    EPwm4Regs.ETSEL.bit.SOCAEN = 1;
    //! Generate pulse on 1st event
    EPwm4Regs.ETPS.bit.SOCAPRD = 1;
    EPwm4Regs.TBCTL.bit.CTRMODE = 3;
    EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADSetupAdcdEpwm(void)
 * \brief To configure PWM trigger selection
 * 		Description:
 *      This function configures pwm trigger selection , aquisition window
 * \return void
 */
/******************************************************************************/
void ADSetupAdcdEpwm(void)
{
    EALLOW;
#if 1
    //! SOC0 will convert pin D0
    AdcdRegs.ADCSOC0CTL.bit.CHSEL = 0x00;
    //! sample window is 15 SYSCLK cycles
    AdcdRegs.ADCSOC0CTL.bit.ACQPS = 19;
    //! trigger on ePWM4 SOCA
    AdcdRegs.ADCSOC0CTL.bit.TRIGSEL = 0x0B;

    //! SOC1 will convert pin D1
    AdcdRegs.ADCSOC1CTL.bit.CHSEL = 0x01;
    //! sample window is 15 SYSCLK cycles
    AdcdRegs.ADCSOC1CTL.bit.ACQPS = 19;
    //! trigger on ePWM4 SOCA
    AdcdRegs.ADCSOC1CTL.bit.TRIGSEL = 0x0B;

    //! SOC2 will convert pin D2
    AdcdRegs.ADCSOC2CTL.bit.CHSEL = 0x02;
    //! sample window is 15 SYSCLK cycles
    AdcdRegs.ADCSOC2CTL.bit.ACQPS = 19;
    //! trigger on ePWM4 SOCA
    AdcdRegs.ADCSOC2CTL.bit.TRIGSEL = 0x0B;

    //! SOC3 will convert pin D3
    AdcdRegs.ADCSOC3CTL.bit.CHSEL = 0x03;
    //! sample window is 15 SYSCLK cycles
    AdcdRegs.ADCSOC3CTL.bit.ACQPS = 19;
    //! trigger on ePWM4 SOCA
    AdcdRegs.ADCSOC3CTL.bit.TRIGSEL = 0x0B;

    //! SOC4 will convert pin D4
    AdcdRegs.ADCSOC4CTL.bit.CHSEL = 0x04;
    //! sample window is 15 SYSCLK cycles
    AdcdRegs.ADCSOC4CTL.bit.ACQPS = 19;
    //! trigger on ePWM4 SOCA
    AdcdRegs.ADCSOC4CTL.bit.TRIGSEL = 0x0B;

#endif

    //! end of SOC4 will set INT1 flag //SKM_CHANGE
    AdcdRegs.ADCINTSEL1N2.bit.INT1SEL = 4;  //5
    //! enable INT1 flag
    AdcdRegs.ADCINTSEL1N2.bit.INT1E = 1;
    //! make sure INT1 flag is cleared
    AdcdRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
    EDIS;
}
/******************************************************************************/
/*!
 * \fn void ADAcquirePressureData(Uint16 DataSize)
 * \param DataSize specifies data length of the Sample buffer
 * \brief To start Pressure signal- ADC data Aquisition
 * 		Description:
 *      This function configures and initiates the PWM trigger to start
 *      Aquisition
 * \return void
 */
/******************************************************************************/
void ADAcquirePressureData(Uint16 DataSize)
{
    //!Set the AdcCounter to zero before starting the acquisition
	PressureSense.AdcCounter = 0;
	//!Start ADC acquisition
	PressureSense.AcquireStatus = ACQUIRE_START;
	//!Set the data size to 20
	PressureSense.DataSize = DataSize;
    EALLOW;
    //!Set the control mode of the PWM to zero to start acquisiotn
	EPwm4Regs.TBCTL.bit.CTRMODE = 0;
	EDIS;

}
/******************************************************************************/
/*!
 * \fn adcd1_isr()
 *
 * \brief ADCd channel 1 ISR handler
 * 		Description:
 *      This ISR handler calls on every sample conversion
 *      Clear INT1 flag
 *      acknowledge PIE group 1 to enable further interrupts
 * \return void
 */
/******************************************************************************/
interrupt void adcd1_isr(void)
{
#if 1

    //SKM_CHANGE - START

    //!update the samples temperaturedata sampled from ADC to circular array buffer
    m_stHealthStatusSense.m_arrusnTemperatureAdc[g_usnHealthindexCounter] =
            (AdcdResultRegs.ADCRESULT0 & MASKING_12_BIT);
    //!update the samples pressure data sampled from ADC to circular array buffer
    m_stHealthStatusSense.m_arrusnPressureAdc[g_usnHealthindexCounter] =
            (AdcdResultRegs.ADCRESULT1 & MASKING_12_BIT);
    //!update the samples HgB data sampled from ADC to circular array buffer
    m_stHealthStatusSense.m_arrusnHgbAdc[g_usnHealthindexCounter] =
            (AdcdResultRegs.ADCRESULT2 & MASKING_12_BIT);
    //!update the samples 24V data sampled from ADC to circular array buffer
    m_stHealthStatusSense.m_arrusnSense24Voltage[g_usnHealthindexCounter] =
            (AdcdResultRegs.ADCRESULT3 & MASKING_12_BIT);
    //!update the samples 5V data sampled from ADC to circular array buffer
    m_stHealthStatusSense.m_arrusnSense5Voltage[g_usnHealthindexCounter++] =
            (AdcdResultRegs.ADCRESULT4 & MASKING_12_BIT);

    //!Check whether the array counter has reached to maximum array size.
    //!The array is circulat buffer, if the counter reaches to maximum array
    //!size, set it to Zero
    if(g_usnHealthindexCounter >= HEALTH_STATUS_ARRAY_SIZE)
    {
        g_usnHealthindexCounter = 0;
        //!At boot up, the array is filled with unknown values. So don't
        //!calculate the health status till the array is filled.
        //!Enable the g_bHealthStatusCheck once the array is filled with the
        //!sample ADC data
        g_bHealthStatusCheck = TRUE;
    }
#endif

    //! Return from interrupt
    EALLOW;
    //! stopping the ePWM4
    EPwm4Regs.TBCTL.bit.CTRMODE = 3;
    //! Giving Complete status
    //PressureSense.AcquireStatus = ACQUIRE_COMPLETE;
    //SKM_CHANGE - END
    //! Clear INT1 flag
    AdcdRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
    //! acknowledge PIE group 1 to enable further interrupts
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
    EDIS;
}
/******************************************************************************/
/*!
 * \fn ADAdcAcquireHealthStatus()
 * \brief Start ADC for health status
 *      Description:
 *      This function triggers the pwm for ADC data acquisition to Acquire -
 *      Temperature, Pressure, HgB, 24V and 5V
 * \return void
 */
/******************************************************************************/
void ADAdcAcquireHealthStatus(void)
{
    EALLOW;
    //!Set the control mode of the PWM to zero to aquire the health data from ADC
        EPwm4Regs.TBCTL.bit.CTRMODE = 0;
    EDIS;

}
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

/******************************************************************************/
/*! \file InitSDRAM.c
 *
 *  \brief Initialization of SDRAM
 *
 *  \b  Description:
 *      In this file emif1 & SDRAM initialization.
 *      After initialization, SDRAM memory shall be erased
 *      In this file, SDRAM configuration for CPU1 & CPU2 ownership
 *      Also reads the status of which CPU holding ownership of SDRAM
 *
 *   $Version: $ 0.1
 *   $Date:    $ Aug-18-2015
 *   $Author:  $ GURUDUTT R, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_sdram_initialization_module
//! @{
//*****************************************************************************

#include "InitSDRAM.h"
#include <UartDebug.h>
#include "AdcDriver.h"

/******************************************************************************/
/*!
 * \fn void ISInitializeSDRAM(void)
 * \brief Configuration function call for EMIF, DMA & SDRAM
 * \brief Description:
 *      This function configures and initiates EMIF , DMA & SDRAM registers
 *      	ISConfigureEMIF();
 *			ISConfigureSDRAMRegisters();
 *			ISEraseSdramMemoryDWORD(SDRAM_CS0_MEM_START_ADDR,
 *			SDRAM_CS0_MEM_SIZE_64MB);
 * \return void
 */
/******************************************************************************/
void ISInitializeSDRAM(void)
{

    //!To configure the External memory interface
	ISConfigureEMIF();

	//!To configure SDRAM register
	ISConfigureSDRAMRegisters();

	//!To write data to SDRAM in the given location
	ISSdramWriteWORD(0x81FF0000, 0xAAAA);
	ISSdramWriteWORD(0x81FF0001, 0x5555);
	ISSdramWriteWORD(0x81FF0002, 0x1234);
	ISSdramWriteWORD(0x81FF0003, 0x4321);

	//!To erase the SDRAM from the specific location
	ISEraseSdramMemoryWORD(SDRAM_CS0_MEM_START_ADDR, SDRAM_CS0_MEM_SIZE_64MB);
}
/******************************************************************************/
/*!
 * \fn int16 ISConfigEmif1CPU(Uint16 CpuConfig)
 * \param CpuConfig CPU1 cofigures for emif1 R/W control
 * \brief EMIF1 CPU configuration
 * \brief  Description:
 *      This function provided R/W access to the CPU1 core
 *
 * \return 0 if Success -1 if Failure
 */
/******************************************************************************/
int16 ISConfigEmif1CPU(Uint16 CpuConfig)
{
    if(CpuConfig == 1)
    {
    	EALLOW;
    	//! Grab EMIF1 For CPU1
    	Emif1ConfigRegs.EMIF1MSEL.all = 0x93A5CE71;
    	if (Emif1ConfigRegs.EMIF1MSEL.all != 0x1)
    	{
    		return (-1);
    	}
    	EDIS;
    	return (0);

    }
    else
    {
    	EALLOW;
    	//! CPU1 is master but not grabbed
    	Emif1ConfigRegs.EMIF1MSEL.all = 0x93A5CE70;
    	if (Emif1ConfigRegs.EMIF1MSEL.all != 0x0)
    	{
    		return (-1);
    	}
    	EDIS;
    	return (0);
    }
}
/******************************************************************************/
/*!
 * \fn Uint16 ISGetEmif1Status(void)
 *
 * \brief EMIF1 status
 * \brief  Description:
 *      This function read EMIF1MSEL bit and returns the value
 *
 * \return 0 CPU1 is master but not grabbed
 * 		   1 CPU1 Master
 * 		   2 CPU2 master
 * 		   3 CPU1 is master but not grabbed
 */
/******************************************************************************/
Uint16 ISGetEmif1Status(void)
{
	Uint16 Emif1Sel = 0xFFFF;
	Emif1Sel = (Uint16)Emif1ConfigRegs.EMIF1MSEL.all;
	return Emif1Sel;

}
/******************************************************************************/
/*!
 * \fn int ISConfigureEMIF(void)
 * \brief Configuration EMIF registers
 * \brief Description:
 *      This function configures EMIF registers
 *
 * \return int 0 if Success else Fail
 */
/******************************************************************************/
int ISConfigureEMIF(void)
{


	//Configure to run EMIF1 on half Rate (EMIF1CLK = CPU1SYSCLK/2)
	EALLOW;
	ClkCfgRegs.PERCLKDIVSEL.bit.EMIF1CLKDIV = 0x1;
	EDIS;

	EALLOW;
	//! Grab EMIF1 For CPU1
	Emif1ConfigRegs.EMIF1MSEL.all = 0x93A5CE71;
	if (Emif1ConfigRegs.EMIF1MSEL.all != 0x1)
	{
		return (-1);
	}

	//!Disable Access Protection (CPU_FETCH/CPU_WR/DMA_WR)
	Emif1ConfigRegs.EMIF1ACCPROT0.all = 0x0;
	if (Emif1ConfigRegs.EMIF1ACCPROT0.all != 0x0)
	{
		return (-1);
	}

	//! Commit the configuration related to protection. Till this bit remains set
	//! content of EMIF1ACCPROT0 register can't be changed.
	Emif1ConfigRegs.EMIF1COMMIT.all = 0x1;

	if(Emif1ConfigRegs.EMIF1COMMIT.all != 0x1)
	{
		return (-1);
	}


	//! Lock the configuration so that EMIF1COMMIT register can't be changed any more.
	Emif1ConfigRegs.EMIF1LOCK.all = 0x1;

	if (Emif1ConfigRegs.EMIF1LOCK.all != 1)
	{
		return (-1);
	}

	EDIS;
	return (0);


}
/******************************************************************************/
/*!
 * \fn void ISConfigureSDRAMRegisters(void)
 * \brief Configure EMIF peripheral registers
 * \brief Description:
 *      This function Configure EMIF peripheral registers
 *
 *
 * \return void
 */
/******************************************************************************/
void ISConfigureSDRAMRegisters(void)
{

	DELAY_US(2);
	//!Configure GPIO pins for EMIF1

	setup_emif1_pinmux_sdram_16bit(0);
	DELAY_US(2);

	/////////////////////////////////////
	//!Configure SDRAM control registers//
	/////////////////////////////////////

	//! Need to be programmed based on SDRAM Data-Sheet.
	//!T_RFC = 60ns = 0x6
	//!T_RP  = 18ns = 0x1
	//!T_RCD = 18ns = 0x1
	//!T_WR  = 1CLK + 6 ns = 0x1
	//!T_RAS = 42ns = 0x4
	//!T_RC  = 60ns = 0x6
	//!T_RRD = 12ns = 0x1
//			Emif1Regs.SDRAM_TR.all = 0x31114610;
	Emif1Regs.SDRAM_TR.all = 0x31114610;

	//!Txsr = 70ns = 0x7
	Emif1Regs.SDR_EXT_TMNG.all = 0x7;

	//!Tref = 64ms for 8192 ROW, RR = 64000*100(Tfrq)/8192 = 781.25 (0x30E)
//			Emif1Regs.SDRAM_RCR.all = 0x30E;
	Emif1Regs.SDRAM_RCR.all = 0x30E;


	//!PAGESIZE=2 (1024 elements per ROW), IBANK = 2 (4 BANK), CL = 3, NM = 1 (16bit)
//			Emif1Regs.SDRAM_CR.all = 0x00015622;

	Emif1Regs.SDRAM_CR.all = 0x00015622;
//			Emif1Regs.SDRAM_CR.bit.CL = 2;

	DELAY_US(2000);

}
/******************************************************************************/
/*!
 * \fn int16 ISEraseSdramMemoryDWORD(Uint32 StartAddr, Uint32 MemSize)
 * \param StartAddr	Destination start address to be passed
 * \param MemSize	Size/lenth of the memory to be erased
 * \brief To erase Sdram memory
 * \brief Description:
 *      This function erases the Sdram memory based on starting address & size
 *		Function call:
 *		__addr32_write_uint32((Uint32)Address, Data);
 * \return 0 Successfull -1 Failure
 */
/******************************************************************************/
int16 ISEraseSdramMemoryDWORD(Uint32 StartAddr, Uint32 MemSize)
{
	Uint32 i=0;
    for(i = 0; i < MemSize; i++)
    {
        //!Write data to SDRAM
    	__addr32_write_uint32((StartAddr + (i*2)), 0x00000000);
    }
    DELAY_US(20);
	return 0;

}
/******************************************************************************/
/*!
 * \fn int16 ISEraseSdramMemoryWORD(Uint32 StartAddr, Uint32 MemSize)
 * \param StartAddr	Destination start address to be passed
 * \param MemSize	Size/lenth of the memory to be erased
 * \brief To erase Sdram memory
 * \brief Description:
 *      This function erases the Sdram memory based on starting address & size
 *		Function call:
 *		__addr32_write_uint16((Uint32)Address, Data);
 * \return 0 Successfull -1 Failure
 */
/******************************************************************************/
int16 ISEraseSdramMemoryWORD(Uint32 StartAddr, Uint32 MemSize)
{
	Uint32 i=0;
    for(i = 0; i < MemSize; i++)
    {
        //!Write data to SDRAM
    	__addr32_write_uint16((StartAddr + i), 0x0000);
    }
    DELAY_US(20);
	return 0;

}
/******************************************************************************/
/*!
 * \fn void ISSdramWriteWORD(Uint32 MemAddr, Uint16 Data)
 * \param Data	Data to be written to the memory
 * \param MemAddr	Location/Address of the memory
 * \brief To write word to the memory address
 * \brief Description:
 *      This function writes data to the specific address passed
 *		Function call:
 *		__addr32_write_uint16(MemAddr, Data);
 * \return void
 */
/******************************************************************************/
void ISSdramWriteWORD(Uint32 MemAddr, Uint16 Data)
{
    //!Write data to SDRAM
	__addr32_write_uint16(MemAddr, Data);
}
/******************************************************************************/
/*!
 * \fn void ISSdramWriteDWORD(Uint32 MemAddr,Uint32 Data)
 * \param Data	Data to be written to the memory
 * \param MemAddr	Location/Address of the memory
 * \brief To write word to the memory address
 * \brief Description:
 *      This function writes data to the specific address passed
 *		Function call:
 *		__addr32_write_uint32(MemAddr, Data);
 * \return void
 */
/******************************************************************************/
void ISSdramWriteDWORD(Uint32 MemAddr,Uint32 Data)
{
    //!Write data to SDRAM
	__addr32_write_uint32(MemAddr, Data);
}
/******************************************************************************/
/*!
 * \fn Uint16 ISSdramReadWORD(Uint32 MemAddr)
 *
 * \param MemAddr	Location/Address of the memory
 * \brief To read word to the memory address
 * \brief Description:
 *      This function read data to the specific address passed
 *		Function call:
 *		__addr32_read_uint16(MemAddr, Data);
 * \return Data read from sdram memory address
 */
/******************************************************************************/
Uint16 ISSdramReadWORD(Uint32 MemAddr)
{
//	Uint16 Data=0x0000;
//	Data = __addr32_read_uint16(MemAddr);
//	return Data;
    //!Read data from SDRAM
	return __addr32_read_uint16(MemAddr);
}
/******************************************************************************/
/*!
 * \fn Uint32 ISSdramReadDWORD(Uint32 MemAddr)
 *
 * \param MemAddr	Location/Address of the memory
 * \brief To read word to the memory address
 * \brief Description:
 *      This function read data to the specific address passed
 *		Function call:
 *		__addr32_read_uint32(MemAddr, Data);
 * \return Data read from sdram memory address
 */
/******************************************************************************/
Uint32 ISSdramReadDWORD(Uint32 MemAddr)
{
//	Uint32 Data=0x0000;
//	Data = __addr32_read_uint32(MemAddr);
//	return Data;
    //!Read data from SDRAM
	return __addr32_read_uint32(MemAddr);
 }
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

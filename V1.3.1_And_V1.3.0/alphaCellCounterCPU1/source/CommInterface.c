/********************************************************************************/
/*! \file CommInterface.c
 *
 *  \brief Processing the data that is received from processor and to frame
 *  the packet to be sent to processor
 *
 *  \b  Description: Processing the data that is received from processor and
 *  to frame the packet to be sent to processor
 *
 *   $Version: $ 0.1
 *   $Date:    $ May-21-2015
 *   $Author:  $ Samyuktha B S, Payam Hemadri
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_Communication_Interface
//! @{
//*****************************************************************************
/******************************************************************************/
#include "BloodCellCounter.h"
#include <stdio.h>
#include <stdlib.h>
#include "CommInterface.h"
#include "SystemInitialize.h"
#include "MotorConfig.h"

/******************************************************************************/
/*!
 * \var m_bNAckReceived
 * \brief  to check whether NAK is received from the processor
 */
/******************************************************************************/
volatile bool_t m_bNAckReceived = FALSE;
extern char DebugPrintBuf[50];
extern bool_t m_bVlaveCheckStatus;
extern bool_t g_bYMotorFaultMonEn;
extern bool_t g_bXMotorFaultMonEn;
extern bool_t g_bDilMotorFaultMonEn;
extern bool_t g_bWasteMotorFaultMonEn;
extern bool_t g_bSampleMotorFaultMonEn;
extern uint16_t g_usnWholeBloodZapTime;
extern uint16_t g_LyseMixTime;
extern uint16_t g_DiluentMixTime;
extern uint16_t g_RBCMixTime;
extern int16_t g_usnWbcCalibratedTime;
extern int16_t g_usnRbcCalibratedTime;
extern uint32_t sncalZeroPSI;
extern uint16_t g_TestDataI0 ;
extern uint16_t g_TestDataI1 ;
extern uint16_t g_TestDataI2 ;
extern uint16_t Counter;
extern bool_t g_bLowVolumeDisp;
/******************************************************************************/
/*!
 *
 * \var g_bStartButtonStatus
 * \brief  Start button is connected to controller. Start button is used for aspiration.
 * Controller polls whether the start button is pressed or not.
 * However polling to be done only during particular sequences and when
 * enabled from processor.
 * g_bStartButtonStatus variable is enabled/disabled by processor indicating
 * when to start/stop polling the start button switch.
 */
/******************************************************************************/
volatile bool_t g_bStartButtonStatus = FALSE;

/******************************************************************************/
/*!
 * \var m_eTypeofCell
 * \brief  Enum to store which type of cell (RBC, WBC, PLT)
 */
/******************************************************************************/
volatile E_CELL_TYPE m_eTypeofCell;

/******************************************************************************/
/*!
 * \var m_ucValveNo
 * \brief  To store which variable to be operated during valve test
 */
/******************************************************************************/
volatile unsigned char m_ucValveNo = 0;

/******************************************************************************/
/*!
 * \var m_usnPacketCount
 * \brief  During pulse height and raw data transmission, multiple packets
 * to be transmitted. m_usnPacketCount indicates how many pending packets to be
 * transmitted to processor.
 */
/******************************************************************************/
volatile uint16_t m_usnPacketCount =0;

/******************************************************************************/
/*!
 * \var g_usnCurrentMode
 * \brief  It indicates what is the current mode the controller is running
 */
/******************************************************************************/
uint16_t g_usnCurrentMode = 0;

/******************************************************************************/
/*!
 * \var g_usnCurrentModeTemp
 * \brief  It indicates the intermediate mode for pre-dilute mode during
 * pre-dilute sequence
 */
/******************************************************************************/
uint16_t g_usnCurrentModeTemp = 0;

/******************************************************************************/
/*!
 * \var g_bNextSecRawDataReq
 * \brief  It indicates whether the next raw data is to be sent to controller or not
 */
/******************************************************************************/
bool_t g_bNextSecRawDataReq = FALSE;

/******************************************************************************/
/*!
 * \var m_bAbnormalStartup
 * \brief  It indicates whether the start up is normal sequence after soft
 *      shutdown or abnormal shutdown. If it was abnormal shutdown, then the
 *      variable is set to true to perform PrimeAll sequence.
 */
/******************************************************************************/
bool_t m_bAbnormalStartup = FALSE;
bool_t g_bPressureCaliStart = FALSE;
bool_t m_bSystemStatusFlag = FALSE;
bool_t m_bZapInitiateFlag = FALSE;

extern ST_SPI_PACKET_FRAME m_stPrevTxPacket;
extern ST_SPI_PACKET_FRAME m_stReceivedPacketFrame;
extern bool_t g_bBusyBitStatus;
uint16_t g_usnCurrentModeMajor = 0;//SKM_CHANGE_MBD
extern bool_t m_bSequenceCompleted;
extern uint16_t g_usnFirstDilutionFreq;
extern uint16_t g_usnLyseMixingFreq;
extern uint16_t g_usnFinalRBCMixingFreq;
extern bool_t g_bMonDiluentPriming;
extern bool_t g_bMonLysePriming;
extern bool_t g_bMonRinsePriming;
extern union UT_ERROR_INFO utErrorInfoCheck;
extern Uint32 g_unSequenceStateTime;
extern Uint32 g_unSequenceStateTime1;
extern E_CYCLE_COMPLETION m_eCycleCompletion;
extern unsigned char m_ucValveControlTime;
extern bool_t m_bReadyforAspCommand;

extern bool_t m_bPressureMonitor;
extern volatile bool_t m_bSPIWriteControlStatus;
extern bool_t m_bTxPendingFlag;
extern bool m_bTxToDMAFlag;
extern uint16_t g_usnDataMode;
extern void SPIReConfigCommDmaReg(void);
extern bool_t g_bWasteDetectCheck;
extern bool_t g_bDataTransmitted;
/******************************************************************************/
/*!
 *  \fn     CIProcessCommandAttribute()
 *  \brief  To Process the Commands received from SPI Master
 *  \param1  uint16_t usnMajorCmd - Major Command
 *  \param2  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessCommandAttribute(uint16_t usnMajorCmd, uint16_t usnMinorCmd)
{
    //!Check for the major command of the command received from the processor
//
//    sprintf(DebugPrintBuf, "\r\n CIProcessCommandAttribute %d %d\n\n",usnMajorCmd, usnMinorCmd);
//    UartDebugPrint((int *)DebugPrintBuf, 50);
	switch (usnMajorCmd)
	{
	    //!If the decoded data is Sync command perform the respective action
		case SITARA_SYNC_MAIN_CMD:
			if( SITARA_SYNC_SUB_CMD == usnMinorCmd)
			{
				//Do Something
			}
		break;

		//!If the decoded data is Patient Handling command perform the action
		//!with respect to patient handling sequences
		case SITARA_PATIENT_HANDLING:
		{
            //!Patient handling function is called by passing the minor command
		    //!Patient handling has different seqence as Whole blood,
		    //!Pre-Dilute, Body fluid, Partial blood count of WBC, RBC & PLT
		    //!Corresponding actions are performed depending upon the minor
		    //!command passed
		    //!Patient handling command is used for sending the results in any
		    //!of the above mentioned sequence
		    g_usnCurrentModeMajor = SITARA_PATIENT_HANDLING;
			CIProcessPatientHandlingCmd(usnMinorCmd);

            //!Since it is only processing command,
            //!and no further sequence is initiated, clear the busy pin
            SPIClearCommCtrlBusyInt();

            //!Enable the flag for ready for aspiration as soon as the trigger
            //! button is pressed
            m_bReadyforAspCommand = TRUE;
		}
		break;

        //!If the decoded data is Service Handling command perform the action
        //!with respect to service handling sequences
		case SITARA_SERVICE_HANDLING:
		{
            //!Service handling function is called by passing the minor command
            //!Service handling has different seqence:  Diluent, Lyse,Rinse
            //!Priming, Prime all, Flush Apperture, E-Z Cleaning, drain bath,
            //!clean bath, ZAP Aperture, Valve test, Drain all,
            //!Startup, System status, waste bin replace, Head rinse,
            //!Probe cleaning, RBC & WBC Count time sequence, Wakeup,
            //!Motor check (X, Y, Sample, Diluent & Waste)
            //!Corresponding actions are performed depending upon the minor command
            //!passed
//            sprintf(DebugPrintBuf, "\r\n SITARA_SERVICE_HANDLING mir: %d \n",usnMinorCmd);
//            UartDebugPrint((int *)DebugPrintBuf, 50);
		    g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;
			CIProcessServiceHandlingCmd(usnMinorCmd);
		}
		break;

        //!If the decoded data is Acknowledgment command from Sitara, process
        //!the acknowledgment
		case SITARA_ACK_MAIN_CMD:
		{
		    //!Process the acknowledgment command that is received
			CIProcessACKCmd(usnMinorCmd);

			//!Busy pin is set if any sequence to be initiated.
			//!Since it is only processing the ACK command, and no further
			//!sequence is initiated, clear the busy pin
			SPIClearCommCtrlBusyInt();
		}
		break;

        //!If the decoded data is NAK command from Sitara, process
        //!the NAK command
		case SITARA_NACK_MAIN_CMD:
		{
		    //!Process the Negative acknowledgement command that is received
			CIProcessNACKCmd(usnMinorCmd);

            //!Busy pin is set if any sequence to be initiated.
            //!Since it is only processing the NACK command, and no further
            //!sequence is initiated, clear the busy pin
			SPIClearCommCtrlBusyInt();
		}
		break;

        //!If the decoded data is the request for version command, send the
        //!CPU1 and CPU2 version command
		case SITARA_FIRMWARE_VERSION_CMD:
		{
		    //!Function is called to packetize the firmware version of the
		    //!Controller
			CIProcessFirmwareVersionReq();

            //!Busy pin is set if any sequence to be initiated.
            //!Since it is only processing and framing the Version command,
            //!and no further sequence is initiated, clear the busy pin
			SPIClearCommCtrlBusyInt();
		}

		break;

        //!If the decoded data is Calibration Handling command perform the action
        //!with resspect to Calibration handling sequences
		case SITARA_CALIBRATION_HANDLING:
		{
            //!Calibration handling function is called by passing the minor command
            //!Calibration handling has different seqence: Auto Calibration,
            //!Commercial Calibration WBC, Commercial Calibration RBC,
            //!Commercial Calibration PLT
            //!Calibration handling command is used for sending the results in
            //!any of the above mentioned sequence
		    g_usnCurrentModeMajor = SITARA_CALIBRATION_HANDLING;
			CIProcessCalibrationHandlingCmd(usnMinorCmd);

            //!Since it is only processing command,
            //!and no further sequence is initiated, clear the busy pin
            SPIClearCommCtrlBusyInt();
            //!Enable the flag for ready for aspiration as soon as the trigger
            //! button is pressed
            m_bReadyforAspCommand = TRUE;
		}
		break;

        //!If the decoded data is Quality Handling command perform the action
        //!with resspect to Quality handling sequences
		case SITARA_QUALITY_HANDLING:
		{
            //!Quality handling function is called by passing the minor command
            //!Quality handling has different seqence: Quality Handling for
            //!Whole blood and body fluid
            //!Quality handling command is used for sending the results in
            //!any of the above mentioned sequence
		    g_usnCurrentModeMajor = SITARA_QUALITY_HANDLING;
			CIProcessQualityHandlingCmd(usnMinorCmd);

            //!Since it is only processing command,
            //!and no further sequence is initiated, clear the busy pin
            SPIClearCommCtrlBusyInt();
		}
		break;

        //!If the decoded data is Shutdown sequence perform the shutdown action
		case SITARA_SHUTDOWN_CMD:
		{
		    //!Shudown function is called to initiate shutdown sequence
		    g_usnCurrentModeMajor = SITARA_SHUTDOWN_CMD;
		    CIProcessShutdownCmd(usnMinorCmd);
		}
		break;

		//!If the decoded data is to abort the the current running process,
		//!Abort the sequence immediately
		case SITARA_ABORT_MAIN_PROCESS_CMD:
		{
            //!Set Major command to MBD
            g_usnCurrentModeMajor = SITARA_ABORT_MAIN_PROCESS_CMD;

		    CIProcessAbortMainProcessCmd(usnMinorCmd);

            //!After aborting any sequence, controller is in idle condition.
            //!Clear the busy pin
            SPIClearCommCtrlBusyInt();
		}
		break;

		//!If the decoded data is System settings command, set the corresponding
        //!settings to the parameters
        case SITARA_SETTINGS_CMD:
        {
            g_usnCurrentModeMajor = SITARA_SETTINGS_CMD;
            CIProcessSystemSettingsCmd(usnMinorCmd);

        }
        break;

        //!If the decoded data is Patient Handling command perform the action
        //!with respect to patient handling sequences
        case SITARA_STARTUP_CMD:
        {
            g_bYMotorFaultMonEn = true;
            //!Start monitoring the waste bin during start up
            g_bWasteDetectCheck = TRUE;

            //!Startup handling function is called by passing the minor command
            g_usnCurrentModeMajor = SITARA_STARTUP_CMD;
            CIProcessStartupHandlingCmd(usnMinorCmd);

            //!Since it is only processing command,
            //!and no further sequence is initiated, clear the busy pin
            SPIClearCommCtrlBusyInt();
        }
        break;

        //!If the decoded data is Patient Handling command perform the action
        //!with respect to patient handling sequences
        case SITARA_ERROR_CMD:
        {
            CIProcessErrorCommand(usnMinorCmd);
            SPIClearCommCtrlBusyInt();
        }
        break;

		default:
		{
            //!If the proper command is received and no major command,
		    //!Do not abort the current running sequence also
            //!TBD to clear the busy pin and do not initiate any sequence

		    //SPIClearCommCtrlBusyInt();
		}
		break;
	}
}
/******************************************************************************/
/*!
 *  \fn     CIProcessPatientHandlingCmd()
 *  \brief  To process the Sitara Patient Handling Commands
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessPatientHandlingCmd(uint16_t usnMinorCmd)
{
    m_bReadyforAspCommand = TRUE;
//    sprintf(DebugPrintBuf, "\r\n CIProcessPatientHandlingCmd %d\n\n", usnMinorCmd);
//    UartDebugPrint((int *)DebugPrintBuf, 50);
    //!Check for the minor command of the command received from the processor
	switch (usnMinorCmd)
	{
	    //!If the command received from processor is Whole blood mode,
	    //!perform the below operations
		case SITARA_WHOLE_BLOOD_WBC_HGB:
		case SITARA_WHOLE_BLOOD_RBC_PLT:
		case SITARA_WHOLE_BLOOD_RBC_WBC_PLT:
		{
		    //!if the previous sequence is pre-dilunet and we changed to whole blood mode
		    //! ReSet the g_bLowVolumeDisp
		    g_bLowVolumeDisp = FALSE;
            //!Re-initilaise the major variables releated to new sequence
			//!starting
			CIReInitialiseOnNewMeasurement();

			//!Set the current mode to Whole Blood mode which will be given
			//!input to MBD to initiate Whole blood
			g_usnCurrentMode = WHOLE_BLOOD_MODE;
            sprintf(DebugPrintBuf, "\r\n WHOLE_BLOOD_MODE\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
		}
		break;

        //!If the command received from processor is Pre-dilute mode,
        //!perform the pre-dilute mode
		case SITARA_PRE_DILUENT_MODE:
		    //!Reset the pop-up cmd
		    g_usnCurrentModeTemp = ZERO;//HN_added 2/12/2020
            //!Re-initilaise the major variables releated to new sequence
            //!starting
		    CIReInitialiseOnNewMeasurement();
		    //!Set the current mode to Pre-dilute mode which will be given
            //!input to MBD to initiate Pre-dilute
			g_usnCurrentMode = PRE_DILUENT_MODE;
            //!Clear the busy pin, after framing and sending the packet
            SPIClearCommCtrlBusyInt();

		break;

        //!If the command received from processor is body fluid mode,
		case SITARA_BODY_FLUID:
		    //!perform the body fluid

		    //!if the previous sequence is pre-dilunet and we changed to whole blood mode
            //! ReSet the g_bLowVolumeDisp
            g_bLowVolumeDisp = FALSE;


            //!Re-initilaise the major variables releated to new sequence
            //!starting
			CIReInitialiseOnNewMeasurement();
            //!Set the current mode to Body Fluid mode which will be given
            //!input to MBD to initiate Pre-dilute
			g_usnCurrentMode = BODY_FLUID_MODE;

		break;

        //!If the command received from processor is to request WBC pulse height,
        //!Frame the packet for WBC pulse height and send it to processor
		case SITARA_WB_WBC_PULSE_HEIGHT_CMD:
		    //!Set the cell type to WBC so that the packet framed shall be for
		    //!WBC Pulse height
			m_eTypeofCell = eCellWBC;
			//!Frame the packet for Patient handling: WBC pulse height and
			//!Send to the processor
			CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\
			        DELFINO_WBC_PULSE_HEIGHT_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request RBC pulse height,
        //!Frame the packet for RBC pulse height and send it to processor
		case SITARA_WB_RBC_PULSE_HEIGHT_CMD:
            //!Set the cell type to RBC so that the packet framed shall be for
            //!RBC Pulse height
			m_eTypeofCell = eCellRBC;
            //!Frame the packet for Patient handling: RBC pulse height and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\
			        DELFINO_RBC_PULSE_HEIGHT_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request PLT pulse height,
        //!Frame the packet for PLT pulse height and send it to processor
		case SITARA_WB_PLT_PULSE_HEIGHT_CMD:
            //!Set the cell type to PLT so that the packet framed shall be for
            //!PLT Pulse height
			m_eTypeofCell = eCellPLT;
            //!Frame the packet for Patient handling: PLT pulse height and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\
			        DELFINO_PLT_PULSE_HEIGHT_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request 1st second of
        //!WBC Raw Data, frame the packet for WBC Raw Data and send it to processor
		case SITARA_WBC_RAW_DATA_1SEC:
            //!Set the cell type to WBC so that the packet framed shall be for
            //!WBC Raw Data
			m_eTypeofCell = eCellWBC;
            //!Frame the packet for Patient handling: WBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\
			        DELFINO_WBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
			break;

	    //!If the command received from processor is to request 1st second of
	    //!RBC Raw Data, frame the packet for RBC Raw Data and send it to processor
		case SITARA_RBC_RAW_DATA_1SEC:
            //!Set the cell type to RBC so that the packet framed shall be for
            //!RBC Raw Data
			m_eTypeofCell = eCellRBC;
            //!Frame the packet for Patient handling: RBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\
			        DELFINO_RBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
			break;

	    //!If the command received from processor is to request 1st second of
	    //!PLT Raw Data, frame the packet for PLT Raw Data and send it to processor
		case SITARA_PLT_RAW_DATA_1SEC:
            //!Set the cell type to PLT so that the packet framed shall be for
            //!PLT Raw Data
			m_eTypeofCell = eCellPLT;
            //!Frame the packet for Patient handling: PLT Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\
			        DELFINO_PLT_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
			break;

		//!If the command received from processor is for dispensing the diluent
		//!in predilute mode, input the command to the MBD to dispense the diluent
		case SITARA_START_DISPENSE_CMD:
            sprintf(DebugPrintBuf, "\r\n SITARA_START_DISPENSE_CMD\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
		    g_usnCurrentModeTemp = DISPENSE_THE_DILUENT;
		break;

        //!If the command received from processor is to continue the pre-diluent
        //!after dipensing the diluent to the blood sample tube,
        //!input the command to the MBD to dispense the diluent
		case SITARA_START_PRE_DILUENT_COUNTING_CMD:
			//!Re-initilaize the variables and start proceed with pre-diluent
			//!mode after mixing with the blood sample
			CIReInitialiseOnNewMeasurement();
			//!Set the secondary mode (Temp mode) to Pre-Diluent Mode
			//!this mode is the secondary input to Pre-Diluent mode whcih is
			//!continued after the initial pre-diluent mode
			g_usnCurrentModeTemp = START_PRE_DILUENT_COUNTING_CMD;
		break;

        //!If the command received from processor is to request next second of
        //!WBC Raw Data, frame the packet for WBC Raw Data and send it to processor
		case SITARA_WBC_RAW_DATA_NEXT_SEC:
            //!Set the cell type to WBC so that the packet framed shall be for
            //!WBC Raw Data
			m_eTypeofCell = eCellWBC;
            //!Raw data is acquired for total acquisition time of WBC, RBC & PLT
            //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
            //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be
            //!transmitted to processor. It is transmitted in 1 second each as requested
            //!by processor.
            //!So next second data is packetized by enabling the variable
            //!g_bNextSecRawDataReq
			g_bNextSecRawDataReq = TRUE;
            //!Frame the packet for Patient handling: WBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\
			        DELFINO_WBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request next second of
        //!RBC Raw Data, frame the packet for RBC Raw Data and send it to processor
		case SITARA_RBC_RAW_DATA_NEXT_SEC:
            //!Set the cell type to RBC so that the packet framed shall be for
            //!RBC Raw Data
			m_eTypeofCell = eCellRBC;
			//!Raw data is acquired for total acquisition time of WBC, RBC & PLT
			//!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
			//!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be
			//!transmitted to processor. It is transmitted in 1 second each as requested
			//!by processor.
			//!So next second data is packetized by enabling the variable
			//!g_bNextSecRawDataReq
			g_bNextSecRawDataReq = TRUE;
            //!Frame the packet for Patient handling: RBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\
			        DELFINO_RBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request next second of
        //!PLT Raw Data, frame the packet for PLT Raw Data and send it to processor
		case SITARA_PLT_RAW_DATA_NEXT_SEC:
            //!Set the cell type to PLT so that the packet framed shall be for
            //!PLT Raw Data
			m_eTypeofCell = eCellPLT;
            //!Raw data is acquired for total acquisition time of WBC, RBC & PLT
            //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
            //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be
            //!transmitted to processor. It is transmitted in 1 second each as requested
            //!by processor.
            //!So next second data is packetized by enabling the variable
            //!g_bNextSecRawDataReq
			g_bNextSecRawDataReq = TRUE;
            //!Frame the packet for Patient handling: PLT Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\
			        DELFINO_PLT_ADC_RAW_DATA, FALSE);
            //!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to enable the start button
        //!polling, enable the start button polling
		case SITARA_START_POLLING_START_BUTTON_CMD:
		    sprintf(DebugPrintBuf, "\n SITARA_START_POLLING_START_BUTTON_CMD\n");
		    UartDebugPrint((int *)DebugPrintBuf, 50);
            //!Set the cell type to invalid when initiating any seqence or
            //!during the polling of start button
			m_eTypeofCell = ecellInvalid;
			//!To enable start button for next measurement
			//g_bStartButtonStatus = TRUE;
            //!Clear the busy pin, so that process shall send any command for
            //!further sequence initiation
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to disable the start button
        //!polling, disable the start button polling
		case SITARA_STOP_POLLING_START_BUTTON_CMD:
		    sprintf(DebugPrintBuf, "\n SITARA_STOP_POLLING_START_BUTTON_CMD\n");
		    UartDebugPrint((int *)DebugPrintBuf, 50);
            //!Set the cell type to invalid when initiating any seqence or
            //!during the polling of start button
			m_eTypeofCell = ecellInvalid;
			//!To disable start button for next measurement
			g_bStartButtonStatus = FALSE;
			//!Clear the busy pin, so that process shall send any command for
			//!further sequence initiation
			SPIClearCommCtrlBusyInt();
		break;

		//!If the command received from processor is to disable the start button
        //!polling, disable the start button polling
        case SITARA_PULSE_HEIGHT_DATA_RECEIVED:
            //m_eCycleCompletion = eERROR_CHECK;
            g_bDataTransmitted = TRUE;
            SPISetCommCtrlBusyInt();
            sprintf(DebugPrintBuf, "\r\n PULSE_HEIGHT_DATA_received at %ld\n",g_unSequenceStateTime);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            /*! Set the m_bTxPendingFlag to False*/
            m_bTxPendingFlag = FALSE;
        break;

        //!If the minor command is not valid, do nothing and set the cell type
        //!to invalid
		default:
		    //!Cell type is set to invalid when the minor command is invalid
			m_eTypeofCell = ecellInvalid;
		break;
	}//end of switch case
}

/******************************************************************************/
/*!
 *  \fn     CIProcessServiceHandlingCmd()
 *  \brief  To process Sitara Service handling commands
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     CIProcessACKCmd()
 *  \brief  To process Acknowledgement command received from Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessACKCmd(uint16_t usnMinorCmd)
{
    //!If the minor command is Acknowledgment command
    //!Set the NAK received is False, so nothing to be processed and set the packet
    //!Count to Zero, so that no further packet to be transmitted to processor
	if(usnMinorCmd == SITARA_ACK_SUB_CMD)
	{
		m_bNAckReceived = FALSE;
	}
	m_usnPacketCount = 0;
}
/******************************************************************************/
/*!
 *  \fn     CIProcessNACKCmd()
 *  \brief  To process the Negative Acknowledgment command received from Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessNACKCmd(uint16_t usnMinorCmd)
{
    //!If the minor command is NAK command, re-transmit the packet which was
    //!not transmitted
	if(usnMinorCmd == SITARA_NACK_SUB_CMD)
	{
        sprintf(DebugPrintBuf, "\r\n NEGATIVE_ACKNOWLEDGEMENT_HANDELING\n");
        UartDebugPrint((int *)DebugPrintBuf, 50);
        //!Reset the g_usnCurrentModeTemp
        g_usnCurrentModeTemp = ZERO; // Added 8/1/2020
	    //!Check for the previously transmitted minor command for the
	    //!re-transmission of the packet
		switch(m_stPrevTxPacket.usnMinorCommand)
		{
		    //!If the previously transmitted packet is WBC ADC Raw data,
		    //!perform the below operations
			case DELFINO_WBC_ADC_RAW_DATA:
			    //!Set NAK is received to indicate problem in transmission of packet
				m_bNAckReceived = TRUE;
				//!Set the cell type to WBC to transmit WBC packet
				m_eTypeofCell = eCellWBC;
				//!Frame the packet for re-transmission
				CPFrameDataPacket(m_stPrevTxPacket.usnMajorCommand,\
				        m_stPrevTxPacket.usnMinorCommand, FALSE);
		        sprintf(DebugPrintBuf, "\r\n N_A_H..WBC\n");
		        UartDebugPrint((int *)DebugPrintBuf, 50);
				break;

	        //!If the previously transmitted packet is RBC ADC Raw data,
	        //!perform the below operations
			case DELFINO_RBC_ADC_RAW_DATA:
                //!Set NAK is received to indicate problem in transmission of packet
				m_bNAckReceived = TRUE;
				//!Set the cell type to RBC to transmit RBC packet
				m_eTypeofCell = eCellRBC;
                //!Frame the packet for re-transmission
				CPFrameDataPacket(m_stPrevTxPacket.usnMajorCommand,\
				        m_stPrevTxPacket.usnMinorCommand, FALSE);
		        sprintf(DebugPrintBuf, "\r\n N_A_H...RBC\n");
		        UartDebugPrint((int *)DebugPrintBuf, 50);
				break;

	        //!If the previously transmitted packet is PLT ADC Raw data,
	        //!perform the below operations
			case DELFINO_PLT_ADC_RAW_DATA:
                //!Set NAK is received to indicate problem in transmission of packet
				m_bNAckReceived = TRUE;
				//!Set the cell type to PLT to transmit PLT packet
				m_eTypeofCell = eCellPLT;
                //!Frame the packet for re-transmission
				CPFrameDataPacket(m_stPrevTxPacket.usnMajorCommand,\
				        m_stPrevTxPacket.usnMinorCommand, FALSE);
		        sprintf(DebugPrintBuf, "\r\n N_A_H....PLT\n");
		        UartDebugPrint((int *)DebugPrintBuf, 50);
				break;

			default:
			    //!Set NAK is received to indicate problem in transmission of packet
				m_bNAckReceived = TRUE;
				//!In default scenario, re-transmit the previous packet
				CPFrameDataPacket(m_stPrevTxPacket.usnMajorCommand,\
				        m_stPrevTxPacket.usnMinorCommand, FALSE);
				//!Set NAK to false after framing of packet
				m_bNAckReceived = FALSE;
				//!Set packet count to zero so that no further packet to be
				//!transmitted to processor
				m_usnPacketCount =0;
		        sprintf(DebugPrintBuf, "\r\n N_A_H default\n");
		        UartDebugPrint((int *)DebugPrintBuf, 50);
				break;

		}

	}
}
/******************************************************************************/
/*!
 *  \fn     CIFormServiceHandlingPacket()
 *  \brief  To form the service handling packet to be sent to Sitara
 *  \param  uint16_t usnServicehandled -Service handled
 *          bool_t bStartEnd
 *          True for Service Handling start, and false for Service Handling
 *          Completed
 *  \return void
 */
/******************************************************************************/
void CIFormServiceHandlingPacket(uint16_t usnServicehandled, bool_t bStartEnd)
{
	uint16_t usnMinorCmd;

//    sprintf(DebugPrintBuf, "\n CIFormServiceHandlingPacket %x %x\n",usnServicehandled, bStartEnd);
//    UartDebugPrint((int *)DebugPrintBuf, 50);
	//!During service handling, starting of the service handling and end of
	//!service handling to be sent to processor.
	//!\var bStartEnd is used to identify whether the service handling is
	//!starting or ending
	//!If bStartEnd is TRUE, service handling is starting and if bStartEnd is
	//!FALSE, Service handling is ending.

	//!If the Service handling is starting (bStartEnd is true), set the busy
	//!pin to high to indicate processor that controller is busy in running the sequence.
	//!else (bStartEnd is false), clear the busy pin to indicate controller is
	//!not busy and the current sequence is ended.
	if(TRUE == bStartEnd)
	{
        if(g_bBusyBitStatus == FALSE)
        {
            SPISetCommCtrlBusyInt();
        }
	}
    else
    {
        if((SYSTEM_STATUS_SERVICE_HANDLING != usnServicehandled) || \
                        (SPI_ERROR_CHECK != usnServicehandled))
        {
            if(g_bBusyBitStatus == TRUE)
            {
                SPIClearCommCtrlBusyInt();
            }
        }
    }
	//!Check for the current service handling that was initiated
	switch(usnServicehandled)
	{
	    //!If the service handling initiated is Rinse Prime and if the sequence
	    //!is starting, set the \var usnMinorCmd to Rinse Prime started
	    //!If the sequence is ending, set the \var usnMinorCmd to Rinse
	    //!Prime completed
		case PRIME_WITH_RINSE_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
		    {
		        usnMinorCmd = DELFINO_PRIME_WITH_RINSE_STARTED_CMD;
		        //!Disable the Rinse Prime monitoring
		        g_bMonRinsePriming = FALSE;
		    }
		    else
		    {
		        usnMinorCmd = DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD;

                //!Enable the Lyse Prime monitoring
                g_bMonLysePriming = TRUE;

                //!Enable the Diluent Prime monitoring
                g_bMonDiluentPriming = TRUE;

                //!Enable the Rinse Prime monitoring
                g_bMonRinsePriming = TRUE;

                //!Clear Rinse prime error only if the priming sequence is
                //! completed successfully
                utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;
		    }
		break;

        //!If the service handling initiated is Lyse Prime and if the sequence
        //!is starting, set the \var usnMinorCmd to Lyse Prime started
        //!If the sequence is ending, set the \var usnMinorCmd to Lyse
        //!Prime completed
		case PRIME_WITH_LYSE_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_PRIME_WITH_LYSE_STARTED_CMD;

                //!Disable the Lyse Prime monitoring
                g_bMonLysePriming = FALSE;
            }
            else
            {
                usnMinorCmd = DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD;
                //!Enable the Lyse Prime monitoring
                g_bMonLysePriming = TRUE;

                //!Enable the Diluent Prime monitoring
                g_bMonDiluentPriming = TRUE;

                //!Enable the Rinse Prime monitoring
                g_bMonRinsePriming = TRUE;

                //!Clear the lyse prime error only if the priming sequence is
                //! completed successfully
                utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;
            }
		break;

        //!If the service handling initiated is Diluent Prime and if the sequence
        //!is starting, set the \var usnMinorCmd to Diluent Prime started
        //!If the sequence is ending, set the \var usnMinorCmd to Diluent
        //!Prime completed
		case PRIME_WITH_DILUENT_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_PRIME_WITH_DILUENT_STARTED_CMD;
                //!Disable the Diluent Prime monitoring
                g_bMonDiluentPriming = FALSE;
            }
            else
            {
                usnMinorCmd = DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD;
                //!Enable the Lyse Prime monitoring
                g_bMonLysePriming = TRUE;

                //!Enable the Diluent Prime monitoring
                g_bMonDiluentPriming = TRUE;

                //!Enable the Rinse Prime monitoring
                g_bMonRinsePriming = TRUE;

                //!Clear the Diluent prime error only if the priming sequence is
                //! completed successfully
                utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;

            }
		break;

        //!If the service handling initiated is EZ Cleanser and if the sequence
        //!is starting, set the \var usnMinorCmd to EZ cleanser started
        //!If the sequence is ending, set the \var usnMinorCmd to EZ Cleanser
        //!Prime completed
		case PRIME_WITH_EZ_CLEANSER_CMD:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_PRIME_WITH_EZ_CLEANSER_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_EZ_CLEANSER_CMD : %ld\n", g_unSequenceStateTime1);
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
		break;

        //!If the service handling initiated is Prime All and if the sequence
        //!is starting, set the \var usnMinorCmd to Prime All started
        //!If the sequence is ending, set the \var usnMinorCmd to Prime ALL
        //!completed
		case PRIME_ALL_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_PRIME_ALL_STARTED_CMD;

                //!Disable the Diluent Prime monitoring
                g_bMonDiluentPriming = FALSE;

                //!Disable the Lyse Prime monitoring
                g_bMonLysePriming = FALSE;

                //!Disable the Rinse Prime monitoring
                g_bMonRinsePriming = FALSE;

            }
            else
            {
                usnMinorCmd = DELFINO_PRIME_ALL_COMPLETED_CMD;

                //!Enable the Rinse Prime monitoring
                g_bMonRinsePriming = TRUE;

                //!Clear the Diluent prime error
                utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;

                //!Enable the Lyse Prime monitoring
                g_bMonLysePriming = TRUE;

                //!Clear the lyse prime error
                utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;

                //!Enable the Diluent Prime monitoring
                g_bMonDiluentPriming = TRUE;

                //!Clear Rinse prime error
                utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;

            }
		break;

        //!If the service handling initiated is Back Flush and if the sequence
        //!is starting, set the \var usnMinorCmd to Back Flush started
        //!If the sequence is ending, set the \var usnMinorCmd to Back Flush
        //!completed
		case BACK_FLUSH_SERVICE_HANDLING:
		case MBD_STARTUP_BACKFLUSH_CMD:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_BACK_FLUSH_STARTED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_STARTED_CMD\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_BACK_FLUSH_COMPLETED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
		break;

        //!If the service handling initiated is ZAP Aperture and if the sequence
        //!is starting, set the \var usnMinorCmd to ZAP Aperture started
        //!If the sequence is ending, set the \var usnMinorCmd to ZAP Aperture
        //!completed
		case ZAP_APERTURE_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_ZAP_APERTURE_STARTED_CMD;
            }
            else
            {
                //!Zap Completed input given to MBD variable to be made false
                //!when Zapping completed command is sent to Sitara
                //!To avoid conflict in the next sequence of Zap complete
                BloodCellCounter_U.ZapComplete  = FALSE;
                usnMinorCmd = DELFINO_ZAP_APERTURE_COMPLETED_CMD;
            }
        break;

        //!If the service handling initiated is Drain Bath and if the sequence
        //!is starting, set the \var usnMinorCmd to Drain Bath started
        //!If the sequence is ending, set the \var usnMinorCmd to Drain Bath
        //!completed
		case DRAIN_BATH_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_DRAIN_BATH_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_DRAIN_BATH_COMPLETED_CMD;
            }
        break;

        //!If the service handling initiated is Drain All and if the sequence
        //!is starting, set the \var usnMinorCmd to Drain All started
        //!If the sequence is ending, set the \var usnMinorCmd to Drain All
        //!completed
		case DRAIN_ALL_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_DRAIN_ALL_STARTED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_ALL_STARTED_CMD\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_DRAIN_ALL_COMPLETED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_ALL_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the service handling initiated is Clean Bath and if the sequence
        //!is starting, set the \var usnMinorCmd to Clean Bath started
        //!If the sequence is ending, set the \var usnMinorCmd to Clean Bath
        //!completed
		case CLEAN_BATH_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_CLEAN_BATH_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_CLEAN_BATH_COMPLETED_CMD;
            }
        break;

        //!If the data request from processor is System status, no sequence is
        //!involved, it is only sending the system status to processor.
        //!So minor command is set to System Status
		case SYSTEM_STATUS_SERVICE_HANDLING:
			usnMinorCmd = DELFINO_SYSTEM_STATUS_DATA;

		break;

        //!If the command is valve test, no sequence is involved, it is only
        //!sending the status of completion of valve test.
        //!So minor command is set to System Status
		case VALVE_TEST_COMPLETION:
			usnMinorCmd = DELFINO_VALVE_TEST_COMPLETED_CMD;

		break;

		//!If Waste Bin is detected, then send Waste bin full command to processor.
		//!Set the minor command to Waste Bin full.
		case WASTE_BIN_FULL_CMD:
			//usnMinorCmd = DELFINO_SYSTEM_STATUS_DATA;
			if(TRUE == bStartEnd)
            {
			    usnMinorCmd = DELFINO_SET_WASTE_BIN_FULL;
	            sprintf(DebugPrintBuf, "\n DELFINO_SET_WASTE_BIN_FULL\n");
	            UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_CLEAR_WASTE_BIN_FULL;
                sprintf(DebugPrintBuf, "\n DELFINO_CLEAR_WASTE_BIN_FULL\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
		break;

        //!If the service handling initiated is Head Rinse and if the sequence
        //!is starting, set the \var usnMinorCmd to Head Rinse started
        //!If the sequence is ending, set the \var usnMinorCmd to Head Rinse
        //!completed
		case HEAD_RINSING_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
		    {

		    }
		    else
		    {
		        usnMinorCmd = DELFINO_HEAD_RINSING_COMPLETED_CMD;
		    }
			break;

        //!If the service handling initiated is Probe Cleaning and if the sequence
        //!is starting, set the \var usnMinorCmd to Probe Cleaning started
        //!If the sequence is ending, set the \var usnMinorCmd to Probe Cleaning
        //!completed
		case  PROBE_CLEANING_SERVICE_HANDLING:
		    if(TRUE == bStartEnd)
            {

            }
            else
            {
                usnMinorCmd = DELFINO_PROBE_CLEANING_COMPLETED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_PROBE_CLEANING_COMPLETED_CMD\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
			break;
		//SAM_MOTOR_CHK -  START

        //!If the service handling initiated is X-Motor Check, send only the
        //!sequence completed status to the processor.
		case X_AXIS_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD;

            }
            else
            {
                usnMinorCmd = DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD;

            }
			break;

        //!If the service handling initiated is Y-Motor Check, send only the
        //!sequence completed status to the processor.
		case Y_AXIS_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD;
            }
			break;

        //!If the service handling initiated is Diluent Motor Check, send only the
        //!sequence completed status to the processor.
		case DILUENT_SYRINGE_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD;
            }
			break;

        //!If the service handling initiated is Waste Motor Check, send only the
        //!sequence completed status to the processor.
		case WASTE_SYRINGE_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD;
            }
			break;

        //!If the service handling initiated is Sample Motor Check, send only the
        //!sequence completed status to the processor.
		case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD;
            }
			break;
			//SAM_MOTOR_CHK - END

        //!If the service handling initiated is Xountign Time Sequence, send only the
        //!sequence completed status to the processor.
		case COUNTING_TIME_SEQUENCE:
		    if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD;//SKM_CHANGE_COUNTSTART
            }
            else
            {
                usnMinorCmd = DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD;
             }
			break;
		//SKM_WAKEUP_START
        //!If the service handling initiated is Wake Up and if the sequence
        //!is starting, set the \var usnMinorCmd to Wake Up started
        //!If the sequence is ending, set the \var usnMinorCmd to Wake Up
        //!completed
		case MBD_WAKEUP_START:
		    if(TRUE == bStartEnd)
            {
		        usnMinorCmd =  DELFINO_WAKEUP_SEQUENCE_STARTED;
		        sprintf(DebugPrintBuf, "\n DELFINO_WAKEUP_SEQUENCE_STARTED \n");
		        UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_WAKE_UP_COMPLETED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_WAKE_UP_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
		    break;
		    //SKM_WAKEUP_END

		    //HN_TOSHIP_Start
		case MBD_TO_SHIP_SEQUENCE_START:
		     if(TRUE == bStartEnd)
		     {
	               usnMinorCmd = DELFINO_TO_SHIP_STARTED;
		     }
		     else
		     {
		         usnMinorCmd = DELFINO_TO_SHIP_COMPLETED_CMD;
		     }
		     break;
		     //HN_TOSHIP_end

		    //PH_CHANGE_SAMPLE_START_BUTTON - START
		case DELFINO_READY_FOR_ASPIRATION:
		    usnMinorCmd = DELFINO_READY_FOR_ASPIRATION;
		    break;
		    //PH_CHANGE_SAMPLE_START_BUTTON - END

		case DELFINO_ASPIRATION_COMPLETED:
		    usnMinorCmd = DELFINO_ASPIRATION_COMPLETED;
		    break;

		case START_UP_SEQUENCE_MODE:
		case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
		case MBD_STARTUP_FAILED_CMD:
		    usnMinorCmd = DELFINO_START_UP_SEQUENCE_STARTED_CMD;
		    break;

		case MBD_VOLUMETRIC_BOARD_CHECK:
		    if(TRUE == bStartEnd)
             {
		        usnMinorCmd = DELFINO_VOLUMETRIC_CHECK_STARTED;
             }
             else
             {
                 usnMinorCmd = DELFINO_VOLUMETRIC_CHECK_COMPLETED;
             }
		    break;


        case MBD_SLEEP_PROCESS:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_SLEEP_SEQUENCE_STARTED;
             }
             else
             {
                 usnMinorCmd = DELFINO_SLEEP_SEQUENCE_COMPLETED;
             }
            break;

		case DELFINO_ERROR_MINOR_CMD:
		    usnMinorCmd = DELFINO_ERROR_MINOR_CMD;
		    break;

        case MBD_ALL_MOTOR_Y_AXIS_CHECK:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_ALL_MOTOR_Y_AXIS_CHECK_SEQUENCE_STARTED;
             }
             else
             {
                 usnMinorCmd = DELFINO_ALL_MOTOR_Y_AXIS_CHECK_SEQUENCE_COMPLETED;
             }
            break;

        case MBD_ALL_MOTOR_X_AXIS_CHECK:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_ALL_MOTOR_X_AXIS_CHECK_SEQUENCE_STARTED;
             }
             else
             {
                 usnMinorCmd = DELFINO_ALL_MOTOR_X_AXIS_CHECK_SEQUENCE_COMPLETED;
             }
            break;

        case MBD_ALL_MOTOR_SAMPLE_CHECK:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_ALL_MOTOR_SAMPLE_CHECK_SEQUENCE_STARTED;
             }
             else
             {
                 usnMinorCmd = DELFINO_ALL_MOTOR_SAMPLE_CHECK_SEQUENCE_COMPLETED;
             }
            break;

        case MBD_ALL_MOTOR_DILUENT_CHECK:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_ALL_MOTOR_DILUENT_CHECK_SEQUENCE_STARTED;
             }
             else
             {
                 usnMinorCmd = DELFINO_ALL_MOTOR_DILUENT_CHECK_SEQUENCE_COMPLETED;
             }
            break;

        case MBD_ALL_MOTOR_WASTE_CHECK:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_ALL_MOTOR_WASTE_CHECK_SEQUENCE_STARTED;
             }
             else
             {
                 usnMinorCmd = DELFINO_ALL_MOTOR_WSATE_CHECK_SEQUENCE_COMPLETED;
             }
            break;

        case MBD_VOL_TUBE_EMPTY:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_VOL_TUBE_EMPTY_SEQUENCE_STARTED;
             }
             else
             {
                 usnMinorCmd = DELFINO_VOL_TUBE_EMPTY_SEQUENCE_COMPLETED;
             }
            break;

        case SPI_ERROR_CHECK:
            usnMinorCmd = DELFINO_SPI_ERROR_CHECK;
            sprintf(DebugPrintBuf, "\r\n SPI Error Check\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            break;

    case MBD_USER_ABORT_PROCESS:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_ABORT_SEQUENCE_STARTED_CMD;
                sprintf(DebugPrintBuf, "\r\n MBD_USER_ABORT_PROCESS started\n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
             }
             else
             {
                 usnMinorCmd = DELFINO_ABORT_SEQUENCE_COMPLETED_CMD;
                 sprintf(DebugPrintBuf, "\r\n MBD_USER_ABORT_PROCESS Completed\n");
                 UartDebugPrint((int *)DebugPrintBuf, 50);
             }
            break;
        case MBD_BATH_FILL_CMD:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_BATH_FILL_SEQUENCE_STARTED_CMD;
             }
             else
             {
                 usnMinorCmd = DELFINO_BATH_FILL_SEQUENCE_COMPLETED_CMD;
             }
            break;
        case MBD_USER_ABORT_Y_AXIS_PROCESS:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_Y_AXIS_ABORT_SEQUENCE_STARTED_CMD;
             }
             else
             {
                 usnMinorCmd = DELFINO_Y_AXIS_ABORT_SEQUENCE_COMPLETED_CMD;
             }
            break;
        case MBD_ZAP_INITIATE:
            if(TRUE == bStartEnd)
             {
                usnMinorCmd = DELFINO_ZAP_INITIATE_STARTED_CMD;
             }
             else
             {
                 usnMinorCmd = DELFINO_ZAP_INITIATE_COMPLETED_CMD;
             }
            break;
        case  GLASS_TUBE_FILLING:
            if(TRUE == bStartEnd)
            {
               usnMinorCmd = GLASS_TUBE_FILLING_STARTED;
               sprintf(DebugPrintBuf, "\r\n GLASS_TUBE_FILLING_STARTED\n");
                                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = GLASS_TUBE_FILLING_COMPLETED;
                sprintf(DebugPrintBuf, "\r\n GLASS_TUBE_FILLING_COMPLETED\n");
                                 UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            break;
        case COUNT_FAIL:
            if(TRUE == bStartEnd)
            {
               usnMinorCmd = COUNT_FAIL_SEQUNCE_STARTED;
               sprintf(DebugPrintBuf, "\r\n COUNT_FAIL_SEQUNCE_STARTED\n");
                                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = COUNT_FAIL_SEQUNCE_COMPLETED;
                sprintf(DebugPrintBuf, "\r\n COUNT_FAIL_SEQUNCE_COMPLETED\n");
                                 UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            break;
        case HYPO_CLEANER_FAIL:
            if(TRUE == bStartEnd)
            {
               usnMinorCmd = HYPO_CLEANER_FAIL_SEQUNCE_STARTED;
               sprintf(DebugPrintBuf, "\r\n HYPO_CLEANER_FAIL_SEQUNCE_STARTED\n");
                                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = HYPO_CLEANER_FAIL_SEQUNCE_COMPLETED;
                sprintf(DebugPrintBuf, "\r\n HYPO_CLEANER_FAIL_SEQUNCE_COMPLETED\n");
                                 UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            break;

		default:
		    //!In default scenario, set the minor command to null
			usnMinorCmd = NULL;//MBD_UPDATION
			break;

	}//end of switch case
	//!If the minor command is set, then frame the Service handling packet and
	//!send to the processor
	if(usnMinorCmd != NULL)
	{
		CPFrameDataPacket(DELFINO_SERVICE_HANDLING,usnMinorCmd, FALSE);
	}
}
/******************************************************************************/
/*!
 *  \fn     void CIFormStartupHandlingPacket()
 *  \brief  To form Startup handling packet
 *  \param  uint16_t usnServicehandled -Service handled
 *          bool_t bStartEnd
 *          True for Service Handling start, and false for Service Handling
 *          Completed
 *  \return void
 */
/******************************************************************************/
void CIFormStartupHandlingPacket(uint16_t usnServicehandled, bool_t bStartEnd)
{
    uint16_t usnMinorCmd;

    //!During startup handling, starting of the startup handling and end of
    //!startup handling to be sent to processor.
    //!\var bStartEnd is used to identify whether the startup handling is
    //!starting or ending
    //!If bStartEnd is TRUE, startup handling is starting and if bStartEnd is
    //!FALSE, startup handling is ending.

    //!If the startup handling is starting (bStartEnd is true), set the busy
    //!pin to high to indicate processor that controller is busy in running the sequence.
    //!else (bStartEnd is false), clear the busy pin to indicate controller is
    //!not busy and the current sequence is ended.
    if(TRUE == bStartEnd)
    {
        if(g_bBusyBitStatus == FALSE)
        {
            SPISetCommCtrlBusyInt();
        }
    }
    else
    {
        if(g_bBusyBitStatus == TRUE)
        {
            SPIClearCommCtrlBusyInt();
        }
    }
    //!Check for the current startup handling that was initiated
    switch(usnServicehandled)
    {

        //!If the data request from processor is System status, no sequence is
        //!involved, it is only sending the system status to processor.
        //!So minor command is set to System Status
        case SYSTEM_STATUS_SERVICE_HANDLING:
            usnMinorCmd = DELFINO_SYSTEM_STATUS_DATA;

        break;
        case BACK_FLUSH_SERVICE_HANDLING:
        case MBD_STARTUP_BACKFLUSH_CMD:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_BACK_FLUSH_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_BACK_FLUSH_COMPLETED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the startup handling initiated is Prime All and if the sequence
        //!is starting, set the \var usnMinorCmd to Prime All started
        //!If the sequence is ending, set the \var usnMinorCmd to Prime ALL
        //!completed
        case PRIME_ALL_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_PRIME_ALL_STARTED_CMD;

                //!Disable the Diluent Prime monitoring
                g_bMonDiluentPriming = FALSE;

                //!Disable the Lyse Prime monitoring
                g_bMonLysePriming = FALSE;

                //!Disable the Rinse Prime monitoring
                g_bMonRinsePriming = FALSE;

            }
            else
            {
                usnMinorCmd = DELFINO_PRIME_ALL_COMPLETED_CMD;

                //!Enable the Rinse Prime monitoring
                g_bMonRinsePriming = TRUE;

                //!Clear the Diluent prime error
                utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;

                //!Enable the Lyse Prime monitoring
                g_bMonLysePriming = TRUE;

                //!Clear the lyse prime error
                utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;

                //!Enable the Diluent Prime monitoring
                g_bMonDiluentPriming = TRUE;

                //!Clear Rinse prime error
                utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;

                sprintf(DebugPrintBuf, "\n DELFINO_PRIME_ALL_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //SAM_MOTOR_CHK -  START
        //!If the startup handling initiated is X-Motor Check, send only the
        //!sequence completed status to the processor.
        case X_AXIS_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD;
            }
            break;

        //!If the startup handling initiated is Y-Motor Check, send only the
        //!sequence completed status to the processor.
        case Y_AXIS_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD;
            }
            break;

        //!If the startup handling initiated is Diluent Motor Check, send only the
        //!sequence completed status to the processor.
        case DILUENT_SYRINGE_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD;
            }
            break;

        //!If the startup handling initiated is Waste Motor Check, send only the
        //!sequence completed status to the processor.
        case WASTE_SYRINGE_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD;
            }
            break;

        //!If the startup handling initiated is Sample Motor Check, send only the
        //!sequence completed status to the processor.
        case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD;
            }
            else
            {
                usnMinorCmd = DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD;
            }
            break;
        case MBD_VOLUMETRIC_BOARD_CHECK:
            if(TRUE == bStartEnd)
            {
                //!sequence started
            }
            else
            {
              //!sequence completed
            }
            //SAM_MOTOR_CHK - END
#if 0
        //!If the startup handling initiated is Rinse Prime and if the sequence
        //!is starting, set the \var usnMinorCmd to Rinse Prime started
        //!If the sequence is ending, set the \var usnMinorCmd to Rinse
        //!Prime completed
        case PRIME_WITH_RINSE_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_PRIME_WITH_RINSE_STARTED_CMD;
                //!Disable the Rinse Prime monitoring
                g_bMonRinsePriming = FALSE;
                //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_RINSE_STARTED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD;
                //!Enable the Rinse Prime monitoring
                g_bMonRinsePriming = TRUE;

                //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
                //!Clear Rinse prime error only if the priming sequence is
                //! completed successfully
                utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;
            }
        break;

        //!If the startup handling initiated is Lyse Prime and if the sequence
        //!is starting, set the \var usnMinorCmd to Lyse Prime started
        //!If the sequence is ending, set the \var usnMinorCmd to Lyse
        //!Prime completed
        case PRIME_WITH_LYSE_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_PRIME_WITH_LYSE_STARTED_CMD;

                //!Disable the Lyse Prime monitoring
                g_bMonLysePriming = FALSE;
                //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_LYSE_STARTED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD;
                //!Enable the Lyse Prime monitoring
                g_bMonLysePriming = TRUE;

                //!Enable the Diluent Prime monitoring
                g_bMonDiluentPriming = TRUE;

                //!Clear the lyse prime error only if the priming sequence is
                //! completed successfully
                utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;

                //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the startup handling initiated is Diluent Prime and if the sequence
        //!is starting, set the \var usnMinorCmd to Diluent Prime started
        //!If the sequence is ending, set the \var usnMinorCmd to Diluent
        //!Prime completed
        case PRIME_WITH_DILUENT_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_PRIME_WITH_DILUENT_STARTED_CMD;
                //!Disable the Diluent Prime monitoring
                g_bMonDiluentPriming = FALSE;
                //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_DILUENT_STARTED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD;
                //!Enable the Diluent Prime monitoring
                g_bMonDiluentPriming = TRUE;

                //!Clear the Diluent prime error only if the priming sequence is
                //! completed successfully
                utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;

                //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the startup handling initiated is EZ Cleanser and if the sequence
        //!is starting, set the \var usnMinorCmd to EZ cleanser started
        //!If the sequence is ending, set the \var usnMinorCmd to EZ Cleanser
        //!Prime completed
        case PRIME_WITH_EZ_CLEANSER_CMD:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_PRIME_WITH_EZ_CLEANSER_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_EZ_CLEANSER_CMD : %ld\n", g_unSequenceStateTime1);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the startup handling initiated is Back Flush and if the sequence
        //!is starting, set the \var usnMinorCmd to Back Flush started
        //!If the sequence is ending, set the \var usnMinorCmd to Back Flush
        //!completed
        case BACK_FLUSH_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_BACK_FLUSH_STARTED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_STARTED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_BACK_FLUSH_COMPLETED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the startup handling initiated is ZAP Aperture and if the sequence
        //!is starting, set the \var usnMinorCmd to ZAP Aperture started
        //!If the sequence is ending, set the \var usnMinorCmd to ZAP Aperture
        //!completed
        case ZAP_APERTURE_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_ZAP_APERTURE_STARTED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_ZAP_APERTURE_STARTED_CMD\n");
                // UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                //!Zap Completed input given to MBD variable to be made false
                //!when Zapping completed command is sent to Sitara
                //!To avoid conflict in the next sequence of Zap complete
                BloodCellCounter_U.ZapComplete  = FALSE;
                usnMinorCmd = DELFINO_ZAP_APERTURE_COMPLETED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_ZAP_APERTURE_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the startup handling initiated is Drain Bath and if the sequence
        //!is starting, set the \var usnMinorCmd to Drain Bath started
        //!If the sequence is ending, set the \var usnMinorCmd to Drain Bath
        //!completed
        case DRAIN_BATH_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_DRAIN_BATH_STARTED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_BATH_STARTED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_DRAIN_BATH_COMPLETED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_BATH_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the startup handling initiated is Drain All and if the sequence
        //!is starting, set the \var usnMinorCmd to Drain All started
        //!If the sequence is ending, set the \var usnMinorCmd to Drain All
        //!completed
        case DRAIN_ALL_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_DRAIN_ALL_STARTED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_ALL_STARTED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_DRAIN_ALL_COMPLETED_CMD;
                sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_ALL_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the startup handling initiated is Clean Bath and if the sequence
        //!is starting, set the \var usnMinorCmd to Clean Bath started
        //!If the sequence is ending, set the \var usnMinorCmd to Clean Bath
        //!completed
        case CLEAN_BATH_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_CLEAN_BATH_STARTED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_CLEAN_BATH_STARTED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_CLEAN_BATH_COMPLETED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_CLEAN_BATH_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                // UartDebugPrint((int *)DebugPrintBuf, 50);
            }
        break;

        //!If the command is valve test, no sequence is involved, it is only
        //!sending the status of completion of valve test.
        //!So minor command is set to System Status
        case VALVE_TEST_COMPLETION:
            usnMinorCmd = DELFINO_VALVE_TEST_COMPLETED_CMD;
            //sprintf(DebugPrintBuf, "\n DELFINO_VALVE_TEST_COMPLETED_CMD\n");
            //UartDebugPrint((int *)DebugPrintBuf, 50);
        break;

        //!If Waste Bin is detected, then send Waste bin full command to processor.
        //!Set the minor command to Waste Bin full.
        case WASTE_BIN_FULL_CMD:
            //sprintf(DebugPrintBuf, "\n DELFINO_SYSTEM_STATUS_DATA\n");
            //UartDebugPrint((int *)DebugPrintBuf, 50);
            usnMinorCmd = DELFINO_SYSTEM_STATUS_DATA;
        break;

        //!If the startup handling initiated is Head Rinse and if the sequence
        //!is starting, set the \var usnMinorCmd to Head Rinse started
        //!If the sequence is ending, set the \var usnMinorCmd to Head Rinse
        //!completed
        case HEAD_RINSING_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {

            }
            else
            {
                usnMinorCmd = DELFINO_HEAD_RINSING_COMPLETED_CMD;
                // sprintf(DebugPrintBuf, "\n DELFINO_HEAD_RINSING_COMPLETED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            break;

        //!If the startup handling initiated is Probe Cleaning and if the sequence
        //!is starting, set the \var usnMinorCmd to Probe Cleaning started
        //!If the sequence is ending, set the \var usnMinorCmd to Probe Cleaning
        //!completed
        case  PROBE_CLEANING_SERVICE_HANDLING:
            if(TRUE == bStartEnd)
            {

            }
            else
            {
                usnMinorCmd = DELFINO_PROBE_CLEANING_COMPLETED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_PROBE_CLEANING_COMPLETED_CMD\n");
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            break;

        //!If the startup handling initiated is Xountign Time Sequence, send only the
        //!sequence completed status to the processor.
        case COUNTING_TIME_SEQUENCE:
            if(TRUE == bStartEnd)
            {
                usnMinorCmd = DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD;//SKM_CHANGE_COUNTSTART
                //sprintf(DebugPrintBuf, "\n DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD : %ld\n", g_unSequenceStateTime1);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            else
            {
                usnMinorCmd = DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD;
                //sprintf(DebugPrintBuf, "\n DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
             }


            break;
        //SKM_WAKEUP_START
        //!If the startup handling initiated is Wake Up and if the sequence
        //!is starting, set the \var usnMinorCmd to Wake Up started
        //!If the sequence is ending, set the \var usnMinorCmd to Wake Up
        //!completed
        case MBD_WAKEUP_START:
            if(TRUE == bStartEnd)
            {

            }
            else
            {
                usnMinorCmd = DELFINO_WAKE_UP_COMPLETED_CMD;
                // sprintf(DebugPrintBuf, "\n DELFINO_WAKE_UP_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                //UartDebugPrint((int *)DebugPrintBuf, 50);
            }
            break;
            //SKM_WAKEUP_END

            //HN_TOSHIP_Start
        case MBD_TO_SHIP_SEQUENCE_START:
             if(TRUE == bStartEnd)
             {
                   sprintf(DebugPrintBuf, "\n DELFINO_TO_SHIP_STARTED : %ld\n", g_unSequenceStateTime1);
                   UartDebugPrint((int *)DebugPrintBuf, 50);
                   usnMinorCmd = DELFINO_TO_SHIP_STARTED;
             }
             else
             {
                 usnMinorCmd = DELFINO_TO_SHIP_COMPLETED_CMD;
                 sprintf(DebugPrintBuf, "\n DELFINO_TO_SHIP_COMPLETED_CMD : %ld\n", g_unSequenceStateTime1);
                 UartDebugPrint((int *)DebugPrintBuf, 50);
             }
             break;
             //HN_TOSHIP_end

            //PH_CHANGE_SAMPLE_START_BUTTON - START
        case DELFINO_READY_FOR_ASPIRATION:
            usnMinorCmd = DELFINO_READY_FOR_ASPIRATION;
            break;
            //PH_CHANGE_SAMPLE_START_BUTTON - END
#endif
        case START_UP_SEQUENCE_MODE:
        case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
        case MBD_STARTUP_FAILED_CMD:
            usnMinorCmd = DELFINO_START_UP_SEQUENCE_STARTED_CMD;
            sprintf(DebugPrintBuf, "\r\n DELFINO_SEQUENCE_STARTED : %ld\n", g_unSequenceStateTime1);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            break;

        case DELFINO_ERROR_MINOR_CMD:
            usnMinorCmd = DELFINO_ERROR_MINOR_CMD;
            break;

        default:
            //!In default scenario, set the minor command to null
            usnMinorCmd = NULL;//MBD_UPDATION
            break;

    }//end of switch case

    //!If the minor command is set, then frame the startup handling packet and
    //!send to the processor
    if(usnMinorCmd != NULL)
    {
        //!Frame startup handling packet to be sent to processor
        CPFrameDataPacket(DELFINO_STARTUP_HANDLING_CMD, usnMinorCmd, FALSE);
    }
}
/******************************************************************************/
/*!
 *  \fn     CIFormSyncCmd()
 *  \brief  To form the Sync Command to Sitara
 *  \return  void
 */
/****************************************************************************/
void CIFormSyncCmd()
{
    //!Frame the packet for sync command
	CPFrameDataPacket(DELFINO_SYNC_MAIN_CMD,DELFINO_SYNC_SUB_CMD, FALSE);
}
/******************************************************************************/
/*!
 *  \fn     CIProcessFirmwareVersionReq()
 *  \brief  To process the Firmware Version Command request from Sitara
 *  \return  void
 */
/******************************************************************************/
void CIProcessFirmwareVersionReq()
{
    //!Frame the packet for Firmware version
	CPFrameDataPacket(DELFINO_FIRMWARE_VERSION_CMD,DELFINO_CPU1_CPU2_VERSION,\
	        FALSE);
}
/******************************************************************************/
/*!
 *  \fn     CIProcessSystemSettingsCmd()
 *  \brief  To process system setting command
 *  \param  unsigned char usnMinorCmd
 *  \return  void
 */
/******************************************************************************/
void CIProcessSystemSettingsCmd(uint16_t usnMinorCmd)
{
    //!Check for the minor command
    switch(usnMinorCmd)
    {
        //!If the minor command is to set the bubbling time, set the corresponding
        //!parameters
        case SITARA_BUBBLING_TIME_CMD:
            sprintf(DebugPrintBuf, "\r\n SETTINGS_BUBBLING_TIME_SEQUENCE_HANDLING \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            g_usnFirstDilutionFreq = m_stReceivedPacketFrame.arrusnData[0];
            g_usnLyseMixingFreq = m_stReceivedPacketFrame.arrusnData[1];
            g_usnFinalRBCMixingFreq = m_stReceivedPacketFrame.arrusnData[2];
            g_usnWholeBloodZapTime = m_stReceivedPacketFrame.arrusnData[6];
            g_DiluentMixTime = m_stReceivedPacketFrame.arrusnData[3];
            g_RBCMixTime = m_stReceivedPacketFrame.arrusnData[4];
            g_LyseMixTime = m_stReceivedPacketFrame.arrusnData[5];
            g_usnWbcCalibratedTime = m_stReceivedPacketFrame.arrusnData[7];     //DS_added
            g_usnRbcCalibratedTime = m_stReceivedPacketFrame.arrusnData[8];     //DS_added
            sncalZeroPSI = m_stReceivedPacketFrame.arrusnData[9];      //HN_added
            g_TestDataI0 = m_stReceivedPacketFrame.arrusnData[11];      //HN_added

            //!If the sign value is negative then change sign of sncalZeroPSI to negative
            if(m_stReceivedPacketFrame.arrusnData[10] == 1 ) //index 10 for pressure calibration sign data
            {
                sncalZeroPSI = sncalZeroPSI * (- 1);
            }


            //Changing msec to 20msec counter
            //g_DiluentMixTime = g_DiluentMixTime/20;
            //g_RBCMixTime = g_RBCMixTime/20;
            //g_LyseMixTime = g_LyseMixTime/20;
            g_usnWbcCalibratedTime = g_usnWbcCalibratedTime/20;
            g_usnRbcCalibratedTime = g_usnRbcCalibratedTime/20;

            //!------------>  Mixing Time -start <-------------//////////////
            if(g_LyseMixTime >= 3280)
            {
                g_LyseMixTime = 3280;
            }
            if(g_LyseMixTime <= 1080)
            {
                g_LyseMixTime = 1080;
            }
            /////////////////////////////
            if(g_RBCMixTime >= 1200)
            {
                g_RBCMixTime = 1200;
            }
            if(g_RBCMixTime <= 410)
            {
                g_RBCMixTime = 410;
            }
            ///////////////////////////////
            if(g_DiluentMixTime >= 2050)
            {
                g_DiluentMixTime = 2050;
            }
            if(g_DiluentMixTime <= 680)
            {
                g_DiluentMixTime = 680;
            }
            //////////////////////////////////
            g_DiluentMixTime = g_DiluentMixTime/20;
            g_RBCMixTime = g_RBCMixTime/20;
            g_LyseMixTime = g_LyseMixTime /20;
            //!------------>  Mixing Time - End <-------------//////////////

            if(g_usnWholeBloodZapTime > 10000)
            {
                g_usnWholeBloodZapTime = 10000;
            }
            g_usnWholeBloodZapTime = g_usnWholeBloodZapTime/20;


            sprintf(DebugPrintBuf, "\r\n arrusnData[0] %d\n", m_stReceivedPacketFrame.arrusnData[0]);
                        UartDebugPrint((int *)DebugPrintBuf, 50);
            sprintf(DebugPrintBuf, "\r\n arrusnData[1] %d\n", m_stReceivedPacketFrame.arrusnData[1]);
                        UartDebugPrint((int *)DebugPrintBuf, 50);
            sprintf(DebugPrintBuf, "\r\n arrusnData[2] %d\n", m_stReceivedPacketFrame.arrusnData[2]);
                        UartDebugPrint((int *)DebugPrintBuf, 50);
            sprintf(DebugPrintBuf, "\r\n arrusnData[3] %d\n", m_stReceivedPacketFrame.arrusnData[3]);
                        UartDebugPrint((int *)DebugPrintBuf, 50);
            sprintf(DebugPrintBuf, "\r\n arrusnData[4] %d\n", m_stReceivedPacketFrame.arrusnData[4]);
                         UartDebugPrint((int *)DebugPrintBuf, 50);
            sprintf(DebugPrintBuf, "\r\n arrusnData[5] %d\n", m_stReceivedPacketFrame.arrusnData[5]);
                         UartDebugPrint((int *)DebugPrintBuf, 50);
            sprintf(DebugPrintBuf, "\r\n arrusnData[6] %d\n", m_stReceivedPacketFrame.arrusnData[6]);
                          UartDebugPrint((int *)DebugPrintBuf, 50);
            sprintf(DebugPrintBuf, "\r\n arrusnData[7] %d\n", m_stReceivedPacketFrame.arrusnData[7]);
                           UartDebugPrint((int *)DebugPrintBuf, 50);
           sprintf(DebugPrintBuf, "\r\n arrusnData[8] %d\n", m_stReceivedPacketFrame.arrusnData[8]);
                          UartDebugPrint((int *)DebugPrintBuf, 50);
           sprintf(DebugPrintBuf, "\r\n Default Vacuum: %d\n", m_stReceivedPacketFrame.arrusnData[11]);
                          UartDebugPrint((int *)DebugPrintBuf, 50);




            //!Clear the busy pin after receiving the bubbling time.
            //!So that processor can send any command to initiate next sequence
            SPIClearCommCtrlBusyInt();

            //!Limit the First dilution freq to MIN_MIXING_FREQ (100) if it is
            //! less than MIN_MIXING_FREQ (100)
            if(g_usnFirstDilutionFreq < MIN_MIXING_FREQ)
            {
                g_usnFirstDilutionFreq = MIN_MIXING_FREQ;
            }
            //!Limit the First dilution freq to MAX_MIXING_FREQ (300) if it is
            //! greater than 300
            if(g_usnFirstDilutionFreq > MAX_MIXING_FREQ)
            {
                g_usnFirstDilutionFreq = MAX_MIXING_FREQ;
            }
            //!Limit the Lyse Mixing freq to MIN_MIXING_FREQ (100) if it is
            //! less than MIN_MIXING_FREQ (100)
            if(g_usnLyseMixingFreq < MIN_MIXING_FREQ)
            {
                g_usnLyseMixingFreq = MIN_MIXING_FREQ;
            }
            //!Limit the Lyse Mixing freq to MAX_MIXING_FREQ (300) if it is
            //! greater than MAX_MIXING_FREQ (300)
            if(g_usnLyseMixingFreq > MAX_MIXING_FREQ)
            {
                g_usnLyseMixingFreq = MAX_MIXING_FREQ;
            }
            //!Limit the Final RBC Mixing freq to MIN_MIXING_FREQ (100) if it
            //! is less than MIN_MIXING_FREQ (100)
            if(g_usnFinalRBCMixingFreq < MIN_MIXING_FREQ)
            {
                g_usnFinalRBCMixingFreq = MIN_MIXING_FREQ;
            }
            //!Limit the Final RBC Mixing freq to MAX_MIXING_FREQ (300) if it
            //! is less than MAX_MIXING_FREQ (300)
            if(g_usnFinalRBCMixingFreq > MAX_MIXING_FREQ)
            {
                g_usnFinalRBCMixingFreq = MAX_MIXING_FREQ;
            }
            break;

        case SITARA_PRIME_WITH_RINSE_CMD: // HN Added
            //!Set the current mode to Rinse prime before flow cal Sequence
            //!which will be given
            //!input to MBD to initiate Counting Time Sequence
            g_usnCurrentMode = RINSE_PRIME_FLOW_CALIBRATION;
             break;

        case SITARA_FLOW_CALIBRATION_CMD:
            //!Set the current mode to Counting Time Sequence which will be given
            //!input to MBD to initiate Counting Time Sequence
            g_usnCurrentMode = FLOW_CALIBRATION;
            break;
        case SITARA_PRESSURE_CALIBRATION_CMD: // HN Added
            SPIClearCommCtrlBusyInt();
            sprintf(statDebugPrintBuf, "\r\n SITARA_PRESSURE_CALIBRATION_CMD received\n");
            UartDebugPrint((int *)statDebugPrintBuf, 50);
            //!The current execution mode shall be set to no mode
            g_usnCurrentModeMajor = ZERO;
            //!The current execution mode shall be set to no mode
            g_usnCurrentMode = ZERO;
            g_bPressureCaliStart = TRUE ;
            break;
        case SITARA_FLOW_CALIBRATION_RESULTS_CMD:
            //!Set the current mode to Counting Time Sequence which will be given
            //!input to MBD to initiate Counting Time Sequence
            //g_usnCurrentMode = FLOW_CALIBRATION;
            g_usnWbcCalibratedTime = m_stReceivedPacketFrame.arrusnData[0];     //DS_added
            g_usnRbcCalibratedTime = m_stReceivedPacketFrame.arrusnData[1];     //DS_added

            //Changing msec to 20msec counter
            g_usnWbcCalibratedTime = g_usnWbcCalibratedTime/20;
            g_usnRbcCalibratedTime = g_usnRbcCalibratedTime/20;

            //!Clear the busy pin
            SPIClearCommCtrlBusyInt();
            //!Reset the current mode variables
            g_usnCurrentModeMajor = ZERO;
            g_usnCurrentMode = ZERO;
            g_usnCurrentModeTemp = ZERO;
            sprintf(DebugPrintBuf, "\r\n SITARA_FLOW_CALIBRATION_RESULTS_CMD \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            sprintf(DebugPrintBuf, "\r\n g_usnWbcCalibratedTime : %d\n", g_usnWbcCalibratedTime);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            sprintf(DebugPrintBuf, "\r\n g_usnRbcCalibratedTime : %d\n", g_usnRbcCalibratedTime);
            UartDebugPrint((int *)DebugPrintBuf, 50);
            break;
        case SITARA_TEST_DATA_FROM_UI:
            sprintf(DebugPrintBuf, "\r\n SITARA_TEST_DATA_FROM_UI \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            //g_TestDataI0 = m_stReceivedPacketFrame.arrusnData[0]; //Using for  count vacuum
            g_TestDataI1 = m_stReceivedPacketFrame.arrusnData[1];
            g_TestDataI2 = m_stReceivedPacketFrame.arrusnData[2]; //For settling Time

            //!Clear the busy pin after receiving the bubbling time.
            //!So that processor can send any command to initiate next sequence
            SPIClearCommCtrlBusyInt();

            break;

        default:
            break;
    }

}


void CIValveTestCompleted(unsigned char ucValveNumber)
{
    //!Set the valve number which was tested
	m_ucValveNo = ucValveNumber;
	//!Frame the service handling packet for valve test completion
	CIFormServiceHandlingPacket(VALVE_TEST_COMPLETION, FALSE);
}
/******************************************************************************/
/*!
 *  \fn     void CIFormACKPacket()
 *  \brief  To frame and send an Acknowledgment packet to sitara
 *  \param  bool_t bTransmitNACK To check the NAK
 *  \return  void
 */
/******************************************************************************/
void CIFormACKPacket(bool_t bTransmitNACK)
{
    //!If the command received from processor is not valid, then send NAK
    //!packet to processor.
    //!If the command received from processor is valid, then send Ack packet
    //!to processor
	if(bTransmitNACK == TRUE)
	{
	    //!AFter boot-up, NAK command is continuously sent to Sitara
	    //! which is blocking the version command and IPC synchronization at Sitara
	    //! is not happening due to multiple NAK commands.
	    //! So NAK is removed.
	    //! Additionally, NAK is not handled by process.It shall be used in future
        //sprintf(DebugPrintBuf, "\n DELFINO_NACK_SUB_CMD \n");
        //UartDebugPrint((int *)DebugPrintBuf, 50);
		//CPFrameDataPacket(DELFINO_NACK_MAIN_CMD, DELFINO_NACK_SUB_CMD, FALSE);
	}
	else
	{
        sprintf(DebugPrintBuf, "\n DELFINO_ACK_SUB_CMD \n");
        UartDebugPrint((int *)DebugPrintBuf, 50);
		CPFrameDataPacket(DELFINO_ACK_MAIN_CMD, DELFINO_ACK_SUB_CMD, FALSE);
	}
}
/******************************************************************************/
/*!
 *  \fn     void CIFormPatientHandlingPacket()
 *  \brief  To form Patient handling packet
 *  \param  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormPatientHandlingPacket(uint16_t usnSubCmd)
{
    //!Frame patient handling packet to be sent to processor
	CPFrameDataPacket(DELFINO_PATIENT_HANDLING, usnSubCmd, FALSE);
}
/******************************************************************************/
/*!
 *  \fn     void CIReInitialiseOnNewMeasurement()
 *  \brief  To initialize the variable on every new measurement
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void CIReInitialiseOnNewMeasurement()
{
	//!Reset the the ACK variable
	m_bNAckReceived = FALSE;

	//g_bStartButtonStatus = TRUE;
	//! Set the cell type to Invalid
	m_eTypeofCell = ecellInvalid;

	//!Reset the cycle completion to default on every new measurement -HM
	//m_eCycleCompletion = eDefault;

	//!Sequence completed to be set to false at the beginning of the sequence -HM
	//m_bSequenceCompleted = FALSE;

	//! Reset the PAcket count
	m_usnPacketCount = 0;
	//! Reset the NextSecRawDataReq
	g_bNextSecRawDataReq = FALSE;
	//! Reset the Monitoring timer on new measurement
	g_unSequenceStateTime = 0;
	//! Call Comm Protocol ReInitialiseOnNewMeasurement Function
	CPReInitialiseOnNewMeasurement();

	SysReInit();
}
/******************************************************************************/
/*!
 *  \fn     CIProcessCalibrationHandlingCmd()
 *  \brief  To process the Calibration Handling sent by Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessCalibrationHandlingCmd(uint16_t usnMinorCmd)
{
    //!Check the minor command received in calibration mode and set the sequence
    //!accordingly
	switch(usnMinorCmd)
	{
	    //!If the command received from processor is Auto Calibration,
	    //!Set the current mode to Auto Calibration which will be given
	    //!input to MBD to initiate Auto Calibration
		case SITARA_CALIBRATION_AUTO_WB:
            //!Re-initilaize the variables and proceed
            CIReInitialiseOnNewMeasurement();

			g_usnCurrentMode = AUTO_CALIBRATION_WB;
		break;

        //!If the command received from processor is WBC Commercial Calibration,
        //!Set the current mode to WBC Commercial Calibration which will be given
        //!input to MBD to initiate WBC Commercial Calibration
		case SITARA_CALIBRATION_COMMERCIAL_WBC:
            //!Re-initilaize the variables and proceed
            CIReInitialiseOnNewMeasurement();

			g_usnCurrentMode = COMMERCIAL_CALIBRATION_WBC;
		break;

        //!If the command received from processor is RBC Commercial Calibration,
        //!Set the current mode to RBC Commercial Calibration which will be given
        //!input to MBD to initiate RBC Commercial Calibration
		case SITARA_CALIBRATION_COMMERCIAL_RBC:
            //!Re-initilaize the variables and proceed
            CIReInitialiseOnNewMeasurement();

            g_usnCurrentMode = COMMERCIAL_CALIBRATION_RBC;
        break;

        //!If the command received from processor is PLT Commercial Calibration,
        //!Set the current mode to PLT Commercial Calibration which will be given
        //!input to MBD to initiate PLT Commercial Calibration
		case SITARA_CALIBRATION_COMMERCIAL_PLT:
            //!Re-initilaize the variables and proceed
            CIReInitialiseOnNewMeasurement();

            g_usnCurrentMode = COMMERCIAL_CALIBRATION_PLT;
        break;

        //!If the command received from processor is to request WBC pulse height,
        //!Frame the packet for WBC pulse height and send it to processor
		case SITARA_WB_WBC_PULSE_HEIGHT_CMD:
            //!Set the cell type to WBC so that the packet framed shall be for
            //!WBC Pulse height
			m_eTypeofCell = eCellWBC;
            //!Frame the packet for Calibration handling: WBC pulse height and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\
			        DELFINO_WBC_PULSE_HEIGHT_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request RBC pulse height,
        //!Frame the packet for WBC pulse height and send it to processor
		case SITARA_WB_RBC_PULSE_HEIGHT_CMD:
            //!Set the cell type to RBC so that the packet framed shall be for
            //!rBC Pulse height
			m_eTypeofCell = eCellRBC;
            //!Frame the packet for Calibration handling: RBC pulse height and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\
			        DELFINO_RBC_PULSE_HEIGHT_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is requent for PLT Pulse Height,
        //!perform the below operations
		case SITARA_WB_PLT_PULSE_HEIGHT_CMD:
		    //!Set the cell type to PLT
			m_eTypeofCell = eCellPLT;
			//!Frame the PLT pulse height data
			CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\
			        DELFINO_PLT_PULSE_HEIGHT_DATA, FALSE);
			//!Clear the busy pin after completion of transmission of data
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request 1st second of
        //!WBC Raw Data, frame the packet for WBC Raw Data and send it to processor
		case SITARA_WBC_RAW_DATA_1SEC:
            //!Set the cell type to WBC so that the packet framed shall be for
            //!WBC Raw Data
			m_eTypeofCell = eCellWBC;

            sprintf(DebugPrintBuf, "\r\n SITARA_WBC_RAW_DATA_1SEC rcd\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Frame the packet for Patient handling: WBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\
			        DELFINO_WBC_ADC_RAW_DATA, FALSE);

            sprintf(DebugPrintBuf, "\r\n SITARA_WBC_RAW_DATA_1SEC in snt \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

		//!If the command received from processor is to request 1st second of
		//!RBC Raw Data, frame the packet for RBC Raw Data and send it to processor
		case SITARA_RBC_RAW_DATA_1SEC:
            //!Set the cell type to RBC so that the packet framed shall be for
            //!RBC Raw Data
			m_eTypeofCell = eCellRBC;

            sprintf(DebugPrintBuf, "\r\n SITARA_RBC_RAW_DATA_1SEC in rcd\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Frame the packet for Patient handling: RBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\
			        DELFINO_RBC_ADC_RAW_DATA, FALSE);

            sprintf(DebugPrintBuf, "\r\n SITARA_RBC_RAW_DATA_1SEC in snt\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request 1st second of
        //!PLT Raw Data, frame the packet for PLT Raw Data and send it to processor
        case SITARA_PLT_RAW_DATA_1SEC:
            //!Set the cell type to PLT so that the packet framed shall be for
            //!PLT Raw Data
			m_eTypeofCell = eCellPLT;
            //!Frame the packet for Patient handling: PLT Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\
			        DELFINO_PLT_ADC_RAW_DATA, FALSE);

            sprintf(DebugPrintBuf, "\r\n SITARA_PLT_RAW_DATA_1SEC in cmr\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);

            //!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

		//!If the command received from processor is for dispensing the diluent
		//!in pre-dilute mode, input the command to the MBD to dispense the diluent
		case SITARA_START_DISPENSE_CMD:
			g_usnCurrentMode = DISPENSE_THE_DILUENT;
		break;

        //!If the command received from processor is to continue the pre-diluent
        //!after dispensing the diluent to the blood sample tube,
        //!input the command to the MBD to dispense the diluent
		case SITARA_START_PRE_DILUENT_COUNTING_CMD:
            //!Re-initialize the variables and start proceed with pre-diluent
            //!mode after mixing with the blood sample
			CIReInitialiseOnNewMeasurement();
            //!Set the secondary mode (Temp mode) to Pre-Diluent Mode
            //!this mode is the secondary input to Pre-Diluent mode whcih is
            //!continued after the initial pre-diluent mode
			g_usnCurrentMode = START_PRE_DILUENT_COUNTING_CMD; //CRH_MBD
		break;

        //!If the command received from processor is to request next second of
		//!WBC Raw Data, frame the packet for WBC Raw Data and send it to processor
		case SITARA_WBC_RAW_DATA_NEXT_SEC:
            //!Set the cell type to WBC so that the packet framed shall be for
            //!WBC Raw Data
			m_eTypeofCell = eCellWBC;
            //!Raw data is acquired for total acquisition time of WBC, RBC & PLT
            //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
            //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be
            //!transmitted to processor. It is transmitted in 1 second each as requested
            //!by processor.
            //!So next second data is packetized by enabling the variable
            //!g_bNextSecRawDataReq
			g_bNextSecRawDataReq = TRUE;
            //!Frame the packet for Patient handling: WBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\
			        DELFINO_WBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request next second of
        //!RBC Raw Data, frame the packet for RBC Raw Data and send it to processor
		case SITARA_RBC_RAW_DATA_NEXT_SEC:
            //!Set the cell type to RBC so that the packet framed shall be for
            //!RBC Raw Data
			m_eTypeofCell = eCellRBC;
            //!Raw data is acquired for total acquisition time of WBC, RBC & PLT
            //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
            //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be
            //!transmitted to processor. It is transmitted in 1 second each as requested
            //!by processor.
            //!So next second data is packetized by enabling the variable
            //!g_bNextSecRawDataReq
			g_bNextSecRawDataReq = TRUE;
            //!Frame the packet for Patient handling: RBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\
			        DELFINO_RBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request next second of
        //!PLT Raw Data, frame the packet for PLT Raw Data and send it to processor
		case SITARA_PLT_RAW_DATA_NEXT_SEC:
            //!Set the cell type to PLT so that the packet framed shall be for
            //!PLT Raw Data
			m_eTypeofCell = eCellPLT;
            //!Raw data is acquired for total acquisition time of WBC, RBC & PLT
            //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
            //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be
            //!transmitted to processor. It is transmitted in 1 second each as requested
            //!by processor.
            //!So next second data is packetized by enabling the variable
            //!g_bNextSecRawDataReq
			g_bNextSecRawDataReq = TRUE;
            //!Frame the packet for Patient handling: PLT Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\
			        DELFINO_PLT_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to enable the start button
        //!polling, enable the start button polling
		case SITARA_START_POLLING_START_BUTTON_CMD:
            //!Set the cell type to invalid when initiating any seqence or
            //!during the polling of start button
            m_eTypeofCell = ecellInvalid;
            //!To enable start button for next measurement
            //g_bStartButtonStatus = TRUE;
            //!Clear the busy pin, so that process shall send any command for
            //!further sequence initiation
            SPIClearCommCtrlBusyInt();
        break;

        //!If the command received from processor is to disable the start button
        //!polling, disable the start button polling
        case SITARA_STOP_POLLING_START_BUTTON_CMD:
            //!Set the cell type to invalid when initiating any seqence or
            //!during the polling of start button
            m_eTypeofCell = ecellInvalid;
            //!To disable start button for next measurement
            g_bStartButtonStatus = FALSE;
            //!Clear the busy pin, so that process shall send any command for
            //!further sequence initiation
            SPIClearCommCtrlBusyInt();
        break;

        //!If the command received from processor is to disable the start button
        //!polling, disable the start button polling
        case SITARA_PULSE_HEIGHT_DATA_RECEIVED:
            //!Set the cycle completion state to error check
            //m_eCycleCompletion = eERROR_CHECK;
            g_bDataTransmitted = TRUE;
            //!Set the busy line high
            SPISetCommCtrlBusyInt();
            sprintf(DebugPrintBuf, "\n SITARA_PULSE_HEIGHT_DATA_received 2\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
        break;

        default:
            //!Nothing in case of default scenario
        break;
	}
}
/******************************************************************************/
/*!
 *  \fn     void CIFormCalibrationHandlingPacket()
 *  \brief  To form Calibration handling packet
 *  \param  unsigned int usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormCalibrationHandlingPacket(uint16_t usnSubCmd)
{
    //!Frame packet for calibration handling
	CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING, usnSubCmd, FALSE);
}
/******************************************************************************/
/*!
 *  \fn     CIProcessQualityHandlingCmd()
 *  \brief  To process the Quality Handling sent by Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessQualityHandlingCmd(uint16_t usnMinorCmd)
{
    //!Check the minor command received in calibration mode and set the sequence
    //!accordingly
	switch(usnMinorCmd)
	{
        //!If the command received from processor is Whole blood QC,
        //!Set the current mode to Whole blood QC which will be given
        //!input to MBD to initiate Whole blood QC
		case SITARA_WHOLE_BLOOD_CONTROL:
		{
            //!Re-initilaize the variables and proceed
            CIReInitialiseOnNewMeasurement();

			g_usnCurrentMode = QUALITY_CONTROL_WB;
		}
		break;

        //!If the command received from processor is Body Fluid QC,
        //!Set the current mode to Body Fluid QC which will be given
        //!input to MBD to initiate Body Fluid QC
		case SITARA_BODY_FLUID_CONTROL://case QUALITY_CONTROL_BODY_FLUID:SKM_CHANGE_QC
		{
            //!Re-initilaize the variables and proceed
            CIReInitialiseOnNewMeasurement();

			g_usnCurrentMode = QUALITY_CONTROL_BODY_FLUID;
		}
		break;

        //!If the command received from processor is to request WBC pulse height,
        //!Frame the packet for WBC pulse height and send it to processor
		case SITARA_WB_WBC_PULSE_HEIGHT_CMD:
            //!Set the cell type to WBC so that the packet framed shall be for
            //!WBC Pulse height
			m_eTypeofCell = eCellWBC;
            //!Frame the packet for Patient handling: WBC pulse height and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\
			        DELFINO_WBC_PULSE_HEIGHT_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request RBC pulse height,
        //!Frame the packet for RBC pulse height and send it to processor
		case SITARA_WB_RBC_PULSE_HEIGHT_CMD:
            //!Set the cell type to RBC so that the packet framed shall be for
            //!RBC Pulse height
			m_eTypeofCell = eCellRBC;
            //!Frame the packet for Patient handling: RBC pulse height and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\
			        DELFINO_RBC_PULSE_HEIGHT_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request PLT pulse height,
        //!Frame the packet for PLT pulse height and send it to processor
		case SITARA_WB_PLT_PULSE_HEIGHT_CMD:
            //!Set the cell type to PLT so that the packet framed shall be for
            //!PLT Pulse height
			m_eTypeofCell = eCellPLT;
            //!Frame the packet for Patient handling: PLT pulse height and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\
			        DELFINO_PLT_PULSE_HEIGHT_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request 1st second of
        //!WBC Raw Data, frame the packet for WBC Raw Data and send it to processor
		case SITARA_WBC_RAW_DATA_1SEC:
            //!Set the cell type to WBC so that the packet framed shall be for
            //!WBC Raw Data
			m_eTypeofCell = eCellWBC;
            //!Frame the packet for Patient handling: WBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\
			        DELFINO_WBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
			break;

	    //!If the command received from processor is to request 1st second of
	    //!RBC Raw Data, frame the packet for RBC Raw Data and send it to processor
		case SITARA_RBC_RAW_DATA_1SEC:
            //!Set the cell type to RBC so that the packet framed shall be for
            //!RBC Raw Data
			m_eTypeofCell = eCellRBC;
            //!Frame the packet for Patient handling: RBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\
			        DELFINO_RBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
			break;

	    //!If the command received from processor is to request 1st second of
	    //!PLT Raw Data, frame the packet for PLT Raw Data and send it to processor
		case SITARA_PLT_RAW_DATA_1SEC:
            //!Set the cell type to PLT so that the packet framed shall be for
            //!PLT Raw Data
			m_eTypeofCell = eCellPLT;
            //!Frame the packet for Patient handling: PLT Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\
			        DELFINO_PLT_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
			break;

	    //!If the command received from processor is to request next second of
	    //!WBC Raw Data, frame the packet for WBC Raw Data and send it to processor
		case SITARA_WBC_RAW_DATA_NEXT_SEC:
            //!Set the cell type to WBC so that the packet framed shall be for
            //!WBC Raw Data
			m_eTypeofCell = eCellWBC;
            //!Raw data is acquired for total acquisition time of WBC, RBC & PLT
            //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
            //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be
            //!transmitted to processor. It is transmitted in 1 second each as requested
            //!by processor.
            //!So next second data is packetized by enabling the variable
            //!g_bNextSecRawDataReq
			g_bNextSecRawDataReq = TRUE;
            //!Frame the packet for Patient handling: WBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\
			        DELFINO_WBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request next second of
        //!RBC Raw Data, frame the packet for RBC Raw Data and send it to processor
		case SITARA_RBC_RAW_DATA_NEXT_SEC:
            //!Set the cell type to RBC so that the packet framed shall be for
            //!RBC Raw Data
			m_eTypeofCell = eCellRBC;
            //!Raw data is acquired for total acquisition time of WBC, RBC & PLT
            //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
            //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be
            //!transmitted to processor. It is transmitted in 1 second each as requested
            //!by processor.
            //!So next second data is packetized by enabling the variable
            //!g_bNextSecRawDataReq
			g_bNextSecRawDataReq = TRUE;
            //!Frame the packet for Patient handling: RBC Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\
			        DELFINO_RBC_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to request next second of
        //!PLT Raw Data, frame the packet for PLT Raw Data and send it to processor
		case SITARA_PLT_RAW_DATA_NEXT_SEC:
            //!Set the cell type to PLT so that the packet framed shall be for
            //!PLT Raw Data
			m_eTypeofCell = eCellPLT;
            //!Raw data is acquired for total acquisition time of WBC, RBC & PLT
            //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
            //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be
            //!transmitted to processor. It is transmitted in 1 second each as requested
            //!by processor.
            //!So next second data is packetized by enabling the variable
            //!g_bNextSecRawDataReq
			g_bNextSecRawDataReq = TRUE;
            //!Frame the packet for Patient handling: PLT Raw data and
            //!Send to the processor
			CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\
			        DELFINO_PLT_ADC_RAW_DATA, FALSE);
			//!Clear the busy pin, after framing and sending the packet
			SPIClearCommCtrlBusyInt();
		break;

        //!If the command received from processor is to enable the start button
        //!polling, enable the start button polling
		case SITARA_START_POLLING_START_BUTTON_CMD:
            //!Set the cell type to invalid when initiating any seqence or
            //!during the polling of start button
            m_eTypeofCell = ecellInvalid;
            //!To enable start button for next measurement
            //g_bStartButtonStatus = TRUE;
            //!Clear the busy pin, so that process shall send any command for
            //!further sequence initiation
            SPIClearCommCtrlBusyInt();
        break;

        //!If the command received from processor is to disable the start button
        //!polling, disable the start button polling
        case SITARA_STOP_POLLING_START_BUTTON_CMD:
            //!Set the cell type to invalid when initiating any seqence or
            //!during the polling of start button
            m_eTypeofCell = ecellInvalid;
            //!To disable start button for next measurement
            g_bStartButtonStatus = FALSE;
            //!Clear the busy pin, so that process shall send any command for
            //!further sequence initiation
            SPIClearCommCtrlBusyInt();
        break;

        //!If the command received from processor is to disable the start button
        //!polling, disable the start button polling
        case SITARA_PULSE_HEIGHT_DATA_RECEIVED:
            //!Set the cycle state to error check after transmission of pulse height data
            //! So that the errors if any shall be sent to Processor
            //m_eCycleCompletion = eERROR_CHECK;
            g_bDataTransmitted = TRUE;
            //!Set the busy pin to high, indicating the current sequence is running
            SPISetCommCtrlBusyInt();
            sprintf(DebugPrintBuf, "\n SITARA_PULSE_HEIGHT_DATA_received 3\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
        break;

        //!If the minor command is not valid, do nothing
		default :
			break;
	}
}
/******************************************************************************/
/*!
 *  \fn     void CIFormQualityHandlingPacket()
 *  \brief  To form Quality handling packet
 *  \param  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormQualityHandlingPacket(uint16_t usnSubCmd)
{
    //!Frame packet for Quality Handling
	CPFrameDataPacket(DELFINO_QUALITY_HANDLING, usnSubCmd, FALSE);
}
/******************************************************************************/
/*!
 *  \fn     void CIFormAbortProcessPacket()
 *  \brief  To form packet for abort command
 *  \param  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormAbortProcessPacket(uint16_t usnSubCmd)
{
    //!Frame packet when any sequence is aborted
	CPFrameDataPacket(DELFINO_ABORT_PROCESS_MAIN_CMD, usnSubCmd, FALSE);
}
//CRH_SEQUENCE_STATE - START
/******************************************************************************/
/*!
 *  \fn     void CIFormSequenceStatusPacket()
 *  \brief  To form Sequence Status packet to be sent to Sitara
 *  \param  uint16_t usnSequenceState
 *  \return  void
 */
/******************************************************************************/
void CIFormSequenceStatusPacket(uint16_t usnSequenceState)
{
/*
    sprintf(DebugPrintBuf, "\n CIFormSequenceStatusPacket %d \n", usnSequenceState);
    UartDebugPrint((int *)DebugPrintBuf, 50);
    //g_unSequenceStateTime = 0;

   uint16_t usnMajorCmd=0,usnMinorCmd=0;
   //!Set major command as Delfino Sequence Monitoring
    usnMajorCmd = DELFINO_MONITOR_SEQUENCE;
    //!Set minor command based on the current sequence and the state
    usnMinorCmd = CIFormSequenceStatusMinorCmd(usnSequenceState);
    if((usnMajorCmd != NULL) && (usnMinorCmd != NULL))
    {
        //!Frame data packet for sequence monitoring
        CPFrameDataPacket(usnMajorCmd,usnMinorCmd, FALSE);
    }*/


}
/******************************************************************************/
/*!
 *  \fn     void CIFormSequenceStatusMinorCmd()
 *  \brief  To form Sequence Status major and minor command
 * depending upon the current mode
 *  \param  uint16_t usnSequenceState
 *  \return  uint16_t usnMinorCmd=0;
 */
/******************************************************************************/
uint16_t CIFormSequenceStatusMinorCmd(uint16_t usnSequenceState)
{
    //!Set the minor command to Zero by default
    uint16_t usnMinorCmd=0;
    //!Check for the current state sent from MBD
    switch(usnSequenceState)
    {
    /*
        case eSEQUENCE_STARTED:
            //Set the minor comand
            usnMinorCmd = CIFormSequneceStartedMinorCmd();
            break;
     */
        //!If the state received from MBD is Ready for Dispensing, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_READY_FOR_DISPENSING
        case eREADY_FOR_DISPENSING:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_READY_FOR_DISPENSING;
            break;

        //!If the state received from MBD is Dispensing Completed, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_DISPENSE_COMPLETED
        case eDISPENSE_COMPLETED:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_DISPENSE_COMPLETED;
            break;

        //!If the state received from MBD is Ready for Aspiration, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_READY_FOR_ASPIRATION
        case eREADY_FOR_ASPIRATION:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_READY_FOR_ASPIRATION;
            break;

        //!If the state received from MBD is Aspiration Completed, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_ASPIRATION_COMPLETED
        case eASPIRATION_COMPLETED_MONITORING:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_ASPIRATION_COMPLETED;
            break;

        //!If the state received from MBD is WBC Dispensing Completed, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_WBC_DISPENSATION_COMPLETED
        case eFIRST_DILUTION_COMPLETED:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_WBC_DISPENSATION_COMPLETED;
            break;

        //!If the state received from MBD is RBC Dispensing Completed, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_RBC_DISPENSATION_COMPLETED
        case eRBC_DISPENSATION_COMPLETED:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_RBC_DISPENSATION_COMPLETED;
            break;

        //!If the state received from MBD is WBC & RBC Bubbling Completed, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_WBC_RBC_BUBBLING_COMPLETED
        case eWBC_RBC_BUBBLING_COMPLETED:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_WBC_RBC_BUBBLING_COMPLETED;
            break;

        //!If the state received from MBD is Counting Started, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_COUNTING_STARTED
        case eCOUNTING_STARTED:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_COUNTING_STARTED;
            break;

        //!If the state received from MBD is Counting Completed, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_COUNTING_COMPLETED
        case eCOUNTING_COMPLETED:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_COUNTING_COMPLETED;
            break;

        //!If the state received from MBD is Backflush Completed, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_BACKFLUSH_COMPLETED
        case eBACKFLUSH_COMPLETED:
            //Set the minor comand
            usnMinorCmd = DELFINO_STATE_BACKFLUSH_COMPLETED;
            break;

        //!If the state received from MBD is Bath Filling Completed, set the
        //!minor command to  the corresponding state
        //!\def DELFINO_STATE_BATH_FILLING_COMPLETED
        case eBATH_FILLING_COMPLETED:
            //!Set the minor comand
            usnMinorCmd = DELFINO_STATE_BATH_FILLING_COMPLETED;
            break;

        //!Do nothing if the state received from MDB doesnot match with
        //!any above mentioned state.
        default:
            break;
    }
    //!Return the minor command to frame the packet
    return usnMinorCmd;

}
/******************************************************************************/
/*!
 *  \fn void CIFormSequneceStartedCmd()
 *  \brief  To form minor command for starting of the sequence based on the mode \
 *  selected
 *  \param void
 *  \return void
 */
/******************************************************************************/
void CIFormSequneceStartedCmd(void)
{
    //!Check for the current mode
//    sprintf(DebugPrintBuf, "\n CIFormSequneceStartedCmd: %x \n", g_usnCurrentMode);
//    UartDebugPrint((int *)DebugPrintBuf, 50);
    switch(g_usnCurrentMode)
        {
            case WHOLE_BLOOD_MODE:
            case PRE_DILUENT_MODE:
            case BODY_FLUID_MODE:
                //usnMajorCmd = DELFINO_WHOLE_BLOOD_STATUS;
                //usnMinorCmd = CIFormSequenceStatusMinorCmd(usnSequenceState);
                //CIFormServiceHandlingPacket(MBD_USER_ABORT_Y_AXIS_PROCESS, TRUE);
                break;

            //!If the current mode mode is any of the service handling mode,
            //!then frame service handling start and end packet to be sent
            //!to processor
            case PRIME_WITH_RINSE_SERVICE_HANDLING:
            case PRIME_WITH_LYSE_SERVICE_HANDLING:
            case PRIME_WITH_DILUENT_SERVICE_HANDLING:
            case PRIME_WITH_EZ_CLEANSER_CMD:
            case PRIME_ALL_SERVICE_HANDLING:
            case BACK_FLUSH_SERVICE_HANDLING:
            case ZAP_APERTURE_SERVICE_HANDLING:
            case DRAIN_BATH_SERVICE_HANDLING:
            case DRAIN_ALL_SERVICE_HANDLING:
            case CLEAN_BATH_SERVICE_HANDLING:
            case HEAD_RINSING_SERVICE_HANDLING:
            case PROBE_CLEANING_SERVICE_HANDLING:
            case MBD_WAKEUP_START:
            case COUNTING_TIME_SEQUENCE:
            case MBD_TO_SHIP_SEQUENCE_START:
            case MBD_VOLUMETRIC_BOARD_CHECK:
            case MBD_SLEEP_PROCESS:
            case MBD_ALL_MOTOR_Y_AXIS_CHECK:
            case MBD_ALL_MOTOR_X_AXIS_CHECK:
            case MBD_ALL_MOTOR_SAMPLE_CHECK:
            case MBD_ALL_MOTOR_DILUENT_CHECK:
            case MBD_ALL_MOTOR_WASTE_CHECK:
            case MBD_VOL_TUBE_EMPTY:
            case X_AXIS_MOTOR_CHECK:
            case Y_AXIS_MOTOR_CHECK:
            case DILUENT_SYRINGE_MOTOR_CHECK:
            case WASTE_SYRINGE_MOTOR_CHECK:
            case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:
            case MBD_BATH_FILL_CMD:
            case MBD_ZAP_INITIATE:
            case GLASS_TUBE_FILLING:
            case COUNT_FAIL:
            case HYPO_CLEANER_FAIL:
            //case MBD_USER_ABORT_PROCESS:
//                sprintf(DebugPrintBuf, "\r\n CIFormSequneceStartedCmd %d  %d \n",g_usnCurrentMode,g_usnCurrentModeMajor);
//                                 UartDebugPrint((int *)DebugPrintBuf, 50);

                if(SITARA_STARTUP_CMD == g_usnCurrentModeMajor)
                {
                    CIFormStartupHandlingPacket(g_usnCurrentMode, TRUE);
                }
                else
                {
                    //!Frame sequence started command and send to Sitara
                    CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);
                }
                break;

            case START_UP_SEQUENCE_MODE:
            case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:
            case MBD_STARTUP_FAILED_CMD:
                CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);
                break;

            case RINSE_PRIME_FLOW_CALIBRATION://SKM_CHANGE_FLOWCAL

                SPISetCommCtrlBusyInt();
                //! Frame sequence started command and send to sitara
                CIFormSystemSettingPacket(DELFINO_FLOW_CALIBRATION_STARTED);
                //CPFrameDataPacket(DELFINO_SYSTEM_SETTING_CMD, DELFINO_FLOW_CALIBRATION_STARTED, TRUE);
                break;

            case SHUTDOWN_SEQUENCE://HN_Added
                 SPISetCommCtrlBusyInt();
                 //! Frame sequence started command and send to sitara
                 CIFormShutDownSequencePacket(DELFINO_SHUTDOWN_SEQUENCE_STARTED);
                 break;

            case MBD_USER_ABORT_PROCESS:
                 SPISetCommCtrlBusyInt();
                 CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);
                 //CIFormAbortProcessPacket(DELFINO_ABORT_SEQUENCE_STARTED_CMD);
                 break;


            case MBD_USER_ABORT_Y_AXIS_PROCESS:
                 SPISetCommCtrlBusyInt();
                 //CIFormAbortProcessPacket(DELFINO_Y_AXIS_ABORT_SEQUENCE_STARTED_CMD);         /*DS Testing*/
                 CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);                           /*DS Testing*/
                 break;
            default:
                break;
        }
}
/******************************************************************************/
/*!
 *  \fn     CIProcessShutdownCmd()
 *  \brief  To to Form the Shutdown Sequence
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     CIFormShutDownSequencePacket()
 *  \brief  To form and send Shutdown completed command to Sitara
 *  \param  uint16_t usnShutdownhandled - Current Mode
 *  \return void
 */
/******************************************************************************/
void CIFormShutDownSequencePacket(uint16_t usnMinorCmd)
{
    //sprintf(DebugPrintBuf, "\n Shutdown Minor Command: %x \n", usnMinorCmd);
    //UartDebugPrint((int *)DebugPrintBuf, 50);

    //!Frame shutdown seqeunce completed command and sent to Sitara
    if(usnMinorCmd != NULL)
    {
        CPFrameDataPacket(DELFINO_SHUTDOWN_SEQUENCE_CMD,usnMinorCmd, FALSE);
    }

}
/******************************************************************************/
/*!
 *  \fn     CIProcessAbortMainProcessCmd()
 *  \brief  To process the abort command(cancel) sent by Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessAbortMainProcessCmd(uint16_t usnMinorCmd)
{
    //!Check for the minor command
    //!If the minor command is Abort process, re-initialize all the MBD related
    //!variables and set the current mode to Abort process.
    switch(usnMinorCmd)
    {
        case SITARA_ABORT_PROCESS_SUB_CMD:
            //!Initialize MBD related variables
            BloodCellCounter_initialize();
            //!Set all motors to idlemode
            SysMotorControlIdleMode();

            PWMParamDefaultState(); //HN_added

            //!Set minor command to MBD
            g_usnCurrentMode = MBD_USER_ABORT_PROCESS;
            g_usnCurrentModeTemp = ZERO;
            //!Clear the Data mode to Zero
            g_usnDataMode = 0;
            //!Set sequence completed to False
            m_bSequenceCompleted = FALSE;
            //!Set cycle completion state to default
            m_eCycleCompletion = eDefault;

            g_bStartButtonStatus = FALSE;

            //sprintf(DebugPrintBuf, "\r\n eDefault 4\n");
            //UartDebugPrint((int *)DebugPrintBuf, 40);

            //!Enable the flag for ready for aspiration which is the default condition
            m_bReadyforAspCommand = TRUE;

            m_bPressureMonitor = FALSE;

            //!Reset the control status flag to false to write next command
            //! to send to Sitara
            m_bSPIWriteControlStatus = FALSE;

            /*!Set the m_bTxToDMAFlag to FALSE*/
            m_bTxToDMAFlag = FALSE;

            /*! Set the m_bTxPendingFlag to False*/
            m_bTxPendingFlag = FALSE;
            SPIReConfigCommDmaReg();
            break;

        //!If the command received from processor is to inform the start button
        //!is pressed
        case SITARA_TRIGGER_BUTTON_PRESSED:
            //!Set the sample start button press to TRUE
            g_bStartButtonStatus = TRUE;
        break;

        case SITARA_ABORT_PROCESS_RESET_COMMAND:

            //!Clear the busy pin so that Processor can send the next command
            SPIClearCommCtrlBusyInt();
            //!Set minor command to MBD
            g_usnCurrentMode = ZERO;
            g_usnCurrentModeMajor = ZERO;
            g_usnCurrentModeTemp = ZERO;


            //!Initialize MBD related variables
            BloodCellCounter_initialize();
            //!Set all the motors to Idle mode - No Motor shall run after
            //!any process is aborted
            SysMotorControlIdleMode();

            PWMParamDefaultState();
            //!Clear the Data mode to Zero
            g_usnDataMode = 0;
            //!Set sequence completed to False
            m_bSequenceCompleted = FALSE;
            //!Set cycle completion state to default
            m_eCycleCompletion = eDefault;

            sprintf(DebugPrintBuf, "\r\n Arrest From Master\n");
            UartDebugPrint((int *)DebugPrintBuf, 40);

            //!Set all the motors to Idle mode - No Motor shall run after
            //!any process is aborted
            SysMotorControlIdleMode();

            //!Enable the flag for ready for aspiration which is the default condition
            m_bReadyforAspCommand = TRUE;

            g_bStartButtonStatus = FALSE;

            m_bPressureMonitor = FALSE;

            //!Reset the control status flag to false to write next command
            //! to send to Sitara
            m_bSPIWriteControlStatus = FALSE;

            /*!Set the m_bTxToDMAFlag to FALSE*/
            m_bTxToDMAFlag = FALSE;

            /*! Set the m_bTxPendingFlag to False*/
            m_bTxPendingFlag = FALSE;
            SPIReConfigCommDmaReg();
            break;

        case SITARA_ABORT_Y_AXIS_PROCESS:
            //! valve should close If any pressure valve is open
            m_bVlaveCheckStatus = false;
            m_ucValveControlTime = ZERO;
            g_usnCurrentMode = MBD_USER_ABORT_Y_AXIS_PROCESS;

            //!Switch on the PWM of the HGB
            PWMHgbPwmControl(START, HGB_PWM_FREQUENCY);

            //sprintf(DebugPrintBuf, "\r\n MBD_USER_ABORT_Y_AXIS_PROCESS Recieved\n\n", BloodCellCounter_Y.CycleFlag);
            //UartDebugPrint((int *)DebugPrintBuf, 50);
            break;
        default:
            break;
    }

}

/******************************************************************************/
/*!
 *  \fn     void CIFormSystemSettingPacket()
 *  \brief  To form system setting packet
 *  \param  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormSystemSettingPacket(uint16_t usnSubCmd)
{
    //!Frame patient handling packet to be sent to processor
    CPFrameDataPacket(DELFINO_SYSTEM_SETTING_CMD, usnSubCmd, FALSE);
}
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************
void CIProcessServiceHandlingCmd(uint16_t usnMinorCmd)
{
    //!Switch on the PWM of the HGB
    PWMHgbPwmControl(START, HGB_PWM_FREQUENCY);
//    sprintf(DebugPrintBuf, "\r\n CIProcessServiceHandlingCmd %d \n\n", usnMinorCmd);
//    UartDebugPrint((int *)DebugPrintBuf, 50);

    //!Check for the minor command of the command received from the processor
	switch (usnMinorCmd)
	{
        //!If the command received from processor is Rinse Prime,
        //!perform the below operations
		case SITARA_PRIME_WITH_RINSE_CMD:
            //!Set the current mode to Rinse Prime which will be given
            //!input to MBD to initiate Rinse Prime
			g_usnCurrentMode = PRIME_WITH_RINSE_SERVICE_HANDLING;
			//!Reset the reagent mon variable -HN
			g_bMonRinsePriming = FALSE;
			break;

        //!If the command received from processor is Lyse Prime,
        //!perform the below operations
		case SITARA_PRIME_WITH_LYSE_CMD:
            //!Set the current mode to Lyse Prime which will be given
            //!input to MBD to initiate Lyse Prime
			g_usnCurrentMode = PRIME_WITH_LYSE_SERVICE_HANDLING;
			//!Reset the reagent mon variable -HN
			g_bMonLysePriming = FALSE;
			break;

        //!If the command received from processor is Diluent Prime,
        //!perform the below operations
		case SITARA_PRIME_WITH_DILUENT_CMD:
            //!Set the current mode to Diluent Prime which will be given
            //!input to MBD to initiate Diluent Prime
			g_usnCurrentMode = PRIME_WITH_DILUENT_SERVICE_HANDLING;
			//!Reset the reagent mon variable -HN
            g_bMonDiluentPriming = FALSE;
			break;

        //!If the command received from processor is EZ Cleaner,
        //!perform the below operations
		case SITARA_PRIME_WITH_EZ_CLEANSER_CMD:
            //!Set the current mode to EZ Cleaner which will be given
            //!input to MBD to initiate EZ Cleaner
			g_usnCurrentMode = PRIME_WITH_EZ_CLEANSER_CMD;
			//g_bStartButtonStatus = TRUE;

			break;

        //!If the command received from processor is Prime All,
        //!perform the below operations
		case SITARA_PRIME_ALL_CMD:
            //!Set the current mode to Prime All which will be given
            //!input to MBD to initiate Prime All
            //sprintf(DebugPrintBuf, "\r\n SITARA_PRIME_ALL_CMD \n");
            ///UartDebugPrint((int *)DebugPrintBuf, 50);
			g_usnCurrentMode = PRIME_ALL_SERVICE_HANDLING;
			//!Reset the reagent mon variables -HN
            g_bMonDiluentPriming = FALSE;
            g_bMonLysePriming = FALSE;
            g_bMonRinsePriming = FALSE;
			break;

        //!If the command received from processor is Back Flush / Flush Aperture,
        //!perform the below operations
		case SITARA_BACK_FLUSH_CMD:
            //!Set the current mode to Back Flush / Flush Aperture which will be given
            //!input to MBD to initiate Back Flush / Flush Aperture
			g_usnCurrentMode = BACK_FLUSH_SERVICE_HANDLING;
			break;

        //!If the command received from processor is ZAP Aperture,
        //!perform the below operations
		case SITARA_ZAP_APERTURE:
            //!Set the current mode to ZAP Aperture which will be given
            //!input to MBD to initiate ZAP Aperture
			g_usnCurrentMode = ZAP_APERTURE_SERVICE_HANDLING;
            break;

        //!If the command received from processor is Drain Bath,
        //!perform the below operations
		case SITARA_DRAIN_BATH:
            //!Set the current mode to Drain Bath which will be given
            //!input to MBD to initiate Drain Bath
			g_usnCurrentMode = DRAIN_BATH_SERVICE_HANDLING;
            break;

        //!If the command received from processor is Drain All,
        //!perform the below operations
		case SITARA_DRAIN_ALL:
            //!Set the current mode to Drain All which will be given
            //!input to MBD to initiate Drain All
			g_usnCurrentMode = DRAIN_ALL_SERVICE_HANDLING;
			//!Reset the reagent mon variables -HN
            g_bMonDiluentPriming = FALSE;
            g_bMonLysePriming = FALSE;
            g_bMonRinsePriming = FALSE;
            break;

        //!If the command received from processor is Clean Bath,
        //!perform the below operations
        case SITARA_CLEAN_BATH:
            //!Set the current mode to Clean Bath which will be given
            //!input to MBD to initiate Clean Bath

            g_usnCurrentMode = CLEAN_BATH_SERVICE_HANDLING;
            //g_usnCurrentMode = MBD_VOL_TUBE_EMPTY; //added by jojo
            break;
        case SITARA_GLASSTUBE_FILLING:
            //JOJO added
            //!Set the current mode to Clean Bath which will be given
            //!input to MBD to initiate Clean Bath

            g_usnCurrentMode = GLASS_TUBE_FILLING;
            //g_usnCurrentMode = MBD_VOL_TUBE_EMPTY;
            break;
        case SITARA_COUNT_FAIL:
            //JOJO Added
            //!Set the current mode to Clean Bath which will be given
            //!input to MBD to initiate Clean Bath
            g_usnCurrentMode = COUNT_FAIL;
            //g_usnCurrentMode = MBD_VOL_TUBE_EMPTY;
            break;
        case SITARA_HYPO_CLEANER_FAILED:
            //JOJO added
            //!Set the current mode to Clean Bath which will be given
            //!input to MBD to initiate Clean Bath
            SysInitProbeCleanArrestSequence(TRUE);
            g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;
            g_usnCurrentMode = HYPO_CLEANER_FAIL;
            break;

        //!If the command received from processor is Valve Test to test specific
        //!valve indicated by processor, perform the below operations
		case SITARA_VALVE_TEST_CMD:
            //!Set the current mode to Valve Test and specific valve will be excited
            //!to check its operation
            //sprintf(DebugPrintBuf, "\r\n SITARA_VALVE_TEST_CMD \n");
            //UartDebugPrint((int *)DebugPrintBuf, 50);
		    //!Set the counter to excite the valve to particular time
		    m_ucValveControlTime = VALVE_CONTROL_TIME;
			SysInitValveTest((unsigned char)m_stReceivedPacketFrame.arrusnData[0]);
			break;

        //!If the command received from processor is to get the sytem status,
        //!perform the below operations
		case SITARA_SYSTEM_STATUS_CMD:
//            sprintf(DebugPrintBuf, "\r\n SITARA_SYSTEM_STATUS_CMD Receiced\n");
//            UartDebugPrint((int *)DebugPrintBuf, 50);
		    //!Packetize the system status and send to processor
		    //CIFormServiceHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);
		    //! Set the flag to indicate the system status command is recieved
		    m_bSystemStatusFlag = TRUE;
			//!System status is requested only when the controller is not
			//!running any sequence, after sending the system status, pull the
			//!busy pin low
			SPIClearCommCtrlBusyInt();
			Counter = 0;
			break;

        //!If the command received from processor is waste bin replace command,
	    //!perform the below operations
		case SITARA_WASTE_BIN_REPLACED_CMD:
		    //!Do not monitor the waste bin once the waste bin is detected.
		    //!Processor shall send commnad indicating waste bin is replaced.
		    //!Start monitoring the waste bin after replaing the waste bin.
			SysInitWasteBinReplaced();

			sprintf(DebugPrintBuf, "\r\n SITARA_WASTE_BIN_REPLACED_CMD received \n");
			UartDebugPrint((int *)DebugPrintBuf, 50);
			//!Clear the busy pin after recieving the waste bin.
			//!So that processor can send any command to initiate next sequence
			SPIClearCommCtrlBusyInt();
			break;

        //!If the command received from processor is Prime with Rinse,
        //!perform the below operations
		case SITARA_START_UP_SEQUENCE_CMD:
            //!Set the current mode to Start up sequence which will be given
            //!input to MBD to initiate start up sequence
			g_usnCurrentMode = START_UP_SEQUENCE_MODE;
			break;

        //!If the command received from processor is Head Rinse,
        //!perform the below operations
		case SITARA_HEAD_RINSING_CMD:
            //!Set the current mode to Head Rinse which will be given
            //!input to MBD to initiate Head Rinse
			g_usnCurrentMode = HEAD_RINSING_SERVICE_HANDLING;
			break;

        //!If the command received from processor is Probe Cleaning,
        //!perform the below operations
		case SITARA_PROBE_CLEANING_CMD:
            //!Set the current mode to Probe Cleaning Prime which will be given
            //!input to MBD to initiate Probe Cleaning
			g_usnCurrentMode = PROBE_CLEANING_SERVICE_HANDLING;
			break;

        //!If the command received from processor is X-Motor Check,
        //!perform the below operations
		case SITARA_X_AXIS_MOTOR_CHECK_CMD:
            //!Set the current mode to X-Motor Check which will be given
            //!input to MBD to initiate X-Motor Check
			g_usnCurrentMode = X_AXIS_MOTOR_CHECK;
			break;

        //!If the command received from processor is Y-Motor Check,
        //!perform the below operations
		case SITARA_Y_AXIS_MOTOR_CHECK_CMD:
            //!Set the current mode to Y-Motor Check which will be given
            //!input to MBD to initiate Y-Motor Check
			g_usnCurrentMode = Y_AXIS_MOTOR_CHECK;
			break;

        //!If the command received from processor is Diluent Motor Check,
        //!perform the below operations
		case SITARA_DILUENT_SYRINGE_MOTOR_CHECK_CMD:
            //!Set the current mode to Diluent Motor Check which will be given
            //!input to MBD to initiate Diluent Motor Check
			g_usnCurrentMode = DILUENT_SYRINGE_MOTOR_CHECK;
			break;

        //!If the command received from processor is Waste Motor Check,
        //!perform the below operations
		case SITARA_WASTE_SYRINGE_MOTOR_CHECK_CMD:
            //!Set the current mode to Waste Motor Check which will be given
            //!input to MBD to initiate Waste Motor Check
			g_usnCurrentMode = WASTE_SYRINGE_MOTOR_CHECK;
			break;

        //!If the command received from processor is Sample Motor Check,
        //!perform the below operations
		case SITARA_SAMPLE_LYSE_SYRINGE_MOTOR_CHECK_CMD:
            //!Set the current mode to Sample Motor Check which will be given
            //!input to MBD to initiate Sample Motor Check
			g_usnCurrentMode = SAMPLE_LYSE_SYRINGE_MOTOR_CHECK;
			break;

        //!If the command received from processor is Counting Time Sequence,
        //!perform the below operations
		case SITARA_COUNTING_TIME_SEQUENCE_START_CMD:
            //!Set the current mode to Counting Time Sequence which will be given
            //!input to MBD to initiate Counting Time Sequence
			g_usnCurrentMode = COUNTING_TIME_SEQUENCE;
			break;

        //!If the command received from processor is Wake Up Sequence,
        //!perform the below operations
		case SITARA_WAKE_UP_SEQUENCE_CMD:

            //!Set the current mode to Wake Up Sequence which will be given
            //!input to MBD to initiate Wake Up Sequence
		    g_usnCurrentMode = MBD_WAKEUP_START;
		    break;

		//!If the command received from processor is request for Home position
		//!status, perform the below operations
		case SITARA_HOME_POSITION_STATUS_CMD:
		    //!Packetize the current Home position status of the motors and
		    //!send to processor
            CPFrameDataPacket(DELFINO_SERVICE_HANDLING, \
                    DELFINO_HOME_POSITION_SENSOR_CHECK, FALSE);
            //!Current Home position status of the motors is requested only
            //!when the controller is not running any sequence, after sending
            //!pull the busy pin low
            SPIClearCommCtrlBusyInt();
		    break;
		case SITARA_START_TOSHIP_SEQUENCE_CMD:
		    //!Set the current mode to ship which will be given
		    //!input to MBD to initiate to ship sequence
		    g_usnCurrentMode = MBD_TO_SHIP_SEQUENCE_START;
		    //!Reset the reagent mon variables -HN
		    g_bMonDiluentPriming = FALSE;
            g_bMonLysePriming = FALSE;
            g_bMonRinsePriming = FALSE;
		    break;


		case SITARA_VOLUMETRIC_CHECK_CMD:
            //!Set the current mode to volumetric board check which will be given
            //!input to MBD to initiate to check volumetric sensors
            g_usnCurrentMode = MBD_VOLUMETRIC_BOARD_CHECK;
		    break;

		case SITARA_VOLUMETRIC_TUBE_CLEAN_CMD:       //HN_Added
            //!Set the current mode to clean volumetric which will be given
            //!input to MBD to initiate to clean volumetric
            g_usnCurrentMode = MBD_VOL_TUBE_EMPTY;
            break;

        case SITARA_SLEEP_SEQUENCE_CMD:
            sprintf(DebugPrintBuf, "\r\n SITARA_SLEEP_SEQUENCE_CMD \n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            //!Set the current mode to Sleep  which will be given
            //!input to MBD to initiate sleep sequence
            g_usnCurrentMode = MBD_SLEEP_PROCESS;
            break;

        case SITARA_ALL_MOTOR_Y_AXIS_CMD:
            //!Set the current mode to Sleep  which will be given
            //!input to MBD to initiate sleep sequence
            g_usnCurrentMode = MBD_ALL_MOTOR_Y_AXIS_CHECK;
            break;

        case SITARA_ALL_MOTOR_X_AXIS_CMD:
            //!Set the current mode to Sleep  which will be given
            //!input to MBD to initiate sleep sequence
            g_usnCurrentMode = MBD_ALL_MOTOR_X_AXIS_CHECK;
            break;

        case SITARA_ALL_MOTOR_SAMPLE_CMD:
            //!Set the current mode to Sleep  which will be given
            //!input to MBD to initiate sleep sequence
            g_usnCurrentMode = MBD_ALL_MOTOR_SAMPLE_CHECK;
            break;

        case SITARA_ALL_MOTOR_DILUENT_CMD:
            //!Set the current mode to Sleep  which will be given
            //!input to MBD to initiate sleep sequence
            g_usnCurrentMode = MBD_ALL_MOTOR_DILUENT_CHECK;
            break;

        case SITARA_ALL_MOTOR_WASTE_CMD:
            //!Set the current mode to Sleep  which will be given
            //!input to MBD to initiate sleep sequence
            g_usnCurrentMode = MBD_ALL_MOTOR_WASTE_CHECK;
            break;
        case SITARA_BATH_FILL_CMD:
            //!Set the current mode to bath fill  which will be given
            //!input to MBD to initiate bath filling sequence
            g_usnCurrentMode = MBD_BATH_FILL_CMD;
            //sprintf(DebugPrintBuf, "\r\n SITARA_BATH_FILL_CMD \n");
            //UartDebugPrint((int *)DebugPrintBuf, 50);
            break;
        case SITARA_INITIATE_ZAP:
            //!Set the current mode to Zap initiate
            g_usnCurrentMode = MBD_ZAP_INITIATE;
            sprintf(DebugPrintBuf, "\r\n SITARA_INITIATE_ZAP Receiced\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            //m_bZapInitiateFlag =  true;
            SPIClearCommCtrlBusyInt();
            break;
            //default case
		default :
			break;

	}//end of switch case
}
void CIProcessShutdownCmd(uint16_t usnMinorCmd)
{
    //!Check for the minor command. If the minor command is Shutdown sequence,
    //!set the current mode to shutdown sequence and enable the start button
    //!to aspirate the EZ Cleanser.
    /*switch(usnMinorCmd)
    {
        case SITARA_SHUTDOWN_SEQUENCE_START_CMD:
            m_bReadyforAspCommand = TRUE;
            //!Set the current mode to Shutdown sequence
            //sprintf(DebugPrintBuf, "\n SHUTDOWN_SEQUENCE_STARTED \n");
            //UartDebugPrint((int *)DebugPrintBuf, 50);
            g_usnCurrentMode = SHUTDOWN_SEQUENCE;
            //g_bStartButtonStatus = TRUE;
            break;

        default:
            break;
    }*/
    if(SITARA_SHUTDOWN_SEQUENCE_START_CMD == usnMinorCmd)
    {
        m_bReadyforAspCommand = TRUE;
        //!Set the current mode to Shutdown sequence
        g_usnCurrentMode = SHUTDOWN_SEQUENCE;
        //g_bStartButtonStatus = TRUE;

    }

}
/******************************************************************************/
/*!
 *  \fn     CIProcessStartupHandlingCmd()
 *  \brief  To process the Sitara startup Handling Commands
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessStartupHandlingCmd(uint16_t usnMinorCmd)
{
    //!Check for the minor command of the command received from the processor
   switch (usnMinorCmd)
   {
       //!If the command received from processor is Prime All,
       //!perform the below operations
       case SITARA_PRIME_ALL_CMD:
           //!Set the current mode to Prime All which will be given
          //!input to MBD to initiate Prime All
          g_usnCurrentMode = PRIME_ALL_SERVICE_HANDLING;
          sprintf(DebugPrintBuf, "\n PRIME_ALL_SERVICE_HANDLING started: %ld\n", g_unSequenceStateTime1);
          UartDebugPrint((int *)DebugPrintBuf, 50);

          /* if(TRUE == m_bAbnormalStartup)
           {
               m_bAbnormalStartup = FALSE;
               //Send Prime all cmd

           }
           else
           {
               //!Send Prime all cmplt to sitara
               CIFormStartupHandlingPacket(PRIME_ALL_SERVICE_HANDLING, FALSE);
               g_usnCurrentModeMajor = ZERO;
               g_usnCurrentMode = ZERO;
           }*/
           break;

       //!If the command received from processor is to get the sytem status,
       //!perform the below operations
       case SITARA_SYSTEM_STATUS_CMD:
           //sprintf(DebugPrintBuf, "\n SYSTEM_TEST_SERVICE_HANDLING \n");
           //UartDebugPrint((int *)DebugPrintBuf, 50);
           //!Packetize the system status and send to processor
           //CPFrameDataPacket(DELFINO_STARTUP_HANDLING_CMD, \
                   DELFINO_SYSTEM_STATUS_DATA, FALSE);

           CIFormStartupHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);
           //!System status is requested only when the controller is not
           //!running any sequence, after sending the system status, pull the
           //!busy pin low
           SPIClearCommCtrlBusyInt();
           break;

       case SITARA_NORMAL_STARTUP_CMD:    //0x1027
           //!Do not perform prime all sequence if regular start up sequence is
           //! requested for.
           //m_bAbnormalStartup = FALSE;
           sprintf(DebugPrintBuf, "\n Initial SITARA_NORMAL_STARTUP_CMD received \n");
           UartDebugPrint((int *)DebugPrintBuf, 50);
           //!Packetize the system status and send to processor
           //CPFrameDataPacket(DELFINO_STARTUP_HANDLING_CMD, \
                   DELFINO_SYSTEM_STATUS_DATA, FALSE);
           CIFormStartupHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);
           //!System status is requested only when the controller is not
           //!running any sequence, after sending the system status, pull the
           //!busy pin low
           SPIClearCommCtrlBusyInt();
           break;
       //!If the command received from processor is to get the sytem status,
       //!perform the below operations
       /*case SITARA_ABNORMAL_STARTUP_CMD:
           //!Perform prime all sequence if abnormal start up sequence is
           //! requested for.
           //m_bAbnormalStartup = TRUE;
           sprintf(DebugPrintBuf, "\n SITARA_ABNORMAL_STARTUP_CMD received \n");
           UartDebugPrint((int *)DebugPrintBuf, 50);
           //!Packetize the system status and send to processor
           //CPFrameDataPacket(DELFINO_STARTUP_HANDLING_CMD, \
                   DELFINO_SYSTEM_STATUS_DATA, FALSE);

           CIFormStartupHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);
           //!System status is requested only when the controller is not
           //!running any sequence, after sending the system status, pull the
           //!busy pin low
           SPIClearCommCtrlBusyInt();
           break;*/
           //!If the command received from processor is Prime with Rinse,
           //!perform the below operations
       case SITARA_START_UP_SEQUENCE_CMD:
           //!Sitara requires the major start up sequence to proceeded in
           //! Patient handling sequence.
           //! So the major command to be changed from Startup to patient handling
           //!Set the major command to patient handling for the startup sequence
           g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;

           sprintf(DebugPrintBuf, "\n Normal internal STARTUP_CMD received\n");
           UartDebugPrint((int *)DebugPrintBuf, 50);
           //!Set the current mode to Start up sequence which will be given
           //!input to MBD to initiate start up sequence
           g_usnCurrentMode = START_UP_SEQUENCE_MODE;
           break;

       case SITARA_STARTUP_FAILED_CMD: //HN_Added(19/12/19)
           sprintf(DebugPrintBuf, "\n SITARA_STARTUP_FAILED_CMD \n");
           UartDebugPrint((int *)DebugPrintBuf, 50);
           //!Set the current mode to Startup failed sequence which will be given
           //!input to MBD to initiate start up sequence
           g_usnCurrentMode = MBD_STARTUP_FAILED_CMD;
           break;

       case SITARA_START_UP_FROM_SERVICE_CMD:   //HN_Added(4/01/19)
          //!Sitara requires the major start up sequence to proceeded in
          //! Patient handling sequence.
          //! So the major command to be changed from Startup to patient handling
          //!Set the major command to service handling for the startup sequence
          g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;

          //!Set the current mode to Start up sequence which will be given
          //!input to MBD to initiate start up sequence
          g_usnCurrentMode = MBD_STARTUP_FROM_SERVICE_SCREEN_CMD;
          sprintf(DebugPrintBuf, "\n STARTUP_FROM_SERVICE_SCREEN_CMD received\n");
          UartDebugPrint((int *)DebugPrintBuf, 50);
          break;

          //!****************************** Imp ********************************
          //!This CMD we should receive only if the previous sequence is shutdown
          //! ******************************************************************
       case SITARA_STARTUP_BACKFLUSH:
          //g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;
         //!Set the current mode to Start up sequence which will be given
         //!input to MBD to initiate start up sequence
         g_usnCurrentMode = MBD_STARTUP_BACKFLUSH_CMD;
         break;

       //!If the command received from processor is X-Motor Check,
       //!perform the below operations
       case SITARA_X_AXIS_MOTOR_CHECK_CMD:
           //!Set the current mode to X-Motor Check which will be given
           //!input to MBD to initiate X-Motor Check
           g_usnCurrentMode = X_AXIS_MOTOR_CHECK;
           break;

       //!If the command received from processor is Y-Motor Check,
       //!perform the below operations
       case SITARA_Y_AXIS_MOTOR_CHECK_CMD:
           /*Hari-Waste bin test -start
           utErrorInfoCheck.stErrorInfo.ulnWasteBinFull = TRUE;
           sprintf(DebugPrintBuf, "\n ulnWasteBinFull \n");
           UartDebugPrint((int *)DebugPrintBuf, 50);
           //!Send Error Message to Processor
           SysInitAbortProcess(TRUE);
           SysErrorCommand(g_usnCurrentMode);
           Hari-Waste bin test -End*/
           //!Set the current mode to Y-Motor Check which will be given
           //!input to MBD to initiate Y-Motor Check
           g_usnCurrentMode = Y_AXIS_MOTOR_CHECK;
           break;

       //!If the command received from processor is Diluent Motor Check,
       //!perform the below operations
       case SITARA_DILUENT_SYRINGE_MOTOR_CHECK_CMD:
           //!Set the current mode to Diluent Motor Check which will be given
           //!input to MBD to initiate Diluent Motor Check
           g_usnCurrentMode = DILUENT_SYRINGE_MOTOR_CHECK;
           break;

       //!If the command received from processor is Waste Motor Check,
       //!perform the below operations
       case SITARA_WASTE_SYRINGE_MOTOR_CHECK_CMD:
           //!Set the current mode to Waste Motor Check which will be given
           //!input to MBD to initiate Waste Motor Check
           g_usnCurrentMode = WASTE_SYRINGE_MOTOR_CHECK;
           break;

       //!If the command received from processor is Sample Motor Check,
       //!perform the below operations
       case SITARA_SAMPLE_LYSE_SYRINGE_MOTOR_CHECK_CMD:
           //!Set the current mode to Sample Motor Check which will be given
           //!input to MBD to initiate Sample Motor Check
           g_usnCurrentMode = SAMPLE_LYSE_SYRINGE_MOTOR_CHECK;
           break;

       //!If the command received from processor is Back Flush / Flush Aperture,
       //!perform the below operations
       case SITARA_BACK_FLUSH_CMD:
           //!Set the current mode to Back Flush / Flush Aperture which will be given
           //!input to MBD to initiate Back Flush / Flush Aperture
           g_usnCurrentMode = BACK_FLUSH_SERVICE_HANDLING;
           break;

       //!If the command received from processor is Rinse Prime,
       //!perform the below operations
       case SITARA_PRIME_WITH_RINSE_CMD:
           //!Set the current mode to Rinse Prime which will be given
           //!input to MBD to initiate Rinse Prime
           g_usnCurrentMode = PRIME_WITH_RINSE_SERVICE_HANDLING;
           break;

       //!If the command received from processor is Lyse Prime,
       //!perform the below operations
       case SITARA_PRIME_WITH_LYSE_CMD:
           //!Set the current mode to Lyse Prime which will be given
           //!input to MBD to initiate Lyse Prime
           g_usnCurrentMode = PRIME_WITH_LYSE_SERVICE_HANDLING;
           break;

       //!If the command received from processor is Diluent Prime,
       //!perform the below operations
       case SITARA_PRIME_WITH_DILUENT_CMD:
           //!Set the current mode to Diluent Prime which will be given
           //!input to MBD to initiate Diluent Prime
           g_usnCurrentMode = PRIME_WITH_DILUENT_SERVICE_HANDLING;
           break;

       //!If the command received from processor is EZ Cleaner,
       //!perform the below operations
       case SITARA_PRIME_WITH_EZ_CLEANSER_CMD:
           //!Set the current mode to EZ Cleaner which will be given
           //!input to MBD to initiate EZ Cleaner
           g_usnCurrentMode = PRIME_WITH_EZ_CLEANSER_CMD;
           //g_bStartButtonStatus = TRUE;
           break;

       //!If the command received from processor is ZAP Aperture,
       //!perform the below operations
       case SITARA_ZAP_APERTURE:
           //!Set the current mode to ZAP Aperture which will be given
           //!input to MBD to initiate ZAP Aperture
           g_usnCurrentMode = ZAP_APERTURE_SERVICE_HANDLING;
           break;

       //!If the command received from processor is Drain Bath,
       //!perform the below operations
       case SITARA_DRAIN_BATH:
           //!Set the current mode to Drain Bath which will be given
           //!input to MBD to initiate Drain Bath
           g_usnCurrentMode = DRAIN_BATH_SERVICE_HANDLING;
           break;

       //!If the command received from processor is Drain All,
       //!perform the below operations
       case SITARA_DRAIN_ALL:
           //!Set the current mode to Drain All which will be given
           //!input to MBD to initiate Drain All
           g_usnCurrentMode = DRAIN_ALL_SERVICE_HANDLING;
           break;

       //!If the command received from processor is Clean Bath,
       //!perform the below operations
       case SITARA_CLEAN_BATH:
           //!Set the current mode to Clean Bath which will be given
           //!input to MBD to initiate Clean Bath
           g_usnCurrentMode = CLEAN_BATH_SERVICE_HANDLING;
           break;

       //!If the command received from processor is Valve Test to test specific
       //!valve indicated by processor, perform the below operations
       case SITARA_VALVE_TEST_CMD:
           //!Set the current mode to Valve Test and specific valve will be excited
           //!to check its operation
           SysInitValveTest((unsigned char)m_stReceivedPacketFrame.arrusnData[0]);
           break;

       //!If the command received from processor is Counting Time Sequence,
       //!perform the below operations
       case SITARA_COUNTING_TIME_SEQUENCE_START_CMD:
           //!Set the current mode to Counting Time Sequence which will be given
           //!input to MBD to initiate Counting Time Sequence
           g_usnCurrentMode = COUNTING_TIME_SEQUENCE;
           break;

       //!If the command received from processor is Wake Up Sequence,
       //!perform the below operations
       case SITARA_WAKE_UP_SEQUENCE_CMD:
           //!Set the current mode to Wake Up Sequence which will be given
           //!input to MBD to initiate Wake Up Sequence
           g_usnCurrentMode = MBD_WAKEUP_START;
           break;

       //!If the command received from processor is request for Home position
       //!status, perform the below operations
       case SITARA_HOME_POSITION_STATUS_CMD:
           //!Packetize the current Home position status of the motors and
           //!send to processor
           CPFrameDataPacket(DELFINO_SERVICE_HANDLING, \
                   DELFINO_HOME_POSITION_SENSOR_CHECK, FALSE);
           //!Current Home position status of the motors is requested only
           //!when the controller is not running any sequence, after sending
           //!pull the busy pin low
           SPIClearCommCtrlBusyInt();
           break;
       case SITARA_START_TOSHIP_SEQUENCE_CMD:
           //!Set the current mode to to ship which will be given
           //!input to MBD to initiate to ship sequence
           g_usnCurrentMode = MBD_TO_SHIP_SEQUENCE_START;
           break;

       case SITARA_VOLUMETRIC_CHECK_CMD:
           //!Set the current mode to volumetric board checkwhich will be given
           //!input to MBD to initiate to ship sequence
           g_usnCurrentMode = MBD_VOLUMETRIC_BOARD_CHECK;
           break;
       //default case
       default :
           break;

   }//end of switch case

}
/******************************************************************************/
/*!
 *  \fn     CIFormErrorPacket()
 *  \brief  To form and Error command to Sitara
 *  \param  uint16_t usnShutdownhandled - Current Mode
 *  \return void
 */
/******************************************************************************/
void CIFormErrorPacket(uint16_t usnMinorCmd)
{
    //sprintf(DebugPrintBuf, "\r\n Error Command: %x \n", usnMinorCmd);
    //UartDebugPrint((int *)DebugPrintBuf, 50);

    //!Frame shutdown sequence completed command and sent to Sitara
    if(usnMinorCmd != NULL)
    {
        CPFrameDataPacket(DELFINO_ERROR_HANDLING_CMD,usnMinorCmd, FALSE);
    }

}
/******************************************************************************/
/*!
 *  \fn     CIProcessSystemSettingsCmd()
 *  \brief  To process system setting command
 *  \param  unsigned char usnMinorCmd
 *  \return  void
 */
/******************************************************************************/
void CIProcessErrorCommand(uint16_t usnMinorCmd)
{
    //!Check for the minor command
        /*switch(usnMinorCmd)
        {
            //!If the minor command is to errors to Sitara
            case SITARA_ERROR_REQUEST:
                sprintf(DebugPrintBuf, "\n SITARA_ERROR_REQUEST \n");
                UartDebugPrint((int *)DebugPrintBuf, 50);
                CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);
                break;

            default:
                break;
        }*/
    if(SITARA_ERROR_REQUEST == usnMinorCmd)     //QA_C
    {
        sprintf(DebugPrintBuf, "\n SITARA_ERROR_REQUEST \n");
        UartDebugPrint((int *)DebugPrintBuf, 50);
        CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);
    }
    else
    {
        //do nothing!!!
    }

}



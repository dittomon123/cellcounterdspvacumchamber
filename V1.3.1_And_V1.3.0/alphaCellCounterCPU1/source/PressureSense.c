/******************************************************************************/
/*! \file PressureSense.c
 *
 *  \brief Pressure data processing
 *
 *  \b  Description:
 *      Pressure data captured from ADC shall be processed it here.
 *      Raw ADC data shall map it to pressure range based on given threshold
 *      The mapped pressure data shall input to motor control module
 *
 *   $Version: $ 0.1
 *   $Date:    $ Aug-19-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_pressure_sensor_module
//! @{
//*****************************************************************************

#include <AdcDriver.h>
#include "PressureSense.h"

/******************************************************************************/
/*!
 * \brief Preprocessor definitions
 * \def ADC_INPUT_VOLTS_FOR_MIN_PRESSURE
 * \def ADC_INPUT_VOLTS_FOR_MAX_PRESSURE
 * \def PRESSURE_MIN
 * \def PRESSURE_MAX
 * \def PRESSURE_DATA_BUF_SIZE
 * \def ADC_REFERENCE
 * \def ADC_COUNTS_SPAN
 */
/******************************************************************************/
    //!30 PSI
    #define ADC_INPUT_VOLTS_FOR_MIN_PRESSURE		3.3*.1 //0.5 for 15 psi sensor
    #define ADC_INPUT_VOLTS_FOR_MAX_PRESSURE		3.3*0.8//2.9 for 15 psi sensor

    #define PRESSURE_MIN							-30.0	//psi
    #define PRESSURE_MAX							30.0	//psi

    #define PRESSURE_DATA_BUF_SIZE					1

    #define ADC_REFERENCE							3
    #define ADC_COUNTS_SPAN							(4096)//4095 for 15 psi sensor

    //!15 PSI
//    #define ADC_INPUT_VOLTS_FOR_MIN_PRESSURE        0.5 //for 15 psi sensor
//    #define ADC_INPUT_VOLTS_FOR_MAX_PRESSURE        2.9 //for 15 psi sensor
//
//    #define PRESSURE_MIN                            -30.0   //psi
//    #define PRESSURE_MAX                            30.0    //psi
//
//    #define PRESSURE_DATA_BUF_SIZE                  1
//
//    #define ADC_REFERENCE                           3
//    #define ADC_COUNTS_SPAN                         4095 //for 15 psi sensor

/******************************************************************************/
/*!
 * \struct PressureParameter:
 * \brief PressureSense instance creation
 * 		  Object: PressureSense
 *
 */
/******************************************************************************/
PressureParameter PressureSense;

/******************************************************************************/
/*!
 * \fn PressureParameter* PSGetPressureSense(void)
 * \brief To get Structure address of PressureSense
 * \brief  Description:
 *      This function returns the address of PressureSense
 *
 * \return &PressureSense
 */
/******************************************************************************/
PressureParameter* PSGetPressureSense(void)
{
	return &PressureSense;
}

/******************************************************************************/
/*!
 * \fn void PSInitPressureModule(void)
 * \brief Initialize pressure related variables
 * \brief  Description:
 *      This function configures default values to variables
 *
 * \return void
 */
/******************************************************************************/
void PSInitPressureModule(void)
{
    PressureSense.ADCMin = (ADC_INPUT_VOLTS_FOR_MIN_PRESSURE*(ADC_COUNTS_SPAN/ADC_REFERENCE));	//	450.56
	PressureSense.ADCMax = ADC_INPUT_VOLTS_FOR_MAX_PRESSURE*(ADC_COUNTS_SPAN/ADC_REFERENCE);	//	3604.48
	PressureSense.ADCSpan = PressureSense.ADCMax - PressureSense.ADCMin;
	PressureSense.PressureValueMin = PRESSURE_MIN;	//psi
	PressureSense.PressureValueMax = PRESSURE_MAX;	//psi
	PressureSense.PressureSpan = PressureSense.PressureValueMax - \
	        PressureSense.PressureValueMin;
	PressureSense.PRESSURE_Err.PressureErrStatus = 0x00;
}

/******************************************************************************/
/*!
 * \fn unsigned int PSGetPressureData(unsigned int *Data, unsigned int DataBufSize)
 * \param *Data pressure raw data read from ADC
 * \param  DataBufSize  pressure raw data read from ADC buffer size to be fixed
 *
 * \brief To get pressure data from ADC interrupt
 * \brief  Description:
 *      This function get the runtime data and averages . The averaged data
 *      shall return with unsigned int type
 *
 * \return unsigned int
 */
/******************************************************************************/
unsigned int PSGetPressureData(unsigned int *Data, unsigned int DataBufSize)
{
	unsigned long AverageData=0;
	unsigned int i=0;
//	static unsigned long AvgData[]
	if(DataBufSize > 0)
	{
		for(i=0; i<DataBufSize; i++)
		{
			AverageData = AverageData + Data[(i*7)+1];
		}

		return (unsigned int)(AverageData/DataBufSize);
	}
	else
	{
		return 0;
	}

}
/******************************************************************************/
/*!
 * \fn float PSADCtoPressureMap(unsigned int ADCRawData)
 * \param ADCRawData pressure raw data read from ADC
 *
 * \brief ADC raw data to pressure conversion mapping function
 * \brief  Description:
 *      This function shall convert the ADC raw data to pressure in PSI
 *      For mapping, the thressolds and converstion factor shall be taken from
 *      defined definitions
 *
 * \return float
 */
/******************************************************************************/
float PSADCtoPressureMap(unsigned int ADCRawData)
{
	float PressureData=0.0;
//	unsigned int
//	char DebugPrintBuf[50];
	if(ADCRawData >= PressureSense.ADCMin && ADCRawData <= \
	        PressureSense.ADCMax && (PressureSense.ADCSpan > 0 ))
	{
		PressureData =(float)(((float)ADCRawData - PressureSense.ADCMin)  *(PressureSense.PressureSpan)/PressureSense.ADCSpan);
		PressureData = PressureData + PressureSense.PressureValueMin;
	}
	else
	{
		if(ADCRawData < PressureSense.ADCMin)
		{
			return PRESSURE_MIN;
		}
		else
		{
			return PRESSURE_MAX;
		}

	}
	return PressureData;
}
/******************************************************************************/
/*!
 * \fn float PSProcessPressureData(void)
 * \brief ADCD raw data to Pressure data
 * \brief  Description:
 *      This function calls the mapping functions to get the mapped data
 *      and return in float type
 *
 * \return float
 */
/******************************************************************************/
float PSProcessPressureData(void)
{
	unsigned int PressureRawData=0;
#ifdef ADC_INPUT_SIMULATION_PRESSURE
	static unsigned int Counter=0;
#endif
	unsigned int *PressureDataBuf;
	PressureDataBuf = ADGetAdcBufAddr();

	if(PressureSense.AcquireStatus == ACQUIRE_COMPLETE)
	{
		PressureRawData = PSGetPressureData(PressureDataBuf, PRESSURE_DATA_BUF_SIZE);
#ifdef ADC_INPUT_SIMULATION_PRESSURE
		if(Counter < 4096)
		{
			Counter++;
		}
		else
		{
			Counter = 0;
		}
		PressureRawData = Counter;
#endif
		PressureSense.PressureData = PSADCtoPressureMap(PressureRawData);
		ADAcquirePressureData(PRESSURE_DATA_BUF_SIZE);
	}
	return PressureSense.PressureData;
}
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

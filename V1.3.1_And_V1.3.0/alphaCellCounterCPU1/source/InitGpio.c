/******************************************************************************/
/*! \file InitGpio.c
 *
 *  \brief CPU GPIO initialization
 *
 *  \b Description:
 *      In this file GPIO initialization and wrapper function for LED operation
 *
 *
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-18-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_gpio_module
//! @{
//*****************************************************************************
#include "F28x_Project.h"
#include "InitGpio.h"
#include "MotorConfig.h"

Uint16 usnSensor1_HomeState;
/*
static GpioPinConfig GpioPinSetup;


******************************************************************************
!
 * \fn GpioPinConfig* getGpioPinSetup(void)
 * \brief To get Structure address of GpioPinSetup
 * \brief  Description:
 *      This function returns the address of GpioPinSetup
 *
 * \return &GpioPinSetup

******************************************************************************
GpioPinConfig* getGpioPinSetup(void)
{
	return &GpioPinSetup;
}

*/



/******************************************************************************/
/*!
 * \fn IGInitialize_Gpio(void)
 * \brief Configuration GPIO for customized & EVM
 * \b Description:
 *      This function configures GPIO
 *
 * \return void
 */
/******************************************************************************/
void IGInitialize_Gpio(void)
{

    //!To initialize the GPIO ports
	InitGpio();
	//!To initialize the GPIO ports related to LED
	IGGpioConfig_LED();
	//!To initialize the GPIO ports related to High voltage pins used to excite
	//!during data acquisition
	IGGpioConfig_HighVoltPin();
	//!To initialize the GPIO ports related to Cycle counter pins to increment
	//!counter when whole blood sequence is running - presently not used
	IGGpioConfig_CycleCounterPin();

	//!To initialize the GPIO ports related to PWM valve control
	IGGpioConfig_ValvePwm();
	//!To initialize the GPIO ports related to PWM of HgB
	IGGpioConfig_HgbPwm();

	//!To initialize the GPIO ports related to SPI Communication pins
	IGGpioConfig_SpiComm();
	//!To initialize the GPIO ports related to SPI communciation control busy pin
	IGGpioConfig_SpiCommControlPin();

	//!To initialize the GPIO ports related to SPI valve control pins
	IGGpioConfig_SpiValve();

	//!To initialize the GPIO ports related to Sample start button
	IGGpioConfig_SampleStartButton();

	//!To initialize the GPIO ports related to Waste full detect pin
	IGGpioConfig_WasteDetectPin();//SKM_WASTE_DETECT_CHANGE
#ifdef IO_EXPANDER_USED
	IGGpioConfig_SpiHomeSense();
#else
	//!To initialize the GPIO ports related to Home position detector pins
	IGInit_GPIO_HomePositionSensor();
#endif

	//!To initialize the GPIO ports related to UART communication pins
	IGGpioConfig_UartComm();

	//!To initialize the GPIOs used to default state
	IGGpioConfig_DefaultState();

}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_ValvePwm(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup for Valve PWM configuration
 * \b Description:
 *      This function configures gpio mux and resgistery for GPIO pin of valve pwm
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_ValvePwm(void)
{

	GpioPinConfig GpioSetupValvePwm;

	//!Set the following parameters of the PWM valve GPIO pin:
	//!GPIO pin no. to S_PWM_VALVE
	//!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
	//!GPIO Pin as output pin
	//!GPIO pin as GPIO_PUSHPULL
	//!GPIO peripheral as high
	GpioSetupValvePwm.GpioPin = S_PWM_VALVE;
	GpioSetupValvePwm.GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupValvePwm.GpioInputOutput = GPIO_OUTPUT;
	GpioSetupValvePwm.GpioFlags = GPIO_PUSHPULL;
	GpioSetupValvePwm.GpioPeripheral = S_PWM_VALVE_PERIPHERAL;

	//!After setting all the configurations of the pin,
	//!Update the configurations of the PWM valve GPIO
	IGConfigGpioPin(GpioSetupValvePwm);


}

/******************************************************************************/
/*!
 * \fn void IGInit_GPIO_HomePositionSensor(void)
 * \brief Configure GPIO's pinmux
 * \brief  Description:
 *      This function configures GPIO's direction & values in pinpux for
 *      HP sensors
 *
 * \return void
 */
/******************************************************************************/
void IGInit_GPIO_HomePositionSensor(void)
{
	GpioPinConfig GpioSetupHomePosGpio[5];

    //!Set the following parameters of the Home position 1 GPIO pin:
    //!GPIO pin no. to GPIO_HOME_POS1
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as low
	GpioSetupHomePosGpio[0].GpioPin = GPIO_HOME_POS1;
	GpioSetupHomePosGpio[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupHomePosGpio[0].GpioInputOutput = GPIO_INPUT;
	GpioSetupHomePosGpio[0].GpioFlags = GPIO_QUAL6;
	GpioSetupHomePosGpio[0].GpioPeripheral = 0;

    //!Set the following parameters of the Home position 2 GPIO pin:
    //!GPIO pin no. to GPIO_HOME_POS2
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as low
	GpioSetupHomePosGpio[1].GpioPin = GPIO_HOME_POS2;
	GpioSetupHomePosGpio[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupHomePosGpio[1].GpioInputOutput = GPIO_INPUT;
	GpioSetupHomePosGpio[1].GpioFlags = GPIO_QUAL6;
	GpioSetupHomePosGpio[1].GpioPeripheral = 0;

    //!Set the following parameters of the Home position 3 GPIO pin:
    //!GPIO pin no. to GPIO_HOME_POS3
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as low
	GpioSetupHomePosGpio[2].GpioPin = GPIO_HOME_POS3;
	GpioSetupHomePosGpio[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupHomePosGpio[2].GpioInputOutput = GPIO_INPUT;
	GpioSetupHomePosGpio[2].GpioFlags = GPIO_QUAL6;
	GpioSetupHomePosGpio[2].GpioPeripheral = 0;

    //!Set the following parameters of the Home position 4 GPIO pin:
    //!GPIO pin no. to GPIO_HOME_POS4
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as low
	GpioSetupHomePosGpio[3].GpioPin = GPIO_HOME_POS4;
	GpioSetupHomePosGpio[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupHomePosGpio[3].GpioInputOutput = GPIO_INPUT;
	GpioSetupHomePosGpio[3].GpioFlags = GPIO_QUAL6;
	GpioSetupHomePosGpio[3].GpioPeripheral = 0;

    //!Set the following parameters of the Home position 5 GPIO pin:
    //!GPIO pin no. to GPIO_HOME_POS5
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as low
	GpioSetupHomePosGpio[4].GpioPin = GPIO_HOME_POS5;
	GpioSetupHomePosGpio[4].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupHomePosGpio[4].GpioInputOutput = GPIO_INPUT;
	GpioSetupHomePosGpio[4].GpioFlags = GPIO_QUAL6;
	GpioSetupHomePosGpio[4].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Home position GPIO
	IGConfigGpioPin(GpioSetupHomePosGpio[0]);
	IGConfigGpioPin(GpioSetupHomePosGpio[1]);
	IGConfigGpioPin(GpioSetupHomePosGpio[2]);
	IGConfigGpioPin(GpioSetupHomePosGpio[3]);
	IGConfigGpioPin(GpioSetupHomePosGpio[4]);

	//! Home sensor 1 satus read. This check is used to set X motor in known
	//!position during startup
	usnSensor1_HomeState = OIHomeSensor1State();
	if(usnSensor1_HomeState == 0)
	{
		usnSensor1_HomeState = 2;
	}
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_HgbPwm(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup for Hgb PWM configuration
 * \b Description:
 *      This function configures gpio mux and resgistery for GPIO pin of Hgb pwm
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_HgbPwm(void)
{

	GpioPinConfig GpioSetupHgbPwm;

    //!Set the following parameters of the HGB PWM GPIO pin:
    //!GPIO pin no. to HB_PWM_HGB
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as high
	GpioSetupHgbPwm.GpioPin = HB_PWM_HGB;
	GpioSetupHgbPwm.GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupHgbPwm.GpioInputOutput = GPIO_OUTPUT;
	GpioSetupHgbPwm.GpioFlags = GPIO_PUSHPULL;
	GpioSetupHgbPwm.GpioPeripheral = HB_PWM_HGB_PERIPHERAL;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the HGB PWM GPIO
	IGConfigGpioPin(GpioSetupHgbPwm);


}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_UartComm(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup for
 *      UART configuration
 * \b Description:
 *      This function configures gpio mux and resgistery for GPIO pin of
 *      communication UART
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_UartComm(void)
{
#if 1
	GpioPinConfig GpioSetupDebugPortUartComm[2];

    //!Set the following parameters of the Debug port UART TX GPIO pin:
    //!GPIO pin no. to DEBUG_PORT_UART_TX
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as DEBUG_PORT_UART_PERIPHERAL
	GpioSetupDebugPortUartComm[0].GpioPin = DEBUG_PORT_UART_TX;
	GpioSetupDebugPortUartComm[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupDebugPortUartComm[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupDebugPortUartComm[0].GpioFlags = GPIO_ASYNC;
	GpioSetupDebugPortUartComm[0].GpioPeripheral = DEBUG_PORT_UART_PERIPHERAL;

    //!Set the following parameters of the Debug port UART RX GPIO pin:
    //!GPIO pin no. to DEBUG_PORT_UART_RX
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as DEBUG_PORT_UART_PERIPHERAL
	GpioSetupDebugPortUartComm[1].GpioPin = DEBUG_PORT_UART_RX;
	GpioSetupDebugPortUartComm[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupDebugPortUartComm[1].GpioInputOutput = GPIO_INPUT;
	GpioSetupDebugPortUartComm[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupDebugPortUartComm[1].GpioPeripheral = DEBUG_PORT_UART_PERIPHERAL;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the UART RX & TX  GPIO
	IGConfigGpioPin(GpioSetupDebugPortUartComm[1]);
	IGConfigGpioPin(GpioSetupDebugPortUartComm[0]);

#endif

}


/******************************************************************************/
/*!
 * \fn void IGGpioConfig_SpiValve(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup for
 *      SPI Valve configuration
 * \b Description:
 *      This function configures gpio mux and resgistery for
 *      GPIO pin of valve SPI
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_SpiValve(void)
{
#if 1
	GpioPinConfig GpioSetupSpiValve[5];

    //!Set the following parameters of the SPI Communciation SIMO GPIO pin:
    //!GPIO pin no. to SPI_SIMO_VALVE
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_VALVE
	GpioSetupSpiValve[0].GpioPin = SPI_SIMO_VALVE;
	GpioSetupSpiValve[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiValve[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiValve[0].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiValve[0].GpioPeripheral = SPI_PERIPHERAL_VALVE;

    //!Set the following parameters of the SPI Communciation SOMI GPIO pin:
    //!GPIO pin no. to SPI_SOMI_VALVE
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_VALVE
	GpioSetupSpiValve[1].GpioPin = SPI_SOMI_VALVE;
	GpioSetupSpiValve[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiValve[1].GpioInputOutput = GPIO_INPUT;
	GpioSetupSpiValve[1].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiValve[1].GpioPeripheral = SPI_PERIPHERAL_VALVE;

    //!Set the following parameters of the SPI Communciation CLK GPIO pin:
    //!GPIO pin no. to SPI_CLK_VALVE
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_VALVE
	GpioSetupSpiValve[2].GpioPin = SPI_CLK_VALVE;
	GpioSetupSpiValve[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiValve[2].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiValve[2].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiValve[2].GpioPeripheral = SPI_PERIPHERAL_VALVE;

    //!Set the following parameters of the SPI Communciation CS GPIO pin:
    //!GPIO pin no. to SPI_CS_VALVE
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_VALVE
	GpioSetupSpiValve[3].GpioPin = SPI_CS_VALVE;
	GpioSetupSpiValve[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiValve[3].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiValve[3].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiValve[3].GpioPeripheral = SPI_PERIPHERAL_VALVE;

    //!Set the following parameters of the SPI Communciation RESET GPIO pin:
    //!GPIO pin no. to SPI_RESET_VALVE
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as ZERO
	GpioSetupSpiValve[4].GpioPin = SPI_RESET_VALVE;
	GpioSetupSpiValve[4].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiValve[4].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiValve[4].GpioFlags = GPIO_PUSHPULL;
	GpioSetupSpiValve[4].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the SPI communcation GPIOs
	IGConfigGpioPin(GpioSetupSpiValve[0]);
	IGConfigGpioPin(GpioSetupSpiValve[1]);
	IGConfigGpioPin(GpioSetupSpiValve[2]);
	IGConfigGpioPin(GpioSetupSpiValve[3]);
	IGConfigGpioPin(GpioSetupSpiValve[4]);
#endif
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_SpiHomeSense(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup for
 *          SPI home sensor configuration
 * \b Description:
 *      This function configures gpio mux and resgistery for
 *      GPIO pin of valve SPI
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_SpiHomeSense(void)
{

	GpioPinConfig GpioSetupSpiHomeSense[4];

    //!Set the following parameters of the SPI Communciation for Home position
    //!sensor SIMO GPIO pin:
    //!GPIO pin no. to SPI_SIMO_HOME_SENSE
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_HOME_SENSE
	GpioSetupSpiHomeSense[0].GpioPin = SPI_SIMO_HOME_SENSE;
	GpioSetupSpiHomeSense[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiHomeSense[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiHomeSense[0].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiHomeSense[0].GpioPeripheral = SPI_PERIPHERAL_HOME_SENSE;

    //!Set the following parameters of the SPI Communciation for Home position
    //!sensor SOMI GPIO pin:
    //!GPIO pin no. to SPI_SOMI_HOME_SENSE
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_HOME_SENSE
	GpioSetupSpiHomeSense[1].GpioPin = SPI_SOMI_HOME_SENSE;
	GpioSetupSpiHomeSense[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiHomeSense[1].GpioInputOutput = GPIO_INPUT;
	GpioSetupSpiHomeSense[1].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiHomeSense[1].GpioPeripheral = SPI_PERIPHERAL_HOME_SENSE;

    //!Set the following parameters of the SPI Communciation for Home position
    //!sensor CLK GPIO pin:
    //!GPIO pin no. to SPI_SOMI_HOME_SENSE
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_HOME_SENSE
	GpioSetupSpiHomeSense[2].GpioPin = SPI_CLK_HOME_SENSE;
	GpioSetupSpiHomeSense[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiHomeSense[2].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiHomeSense[2].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiHomeSense[2].GpioPeripheral = SPI_PERIPHERAL_HOME_SENSE;

    //!Set the following parameters of the SPI Communciation for Home position
    //!sensor CS GPIO pin:
    //!GPIO pin no. to SPI_CS_HOME_SENSE
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as ZERO
	GpioSetupSpiHomeSense[3].GpioPin = SPI_CS_HOME_SENSE;
	GpioSetupSpiHomeSense[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiHomeSense[3].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiHomeSense[3].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiHomeSense[3].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Valve control SPI communcation GPIOs
	IGConfigGpioPin(GpioSetupSpiHomeSense[0]);
	IGConfigGpioPin(GpioSetupSpiHomeSense[1]);
	IGConfigGpioPin(GpioSetupSpiHomeSense[2]);
	IGConfigGpioPin(GpioSetupSpiHomeSense[3]);


}
/******************************************************************************/
/*!
 * \fn void IGConfigHomeSensCS( unsigned int  Pin,  unsigned int  State)
 * \param Pin	GPIO pin
 * \param State	GPIO state
 * \brief Wrapper function for LED operation
 * \b Description:
 *      This function toggles the LED for EVM
 *
 * \return void
 */
/******************************************************************************/
void IGConfigHomeSensCS( unsigned int  Pin,  unsigned int  State)
{
    //!Control the GPIO to high or low with respect to the pin indicated
	GPIO_WritePin(Pin, State);
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_SpiComm(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup for
 *        SPI configuration
 * \b Description:
 *      This function configures gpio mux and resgistery for
 *      GPIO pin of communication SPI
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_SpiComm(void)
{
#if 1
	GpioPinConfig GpioSetupSpiComm[4];

    //!Set the following parameters of the SPI Communciation SIMO GPIO pin:
    //!GPIO pin no. to SPI_SIMO_MP
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_MP
	GpioSetupSpiComm[0].GpioPin = SPI_SIMO_MP;
	GpioSetupSpiComm[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiComm[0].GpioInputOutput = GPIO_INPUT;
	GpioSetupSpiComm[0].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiComm[0].GpioPeripheral = SPI_PERIPHERAL_MP;

    //!Set the following parameters of the SPI Communciation SOMI GPIO pin:
    //!GPIO pin no. to SPI_SOMI_MP
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_MP
	GpioSetupSpiComm[1].GpioPin = SPI_SOMI_MP;
	GpioSetupSpiComm[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiComm[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiComm[1].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiComm[1].GpioPeripheral = SPI_PERIPHERAL_MP;

    //!Set the following parameters of the SPI Communciation CLK GPIO pin:
    //!GPIO pin no. to SPI_CLK_MP
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_MP
	GpioSetupSpiComm[2].GpioPin = SPI_CLK_MP;
	GpioSetupSpiComm[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiComm[2].GpioInputOutput = GPIO_INPUT;
	GpioSetupSpiComm[2].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiComm[2].GpioPeripheral = SPI_PERIPHERAL_MP;

    //!Set the following parameters of the SPI Communciation CS GPIO pin:
    //!GPIO pin no. to SPI_CS_MP
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_ASYNC
    //!GPIO peripheral as SPI_PERIPHERAL_MP
	GpioSetupSpiComm[3].GpioPin = SPI_CS_MP;
	GpioSetupSpiComm[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiComm[3].GpioInputOutput = GPIO_INPUT;
	GpioSetupSpiComm[3].GpioFlags = GPIO_ASYNC;
	GpioSetupSpiComm[3].GpioPeripheral = SPI_PERIPHERAL_MP;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the SPI communcation GPIOs
	IGConfigGpioPin(GpioSetupSpiComm[0]);
	IGConfigGpioPin(GpioSetupSpiComm[1]);
	IGConfigGpioPin(GpioSetupSpiComm[2]);
	IGConfigGpioPin(GpioSetupSpiComm[3]);
#endif


}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_SpiCommControlPin(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup for
 *          SPI commn control pins
 * \b Description:
 *      This function configures gpio mux and resgistery for
 *      GPIO pin for SPI commn
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_SpiCommControlPin(void)
{
#if 1
	GpioPinConfig GpioSetupSpiCommControlPin[4];

    //!Set the following parameters of the SPI Communciation Control GPIO pin:
    //!GPIO pin no. to SPI_COMM_CTRL_DELFINO_TXR_INT_REQ
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as GPIO_PERIPHERAL_MP
	GpioSetupSpiCommControlPin[0].GpioPin = SPI_COMM_CTRL_DELFINO_TXR_INT_REQ;
	GpioSetupSpiCommControlPin[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiCommControlPin[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiCommControlPin[0].GpioFlags = GPIO_PUSHPULL;
	GpioSetupSpiCommControlPin[0].GpioPeripheral = GPIO_PERIPHERAL_MP;

    //!Set the following parameters of the SPI Communciation Control GPIO pin:
    //!GPIO pin no. to SPI_COMM_CTRL_DELFINO_BUSY_STATUS
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as GPIO_PERIPHERAL_MP
	GpioSetupSpiCommControlPin[1].GpioPin = SPI_COMM_CTRL_DELFINO_BUSY_STATUS;
	GpioSetupSpiCommControlPin[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiCommControlPin[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiCommControlPin[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupSpiCommControlPin[1].GpioPeripheral = GPIO_PERIPHERAL_MP;

    //!Set the following parameters of the SPI Communciation Control GPIO pin:
    //!GPIO pin no. to SPI_COMM_CTRL_SITARA_BUSY_STATUS
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as GPIO_PERIPHERAL_MP
	GpioSetupSpiCommControlPin[2].GpioPin = SPI_COMM_CTRL_SITARA_BUSY_STATUS;
	GpioSetupSpiCommControlPin[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiCommControlPin[2].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupSpiCommControlPin[2].GpioFlags = GPIO_PUSHPULL;
	GpioSetupSpiCommControlPin[2].GpioPeripheral = GPIO_PERIPHERAL_MP;

    //!Set the following parameters of the SPI Communciation Control GPIO pin:
    //!GPIO pin no. to SPI_COMM_CTRL_SITARA_ABORT_REQ
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as GPIO_PERIPHERAL_MP
	GpioSetupSpiCommControlPin[3].GpioPin = SPI_COMM_CTRL_SITARA_ABORT_REQ;
	GpioSetupSpiCommControlPin[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSpiCommControlPin[3].GpioInputOutput = GPIO_INPUT;
	GpioSetupSpiCommControlPin[3].GpioFlags = GPIO_PUSHPULL;
	GpioSetupSpiCommControlPin[3].GpioPeripheral = GPIO_PERIPHERAL_MP;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the SPI communcation control GPIOs
	IGConfigGpioPin(GpioSetupSpiCommControlPin[0]);
	IGConfigGpioPin(GpioSetupSpiCommControlPin[1]);
	IGConfigGpioPin(GpioSetupSpiCommControlPin[2]);
	IGConfigGpioPin(GpioSetupSpiCommControlPin[3]);
#endif
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_DefaultState(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup
 * \b Description:
 *      This function configures gpio mux and resgistery for
 *      GPIO pin default state setup
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_DefaultState(void)
{
    //!Set LED 1 to high state
    IGConfigLED(LED1_GPIO, 1);//ON
//    IGConfigLED(LED2_GPIO, 0);//ON

    //MHN_TEST-inter changed HV & ZAP pins
    //!Set High voltage pin state to low
    IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_GPIO_OUTPUT, \
            HIGH_VOLTAGE_SWITCH_OFF);
    //!Set ZAP voltage pin state to low
    IGConfigHighVoltageModule(ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT, ZAP_VOLTAGE_OFF);

    //!Set cycle counter pin 1 to low
    IGWriteCycleCounterGpio(CYCLE_COUNTER_GPIO_OUTPUT1, 0);
    //!Set cycle counter pin 2 to low
    IGWriteCycleCounterGpio(CYCLE_COUNTER_GPIO_OUTPUT2, 0);

 //   IGWriteSpiCommControlPin(SPI_COMM_CTRL_DELFINO_TXR_INT_REQ, 0);
 //   IGWriteSpiCommControlPin(SPI_COMM_CTRL_DELFINO_BUSY_STATUS, 0);

    //!Set SPI Valve reset pin to high
    IGWriteSpiValveResetPin(SPI_RESET_VALVE, 1);
}



/******************************************************************************/
/*!
 * \fn void IGGpioConfig_LED(void)
 *
 *
 * \brief Wrapper function for LED GPIO pin setup
 * \b Description:
 *      This function configures gpio mux and resgistery for LED GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_LED(void)
{
	GpioPinConfig GpioSetupLED[2];

    //!Set the following parameters of the LED 1 GPIO pin:
    //!GPIO pin no. to LED1_GPIO
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupLED[0].GpioPin = LED1_GPIO;
	GpioSetupLED[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupLED[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupLED[0].GpioFlags = GPIO_PUSHPULL;
	GpioSetupLED[0].GpioPeripheral = 0;

    //!Set the following parameters of the LED 2 GPIO pin:
    //!GPIO pin no. to LED2_GPIO
    //!GPIO MUX select to GPIO_MUX_CPU2 to control from CPU2
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupLED[1].GpioPin = LED2_GPIO;
	GpioSetupLED[1].GpioCpuMuxSel = GPIO_MUX_CPU2;
	GpioSetupLED[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupLED[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupLED[1].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the LED control GPIOs
	IGConfigGpioPin(GpioSetupLED[0]);
	IGConfigGpioPin(GpioSetupLED[1]);

}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_MotorReset( void)
 *
 * \brief Wrapper function for Motor input gpio for reset
 * \b Description:
 *      This function writes the motor input gpio
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_MotorReset( void)
{
	GpioPinConfig GpioSetupMotorReset;

    //!Set the following parameters of the Reset GPIO pin:
    //!GPIO pin no. to M_RESET
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotorReset.GpioPin = M_RESET;
	GpioSetupMotorReset.GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotorReset.GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotorReset.GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotorReset.GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the reset control GPIO
	IGConfigGpioPin(GpioSetupMotorReset);
}




/******************************************************************************/
/*!
 * \fn void IGGpioConfig_Motor1(void)
 *
 *
 * \brief Wrapper function for Motor 1 GPIO pin setup
 * \b Description:
 *      This function configures gpio mux and resgistery for Motor 1 GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor1(void)
{
	GpioPinConfig GpioSetupMotor1[7];

    //!Set the following parameters of the Motor1 Mode 0 GPIO pin:
    //!GPIO pin no. to M1_MODE0
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor1[0].GpioPin = M1_MODE0;
	GpioSetupMotor1[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor1[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor1[0].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor1[0].GpioPeripheral = 0;

    //!Set the following parameters of the Motor1 Mode 1 GPIO pin:
    //!GPIO pin no. to M1_MODE1
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor1[1].GpioPin = M1_MODE1;
	GpioSetupMotor1[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor1[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor1[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor1[1].GpioPeripheral = 0;

    //!Set the following parameters of the Motor1 Mode 2 GPIO pin:
    //!GPIO pin no. to M1_MODE2
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor1[2].GpioPin = M1_MODE2;
	GpioSetupMotor1[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor1[2].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor1[2].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor1[2].GpioPeripheral = 0;

    //!Set the following parameters of the Motor1 Enable GPIO pin:
    //!GPIO pin no. to M1_IN1
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor1[3].GpioPin = M1_IN1;
	GpioSetupMotor1[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor1[3].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor1[3].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor1[3].GpioPeripheral = 0;

    //!Set the following parameters of the Motor1 Direction GPIO pin:
    //!GPIO pin no. to M1_IN2
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor1[4].GpioPin = M1_IN2;
	GpioSetupMotor1[4].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor1[4].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor1[4].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor1[4].GpioPeripheral = 0;

    //!Set the following parameters of the Motor1 Decay GPIO pin:
    //!GPIO pin no. to M1_DECAY
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor1[5].GpioPin = M1_DECAY;
	GpioSetupMotor1[5].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor1[5].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor1[5].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor1[5].GpioPeripheral = 0;

    //!Set the following parameters of the Motor1 Fault GPIO pin:
    //!GPIO pin no. to M1_FAULT
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor1[6].GpioPin = M1_FAULT;
	GpioSetupMotor1[6].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor1[6].GpioInputOutput = GPIO_INPUT;
	GpioSetupMotor1[6].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor1[6].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Motor1 control GPIOs
	IGConfigGpioPin(GpioSetupMotor1[0]);
	IGConfigGpioPin(GpioSetupMotor1[1]);
	IGConfigGpioPin(GpioSetupMotor1[2]);
	IGConfigGpioPin(GpioSetupMotor1[3]);
	IGConfigGpioPin(GpioSetupMotor1[4]);
	IGConfigGpioPin(GpioSetupMotor1[5]);
	IGConfigGpioPin(GpioSetupMotor1[6]);
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_Motor2(void)
 *
 *
 * \brief Wrapper function for Motor 2 GPIO pin setup
 * \b Description:
 *      This function configures gpio mux and resgistery for Motor 2 GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor2(void)
{
	GpioPinConfig GpioSetupMotor2[7];

    //!Set the following parameters of the Motor2 Mode 0 GPIO pin:
    //!GPIO pin no. to M2_MODE0
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor2[0].GpioPin = M2_MODE0;
	GpioSetupMotor2[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor2[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor2[0].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor2[0].GpioPeripheral = 0;

    //!Set the following parameters of the Motor2 Mode 1 GPIO pin:
    //!GPIO pin no. to M2_MODE1
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor2[1].GpioPin = M2_MODE1;
	GpioSetupMotor2[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor2[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor2[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor2[1].GpioPeripheral = 0;

    //!Set the following parameters of the Motor2 Mode 2 GPIO pin:
    //!GPIO pin no. to M2_MODE2
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor2[2].GpioPin = M2_MODE2;
	GpioSetupMotor2[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor2[2].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor2[2].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor2[2].GpioPeripheral = 0;

    //!Set the following parameters of the Motor2 Enable GPIO pin:
    //!GPIO pin no. to M2_ENBL
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor2[3].GpioPin = M2_ENBL;
	GpioSetupMotor2[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor2[3].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor2[3].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor2[3].GpioPeripheral = 0;

    //!Set the following parameters of the Motor2 Direction GPIO pin:
    //!GPIO pin no. to M2_DIR
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor2[4].GpioPin = M2_DIR;
	GpioSetupMotor2[4].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor2[4].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor2[4].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor2[4].GpioPeripheral = 0;

    //!Set the following parameters of the Motor2 Decay GPIO pin:
    //!GPIO pin no. to M2_DECAY
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor2[5].GpioPin = M2_DECAY;
	GpioSetupMotor2[5].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor2[5].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor2[5].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor2[5].GpioPeripheral = 0;

    //!Set the following parameters of the Motor2 Fault GPIO pin:
    //!GPIO pin no. to M2_FAULT
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor2[6].GpioPin = M2_FAULT;
	GpioSetupMotor2[6].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor2[6].GpioInputOutput = GPIO_INPUT;
	GpioSetupMotor2[6].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor2[6].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Motor2 control GPIOs
	IGConfigGpioPin(GpioSetupMotor2[0]);
	IGConfigGpioPin(GpioSetupMotor2[1]);
	IGConfigGpioPin(GpioSetupMotor2[2]);
	IGConfigGpioPin(GpioSetupMotor2[3]);
	IGConfigGpioPin(GpioSetupMotor2[4]);
	IGConfigGpioPin(GpioSetupMotor2[5]);
	IGConfigGpioPin(GpioSetupMotor2[6]);
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_Motor3(void)
 *
 *
 * \brief Wrapper function for Motor 3 GPIO pin setup
 * \b Description:
 *      This function configures gpio mux and resgistery for Motor 3 GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor3(void)
{
	GpioPinConfig GpioSetupMotor3[7];

    //!Set the following parameters of the Motor3 Mode 0 GPIO pin:
    //!GPIO pin no. to M3_MODE0
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor3[0].GpioPin = M3_MODE0;
	GpioSetupMotor3[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor3[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor3[0].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor3[0].GpioPeripheral = 0;

    //!Set the following parameters of the Motor3 Mode 1 GPIO pin:
    //!GPIO pin no. to M3_MODE1
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor3[1].GpioPin = M3_MODE1;
	GpioSetupMotor3[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor3[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor3[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor3[1].GpioPeripheral = 0;

    //!Set the following parameters of the Motor3 Mode 2 GPIO pin:
    //!GPIO pin no. to M3_MODE2
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor3[2].GpioPin = M3_MODE2;
	GpioSetupMotor3[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor3[2].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor3[2].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor3[2].GpioPeripheral = 0;

    //!Set the following parameters of the Motor3 Enable GPIO pin:
    //!GPIO pin no. to M3_ENBL
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor3[3].GpioPin = M3_ENBL;
	GpioSetupMotor3[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor3[3].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor3[3].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor3[3].GpioPeripheral = 0;

    //!Set the following parameters of the Motor3 Direction GPIO pin:
    //!GPIO pin no. to M3_DIR
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor3[4].GpioPin = M3_DIR;
	GpioSetupMotor3[4].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor3[4].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor3[4].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor3[4].GpioPeripheral = 0;

    //!Set the following parameters of the Motor3 Decay GPIO pin:
    //!GPIO pin no. to M3_DECAY
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor3[5].GpioPin = M3_DECAY;
	GpioSetupMotor3[5].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor3[5].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor3[5].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor3[5].GpioPeripheral = 0;

    //!Set the following parameters of the Motor3 Fault GPIO pin:
    //!GPIO pin no. to M3_FAULT
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor3[6].GpioPin = M3_FAULT;
	GpioSetupMotor3[6].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor3[6].GpioInputOutput = GPIO_INPUT;
	GpioSetupMotor3[6].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor3[6].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Motor3 control GPIOs
	IGConfigGpioPin(GpioSetupMotor3[0]);
	IGConfigGpioPin(GpioSetupMotor3[1]);
	IGConfigGpioPin(GpioSetupMotor3[2]);
	IGConfigGpioPin(GpioSetupMotor3[3]);
	IGConfigGpioPin(GpioSetupMotor3[4]);
	IGConfigGpioPin(GpioSetupMotor3[5]);
	IGConfigGpioPin(GpioSetupMotor3[6]);
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_Motor4(void)
 *
 *
 * \brief Wrapper function for Motor 4 GPIO pin setup
 * \b Description:
 *      This function configures gpio mux and resgistery for Motor 4 GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor4(void)
{
	GpioPinConfig GpioSetupMotor4[7];

    //!Set the following parameters of the Motor4 Mode 0 GPIO pin:
    //!GPIO pin no. to M4_MODE0
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor4[0].GpioPin = M4_MODE0;
	GpioSetupMotor4[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor4[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor4[0].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor4[0].GpioPeripheral = 0;

    //!Set the following parameters of the Motor4 Mode 1 GPIO pin:
    //!GPIO pin no. to M4_MODE1
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor4[1].GpioPin = M4_MODE1;
	GpioSetupMotor4[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor4[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor4[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor4[1].GpioPeripheral = 0;

    //!Set the following parameters of the Motor4 Mode 2 GPIO pin:
    //!GPIO pin no. to M4_MODE2
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor4[2].GpioPin = M4_MODE2;
	GpioSetupMotor4[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor4[2].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor4[2].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor4[2].GpioPeripheral = 0;

    //!Set the following parameters of the Motor4 Enable GPIO pin:
    //!GPIO pin no. to M4_ENBL
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor4[3].GpioPin = M4_ENBL;
	GpioSetupMotor4[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor4[3].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor4[3].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor4[3].GpioPeripheral = 0;

    //!Set the following parameters of the Motor4 Direction GPIO pin:
    //!GPIO pin no. to M4_DIR
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor4[4].GpioPin = M4_DIR;
	GpioSetupMotor4[4].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor4[4].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor4[4].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor4[4].GpioPeripheral = 0;

    //!Set the following parameters of the Motor4 Decay GPIO pin:
    //!GPIO pin no. to M4_DECAY
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor4[5].GpioPin = M4_DECAY;
	GpioSetupMotor4[5].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor4[5].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor4[5].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor4[5].GpioPeripheral = 0;

    //!Set the following parameters of the Motor4 Fault GPIO pin:
    //!GPIO pin no. to M4_FAULT
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor4[6].GpioPin = M4_FAULT;
	GpioSetupMotor4[6].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor4[6].GpioInputOutput = GPIO_INPUT;
	GpioSetupMotor4[6].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor4[6].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Motor4 control GPIOs
	IGConfigGpioPin(GpioSetupMotor4[0]);
	IGConfigGpioPin(GpioSetupMotor4[1]);
	IGConfigGpioPin(GpioSetupMotor4[2]);
	IGConfigGpioPin(GpioSetupMotor4[3]);
	IGConfigGpioPin(GpioSetupMotor4[4]);
	IGConfigGpioPin(GpioSetupMotor4[5]);
	IGConfigGpioPin(GpioSetupMotor4[6]);
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_Motor5(void)
 *
 *
 * \brief Wrapper function for Motor 5 GPIO pin setup
 * \b Description:
 *      This function configures gpio mux and resgistery for Motor 5 GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor5(void)
{
	GpioPinConfig GpioSetupMotor5[7];

    //!Set the following parameters of the Motor5 Mode 0 GPIO pin:
    //!GPIO pin no. to M5_MODE0
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor5[0].GpioPin = M5_MODE0;
	GpioSetupMotor5[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor5[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor5[0].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor5[0].GpioPeripheral = 0;

    //!Set the following parameters of the Motor5 Mode 1 GPIO pin:
    //!GPIO pin no. to M5_MODE1
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor5[1].GpioPin = M5_MODE1;
	GpioSetupMotor5[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor5[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor5[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor5[1].GpioPeripheral = 0;

    //!Set the following parameters of the Motor5 Mode 2 GPIO pin:
    //!GPIO pin no. to M5_MODE2
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor5[2].GpioPin = M5_MODE2;
	GpioSetupMotor5[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor5[2].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor5[2].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor5[2].GpioPeripheral = 0;

    //!Set the following parameters of the Motor5 Enable GPIO pin:
    //!GPIO pin no. to M5_ENBL
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor5[3].GpioPin = M5_ENBL;
	GpioSetupMotor5[3].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor5[3].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor5[3].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor5[3].GpioPeripheral = 0;

    //!Set the following parameters of the Motor5 Direction GPIO pin:
    //!GPIO pin no. to M5_DIR
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor5[4].GpioPin = M5_DIR;
	GpioSetupMotor5[4].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor5[4].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor5[4].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor5[4].GpioPeripheral = 0;

    //!Set the following parameters of the Motor5 Decay GPIO pin:
    //!GPIO pin no. to M5_DECAY
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor5[5].GpioPin = M5_DECAY;
	GpioSetupMotor5[5].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor5[5].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupMotor5[5].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor5[5].GpioPeripheral = 0;

    //!Set the following parameters of the Motor5 Fault GPIO pin:
    //!GPIO pin no. to M5_FAULT
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupMotor5[6].GpioPin = M5_FAULT;
	GpioSetupMotor5[6].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupMotor5[6].GpioInputOutput = GPIO_INPUT;
	GpioSetupMotor5[6].GpioFlags = GPIO_PUSHPULL;
	GpioSetupMotor5[6].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Motor5 control GPIOs
	IGConfigGpioPin(GpioSetupMotor5[0]);
	IGConfigGpioPin(GpioSetupMotor5[1]);
	IGConfigGpioPin(GpioSetupMotor5[2]);
	IGConfigGpioPin(GpioSetupMotor5[3]);
	IGConfigGpioPin(GpioSetupMotor5[4]);
	IGConfigGpioPin(GpioSetupMotor5[5]);
	IGConfigGpioPin(GpioSetupMotor5[6]);
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_HighVoltPin(void)
 *
 *
 * \brief Wrapper function for High Voltage GPIO pin setup
 * \b Description:
 *      This function configures gpio mux and resgistery for
 *      High Volatage GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_HighVoltPin(void)
{
	//GpioPinConfig GpioSetupHV[2];

    GpioPinConfig GpioSetupHV[3];

    //!Set the following parameters of the High Voltage GPIO pin:
    //!GPIO pin no. to HIGH_VOLTAGE_SELECT_GPIO_OUTPUT
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupHV[0].GpioPin = HIGH_VOLTAGE_SELECT_GPIO_OUTPUT;
	GpioSetupHV[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupHV[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupHV[0].GpioFlags = GPIO_PUSHPULL;
	GpioSetupHV[0].GpioPeripheral = 0;

    //!Set the following parameters of the ZAP voltage GPIO pin:
    //!GPIO pin no. to ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupHV[1].GpioPin = ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT;
	GpioSetupHV[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupHV[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupHV[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupHV[1].GpioPeripheral = 0;


    //!Set the following parameters of the High voltage RBC GPIO pin:
    //!GPIO pin no. to HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
    GpioSetupHV[2].GpioPin = HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT;
    GpioSetupHV[2].GpioCpuMuxSel = GPIO_MUX_CPU1;
    GpioSetupHV[2].GpioInputOutput = GPIO_OUTPUT;
    GpioSetupHV[2].GpioFlags = GPIO_PUSHPULL;
    GpioSetupHV[2].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the High voltage and ZAP voltage control GPIOs
	IGConfigGpioPin(GpioSetupHV[0]);
	IGConfigGpioPin(GpioSetupHV[1]);
    IGConfigGpioPin(GpioSetupHV[2]);

}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_CycleCounterPin(void)
 *
 *
 * \brief Wrapper function for Cycle counter input GPIO pin setup
 * \b Description:
 *      This function configures gpio mux and resgistery for
 *      Cycle counter GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_CycleCounterPin(void)
{
	GpioPinConfig GpioSetupCycleCounter[2];

    //!Set the following parameters of the Cycle counter GPIO pin 1:
    //!GPIO pin no. to CYCLE_COUNTER_GPIO_OUTPUT1
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupCycleCounter[0].GpioPin = CYCLE_COUNTER_GPIO_OUTPUT1;
	GpioSetupCycleCounter[0].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupCycleCounter[0].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupCycleCounter[0].GpioFlags = GPIO_PUSHPULL;
	GpioSetupCycleCounter[0].GpioPeripheral = 0;

    //!Set the following parameters of the Cycle counter GPIO pin 2:
    //!GPIO pin no. to CYCLE_COUNTER_GPIO_OUTPUT2
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as output pin
    //!GPIO pin as GPIO_PUSHPULL
    //!GPIO peripheral as ZERO
	GpioSetupCycleCounter[1].GpioPin = CYCLE_COUNTER_GPIO_OUTPUT2;
	GpioSetupCycleCounter[1].GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupCycleCounter[1].GpioInputOutput = GPIO_OUTPUT;
	GpioSetupCycleCounter[1].GpioFlags = GPIO_PUSHPULL;
	GpioSetupCycleCounter[1].GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of thecycle counter control GPIOs
	IGConfigGpioPin(GpioSetupCycleCounter[0]);
	IGConfigGpioPin(GpioSetupCycleCounter[1]);

}
/******************************************************************************/
/*!
 * \fn void IGConfigGpioPin(GpioPinConfig SetGpioPin)
 * \param SetGpioPin	GPIO oin configuration structure
 *
 * \brief Wrapper function for GPIO pin setup
 * \b Description:
 *      This function configures gpio mux and resgistery for GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void IGConfigGpioPin(GpioPinConfig SetGpioPin)
{
	EALLOW;
	GPIO_SetupPinMux(SetGpioPin.GpioPin, SetGpioPin.GpioCpuMuxSel,\
	        SetGpioPin.GpioPeripheral);
	GPIO_SetupPinOptions(SetGpioPin.GpioPin, SetGpioPin.GpioInputOutput,\
	        SetGpioPin.GpioFlags);
	EDIS;
}
/******************************************************************************/
/*!
 * \fn void IGBlinkLED( unsigned int  mSecLEDBlink)
 * \param mSecLEDBlink
 * \brief Wrapper function for LED operation
 * \b Description:
 *      This function toggles the LED for EVM
 *
 * \return void
 */
/******************************************************************************/
void IGBlinkLED( unsigned int  mSecLEDBlink)
{
	static unsigned int  stLEDToggle=1, stLEDToggleCounter=0;
	unsigned int  LoopExecutionTime = 20;//in mSec


	  if(stLEDToggleCounter >= mSecLEDBlink)
	  {
	      //!Reset the toggle counter to zero, if the toggle counter reaches the
	      //!the blink count value
		  stLEDToggleCounter = 0;
		  //!If the toggle value is set to one, then set the LED 1 GPIO status to high
		  if(stLEDToggle == 1 )
		  {
			  stLEDToggle = 0;
			  //GPIO_WritePin(LED1_GPIO, 1);
//			  GPIO_WritePin(LED2_GPIO, 0);

		  }
		  //!If the toggle value is set to one, then set the LED 1 GPIO status to low
		  else if(stLEDToggle == 0 )
		  {
			  stLEDToggle = 1;
			  //GPIO_WritePin(LED1_GPIO, 0);
//			  GPIO_WritePin(LED2_GPIO, 1);
		  }
	  }
	  else
	  {
	      //!Increment the toggle counter
		  stLEDToggleCounter = stLEDToggleCounter + LoopExecutionTime;
	  }

}
/******************************************************************************/
/*!
 * \fn void IGConfigLED( unsigned int  Pin,  unsigned int  State)
 * \param Pin	GPIO pin
 * \param State	GPIO state
 * \brief Wrapper function for LED operation
 * \b Description:
 *      This function toggles the LED for EVM
 *
 * \return void
 */
/******************************************************************************/
void IGConfigLED( unsigned int  Pin,  unsigned int  State)
{
    //!Control the GPIO to high or low with respect to the pin indicated
	GPIO_WritePin(Pin, State);
}
/******************************************************************************/
/*!
 * \fn int IGWriteMotorGpio( unsigned int  Pin,  unsigned int  State)
 * \param Pin	GPIO pin
 * \param State	GPIO state
 *
 * \brief Wrapper function for Motor input gpio
 * \b Description:
 *      This function writes the motor input gpio
 *
 * \return Err 0 Sucessfull -1 Failure
 */
/******************************************************************************/
int IGWriteMotorGpio( unsigned int  Pin,  unsigned int  State)
{
	int Err = -1;
	switch(Pin)
	{

	case M1_MODE0:
	case M1_MODE1:
	case M1_MODE2:
	case M1_IN1:
	case M1_IN2:
	case M1_DECAY:
	    //!If the input is M1_MODE0, M1_MODE1, M1_MODE2, M1_IN1, M1_IN2, M1_DECAY
	    //!Control the GPIO to high or low with respect to the pin indicated
		GPIO_WritePin(Pin, State);
		//!Reset the error state
		Err = 0;
	break;

	case M2_MODE0:
	case M2_MODE1:
	case M2_MODE2:
	case M2_ENBL:
	case M2_DIR:
	case M2_DECAY:
        //!If the input is M2_MODE0, M2_MODE1, M2_MODE2, M2_ENBL, M2_DIR, M2_DECAY
        //!Control the GPIO to high or low with respect to the pin indicated
		GPIO_WritePin(Pin, State);
		//!Reset the error state
		Err = 0;
	break;

	case M3_MODE0:
	case M3_MODE1:
	case M3_MODE2:
	case M3_ENBL:
	case M3_DIR:
	case M3_DECAY:
        //!If the input is M3_MODE0, M3_MODE1, M3_MODE2, M3_ENBL, M3_DIR, M3_DECAY
        //!Control the GPIO to high or low with respect to the pin indicated
		GPIO_WritePin(Pin, State);
		//!Reset the error state
		Err = 0;
	break;

	case M4_MODE0:
	case M4_MODE1:
	case M4_MODE2:
	case M4_ENBL:
	case M4_DIR:
	case M4_DECAY:
        //!If the input is M4_MODE0, M4_MODE1, M4_MODE2, M4_ENBL, M4_DIR, M4_DECAY
        //!Control the GPIO to high or low with respect to the pin indicated
		GPIO_WritePin(Pin, State);
		//!Reset the error state
		Err = 0;
	break;

	case M5_MODE0:
	case M5_MODE1:
	case M5_MODE2:
	case M5_ENBL:
	case M5_DIR:
	case M5_DECAY:
        //!If the input is M5_MODE0, M5_MODE1, M5_MODE2, M5_ENBL, M5_DIR, M5_DECAY
        //!Control the GPIO to high or low with respect to the pin indicated
		GPIO_WritePin(Pin, State);
		//!Reset the error state
		Err = 0;
	break;

	case M_RESET:
        //!If the input is M_RESET
        //!Control the GPIO to high or low with respect to the pin indicated
		GPIO_WritePin(Pin, State);
		//!Reset the error state
		Err = 0;
	break;

	default:
	break;

	}

	//!Return the error state
	return Err;
}
/******************************************************************************/
/*!
 * \fn Uint16 IGReadMotorGpio( unsigned int  Pin)
 * \param Pin	GPIO pin
 *
 *
 * \brief Wrapper function for Motor output gpio
 * \b Description:
 *      This function reads the motor output gpio
 *
 * \return Err 0 High State 1 Low State
 */
/******************************************************************************/
Uint16 IGReadMotorGpio( unsigned int  Pin)
{
	Uint16 Status = 0x01;
	switch(Pin)
	{

	case M1_FAULT:
        //!If the input is M1_FAULT
        //!Read the GPIO status of the pin indicated
		Status = GPIO_ReadPin(Pin);

	break;

	case M2_FAULT:
        //!If the input is M2_FAULT
        //!Read the GPIO status of the pin indicated
		Status = GPIO_ReadPin(Pin);

	break;

	case M3_FAULT:
        //!If the input is M3_FAULT
        //!Read the GPIO status of the pin indicated
		Status = GPIO_ReadPin(Pin);

	break;

	case M4_FAULT:
        //!If the input is M4_FAULT
        //!Read the GPIO status of the pin indicated
		Status = GPIO_ReadPin(Pin);

	break;

	case M5_FAULT:
        //!If the input is M5_FAULT
        //!Read the GPIO status of the pin indicated
		Status = GPIO_ReadPin(Pin);

	break;

	default:
	break;

	}
	//!Return the status of the GPIO pin status
	return Status;
}
/******************************************************************************/
/*!
 * \fn void IGConfigHighVoltageModule( unsigned int  Pin,  unsigned int  State)
 * \param Pin	GPIO pin
 * \param State	GPIO state
 * \brief Wrapper function for High Voltage operation
 * \b Description:
 *      This function configures the High voltage module GPIO
 *
 * \return void
 */
/******************************************************************************/
void IGConfigHighVoltageModule( unsigned int  Pin,  unsigned int  State)
{
    //!Control the GPIO to high or low with respect to the pin indicated
	GPIO_WritePin(Pin, State);
}
/******************************************************************************/
/*!
 * \fn void IGWriteCycleCounterGpio( unsigned int  Pin,  unsigned int  State)
 * \param Pin	GPIO pin
 * \param State	GPIO state
 * \brief Wrapper function for cycle counter gpio input
 * \b Description:
 *      This function configures the cycle counter GPIO
 *
 * \return void
 */
/******************************************************************************/
void IGWriteCycleCounterGpio( unsigned int  Pin,  unsigned int  State)
{
    //!Control the GPIO to high or low with respect to the pin indicated
	GPIO_WritePin(Pin, State);
}
/******************************************************************************/
/*!
 * \fn void IGWriteSpiCommControlPin( unsigned int  Pin,  unsigned int  State)
 * \param Pin	GPIO pin
 * \param State	GPIO state
 * \brief Wrapper function for spi communication control gpio
 * \b Description:
 *      This function configures the spi communication control GPIO
 *
 * \return void
 */
/******************************************************************************/
void IGWriteSpiCommControlPin( unsigned int  Pin,  unsigned int  State)
{
    //!Control the GPIO to high or low with respect to the pin indicated
	GPIO_WritePin(Pin, State);
}
/******************************************************************************/
/*!
 * \fn void IGWriteSpiValveResetPin( unsigned int  Pin,  unsigned int  State)
 * \param Pin	GPIO pin
 * \param State	GPIO state
 * \brief Wrapper function for spi valve reset gpio
 * \b Description:
 *      This function configures the spi valve reset GPIO
 *
 * \return void
 */
/******************************************************************************/
void IGWriteSpiValveResetPin( unsigned int  Pin,  unsigned int  State)
{
    //!Control the GPIO to high or low with respect to the pin indicated
	GPIO_WritePin(Pin, State);
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_SampleStartButton(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup for Sample Start Button
 * \b Description:
 *      This function configures gpio mux and resgistery for GPIO pin of Sample Start Button
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_SampleStartButton(void)
{

	GpioPinConfig GpioSetupSampleStartBtn;

    //!Set the following parameters of the Sample start GPIO pin:
    //!GPIO pin no. to SAMPLE_START_BUTTON_GPIO
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as ZERO
	GpioSetupSampleStartBtn.GpioPin = SAMPLE_START_BUTTON_GPIO;
	GpioSetupSampleStartBtn.GpioCpuMuxSel = GPIO_MUX_CPU1;
	GpioSetupSampleStartBtn.GpioInputOutput = GPIO_INPUT;
	GpioSetupSampleStartBtn.GpioFlags = GPIO_QUAL6;
	GpioSetupSampleStartBtn.GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Sample start control GPIO
	IGConfigGpioPin(GpioSetupSampleStartBtn);

}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_WasteDetectPin(void)
 *
 *
 * \brief Wrapper function for  GPIO pin default state setup for
 *          Waste Detect pin
 * \b Description:
 *      This function configures gpio mux and resgistery for
 *      GPIO pin of Waste Detect pin
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_WasteDetectPin(void)
{
    GpioPinConfig GpioSetupWasteDetectPin;

    //!Set the following parameters of the Waste bin detect GPIO pin:
    //!GPIO pin no. to WASTE_DETECT_GPIO
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as ZERO
    GpioSetupWasteDetectPin.GpioPin = WASTE_DETECT_GPIO;
    GpioSetupWasteDetectPin.GpioCpuMuxSel = GPIO_MUX_CPU1;
    GpioSetupWasteDetectPin.GpioInputOutput = GPIO_INPUT;
    GpioSetupWasteDetectPin.GpioFlags = GPIO_QUAL6;
    GpioSetupWasteDetectPin.GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the waste bin detect GPIO
    IGConfigGpioPin(GpioSetupWasteDetectPin);
}
/******************************************************************************/
/*!
 * \fn void IGGpioConfig_ReagentPrimingCheck(void)
 *
 *
 * \brief function to configure the GPIOs for detecting the Dilunet,
 *          Lyse and Rinse Priming
 * \b Description:
 *      This function configures gpio mux and resgistery to
 *      configure the GPIOs for detecting the Dilunet,
 *          Lyse and Rinse Priming
 *
 * \return void
 */
/******************************************************************************/
void IGGpioConfig_ReagentPrimingCheck(void)
{
    GpioPinConfig GpioSetupDiluentDetectPin;

    //!Set the following parameters of the Diluent line prime GPIO pin:
    //!GPIO pin no. to DILUENT_PRIME_CHECK
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as ZERO
    GpioSetupDiluentDetectPin.GpioPin = DILUENT_PRIME_CHECK;
    GpioSetupDiluentDetectPin.GpioCpuMuxSel = GPIO_MUX_CPU1;
    GpioSetupDiluentDetectPin.GpioInputOutput = GPIO_INPUT;
    GpioSetupDiluentDetectPin.GpioFlags = GPIO_QUAL6;
    GpioSetupDiluentDetectPin.GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Diluent prime detect GPIO
    IGConfigGpioPin(GpioSetupDiluentDetectPin);

    GpioPinConfig GpioSetupLyseDetectPin;

    //!Set the following parameters of the Lyse line prime GPIO pin:
    //!GPIO pin no. to LYSE_PRIME_CHECK
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as ZERO
    GpioSetupLyseDetectPin.GpioPin = LYSE_PRIME_CHECK;
    GpioSetupLyseDetectPin.GpioCpuMuxSel = GPIO_MUX_CPU1;
    GpioSetupLyseDetectPin.GpioInputOutput = GPIO_INPUT;
    GpioSetupLyseDetectPin.GpioFlags = GPIO_QUAL6;
    GpioSetupLyseDetectPin.GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Lyse prime detect GPIO
    IGConfigGpioPin(GpioSetupLyseDetectPin);

    GpioPinConfig GpioSetupRinseDetectPin;

    //!Set the following parameters of the Rinse line prime GPIO pin:
    //!GPIO pin no. to RINSE_PRIME_CHECK
    //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1
    //!GPIO Pin as input pin
    //!GPIO pin as GPIO_QUAL6
    //!GPIO peripheral as ZERO
    GpioSetupRinseDetectPin.GpioPin = RINSE_PRIME_CHECK;
    GpioSetupRinseDetectPin.GpioCpuMuxSel = GPIO_MUX_CPU1;
    GpioSetupRinseDetectPin.GpioInputOutput = GPIO_INPUT;
    GpioSetupRinseDetectPin.GpioFlags = GPIO_QUAL6;
    GpioSetupRinseDetectPin.GpioPeripheral = 0;

    //!After setting all the configurations of the pin,
    //!Update the configurations of the Rinse prime detect GPIO
    IGConfigGpioPin(GpioSetupRinseDetectPin);


 }
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

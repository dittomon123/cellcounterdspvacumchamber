/******************************************************************************/
/*! \file CommProtocol.c
 *  \brief Implementation of class SPI definition.
 *
 *  \b Description:
 *      This file contains the complete SPI Class defination
 *
 *   $Version: $ 1.0
 *   $Date:    $ 2015-11-06
 *   $Author:  $ Gurudutt R and Sudipta Kumar Sahoo
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Diagnostics Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics Pvt Ltd.
 *  The copyright notice does not evidence any actual or intended publication.
 */
/******************************************************************************/
/*******************************************************************************
Modified by					:	Samyuktha BS
Modified date				:	01-Apr-2016
Purpose	of Modification		:	Added major functionalties for decoding and
                                framing	the data packets
Modification   				:
Version						:	1.1
*******************************************************************************/


/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "CommProtocol.h"
#include "OpticalInterrupter.h"
#include "Structure.h"

/******************************************************************************/
/*!
 * \var commandsQueue
 * \brief  structure to store the packets to be transmitted in queue
 */
/******************************************************************************/
struct ST_QUEUE stCommandQueue;

/******************************************************************************/
/*!
 * \var pstQueue
 * \brief  structure pointer pointing to the queue
 */
/******************************************************************************/
stQueue* pstQueue = &stCommandQueue;

/******************************************************************************/
/*!
 * \brief DATA_SECTION();
 * \brief  map the RX & TX data of DMA to memory
 */
/******************************************************************************/
#pragma DATA_SECTION(stCommandQueue, ".RAMGS9_UserBuf");

/******************************************************************************/
/*!
 * \var m_stPrevTxPacket
 * \brief  structure to store the current packet which is transmitted to Processor
 */
/******************************************************************************/
ST_SPI_PACKET_FRAME m_stPrevTxPacket;

/******************************************************************************/
/*!
 * \var m_stPacketForTx
 * \brief  structure to store the packet which is to be updated to queue
 */
/******************************************************************************/
ST_SPI_PACKET_FRAME m_stPacketForTx;

/******************************************************************************/
/*!
 * \var m_stPacketForTxToSitara
 * \brief  structure to store the packet which is to be transmitted to Processor
 */
/******************************************************************************/
ST_SPI_PACKET_FRAME m_stPacketForTxToSitara;

/******************************************************************************/
/*!
 * \var m_stReceivedPacketFrame
 * \brief  structure to store the packet received from Processor
 */
/******************************************************************************/
ST_SPI_PACKET_FRAME m_stReceivedPacketFrame;

/******************************************************************************/
/*!
 * \var m_bSPIWriteControlStatus
 * \brief  variable to control whether the data to be written to SPI buffer to
 * transmit to the processor
 */
/******************************************************************************/
volatile bool_t m_bSPIWriteControlStatus = FALSE;

/******************************************************************************/
/*!
 * \var m_bTxInterruptStatus
 * \brief  variable to enable the transmit interrupt of the SPI to send data to
 * to the processor
 */
/******************************************************************************/
volatile bool_t m_bTxInterruptStatus = TRUE;

/******************************************************************************/
/*!
 * \var m_bMutiplePackets
 * \brief  variable to know whether multiple packets to be transmitted to processor
 */
/******************************************************************************/
bool_t m_bMutiplePackets = FALSE;

/******************************************************************************/
/*!
 * \var m_bLastMultiplePacket
 * \brief  variable to know whether it is the last paket during multiple
 * packet transmission
 */
/******************************************************************************/
bool_t m_bLastMultiplePacket = FALSE;

/******************************************************************************/
/*!
 * \var m_bTxToDMAFlag
 * \brief  variable to enable the transmit to DMA control of the SPI lines for
 * transmitting the data to processor
 * packet transmission
 */
/******************************************************************************/
bool_t m_bTxToDMAFlag = FALSE;

/******************************************************************************/
/*!
 * \var m_suslRBCNextIndex
 * \brief  RBC Pulse heights and RAW data is stored in SDRAM.
 * During packet transmission, next index from where the data to be obtained to
 * form the packet is obtained from this variable
 */
/******************************************************************************/
static uint32_t m_suslRBCNextIndex = 0;

/******************************************************************************/
/*!
 * \var m_suslWBCNextIndex
 * \brief  WBC Pulse heights and RAW data is stored in SDRAM.
 * During packet transmission, next index from where the data to be obtained to
 * form the packet is obtained from this variable
 */
/******************************************************************************/
static uint32_t m_suslWBCNextIndex = 0;

/******************************************************************************/
/*!
 * \var m_suslPLTNextIndex
 * \brief  WBC Pulse heights and RAW data is stored in SDRAM.
 * During packet transmission, next index from where the data to be obtained to
 * form the packet is obtained from this variable
 */
/******************************************************************************/
static uint32_t m_suslPLTNextIndex = 0;

extern volatile bool_t m_bNAckReceived;
extern volatile E_CELL_TYPE m_eTypeofCell;
extern volatile unsigned char m_ucValveNo;
extern volatile uint16_t m_usnPacketCount;
extern bool_t g_bNextSecRawDataReq;
extern bool_t g_bMotorChkStatus;	//SAM_MOTOR_CHK
extern char DebugPrintBuf[50];
extern Uint32 g_unSequenceStateTime;   //CRH_SEQUENCE_STATE
extern union UT_ERROR_INFO utErrorInfoCheck;
extern union UT_VOLUMETRIC_ERROR utVolumetricErrorInfo;
extern union UT_VOLUMETRIC_ERROR_RE_ACQ utVolumetricErrorInfoReAcq;
extern bool_t m_bTxPendingFlag;
extern bool_t g_bMonLysePriming;
extern bool_t g_bMonDiluentPriming;
extern bool_t g_bMonRinsePriming;
extern bool_t g_usnCurrentMode;
extern uint32_t sncalZeroPSI;
//extern uint16_t m_arrusnTimeStamp[100];
//extern uint16_t m_usnTimeStampLen;

/******************************************************************************/
/*!
 *  \fn    	void CPDecodeReceivedDataPacket()
 *  \brief  Decoding of the packet received from SPI Master
 *  \param  unsinged int* type address of the framePacket
 *  \return void
 */
/******************************************************************************/
void CPDecodeReceivedDataPacket(uint16_t *rxData)
{
    //!Set the controller to busy state, so that processor doesn't send the next
    //!command when the controller is busy
	SPISetCommCtrlBusyInt();

    /*!variable to maintain the count*/
    uint16_t usnCount = 0;
    /*!static variable for data decoding order*/
    //!The decoding order is set to startign of the frame
    uint16_t usnDataDecodingOrder = FRAME_HEADER_HIGH;
    /*! variable for data array index*/
    uint16_t usnDataArrayIndex = 0;
    /*! variable for SPI received data*/
    uint16_t m_usnDSPRxSPIData = 0;
    /*! variable to store the calculated CRC*/
    uint16_t m_usnCRCCalculated =0/*0xFFFF*/;

    /*! Seggrigate the Packet.
     *  The data Packets are as follows
     * --------------------------------------------
     * | 1. |Start Of Packet 1(Header1) (16 BITS) |
     * --------------------------------------------
     * | 2. |Start Of Packet 2(Header2) (16 BITS) |
     * --------------------------------------------
     * | 3. |Received 				    (16 BITS) |
     * --------------------------------------------
     * | 4. |Data Length  			    (16 BITS) |
     * --------------------------------------------
     * | 5. |Major Command			  	(16 BITS) |
     * --------------------------------------------
     * | 6. |Minor Command			   (16 BITS)  |
     * --------------------------------------------
     * | 7. |Data    				 (502 WORDS)  |
     * --------------------------------------------
     * | 8. |Total No Of Packets  	   (16 BITS)  |
     * --------------------------------------------
     * | 9. |CRC                  	   (16 BITS)  |
     * --------------------------------------------
     * | 10.|End Of Packet 1(Footer1)  (16 BITS)  |
     * --------------------------------------------
     * | 11.|End Of Packet 2(Footer2)  (16 BITS)  |
     * --------------------------------------------
     */

    //!The loop to decode the data that is received through SPI.
    //!the loop runs from starting till the complete packet (512 Word)
    for(usnCount = 0 ; usnCount < MAX_SPI_PACKET_LENGTH; usnCount++)
    {
        //!Get the data from the array to the variable to process
        m_usnDSPRxSPIData =  rxData[usnCount];

        //!Check for the decoding order
        switch(usnDataDecodingOrder)
        {
            //!If the decoding order is \def FRAME_HEADER_HIGH,
            //!update the structure: MSW of the start of packet with the
            //!received data and set the next decoding order to
            //!\def FRAME_HEADER_LOW
            case FRAME_HEADER_HIGH:
            {
                m_stReceivedPacketFrame.usnStartOfPacket_MSW = \
                        m_usnDSPRxSPIData;
                usnDataDecodingOrder = FRAME_HEADER_LOW;
            }
            break;
            //!If the decoding order is \def FRAME_HEADER_LOW,
            //!update the structure: LSW of the start of packet with the
            //!received data and set the next decoding order to
            //!\def FRAME_RESERVED_BITS
            case FRAME_HEADER_LOW:
            {
                m_stReceivedPacketFrame.usnStartOfPacket_LSW= \
                        m_usnDSPRxSPIData;
                usnDataDecodingOrder = FRAME_RESERVED_BITS;
            }
            break;

            //!If the decoding order is \def FRAME_RESERVED_BITS,
            //!update the structure: Reserved Word with the received data
            //!and set the next decoding order to \def FRAME_DATA_LENGTH
            case FRAME_RESERVED_BITS:
            {
                m_stReceivedPacketFrame.usnReservedWord = m_usnDSPRxSPIData;
                usnDataDecodingOrder = FRAME_DATA_LENGTH;
            }
            break;

            //!If the decoding order is \def FRAME_DATA_LENGTH,
            //!update the structure: Data length with the received data
            //!and set the next decoding order to \def FRAME_MAJOR_COMMAND
            case FRAME_DATA_LENGTH:
            {

                m_stReceivedPacketFrame.usnDataLength = m_usnDSPRxSPIData;

                usnDataDecodingOrder = FRAME_MAJOR_COMMAND;
            }
            break;

            //!If the decoding order is \def FRAME_MAJOR_COMMAND,
            //!update the structure: Major command with the received data
            //!and set the next decoding order to \def FRAME_MINOR_COMMAND
            case FRAME_MAJOR_COMMAND:
            {
                m_stReceivedPacketFrame.usnMajorCommand = m_usnDSPRxSPIData;
                usnDataDecodingOrder = FRAME_MINOR_COMMAND;
            }
            break;

            //!If the decoding order is \def FRAME_MINOR_COMMAND,
            //!update the structure: Minor Command with the received data
            //!and set the next decoding order to \def FRAME_DATA
            case FRAME_MINOR_COMMAND:
            {
                m_stReceivedPacketFrame.usnMinorCommand= m_usnDSPRxSPIData;
                usnDataDecodingOrder = FRAME_DATA;
            }
            break;

            //!If the decoding order is \def FRAME_DATA,
            //!update the structure: Data with the received data
            //!and set the next decoding order to \def FRAME_REMAINING_PACKETS_FOR_TX
            //!after completion of updating the data to the structure
            case FRAME_DATA:
            {

                if(( usnDataArrayIndex == \
                        m_stReceivedPacketFrame.usnDataLength ) || \
                        (m_stReceivedPacketFrame.usnDataLength == NULL))
                {
                    usnDataDecodingOrder = FRAME_REMAINING_PACKETS_FOR_TX;
                    usnCount = CRC_LOCATION_IN_PACKET - 3;
                    break;
                }
                m_stReceivedPacketFrame.arrusnData[usnDataArrayIndex] = \
                                        m_usnDSPRxSPIData;
                                usnDataArrayIndex++;

            }
            break;

            //!If the decoding order is \def FRAME_REMAINING_PACKETS_FOR_TX,
            //!update the structure: Remaining packet for TX with the received data
            //!and set the next decoding order to \def FRAME_PACKET_CRC
            case FRAME_REMAINING_PACKETS_FOR_TX:
            {
                m_stReceivedPacketFrame.usnRemainingPacketsforTX = \
                        m_usnDSPRxSPIData;

                usnDataDecodingOrder = FRAME_PACKET_CRC;
                // 508th location increments +1 =509
                //usnCount = CRC_LOCATION_IN_PACKET - 2;
            }
            break;

            //!If the decoding order is \def FRAME_PACKET_CRC,
            //!update the structure: Data length with the received data
            //!and set the next decoding order to \def FRAME_FOOTER_HIGH
            //!Calcualte the CRC for the data received
            case FRAME_PACKET_CRC:
            {
                m_stReceivedPacketFrame.usnCRC = m_usnDSPRxSPIData;

                m_usnCRCCalculated = \
                        CPCalculatePacketCRC(m_stReceivedPacketFrame);
                usnDataDecodingOrder = FRAME_FOOTER_HIGH;
            }
            break;

            //!If the decoding order is \def FRAME_FOOTER_HIGH,
            //!update the structure: MSW of End Of Packet with the received data
            //!and set the next decoding order to \def FRAME_DATA_LENGTH
            case FRAME_FOOTER_HIGH:
            {
                m_stReceivedPacketFrame.usnEndOfPacket_MSW = \
                        m_usnDSPRxSPIData;
                usnDataDecodingOrder = FRAME_FOOTER_LOW;
            }
            break;

            //!If the decoding order is \def FRAME_FOOTER_LOW,
            //!update the structure: LSW of End Of Packet with the received data
            //!and set the next decoding order to \def FRAME_HEADER_LOW
            case FRAME_FOOTER_LOW:
            {
                m_stReceivedPacketFrame.usnEndOfPacket_LSW = \
                        m_usnDSPRxSPIData;
                usnDataDecodingOrder = FRAME_HEADER_LOW;
            }
            break;

            //!If the decoding order doesn't match to any of the above scenario,
            //!do nothing
            default:
            break;

        }//end of switch case
    }//end of for loop

    //!Compare the calculated CRC with the received CRC
	//!Packet is valid if calculated checksum is equal to the checksum obtained
	if(m_usnCRCCalculated == m_stReceivedPacketFrame.usnCRC)
	{
		CIProcessCommandAttribute(m_stReceivedPacketFrame.usnMajorCommand,\
		        m_stReceivedPacketFrame.usnMinorCommand);

		//!Enable reagent monitoring based the current mode selected
		if((FALSE == g_bMonLysePriming) || (FALSE == g_bMonDiluentPriming) \
		        || (FALSE == g_bMonRinsePriming))
		{
            switch(g_usnCurrentMode)
            {
                case WHOLE_BLOOD_MODE:
                case PRE_DILUENT_MODE:
                case DISPENSE_THE_DILUENT:
                case START_PRE_DILUENT_COUNTING_CMD:
                case BODY_FLUID_MODE:
                case PARTIAL_BLOOD_COUNT_WBC:
                case PARTIAL_BLOOD_COUNT_RBC_PLT:
                case AUTO_CALIBRATION_WB:
                case COMMERCIAL_CALIBRATION_WBC:
                case COMMERCIAL_CALIBRATION_RBC:
                case COMMERCIAL_CALIBRATION_PLT:
                case QUALITY_CONTROL_WB:
                case QUALITY_CONTROL_BODY_FLUID:
                case PRIME_WITH_EZ_CLEANSER_CMD:
                case BACK_FLUSH_SERVICE_HANDLING:
                case ZAP_APERTURE_SERVICE_HANDLING:
                case DRAIN_BATH_SERVICE_HANDLING:
                case CLEAN_BATH_SERVICE_HANDLING:
                case SHUTDOWN_SEQUENCE:

                    //!Enable the Lyse Prime monitoring
                    g_bMonLysePriming = TRUE;

                    //!Enable the Diluent Prime monitoring
                    g_bMonDiluentPriming = TRUE;

                    //!Enable the Rinse Prime monitoring
                    g_bMonRinsePriming = TRUE;
                    break;

                case PRIME_ALL_SERVICE_HANDLING:
                case PRIME_WITH_DILUENT_SERVICE_HANDLING:
                case PRIME_WITH_LYSE_SERVICE_HANDLING:
                case PRIME_WITH_RINSE_SERVICE_HANDLING:
                case DRAIN_ALL_SERVICE_HANDLING:
                case SITARA_ABORT_MAIN_PROCESS_CMD:
                    //!Do not monitor the reagent priming
                    break;

                default:
                    break;
            }
		}
	}
	else
	{
		/*!SEND NACK TO SITARA*/ // TO BE IMPLEMENTED
		CIFormACKPacket(SET);

        //!Since it is only processing command,
        //!and no further sequence is initiated, clear the busy pin
        SPIClearCommCtrlBusyInt();

	}

}
/******************************************************************************/
/*!
 *  \fn     CPSetStartOfPacket()
 *  \brief  The START_OF_PACKET is written to the Packet.
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return void
 */
/******************************************************************************/
void CPSetStartOfPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket)
{
    //!Update the structure: MSW of Start of Packet with
    //!\def START_OF_PACKET_MSW
    stPtrParsedPacket->usnStartOfPacket_MSW = START_OF_PACKET_MSW;
    //!Update the structure: LSW of Start of Packet with
    //!\def START_OF_PACKET_LSW
    stPtrParsedPacket->usnStartOfPacket_LSW = START_OF_PACKET_LSW;
}
/******************************************************************************/
/*!
 *  \fn     CPSetEndOfPacket()
 *  \brief  The END_OF_PACKET is written to the SPI Packet Frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return void
 */
/******************************************************************************/
void CPSetEndOfPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket)
{
    //!Update the structure: MSW of End of Packet with
    //!\def END_OF_PACKET_MSW
    stPtrParsedPacket->usnEndOfPacket_MSW = END_OF_PACKET_MSW;
    //!Update the structure: LSW of End of Packet with
    //!\def END_OF_PACKET_LSW
    stPtrParsedPacket->usnEndOfPacket_LSW = END_OF_PACKET_LSW;
}
/******************************************************************************/
/*!
 *  \fn     CPSetPacketDataLength()
 *  \brief 	The packet data length is written to the Packet.
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return void
 */
/******************************************************************************/
void CPSetPacketDataLength(ST_SPI_PACKET_FRAME *stPtrParsedPacket, \
        Uint16 dataLength)
{
    //!Update the structure: Data length with the data length of the packet to
    //!be transmitted
    stPtrParsedPacket->usnDataLength = dataLength;
}
/******************************************************************************/
/*!
 *  \fn     CPWritePHDataToPacket()
 *  \brief  Writes the Patient handling data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWritePHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived)
{
    //!Set data length as zero by default
    uint16_t usnDataLength = 0;
    //!Set minor/sub command as zero by default
	uint16_t usnSubCmd =0;

	//!Get the minor command from the structure to update the data
    usnSubCmd = stPtrParsedPacket->usnMinorCommand;

    //!Check for the minor/sub command
    switch(usnSubCmd)
    {
        //!If the minor command is WBC, RBC, PLT, HGB result,
        //!Perform the below operation:
		case DELFINO_WBC_RBC_PLT_HGB_COUNT:
		{
		    //!Frame the Structure: data with the final count results and get
		    //!the length of the packet
		    usnDataLength = CPGetCountData(stPtrParsedPacket);
		}
		break;

		//!If the minor command is WBC Pulse height, RBC Pulse Height,
		//!PLT pulse height, Perform the below operation:
		case DELFINO_WBC_PULSE_HEIGHT_DATA:
		case DELFINO_RBC_PULSE_HEIGHT_DATA:
		case DELFINO_PLT_PULSE_HEIGHT_DATA:
		{
		    //!If NAK is not received for the transmitted data, frame the
		    //!pulse height data for the corresponding cell and transmit to the
		    //!processor, else frame the faulty packets and send it to the
		    //!processor
			if(bNAKReceived == FALSE)
			{
				usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\
				        usnSubCmd, FALSE);
			}
			else
			{
				usnDataLength = \
				        CPGetHeightDataForReTransmission(stPtrParsedPacket,\
				                usnSubCmd);
			}
		}
		break;

        //!If the minor command is WBC Raw Data, RBC Raw Data, PLT Raw Data
        //!Perform the below operation:
		case DELFINO_WBC_ADC_RAW_DATA:
		case DELFINO_RBC_ADC_RAW_DATA:
		case DELFINO_PLT_ADC_RAW_DATA:
		{
            //!If NAK is not received for the transmitted data, frame the
            //!Raw Data for the corresponding cell and transmit to the
            //!processor, else frame the faulty packets and send it to the
            //!processor
			if(bNAKReceived == FALSE)
			{
				usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\
				        usnSubCmd, TRUE);
			}
			else
			{
				usnDataLength = CPReTransmistADCRawData(stPtrParsedPacket,\
				        usnSubCmd);
			}
		}
		break;


		case DELFINO_ERROR_MINOR_CMD:
		    usnDataLength = CPGetErrorData(stPtrParsedPacket);
		    m_usnPacketCount =0;
		    break;

		case DELFINO_SEQUENCE_COMPLETION:
            //sprintf(DebugPrintBuf, "\r\n m_usnTimeStampLen: %d\n", m_usnTimeStampLen);
            //UartDebugPrint((int *)DebugPrintBuf, 40);
		    //memcpy_fast_far(&stPtrParsedPacket->arrusnData[0],&m_arrusnTimeStamp[0],m_usnTimeStampLen);
		    //usnDataLength = m_usnTimeStampLen;
		    //m_usnTimeStampLen = 0;
		    //memset(m_arrusnTimeStamp, 0, sizeof(m_arrusnTimeStamp));
            usnDataLength = 0;
		    m_usnPacketCount =0;
		    break;

		//!For any other patient handling commands (Aspiration completed, Ready
		//!for dispensing, dispensing completed, ready for aspiration,
		//!data length is zero and packet count is zero
		case DELFINO_ASPIRATION_COMPLETED:
		case DELFINO_NEEDLE_READY_FOR_DISPENSING_CMD:
		case DELFINO_DISPENSE_COMPLETED_CMD:
		case DELFINO_READY_FOR_ASPIRATION:
		case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:

		default:
		{
			usnDataLength = 0;
			m_usnPacketCount =0;
		}
		break;
    }//end of switch case
    //!Return the data length which shall be updated in the structure of the
    //!data to be transmitted
    return usnDataLength;
}
/******************************************************************************/
/*!
 *  \fn     CPSetCommandsForPacket()
 *  \brief  Writes the  Commands to the Packet Frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \param  usnMajorCmd Major command
 *  \param  usnSubCmd Sub command
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
void CPSetCommandsForPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        Uint16 usnMajorCmd, Uint16 usnSubCmd)
{
    //!Update the structure: major command with the major command to be transmitted
	stPtrParsedPacket ->usnMajorCommand = usnMajorCmd;
	//!Update the structure: minor command with the minor command to be transmitted
    stPtrParsedPacket ->usnMinorCommand = usnSubCmd;
}
/******************************************************************************/
/*!
 *  \fn     CPWriteDataToSPI()
 *  \brief  Writes the data to SPI File Descriptor
 *
 *  \return void
 */
/******************************************************************************/
bool_t CPWriteDataToSPI()
{
	bool_t bStatus = FALSE;

	/*! Prepare the Packet to send to DSP Controller */

	//!Check for the control status, whether any packet to be transmitted to processor
	if(m_bSPIWriteControlStatus == TRUE)
	{
	    //!Copy the updated packet framed structure the memory to transmit the
	    //!data to the processor
		uint16_t* pTxData = (uint16_t*)&m_stPacketForTx;
		//!Write the data to DMA
		bStatus = SPIDmaWrite(pTxData);
		//!If the data is written successfully, the data is transmitted to processor
		//!set the \var m_bSPIWriteControlStatus to false
		if(bStatus == TRUE)
		{
		    //!Successfully command sent
			m_bSPIWriteControlStatus = FALSE;
		}
		else
		{
			//DO Nothing
		}
		return bStatus;
	}
  return FALSE;

}
/******************************************************************************/
/*!
 *  \fn     void CPPAbort(char *errString)
 *  \brief  Returns the Error String and aborts the process
 *
 *  \return void
 */
/******************************************************************************/
void CPPAbort(char *errString)
{
	perror(errString);
    abort();
}
/******************************************************************************/
/*!
 *  \fn     CPCalculatePacketCRC()
 *  \brief  calculates the CRC of the updated data and returns it.
 *
 *  \param  ST_SPI_PACKET_FRAME  stPtrParsedPacket receives the value
 *          of the parsedPacketFrame
 *  \return Returns the calculated CRC.
 */
/******************************************************************************/
uint16_t CPCalculatePacketCRC(ST_SPI_PACKET_FRAME stPacketforCRC)
{
    //!* Initialize the "CalCheckSum" for first time to 0XFFFF
    uint16_t usnCalCheckSum=0xFFFF;

    //!Calculate CRC for reserved Word and update to \var usnCalCheckSum
    CPUpdateCRC(&stPacketforCRC.usnReservedWord, 1, &usnCalCheckSum);

    //!Calculate The CRC for DataLength and update to \var usnCalCheckSum
    CPUpdateCRC(&stPacketforCRC.usnDataLength, 1, &usnCalCheckSum);

	//!Calculate CRC for Major Command and update to \var usnCalCheckSum
    CPUpdateCRC(&stPacketforCRC.usnMajorCommand, 1, &usnCalCheckSum);

	//!Calculate CRC for Minor Command and update to \var usnCalCheckSum
    CPUpdateCRC(&stPacketforCRC.usnMinorCommand, 1, &usnCalCheckSum);

    //!Calculate the CRC for Data and update to \var usnCalCheckSum
    CPUpdateCRC(stPacketforCRC.arrusnData, stPacketforCRC.usnDataLength,\
            &usnCalCheckSum);

    //!Calculate the CRC for remaining packet for transmission and update to
    //!\var usnCalCheckSum
    CPUpdateCRC(&stPacketforCRC.usnRemainingPacketsforTX,1,&usnCalCheckSum);

    //!Return the calculated CRC to update it to the structure to the packet to
    //!be transmitted to processor
    return usnCalCheckSum;
}
/******************************************************************************/
/*!
 *  \fn     CPUpdateCRC()
 *  \brief  calculates the CRC of the Individual Packet field.
 *  \param  Address of the Value for which CRC to be calculated.
 *  \param  No of Bytes in the dta field.
 *  \param  Calculated CRC that has to be updated after each CRC Calculation.
 */
/******************************************************************************/
void CPUpdateCRC(uint16_t *nData, uint16_t nByteCnt, uint16_t* usnCalculatedCRC)
{
    //!Set the index to Zero by default
    uint16_t usnIndex = 0;
    uint16_t usnValRead;

    //!CRC is calculated for the data that is sent
    while(usnIndex < nByteCnt)
    {
        usnValRead = *(nData + usnIndex);
        *usnCalculatedCRC = *usnCalculatedCRC ^ usnValRead;
        usnIndex++;
    }
}
/******************************************************************************/
/*!
 *  \fn     CPFrameDataPacket()
 *  \brief  Forms and Sends the packet to Sitara.
 *
 *  \param  Major Command.
 *  \param  Sub Command.
 *  \param  No of Bytes in the data field.
 */
/******************************************************************************/
void CPFrameDataPacket(Uint16 usnMajorCmd, Uint16 usnSubCmd,\
        bool_t bRemainingPackets)
{
    //!Initialize \var ndataSize by default to Zero
    uint16_t ndataSize = 0;
    //!Initialize \var usnCalculatedChkSum by default to Zero
	uint16_t usnCalculatedChkSum = 0xFFFF;

	//!Check if there is no packet presently to be transmitted to processor.
	//!If no packet to be transmitted, then the data shall be updated to the
	//!structure to be transmitted to the processor
	if((m_bSPIWriteControlStatus == FALSE ) && (FALSE == m_bTxPendingFlag))
	{
	    //!Set major and minor command to the packet attribute
		CPSetPacketAttributesForTx(&m_stPacketForTx, usnMajorCmd, usnSubCmd);

		//!Check for the major command to write the data to the packet
		switch(usnMajorCmd)
		{
		    //!If the major command if patient handling, write patient handling
		    //!data packet to the structure
			case DELFINO_PATIENT_HANDLING:
			   	ndataSize = CPWritePHDataToPacket(&m_stPacketForTx,\
			   	        m_bNAckReceived);
			break;
            //!If the major command if Firmware Version, write Firmware Version
            //!data packet to the structure
			case DELFINO_FIRMWARE_VERSION_CMD:
				ndataSize = CPWriteFirmwareVerToPacket(&m_stPacketForTx,\
				        m_bNAckReceived);
			break;
            //!If the major command if Service Handling, write Service Handling
            //!data packet to the structure
			case DELFINO_SERVICE_HANDLING:
				ndataSize = CPWriteSHDataToPacket(&m_stPacketForTx,\
				        m_bNAckReceived);
			break;

            //!If the major command if Calibration handling, write Calibration
            //!handling data packet to the structure
			case DELFINO_CALIBRATION_HANDLING:
				ndataSize = CPWriteCHDataToPacket(&m_stPacketForTx,\
				        m_bNAckReceived);
			break;

            //!If the major command if Quality handling, write Quality
            //!handling data packet to the structure
			case DELFINO_QUALITY_HANDLING:
				ndataSize = CPWriteQHDataToPacket(&m_stPacketForTx,\
				        m_bNAckReceived);
			break;

            //!If the major command if Monitoirng Sequence, write Monitoring
            //!Sequence data packet to the structure
			case DELFINO_MONITOR_SEQUENCE:
			    ndataSize = CPWriteSeqMonitoringDataToPacket(&m_stPacketForTx,\
			                            m_bNAckReceived);
			    break;

            //!If the major command if Shutdown Sequence, set the data length
            //!and packet size to Zero
			case DELFINO_SHUTDOWN_SEQUENCE_CMD:
			    ndataSize =0;
			    m_usnPacketCount = 0;
			    break;

            //!If the major command if Abort the current process, write the
            //!data packet to the structure
			case DELFINO_ABORT_PROCESS_MAIN_CMD:
			    ndataSize = CPWriteAbortProcessDataToPacket(&m_stPacketForTx,\
			                                            m_bNAckReceived);
			    m_usnPacketCount = 0;
			    break;

            //!If the major command if system setting command, write the
            //!data packet to the structure
            case DELFINO_SYSTEM_SETTING_CMD:
                ndataSize = CPWriteSystemSettingDataToPacket(&m_stPacketForTx,\
                                                        m_bNAckReceived);
                m_usnPacketCount = 0;
                break;

            //!If the major command if Startup command, write the
            //!data packet to the structure
            case DELFINO_STARTUP_HANDLING_CMD:
                ndataSize = CPWriteSTPHDataToPacket(&m_stPacketForTx,\
                                                        m_bNAckReceived);
                m_usnPacketCount = 0;
                break;

            //!If the major command if error command, write the
            //!data packet to the structure
            case DELFINO_ERROR_HANDLING_CMD:
                ndataSize = CPGetErrorData(&m_stPacketForTx);
                m_usnPacketCount = 0;
                break;
			//!If the major command does not match to any of the above mentioned
			//!scenario, set the data size and packet count to Zero
			default:
				ndataSize =0;
				m_usnPacketCount = 0;
			break;
		}//end of switch case

		//!Fill the Data Length Attribute to the structure
		CPSetPacketDataLength(&m_stPacketForTx, ndataSize);

		//!Fill the total no of packets to be sent to Sitara
		CPSetTotalNoOfPackets(&m_stPacketForTx);

		//!Calculate the CRC to the Data frame
		usnCalculatedChkSum = CPCalculatePacketCRC(m_stPacketForTx);

		//!Add the Calculated CRC to the packet
		CPSetPacketCRC(&m_stPacketForTx, usnCalculatedChkSum);

		//!Set the SPI Control status to true indicating data to be transmitted
		//!to the processor
		m_bSPIWriteControlStatus = TRUE;

        //!Set the transmit DMA flag to true to write data to DMA
		m_bTxToDMAFlag = TRUE;

		//!If the there are no remaining packets to be transmitted to processor
		//!in case of multiple packet transmission, set the \var m_bTxInterruptStatus
		//!to true, else set it to false
		if(bRemainingPackets == FALSE)
		{
			m_bTxInterruptStatus = TRUE;
		}
		else
		{
			m_bTxInterruptStatus = FALSE;
		}
		/*!Copy the contents to the previous Parsed Packet*/
		memcpy(&m_stPrevTxPacket,&m_stPacketForTx,sizeof(m_stPacketForTx));

		//!If the packet count is not Zero, then multiple packet to be transmitted
		//!to the processor. So set the \var m_bMutiplePackets to true,
		//!else set the \var m_bMutiplePackets to false.
		if(m_usnPacketCount != NULL)
		{
			m_bMutiplePackets = TRUE;
		}
		else
		{
			m_bMutiplePackets = FALSE;
		}

        //sprintf(statDebugPrintBuf, "\r\n MJ: %d\n", m_stPacketForTx.usnMajorCommand);
        //UartDebugPrint((int *)statDebugPrintBuf, 50);
        //sprintf(statDebugPrintBuf, "\r\n Mr: %d\n", m_stPacketForTx.usnMinorCommand);
        //UartDebugPrint((int *)statDebugPrintBuf, 50);

        //sprintf(statDebugPrintBuf, "\r\n CRC %d\n", usnCalculatedChkSum);
        //UartDebugPrint((int *)statDebugPrintBuf, 50);

		//!Write the framed packet to SPI
		CPWriteDataToSPI();

		//CPEnqueue(&m_stPacketForTx);

	}
}
/******************************************************************************/
/*!
 *  \fn     CPSetPacketCRC()
 *  \brief to write to the CRC field of the SPI Packet Frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \param uint16_t usnCalculatedCRC CRC calculated
 *  \return void
 */
/******************************************************************************/
void CPSetPacketCRC(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnCalculatedCRC )
{
    //!Update the calculated CRC to the structure: CRC
	stPtrParsedPacket->usnCRC = usnCalculatedCRC;
}
//SAM_PACKET_COUNT - START
/******************************************************************************/
/*!
 *  \fn     CPSetTotalNoOfPackets()
 *  \brief	To set the total no of packets for transmission
 *  \param  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \return void
 */
/******************************************************************************/
void CPSetTotalNoOfPackets(ST_SPI_PACKET_FRAME *stPtrParsedPacket)
{
    //!Update the remaining packet count to the structure: Remianing packe to
    //!to be transmitted
	stPtrParsedPacket->usnRemainingPacketsforTX = m_usnPacketCount;
}
/******************************************************************************/
/*!
 *  \fn     CPGetRawADCOrHeightData()
 *  \brief  To update the ST_SPI_PACKET_FRAME structure with the
 *  			pulse height data
 *  \param1  E_CELL_TYPE eTypeofCell
 *  \param2  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param3  uint16_t usnSubCmd
 *  \return  uint16_t
 */
/******************************************************************************/
uint16_t CPGetRawADCOrHeightData(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnSubCmd, bool_t bRawADCDataReq)
{
    //!Set the data length to zero by default
	uint16_t usnDataLength = 0;
	//!Reminder is used to store the data length of the final packet when
	//!multiple packets are transmitted
	//static uint16_t usnRemainder = 0;
	static uint32_t usnRemainder = 0;    //QA_C

	//uint16_t usncount = 0;
	//!Set no. of packets to be sent to Sitara to Zero by default
	//static uint16_t susnNoOfPacketsToSitara = 0;
	static uint32_t susnNoOfPacketsToSitara = 0;   //QA_C
	//!Previous index is used to obtain the Raw data address location
	//!Set the previous index to zero by default
	static uint32_t usnPreviousIndex = 0;
	//!Set the new index to Zero by default
	static uint32_t usnNewIndex =0;
	//!Set the data size to Zero by default
	static uint32_t uslDataSize = 0;

	//!Check if packet is pending to be sent to Sitara
	if(susnNoOfPacketsToSitara == NULL)
	{
	    //!Check if next second Raw data to be sent to Sitara
		if(g_bNextSecRawDataReq == TRUE)
		{
		    //!Get the index of the next second data to be transmitted to Processor
			usnPreviousIndex = CPGetNextIndexForADCData(m_eTypeofCell);
			//!Set next second Raw Data to False.
			g_bNextSecRawDataReq = FALSE;
		}
		else
		{
		    //!If next second data need not to be sent to Sitara, set the
		    //!previous index to zero
			usnPreviousIndex = 0;
		}
		//!Set new index and the reminder to Zero
		usnNewIndex = 0;
		usnRemainder =0;

		//!Set the data length of the ADC Raw Data request.
		//!If it is not the last second  of the RAW data, set the data length
		//!as \def RAW_ADC_DATA_SIZE, else set the data length as per the data
		//!data available in the last second
		if(bRawADCDataReq == FALSE)
		{
			uslDataSize = SysGetADCDataLength(usnSubCmd);
		}
		else
		{
			uslDataSize = RAW_ADC_DATA_SIZE ;
		}
		//!If it is last packet to be transmitted to processor, no multiple packet
		//!to be transmitted to sitara, the packet count is set to zero, the
		//!last multiple packet is set to false to ensure no multiple packet is transmitted
		if(uslDataSize<MAXIMUM_DATA_SIZE)
		{
			susnNoOfPacketsToSitara = 0;
			m_usnPacketCount =0;
			m_bLastMultiplePacket = FALSE;
		}
		else
		{
		    //!If it is not last packet, check for the reminder.
		    //!Set the number of packets and the packet count to Sitara to be sent
			//!Every packet contains 502 WORD (1008 bytes) of data except the last
			//!packet to be sent
			usnRemainder = uslDataSize % MAXIMUM_DATA_SIZE;
			if(usnRemainder == 0)
			{
			    susnNoOfPacketsToSitara = uslDataSize /MAXIMUM_DATA_SIZE;
				m_usnPacketCount = susnNoOfPacketsToSitara -1;
			}
			else
			{
				susnNoOfPacketsToSitara = uslDataSize /MAXIMUM_DATA_SIZE;
				susnNoOfPacketsToSitara =  susnNoOfPacketsToSitara + 1;
				m_usnPacketCount = susnNoOfPacketsToSitara - 1;
			}
		}
	}
	else
	{
	    //!Reduce the count of no. of packets to Sitara after transmission of
	    //!every packet
	    susnNoOfPacketsToSitara--;
		//Update m_usnPacketCount to set the remaining chunks
	    //!Update the packet count after sending the packet to Sitara
		m_usnPacketCount = susnNoOfPacketsToSitara - 1;
	}

	//!Update the data length to maximum if it is not the last packet to be
	//!transmitted when multiple packets are transmitted.
	//!else, update the data length with the data left to be transmitted to the
	//!processor
	if(m_usnPacketCount != NULL)
	{
		usnDataLength = MAXIMUM_DATA_SIZE;
	}
	else
	{
		if(uslDataSize<MAXIMUM_DATA_SIZE)
		{
			usnDataLength = (uint16_t)uslDataSize;
		}
		else if(usnRemainder != NULL)
		{
			usnDataLength = usnRemainder;
		}
		else
		{
			usnDataLength = MAXIMUM_DATA_SIZE;
		}
	}

	//!Update the index to obtain the data from the SDRAM for the raw data and
	//!for the pulse height.
	if(bRawADCDataReq == FALSE)
	{
		usnNewIndex =  SysGetPulseHeightDataFrmSDRAM(m_eTypeofCell, \
		        stPtrParsedPacket->arrusnData,usnPreviousIndex,usnDataLength);
	}
	else
	{
		usnNewIndex =  SysGetADCRawDataFrmSDRAM(m_eTypeofCell, \
		        stPtrParsedPacket->arrusnData,usnPreviousIndex,usnDataLength);
	}

    //!If it is last packet to be sent to Sitara, update the cell type and new index
    //!and reset all the associated variables
	if(susnNoOfPacketsToSitara == 1)
	{
		if(bRawADCDataReq == TRUE)
		{
			CPSetNextIndexForADCData(m_eTypeofCell, usnNewIndex);
		}
		usnPreviousIndex = 0;
		usnNewIndex = 0;
		usnRemainder = 0;
		susnNoOfPacketsToSitara =0;
		m_usnPacketCount =0;
		m_bLastMultiplePacket = TRUE;
		uslDataSize = 0;
	}
	else
	{
		usnPreviousIndex = usnNewIndex;
		m_bLastMultiplePacket = FALSE;
	}

	//!Return the data length to be sent to Sitara
	 return usnDataLength;//return here
}
/******************************************************************************/
/*!
 *  \fn     CPGetHeightDataForReTransmission()
 *  \brief	To set the total no of packets for transmission
 *  \param1  E_CELL_TYPE eTypeofCell
 *  \param2  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param3  uint16_t usnSubCmd
 *  \return uint32_t
 */
/******************************************************************************/
uint16_t CPGetHeightDataForReTransmission(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnSubCmd)
{
    //!Set the data length to zero by default
	uint16_t usnDataLength = 0;
	//!Reminder is used to store the data length of the final packet when
	//!multiple packets are transmitted
	//uint16_t iRemainder = 0;
	uint32_t iRemainder = 0;     //QA_C
	//!total no. of packets to be re-transmitted to Sitara
	//uint16_t iTotalNoOfPackets = 0;
	uint32_t iTotalNoOfPackets = 0;      //QA_C
    //!Previous index is used to obtain the Raw data address location
    //!Set the previous index to zero by default
	uint32_t usnPreviousIndex =0;
	//!Set the data size to Zero by default
	uint32_t uslDataSize = 0;
	//!Count to run the loop to update the structure with zero where data is not available
	uint16_t usncount = 0;

	//!Udpate the previous index with the data array
	usnPreviousIndex = m_stReceivedPacketFrame.arrusnData[0] * MAXIMUM_DATA_SIZE;
	//!Update the data size with for the data to be sent to Processor
	uslDataSize = SysGetADCDataLength(stPtrParsedPacket->usnMinorCommand);

    //!If it is not last packet, check for the reminder.
    //!Set the number of packets and the packet count to Sitara to be sent
    //!Every packet contains 502 WORD (1008 bytes) of data except the last
    //!packet to be sent
	//every packet can contain 502 WORD of data.
	iRemainder = uslDataSize % MAXIMUM_DATA_SIZE;
	//!Update the total number of packets depending on the reminder.
	//!If the reminder is Zero, total no of packet to be transmitted to processor
	//!is calculated as (Data Size/Maximum Data Packet)
	//!if the reminder is non Zero, then the total no. of packet to be transmitted
	//!is  (Data Size/Maximum Data Packet) + 1
	if(iRemainder == 0)
	{
		iTotalNoOfPackets = uslDataSize / MAXIMUM_DATA_SIZE;
	}
	else
	{
		iTotalNoOfPackets = uslDataSize / MAXIMUM_DATA_SIZE;
		iTotalNoOfPackets =  iTotalNoOfPackets + 1;
	}

	//!Update the data length depending upon the data size to be transmitted.
	//!If it is last packet and the data to be transmitted is less than the
	//!Maximum data size, then update it with the pending data.
	//!If it is no tthe last packet then update it with the maximum data packet
	if(uslDataSize<MAXIMUM_DATA_SIZE)
	{
		usnDataLength = (uint16_t)uslDataSize;
	}
	else if(m_stReceivedPacketFrame.arrusnData[0] == (iTotalNoOfPackets - 1))
	{
		usnDataLength = iRemainder;
	}
	else
	{
		usnDataLength = MAXIMUM_DATA_SIZE;
	}

	//!Get the pulse height data of the cell type that needs to be transmitted
	SysGetPulseHeightDataFrmSDRAM(m_eTypeofCell, stPtrParsedPacket->arrusnData,\
	        usnPreviousIndex,usnDataLength);

	//!Update the structure: Data with the zero for the pending data
	if(usnDataLength < MAXIMUM_DATA_SIZE)
	{
		 for(usncount = usnDataLength; usncount < MAXIMUM_DATA_SIZE; usncount++)
		{
			stPtrParsedPacket->arrusnData[usncount] = 0;
		}
	}

	//!Set the packet count to Zero
	m_usnPacketCount = 0;
	//!Set the packet count to Zero
	m_bNAckReceived = FALSE;
	return usnDataLength;
}

/******************************************************************************/
/*!
 *  \fn     CPSetPacketAttributesForTx()
 *  \brief	To set the message attributes for transmission
 *  \param1  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param2  uint16_t usnMajorCmd
 *  \param3  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CPSetPacketAttributesForTx(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnMajorCmd,uint16_t usnSubCmd)
{
	 //!Adds the Start of packet to the Packet Frame
	CPSetStartOfPacket(&m_stPacketForTx);

	//!Fill the Command Attribute
	CPSetCommandsForPacket(&m_stPacketForTx, usnMajorCmd, usnSubCmd);

	//!Adds the Footer to the Data Packet
	CPSetEndOfPacket(&m_stPacketForTx);
}
/******************************************************************************/
/*!
 *  \fn     CPGetCountData()
 *  \brief	To write the counts data to the SPI Packet
 *  \param	ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \return  uint16_t return the data length
 */
/******************************************************************************/
uint16_t CPGetCountData(ST_SPI_PACKET_FRAME *stPtrParsedPacket)
{
    //!By default set the data length to Zero
	uint16_t usnDataLength = 0;
	//!By default set the count to Zero
	//!Count to run the loop to update the structure with zero where data is not available
	uint16_t usncount = 0;

	//!If the minor command is WBC, RBC, PLT, HGB Count
	//!Update the WBC, RBC, PLT, HGB data to the structure: data and
	//!update the rest of the data section to zero
	if(stPtrParsedPacket->usnMinorCommand == DELFINO_WBC_RBC_PLT_HGB_COUNT)
	{
		usnDataLength = SysGetAdcCountData(stPtrParsedPacket->arrusnData);

		for(usncount = usnDataLength; usncount < MAXIMUM_DATA_SIZE ; usncount++)
		{
			stPtrParsedPacket->arrusnData[usncount] = 0;
		}
	}

	//!The data count for Set the packet count to zero, multiple packet to false
	//!and last multiple packet to false
	m_usnPacketCount =0;
	m_bMutiplePackets = FALSE;
	m_bLastMultiplePacket = FALSE;
	//!Return the data length
	return usnDataLength;//return here
}
/******************************************************************************/
/*!
 *  \fn     CPWriteSHDataToPacket()
 *  \brief	To write the Service handled to the SPI Packet
 *  \param	ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param	bool_t bNAKReceived
 *  \return  uint16_t return the data length
 */
/******************************************************************************/
uint16_t CPWriteSHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived)
{
    //!Set the data length to zero by default
	uint16_t usnDataLength = 0;


	union UT_MOTOR_HOME_POSITION utMotorHomePositionCheck;
	//!Check for the minor command and set the data, data length and packet
	//! count based on the minor command
	switch(stPtrParsedPacket->usnMinorCommand)
	{
		case DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD:
		case DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD:
		case DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD:
		case DELFINO_PRIME_ALL_COMPLETED_CMD:
		case DELFINO_BACK_FLUSH_COMPLETED_CMD:
		case DELFINO_PRIME_WITH_EZ_CLEANSER_CMD:
		case DELFINO_ZAP_APERTURE_COMPLETED_CMD:
		case DELFINO_DRAIN_BATH_COMPLETED_CMD:
		case DELFINO_DRAIN_ALL_COMPLETED_CMD:
		case DELFINO_CLEAN_BATH_COMPLETED_CMD:
		case DELFINO_START_UP_SEQUENCE_COMPLETED_CMD:
		case DELFINO_HEAD_RINSING_COMPLETED_CMD:
		case DELFINO_PROBE_CLEANING_COMPLETED_CMD:
		case DELFINO_SPI_ERROR_CHECK:
		case DELFINO_SLEEP_SEQUENCE_COMPLETED:
		case DELFINO_ABORT_SEQUENCE_COMPLETED_CMD:  //HN_added
		case DELFINO_BATH_FILL_SEQUENCE_COMPLETED_CMD:  //HN_added
		case DELFINO_ZAP_INITIATE_COMPLETED_CMD:
		case DELFINO_SET_WASTE_BIN_FULL:
		case DELFINO_CLEAR_WASTE_BIN_FULL:
		{
		    //!Set data length and packet count to zero for the above sequence
			usnDataLength = 0;
			m_usnPacketCount =0;
		}
		break;


		case DELFINO_VALVE_TEST_COMPLETED_CMD:
		{
		    //!Set the valve number for which the valve test was performed
			stPtrParsedPacket->arrusnData[0] =  m_ucValveNo;
			//!Reset the valve number
			m_ucValveNo = 0;
			//!Set the data length to one
			usnDataLength = 1;
			//!Set the packet count to Zero
			m_usnPacketCount = 0;

		}
		break;

		case DELFINO_SYSTEM_STATUS_DATA:
		{
		    //!Set the data for System health status packet
			SysInitSystemHealthData(stPtrParsedPacket->arrusnData);
			//!Set the data length according to system health packet
			usnDataLength = CP_SYSTEM_STATUS_LENGTH;
			//!Set the packet count to Zero
			m_usnPacketCount = 0;
		}
		break;

		case DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD:
		{
		    //!If the motor check status is success,
		    //! then set the success info in the data field, else set motor
		    //! failure status in data filed
			if(g_bMotorChkStatus == TRUE)
			{
				stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
				//!Reset the motor check status flag
				g_bMotorChkStatus = FALSE;
			}
			else
			{
				stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
			}

			//!Get the home position check for the motors and update the data
			//!accordingly
			if(TRUE == OIHomeSensor4State())
			{
			    stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
			}
			else
			{
			    stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
			}
			//!Set the data length
			usnDataLength = 2;
			//!Set the packet count to Zero
			m_usnPacketCount = 0;
		}
		break;

        case DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD:
        {
            //!If the motor check status is success,
            //! then set the success info in the data field, else set motor
            //! failure status in data filed
            if(g_bMotorChkStatus == TRUE)
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
                g_bMotorChkStatus = FALSE;
            }
            else
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
            }
            //!Get the home position check for the motors and update the data
            //!accordingly
            if(TRUE == OIHomeSensor5State())
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
            }
            else
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
            }
            //!Set the data length
            usnDataLength = 2;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD:
        {
            //!If the motor check status is success,
            //! then set the success info in the data field, else set motor
            //! failure status in data filed
            if(g_bMotorChkStatus == TRUE)
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
                g_bMotorChkStatus = FALSE;
            }
            else
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
            }
            //!Get the home position check for the motors and update the data
            //!accordingly
            if(TRUE == OIHomeSensor2State())
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
            }
            else
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
            }
            //!Set the data length
            usnDataLength = 2;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD:
        {
            //!If the motor check status is success,
            //! then set the success info in the data field, else set motor
            //! failure status in data filed
            if(g_bMotorChkStatus == TRUE)
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
                g_bMotorChkStatus = FALSE;
            }
            else
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
            }
            //!Get the home position check for the motors and update the data
            //!accordingly
            if(TRUE == OIHomeSensor1State())
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
            }
            else
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
            }
            //!Set the data length
            usnDataLength = 2;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD:
        {
            //!If the motor check status is success,
            //! then set the success info in the data field, else set motor
            //! failure status in data filed
            if(g_bMotorChkStatus == TRUE)
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
                g_bMotorChkStatus = FALSE;
            }
            else
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
            }
            //!Get the home position check for the motors and update the data
            //!accordingly
            if(TRUE == OIHomeSensor3State())
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
            }
            else
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
            }
            //!Set the data length
            usnDataLength = 2;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        //!If the sequence is counting time, get the data and update to the data
        //! field
		case DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD:
		{
		    //!Get the counting time for WBC and RBC and update the data length
			usnDataLength = SysInitGetCountingTime(stPtrParsedPacket->arrusnData);
			//!Set the packet count to Zero
			m_usnPacketCount = 0;
		}
		break;

		case DELFINO_PRIME_WITH_RINSE_STARTED_CMD:
        case DELFINO_PRIME_WITH_LYSE_STARTED_CMD:
        case DELFINO_PRIME_WITH_DILUENT_STARTED_CMD:
        case DELFINO_PRIME_ALL_STARTED_CMD:
        case DELFINO_BACK_FLUSH_STARTED_CMD:
        case DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD:
        case DELFINO_ZAP_APERTURE_STARTED_CMD:
        case DELFINO_DRAIN_BATH_STARTED_CMD:
        case DELFINO_DRAIN_ALL_STARTED_CMD:
        case DELFINO_CLEAN_BATH_STARTED_CMD:
        case DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD://SKM_COUNTSTART_CHANGE
        case DELFINO_SLEEP_SEQUENCE_STARTED:
        case DELFINO_ABORT_SEQUENCE_STARTED_CMD: //HN_added
        case DELFINO_BATH_FILL_SEQUENCE_STARTED_CMD: //HN_added
        case DELFINO_ZAP_INITIATE_STARTED_CMD:
        {
            //!Set the data length
            usnDataLength = 0;
            //!Set the packet count to Zero
            m_usnPacketCount =0;
        }
        break;

        case DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD:
        case DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD:
        case DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD:
        case DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD:
        case DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD:
        {
            //!Set the data length
            usnDataLength = 0;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_HOME_POSITION_SENSOR_CHECK:
        {
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor4State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnXHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnXHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor5State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnYHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnYHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor2State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnDiluentHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnDiluentHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor1State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnWasteHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnWasteHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor3State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnSampleHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnSampleHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!set Motor home position information to data
            stPtrParsedPacket->arrusnData[0] = utMotorHomePositionCheck.usnMotorHomePosition;
            //!Set the data length
            usnDataLength = 1;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_READY_FOR_ASPIRATION:
        case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:
        {
            //!Set the data length
            usnDataLength = 0;
            //!Set the packet count to Zero
            m_usnPacketCount =0;
        }
        break;

        case DELFINO_ERROR_MINOR_CMD:
            sprintf(DebugPrintBuf, "\r\nDELFINO_ERROR_MINOR_CMD\n");
            UartDebugPrint((int *)DebugPrintBuf, 50);
            usnDataLength = CPGetErrorData(stPtrParsedPacket);
            m_usnPacketCount =0;
            break;

		default:
		{
		    //!Set the data length
			usnDataLength = 0;
			//!Set the packet count to Zero
			m_usnPacketCount =0;
		}
		break;
	}//end of switch case
	return usnDataLength;
}
/******************************************************************************/
/*!
 *  \fn     CPWriteFirmwareVerToPacket()
 *  \brief	To write the firmware version to the SPI Packet
 *  \param	ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param	bool_t bNAKReceived
 *  \return  uint16_t return the data length
 */
/******************************************************************************/
uint16_t CPWriteFirmwareVerToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived)
{
    //!Get the firmware version and update to the data packet
	SysInitGetFirmwareVer(stPtrParsedPacket->arrusnData);
	//!Return the firmware version data length
	return CP_FIRMWARE_VERSION_LENGTH;
}
/******************************************************************************/
/*!
 *  \fn     CPSetNextIndexForADCData()
 *  \brief	To set the Index for ADC data
 *  \param	E_CELL_TYPE eTypeofCell
 *  \param	uint32_t uslPreviousIndex
 *  \return  void
 */
/******************************************************************************/
void CPSetNextIndexForADCData(E_CELL_TYPE eTypeofCell, uint32_t uslNextIndex)
{
    //!Check for the cell type
	switch(eTypeofCell)
	{
	//!If the cell type is RBC, update the next index for getting the data from SDRAM
	case eCellRBC:
		m_suslRBCNextIndex = uslNextIndex;
		break;

	//!If the cell type is PLT, update the next index for getting the data from SDRAM
	case eCellPLT:
		m_suslPLTNextIndex = uslNextIndex;
		break;

	//!If the cell type is WBC, update the next index for getting the data from SDRAM
	case eCellWBC:
		m_suslWBCNextIndex = uslNextIndex;
		break;

	default:
		break;
	}

}
/******************************************************************************/
/*!
 *  \fn     CPGetNextIndexForADCData()
 *  \brief	To get the next Index for ADC data
 *  \param	E_CELL_TYPE eTypeofCell
 *  \return  uint32_t previous index based on the cell type
 */
/******************************************************************************/
uint32_t CPGetNextIndexForADCData(E_CELL_TYPE eTypeofCell)
{
	uint32_t uslIndex = 0;
	switch (eTypeofCell)
	{
	    //!If the cell type is RBC, get the next index for getting the data from SDRAM
		case eCellRBC:
			uslIndex = m_suslRBCNextIndex;
		break;

		//!If the cell type is PLT, get the next index for getting the data from SDRAM
		case eCellPLT:
			uslIndex = m_suslPLTNextIndex;
		break;

		//!If the cell type is WBC, get the next index for getting the data from SDRAM
		case eCellWBC:
			uslIndex = m_suslWBCNextIndex;
		break;

		default:
			uslIndex = NULL;
		break;
	}
	//!Return the index
	return uslIndex;
}
/******************************************************************************/
/*!
 *  \fn     CPReInitialiseOnNewMeasurement()
 *  \brief	To initialise the variable on every new measurement
 *  \param	void
 *  \return  void
 */
/******************************************************************************/
void CPReInitialiseOnNewMeasurement()
{
    //!Initialize the RBC next index
	m_suslRBCNextIndex = 0;
	//!Initialize the WBC next index
	m_suslWBCNextIndex = 0;
	//!Initialize the PLT next index
	m_suslPLTNextIndex = 0;

	//!Reset the multiple packet to false
	m_bMutiplePackets = FALSE;
	//!Reset the last multiple packet to false
	m_bLastMultiplePacket = FALSE;

    //!Reset the volumetric error
    utVolumetricErrorInfo.unVolumetricError = ZERO;
    utVolumetricErrorInfoReAcq.unVolumetricError = ZERO;

}
/******************************************************************************/
/*!
 *  \fn     CPReTransmistADCRawData()
 *  \brief  To retransmit ADC Raw data
 *  \param	void
 *  \return  uint32_t
 */
/******************************************************************************/
uint16_t CPReTransmistADCRawData(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnSubCmd)
{
    //!Initialize the data length, ADC reminder, No. of packets,
    //! Previous index and present index
	uint16_t usnDataLength = 0;
	//static uint16_t usnADCRemainder = 0;
	static uint32_t usnADCRemainder = 0;    //QA_C
	//static uint16_t susnNoOfPackets = 0;
	//msg(3:4461) A non-constant expression of 'essentially unsigned' type (unsigned long)
	//is being converted to narrower unsigned type, 'unsigned int' on assignment.
	static uint32_t susnNoOfPackets = 0;     //QA_C
	static uint32_t usnPrevIndex = 0;
	static uint32_t usnNextIndex =0;
	uint32_t uslDataSize = 0;

	//!If the no. of packets to be transmitted is null
	if(susnNoOfPackets == NULL)
	{
	    //!Get the next index for RAW ADC Data
		usnPrevIndex = CPGetNextIndexForADCData(m_eTypeofCell);
		//!Set next index to Zero
		usnNextIndex = 0;
		//!ADC Reminder to Zero
		usnADCRemainder =0;
		//!ADC Data size as maximum packet data size
		uslDataSize = RAW_ADC_DATA_SIZE ;

		//!If the Data size is less than the Maximum data size, set the
		//! packet count to and no.of packet to Zero and set
		//! last multiple packet to Zero
		if(uslDataSize<MAXIMUM_DATA_SIZE)
		{
			susnNoOfPackets = 0;
			m_usnPacketCount =0;
			m_bLastMultiplePacket = FALSE;
		}
		else
		{
			//!every packet can contain 502 WORD (1008 bytes) of data.
			//! and calculate the ADC reminder
			usnADCRemainder = uslDataSize % MAXIMUM_DATA_SIZE;
			//!Set no. of packet and packet count, based on the reminder
			if(usnADCRemainder == 0)
			{

				susnNoOfPackets = uslDataSize /MAXIMUM_DATA_SIZE;
				m_usnPacketCount = susnNoOfPackets -1;
			}
			else
			{

				susnNoOfPackets = uslDataSize /MAXIMUM_DATA_SIZE;
				susnNoOfPackets =  susnNoOfPackets + 1;
				m_usnPacketCount = susnNoOfPackets - 1;
			}
		}
	}
	else
	{
	    //!Decrement the packet count and no. of packet
		susnNoOfPackets--;
		//Update m_usnPacketCount to set the remaining chunks
		m_usnPacketCount = susnNoOfPackets - 1;
	}

	//!Set the data length based on the packet count
	if(m_usnPacketCount != NULL)
	{
		usnDataLength = MAXIMUM_DATA_SIZE;
	}
	else
	{
	    //!Set the data length of the packet based on the Data size
		if(uslDataSize<MAXIMUM_DATA_SIZE)
		{
			usnDataLength = (uint16_t)uslDataSize;
		}
		else if(usnADCRemainder != NULL)
		{
			usnDataLength = usnADCRemainder;
		}
		else
		{
			usnDataLength = MAXIMUM_DATA_SIZE;
		}
	}

	//!Update the ADC raw data to the packet to send to Sitara and update the
	//! next index to send data to Sitara
	usnNextIndex =  SysGetADCRawDataFrmSDRAM(m_eTypeofCell, \
	        stPtrParsedPacket->arrusnData,usnPrevIndex,usnDataLength);

	//!Reset the variables based on the data transmitted to sitara
	if(susnNoOfPackets == 1)
	{

	    //!Update the index to copy data from SDRAM to for paket to Sitara
		CPSetNextIndexForADCData(m_eTypeofCell,usnNextIndex);

		usnPrevIndex = 0;
		usnNextIndex = 0;
		usnADCRemainder = 0;
		susnNoOfPackets =0;
		m_usnPacketCount =0;
		m_bLastMultiplePacket = TRUE;
		m_bNAckReceived = FALSE;
	}
	else
	{
		usnPrevIndex = usnNextIndex;
		m_bLastMultiplePacket = FALSE;
	}

	//!Return the data length
	return usnDataLength;//return here
}
//SAM_CALIBRATION - START
/******************************************************************************/
/*!
 *  \fn     CPWriteCHDataToPacket()
 *  \brief  Writes the data field for Calibration handling of the SPI Packet Frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteCHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived)
{
    uint16_t usnDataLength = 0;
	uint16_t usnSubCmd =0;

    usnSubCmd = stPtrParsedPacket->usnMinorCommand;

    //!Depending upon the minor command frame the data packet
    switch(usnSubCmd)
    {
        //!If the minor command is WBC, RBC, PLT count, then packetize the results
		case DELFINO_WBC_RBC_PLT_HGB_COUNT:
		{
			usnDataLength = CPGetCountData(stPtrParsedPacket);
		}
		break;

		//!If the minor command is pulse height, then packetize the pulse height data
		case DELFINO_WBC_PULSE_HEIGHT_DATA:
		case DELFINO_RBC_PULSE_HEIGHT_DATA:
		case DELFINO_PLT_PULSE_HEIGHT_DATA:
		{
			if(bNAKReceived == FALSE)
			{
			    //!fn. to get Pulse height or ADC RAW data
				usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\
				        usnSubCmd, FALSE);
			}
			else
			{
			    //!fn. to get Pulse height or ADC RAW data
				usnDataLength =\
				        CPGetHeightDataForReTransmission(stPtrParsedPacket,\
				                usnSubCmd);
			}
		}
		break;

		//!If the minor command is ADC RAW data, then packetize the ADC RAW data
		case DELFINO_WBC_ADC_RAW_DATA:
		case DELFINO_RBC_ADC_RAW_DATA:
		case DELFINO_PLT_ADC_RAW_DATA:
		{
			if(bNAKReceived == FALSE)
			{
			    //!fn. to get Pulse height or ADC RAW data
				usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\
				        usnSubCmd, TRUE);
			}
			else
			{
			    //!fn. to get Pulse height or ADC RAW data
				usnDataLength = CPReTransmistADCRawData(stPtrParsedPacket,\
				        usnSubCmd);
			}
		}
		break;

        case DELFINO_ERROR_MINOR_CMD:
            usnDataLength = CPGetErrorData(stPtrParsedPacket);
            m_usnPacketCount =0;
            break;

		case DELFINO_ASPIRATION_COMPLETED:
		case DELFINO_NEEDLE_READY_FOR_DISPENSING_CMD:
		case DELFINO_DISPENSE_COMPLETED_CMD:
		case DELFINO_READY_FOR_ASPIRATION://SAM_ASP_READY
		case DELFINO_SEQUENCE_COMPLETION:
		case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:
		default:
		{
			usnDataLength = 0;
			m_usnPacketCount =0;
		}
		break;
    }//end of switch case

    //!Return the data length
    return usnDataLength;
}
//SAM_CALIBRATION - END

//SAM_QUALITY - START
/******************************************************************************/
/*!
 *  \fn     CPWriteQHDataToPacket()
 *  \brief  Writes the Quality Control handling data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteQHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived)
{
    uint16_t usnDataLength = 0;
	uint16_t usnSubCmd =0;

    usnSubCmd = stPtrParsedPacket->usnMinorCommand;

    //!Depending upon the minor command frame the data packet
    switch(usnSubCmd)
    {
        //!If the minor command is WBC, RBC, PLT count, then packetize the results
		case DELFINO_WBC_RBC_PLT_HGB_COUNT:
		{
			usnDataLength = CPGetCountData(stPtrParsedPacket);
		}
		break;

		//!If the minor command is pulse height, then packetize the pulse height data
		case DELFINO_WBC_PULSE_HEIGHT_DATA:
		case DELFINO_RBC_PULSE_HEIGHT_DATA:
		case DELFINO_PLT_PULSE_HEIGHT_DATA:
		{
			if(bNAKReceived == FALSE)
			{
			    //!fn. to get Pulse height or ADC RAW data
				usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\
				        usnSubCmd, FALSE);
			}
			else
			{
			    //!fn. to get Pulse height or ADC RAW data
				usnDataLength = \
				        CPGetHeightDataForReTransmission(stPtrParsedPacket,\
				                usnSubCmd);
			}
		}
		break;

		//!If the minor command is ADC RAW data, then packetize the ADC RAW data
		case DELFINO_WBC_ADC_RAW_DATA:
		case DELFINO_RBC_ADC_RAW_DATA:
		case DELFINO_PLT_ADC_RAW_DATA:
		{
			if(bNAKReceived == FALSE)
			{
			    //!fn. to get Pulse height or ADC RAW data
				usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\
				        usnSubCmd, TRUE);
			}
			else
			{
			    //!fn. to get Pulse height or ADC RAW data
				usnDataLength = CPReTransmistADCRawData(stPtrParsedPacket,\
				        usnSubCmd);
			}
		}
		break;

        case DELFINO_ERROR_MINOR_CMD:
            usnDataLength = CPGetErrorData(stPtrParsedPacket);
            m_usnPacketCount =0;
            break;

		case DELFINO_ASPIRATION_COMPLETED:
		case DELFINO_NEEDLE_READY_FOR_DISPENSING_CMD:
		case DELFINO_DISPENSE_COMPLETED_CMD:
		case DELFINO_READY_FOR_ASPIRATION:
		case DELFINO_SEQUENCE_COMPLETION:
		case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:
		default:
		{
			usnDataLength = 0;
			m_usnPacketCount =0;
		}
		break;
    }//end of switch case

    //!Return the data length
    return usnDataLength;
}
/******************************************************************************/
/*!
 *  \fn     CPWriteSeqMonitoringDataToPacket()
 *  \brief  Writes the Sequence Monitoring data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteSeqMonitoringDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived)
{
    uint16_t usnDataLength = 0;
    uint16_t usnSubCmd =0;

    usnSubCmd = stPtrParsedPacket->usnMinorCommand;
    switch(usnSubCmd)
    {
        case DELFINO_STATE_SEQUENCE_STARTED:
        case DELFINO_STATE_READY_FOR_DISPENSING:
        case DELFINO_STATE_DISPENSE_COMPLETED:
        case DELFINO_STATE_READY_FOR_ASPIRATION:
        case DELFINO_STATE_ASPIRATION_COMPLETED:
        case DELFINO_STATE_WBC_DISPENSATION_COMPLETED:
        case DELFINO_STATE_RBC_DISPENSATION_COMPLETED:
        case DELFINO_STATE_WBC_RBC_BUBBLING_COMPLETED:
        case DELFINO_STATE_COUNTING_STARTED:
        case DELFINO_STATE_COUNTING_COMPLETED:
        case DELFINO_STATE_BACKFLUSH_COMPLETED:
        case DELFINO_STATE_BATH_FILLING_COMPLETED:
            //!Length of the packet is 1 for sequence monitoring.
            usnDataLength =1;
            //!Packet data is sequence state time
            m_stPacketForTx.arrusnData[0]= g_unSequenceStateTime;
            //!Reset the sequence state to Zero so that the next state period is
            //! counted from the previous state
            g_unSequenceStateTime = 0;
            //!Since it is single packet, packet count is zero
            m_usnPacketCount = 0;
            break;

        default:
            break;

    }//end of switch case
    //!Return the data length
    return usnDataLength;
}
/******************************************************************************/
/*!
 *  \fn     CPWriteAbortProcessDataToPacket()
 *  \brief  Writes the Abort Process data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteAbortProcessDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived)
{
    //!Initialize data length of the packet to zero
    uint16_t usnDataLength = ZERO;
    uint16_t usnSubCmd = ZERO;

    //!Get the minor command from the structure
    usnSubCmd = stPtrParsedPacket->usnMinorCommand;
    /*switch(usnSubCmd)
    {
        //!If the minor command is to abort the process then perform the below operations
        case DELFINO_ABORT_PROCESS_SUB_CMD:
            //!Set the data length to 8 and update the errors
            usnDataLength =8;
            m_stPacketForTx.arrusnData[0]= 0;
            m_stPacketForTx.arrusnData[1]= 0;
            m_stPacketForTx.arrusnData[2]= 0;
            m_stPacketForTx.arrusnData[3]= 0;
            m_stPacketForTx.arrusnData[4]= 0;
            m_stPacketForTx.arrusnData[5]= utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty;
            m_stPacketForTx.arrusnData[6]= utErrorInfoCheck.stErrorInfo.ulnLyseEmpty;
            m_stPacketForTx.arrusnData[7]= utErrorInfoCheck.stErrorInfo.ulnRinseEmpty;
            //!Since it is single packet, packet count is zero
            m_usnPacketCount = ZERO;
            break;

        default:
            break;

    }//end of switch case*/
    if(usnSubCmd == DELFINO_ABORT_PROCESS_SUB_CMD)    //QA_C
    {

        //!If the minor command is to abort the process then perform the below operations

            //!Set the data length to 8 and update the errors
            usnDataLength =8;
            m_stPacketForTx.arrusnData[0]= 0;
            m_stPacketForTx.arrusnData[1]= 0;
            m_stPacketForTx.arrusnData[2]= 0;
            m_stPacketForTx.arrusnData[3]= 0;
            m_stPacketForTx.arrusnData[4]= 0;
            m_stPacketForTx.arrusnData[5]= utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty;
            m_stPacketForTx.arrusnData[6]= utErrorInfoCheck.stErrorInfo.ulnLyseEmpty;
            m_stPacketForTx.arrusnData[7]= utErrorInfoCheck.stErrorInfo.ulnRinseEmpty;
            //!Since it is single packet, packet count is zero
            m_usnPacketCount = ZERO;
    }

    //!Return the data length
    return usnDataLength;
}
/******************************************************************************/
/*!
 *  \fn     CPGetErrorData()
 *  \brief  Writes error data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPGetErrorData(ST_SPI_PACKET_FRAME *stPtrParsedPacket)
{
    //!Atleast one data shall be sent in a packet , so set data length to 1,
    uint16_t usnDataLength = 0;
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnSequenceAborted)
    {
        //!Set the Zeroth location as sequence aborted
        m_stPacketForTx.arrusnData[usnDataLength] = eSEQUENCE_ABORTED;
        //!Increment the data length
        usnDataLength++;
        utErrorInfoCheck.stErrorInfo.ulnSequenceAborted = FALSE;

    }

    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnSequenceArrested)
    {
        //!Set the Zeroth location as sequence aborted
        m_stPacketForTx.arrusnData[usnDataLength] = eSEQUENCE_ARRESTED;
        //!Increment the data length
        usnDataLength++;
        utErrorInfoCheck.stErrorInfo.ulnSequenceArrested = FALSE;

    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnX_Motor)
    {
        //!Set X-Motor Error
        m_stPacketForTx.arrusnData[usnDataLength]= eX_MOTOR;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnX_Motor = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnDiluentSyringe)
    {
        //!Set Diluent Syringe Error
        m_stPacketForTx.arrusnData[usnDataLength]= eDILUENT_SYRINGE;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnSampleSyringe)
    {
        //!Set Sample Syringe Error
        m_stPacketForTx.arrusnData[usnDataLength]= eSAMPLE_SYRINGE;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWasteSyringe)
    {
        //!Set Waste Syringe Error
        m_stPacketForTx.arrusnData[usnDataLength]= eWASTE_SYRINGE;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnY_Motor)
    {
        //!Set Y-Motor Error
        m_stPacketForTx.arrusnData[usnDataLength]= eY_MOTOR;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacWBCDraining)
    {
        //!Set error for WBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_WBC_DRAINING;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDraining)
    {
        //!Set error for RBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingEnd)
    {
        //!Set error for RBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresWBCBubblingStart)
    {
        //!Set error for WBC bubbling pressure
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_WBC_BUBBLING_STARTED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresWBCBubblingEnd)
    {
        //!Set error for WBC bubbling pressure
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_WBC_BUBBLING_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresWBCBubblingStartLyse)
    {
        //!Set error for WBC bubbling after adding lyse pressure
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_WBC_BUBBLING_STARTED_AFTER_LYSE;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresRBCBubblingStartLyse)
    {
        //!Set error for RBC bubbling pressure
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_RBC_BUBBLING_STARTED_AFTER_LYSE;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresRBCBubblingStartLyseEnd)
    {
        //!Set error for RBC bubbling pressure
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_RBC_BUBBLING_COMPLETED_AFTER_LYSE;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacCountingStart)
    {
        //!Set error for counting start vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_COUNTING_STARTED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacCountingEnd)
    {
        //!Set error for counting completed vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_COUNTING_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacWBCDrainingAC)
    {
        //!Set error for WBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_WBC_DRAINING_AC;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingAC)
    {
        //!Set error for RBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING_AC;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingACEnd)
    {
        //!Set error for RBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING_COMPLETED_AC;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresBackflushStart)
    {
        //!Set error for backflush pressure
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_BACKFLUSH_STARTED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresBackflushEnd)
    {
        //!Set error for backflush pressure
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_BACKFLUSH_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacWBCDrainingFD)
    {
        //!Set error for WBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_WBC_DRAINING_FD;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingFD)
    {
        //!Set error for RBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING_FD;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingFDEnd)
    {
        //!Set error for RBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING_COMPLETED_FD;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnAspirationNotCompleted)
    {
        //!Set error for RBC draining vacuum
        m_stPacketForTx.arrusnData[usnDataLength]= eASPIRATION_NOT_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnAspirationCompletedImproper)
    {
        //!Set error for improper aspiration time
        m_stPacketForTx.arrusnData[usnDataLength]= eASPIRATION_IMPROPER_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCDispensingNotCompleted)
    {
        //!Set error for WBC dispensing not completed
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_DISPENSING_NOT_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCDispensingCompletedImproper)
    {
        //!Set error for WBC dispensing completion improper
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_DISPENSING_IMPROPER_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnRBCDispensingNotCompleted)
    {
        //!Set error for RBC dispensing not completed
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_DISPENSING_NOT_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnRBCDispensingCompletedImproper)
    {
        //!Set error for WBC dispensing completion improper
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_DISPENSING_IMPROPER_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCRBCBubblingNotCompleted)
    {
        //!Set error for WBC RBC bubbling not completed
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_RBC_BUBBLING_NOT_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCRBCBubblingCompletedImproper)
    {
        //!Set error for WBC RBC bubbling completion improper
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_RBC_BUBBLING_IMPROPER_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted)
    {
        //!Set error for WBC counting not started
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_NOT_STARTED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted = FALSE;
    }
    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingStartedImproper)
    {
        //!Set error for WBC counting started improper
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_IMPROPER_START;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unWBCCountingStartedImproper = FALSE;
    }
    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted)
    {
        //!Set error for RBC counting not started
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_NOT_STARTED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted = FALSE;
    }
    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingStartedImproper)
    {
        //!Set error for RBC counting started improper
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_IMPROPER_START;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unRBCCountingStartedImproper = FALSE;
    }
    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotCompleted)
    {
        //!Set error for RBC counting not completed
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_NOT_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotCompleted = FALSE;
    }
    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper)
    {
        //!Set error for WBC counting completed improper
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_IMPROPER_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper = FALSE;
    }

    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotCompleted)
    {
        //!Set error for RBC counting not completed
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_NOT_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotCompleted = FALSE;
    }
    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper)
    {
        //!Set error for RBC counting completed improper
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_IMPROPER_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnBackFlushNotCompleted)
    {
        //!Set error for Backflush not completed
        m_stPacketForTx.arrusnData[usnDataLength]= eBACKFLUSH_NOT_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnBackFlushCompletedImproper)
    {
        //!Set error for Backflush completion improper
        m_stPacketForTx.arrusnData[usnDataLength]= eBACKFLUSH_IMPROPER_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnBathFillingNotCompleted)
    {
        //!Set error for Bath filling not completed
        m_stPacketForTx.arrusnData[usnDataLength]= eBATHFILLING_NOT_COMPLETED;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnBathFillingCompletedImproper)
    {
        //!Set error for Backflush completion improper
        m_stPacketForTx.arrusnData[usnDataLength]= eBATHFILLING_IMPROPER_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Reset the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWasteBinFull)
    {
        //!Set error for Waste bin full
        m_stPacketForTx.arrusnData[usnDataLength]= eWASTE_BIN_FULL;
        //!Increment the data length
        usnDataLength++;

        //!Do not reset the error after sending the error to Sitara
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty)
    {
        //!Set error for Diluent empty
        m_stPacketForTx.arrusnData[usnDataLength]= eDILUENT_EMPTY;

        //!Increment the data length
        usnDataLength++;

        //!Reset the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnLyseEmpty)
    {
        //!Set error for Lyse empty
        m_stPacketForTx.arrusnData[usnDataLength]= eLYSE_EMPTY;

        //!Increment the data length
        usnDataLength++;

        //!Reset the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnRinseEmpty)
    {
        //!Set error for Rinse empty
        m_stPacketForTx.arrusnData[usnDataLength]= eRINSE_EMPTY;

        //!Increment the data length
        usnDataLength++;

        //!Reset the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnTemperature)
    {
        //!Set error for Temperature not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eTEMPERATURE_ERROR;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnTemperature = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.uln24V_Check)
    {
        //!Set error for 24V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= e24V_ERROR;

        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.uln24V_Check = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.uln5V_Check)
    {
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= e5V_ERROR;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.uln5V_Check = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnReAcq)
    {
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eRE_ACQUISITION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnReAcq = FALSE;
    }

    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCEarlyCompletion)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCEarlyCompletion");
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_EARLY_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unWBCEarlyCompletion = FALSE;
    }

    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCLateCompletion)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCLateCompletion");
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_LATE_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unWBCLateCompletion = FALSE;
    }

    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCEarlyCompletion)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCEarlyCompletion");
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_EARLY_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unRBCEarlyCompletion = FALSE;
    }

    if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCLateCompletion)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCLateCompletion");
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_LATE_COMPLETION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfo.stVolumetricError.unRBCLateCompletion = FALSE;
    }

    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingNotStarted)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCCountingNotStarted");
        //!Set error for WBC counting not started
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_NOT_STARTED_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingNotStarted = FALSE;
    }
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingStartedImproper)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCCountingStartedImproper");
        //!Set error for WBC counting started improper
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_IMPROPER_START_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingStartedImproper = FALSE;
    }
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingNotStarted)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCCountingNotStarted");
        //!Set error for RBC counting not started
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_NOT_STARTED_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingNotStarted = FALSE;
    }
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingStartedImproper)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCCountingStartedImproper");
        //!Set error for RBC counting started improper
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_IMPROPER_START_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingStartedImproper = FALSE;
    }
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingNotCompleted)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCCountingNotCompleted");
        //!Set error for RBC counting not completed
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_NOT_COMPLETED_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingNotCompleted = FALSE;
    }
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingCompletedImproper)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCCountingCompletedImproper");
        //!Set error for WBC counting completed improper
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_IMPROPER_COMPLETION_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingCompletedImproper = FALSE;
    }

    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingNotCompleted)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCCountingNotCompleted");
        //!Set error for RBC counting not completed
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_NOT_COMPLETED_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingNotCompleted = FALSE;
    }
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingCompletedImproper)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCCountingCompletedImproper");
        //!Set error for RBC counting completed improper
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_IMPROPER_COMPLETION_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingCompletedImproper = FALSE;
    }

    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCEarlyCompletion)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCEarlyCompletion");
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_EARLY_COMPLETION_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCEarlyCompletion = FALSE;
    }

    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCLateCompletion)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCLateCompletion");
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_LATE_COMPLETION_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCLateCompletion = FALSE;
    }

    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCEarlyCompletion)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCEarlyCompletion");
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_EARLY_COMPLETION_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCEarlyCompletion = FALSE;
    }

    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCLateCompletion)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCLateCompletion");
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_LATE_COMPLETION_RE_ACQ;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCLateCompletion = FALSE;
    }
//DS_TESTING
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCImproperAbortion)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCImproperAbortion");
        UartDebugPrint((int *)DebugPrintBuf, 40);
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_IMPROPER_ABORTION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCImproperAbortion = FALSE;
    }
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCImproperAbortion)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCImproperAbortion");
        UartDebugPrint((int *)DebugPrintBuf, 40);
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_IMPROPER_ABORTION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCImproperAbortion = FALSE;
    }
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCFlowCalibAbortion)
    {
        sprintf(DebugPrintBuf, "\r\nunWBCFlowCalibAbortion");
        UartDebugPrint((int *)DebugPrintBuf, 40);
        m_stPacketForTx.arrusnData[usnDataLength]= eWBC_FLOWCALIB_ABORTION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCFlowCalibAbortion = FALSE;
    }
    if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCFlowCalibAbortion)
    {
        sprintf(DebugPrintBuf, "\r\nunRBCFlowCalibAbortion");
        UartDebugPrint((int *)DebugPrintBuf, 40);
        m_stPacketForTx.arrusnData[usnDataLength]= eRBC_FLOWCALIB_ABORTION;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCFlowCalibAbortion = FALSE;
    }
//DS_TESTING
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail)
    {
        //!Set error for pressure sensor error
        m_stPacketForTx.arrusnData[usnDataLength]= ePRESSURE_SENSOR_FAILURE;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail = FALSE;
    }

    if(TRUE == utErrorInfoCheck.stErrorInfo.PressureCalCmpltd)
    {
        sprintf(statDebugPrintBuf, "\r\n PressureCalCmpltd sent to sitara  \n");
        UartDebugPrint((int *)statDebugPrintBuf, 50);

        //!Set Pressure calibration completed
        m_stPacketForTx.arrusnData[usnDataLength]= ePRESSURE_CAL_CMPLTD;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.PressureCalCmpltd = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnLowVacuum)
    {
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eHIGH_VACUUM;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnLowVacuum = FALSE;
    }

    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnHighPressure)
    {
        //!Set error for 5V not with in the range
        m_stPacketForTx.arrusnData[usnDataLength]= eHIGH_PRESSURE;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnHighPressure = FALSE;
    }

    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnY_MotorFault)
    {
        //!Set error for Y_MotorFault
        m_stPacketForTx.arrusnData[usnDataLength]= eY_MOTOR_FAULT;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnY_MotorFault = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnX_MotorFault)
    {
        //!Set error for X_MotorFault
        m_stPacketForTx.arrusnData[usnDataLength]= eX_MOTOR_FAULT;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnX_MotorFault = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnDiluent_MotorFault)
    {
        //!Set error for Diluent_MotorFault
        m_stPacketForTx.arrusnData[usnDataLength]= eDILUENT_MOTOR_FAULT;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnDiluent_MotorFault = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnSample_MotorFault)
    {
        //!Set error for Sample_MotorFault
        m_stPacketForTx.arrusnData[usnDataLength]= eSAMPLE_MOTOR_FAULT;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnSample_MotorFault = FALSE;
    }
    if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWaste_MotorFault)
    {
        //!Set error for Waste_MotorFault
        m_stPacketForTx.arrusnData[usnDataLength]= eWASTE_MOTOR_FAULT;
        //!Increment the data length
        usnDataLength++;

        //!Rest the error after sending the error to Sitara
        utErrorInfoCheck.stErrorInfo.ulnWaste_MotorFault = FALSE;
    }

    //utErrorInfoCheck.ulnErrorInfo = 0;
    //!Since it is single packet, packet count is zero
    m_usnPacketCount = 0;
    //!Return the data length
    return usnDataLength;
}
//SAM_QUALITY - END
/******************************************************************************/
/*!
 *  \fn     CPWriteSystemSettingDataToPacket()
 *  \brief  Writes the System setting data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteSystemSettingDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived)
{
    //!Initialize data length of the packet to zero
    uint16_t usnDataLength = ZERO;
    uint16_t usnSubCmd = ZERO;

    //!Get the minor command from the structure
    usnSubCmd = stPtrParsedPacket->usnMinorCommand;
    switch(usnSubCmd)
    {
        //!If the minor command is to abort the process then perform the below operations
        case DELFINO_FLOW_CALIBRATION_CMD:
            //!Get the counting time for WBC and RBC and update the data length
            usnDataLength = SysInitGetCountingTime(stPtrParsedPacket->arrusnData);
            //!Set the packet count to Zero
            m_usnPacketCount = ZERO;
            break;

        case DELFINO_FLOW_CALIBRATION_STARTED:
            usnDataLength = ZERO;
            m_usnPacketCount = ZERO;
            break;

        case DELFINO_RINSE_PRIME_COMPLETED:
            usnDataLength = ZERO;
            m_usnPacketCount = ZERO;
            break;
        case DELFINO_PRESSURE_CALIBRATED_DATA:
            //!Set the pressure calibrated value to the data packet
            if(sncalZeroPSI > ZERO)
            {
                //!SPI configuration is only done for positive numbers txRx
                //! so we should send only positive numbers because of that here
                //! we are sending absolute value and Sign value
                stPtrParsedPacket->arrusnData[0] = sncalZeroPSI;   //Hn_added
                stPtrParsedPacket->arrusnData[1] = 0;   //Positive          //Hn_added
            }
            else
            {
                //! SPI configuration is only done for positive numbers txRx
                //! so we should send only positive numbers because of that here
                //! we are sending absolute value and Sign value
                stPtrParsedPacket->arrusnData[0] = (sncalZeroPSI * -1);  //Hn_added
                stPtrParsedPacket->arrusnData[1] = 1;  //negative          //Hn_added

            }

            usnDataLength = 2;
            m_usnPacketCount = ZERO;
            break;
        default:
            break;

    }//end of switch case

    //!Return the data length
    return usnDataLength;
}

/******************************************************************************/
/*!
 *  \fn     CPWriteSTPHDataToPacket()
 *  \brief  Writes the Startup handling data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteSTPHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived)
{

    //!Set data length as zero by default
    uint16_t usnDataLength = 0;
    //!Set minor/sub command as zero by default
    uint16_t usnSubCmd =0;

    //!Get the minor command from the structure to update the data
    usnSubCmd = stPtrParsedPacket->usnMinorCommand;

    //union UT_MOTOR_HOME_POSITION utMotorHomePositionCheck;

    //!Check for the minor/sub command
    switch(usnSubCmd)
    {
        case DELFINO_ERROR_MINOR_CMD:
            usnDataLength = CPGetErrorData(stPtrParsedPacket);
            m_usnPacketCount =0;
            break;

        //case DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD:
        //case DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD:
        //case DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD:
        case DELFINO_PRIME_ALL_COMPLETED_CMD:
        case DELFINO_BACK_FLUSH_COMPLETED_CMD:
        //case DELFINO_PRIME_WITH_EZ_CLEANSER_CMD:
        //case DELFINO_ZAP_APERTURE_COMPLETED_CMD:
        //case DELFINO_DRAIN_BATH_COMPLETED_CMD:
        //case DELFINO_DRAIN_ALL_COMPLETED_CMD:
        //case DELFINO_CLEAN_BATH_COMPLETED_CMD:
        //case DELFINO_START_UP_SEQUENCE_COMPLETED_CMD:
        //case DELFINO_HEAD_RINSING_COMPLETED_CMD:
        //case DELFINO_PROBE_CLEANING_COMPLETED_CMD:
        {
            //!Set data length and packet count to zero for the above sequence
            usnDataLength = 0;
            m_usnPacketCount =0;
        }
        break;


        case DELFINO_SYSTEM_STATUS_DATA:
        {
            //!Set the data for System health status packet
            SysInitSystemHealthData(stPtrParsedPacket->arrusnData);
            //!Set the data length according to system health packet
            usnDataLength = CP_SYSTEM_STATUS_LENGTH;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD:
        {
            //!If the motor check status is success,
            //! then set the success info in the data field, else set motor
            //! failure status in data filed
            if(g_bMotorChkStatus == TRUE)
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
                //!Reset the motor check status flag
                g_bMotorChkStatus = FALSE;
            }
            else
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
            }

            //!Get the home position check for the motors and update the data
            //!accordingly
            if(TRUE == OIHomeSensor4State())
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
            }
            else
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
            }
            //!Set the data length
            usnDataLength = 2;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD:
        {
            //!If the motor check status is success,
            //! then set the success info in the data field, else set motor
            //! failure status in data filed
            if(g_bMotorChkStatus == TRUE)
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
                g_bMotorChkStatus = FALSE;
            }
            else
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
            }
            //!Get the home position check for the motors and update the data
            //!accordingly
            if(TRUE == OIHomeSensor5State())
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
            }
            else
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
            }
            //!Set the data length
            usnDataLength = 2;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD:
        {
            //!If the motor check status is success,
            //! then set the success info in the data field, else set motor
            //! failure status in data filed
            if(g_bMotorChkStatus == TRUE)
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
                g_bMotorChkStatus = FALSE;
            }
            else
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
            }
            //!Get the home position check for the motors and update the data
            //!accordingly
            if(TRUE == OIHomeSensor2State())
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
            }
            else
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
            }
            //!Set the data length
            usnDataLength = 2;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD:
        {
            //!If the motor check status is success,
            //! then set the success info in the data field, else set motor
            //! failure status in data filed
            if(g_bMotorChkStatus == TRUE)
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
                g_bMotorChkStatus = FALSE;
            }
            else
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
            }
            //!Get the home position check for the motors and update the data
            //!accordingly
            if(TRUE == OIHomeSensor1State())
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
            }
            else
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
            }
            //!Set the data length
            usnDataLength = 2;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD:
        {
            //!If the motor check status is success,
            //! then set the success info in the data field, else set motor
            //! failure status in data filed
            if(g_bMotorChkStatus == TRUE)
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;
                g_bMotorChkStatus = FALSE;
            }
            else
            {
                stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;
            }
            //!Get the home position check for the motors and update the data
            //!accordingly
            if(TRUE == OIHomeSensor3State())
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;
            }
            else
            {
                stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;
            }
            //!Set the data length
            usnDataLength = 2;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        //case DELFINO_PRIME_WITH_RINSE_STARTED_CMD:
        //case DELFINO_PRIME_WITH_LYSE_STARTED_CMD:
        //case DELFINO_PRIME_WITH_DILUENT_STARTED_CMD:
        case DELFINO_PRIME_ALL_STARTED_CMD:
        //case DELFINO_BACK_FLUSH_STARTED_CMD:
        //case DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD:
        //case DELFINO_ZAP_APERTURE_STARTED_CMD:
        //case DELFINO_DRAIN_BATH_STARTED_CMD:
        //case DELFINO_DRAIN_ALL_STARTED_CMD:
        //case DELFINO_CLEAN_BATH_STARTED_CMD:
        //case DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD://SKM_COUNTSTART_CHANGE
        case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:
        {
            //!Set the data length
            usnDataLength = 0;
            //!Set the packet count to Zero
            m_usnPacketCount =0;
        }
        break;

        case DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD:
        case DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD:
        case DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD:
        case DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD:
        case DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD:
        {
            //!Set the data length
            usnDataLength = 0;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

#if 0
        //!If the sequence is counting time, get the data and update to the data
        //! field
        case DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD:
        {
            //!Get the counting time for WBC and RBC and update the data length
            usnDataLength = SysInitGetCountingTime(stPtrParsedPacket->arrusnData);
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

        case DELFINO_VALVE_TEST_COMPLETED_CMD:
        {
            //!Set the valve number for which the valve test was performed
            stPtrParsedPacket->arrusnData[0] =  m_ucValveNo;
            //!Reset the valve number
            m_ucValveNo = 0;
            //!Set the data length to one
            usnDataLength = 1;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;

        }
        break;

        case DELFINO_HOME_POSITION_SENSOR_CHECK:
        {
            sprintf(statDebugPrintBuf, " DELFINO_HOME_POSITION_SENSOR_CHECK \n");
            UartDebugPrint((int *)statDebugPrintBuf, 50);
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor4State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnXHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnXHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor5State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnYHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnYHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor2State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnDiluentHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnDiluentHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor1State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnWasteHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnWasteHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!If the motor is at home position, set motor is at home position,
            //! else set motor is not at home position
            if(TRUE == OIHomeSensor3State())
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnSampleHomePosition \
                                                 = MOTOR_HOME_POSITION;
            }
            else
            {
                utMotorHomePositionCheck.stMotorHomePosition.usnSampleHomePosition \
                                                 = MOTOR_NOT_HOME_POSITION;
            }
            //!set Motor home position information to data
            stPtrParsedPacket->arrusnData[0] = utMotorHomePositionCheck.usnMotorHomePosition;
            //!Set the data length
            usnDataLength = 1;
            //!Set the packet count to Zero
            m_usnPacketCount = 0;
        }
        break;

#endif
        default:
        {
            usnDataLength = 0;
            m_usnPacketCount =0;
        }
        break;
    }//end of switch case


    //!Return the data length
    return usnDataLength;
}

/******************************************************************************/
/*!
 *  \fn     CPWriteSTPHDataToPacket()
 *  \brief  Writes the Startup handling data to the SPI Packet Frame
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void CPInitQueue(void)
{
    pstQueue->nFront = -1;
    pstQueue->nRear = - 1;
    memset(pstQueue->SSPIPacketFrame, 0, sizeof(pstQueue->SSPIPacketFrame));
}

/******************************************************************************/
/*!
 *  \fn     CPWriteSTPHDataToPacket()
 *  \brief  To check whether the queue is full
 *  \param  void
 *  \return int
 */
/******************************************************************************/
int CPIsFull(void)
{
    return ((pstQueue->nCurrentSize == MAX_COMMAND_QUEUE_LENGTH));
}

/******************************************************************************/
/*!
 *  \fn     CPIsEmpty()
 *  \brief  To check whether the queue is empty
 *  \param  struct ST_QUEUE* queue
 *  \return int
 */
/******************************************************************************/
int CPIsEmpty(void)
{
    return (pstQueue->nCurrentSize);
}

/******************************************************************************/
/*!
 *  \fn     CPEnqueue()
 *  \brief  To update the queue with the latest command
 *  \param  ST_SPI_PACKET_FRAME *stPacketFrame
 *  \return void
 */
/******************************************************************************/
void CPEnqueue(ST_SPI_PACKET_FRAME *stPacketFrame)
{
    if (CPIsFull())
        return;

    if(0 == pstQueue->nCurrentSize)
    {
        CPInitQueue();
    }
    if(-1 == pstQueue->nFront)
    {
        pstQueue->nFront = 0;
    }

    pstQueue->nRear = pstQueue->nRear+1;
    pstQueue->nCurrentSize = pstQueue->nCurrentSize + 1;
    if(pstQueue->nCurrentSize > MAX_COMMAND_QUEUE_LENGTH)
    {
        pstQueue->nCurrentSize = MAX_COMMAND_QUEUE_LENGTH;
    }
    if(pstQueue->nRear > (pstQueue->nCurrentSize - 1))
    {
        pstQueue->nRear = pstQueue->nCurrentSize - 1;
    }

    memcpy(&pstQueue->SSPIPacketFrame[pstQueue->nRear], stPacketFrame, sizeof(stPacketFrame));
}

/******************************************************************************/
/*!
 *  \fn     CPDequeue()
 *  \brief  To send first command from the queue to the processor
 *  \param  ST_SPI_PACKET_FRAME *stPacketFrame
 *  \return int
 */
/******************************************************************************/
int CPDequeue()
{
    if (CPIsEmpty()||(pstQueue->nFront <= -1))
        return (-1);

    pstQueue->nFront = (pstQueue->nFront + 1);
    pstQueue->nCurrentSize = pstQueue->nCurrentSize - 1;

    if(pstQueue->nCurrentSize < 0 )
    {
        pstQueue->nCurrentSize = 0;
    }
    if(pstQueue->nFront > (pstQueue->nCurrentSize - 1))
    {
        pstQueue->nFront = pstQueue->nCurrentSize - 1;
    }
    memcpy(&m_stPacketForTxToSitara, &pstQueue->SSPIPacketFrame[pstQueue->nFront], \
            sizeof(m_stPacketForTxToSitara));

    return 1;
}

/******************************************************************************/
/*!
 *  \fn     CPSendPacketToSitara()
 *  \brief  To send packet to processor
 *  \param  void
 *  \return int
 */
/******************************************************************************/
void CPSendPacketToSitara(void)
{
    if(!CPIsEmpty())
    {
        CPDequeue();
        bool_t bTxStatus = CPWriteDataToSPI();
        if(FALSE == bTxStatus)
        {
            pstQueue->nFront = (pstQueue->nFront - 1);
            pstQueue->nCurrentSize = pstQueue->nCurrentSize + 1;

            if(pstQueue->nCurrentSize > MAX_COMMAND_QUEUE_LENGTH)
            {
                pstQueue->nCurrentSize = MAX_COMMAND_QUEUE_LENGTH;
            }
        }
    }
}

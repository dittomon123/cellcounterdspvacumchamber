/******************************************************************************/
/*! \file WatchdogTimer.c
 *
 *  \brief Watchdog initialization
 *
 *  \b Description:
 *      Timer configuration and wrapper function for watchdog operations
 *
 *   $Version: $ 0.2
 *   $Date:    $ Jan-21-2015
 *   $Author:  $ 20040919, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_watchdog_module
//! @{
//*****************************************************************************
#include"WatchdogTimer.h"

/******************************************************************************/
/*!
 * \fn void Service_WatchdogTimer(void)
 * \brief watchdog initialization
 * \brief Description:
 *      This function initialize CPU watchdog
 * \return void
 */
/******************************************************************************/
void Service_WatchdogTimer(void)
{
	//!Reset the watchdog counter
	ServiceDog();
}

/******************************************************************************/
/*!
 * \fn void Enable_WatchdogTimer(void)
 * \brief watchdog enable
 * \brief Description:
 *      This function enables CPU watchdog
 * \return void
 */
/******************************************************************************/
void Enable_WatchdogTimer(void)
{
    //!Enable the watchdog for 60ms
    EALLOW;
    //WdRegs.WDCR.all = 0x0028;  //Clock rate to 512/1.
    //WdRegs.WDCR.all = 0x002A;  //Clock rate to 512/2.
    WdRegs.WDCR.all = 0x002B;  //Clock rate to 512/4.
    //WdRegs.WDCR.all = 0x002D; //Clock rate to 512/16.
    //WdRegs.WDCR.all = 0x002F; //Clock rate to 512/64.
    //WdRegs.WDCR.all = 0x002A; //Clock rate to 512/2.
    EDIS;

}
/******************************************************************************/
/*!
 * \fn void Disable_WatchdogTimer(void)
 * \brief watchdog disable
 * \brief Description:
 *      This function disables CPU watchdog
 * \return void
 */
/******************************************************************************/
void Disable_WatchdogTimer(void)
{
	//!Disable the watchdog
	DisableDog();
}
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

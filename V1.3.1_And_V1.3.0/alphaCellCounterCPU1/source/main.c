 /*****************************************************************************/
/*! \file main.c
 *
 *  \brief Main function of Cell counter module
 *
 *  \b Description:
 *			This file contains, initialization of CPU, CPU peripherals,
 *			MBD module and Algorithm
 *
 *   $Version: $ 0.1
 *   $Date:    $ Jan-12-2015
 *   $Author:  $ 20040919, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_main_module
//! @{
//*****************************************************************************

#include "F28x_Project.h"     // Device Header file and Examples Include File
#include "SystemInitialize.h"
#include "CommInterface.h"
extern char DebugPrintBuf[50];
Uint32 g_unSequenceStateTime=0;

Uint32 g_unSequenceStateTime1=0;
extern uint16_t Counter;

uint16_t g_unSysHealthMonCount = 0;

uint16_t g_unSPIErrorCheckCounter = 1;

uint16_t g_untimerFor20ms = 0;



/******************************************************************************/
/*!
 * \fn void main(void)
 * \brief main function of cell counter
 * \brief Description:
 *      This function calls initialization functions and sequence of operations
 *      based on cpu timer
 *
 * \return void
 */
/******************************************************************************/
void main(void)
{
    //!SysSystem_Initalize() function initialize CPU peripherals and application
    //!modules
    SysSystem_Initalize();

    //!BloodCellCounter_initialize() function initialize all the variables of MBD
	BloodCellCounter_initialize();

	//!GPIO used for toggling the LED1 is used for Controlling resistor
    //! R150 bypassing
    //! This has to be reverted after revising the board
    IGConfigHighVoltageModule(LED1_GPIO, 0);

	//!1 Second delay is introduces so that the process runs after proper and
	//!complete initialization of peripherals
	DELAY_US(1000000);

	Enable_WatchdogTimer();

	while(1)
	{
	    //!The timer is configured to 5mSec.
	    if(usTimerFlag == 1)
	    {
	        //!Reset the timer flag
	        usTimerFlag = 0;

	        g_untimerFor20ms++;

	        //!Acquire the ADC health status
	        ADAdcAcquireHealthStatus();
	        //!Get the System health status from ADC
	        SysInitGetSystemStatus();
	    }
	    //!The timer is configured to 20mSec.
	    //!Each cycle does not exceed 20mSec.
	    //!If the cycle exceeds greater than 20mSec, then either the current
	    //!execution of activities in 20mSec or the next cycle shall be affected
	    if(4 == g_untimerFor20ms)
		{
		    //!SysRT_OneStep() function perform the sequence of operation for
		    //!blood data analysis and motor control
		    SysRT_OneStep();

		    //!Reset the timer flag
		    //!If the execution of the function SysRT_OneStep() finishes in less
		    //!than 20mSec, the cycle shall continue only for the next 20mSec

		    g_untimerFor20ms = 0;

			//!g_unSequenceStateTime maintains the count of the execution time or the
			//!can be used to count no. of 20mSec execution occurred
			g_unSequenceStateTime++;

			g_unSequenceStateTime1++;

			//!System health status to be monitored every 60ms
			g_unSysHealthMonCount++;

			//!SPI Error check counter to be incremented
			g_unSPIErrorCheckCounter++;

			Counter++;

			//!Reset the Watchdog timer so that i will not reach max
			ServiceDog();
		}

		//!SPICommWrite() function to write the data to DMA and SPI buffer
		SPICommWrite();

		//!SPICommRead() function to read the data from DMA and SPI buffer
		SPICommRead();

		//!SPIDmaTxRx() function to decode the received data and to start the
		//!receive DMA
		SPIDmaTxRx();
	}
}
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

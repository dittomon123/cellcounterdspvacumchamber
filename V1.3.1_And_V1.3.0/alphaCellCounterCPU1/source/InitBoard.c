/******************************************************************************/
/*! \file InitBoard.c
 *
 *  \brief CPU initialization file.
 *
 *  \b Description:
 *      Initializaton of Vector table, Global Interrupts, System Clock,
 *      Timers & Watchdog configuration
 *
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-23-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_cpu_initialization_module
//! @{
//*****************************************************************************
#include "InitBoard.h"
#include "SpiComm.h"
#include "F2837xD_Ipc_drivers.h"
#include "Timer.h"

#define JTAG_DEBUG_MODE_ENABLE								FALSE
#define IO_EXPANDER_USED									FALSE
//!variables Declaration
/******************************************************************************/
/*!
 *
 * \var Uint32 unWakeCount
 * \var Uint16 usTimerFlag
 *
 */
/******************************************************************************/
Uint32 unWakeCount;
Uint16 usTimerFlag;
extern char DebugPrintBuf[50];
extern Uint32 g_unSequenceStateTime;
/******************************************************************************/
/*!
 * \fn void IBInitialize_Board(void)
 * \brief CPU initialization
 * \brief  Description:
 *      This function initialize System Clock & Vector table
 *		Calls InitPieCtrl(), InitPieVectTable()
 * \return void
 */
/******************************************************************************/
void IBInitialize_Board(void)
{
	InitSysCtrl();

#if !JTAG_DEBUG_MODE_ENABLE
#ifdef _STANDALONE
#ifdef _FLASH
// Send boot command to allow the CPU2 application to begin execution
IPCBootCPU2(C1C2_BROM_BOOTMODE_BOOT_FROM_FLASH);
#else
// Send boot command to allow the CPU2 application to begin execution
IPCBootCPU2(C1C2_BROM_BOOTMODE_BOOT_FROM_RAM);
#endif
#endif
#endif
	//SysCtlClockSet(SYSCTL_OSCSRC_XTAL | SYSCTL_PLL_ENABLE | SYSCTL_IMULT(40) \
//| SYSCTL_SYSDIV(2));
// Call Flash Initialization to setup flash waitstates
// This function must reside in RAM
#ifdef _FLASH
   InitFlash();
#endif
	//! Disable CPU __interrupts
	DINT;

	//! This function is found in the F2837xD_PieCtrl.c file.
	InitPieCtrl();

	//! Disable CPU __interrupts and clear all CPU __interrupt flags:
	IER = 0x0000;
	IFR = 0x0000;

	//! Initialize the PIE vector table with pointers to the shell Interrupt
	//! Service Routines (ISR).
	//! This will populate the entire table, even if the __interrupt
	//! is not used in this example.  This is useful for debug purposes.
	//! The shell ISR routines are found in F2837xD_DefaultIsr.c.
	//! This function is found in F2837xD_PieVect.c.
	InitPieVectTable();

}


/******************************************************************************/
/*!
 * \fn void IBTimer_Interrupt(void)
 * \brief Timer initialization
 * \brief  Description:
 *      This function initialize timer
 *
 * \return void
 */
/******************************************************************************/
void IBTimer_Interrupt(void)
{
	//! Interrupts that are used in this example are re-mapped to
	//! ISR functions found within this file.
	EALLOW;  //! This is needed to write to EALLOW protected registers
	PieVectTable.TIMER0_INT = &cpu_timer0_isr;
	PieVectTable.TIMER1_INT = &cpu_timer1_isr;
	//PieVectTable.TIMER2_INT = &cpu_timer2_isr;
	EDIS;    //! This is needed to disable write to EALLOW protected registers

	//! Enable CPU int1 which is connected to CPU-Timer 0, CPU int13
	//! which is connected to CPU-Timer 1, and CPU int 14, which is connected
	//! to CPU-Timer 2:
	IER |= M_INT1;
	IER |= M_INT13;
//	IER |= M_INT14;

	//! Enable TINT0 in the PIE: Group 1 interrupt 7
	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;

	//! Enable global Interrupts and higher priority real-time debug events:
	EINT;  //! Enable Global interrupt INTM
	ERTM;  //! Enable Global realtime interrupt DBGM*/

	Initialize_Timer();
}


/******************************************************************************/
/*!
 * \fn void IBWatchDog_Timer_Interrupt()
 * \brief Watchdog initialization
 * \brief  Description:
 *      This function initialize watchdog timer
 *
 * \return void
 */
/******************************************************************************/
void IBWatchDog_Timer_Interrupt(void)
{
	//! Interrupts that are used in this example are re-mapped to
	//! ISR functions found within this file.
	EALLOW;  //! This is needed to write to EALLOW protected registers
	PieVectTable.WAKE_INT = &wakeint_isr;
	EDIS;    //! This is needed to disable write to EALLOW protected registers

	//! Connect the watchdog to the WAKEINT interrupt of the PIE
	//! Write to the whole SCSR register to avoid clearing WDOVERRIDE bit
	EALLOW;
	WdRegs.SCSR.all = BIT1;
	EDIS;

	//! Enable WAKEINT in the PIE: Group 1 interrupt 8
	//! Enable INT1 which is connected to WAKEINT:
	PieCtrlRegs.PIECTRL.bit.ENPIE = 1;   //! Enable the PIE block
	PieCtrlRegs.PIEIER1.bit.INTx8 = 1;   //! Enable PIE Gropu 1 INT8
	IER |= M_INT1;                       //! Enable CPU INT1
	EINT;                                //! Enable Global Interrupts


}


/******************************************************************************/
/*!
 * \fn __interrupt void cpu_timer0_isr()
 * \brief CPU timer 0 interrupt handler
 * \brief  Description:
 *      CPU timer 0 interrupt handler configured in uSec
 *
 * \return void
 */
/******************************************************************************/
__interrupt void cpu_timer0_isr(void)
{
    //!Increment the interrupt counter
    CpuTimer0.InterruptCount++;

    //!Set the interrupt flag
    usTimerFlag = 1;

   /* EALLOW;
	GpioCtrlRegs.GPCPUD.bit.GPIO70 = 0;   //! Enable pullup on GPIO6
	GpioDataRegs.GPCTOGGLE.bit.GPIO70= 1;   //! Load output latch
	GpioCtrlRegs.GPCMUX1.bit.GPIO70 = 0;  //! GPIO6 = GPIO6
	GpioCtrlRegs.GPCDIR.bit.GPIO70 =  1;   //! GPIO6 = output
	EDIS;*/

   //! Acknowledge this interrupt to receive more interrupts from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}


/******************************************************************************/
/*!
 * \fn __interrupt void cpu_timer1_isr()
 * \brief CPU timer 0 interrupt handler
 * \brief  Description:
 *      CPU timer 1 interrupt handler configured in uSec
 * \return void
 */
/******************************************************************************/
#if 1
__interrupt void cpu_timer1_isr(void)
{
    //!Increment the CPU1 interrupt counter
    CpuTimer1.InterruptCount++;

   //! Acknowledge this interrupt to receive more interrupts from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
#endif


/******************************************************************************/
/*!
 * \fn __interrupt void wakeint_isr(void)
 * \brief CPU watchdog interrupt handler
 * \brief  Description:
 *      CPU watchdog interrupt handler
 *
 * \return void
 */
/******************************************************************************/
__interrupt void wakeint_isr(void)
{
    unWakeCount++;

    sprintf(DebugPrintBuf, "\n\r #### WatchDog_isr:%ld ####\n",g_unSequenceStateTime);
    UartDebugPrint((int *)DebugPrintBuf, 50);

    ServiceDog();
    //!Reset the watchdog core and set the watchdog timer to 60ms
    EALLOW;
    //WdRegs.WDCR.all = 0x0080;  // 512/1
    //WdRegs.WDCR.all = 0x00AA;  // 512/2
    WdRegs.WDCR.all = 0x00AB;  // 512/4
    //WdRegs.WDCR.all = 0x00AD; // 512/16
    //WdRegs.WDCR.all = 0x00AF; // 512/64

    //WdRegs.WDCR.all = 0x0085;
    EDIS;

    //! Acknowledge this interrupt to get more from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

    g_unSequenceStateTime = 0;
}


/******************************************************************************/
/*!
 * \fn Uint16 IBGetInterruptRegStatus(void)
 * \brief CPU IER register status
 * \brief  Description:
 *      This function returns status of IER register
 *
 * \return Uint16 IER interrupt rester status
 */
/******************************************************************************/
Uint16 IBGetInterruptRegStatus()
{
    //!Return IER register status
	return IER;
}


/******************************************************************************/
/*!
 * \fn void IBSetInterruptRegStatus(void)
 * \param Uint16 InterruptReg
 * \brief CPU IER register status
 * \brief  Description:
 *      This function set IER register
 *
 * \return Uint16 IER
 */
/******************************************************************************/
void IBSetInterruptRegStatus(Uint16 InterruptReg)
{
    //!Set IER register
	IER = InterruptReg;
}

//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

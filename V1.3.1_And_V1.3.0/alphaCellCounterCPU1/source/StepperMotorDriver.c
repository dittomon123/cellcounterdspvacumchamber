/******************************************************************************/
/*! \file StepperMotorDriver.c
 *
 *  \brief Stepper motor Driver module initialization and control
 *
 *  \b Description:
 *      Here Stepper motor Driver module initialization by configuration
 *      of GPIO's and PWM channels for 5 Stepper motor driver modules
 *      Driver module initialization contains, Motor Direction, Mode of
 *      operation, Decay, Step size, fault check, motor enable and
 *      Step frequency for motor through pwm
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-25-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_motor_module
//! @{
//******************************************************************************

#include <stdio.h>
#include "StepperMotorDriver.h"
#include "MotorConfig.h"
#include "InitGpio.h"
#include "UartDebug.h"
#include "commands.h"
extern uint16_t g_usnCurrentMode;
extern bool_t m_bXAxisMonEnable;
extern bool_t m_bDiluentSyringeMonEnable;
extern bool_t m_bSampleSyringeMonEnable;
extern bool_t m_bWasteSyringeMonEnable;
extern bool_t m_bYAxisMonEnable;
extern Uint32 g_unSequenceStateTime1;
extern char DebugPrintBuf[50];
extern Uint32 g_unSequenceStateTime;
extern bool_t m_bBubblingMovEnFlag;
extern uint16_t g_usnCurrentModeMajor;
extern uint16_t g_TestDataI2;
/******************************************************************************/
bool_t g_bLowVolumeDisp = FALSE;
/******************************************************************************/
/*!
 * \fn void SMDInitialize_MotorGpio(void)
 *
 *
 * \brief Initialize the Motor GPIO
 * \b Description:
 *      This function configures gpio mux and resgistery for Motor GPIO pin
 *
 * \return void
 */
/******************************************************************************/
void SMDInitialize_MotorGpio(void)
{
    //!Configure the motor reset GPIO
    IGGpioConfig_MotorReset();

    //!Configure the motor1 control GPIOs
    IGGpioConfig_Motor1();

    //!Configure the motor2 control GPIOs
    IGGpioConfig_Motor2();

    //!Configure the motor3 control GPIOs
    IGGpioConfig_Motor3();

    //!Configure the motor4 control GPIOs
    IGGpioConfig_Motor4();

    //!Configure the motor5 control GPIOs
    IGGpioConfig_Motor5();

    //!Configure the default state of the motor control GPIOs
	SMDGpioConfig_MotorDefaultState();
}
/******************************************************************************/
/*!
 * \fn void SMDGpioConfig_MotorDefaultState(void)
 *
 *
 * \brief Initialize the Motor GPIO default state
 * \b Description:
 *      This function configures for Motor GPIO pin to default state
 *
 * \return void
 */
/******************************************************************************/
void SMDGpioConfig_MotorDefaultState(void)
{
    //!Reset the Motor 1
	SMDMotorReset();
	//!Enable Motor 1
	SMDSetMotorEnable(MOTOR1_SELECT);
	//!Set the default step size of Motor 1
	SMDSetMotorStepSize(MOTOR1_SELECT, FULL_STEP);
	//!Set the motor 1 default direction
	SMDSetMotorDirection(MOTOR1_SELECT, M1_DIR_DEFAULT);
	//!Set motor 1 decay
	SMDSetMotorDecay(MOTOR1_SELECT, M1_DECAY_DEFAULT);
	//!Disable the motor 1 by default
	SMDSetMotorDisable(MOTOR1_SELECT);
	//!Configure the PWM of the motor 1
	PWMConfigMotor1Param(1, M1_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);

	//!Reset the Motor
	SMDMotorReset();
	//!Enable Motor 2
	SMDSetMotorEnable(MOTOR2_SELECT);
	//!Set the default step size of Motor 2
	SMDSetMotorStepSize(MOTOR2_SELECT, FULL_STEP);
	//!Set the motor 2 default direction
	SMDSetMotorDirection(MOTOR2_SELECT, M2_DIR_DEFAULT);
	//!Set motor 2 decay
	SMDSetMotorDecay(MOTOR2_SELECT, M2_DECAY_DEFAULT);
	//!Disable the motor 2 by default
	SMDSetMotorDisable(MOTOR2_SELECT);
	//!Configure the PWM of the motor 2
	PWMConfigMotor2Param(1, M2_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);

	//!Reset the Motor
	SMDMotorReset();
	//!Enable Motor 3
	SMDSetMotorEnable(MOTOR3_SELECT);
	//!Set the default step size of Motor 3
	SMDSetMotorStepSize(MOTOR3_SELECT, FULL_STEP);
	//!Set the motor 3 default direction
	SMDSetMotorDirection(MOTOR3_SELECT, M3_DIR_DEFAULT);
	//!Set motor 3 decay
	SMDSetMotorDecay(MOTOR3_SELECT, M3_DECAY_DEFAULT);
	//!Disable the motor 3 by default
	SMDSetMotorDisable(MOTOR3_SELECT);
	//!Configure the PWM of the motor 3
	PWMConfigMotor3Param(1, M3_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);

	//!Reset the Motor
	SMDMotorReset();
	//!Enable Motor 4
	SMDSetMotorEnable(MOTOR4_SELECT);
	//!Set the default step size of Motor 4
	SMDSetMotorStepSize(MOTOR4_SELECT, FULL_STEP);
	//!Set the motor 4 default direction
	SMDSetMotorDirection(MOTOR4_SELECT, M4_DIR_DEFAULT);
	//!Set motor 4 decay
	SMDSetMotorDecay(MOTOR4_SELECT, M4_DECAY_DEFAULT);
	//!Disable the motor 4 by default
	SMDSetMotorDisable(MOTOR4_SELECT);
	//!Configure the PWM of the motor 4
	PWMConfigMotor4Param(1, M4_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);

	//!Reset the Motor
	SMDMotorReset();
	//!Enable Motor 5
	SMDSetMotorEnable(MOTOR5_SELECT);
	//!Set the default step size of Motor 5
	SMDSetMotorStepSize(MOTOR5_SELECT, FULL_STEP);
	//!Set the motor 5 default direction
	SMDSetMotorDirection(MOTOR5_SELECT, M5_DIR_DEFAULT);
	//!Set motor 5 decay
	SMDSetMotorDecay(MOTOR5_SELECT, M5_DECAY_DEFAULT);
	//!Disable the motor 5 by default
	SMDSetMotorDisable(MOTOR5_SELECT);
	//!Configure the PWM of the motor 5
	PWMConfigMotor5Param(1, M5_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);

}
/******************************************************************************/
/*!
 * \fn void SMDSetMotorStepSize(Uint16 Motor, Uint16 StepSize)
 * \param Motor motor number
 * \param StepSize Step size to be configured
 * \brief Write motor step size
 * \b Description:
 *      This function configures motor step size
 *
 * \return void
 */
/******************************************************************************/
void SMDSetMotorStepSize(Uint16 Motor, Uint16 Step)
{
	Uint16 StepSize[3];
	//!Configure the step size to operate the motor based on the step size selected
	switch(Step)
	{
	case FULL_STEP:
		StepSize[0] = FULLL_STEP_MODE0;
		StepSize[1] = FULLL_STEP_MODE1;
		StepSize[2] = FULLL_STEP_MODE2;
		break;

	case TWO_MICRO_STEP:
		StepSize[0] = TWO_MICRO_STEP_MODE0;
		StepSize[1] = TWO_MICRO_STEP_MODE1;
		StepSize[2] = TWO_MICRO_STEP_MODE2;
		break;

	case FOUR_MICRO_STEP:
		StepSize[0] = FOUR_MICRO_STEP_MODE0;
		StepSize[1] = FOUR_MICRO_STEP_MODE1;
		StepSize[2] = FOUR_MICRO_STEP_MODE2;
		break;

	case EIGHT_MICRO_STEP:
		StepSize[0] = EIGHT_MICRO_STEP_MODE0;
		StepSize[1] = EIGHT_MICRO_STEP_MODE1;
		StepSize[2] = EIGHT_MICRO_STEP_MODE2;
		break;


	case SIXTEEN_MICRO_STEP:
		StepSize[0] = SIXTEEN_MICRO_STEP_MODE0;
		StepSize[1] = SIXTEEN_MICRO_STEP_MODE1;
		StepSize[2] = SIXTEEN_MICRO_STEP_MODE2;
		break;

	case THIRTYTWO_MICRO_STEP:
		StepSize[0] = THIRTYTWO_MICRO_STEP_MODE0;
		StepSize[1] = THIRTYTWO_MICRO_STEP_MODE1;
		StepSize[2] = THIRTYTWO_MICRO_STEP_MODE2;
		break;

	default:
		StepSize[0] = FULLL_STEP_MODE0;
		StepSize[1] = FULLL_STEP_MODE1;
		StepSize[2] = FULLL_STEP_MODE2;
		break;

	}

	//!Set the GPIO and for specific modes for the motor selected
	switch(Motor)
	{
	case MOTOR1_SELECT:
		IGWriteMotorGpio(M1_MODE0, StepSize[0]);
		IGWriteMotorGpio(M1_MODE1, StepSize[1]);
		IGWriteMotorGpio(M1_MODE2, StepSize[2]);
		break;

	case MOTOR2_SELECT:
		IGWriteMotorGpio(M2_MODE0, StepSize[0]);
		IGWriteMotorGpio(M2_MODE1, StepSize[1]);
		IGWriteMotorGpio(M2_MODE2, StepSize[2]);
		break;

	case MOTOR3_SELECT:
		IGWriteMotorGpio(M3_MODE0, StepSize[0]);
		IGWriteMotorGpio(M3_MODE1, StepSize[1]);
		IGWriteMotorGpio(M3_MODE2, StepSize[2]);
		break;

	case MOTOR4_SELECT:
		IGWriteMotorGpio(M4_MODE0, StepSize[0]);
		IGWriteMotorGpio(M4_MODE1, StepSize[1]);
		IGWriteMotorGpio(M4_MODE2, StepSize[2]);
		break;

	case MOTOR5_SELECT:
		IGWriteMotorGpio(M5_MODE0, StepSize[0]);
		IGWriteMotorGpio(M5_MODE1, StepSize[1]);
		IGWriteMotorGpio(M5_MODE2, StepSize[2]);
		break;

	default:
		break;
	}
}
/******************************************************************************/
/*!
 * \fn void SMDSetMotorEnable(Uint16 Motor)
 * \param Motor motor number
 *
 * \brief Write motor enable
 * \b Description:
 *      This function configures enable gpio pin to enable the motor
 *
 * \return void
 */
/******************************************************************************/
void SMDSetMotorEnable(Uint16 Motor)
{
    //!Enbale the motor based on the motor selected
	switch(Motor)
	{
	case 1:
	    //!Enable motor 1
		IGWriteMotorGpio(M1_IN1, MOTOR_ENABLE);
		IGWriteMotorGpio(M1_IN2, 1);
		break;

	case 2:
	    //!Enable motor 2
		IGWriteMotorGpio(M2_ENBL, MOTOR_ENABLE);
		break;

	case 3:
	    //!Enable motor 3
		IGWriteMotorGpio(M3_ENBL, MOTOR_ENABLE);
		break;

	case 4:
	    //!Enable motor 4
		IGWriteMotorGpio(M4_ENBL, MOTOR_ENABLE);
		break;

	case 5:
	    //!Enable motor 5
		IGWriteMotorGpio(M5_ENBL, MOTOR_ENABLE);

	default:
		break;
	}
}
/******************************************************************************/
/*!
 * \fn void SMDSetMotorDisable(Uint16 Motor)
 * \param Motor motor number
 *
 * \brief Write motor disable
 * \b Description:
 *      This function configures enable gpio pin to disable the motor
 *
 * \return void
 */
/******************************************************************************/
void SMDSetMotorDisable(Uint16 Motor)
{
    //!Disable the motor based on the motor selected
	switch(Motor)
	{
	case MOTOR1_SELECT:
	    //!Disable motor 1
		IGWriteMotorGpio(M1_IN1, 0);
		IGWriteMotorGpio(M1_IN2, 0);
		GPIO_WritePin(CYCLE_COUNTER_GPIO_OUTPUT1,0);
		break;

	case MOTOR2_SELECT:
	    //!Disable motor 2
		IGWriteMotorGpio(M2_ENBL, MOTOR_DISABLE);
		break;

	case MOTOR3_SELECT:
	    //!Disable motor 3
		IGWriteMotorGpio(M3_ENBL, MOTOR_DISABLE);
		break;

	case MOTOR4_SELECT:
	    //!Disable motor 4
		IGWriteMotorGpio(M4_ENBL, MOTOR_DISABLE);
		break;

	case MOTOR5_SELECT:
	    //!Disable motor 5
		IGWriteMotorGpio(M5_ENBL, MOTOR_DISABLE);

	default:
		break;
	}
}
/******************************************************************************/
/*!
 * \fn void SMDSetMotorDirection(Uint16 Motor)
 * \param Motor motor number
 * \param Direction specifies the direction of the motor
 * \brief Write motor direction setting
 * \b Description:
 *      This function configures diection of gpio pin of the motor
 *
 * \return void
 */
/******************************************************************************/
void SMDSetMotorDirection(Uint16 Motor, Uint16 Direction)
{
    //!Set the motor direction based on the motor selected
	switch(Motor)
	{
	case MOTOR1_SELECT:
	    //!Set the direction of motor 1
		IGWriteMotorGpio(M1_DIR, Direction);
		break;

	case MOTOR2_SELECT:
	    //!Set the direction of motor 2
		IGWriteMotorGpio(M2_DIR, Direction);
		break;

	case MOTOR3_SELECT:
	    //!Set the direction of motor 3
		IGWriteMotorGpio(M3_DIR, Direction);
		break;

	case MOTOR4_SELECT:
	    //!Set the direction of motor 4
		IGWriteMotorGpio(M4_DIR, (int)(!Direction));  //QA_C
		break;

	case MOTOR5_SELECT:
	    //!Set the direction of motor 5
		IGWriteMotorGpio(M5_DIR, Direction);

	default:
		break;
	}
}
/******************************************************************************/
/*!
 * \fn void SMDSetMotorDecay(Uint16 Motor, Uint16 Decay)
 * \param Motor motor number
 * \param Decay decay type
 * \brief Write motor direction setting
 * \b Description:
 *      This function configures decay of gpio pin of the motor
 *
 * \return void
 */
/******************************************************************************/
void SMDSetMotorDecay(Uint16 Motor, Uint16 Decay)
{
    //!Set the decay state of the motor selected
	switch(Motor)
	{
	case MOTOR1_SELECT:
	    //!Set the decay state of motor 1
		IGWriteMotorGpio(M1_DECAY, Decay);
		break;

	case MOTOR2_SELECT:
	    //!Set the decay state of motor 2
		IGWriteMotorGpio(M2_DECAY, Decay);
		break;

	case MOTOR3_SELECT:
	    //!Set the decay state of motor 3
		IGWriteMotorGpio(M3_DECAY, Decay);
		break;

	case MOTOR4_SELECT:
	    //!Set the decay state of motor 4
		IGWriteMotorGpio(M4_DECAY, Decay);
		break;

	case MOTOR5_SELECT:
	    //!Set the decay state of motor 5
		IGWriteMotorGpio(M5_DECAY, Decay);

	default:
		break;
	}
}
/******************************************************************************/
/*!
 * \fn Uint16 SMDGetMotorFault(Uint16 Motor)
 * \param Motor motor number
 *
 * \brief Write motor direction setting
 * \b Description:
 *      This function configures decay of gpio pin of the motor
 *
 * \return void
 */
/******************************************************************************/
Uint16 SMDGetMotorFault(Uint16 Motor)
{
	Uint16 FaultStatus=0x00;
	//!Get the motor fault status based on the motor selected
	switch(Motor)
	{
	case MOTOR1_SELECT:
	    //!Get the fault status of motor 1
		FaultStatus = IGReadMotorGpio(M1_FAULT);
		break;

	case MOTOR2_SELECT:
	    //!Get the fault status of motor 2
		FaultStatus = IGReadMotorGpio(M2_FAULT);
		break;

	case MOTOR3_SELECT:
	    //!Get the fault status of motor 3
		FaultStatus = IGReadMotorGpio(M3_FAULT);
		break;

	case MOTOR4_SELECT:
	    //!Get the fault status of motor 4
		FaultStatus = IGReadMotorGpio(M4_FAULT);
		break;

	case MOTOR5_SELECT:
	    //!Get the fault status of motor 5
		FaultStatus = IGReadMotorGpio(M5_FAULT);

	default:
		break;
	}
	//!Return the fault status
	return FaultStatus;
}
/******************************************************************************/
/*!
 * \fn void SMDMotorReset(void)
 * \brief Write motor reset
 * \b Description:
 *      This function resets the Motor
 *
 * \return void
 */
/******************************************************************************/
void SMDMotorReset(void)
{
	IGWriteMotorGpio(M_RESET, !M_RESET_DEFAULT);
	DELAY_US(10);
	IGWriteMotorGpio(M_RESET, M_RESET_DEFAULT);
}

//-----------------------------Motor operation--------------------------------//
/******************************************************************************/
/*!
 * \fn void SMDStart_Motor1(unsigned int  , unsigned int , unsigned int )
 * \param usnDisplacement specifies the displacement parameter for the
 *          motor configuration
 * \param usnDirection specifies the direction parameter for the
 *          motor configuration
 * \param StepFrequency specifies the frequency parameter for the
 *          motor configuration
 *
 * \brief To start motor 1
 * \brief  Description:
 *      This function start motor 1 using Displacement, Direction and Frequency
 *      parameters
 *      Calls Motor enable, Motor Direction & ConfigMotor1PWM parameters
 *      PWM frequency/Period shall be used to start & Stop motor during runtime
 *  *
 * \return void
 */
/******************************************************************************/
void SMDStart_Motor1(unsigned int  usnDisplacement, unsigned int usnDirection, \
        unsigned int StepFrequency)
{
    //char DebugPrintBuf[50];
/*	if(PWMGetMotor1Status() == IDLE)
	{

#if MOTOR_DEBUG_PRINT_ENABLE
		printf(" M1 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\
		        usnDirection,StepFrequency);
#endif

        //!Enable to monitor the Waste syringe motor movement
        m_bWasteSyringeMonEnable = TRUE;

        if(StepFrequency > 100 && StepFrequency < 300)
        {
            m_bBubblingMovEnFlag = true;
        }
        else
        {
            m_bBubblingMovEnFlag = false;
        }
*/
/*        if(SITARA_PATIENT_HANDLING != g_usnCurrentModeMajor)
        {
            if(usnDisplacement >= 6000)
            {
                if(StepFrequency > 500)
                {
                    StepFrequency = 500;
                }
            }
        }*/
/*		//!Set the motor step size
		SMDSetMotorStepSize(MOTOR1_SELECT, MOTOR1_STEP_CONFIGURATION);
		//!Enable motor
		SMDSetMotorEnable(MOTOR1_SELECT);
		//!Set the motor direction
		SMDSetMotorDirection(MOTOR1_SELECT, usnDirection);
//			BloodCellCounter_U.HP_YAxis = IDLE;

		//!Configure the pwm to start the motor
		PWMConfigMotor1Param(usnDisplacement, usnDirection, StepFrequency);
	}
	*/

    if(usnDirection == 1)
    {
    IGWriteMotorGpio(M1_MODE0, 1);
    IGWriteMotorGpio(M1_MODE1, 1);
    IGWriteMotorGpio(M1_MODE2, 1);
    SMDSetMotorEnable(MOTOR1_SELECT);
    }
    else
    {
        GPIO_WritePin(CYCLE_COUNTER_GPIO_OUTPUT1,1);
        //PWMControl(PWM8A, START, 3000);
    }
}
/******************************************************************************/
/*!
 * \fn void SMDStart_Motor2(unsigned int  , unsigned int , unsigned int )
 * \param usnDisplacement specifies the displacement parameter for the
 *          motor configuration
 * \param usnDirection specifies the direction parameter for the
 *          motor configuration
 * \param StepFrequency specifies the frequency parameter for the
 *          motor configuration
 *
 * \brief To start motor 2
 * \brief  Description:
 *      This function start motor 2 using Displacement, Direction and Frequency
 *      parameters
 *      Calls Motor enable, Motor Direction & ConfigMotor2PWM parameters
 *      PWM frequency/Period shall be used to start & Stop motor during runtime
 *  *
 * \return void
 */
/******************************************************************************/
void SMDStart_Motor2(unsigned int  usnDisplacement, unsigned int usnDirection, \
        unsigned int StepFrequency)
{
    //! 650Hz - There is pressure build up when the displacement is small
    //! The syringe body moves due to high pressure which will impact the reliability of the syringe and motor
    int maxstepFreq = 550;
    //char DebugPrintBuf[50];
	if(PWMGetMotor2Status() == IDLE)
	{
#if MOTOR_DEBUG_PRINT_ENABLE
		printf(" M2 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\
		        usnDirection,StepFrequency);
#endif
		//!If the motor is moving towards home, then check the frq of the motor
		if(usnDirection == MOTOR2_HOME_DIRECTION)
		{
		    //!If the frq is more then 550Hz, set it to 550Hz.
            if(StepFrequency > maxstepFreq)
            {
                StepFrequency = maxstepFreq;
            }

            if(usnDisplacement <= 340)
            //if(usnDisplacement < 300)
            {
                StepFrequency = 170;
                //! If the sequence is pre-diluent then make g_bLowVolumeDisp variable to true
                //! so that we will not apply sCurve for that movement.
                if(g_bLowVolumeDisp)
                {
                    StepFrequency = 50;
                    sprintf(DebugPrintBuf, "\n g_bLowVolumeDisp \n");
                    UartDebugPrint((int *)DebugPrintBuf, 50);
                }
            }

        }
		else
		{
		    //!If the frequency is > 100 Hz, motor is not moving smoothly
		    //! so this condition got introduced to move motor smoothly.
		    if(StepFrequency > 900)
            {
              StepFrequency = 900;
            }
		    //! This is for sucking the Drop let at the tip of the needle by
		    //! using diluent syringe moving down 0.25mm with 50hz frq and without sCurve
		    if(g_bLowVolumeDisp)
		    {
		        StepFrequency = 50;
		    }

		}

		//!Enable to monitor the Diluent syringe motor movement
		m_bDiluentSyringeMonEnable = TRUE;

		//!Set the motor step size
		SMDSetMotorStepSize(MOTOR2_SELECT, MOTOR2_STEP_CONFIGURATION);
		//!Enable motor
		SMDSetMotorEnable(MOTOR2_SELECT);
		//!Set the motor direction
		SMDSetMotorDirection(MOTOR2_SELECT, usnDirection);
		BloodCellCounter_U.HomeStatus[0] = IDLE;
		//BloodCellCounter_U.HP_YAxis = IDLE;
		//!Configure the pwm to start the motor
		PWMConfigMotor2Param(usnDisplacement, usnDirection, StepFrequency);
	}
}

/******************************************************************************/
/*!
 * \fn void SMDStart_Motor3(unsigned int  , unsigned int , unsigned int )
 * \param usnDisplacement specifies the displacement parameter for
 *          the motor configuration
 * \param usnDirection specifies the direction parameter for
 *          the motor configuration
 * \param StepFrequency specifies the frequency parameter for
 *          the motor configuration
 *
 * \brief To start motor 3
 * \brief  Description:
 *      This function start motor 3 using Displacement, Direction and Frequency
 *      parameters
 *      Calls Motor enable, Motor Direction & ConfigMotor3PWM parameters
 *      PWM frequency/Period shall be used to start & Stop motor during runtime
 *  *
 * \return void
 */
/******************************************************************************/
void SMDStart_Motor3(unsigned int  usnDisplacement, unsigned int usnDirection, \
        unsigned int StepFrequency)
{
    //char DebugPrintBuf[50];
	if(PWMGetMotor3Status() == IDLE)
	{
#if MOTOR_DEBUG_PRINT_ENABLE
		printf(" M3 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\
		        usnDirection,StepFrequency);
#endif

		//!Enable to monitor the Sample syringe motor movement
		m_bSampleSyringeMonEnable = TRUE;


		//!Set the motor step size
		SMDSetMotorStepSize(MOTOR3_SELECT, MOTOR3_STEP_CONFIGURATION);
		//!Enable motor
		SMDSetMotorEnable(MOTOR3_SELECT);
		//!Set the motor direction
		SMDSetMotorDirection(MOTOR3_SELECT, usnDirection);
		//!Configure the pwm to start the motor
		PWMConfigMotor3Param(usnDisplacement, usnDirection, StepFrequency);
	}
}
/******************************************************************************/
/*!
 * \fn void SMDStart_Motor4(unsigned int  , unsigned int , unsigned int )
 * \param usnDisplacement specifies the displacement parameter for
 *          the motor configuration
 * \param usnDirection specifies the direction parameter for
 *          the motor configuration
 * \param StepFrequency specifies the frequency parameter for
 *          the motor configuration
 *
 * \brief To start motor 4
 * \brief  Description:
 *      This function start motor 4 using Displacement, Direction and Frequency
 *      parameters
 *      Calls Motor enable, Motor Direction & ConfigMotor4PWM parameters
 *      PWM frequency/Period shall be used to start & Stop motor during runtime
 *  *
 * \return void
 */
/******************************************************************************/
void SMDStart_Motor4(unsigned int  usnDisplacement, unsigned int usnDirection, \
        unsigned int StepFrequency)
{
	if(PWMGetMotor4Status() == IDLE)
	{
#if MOTOR_DEBUG_PRINT_ENABLE
		printf(" M4 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\
		        usnDirection,StepFrequency);
#endif

        //!Enable to monitor the X-Axis motor movement
        m_bXAxisMonEnable = TRUE;

		//!Set the motor frequency based on the sequences
	    switch(g_usnCurrentMode)
	    {
	        case WHOLE_BLOOD_MODE:
	        //!Do not check for START_PRE_DILUENT_COUNTING_CMD
	        //!Motor movement is mainly for the main mode (pre-dilute mode) and
	        //!not the intermediate mode of starting the pre-dilute sequence after
	        //!diluting the blood externally in pre-dilute mode
	        //case START_PRE_DILUENT_COUNTING_CMD:
	        case PRE_DILUENT_MODE:
	        case BODY_FLUID_MODE:
	        case START_UP_SEQUENCE_MODE:
	        case AUTO_CALIBRATION_WB:
	        case COMMERCIAL_CALIBRATION_WBC:
	        case COMMERCIAL_CALIBRATION_RBC:
	        case COMMERCIAL_CALIBRATION_PLT:
	        case QUALITY_CONTROL_WB:
	        case QUALITY_CONTROL_BODY_FLUID:
	        case PARTIAL_BLOOD_COUNT_WBC:
	        case PARTIAL_BLOOD_COUNT_RBC_PLT:
	        case FLOW_CALIBRATION:
	            break;

	        case MBD_WAKEUP_START:
	        case PRIME_WITH_RINSE_SERVICE_HANDLING:
	        case PRIME_WITH_LYSE_SERVICE_HANDLING:
	        case PRIME_WITH_DILUENT_SERVICE_HANDLING:
	        case PRIME_ALL_SERVICE_HANDLING:
	        case BACK_FLUSH_SERVICE_HANDLING:
	        case ZAP_APERTURE_SERVICE_HANDLING:
	        case DRAIN_BATH_SERVICE_HANDLING:
	        case DRAIN_ALL_SERVICE_HANDLING:
	        case CLEAN_BATH_SERVICE_HANDLING:
	        case HEAD_RINSING_SERVICE_HANDLING:
	        case PROBE_CLEANING_SERVICE_HANDLING:
	        case MBD_TO_SHIP_SEQUENCE_START:
	        case SHUTDOWN_SEQUENCE:
	        case COUNTING_TIME_SEQUENCE:
	        //case FLOW_CALIBRATION:
	        case PRIME_WITH_EZ_CLEANSER_CMD:
	        case MBD_USER_ABORT_PROCESS:
	        case RINSE_PRIME_FLOW_CALIBRATION:
	        case MBD_USER_ABORT_Y_AXIS_PROCESS:
	        case MBD_VOLUMETRIC_BOARD_CHECK:
	        case MBD_SLEEP_PROCESS:
	            if(StepFrequency > 700)
	            {
	                StepFrequency = 700;
	            }
	            break;

	        default:
	            break;
	    }
		//!Set the motor step size
		SMDSetMotorStepSize(MOTOR4_SELECT, MOTOR4_STEP_CONFIGURATION);
		//!Enable motor
		SMDSetMotorEnable(MOTOR4_SELECT);
		//!Set the motor direction
		SMDSetMotorDirection(MOTOR4_SELECT, usnDirection);
		//!Configure the pwm to start the motor
		PWMConfigMotor4Param(usnDisplacement, usnDirection, StepFrequency);
	}
}
/******************************************************************************/
/*!
 * \fn void SMDStart_Motor5(unsigned int  , unsigned int , unsigned int )
 * \param usnDisplacement specifies the displacement parameter for
 *          the motor configuration
 * \param usnDirection specifies the direction parameter for
 *          the motor configuration
 * \param StepFrequency specifies the frequency parameter for
 *          the motor configuration
 *
 * \brief To start motor 5
 * \brief  Description:
 *      This function start motor 5 using Displacement, Direction and Frequency
 *      parameters
 *      Calls Motor enable, Motor Direction & ConfigMotor3PWM parameters
 *      PWM frequency/Period shall be used to start & Stop motor during runtime
 *  *
 * \return void
 */
/******************************************************************************/
void SMDStart_Motor5(unsigned int  usnDisplacement, unsigned int usnDirection, \
        unsigned int StepFrequency)
{
    /* Y_Motor*/
	if(PWMGetMotor5Status() == IDLE)
	{
#if MOTOR_DEBUG_PRINT_ENABLE
		printf(" M5 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\
		        usnDirection,StepFrequency);
#endif

        //!Enable to monitor the Y-Axis motor movement
        m_bYAxisMonEnable = TRUE;

        switch(g_usnCurrentMode)
        {
            case WHOLE_BLOOD_MODE:
            //!Do not check for START_PRE_DILUENT_COUNTING_CMD
            //!Motor movement is mainly for the main mode (pre-dilute mode) and
            //!not the intermediate mode of starting the pre-dilute sequence after
            //!diluting the blood externally in pre-dilute mode
            //case START_PRE_DILUENT_COUNTING_CMD:
            case PRE_DILUENT_MODE:
            case BODY_FLUID_MODE:
            case START_UP_SEQUENCE_MODE:
            case AUTO_CALIBRATION_WB:
            case COMMERCIAL_CALIBRATION_WBC:
            case COMMERCIAL_CALIBRATION_RBC:
            case COMMERCIAL_CALIBRATION_PLT:
            case QUALITY_CONTROL_WB:
            case QUALITY_CONTROL_BODY_FLUID:
            case PARTIAL_BLOOD_COUNT_WBC:
            case PARTIAL_BLOOD_COUNT_RBC_PLT:
            case FLOW_CALIBRATION:
                break;

            case MBD_WAKEUP_START:
            case PRIME_WITH_RINSE_SERVICE_HANDLING:
            case PRIME_WITH_LYSE_SERVICE_HANDLING:
            case PRIME_WITH_DILUENT_SERVICE_HANDLING:
            case PRIME_ALL_SERVICE_HANDLING:
            case BACK_FLUSH_SERVICE_HANDLING:
            case ZAP_APERTURE_SERVICE_HANDLING:
            case DRAIN_BATH_SERVICE_HANDLING:
            case DRAIN_ALL_SERVICE_HANDLING:
            case CLEAN_BATH_SERVICE_HANDLING:
            case HEAD_RINSING_SERVICE_HANDLING:
            case PROBE_CLEANING_SERVICE_HANDLING:
            case MBD_TO_SHIP_SEQUENCE_START:
            case SHUTDOWN_SEQUENCE:
            case COUNTING_TIME_SEQUENCE:
            //case FLOW_CALIBRATION:
            case PRIME_WITH_EZ_CLEANSER_CMD:
            case MBD_USER_ABORT_PROCESS:
            case RINSE_PRIME_FLOW_CALIBRATION:
            case MBD_VOLUMETRIC_BOARD_CHECK:
            case MBD_SLEEP_PROCESS:
            case MBD_USER_ABORT_Y_AXIS_PROCESS:
                if(StepFrequency > 500)
                {
                    StepFrequency = 500;
                }
                break;

            default:
                break;
        }
		//!Set the motor step size
		SMDSetMotorStepSize(MOTOR5_SELECT, MOTOR5_STEP_CONFIGURATION);
		//!Enable motor
		SMDSetMotorEnable(MOTOR5_SELECT);
		//!Set the motor direction
		SMDSetMotorDirection(MOTOR5_SELECT, usnDirection);
		//!Configure the pwm to start the motor
		PWMConfigMotor5Param(usnDisplacement, usnDirection, StepFrequency);
	}

}

//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

/******************************************************************************/
/*! \file IpcModule.c
 *
 *  \brief IPC communication between CPU1 & CPU2
 *
 *  \b Description:
 *      In this file IPC communication wrapper funtions & IPC interrupt handles
 *      are written.
 *      IPC communication between CPU1 & CPU2 for data exchanging between CPU's.
 *      CPU1 invokes CPU2 to process ADC data while motor control sequence
 *      execution
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-16-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_ipc_communication_module
//! @{
//*****************************************************************************
#include "IpcModule.h"

/******************************************************************************/
/*!
 * \brief Global variable used in this example to track errors
 */
/******************************************************************************/

volatile tIpcController g_sIpcController1;
volatile tIpcController g_sIpcController2;

#pragma DATA_SECTION(Cpu1IpcParam,".RAMGS10_Cpu1IpcParam");
#pragma DATA_SECTION(Cpu1IpcTxMsgQue,".RAMGS10_Cpu1IpcParam");
#pragma DATA_SECTION(Cpu1IpcRxMsgQue,".RAMGS10_Cpu1IpcParam");

uint32_t Cpu1IpcTxMsgQue[10][8];
uint32_t Cpu1IpcRxMsgQue[10][8];

static stIpcParameters Cpu1IpcParam;

uint32_t *Cpu1pulMsgRam;
uint32_t *Cpu2pulMsgRam;


//------------------------------IPC contents Ends ----------------------------//

/******************************************************************************/
/*!
 * \fn void IPCInitialize_IPC(void)
 * \brief Configuration of IPC interrrupts & registers of CPU1
 * \brief  Description:
 *      This function configures IPC interrrupts registers of CPU1
 *      initializes peripheral & enables interrupts
 *
 * \return void
 */
/******************************************************************************/
//------------------------------IPC contents----------------------------------//
void IPCInitialize_IPC(void)
{
    IPCCpu2IpcReqMemAccess();
	DELAY_US(10000);
//! Interrupts that are used in this example are re-mapped to
//! ISR functions found within this file.
	EALLOW; //! This is needed to write to EALLOW protected registers
	PieVectTable.IPC0_INT = &CPU02toCPU01IPC0IntHandler;
	PieVectTable.IPC1_INT = &CPU02toCPU01IPC1IntHandler;
	EDIS;    //! This is needed to disable write to EALLOW protected registers

//! Step 4. Initialize the Device Peripherals:

	IPCInitialize (&g_sIpcController1, IPC_INT0, IPC_INT0);
	IPCInitialize (&g_sIpcController2, IPC_INT1, IPC_INT1);
// TODO

//! Step 5. User specific code, enable interrupts:

	//! Enable CPU INT1 which is connected to Upper PIE IPC INT0-3:
	IER |= M_INT1;

	//! Enable CPU2 to CPU1 IPC INTn in the PIE: Group 1 interrupts
	PieCtrlRegs.PIEIER1.bit.INTx13 = 1;    //! CPU2 to CPU1 INT0
	PieCtrlRegs.PIEIER1.bit.INTx14 = 1;    //! CPU2 to CPU1 INT1

	//! Enable global Interrupts and higher priority real-time debug events:
	EINT;   //! Enable Global interrupt INTM
	ERTM;   //! Enable Global realtime interrupt DBGM

	InitIpc();
}
/******************************************************************************/
/*!
 * \fn void IPCCpu1toCpu2IpcCommSync(void)
 * \brief To intiate communication from CPU1 to CPU2
 * \brief  Description:
 *      CPU1 shall initiates communication by polling STS bit to ensure CPU2
 *      is ready for the handshaking signals.
 *
 * \return void
 */
/******************************************************************************/
void IPCCpu1toCpu2IpcCommSync(void)
{
	Cpu2pulMsgRam = (void *)CPU02TOCPU01_PASSMSG;
	Cpu1pulMsgRam = (void *)CPU01TOCPU02_PASSMSG;

	//!Get the address of CPU2 to CPU1 buffer location
	Cpu1pulMsgRam[0] = (uint32_t)&Cpu1IpcParam.unCpu2ToCpu1RxBuf[0];

    IpcSync(FLAG17);
}
/******************************************************************************/
/*!
 * \fn void IPCCpu1toCpu2IpcInterrupt(uint16_t Flag)
 * \brief To generate interrupt at Cpu2
 * \brief  Description:
 *      generate interrupt at Cpu2
 *
 *! \return void
 */
/******************************************************************************/
void IPCCpu1toCpu2IpcInterrupt(uint16_t Flag)
{
    //!Set IPC flag
	SendIpcFlag(Flag);
}
/******************************************************************************/
/*!
 * \fn uint16_t IPCCpu1toCpu2IpcCmdData(stIpcParameters *Cpu1IpcPacket,
 *      uint16_t dataLength)
 * \param Cpu1IpcPacket data packet structure
 * \param dataLength
 * \brief To Transmit data packet to another CPU
 * \brief  Description:
 *      CPU1 shall transmit data packet to CPU2 using IPCLtoRBlockWrite call
 *
 *! \return status of command (\b STATUS_PASS =success, \b STATUS_FAIL = error
 *! because PutBuffer was full, command was not sent)
 */
/******************************************************************************/
uint16_t IPCCpu1toCpu2IpcCmdData(stIpcParameters *Cpu1IpcPacket, uint16_t dataLength)
{
//	uint32_t dataLength = 8;
	uint16_t status = 0;
	//!If the data length is greater htan 8, limit it to 8
	if(dataLength > 8)
	{
		dataLength = 8;
	}
	//!Request for CPU1 Memory Access
	IPCCpu1IpcReqMemAccess();
	//!Write the data to be sent through IPC
	status = IPCLtoRBlockWrite(&g_sIpcController1, Cpu2pulMsgRam[0],\
	        (uint32_t)&Cpu1IpcPacket->unCpu1ToCpu2TxBuf[0], \
	        (uint32_t)dataLength, IPC_LENGTH_32_BITS,ENABLE_BLOCKING);
	// Write retry code for status failure
	return status;
}
/******************************************************************************/
/*!
 * \fn uint16_t IPCCpu1toCpu2IpcCmd(stIpcParameters *Cpu1IpcPacket)
 * \param Cpu1IpcPacket data packet structure
 * \brief To Transmit data packet to another CPU
 * \brief  Description:
 *      CPU1 shall transmit data packet to CPU2 using IPCLtoRBlockWrite call
 *
 *! \return status of command (\b STATUS_PASS =success, \b STATUS_FAIL = error
 *! because PutBuffer was full, command was not sent)
 */
/******************************************************************************/
uint16_t IPCCpu1toCpu2IpcCmd(stIpcParameters *Cpu1IpcPacket)
{
	uint16_t status = 0;

	//!Request for the memory access
	IPCCpu1IpcReqMemAccess();
	//!Write data to the memory access
	status = IPCLtoRDataWrite(&g_sIpcController1, Cpu2pulMsgRam[0], \
	        Cpu1IpcPacket->unCpu1ToCpu2TxBuf[0], IPC_LENGTH_32_BITS,\
	        ENABLE_BLOCKING,NO_FLAG);
	// Write retry code for status failure
	return status;
}
/******************************************************************************/
//! \fn void IPCClearCpu1IpcPacketTxBuf(stIpcParameters *Cpu1IpcPacket)
//! \param Cpu1IpcPacket instance
//!
//! Clears the Tx buffer of stIpcParameters
//!
//! \return None.
/******************************************************************************/
void IPCClearCpu1IpcPacketTxBuf(stIpcParameters *Cpu1IpcPacket)
{
	uint16_t Counter=0;
	//!Clear the IPC transmit buffer
	for(Counter = 0; Counter < 8 ; Counter++)
	{
		Cpu1IpcPacket->unCpu1ToCpu2TxBuf[Counter] = 0x00000000;
	}
}
/******************************************************************************/
//! \fn void IPCClearCpu1IpcPacketRxBuf(stIpcParameters *Cpu1IpcPacket)
//! \param Cpu1IpcPacket instance
//!
//! Clears the Rx buffer of stIpcParameters
//!
//! \return None.
/******************************************************************************/
void IPCClearCpu1IpcPacketRxBuf(stIpcParameters *Cpu1IpcPacket)
{
	uint16_t Counter=0;
	//!Clear the IPC recieve buffer
	for(Counter = 0; Counter < 8 ; Counter++)
	{
		Cpu1IpcPacket->unCpu2ToCpu1RxBuf[Counter] = 0x00000000;
	}
}
/******************************************************************************/
//! \fn void IPCCpu1IpcPacketToTxMsgQue(stIpcParameters *Cpu1IpcPacket)
//! \param Cpu1IpcPacket instance
//!
//! Clears the Rx buffer of stIpcParameters
//!
//! \return None.
/******************************************************************************/
void IPCCpu1IpcPacketToTxMsgQue(stIpcParameters *Cpu1IpcPacket)
{
	uint16_t Counter=0;
	//!Update the data to be transmitted to Tranmit message queue
	for(Counter = 0; Counter < 8 ; Counter++)
	{
		Cpu1IpcTxMsgQue[Cpu1IpcPacket->TxMsgQueCurrentPtr][Counter] = \
		        Cpu1IpcPacket->unCpu1ToCpu2TxBuf[Counter];
	}
	Cpu1IpcPacket->TxMsgQueCurrentPtr++;

}

/******************************************************************************/
//! \fn void IPCCpu1IpcPacketToRxMsgQue(void)
//!
//! Clears the Rx buffer of stIpcParameters
//!
//! \return None.
/******************************************************************************/
void IPCCpu1IpcPacketToRxMsgQue(void)
{
	uint16_t Counter=0;
	//!Update the data to received to the recieve message queue
	for(Counter = 0; Counter < 8 ; Counter++)
	{
		Cpu1IpcTxMsgQue[Cpu1IpcParam.RxMsgQueCurrentPtr][Counter] =\
		        Cpu1IpcParam.unCpu2ToCpu1RxBuf[Counter];

	}
	Cpu1IpcParam.RxMsgQueCurrentPtr++;

}

/******************************************************************************/
/*!
 * \fn void IPCCpu1IpcReqMemAccess(void)
 * \brief To request for shared memory access
 * \brief  Description:
 *      CPU1 or CPU2 requests memory access for RW
 * \return void
 */
/******************************************************************************/
void IPCCpu1IpcReqMemAccess(void)
{
    //!Request Memory Access to GS0 SARAM for CPU01
    //!Clear bits to let CPU01 own GS0
    if((MemCfgRegs.GSxMSEL.bit.MSEL_GS10) == 1)
    {
    	EALLOW;
    	MemCfgRegs.GSxMSEL.bit.MSEL_GS10 = 0;
    	EDIS;
    }
}

/******************************************************************************/
/*!
 * \fn void IPCCpu2IpcReqMemAccess(void)
 * \brief To request for shared memory access
 * \brief  Description:
 *      CPU1 or CPU2 requests memory access for RW
 *
 *
 * \return void
 */
/******************************************************************************/
void IPCCpu2IpcReqMemAccess(void)
{

    //!Give Memory Access to GS11 SARAM to CPU02
    while(!(MemCfgRegs.GSxMSEL.bit.MSEL_GS4))
    {
    	EALLOW;
    	MemCfgRegs.GSxMSEL.bit.MSEL_GS4 = 1;
    	EDIS;
    }


    //!Give Memory Access to GS11 SARAM to CPU02
    while(!(MemCfgRegs.GSxMSEL.bit.MSEL_GS11))
    {
    	EALLOW;
    	MemCfgRegs.GSxMSEL.bit.MSEL_GS11 = 1;
    	EDIS;
    }

    //!Give Memory Access to GS12 SARAM to CPU02
    while(!(MemCfgRegs.GSxMSEL.bit.MSEL_GS12))
    {
    	EALLOW;
    	MemCfgRegs.GSxMSEL.bit.MSEL_GS12 = 1;
    	EDIS;
    }


    //!Give Memory Access to GS13 SARAM to CPU02
    while(!(MemCfgRegs.GSxMSEL.bit.MSEL_GS13))
    {
    	EALLOW;
    	MemCfgRegs.GSxMSEL.bit.MSEL_GS13 = 1;
    	EDIS;
    }

}
/******************************************************************************/
//! \fn stIpcParameters* IPCGetCpu1IpcParam(void)
//! Responds to 16/32-bit data word write command the remote CPU system
//!
//!
//! CPU system which includes the 16/32-bit data word to write to the local CPU
//! address.
//!
//! This function will allow the local CPU system to update all ipc related variables
//! and status flags
//!
//!
//! \return Cpu1IpcParam structure address
/******************************************************************************/
stIpcParameters* IPCGetCpu1IpcParam(void)
{
	return &Cpu1IpcParam;
}



/******************************************************************************/
/*!
 * \fn __interrupt void CPU02toCPU01IPC0IntHandler (void)
 *
 * \brief interrupt handler for CPU2 interrupt 0
 * \brief  Description:
 *      CPU1 shall read/write to the shared memroy based on the command  received
 *      from CPU2
 *
 *
 * \return uint32_t
 */
/******************************************************************************/
//------------------------------IPC contents----------------------------------//
/******************************************************************************/
//! CPU02 to CPU01 IPC INT0 Interrupt Handler -
//! Handles writes into CPU01 addresses as a result of read commands to the CPU02.
/******************************************************************************/
__interrupt void CPU02toCPU01IPC0IntHandler (void)
{

	tIpcMessage sMessage;

    //! Continue processing messages as long as CPU01 to CPU02 GetBuffer1 is full
    while (IpcGet(&g_sIpcController1, &sMessage, DISABLE_BLOCKING) != STATUS_FAIL)
    {
        switch (sMessage.ulcommand)
        {

        case IPC_SET_BITS_PROTECTED:
            IPCRtoLSetBits_Protected(&sMessage);       // Processes
                                                       // IPCReqMemAccess()
                                                       // function
            break;
        case IPC_CLEAR_BITS_PROTECTED:
            IPCRtoLClearBits_Protected(&sMessage);     // Processes
                                                       // IPCReqMemAccess()
                                                       // function
            break;
        case IPC_BLOCK_WRITE:
            IPCRtoLBlockWrite(&sMessage);
            Cpu1IpcParam.LocalReceiveFlag = 1;
            break;

        case IPC_DATA_WRITE:
        	IPCRtoLDataWrite(&sMessage);
            Cpu1IpcParam.LocalReceiveFlag = 1;
           break;

        default:
        	Cpu1IpcParam.ErrFlag = 1;
        	//! Through Error msg here
            break;
        }
    }
    //!usnIPCStatusFlag = usnIPCStatusFlag+ 1;
    //! Acknowledge IPC INT0 Flag and PIE to receive more interrupts
    IpcRegs.IPCACK.bit.IPC0 = 1;
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

}
/******************************************************************************/
/*!
 * \fn __interrupt void CPU02toCPU01IPC1IntHandler (void)
 *
 * \brief interrupt handler for CPU2 interrupt 1
 * \brief  Description:
 *      CPU1 shall read/write to the shared memroy based on the command  received
 *      from CPU2
 *
 *
 * \return uint32_t
 */
/******************************************************************************/
/******************************************************************************/
//! CPU01 to CPU02 IPC INT1 Interrupt Handler -
//! Should never reach this ISR. This is an optional placeholder for
//! g_sIpcController2.
/******************************************************************************/
__interrupt void CPU02toCPU01IPC1IntHandler (void)
{
	//! Should never reach here - Placeholder for Debug
    //! Acknowledge IPC INT1 Flag and PIE to receive more interrupts

    //printf("\n CPU-01 IPC INT 1 Handler ");
	Cpu1IpcParam.INT1Flag = 1;
    IpcRegs.IPCACK.bit.IPC1 = 1;
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
 }
//------------------------------IPC contents Ends ----------------------------//
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

/********************************************************************************/
/*! \file BloodDataAnalysis.c
 *
 *  \brief Post processing of ADC raw data
 *
 *  \b  Description:
 *      In this file the Analog raw data shall be processed.
 *      Here, WBC, RBC & PLT counts shall be calculated
 *      Alaong with counts, peak information shall be stored in RAM
 *
 *
 *   $Version: $ 0.1
 *   $Date:    $ May-21-2015
 *   $Author:  $ GURUDUTT R, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/

//*****************************************************************************
//! \addtogroup cpu1_data_analysis_module
//! @{
//*****************************************************************************
#include"BloodDataAnalysis.h"




//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

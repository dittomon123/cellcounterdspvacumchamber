/******************************************************************************/
/*! \file SystemInitialize.h
 *
 *  \brief Header file for SystemInitialize.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Apr-21-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_system_module
//! @{
//******************************************************************************
#ifndef POC2V2CELLCOUNTERCPU1_INCLUDE_SYSTEMINITIALIZE_H_
#define POC2V2CELLCOUNTERCPU1_INCLUDE_SYSTEMINITIALIZE_H_

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "BloodCellCounter.h"          /* Model's header file */
#include "rtwtypes.h"                  /* MathWorks types */
#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File
#include "OpticalInterrupter.h"
#include "Timer.h"
#include "IpcModule.h"
#include "SpiInterface.h"
#include "StepperMotorDriver.h"
#include "WatchdogTimer.h"
#include "Pwm.h"
#include "PressureSense.h"
#include "AdcDriver.h"
#include "InitSDRAM.h"
#include "BloodDataAnalysis.h"
#include "InitBoard.h"
#include "InitGpio.h"
#include "UartDebug.h"
#include "SpiComm.h"
#include "CommInterface.h"
//#include "Structure.h"

/******************************************************************************/
/*!
 * \def CELL_LOW_V_RBC
 * \brief   Lower amplitude threshold for RBC Cell Detection
 */
/******************************************************************************/
#define CELL_LOW_V_RBC                          1400// jojo changed 700 //1450 in master
//500//1050//550

/******************************************************************************/
/*!
 * \def CELL_HIGH_V_RBC
 * \brief   Higher amplitude threshold for RBC Cell Detection
 */
/******************************************************************************/
#define CELL_HIGH_V_RBC                         16500//jojo changed 11000   //16000 in master
//30000


/******************************************************************************/
/*!
 * \def DELTA_MIN_MAX_RBC
 * \brief   Not used
 */
/******************************************************************************/
#define DELTA_MIN_MAX_RBC                       0

/******************************************************************************/
/*!
 * \def PULSEWIDTH_MIN_RBC
 * \brief   Lower pulse width threshold for RBC Cell Detection
 */
/******************************************************************************/
#define PULSEWIDTH_MIN_RBC                      3

/******************************************************************************/
/*!
 * \def PULSEWIDTH_MIN_RBC
 * \brief   Higher pulse width threshold for RBC Cell Detection
 */
/******************************************************************************/
#define PULSEWIDTH_MAX_RBC                      120

/******************************************************************************/
/*!
 * \def CELL_LOW_V_WBC
 * \brief   Lower amplitude threshold for WBC Cell Detection
 */
/******************************************************************************/
#define CELL_LOW_V_WBC                          350//jojo changed 550 //400 in master
//600

/******************************************************************************/
/*!
 * \def CELL_HIGH_V_WBC
 * \brief   Higher amplitude threshold for WBC Cell Detection
 */
/******************************************************************************/
#define CELL_HIGH_V_WBC                        21000//jojo changed 30000 //20000 in master
//19050

/******************************************************************************/
/*!
 * \def DELTA_MIN_MAX_WBC
 * \brief   Not used
 */
/******************************************************************************/
#define DELTA_MIN_MAX_WBC                       873

/******************************************************************************/
/*!
 * \def PULSEWIDTH_MIN_WBC
 * \brief   Lower pulse width threshold for WBC Cell Detection
 */
/******************************************************************************/
#define PULSEWIDTH_MIN_WBC                      7//3

/******************************************************************************/
/*!
 * \def PULSEWIDTH_MAX_WBC
 * \brief   Higher pulse width threshold for WBC Cell Detection
 */
/******************************************************************************/
#define PULSEWIDTH_MAX_WBC                      44//60

/******************************************************************************/
/*!
 * \def CELL_LOW_V_PLT
 * \brief   Lower amplitude threshold for PLT Cell Detection
 */
/******************************************************************************/
#define CELL_LOW_V_PLT                          280//jojo changed 150 //500 in master
//510//650

/******************************************************************************/
/*!
 * \def CELL_HIGH_V_PLT
 * \brief   Higher amplitude threshold for PLT Cell Detection
 */
/******************************************************************************/
#define CELL_HIGH_V_PLT                        4000 //jojo changed 3000 //3900 in master
//2850//4500//2500//2143

/******************************************************************************/
/*!
 * \def DELTA_MIN_MAX_PLT
 * \brief   Not used
 */
/******************************************************************************/
#define DELTA_MIN_MAX_PLT                       0

/******************************************************************************/
/*!
 * \def PULSEWIDTH_MIN_PLT
 * \brief   Lower pulse width threshold for PLT Cell Detection
 */
/******************************************************************************/
#define PULSEWIDTH_MIN_PLT                      8

/******************************************************************************/
/*!
 * \def PULSEWIDTH_MAX_PLT
 * \brief   Higher pulse width threshold for PLT Cell Detection
 */
/******************************************************************************/
#define PULSEWIDTH_MAX_PLT                      60

/******************************************************************************/
/*!
 * \def V_VCC
 * \brief Applied DC voltage across the aperture
 */
/******************************************************************************/
#define V_VCC 								3.30

/******************************************************************************/
/*!
 * \def V_SAMPLE_EMPTY
 * \brief Voltage across sampling resistor with aperture and electrolyte
 */
/******************************************************************************/
#define V_SAMPLE_EMPTY						3.00

/******************************************************************************/
/*!
 * \def APERTURE_LENGTH_RBC_PLT
 * \brief Aperture length for the RBC & PLT ruby
 */
/******************************************************************************/
#define APERTURE_LENGTH_RBC_PLT 			0.000112

/******************************************************************************/
/*!
 * \def APERTURE_DIA_RBC_PLT
 * \brief Aperture diameter for the RBC & PLT ruby
 */
/******************************************************************************/
#define APERTURE_DIA_RBC_PLT 				0.00008

/******************************************************************************/
/*!
 * \def APERTURE_LENGTH_ADJUSTED_RBC_PLT
 * \brief Adjusted aperture length for the RBC & PLT ruby
 */
/******************************************************************************/
#define APERTURE_LENGTH_ADJUSTED_RBC_PLT 	(APERTURE_LENGTH_RBC_PLT+0.8*APERTURE_DIA_RBC_PLT)

/******************************************************************************/
/*!
 * \def APERTURE_LENGTH_WBC
 * \brief Aperture length for the WBC ruby
 */
/******************************************************************************/
#define APERTURE_LENGTH_WBC 				0.000140

/******************************************************************************/
/*!
 * \def APERTURE_DIA_WBC
 * \brief Aperture diameter for the WBC ruby
 */
/******************************************************************************/
#define APERTURE_DIA_WBC 					0.00008

/******************************************************************************/
/*!
 * \def APERTURE_LENGTH_ADJUSTED_WBC
 * \brief Adjusted aperture length for the WBC ruby
 */
/******************************************************************************/
#define APERTURE_LENGTH_ADJUSTED_WBC 		(APERTURE_LENGTH_WBC+0.8*APERTURE_DIA_WBC)

/******************************************************************************/
/*!
 * \def PULSE_HEIGHT_SAMPLES
 * \brief Maximum pulse height samples that can be stored
 */
/******************************************************************************/
#define PULSE_HEIGHT_SAMPLES	200000

extern Uint16 usTimerFlag;


int Motor1Pwm8Status(void); //HN_added

/******************************************************************************/
/*!
 *  \fn     SysSystem_Initalize()
 *  \brief Function to initialize CPU peripherals and application modules
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysSystem_Initalize(void);

 /******************************************************************************/
 /*!
  *  \fn     SysRT_OneStep()
  *  \brief This function call shall do the sequence of operation for
  *      blood data analysis and motor control
  *  \param  void
  *  \return void
  */
 /******************************************************************************/
void SysRT_OneStep(void);

/******************************************************************************/
/*!
 *  \fn     SysMotor_Valve_Control()
 *  \brief  This function controls the motor and valves based in mbd input
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysMotor_Valve_Control(void);

/******************************************************************************/
/*!
 *  \fn     SysPeripheralModuleTest()
 *  \brief  Function performs the individual/stand alone test for requested module
 *      or peripheral
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysPeripheralModuleTest(void);

/******************************************************************************/
/*!
 *  \fn     SysCheckHomePosition()
 *  \brief  Function to checks the X & Y home position states and provided to mbd
 *      module
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysCheckHomePosition(void);

/******************************************************************************/
/*!
 *  \fn     SysTestMotors()
 *  \brief  Function to continuous run for all 5 motor UP & DOWN direction
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysTestMotors(void);

/******************************************************************************/
/*!
 *  \fn     SysTestValves()
 *  \brief  Function to control valve output toggle in loop
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysTestValves(void);

/******************************************************************************/
/*!
 *  \fn     SysTestGpioPin()
 *  \brief  Function to control gpio pin toggle in loop
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysTestGpioPin(void);

/******************************************************************************/
/*!
 *  \fn     SysDataAquisition()
 *  \brief  Function to set First & Second counting modules for Blood data and
 *          Pressure Data
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysDataAquisition(void);

/******************************************************************************/
/*!
 *  \fn     SysUpdateMbdToDsp()
 *  \brief  Motor related output parameters to Dsp shall be updated
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysUpdateMbdToDsp(void);

/******************************************************************************/
/*!
 *  \fn     SysUpdateDspToMbd()
 *  \brief  Motor related input parameters to MBD shall be updated
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysUpdateDspToMbd(void);

/******************************************************************************/
/*!
 *  \fn     SysSetParametersDefaultValue()
 *  \brief  Function to set Parameter default values for LED1 and Home sensor control
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysSetParametersDefaultValue(void);

/******************************************************************************/
/*!
 *  \fn     SysMotorControlIdleMode()
 *  \brief  Function to set motors in idle mode
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysMotorControlIdleMode(void);

/******************************************************************************/
/*!
 *  \fn     SysCycleCompletion()
 *  \brief  Function to handle action on any cycle completion
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysCycleCompletion(void);

/******************************************************************************/
/*!
 *  \fn     SysUpdateSampleStartButton()
 *  \brief  Function to handle Sample start button switch
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysUpdateSampleStartButton(void);

/******************************************************************************/
/*!
 *  \fn     SysCpu1IpcCommModule()
 *  \brief  Function to process IPC communication module between CPU1 and CPU2
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysCpu1IpcCommModule(void);

/******************************************************************************/
/*!
 *  \fn     SysUartPrintCpuFirmwareVer()
 *  \brief  Function to print FW verion of cpu
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysUartPrintCpuFirmwareVer(Uint16 Cpu);

/******************************************************************************/
/*!
 *  \fn     SysInitCpu1Cpu2IpcComm()
 *  \brief  Function is to sync b/w cpu1 & cpu2. Initial data handshaking b/w the
 *      cores and firmware version request
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysInitCpu1Cpu2IpcComm(void);

/******************************************************************************/
/*!
 *  \fn     SysGetCpu2FwVer()
 *  \brief  Function to copy cpu2 firmware version
 *  \param  uint32_t addrCpu2FwVer
 *  \return void
 */
/******************************************************************************/
void SysGetCpu2FwVer(uint32_t addrCpu2FwVer);

/******************************************************************************/
/*!
 *  \fn     SysGetWbcBloodCellInfo()
 *  \brief  Function to copy default values for WBC info structure
 *  \param  uint32_t addrBloodCellInfoDefault
 *  \return void
 */
/******************************************************************************/
void SysGetWbcBloodCellInfo(uint32_t addrBloodCellInfoDefault);

/******************************************************************************/
/*!
 *  \fn     SysGetRbcBloodCellInfo()
 *  \brief  Function to copy default values for RBC info structure
 *  \param  uint32_t addrBloodCellInfoDefault
 *  \return void
 */
/******************************************************************************/
void SysGetRbcBloodCellInfo(uint32_t addrBloodCellInfoDefault);

/******************************************************************************/
/*!
 *  \fn     SysGetPltBloodCellInfo()
 *  \brief  Function to copy default values for PLT info structure
 *  \param  uint32_t addrBloodCellInfoDefault
 *  \return void
 */
/******************************************************************************/
void SysGetPltBloodCellInfo(uint32_t addrBloodCellInfoDefault);

/******************************************************************************/
/*!
 *  \fn     SysTestUartModule()
 *  \brief  Function to performs the uart test
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysTestUartModule(void);

/******************************************************************************/
/*!
 *  \fn     SysInitGetFirmwareVer()
 *  \brief  Function is called to get the version of CPU1 and CPU2
 *  \param  uint16_t* pData
 *  \return void
 */
/******************************************************************************/
void SysInitGetFirmwareVer(uint16_t* pData);

/******************************************************************************/
/*!
 *  \fn     SysInitGetSystemStatus()
 *  \brief  Function to get the System status
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysInitGetSystemStatus(void);

/******************************************************************************/
/*!
 *  \fn     SysInitSystemHealthData()
 *  \brief  Function to update the system health status to data array
 *  \param  uint16_t* pData
 *  \return void
 */
/******************************************************************************/
void SysInitSystemHealthData(uint16_t* pData);

/******************************************************************************/
/*!
 *  \fn     SysInitCheckSystemHealthRanges()
 *  \brief  Function to check the system health values against the ranges
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysInitCheckSystemHealthRanges(void);

/******************************************************************************/
/*!
 *  \fn     SysGetAdcCountData()
 *  \brief  Function is called to get the pulse count data obtained from for WBC,
 *      RBC and Platelets
 *  \param  uint16_t* pData
 *  \return void
 */
/******************************************************************************/
uint16_t SysGetAdcCountData(uint16_t* pData);

/******************************************************************************/
/*!
 *  \fn     SysGetADCDataLength()
 *  \brief  Function is called to get the length of the pulse Height data
 *      obtained from ADC
 *  \param  uint16_t usnSubCmd
 *  \return uint32_t
 */
/******************************************************************************/
uint32_t SysGetADCDataLength(uint16_t usnSubCmd);

/******************************************************************************/
/*!
 *  \fn     SysGetPulseHeightDataFrmSDRAM()
 *  \brief  Function is called to get the pulse height data obtained from ADC for
 *      WBC, RBC and Platelets
 *  \param  E_CELL_TYPE eTypeofCell, \
                uint16_t* parrusnHeightData,uint32_t rusnPrevIndex, \
                const uint16_t kusnSize
 *  \return void
 */
/******************************************************************************/
uint32_t SysGetPulseHeightDataFrmSDRAM(E_CELL_TYPE eTypeofCell, \
                uint16_t* parrusnHeightData,uint32_t rusnPrevIndex, \
                const uint16_t kusnSize);//SAM_SPI

/******************************************************************************/
/*!
 *  \fn     SysGetADCRawDataFrmSDRAM()
 *  \brief  Function is called to get the raw ADC data obtained for WBC,
 *      RBC and Platelets
 *  \param  E_CELL_TYPE eTypeofCell, \
                    uint16_t* parrusnADCData,uint32_t rusnPrevADCIndex,\
                    const uint16_t kusnMaxSize
 *  \return void
 */
/******************************************************************************/
uint32_t SysGetADCRawDataFrmSDRAM(E_CELL_TYPE eTypeofCell, \
                    uint16_t* parrusnADCData,uint32_t rusnPrevADCIndex,\
                    const uint16_t kusnMaxSize);

/******************************************************************************/
/*!
 *  \fn     SysInitGetHGBData()
 *  \brief  Function is called to get the HGB data obtained from ADC
 *  \param  uint16_t* pusnData, uint16_t usnDataLength
 *  \return void
 */
/******************************************************************************/
void SysInitGetHGBData(uint16_t* pusnData, uint16_t usnDataLength);

/******************************************************************************/
/*!
 *  \fn     SysInitSDRamAdd()
 *  \brief  Function is called to initialize the SDRam address to member pointers
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysInitSDRamAdd(void);

/******************************************************************************/
/*!
 *  \fn     SysValveStatusTest()
 *  \brief  To switch off the valve when the corresponding Valve is excited.
 *      This assist in testing the valve functioning
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysValveStatusTest(void);

/******************************************************************************/
/*!
 *  \fn     SysWasteOverflowCheck()
 *  \brief  Function to checks status of the waste detect sensor every second.
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysWasteOverflowCheck(void);

/******************************************************************************/
/*!
 *  \fn     SysInitWasteBinReplaced()
 *  \brief  Function to update Waste detect check variable
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysInitWasteBinReplaced(void);

/******************************************************************************/
/*!
 *  \fn     SysInitValveTest()
 *  \brief  Function to excite the valve when the corresponding Valve number is
 *      received. This assist in testing the valve functioning
 *  \param  unsigned char ucValveNumber
 *  \return void
 */
/******************************************************************************/
void SysInitValveTest(unsigned char ucValveNumber);

/******************************************************************************/
/*!
 *  \fn     SysInitHealthStatusCheck()
 *  \brief  Function to check the system health status
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysInitHealthStatusCheck(void);

/******************************************************************************/
/*!
 *  \fn     SysPressureLineCheck()
 *  \brief  Function to check the pressure sensor line presence
 *  \param  void
 *  \return void
 */

void SysPressureLineCheck(void);
/******************************************************************************/
/*!
 *  \fn     SysPressureCalibration()
 *  \brief  Function to calibrate the pressure
 *  \param  void
 *  \return void
 */

void SysPressureCalibration(void);
/******************************************************************************/
/*!
 *  \fn     SysInitAbortProcess()
 *  \brief  Function to abort the process
 *  \param  bool_t bSendCommandToSitara
 *  \return void
 */
/******************************************************************************/
void SysInitAbortProcess(bool_t bSendCommandToSitara);

/******************************************************************************/
/*!
 *  \fn     SysInitGetCountingTime()
 *  \brief  Function is called to get the counting time for WBC and RBC
 *  \param  uint16_t* pData
 *  \return uint16_t
 */
/******************************************************************************/
uint16_t SysInitGetCountingTime(uint16_t* pData);

/******************************************************************************/
/*!
 *  \fn     SysStateIntervalCheck()
 *  \brief  To verify whether the interval between the states are within the range
 *  \param  uint16_t usnSequenceState
 *  \return void
 */
/******************************************************************************/
void SysStateIntervalCheck(uint16_t usnSequenceState);

/******************************************************************************/
/*!
 *  \fn     SysMonitorReagentPriming()
 *  \brief  Function to check whether Diluent, Lyse and Rinse lines are primed
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysMonitorReagentPriming(void);

/******************************************************************************/
/*!
 *  \fn     SysMotorHomePositionCheck()
 *  \brief  Called to check whether the motors are at home position with respect
 *           to the current displacement and position of the motor calculated
 *           If it is not as expected set the error code and indicate UI with
 *           respect to the error
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysMotorHomePositionCheck(void);

/******************************************************************************/
/*!
 *  \fn     SysControllerStatusMon()
 *  \brief  To monitor whether the controller is running fine or not
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysControllerStatusMon(void);

/******************************************************************************/
/*!
 *  \fn     SysHomePositionInit()
 *  \brief  To initialize all the motor home position to Zero
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysHomePositionInit(void);

/******************************************************************************/
/*!
 *  \fn     SysDisableMotorMon()
 *  \brief  To disable motor monitoring in any particular sequence
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysDisableMotorMon(void);

/******************************************************************************/
/*!
 *  \fn     SysXMotorErrorCheck()
 *  \brief  To check whether any error in X-Axis motor movement
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysXMotorErrorCheck(void);

/******************************************************************************/
/*!
 *  \fn     SysDiluentMotorErrorCheck()
 *  \brief  To check whether any error in Diluent syringe movement
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysDiluentMotorErrorCheck(void);

/******************************************************************************/
/*!
 *  \fn     SysSampleMotorErrorCheck()
 *  \brief  To check whether any error in Sample syringe movement
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysSampleMotorErrorCheck(void);

/******************************************************************************/
/*!
 *  \fn     SysWasteMotorErrorCheck()
 *  \brief  To check whether any error in waste syringe movement
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysWasteMotorErrorCheck(void);

/******************************************************************************/
/*!
 *  \fn     SysYMotorErrorCheck()
 *  \brief  Function to check error in Y Motor assembly
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysYMotorErrorCheck(void);

/******************************************************************************/
/*!
 *  \fn     SysMBDSequenceCompletion()
 *  \brief  Function to re-initialize the variables and set motor to idle after
* completion of the sequence
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysMBDSequenceCompletion(void);

/******************************************************************************/
/*!
 *  \fn     SysResultsTransmitToSitara()
 *  \brief  Function to transmit count results to Sitara
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysResultsTransmitToSitara(void);

/******************************************************************************/
/*!
 *  \fn     SysSequenceCompletion()
 *  \brief  Function to send sequence completed status to Sitara
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysSequenceCompletion(void);

/******************************************************************************/
/*!
 *  \fn     SysReInit()
 *  \brief  Function to re-initialize on new measurement
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysReInit(void);
/******************************************************************************/
/*!
 *  \fn     binArrayToInt()
 *  \brief  Function to convert the array of binary value o Integer
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SysBinArrayToInt(void);

/******************************************************************************/
/*!
 * \fn void SysCheckDiluentPriming()
 * \brief   Function to check whether Diluent line is primed
 * \return  void
 */
/******************************************************************************/
void SysCheckDiluentPriming(void);

/******************************************************************************/
/*!
 * \fn void SysCheckLysePriming()
 * \brief   Function to check whether Lyse line is primed
 * \return  void
 */
/******************************************************************************/
void SysCheckLysePriming(void);

/******************************************************************************/
/*!
 * \fn void SysCheckRinsePriming()
 * \brief   Function to check whether Rinse line is primed
 * \return  void
 */
/******************************************************************************/
void SysCheckRinsePriming(void);

/******************************************************************************/
/*!
 *  \fn     SysErrorCommand()
 *  \brief  Function to frame the error command to be sent to processor
 *  \param  uint16_t usnCurrentMode
 *  \return void
 */
/******************************************************************************/
void SysErrorCommand(uint16_t usnCurrentMode);

/******************************************************************************/
/*!
 * \fn void SysInitArrestSequence(void)
 * \brief   Function to arrest the current sequence
 * \return  void
 * \Author  SBS
 */
/******************************************************************************/
void SysInitArrestSequence(bool_t bSendCommandToSitara);
/******************************************************************************/
/*!
 * \fn void SysInitProbeCleanArrestSequence(void)
 * \brief   Function to arrest the current sequence
 * \return  void
 * \Author  SBS
 */
/******************************************************************************/
void SysInitProbeCleanArrestSequence(bool_t bSendCommandToSitara);

/******************************************************************************/
/*!
 * \fn void SysCheckPressure(void)
 * \brief   Function to check, is the pressure within the expected range
 * \return  void
 */
/******************************************************************************/
void SysCheckPressure(void);

/******************************************************************************/
/*!
 * \fn void SysCheckTemperature(void)
 * \brief   Function to check, is the temperature within the expected range
 * \return  void
 */
/******************************************************************************/
void SysCheckTemperature(void);

/******************************************************************************/
/*!
 * \fn void SysCheck_24V(void)
 * \brief   Function to check, is the 24V within the expected range
 * \return  void
 */
/******************************************************************************/
void SysCheck_24V(void);

/******************************************************************************/
/*!
 * \fn void SysCheck_5V(void)
 * \brief   Function to check, is the 5V within the expected range
 * \return  void
 */
/******************************************************************************/
void SysCheck_5V(void);

/******************************************************************************/
/*!
 * \fn void PressureValveControl(void)
 * \brief   Function to control the value at the end of the sequence
 * \return  void
 */
/******************************************************************************/
void PressureValveControl(void);

/******************************************************************************/
/*!
 * \fn void MotorFaultCheck(void)
 * \brief   Function to check for Motor fault Conditions.
 * \return  void
 */
/******************************************************************************/
void  MotorFaultCheck(void);

extern void SPISetCommCtrlBusyInt();
extern void SPIClearCommCtrlBusyInt();
extern void CIFormPatientHandlingPacket(uint16_t usnSubCmd);
extern void CIFormServiceHandlingPacket(uint16_t usnServicehandled, bool_t bStartEnd);
extern void CIValveTestCompleted(unsigned char ucValveNumber);
extern void CIFormCalibrationHandlingPacket(uint16_t usnSubCmd);
extern void CIFormQualityHandlingPacket(uint16_t usnSubCmd);
extern void CIFormAbortProcessPacket(uint16_t usnSubCmd);

#endif /* SYSTEM_INITIALIZE_H_ */
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

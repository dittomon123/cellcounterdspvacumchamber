/*
 * Defines.h
 *
 *  Created on: 29-Jul-2016
 *      Author: 20033200
 */

#ifndef INCLUDE_DEFINES_H_
#define INCLUDE_DEFINES_H_

/******************************************************************************/
/*!
 * \def ZERO
 * \brief  To define "0"
 */
/******************************************************************************/
#define ZERO                           0

/******************************************************************************/
/*!
 * \def ONE
 * \brief  To define "1"
 */
/******************************************************************************/
#define ONE                            1

/******************************************************************************/
/*!
 * \def CRC_16_BIT
 * \brief  To define calculation of 16 bit CRC - Not used
 */
/******************************************************************************/
#define CRC_16_BIT						16

/******************************************************************************/
/*!
 * \def CRC_16_BIT
 * \brief  To Returns Success on successful execution - Not used
 */
/******************************************************************************/
#define CELLCOUNTER_SUCCESS             1

/******************************************************************************/
/*!
 * \def CRC_16_BIT
 * \brief  To Returns on Failure execution - Not used
 */
/******************************************************************************/
#define CELLCOUNTER_FAILURE             -1

/******************************************************************************/
/*!
 * \def COUNT_DATA_LENGTH
 * \brief  To data length of count packet that is sent to processor
 */
/******************************************************************************/
#define COUNT_DATA_LENGTH				16 //13  //10

/******************************************************************************/
/*!
 * \def MOTOR_CHECK_SUCCESS
 * \brief  To define if the motor check was successful during motor test
 */
/******************************************************************************/
#define MOTOR_CHECK_SUCCESS				1

/******************************************************************************/
/*!
 * \def MOTOR_CHECK_FAILURE
 * \brief  To define if the motor check was failure during motor test
 */
/******************************************************************************/
#define MOTOR_CHECK_FAILURE				2

/******************************************************************************/
/*!
 * \def MOTOR_HOME_POSITION
 * \brief  To define if the motor has reached successful to home position
 */
/******************************************************************************/
#define MOTOR_HOME_POSITION             1

/******************************************************************************/
/*!
 * \def MOTOR_NOT_HOME_POSITION
 * \brief  To define if the motor hasn't reached to home position
 */
/******************************************************************************/
#define MOTOR_NOT_HOME_POSITION         0

/******************************************************************************/
/*!
 * \def COUNTING_TIME_DATA_LENGTH
 * \brief  To define if the data lenth of WBC RBC counting time command
 */
/******************************************************************************/
#define COUNTING_TIME_DATA_LENGTH		2

/******************************************************************************/
/*!
 * \def HEALTH_STATUS_ARRAY_SIZE
 * \brief  To define the array to store preivous 33 values of the system health
 * data from ADC
 */
/******************************************************************************/
#define HEALTH_STATUS_ARRAY_SIZE        32

/*!Macros for Comm Protocol*/
/******************************************************************************/
/*!
 * \def HEALTH_STATUS_ARRAY_SIZE
 * \brief  To define Raw ADC Data size
 */
/******************************************************************************/
#define RAW_ADC_DATA_SIZE 				1004000

/******************************************************************************/
/*!
 * \def MESSAGE_ATTRIBUTE_OFFSET_ADDR
 * \brief  To define Message attribute Offset Address
 */
/******************************************************************************/
#define MESSAGE_ATTRIBUTE_OFFSET_ADDR   2

/******************************************************************************/
/*!
 * \def DATA_LENGTH_OFFSET_ADDR
 * \brief  To define Data Length Offset Address
 */
/******************************************************************************/
#define DATA_LENGTH_OFFSET_ADDR         4

/******************************************************************************/
/*!
 * \def START_OF_PACKET_MSW
 * \brief  To define Start of the Packet Frame MSW (Header)
 */
/******************************************************************************/
#define START_OF_PACKET_MSW             0xAAAA

/******************************************************************************/
/*!
 * \def START_OF_PACKET_LSW
 * \brief  To define Start of the Packet Frame LSW (Header)
 */
/******************************************************************************/
#define START_OF_PACKET_LSW				0x5555

/******************************************************************************/
/*!
 * \def END_OF_PACKET_MSW
 * \brief  To define End of the Packet Frame MSW (Header)
 */
/******************************************************************************/
#define END_OF_PACKET_MSW               0x5555

/******************************************************************************/
/*!
 * \def END_OF_PACKET_LSW
 * \brief  To define End of the Packet Frame LSW (Header)
 */
/******************************************************************************/
#define END_OF_PACKET_LSW				0xAAAA

/******************************************************************************/
/*!
 * \def MAX_SPI_PACKET_LENGTH
 * \brief  To define Length of the SPI Packet
 */
/******************************************************************************/
#define MAX_SPI_PACKET_LENGTH           512

/******************************************************************************/
/*!
 * \def DEFAULT_MESSAGE_PACKET_SIZE
 * \brief  To define default Message Packet Size - not used
 */
/******************************************************************************/
#define DEFAULT_MESSAGE_PACKET_SIZE     18

/******************************************************************************/
/*!
 * \def MAXIMUM_DATA_SIZE
 * \brief  To define the maximum Data length
 */
/******************************************************************************/
#define MAXIMUM_DATA_SIZE				502

/******************************************************************************/
/*!
 * \def CRC_LOCATION_IN_PACKET
 * \brief  To define the CRC location in ST_SPI_PACKET_FRAME
 */
/******************************************************************************/
#define CRC_LOCATION_IN_PACKET			510

/*!Commands for MBD*/
/******************************************************************************/
/*!
 * \def PRIME_WITH_RINSE_SERVICE_HANDLING
 * \brief  To define Service Handling command of Rinse Prime to be sent to MBD
 */
/******************************************************************************/
#define PRIME_WITH_RINSE_SERVICE_HANDLING		0x5001

/******************************************************************************/
/*!
 * \def PRIME_WITH_LYSE_SERVICE_HANDLING
 * \brief  To define Service Handling command of Lyse Prime to be sent to MBD
 */
/******************************************************************************/
#define PRIME_WITH_LYSE_SERVICE_HANDLING		0x5002

/******************************************************************************/
/*!
 * \def PRIME_WITH_DILUENT_SERVICE_HANDLING
 * \brief  To define Service Handling command of Diluent Prime to be sent to MBD
 */
/******************************************************************************/
#define PRIME_WITH_DILUENT_SERVICE_HANDLING		0x5003

/******************************************************************************/
/*!
 * \def PRIME_WITH_EZ_CLEANSER_CMD
 * \brief  To define Service Handling command of EZ Cleaner to be sent to MBD
 */
/******************************************************************************/
#define PRIME_WITH_EZ_CLEANSER_CMD				0x5004

/******************************************************************************/
/*!
 * \def PRIME_ALL_SERVICE_HANDLING
 * \brief  To define Service Handling command of Prime All to be sent to MBD
 */
/******************************************************************************/
#define PRIME_ALL_SERVICE_HANDLING				0x5005

/******************************************************************************/
/*!
 * \def BACK_FLUSH_SERVICE_HANDLING
 * \brief  To define Service Handling command of Back flush to be sent to MBD
 */
/******************************************************************************/
#define BACK_FLUSH_SERVICE_HANDLING				0x5006

/******************************************************************************/
/*!
 * \def WHOLE_BLOOD_MODE
 * \brief  To define Patient Handling command of Whole Blood to be sent to MBD
 */
/******************************************************************************/
#define WHOLE_BLOOD_MODE						0x5007

/******************************************************************************/
/*!
 * \def ZAP_APERTURE_SERVICE_HANDLING
 * \brief  To define Service Handling command of Zap Aperture to be sent to MBD
 */
/******************************************************************************/
#define ZAP_APERTURE_SERVICE_HANDLING           0x5008

/******************************************************************************/
/*!
 * \def DRAIN_BATH_SERVICE_HANDLING
 * \brief  To define Service Handling command of Drain Bath to be sent to MBD
 */
/******************************************************************************/
#define DRAIN_BATH_SERVICE_HANDLING             0x5009

/******************************************************************************/
/*!
 * \def DRAIN_ALL_SERVICE_HANDLING
 * \brief  To define Service Handling command of Drain All to be sent to MBD
 */
/******************************************************************************/
#define DRAIN_ALL_SERVICE_HANDLING              0x500A//0x502A

/******************************************************************************/
/*!
 * \def CLEAN_BATH_SERVICE_HANDLING
 * \brief  To define Service Handling command of Clean Bath to be sent to MBD
 */
/******************************************************************************/
#define CLEAN_BATH_SERVICE_HANDLING             0x500B

/******************************************************************************/
/******************************************************************************/
/*!
 * \def GLASS_TUBE_FILLING
 * \brief  To define Service Handling command of fill glass tube Bath to be sent to MBD
 */
/******************************************************************************/
#define GLASS_TUBE_FILLING                      0x503E
/******************************************************************************/
/*!
 * \def COUNT_FAIL
 * \brief  To define Service Handling command of count fail or any maintanance sequence failed to be sent to MBD
 */
/******************************************************************************/
#define COUNT_FAIL                              0x503D
/******************************************************************************/
/*!
 * \def COUNT_FAIL
 * \brief  To define Service Handling command of Hypo cleaner sequnce failed or abort to be sent to MBD
 */
/******************************************************************************/
#define HYPO_CLEANER_FAIL                       0x503F
/******************************************************************************/
/*!
 * \def SYSTEM_STATUS_SERVICE_HANDLING
 * \brief  To define System Status command to get system health status
 */
/******************************************************************************/
#define SYSTEM_STATUS_SERVICE_HANDLING			0x500C

/******************************************************************************/
/*!
 * \def VALVE_TEST_COMPLETION
 * \brief  To define Valve test completion command
 */
/******************************************************************************/
#define VALVE_TEST_COMPLETION					0x500D

/******************************************************************************/
/*!
 * \def WASTE_BIN_FULL_CMD
 * \brief  To define waste bin full command
 */
/******************************************************************************/
#define WASTE_BIN_FULL_CMD						0x500E

/******************************************************************************/
/*!
 * \def PRE_DILUENT_MODE
 * \brief  To define Patient Handling command of Pre-Diluent to be sent to MBD
 */
/******************************************************************************/
#define PRE_DILUENT_MODE			           	0x500F

/******************************************************************************/
/*!
 * \def DISPENSE_THE_DILUENT
 * \brief  To define Patient Handling command of Pre-Diluent to dispense the
 * diluent to dilute the blood
 */
/******************************************************************************/
#define DISPENSE_THE_DILUENT                	0x5010

/******************************************************************************/
/*!
 * \def START_PRE_DILUENT_COUNTING_CMD
 * \brief  To define Patient Handling command of Pre-Diluent to start
 * pre-diluent sequence
 */
/******************************************************************************/
#define START_PRE_DILUENT_COUNTING_CMD      	0x5011

/******************************************************************************/
/*!
 * \def BODY_FLUID_MODE
 * \brief  To define Patient Handling command of body fluid to be sent to MBD
 * sequence
 */
/******************************************************************************/
#define BODY_FLUID_MODE							0x5012

/******************************************************************************/
/*!
 * \def COUNTING_TIME_SEQUENCE
 * \brief  To define service handling command of WBC RBC Counting time to be
 * sent to MBD
 */
/******************************************************************************/
#define COUNTING_TIME_SEQUENCE				    0x5013

/******************************************************************************/
/*!
 * \def START_UP_SEQUENCE_MODE
 * \brief  To define patient handling command of start up sequence to be sent
 * to MBD
 */
/******************************************************************************/
#define START_UP_SEQUENCE_MODE					0x5014

/******************************************************************************/
/*!
 * \def HEAD_RINSING_SERVICE_HANDLING
 * \brief  To define Service handling command of Head Rinse sequence to be sent
 * to MBD
 */
/******************************************************************************/
#define HEAD_RINSING_SERVICE_HANDLING			0x5015

/******************************************************************************/
/*!
 * \def PROBE_CLEANING_SERVICE_HANDLING
 * \brief  To define Service handling command of Probe Cleaning sequence to be
 * sent to MBD
 */
/******************************************************************************/
#define PROBE_CLEANING_SERVICE_HANDLING			0x5016

/******************************************************************************/
/*!
 * \def AUTO_CALIBRATION_MODE
 * \brief  To define Auto calibration command to be sent to MBD
 */
/******************************************************************************/
#define AUTO_CALIBRATION_MODE					0x5017

/******************************************************************************/
/*!
 * \def COMMERCIAL_CALIBRATION_MODE
 * \brief  To define Commercial calibration command to be sent to MBD
 */
/******************************************************************************/
#define COMMERCIAL_CALIBRATION_MODE				0x5018

/******************************************************************************/
/*!
 * \def X_AXIS_MOTOR_CHECK
 * \brief  To define X-Axis motor check command to be sent to MBD
 */
/******************************************************************************/
#define X_AXIS_MOTOR_CHECK						0x5019

/******************************************************************************/
/*!
 * \def Y_AXIS_MOTOR_CHECK
 * \brief  To define Y-Axis motor check command to be sent to MBD
 */
/******************************************************************************/
#define Y_AXIS_MOTOR_CHECK						0x501A

/******************************************************************************/
/*!
 * \def DILUENT_SYRINGE_MOTOR_CHECK
 * \brief  To define Diluent Syringe check command to be sent to MBD
 */
/******************************************************************************/
#define DILUENT_SYRINGE_MOTOR_CHECK				0x501B

/******************************************************************************/
/*!
 * \def WASTE_SYRINGE_MOTOR_CHECK
 * \brief  To define Waste Syringe check command to be sent to MBD
 */
/******************************************************************************/
#define WASTE_SYRINGE_MOTOR_CHECK				0x501C

/******************************************************************************/
/*!
 * \def SAMPLE_LYSE_SYRINGE_MOTOR_CHECK
 * \brief  To define Sample Lyse Syringe check command to be sent to MBD
 */
/******************************************************************************/
#define SAMPLE_LYSE_SYRINGE_MOTOR_CHECK			0x501D

/******************************************************************************/
/*!
 * \def MBD_ABORT_PROCESS_COMPLETED
 * \brief  To define Abort any sequence that is running
 */
/******************************************************************************/
#define MBD_ABORT_PROCESS_COMPLETED             0x501E

/******************************************************************************/
/*!
 * \def SHUTDOWN_SEQUENCE
 * \brief  To define Sutdown Sequence command to be sent to MBD
 */
/******************************************************************************/
#define SHUTDOWN_SEQUENCE                       0x501F

/******************************************************************************/
/*!
 * \def AUTO_CALIBRATION_WB
 * \brief  To define Auto calibration in whole blood mode command to be sent
 * to MBD
 */
/******************************************************************************/
#define AUTO_CALIBRATION_WB                     0x5020

/******************************************************************************/
/*!
 * \def COMMERCIAL_CALIBRATION_WBC
 * \brief  To define Commercial calibration of WBC command to be sent to MBD
 */
/******************************************************************************/
#define COMMERCIAL_CALIBRATION_WBC              0x5021

/******************************************************************************/
/*!
 * \def COMMERCIAL_CALIBRATION_RBC
 * \brief  To define Commercial calibration of RBC command to be sent to MBD
 */
/******************************************************************************/
#define COMMERCIAL_CALIBRATION_RBC              0x5022

/******************************************************************************/
/*!
 * \def COMMERCIAL_CALIBRATION_PLT
 * \brief  To define Commercial calibration of PLT command to be sent to MBD
 */
/******************************************************************************/
#define COMMERCIAL_CALIBRATION_PLT              0x5023

/******************************************************************************/
/*!
 * \def PARTIAL_BLOOD_COUNT_WBC
 * \brief  To define Partial Blood count of WBC command to be sent to MBD
 */
/******************************************************************************/
#define PARTIAL_BLOOD_COUNT_WBC                 0x5024

/******************************************************************************/
/*!
 * \def PARTIAL_BLOOD_COUNT_RBC_PLT
 * \brief  To define Partial Blood count of RBC & PLT command to be sent to MBD
 */
/******************************************************************************/
#define PARTIAL_BLOOD_COUNT_RBC_PLT             0x5025

/******************************************************************************/
/*!
 * \def QUALITY_CONTROL_WB
 * \brief  To define Quality Control command of Whole blood to be sent to MBD
 */
/******************************************************************************/
#define QUALITY_CONTROL_WB                      0x5026

/******************************************************************************/
/*!
 * \def QUALITY_CONTROL_BODY_FLUID
 * \brief  To define Quality Control command of Body Fluid to be sent to MBD
 */
/******************************************************************************/
#define QUALITY_CONTROL_BODY_FLUID              0x5027

/******************************************************************************/
/*!
 * \def MBD_USER_ABORT_PROCESS
 * \brief  To define Abort the current running process
 */
/******************************************************************************/
#define MBD_USER_ABORT_PROCESS                  0x5028

/******************************************************************************/
/*!
 * \def MBD_WAKEUP_START
 * \brief  To define wake up sequence command to be sent to MBD
 */
/******************************************************************************/
#define MBD_WAKEUP_START                        0x5029

/******************************************************************************/
/*!
 * \def FLOW_CALIBRATION
 * \brief  To define WBC and RBC counting sequence command to be sent to MBD
 */
/******************************************************************************/
#define FLOW_CALIBRATION                        0x502B

/******************************************************************************/
/*!
 * \def    RINSE_PRIME_FLOW_CALIBRATION
 * \brief  To define rinse prime before flow calibration
 *         sequence command to be sent to MBD
 */
/******************************************************************************/
#define RINSE_PRIME_FLOW_CALIBRATION             0x502C

/******************************************************************************/
/*!
 * \def    MBD_TO_SHIP_SEQUENCE_START
 * \brief  To define to_ship
 *         sequence command to be sent to MBD
 */
/******************************************************************************/
#define MBD_TO_SHIP_SEQUENCE_START             0x502A  //HN_Added

/******************************************************************************/
/*!
 * \def    MBD_USER_ABORT_Y_AXIS_PROCESS
 * \brief  To define to deactivate the Y axis
 *         sequence command to be sent to MBD
 */
/******************************************************************************/
#define MBD_USER_ABORT_Y_AXIS_PROCESS           0x502D

/******************************************************************************/
/*!
 * \def    MBD_VOLUMETRIC_BOARD_CHECK
 * \brief  To define volumetric board check
 *         sequence command to be sent to MBD
 */
/******************************************************************************/
#define MBD_VOLUMETRIC_BOARD_CHECK              0x502E


/******************************************************************************/
/*!
 * \def    MBD_SLEEP_PROCESS
 * \brief  To define Sleep
 *         sequence command to be sent to MBD
 */
/******************************************************************************/
#define MBD_SLEEP_PROCESS                       0x502F

/******************************************************************************/
/*!
 * \def    MBD_ALL_MOTOR_Y_AXIS_CHECK
 * \brief  To define Y-Axis motor check for all motor check to be sent to MBD
 */
/******************************************************************************/
#define MBD_ALL_MOTOR_Y_AXIS_CHECK                       0x5030

/******************************************************************************/
/*!
 * \def    MBD_ALL_MOTOR_X_AXIS_CHECK
 * \brief  To define X-Axis motor check for all motor check to be sent to MBD
 */
/******************************************************************************/
#define MBD_ALL_MOTOR_X_AXIS_CHECK                       0x5031

/******************************************************************************/
/*!
 * \def    MBD_ALL_MOTOR_SAMPLE_CHECK
 * \brief  To define Sample motor check for all motor check to be sent to MBD
 */
/******************************************************************************/
#define MBD_ALL_MOTOR_SAMPLE_CHECK                       0x5032

/******************************************************************************/
/*!
 * \def    MBD_ALL_MOTOR_DILUENT_CHECK
 * \brief  To define Diluent motor check for all motor check to be sent to MBD
 */
/******************************************************************************/
#define MBD_ALL_MOTOR_DILUENT_CHECK                      0x5033

/******************************************************************************/
/*!
 * \def    MBD_ALL_MOTOR_WASTE_CHECK
 * \brief  To define Waste motor check for all motor check to be sent to MBD
 */
/******************************************************************************/
#define MBD_ALL_MOTOR_WASTE_CHECK                        0x5034

/******************************************************************************/
/*!
 * \def    MBD_VOL_TUBE_EMPTY
 * \brief  To define the sequence to empty the volumetric tube to be sent to MBD
 */
/******************************************************************************/
#define MBD_VOL_TUBE_EMPTY                               0x5036

/******************************************************************************/
/*!
 * \def MBD_STATE_ASPIRATION_COMPLETED
 * \brief  To define Aspiration completed status from MBD
 */
/******************************************************************************/
#define MBD_STATE_ASPIRATION_COMPLETED          2
/******************************************************************************/
/*!
 * \def    SPI_ERROR_CHECK
 * \brief  To define the SPI error check command to be sent to Processor,
 * it has no link to the MBD
 */
/******************************************************************************/
#define SPI_ERROR_CHECK                                  0x5037

/******************************************************************************/
/*!
 * \def    SPI_ERROR_CHECK
 * \brief  To define the SPI error check command to be sent to Processor,
 * it has no link to the MBD
 */
/******************************************************************************/
#define MBD_BATH_FILL_CMD                                  0x5038  //HN_Added(4/01/19)

/******************************************************************************/
/*!
 * \def    MBD_STARTUP_FROM_SERVICE_SCREEN_CMD
 * \brief  To define the SPI error check command to be sent to Processor,
 * it has no link to the MBD
 */
/******************************************************************************/
#define MBD_STARTUP_FROM_SERVICE_SCREEN_CMD                 0x5039  //HN_Added(4/01/19)
/******************************************************************************/
/*!
 * \def    MBD_ZAP_INITIATE
 * \brief  To define the MBD_ZAP_INITATE to initiate
 */
/******************************************************************************/
#define MBD_ZAP_INITIATE                                      0x503B  //HN_Added(15/05/19)
/******************************************************************************/

/******************************************************************************/
/*!
 * \def    MBD_STARTUP_BACKFLUSH_CMD
 * \brief  To define the startup backflush command to be sent to MBD,
 *
 */
/******************************************************************************/
#define  MBD_STARTUP_BACKFLUSH_CMD                          0x503A  //HN_Added(8/01/19)

/******************************************************************************/
/******************************************************************************/
/*!
 * \def    MBD_STARTUP_BACKFLUSH_CMD
 * \brief  To define the startup backflush command to be sent to MBD,
 *
 */
/******************************************************************************/
#define  MBD_STARTUP_FAILED_CMD                          0x503C  //HN_Added(8/01/19)

/******************************************************************************/
/*!
 * \def MBD_STATE_ZAP_START
 * \brief  To define Zap started status from MBD
 */
/******************************************************************************/
#define MBD_STATE_ZAP_START                     3

/******************************************************************************/
/*!
 * \def MBD_STATE_READY_FOR_DISPENSING
 * \brief  To define ready for dispensing status from MBD
 */
/******************************************************************************/
#define MBD_STATE_READY_FOR_DISPENSING          6

/******************************************************************************/
/*!
 * \def MBD_STATE_DISPENSE_COMPLETED
 * \brief  To define dispensing completed status from MBD
 */
/******************************************************************************/
#define MBD_STATE_DISPENSE_COMPLETED            7

/******************************************************************************/
/*!
 * \def HGB_BLANK_ACQUISITION_START
 * \brief  To define HgB Blank Acquisition status from MBD
 */
/******************************************************************************/
#define HGB_BLANK_ACQUISITION_START             4

/******************************************************************************/
/*!
 * \def HGB_BLANK_ACQUISITION_START
 * \brief  To define HgB Blood Sample Acquisition status from MBD
 */
/******************************************************************************/
#define HGB_SAMPLE_ACQUISITION_START            5

/******************************************************************************/
/*!
 * \def MBD_READY_FOR_ASPIRATION
 * \brief  To define Ready for Aspiration status from MBD
 */
/******************************************************************************/
#define MBD_READY_FOR_ASPIRATION                8

/******************************************************************************/
/*!
 * \def MBD_ABORT_PROCESS
 * \brief  To define process aborted status from MBD
 */
/******************************************************************************/
#define MBD_ABORT_PROCESS						9

/******************************************************************************/
/*!
 * \def MBD_ABORT_PROCESS
 * \brief  To define Motor Check success status from MBD
 */
/******************************************************************************/
#define MBD_MOTOR_CHK_SUCCESS					10

/******************************************************************************/
/*!
 * \def MBD_MOTOR_CHK_FALIURE
 * \brief  To define Motor Check failure status from MBD
 */
/******************************************************************************/
#define MBD_MOTOR_CHK_FALIURE					11

/******************************************************************************/
/*!
 * \def MBD_RETURN_TO_DEFAULT_STATE
 * \brief  To define to return MBD to defualt state
 */
/******************************************************************************/
#define MBD_RETURN_TO_DEFAULT_STATE				0x55AA

/******************************************************************************/
/*!
 * \def ZAPPING_TIME
 * \brief  To define to the time Zapping to be applied
 */
/******************************************************************************/
#define ZAPPING_TIME                            200//50//100

/******************************************************************************/
/*!
 * \def ZAP_SEQUENCE_ZAPPING_TIME
 * \brief  To define to the time Zapping to be applied
 */
/******************************************************************************/
#define ZAP_SEQUENCE_ZAPPING_TIME              250//50//100


/******************************************************************************/
/*!
 * \def CP_FIRMWARE_VERSION_LENGTH
 * \brief  To define the length of the firmware version command
 */
/******************************************************************************/
#define CP_FIRMWARE_VERSION_LENGTH 				6

/******************************************************************************/
/*!
 * \def CP_SYSTEM_STATUS_LENGTH
 * \brief  To define the length of the System HEalth Status command
 */
/******************************************************************************/
#define CP_SYSTEM_STATUS_LENGTH					6

/******************************************************************************/
/*!
 *
 * \def FLOAT_MAX - Used in intializing the threshold for RBC, WBC, PLT
 * \def FLOAT_MIN - Used in intializing the threshold for RBC, WBC, PLT
 * \def FLOAT_ZERO - Used in intializing the threshold for RBC, WBC, PLT
 * \def INT_ZERO - Used in intializing the threshold for RBC, WBC, PLT
 * \def YES - Used in intializing the threshold for RBC, WBC, PLT
 * \def NO - Used in intializing the threshold for RBC, WBC, PLT
 */
/******************************************************************************/
#define FLOAT_MAX							65535//(3.4E+38)
#define FLOAT_MIN							0//(-3.4E+38)
#define FLOAT_ZERO							0//0.00
#define INT_ZERO							0
#define YES									1
#define NO									0

/******************************************************************************/
/*!
 * \def HGB_NO_SAMPLE_ACQ
 * \brief  To define the no. of samples to be acquired for HgB data Acquisition
 */
/******************************************************************************/
#define HGB_NO_SAMPLE_ACQ                   1

/******************************************************************************/
/*!
 * \def MEM_BUFFER_SIZE
 * \brief  To define the SPI packet size to be sent to Processor
 */
/******************************************************************************/
#define MEM_BUFFER_SIZE			512

/******************************************************************************/
/*!
 * \def COMM_QUEUE_LENGTH
 * \brief  To define the queue length of the SPI packet to be sent to Processor
 */
/******************************************************************************/
#define COMM_QUEUE_LENGTH		1

/******************************************************************************/
/*!
 * \def MAX_COMMAND_QUEUE_LENGTH
 * \brief  To define the maximum command that can be inserted into the queue
 *          for transmitting into the queue
 */
/******************************************************************************/
#define MAX_COMMAND_QUEUE_LENGTH       6

/******************************************************************************/
/*!
 * \def VALVE_1
 * \brief  To define the Valve 1 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_1              1

/******************************************************************************/
/*!
 * \def VALVE_2
 * \brief  To define the Valve 2 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_2              2

/******************************************************************************/
/*!
 * \def VALVE_3
 * \brief  To define the Valve 3 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_3              3

/******************************************************************************/
/*!
 * \def VALVE_4
 * \brief  To define the Valve 4 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_4              4

/******************************************************************************/
/*!
 * \def VALVE_5
 * \brief  To define the Valve 5 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_5              5

/******************************************************************************/
/*!
 * \def VALVE_6
 * \brief  To define the Valve 6 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_6              6

/******************************************************************************/
/*!
 * \def VALVE_7
 * \brief  To define the Valve 7 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_7              7

/******************************************************************************/
/*!
 * \def VALVE_8
 * \brief  To define the Valve 8 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_8              8

/******************************************************************************/
/*!
 * \def VALVE_9
 * \brief  To define the Valve 9 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_9              9

/******************************************************************************/
/*!
 * \def VALVE_10
 * \brief  To define the Valve 10 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_10             10

/******************************************************************************/
/*!
 * \def VALVE_11
 * \brief  To define the Valve 11 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_11             11

/******************************************************************************/
/*!
 * \def VALVE_12
 * \brief  To define the Valve 12 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_12             12

/******************************************************************************/
/*!
 * \def VALVE_13
 * \brief  To define the Valve 13 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_13             13

/******************************************************************************/
/*!
 * \def VALVE_14
 * \brief  To define the Valve 14 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_14             14

/******************************************************************************/
/*!
 * \def VALVE_15
 * \brief  To define the Valve 15 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_15             15

/******************************************************************************/
/*!
 * \def VALVE_16
 * \brief  To define the Valve 16 used for controlling specific valve
 */
/******************************************************************************/
#define VALVE_16             16

/******************************************************************************/
/*!
 * \def VALVE_CONTROL_TIME
 * \brief  To define the time to excite the valve - Set to 400ms
 * interval
 */
/******************************************************************************/
#define VALVE_CONTROL_TIME   20  //Set to 400ms interval

/******************************************************************************/
/*!
 * \def VALVE_DELAY_TIME
 * \brief  To define the time to excite the valve - Set to 250
 * interval
 */
/******************************************************************************/
#define VALVE_DELAY_TIME   250

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_PRESSURE_SENSOR_FAIL
 * \brief  To define the threshold to indicate pressure sensor failure
 */
/******************************************************************************/
//#define DEFAULT_SYSTEM_PRESSURE_SENSOR_FAIL             500 //for 15 psi sensor
#define DEFAULT_SYSTEM_PRESSURE_SENSOR_FAIL             500 //for 30 psi sensor

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_PRESSURE_LOWER_LIMIT
 * \brief  To define the lower limit of pressure exerted by the waste syringe
 * when it is idle
 */
/******************************************************************************/
//#define DEFAULT_SYSTEM_PRESSURE_LOWER_LIMIT             800 //for 15 psi sensor
#define DEFAULT_SYSTEM_PRESSURE_LOWER_LIMIT           1530 //for 30 psi sensor

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_PRESSURE_UPPER_LIMIT
 * \brief  To define the upper limit of pressure exerted by the waste syringe
 * when it is idle
 */
/******************************************************************************/
//#define DEFAULT_SYSTEM_PRESSURE_UPPER_LIMIT             3700 //for 15 psi sensor
#define DEFAULT_SYSTEM_PRESSURE_UPPER_LIMIT           2975 // for 30 psi sensor

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_PRESSURE_LOWER_HYST_LIMIT
 * \brief  To define the upper limit of pressure exerted by the waste syringe
 * when it is idle
 */
/******************************************************************************/
//#define DEFAULT_SYSTEM_PRESSURE_LOWER_HYST_LIMIT        2013 //for 15 psi sensor
#define DEFAULT_SYSTEM_PRESSURE_LOWER_HYST_LIMIT      2133 // for 30 psi sensor

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_PRESSURE_UPPER_HYST_LIMIT
 * \brief  To define the upper limit of pressure exerted by the waste syringe
 * when it is idle
 */
/******************************************************************************/
//#define DEFAULT_SYSTEM_PRESSURE_UPPER_HYST_LIMIT        2493 //for 15 psi sensor
#define DEFAULT_SYSTEM_PRESSURE_UPPER_HYST_LIMIT        2373 //for 30 psi sensor

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_TEMPERATURE_LOWER_LIMIT
 * \brief  To define the lower limit of temperature of the system
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_TEMPERATURE_LOWER_LIMIT          819//888-15 degree to  10 degree

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_TEMPERATURE_UPPER_LIMIT
 * \brief  To define the upper limit of temperature of the system
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_TEMPERATURE_UPPER_LIMIT			1775//1366-50 degree//1297 change for 45 degree to 50 degree

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_TEMPERATURE_LOWER_LIMIT
 * \brief  To define the lower limit of temperature of the system
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_TEMP_HYST_LOWER_LIMIT            915

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_TEMPERATURE_UPPER_LIMIT
 * \brief  To define the upper limit of temperature of the system
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_TEMP_HYST_UPPER_LIMIT            1270

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_5_VOLTS_LOWER_LIMIT
 * \brief  To define the lower limit of 5V supply
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_5_VOLTS_LOWER_LIMIT				540

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_5_VOLTS_UPPER_LIMIT
 * \brief  To define the upper limit of 5V supply
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_5_VOLTS_UPPER_LIMIT				597

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_5_VOLTS_HYST_LOWER_LIMIT
 * \brief  To define the lower limit of 5V supply
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_5_VOLTS_HYST_LOWER_LIMIT         552

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_5_VOLTS_HYST_UPPER_LIMIT
 * \brief  To define the upper limit of 5V supply
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_5_VOLTS_HYST_UPPER_LIMIT         586

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_24_VOLTS_LOWER_LIMIT
 * \brief  To define the lower limit of 24V supply
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_24_VOLTS_LOWER_LIMIT				2594

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_24_VOLTS_UPPER_LIMIT
 * \brief  To define the upper limit of 24V supply
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_24_VOLTS_UPPER_LIMIT				2867

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_24_VOLTS_LOWER_LIMIT
 * \brief  To define the lower limit of 24V supply
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_24_VOLTS_HYST_LOWER_LIMIT        2649

/******************************************************************************/
/*!
 * \def DEFAULT_SYSTEM_24_VOLTS_UPPER_LIMIT
 * \brief  To define the upper limit of 24V supply
 */
/******************************************************************************/
#define DEFAULT_SYSTEM_24_VOLTS_HYST_UPPER_LIMIT        2813

/******************************************************************************/
/*!
 * \def MOTOR_FAULT_CONDITION_TIME
 * \brief  To define the Time to check motor fault
 */
/******************************************************************************/
#define  MOTOR_FAULT_CONDITION_TIME                     25

/*!
 * \def statDebugPrintBuf
 * \brief  To print debug messages
 */
/******************************************************************************/
static char statDebugPrintBuf[500];

/******************************************************************************/
/*!
 * \def SET
 * \brief  Defined to set any variable to "1"
 */
/******************************************************************************/
#define SET 							1

/******************************************************************************/
/*!
 * \def RESET
 * \brief  Defined to reset any variable to "0"
 */
/******************************************************************************/
#define RESET 							0

/******************************************************************************/
/*!
 * \def WBC_ACQ_TIME_LOW
 * \brief To define the lower limit of WBC Acquisition time
 * \brief 7.02 second - 1 second = 6.02 Seconds
 */
/******************************************************************************/
#define WBC_ACQ_TIME_LOW                        (g_usnWbcCalibratedTime-75)//250   //DS_added//-50 jojo change for 1.5 sec tolarance

/******************************************************************************/
/*!
 * \def WBC_ACQ_TIME_HIGH
 * \brief To define the upper limit of WBC Acquisition time
 * \brief 7.02 second + 2 second = 9.02 Seconds
 */
/******************************************************************************/
#define WBC_ACQ_TIME_HIGH                       (g_usnWbcCalibratedTime+75)//500   //DS_added//+50  jojo change for 1.5 sec tolarance

/******************************************************************************/
/*!
 * \def RBC_ACQ_TIME_LOW
 * \brief To define the lower limit of RBC Acquisition time
 * \brief 8.46 Second - 1 Second = 7.46 Seconds
 */
/******************************************************************************/
#define RBC_ACQ_TIME_LOW                        (g_usnRbcCalibratedTime-70)//275   //DS_added//-50  jojo change for 1.5 sec tolarance

/******************************************************************************/
/*!
 * \def RBC_ACQ_TIME_HIGH
 * \brief To define the lower limit of RBC Acquisition time
 * \brief 8.46 Second + 2 Second = 10.46 Seconds MAX 10 Seconds
 */
/******************************************************************************/
#define RBC_ACQ_TIME_HIGH                       (g_usnRbcCalibratedTime+75)//500   //DS_added//+50  jojo change for 1.5 sec tolarance

/******************************************************************************/
/*!
 * \def MAX_DATA_ACQUISITION_TIME
 * \brief To define the 10 seconds which is maximum acquisition time for WBC,
 * RBC & PLT
 */
/******************************************************************************/
#define MAX_DATA_ACQUISITION_TIME               500

/******************************************************************************/
/*!
 * \def STATE_THRESHOLD
 * \brief To define the minimum allowable tolerance of the expected time
 * difference of the state in the sequence - Set to 0.5 Seconds
 */
/******************************************************************************/
#define STATE_THRESHOLD                         25

/******************************************************************************/
/*!
 * \def STATE_THRESHOLD
 * \brief To define the minimum allowable tolerance of the expected time
 * difference specific to counting time  - Set to 3 Seconds
 */
/******************************************************************************/
#define COUNTING_TIME_STATE_THRESHOLD           100

/******************************************************************************/
/*!
 * \def READY_FOR_ASPIRATION
 * \brief To define the time gap between the Sequence Starting and ready for Aspiration
 */
/******************************************************************************/
#define READY_FOR_ASPIRATION                    206

/******************************************************************************/
/*!
 * \def ASPIRATION_COMPLETED_TIME
 * \brief To define the time gap between the aspiration starting and completion
 */
/******************************************************************************/
#define ASPIRATION_COMPLETED_TIME               347

/******************************************************************************/
/*!
 * \def FIRST_DILUTION_COMPLETED_TIME
 * \brief To define the time gap between the aspiration completion and
 * WBC dispensing
 */
/******************************************************************************/
#define FIRST_DILUTION_COMPLETED_TIME           528

/******************************************************************************/
/*!
 * \def RBC_DISPENSATION_COMPLETED_TIME
 * \brief To define the time gap between the WBC dispensing and RBC dispensing
 */
/******************************************************************************/
#define RBC_DISPENSATION_COMPLETED_TIME         316

/******************************************************************************/
/*!
 * \def WBC_RBC_BUBBLING_COMPLETED_TIME
 * \brief To define the time gap between the RBC dispensing and WBC &
 * RBC bubbling
 */
/******************************************************************************/
#define WBC_RBC_BUBBLING_COMPLETED_TIME         155

/******************************************************************************/
/*!
 * \def COUNTING_STARTED_TIME
 * \brief To define the time gap between the WBC & RBC bubbling to Counting
 * starting
 */
/******************************************************************************/
#define COUNTING_STARTED_TIME                   463

/******************************************************************************/
/*!
 * \def COUNTING_COMPLETED_TIME
 * \brief To define the time gap between the Counting starting and
 * Counting completion
 */
/******************************************************************************/
#define COUNTING_COMPLETED_TIME                 727

/******************************************************************************/
/*!
 * \def BACKFLUSH_COMPLETED_TIME
 * \brief To define the time gap between the Counting completion and backflush
 */
/******************************************************************************/
#define BACKFLUSH_COMPLETED_TIME                208

/******************************************************************************/
/*!
 * \def BATH_FILLING_COMPLETED_TIME
 * \brief To define the time gap between the backflush and bath filling
 */
/******************************************************************************/
#define BATH_FILLING_COMPLETED_TIME             515 //648

/******************************************************************************/
/*!
 * \def READY_FOR_ASPIRATION_HIGH
 * \brief To define the upper limit of Ready for Aspiration time
 */
/******************************************************************************/
#define READY_FOR_ASPIRATION_HIGH               READY_FOR_ASPIRATION + \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def READY_FOR_ASPIRATION_LOW
 * \brief To define the lower limit of WBC dispensation completed time
 */
/******************************************************************************/
#define READY_FOR_ASPIRATION_LOW                READY_FOR_ASPIRATION - \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def ASPIRATION_COMPLETED_TIME_LOW
 * \brief To define the lower limit of Aspiration completed time
 */
/******************************************************************************/
#define ASPIRATION_COMPLETED_TIME_LOW           ASPIRATION_COMPLETED_TIME  - \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def ASPIRATION_COMPLETED_TIME_HIGH
 * \brief To define the upper limit of Aspiration completed time
 */
/******************************************************************************/
#define ASPIRATION_COMPLETED_TIME_HIGH           ASPIRATION_COMPLETED_TIME  + \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def FIRST_DILUTION_COMPLETED_TIME_LOW
 * \brief To define the lower limit of WBC dispensation completed time
 */
/******************************************************************************/
#define FIRST_DILUTION_COMPLETED_TIME_LOW     FIRST_DILUTION_COMPLETED_TIME\
                                                - STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def FIRST_DILUTION_COMPLETED_TIME_HIGH
 * \brief To define the upper limit of WBC dispensation completed time
 */
/******************************************************************************/
#define FIRST_DILUTION_COMPLETED_TIME_HIGH    FIRST_DILUTION_COMPLETED_TIME\
                                                + STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def RBC_DISPENSATION_COMPLETED_TIME_LOW
 * \brief To define the lower limit of RBC dispensation completed time
 */
/******************************************************************************/
#define RBC_DISPENSATION_COMPLETED_TIME_LOW     RBC_DISPENSATION_COMPLETED_TIME\
                                                - STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def RBC_DISPENSATION_COMPLETED_TIME_HIGH
 * \brief To define the upper limit of RBC dispensation completed time
 */
/******************************************************************************/
#define RBC_DISPENSATION_COMPLETED_TIME_HIGH    RBC_DISPENSATION_COMPLETED_TIME\
                                                + STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def BUBBLING_COMPLETED_TIME_LOW
 * \brief To define the lower limit of WBC & RBC bubbling time
 */
/******************************************************************************/
#define BUBBLING_COMPLETED_TIME_LOW             WBC_RBC_BUBBLING_COMPLETED_TIME\
                                                - STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def BUBBLING_COMPLETED_TIME_HIGH
 * \brief To define the upper limit of WBC & RBC bubbling time
 */
/******************************************************************************/
#define BUBBLING_COMPLETED_TIME_HIGH            WBC_RBC_BUBBLING_COMPLETED_TIME\
                                                + STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def COUNTING_STARTED_TIME_LOW
 * \brief To define the lower limit of Counting Started time
 */
/******************************************************************************/
#define COUNTING_STARTED_TIME_LOW               COUNTING_STARTED_TIME - \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def COUNTING_STARTED_TIME_HIGH
 * \brief To define the upper limit of Counting Started time
 */
/******************************************************************************/
#define COUNTING_STARTED_TIME_HIGH              COUNTING_STARTED_TIME + \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def COUNTING_COMPLETED_TIME_LOW
 * \brief To define the lower limit of Counting Completed time
 */
/******************************************************************************/
#define COUNTING_COMPLETED_TIME_LOW             COUNTING_COMPLETED_TIME - \
                                                COUNTING_TIME_STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def COUNTING_COMPLETED_TIME_HIGH
 * \brief To define the upper limit of Counting Completed time
 */
/******************************************************************************/
#define COUNTING_COMPLETED_TIME_HIGH            COUNTING_COMPLETED_TIME + \
                                                COUNTING_TIME_STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def BACKFLUSH_COMPLETED_TIME_LOW
 * \brief To define the upper limit of backflush completed time
 */
/******************************************************************************/
#define BACKFLUSH_COMPLETED_TIME_LOW            BACKFLUSH_COMPLETED_TIME - \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def BACKFLUSH_COMPLETED_TIME_HIGH
 * \brief To define the upper limit of backflush completed time
 */
/******************************************************************************/
#define BACKFLUSH_COMPLETED_TIME_HIGH           BACKFLUSH_COMPLETED_TIME + \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def BATH_FILLING_COMPLETED_TIME_LOW
 * \brief To define the lower limit of bath filling completed time
 */
/******************************************************************************/
#define BATH_FILLING_COMPLETED_TIME_LOW         BATH_FILLING_COMPLETED_TIME - \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def BATH_FILLING_COMPLETED_TIME_HIGH
 * \brief To define the upper limit of bath filling completed time
 */
/******************************************************************************/
#define BATH_FILLING_COMPLETED_TIME_HIGH        BATH_FILLING_COMPLETED_TIME + \
                                                STATE_THRESHOLD

/******************************************************************************/
/*!
 * \def REAGENT_PRIME_MON_TIME
 * \brief To define the monitoring time of the reagents - It is set to 3 seconds
 */
/******************************************************************************/
#define REAGENT_PRIME_MON_TIME                  25

//PH_CHANGE_07_MAR_17 - START
/******************************************************************************/
/*!
 * \def MAX_X_DISTANCE
 * \brief To define maximum distance X-Axis can move
 */
/******************************************************************************/
#define MAX_X_DISTANCE                          82.75

/******************************************************************************/
/*!
 * \def X_POS_MIN
 * \brief To define maximum distance X-Axis can move on the left side
 */
/******************************************************************************/
#define X_POS_MIN                               -37.75

/******************************************************************************/
/*!
 * \def X_POS_MAX
 * \brief To define maximum distance X-Axis can move on the right side
 */
/******************************************************************************/
#define X_POS_MAX                               45

/******************************************************************************/
/*!
 * \def X_HOME_POS_DETECT
 * \brief To define maximum home position detection location for X-Axis
 */
/******************************************************************************/
#define X_HOME_POS_DETECT                       -9.5

/******************************************************************************/
/*!
 * \def X_HOME_POS_NOT_DETECT
 * \brief To define minimum home position not detecting location for X-Axis
 */
/******************************************************************************/
#define X_HOME_POS_NOT_DETECT                   -15.5

/******************************************************************************/
/*!
 * \def MAX_DILUENT_DISTANCE
 * \brief To define maximum distance Diluent Syringe can move
 */
/******************************************************************************/
#define MAX_DILUENT_DISTANCE                    60

/******************************************************************************/
/*!
 * \def MAX_SAMPLE_DISTANCE
 * \brief To define maximum distance Sample Lyse Syringe can move
 */
/******************************************************************************/
#define MAX_SAMPLE_DISTANCE                     30

/******************************************************************************/
/*!
 * \def MAX_WASTE_DISTANCE
 * \brief To define maximum distance Waste Syringe can move
 */
/******************************************************************************/
#define MAX_WASTE_DISTANCE                      55

/******************************************************************************/
/*!
 * \def MAX_Y_AXIS_DISTANCE
 * \brief To define maximum distance Y-Axis can move
 */
/******************************************************************************/
#define MAX_Y_AXIS_DISTANCE                     90

/******************************************************************************/
/*!
 * \def MAX_X_DISTANCE_PULSE
 * \brief To define maximum no. of pulse that can be given to move maximum
 * distance that X-Axis can accomodate
 */
/******************************************************************************/
#define MAX_X_DISTANCE_PULSE                    MAX_X_DISTANCE / MOTOR1_STEP_SIZE

/******************************************************************************/
/*!
 * \def X_POS_MIN_PULSE
 * \brief To define minimum no. of pulses given to X-Axis so that X-Axis is at
 * left most position
 */
/******************************************************************************/
#define X_POS_MIN_PULSE                         X_POS_MIN / MOTOR1_STEP_SIZE

/******************************************************************************/
/*!
 * \def X_POS_MAX_PULSE
 * \brief To define maximum no. of pulses given to X-Axis so that X-Axis is at
 * right most position
 */
/******************************************************************************/
#define X_POS_MAX_PULSE                         X_POS_MAX / MOTOR1_STEP_SIZE

/******************************************************************************/
/*!
 * \def X_HOME_POS_DETECT_PULSE
 * \brief To define maximum no. of pulses so that home position of X-Axis
 * is detected
 */
/******************************************************************************/
#define X_HOME_POS_DETECT_PULSE                 X_HOME_POS_DETECT / MOTOR1_STEP_SIZE

/******************************************************************************/
/*!
 * \def X_HOME_POS_NOT_DETECT_PULSE
 * \brief To define minimum no. of pulses so that home position of X-Axis
 * is not detected
 */
/******************************************************************************/
#define X_HOME_POS_NOT_DETECT_PULSE             X_HOME_POS_NOT_DETECT / MOTOR1_STEP_SIZE

/******************************************************************************/
/*!
 * \def MAX_DILUENT_DISTANCE_PULSE
 * \brief To define maximum no. of pulse that can be given to move maximum
 * distance that Diluent syringe can accomodate
 */
/******************************************************************************/
#define MAX_DILUENT_DISTANCE_PULSE              MAX_DILUENT_DISTANCE / MOTOR2_STEP_SIZE

/******************************************************************************/
/*!
 * \def MAX_SAMPLE_DISTANCE_PULSE
 * \brief To define maximum no. of pulse that can be given to move maximum
 * distance that Sample syringe can accomodate
 */
/******************************************************************************/
#define MAX_SAMPLE_DISTANCE_PULSE               MAX_SAMPLE_DISTANCE / MOTOR3_STEP_SIZE

/******************************************************************************/
/*!
 * \def MAX_WASTE_DISTANCE_PULSE
 * \brief To define maximum no. of pulse that can be given to move maximum
 * distance that Waste syringe can accomodate
 */
/******************************************************************************/
#define MAX_WASTE_DISTANCE_PULSE                MAX_WASTE_DISTANCE / MOTOR4_STEP_SIZE

/******************************************************************************/
/*!
 * \def MAX_Y_AXIS_DISTANCE_PULSE
 * \brief To define maximum no. of pulse that can be given to move maximum
 * distance that Y-Axis can accomodate
 */
/******************************************************************************/
#define MAX_Y_AXIS_DISTANCE_PULSE               MAX_Y_AXIS_DISTANCE / MOTOR5_STEP_SIZE
//PH_CHANGE_07_MAR_17 - END

/******************************************************************************/
/*!
 * \def DRAINING_BC_STARTED_PRESSURE
 * \brief To define ADC value of the starting pressure/vacuum of WBC bath
 * Draining, before counting
 */
/******************************************************************************/
#define DRAINING_BC_STARTED_PRESSURE            1

/******************************************************************************/
/*!
 * \def DRAINING_BC_INTER_STARTED_PRESSURE
 * \brief To define ADC value of the starting pressure/vacuum of RBC bath
 * Draining, before counting
 */
/******************************************************************************/
#define DRAINING_BC_INTER_STARTED_PRESSURE      1

/******************************************************************************/
/*!
 * \def DRAINING_BC_COMPLETED_PRESSURE
 * \brief To define ADC value of the pressure/vacuum of WBC & RBC bath Draining
 * completion, before counting
 */
/******************************************************************************/
#define DRAINING_BC_COMPLETED_PRESSURE          1

/******************************************************************************/
/*!
 * \def WBC_BUBBLING_STARTED_PRESSURE
 * \brief To define ADC value of the starting pressure/vacuum of WBC bath
 * bubbling before adding lyse
 */
/******************************************************************************/
#define WBC_BUBBLING_STARTED_PRESSURE           1

/******************************************************************************/
/*!
 * \def WBC_BUBBLING_COMPLETED_PRESSURE
 * \brief To define ADC value of the Ending pressure/vacuum of WBC bath bubbling
 * before adding lyse
 */
/******************************************************************************/
#define WBC_BUBBLING_COMPLETED_PRESSURE         1

/******************************************************************************/
/*!
 * \def WBC_BUBBLING_COMPLETED_PRESSURE
 * \brief To define ADC value of the starting pressure/vacuum of WBC bath
 * bubbling after adding lyse
 */
/******************************************************************************/
#define WBC_RBC_BUBBLING_STARTED_PRESSURE       1

/******************************************************************************/
/*!
 * \def WBC_RBC_BUBBLING_INTER_STARTED_PRESSURE
 * \brief To define ADC value of the starting pressure/vacuum of RBC bath
 * bubbling after adding lyse
 */
/******************************************************************************/
#define WBC_RBC_BUBBLING_INTER_STARTED_PRESSURE 1

/******************************************************************************/
/*!
 * \def WBC_RBC_BUBBLING_COMPLETED_PRESSURE
 * \brief To define ADC value of the ending pressure/vacuum of RBC bath
 * bubbling after adding lyse
 */
/******************************************************************************/
#define WBC_RBC_BUBBLING_COMPLETED_PRESSURE     1

/******************************************************************************/
/*!
 * \def COUNTING_STARTED_PRESSURE_LOW
 * \brief To define ADC value of the minimum pressure/vacuum required for
 * starting counting
 */
/******************************************************************************/
#define COUNTING_STARTED_PRESSURE_LOW           1

/******************************************************************************/
/*!
 * \def COUNTING_STARTED_PRESSURE_HIGH
 * \brief To define ADC value of the maximum pressure/vacuum required for
 * starting counting
 */
/******************************************************************************/
#define COUNTING_STARTED_PRESSURE_HIGH          1

/******************************************************************************/
/*!
 * \def COUNTING_COMPLETED_PRESSURE_LOW
 * \brief To define ADC value of the minimum pressure/vacuum required for
 * ending counting
 */
/******************************************************************************/
#define COUNTING_COMPLETED_PRESSURE_LOW         1

/******************************************************************************/
/*!
 * \def COUNTING_COMPLETED_PRESSURE_HIGH
 * \brief To define ADC value of the maximum pressure/vacuum required for
 * ending counting
 */
/******************************************************************************/
#define COUNTING_COMPLETED_PRESSURE_HIGH        1

/******************************************************************************/
/*!
 * \def DRAINING_AC_STARTED_PRESSURE
 * \brief To define ADC value of the starting pressure/vacuum of WBC bath
 * Draining, after counting
 */
/******************************************************************************/
#define DRAINING_AC_STARTED_PRESSURE            1

/******************************************************************************/
/*!
 * \def DRAINING_AC_INTER_STARTED_PRESSURE
 * \brief To define ADC value of the starting pressure/vacuum of RBC bath
 * Draining, after counting
 */
/******************************************************************************/
#define DRAINING_AC_INTER_STARTED_PRESSURE      1

/******************************************************************************/
/*!
 * \def DRAINING_AC_COMPLETED_PRESSURE
 * \brief To define ADC value of the pressure/vacuum of WBC & RBC bath Draining
 * completion, after counting
 */
/******************************************************************************/
#define DRAINING_AC_COMPLETED_PRESSURE          1

/******************************************************************************/
/*!
 * \def BACKFLUSH_STARTED_PRESSURE
 * \brief To define ADC value of the pressure/vacuum of starting backflush
 */
/******************************************************************************/
#define BACKFLUSH_STARTED_PRESSURE              1

/******************************************************************************/
/*!
 * \def BACKFLUSH_STARTED_PRESSURE
 * \brief To define ADC value of the pressure/vacuum of ending backflush
 */
/******************************************************************************/
#define BACKFLUSH_COMPLETED_PRESSURE            1

/******************************************************************************/
/*!
 * \def DRAINING_FINAL_STARTED_PRESSURE
 * \brief To define ADC value of the starting pressure/vacuum of WBC bath
 * final draining, after counting
 */
/******************************************************************************/
#define DRAINING_FINAL_STARTED_PRESSURE         1

/******************************************************************************/
/*!
 * \def DRAINING_FINAL_INTER_STARTED_PRESSURE
 * \brief To define ADC value of the starting pressure/vacuum of RBC bath
 * final draining, after counting
 */
/******************************************************************************/
#define DRAINING_FINAL_INTER_STARTED_PRESSURE   1

/******************************************************************************/
/*!
 * \def DRAINING_FINAL_COMPLETED_PRESSURE
 * \brief To define ADC value of the pressure/vacuum of WBC & RBC bath
 * final Draining completion, after counting
 */
/******************************************************************************/
#define DRAINING_FINAL_COMPLETED_PRESSURE       1

/******************************************************************************/
/*!
 * \def CONTROLLER_MON_TIME_HIGH
 * \brief To define controller monitoring GPIO to be set to high
 */
/******************************************************************************/
#define CONTROLLER_MON_TIME_HIGH                150

/******************************************************************************/
/*!
 * \def CONTROLLER_MON_TIME_LOW
 * \brief To define controller monitoring GPIO to be set to low
 */
/******************************************************************************/
#define CONTROLLER_MON_TIME_LOW                 (CONTROLLER_MON_TIME_HIGH * 2)
//PH_NOV_02 - END

/******************************************************************************/
/*!
 * \def FIRST_DILUTION_FREQ
 * \brief To define the position of First Dilution frequency Position in the array
 */
/******************************************************************************/
#define FIRST_DILUTION_FREQ                     0

/******************************************************************************/
/*!
 * \def LYSE_MIXING_FREQ
 * \brief To define the position of Lyse Mixing frequency Position in the array
 */
/******************************************************************************/
#define LYSE_MIXING_FREQ                        1

/******************************************************************************/
/*!
 * \def FINAL_RBC_MIXING_FREQ
 * \brief To define the position of RBC Mixing frequency Position in the array
 */
/******************************************************************************/
#define FINAL_RBC_MIXING_FREQ                   2

/******************************************************************************/
/*!
 * \def ENABLE_RE_ACQ
 * \brief To enable re-acquisition in case of any volumetric error condition
 */
/******************************************************************************/
#define ENABLE_RE_ACQ                           1

/******************************************************************************/
/*!
 * \def DISABLE_RE_ACQ
 * \brief To disable re-acquisition in when no error occurred during acquisition
 * in volumetric tube or if the volumetric error is occurred for the second time
 */
/******************************************************************************/
#define DISABLE_RE_ACQ                          0

/******************************************************************************/
/*!
 * \def FIRST_DILUTION_FREQ_DEFAULT
 * \brief To define the default value of First Dilution mixing frequency
 */
/******************************************************************************/
#define FIRST_DILUTION_FREQ_DEFAULT             300   //HN_Change

/******************************************************************************/
/*!
 * \def LYSE_MIXING_FREQ_DEFAULT
 * \brief To define the default value of Lyse mixing frequency
 */
/******************************************************************************/
#define LYSE_MIXING_FREQ_DEFAULT                200

/******************************************************************************/
/*!
 * \def FINAL_RBC_MIXING_FREQ_DEFAULT
 * \brief To define the default value of Final RBC mixing frequency
 */
/******************************************************************************/
#define FINAL_RBC_MIXING_FREQ_DEFAULT           100//200

/******************************************************************************/
/*!
 * \def MIN_MIXING_FREQ
 * \brief To define the minimum mixing frequency
 */
/******************************************************************************/
#define MIN_MIXING_FREQ                         100

/******************************************************************************/
/*!
 * \def MAX_MIXING_FREQ
 * \brief To define the minimum mixing frequency
 */
/******************************************************************************/
#define MAX_MIXING_FREQ                         300

/******************************************************************************/
/*!
 * \def RBC_PROCESSING_TIME
 * \brief To define the RBC data processing time
 */
/******************************************************************************/
#define RBC_PROCESSING_TIME                     5000

/******************************************************************************/
/*!
 * \def PLT_PROCESSING_TIME
 * \brief To define the PLT data processing time
 */
/******************************************************************************/
#define PLT_PROCESSING_TIME                     6000

/******************************************************************************/
/*!
 * \def LIQUID_SENSOR_ON
 * \brief To define the liquid sensing is On
 */
/******************************************************************************/
#define LIQUID_SENSOR_ON                        0

/******************************************************************************/
/*!
 * \def LIQUID_SENSOR_OFF
 * \brief To define the liquid sensing is Off
 */
/******************************************************************************/
#define LIQUID_SENSOR_OFF                       1
#endif /* INCLUDE_DEFINES_H_ */

/******************************************************************************/
/*! \file StepperMotorDriver.h
 *
 *  \brief Header file for StepperMotorDriver.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-25-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_motor_module
//! @{
//******************************************************************************
#ifndef STEPPER_MOTOR_DRIVER_H_
#define STEPPER_MOTOR_DRIVER_H_

#include "BloodCellCounter.h"
#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File



/******************************************************************************/
/*!
 *
 * \struct MotorParameter
 * \brief struct MOTOR_PARAMETERS
 */
/******************************************************************************/
/*typedef struct MOTOR_PARAMETERS
{

	Uint16 StepFreq;
	Uint16 MicroStep[3];
	Uint16 EnableDisable;
	Uint16 Direction;
	Uint16 Decay;
	Uint16 Fault;
	Uint16 Reset;
}MotorPrameters;*/


/******************************************************************************/
/*!
 * \def   M_RESET_DEFAULT
 * \brief deafult value for motor reset
 */
/******************************************************************************/
#define 	M_RESET_DEFAULT				1


//#define 	M1_STEP				PWM8A

/******************************************************************************/
/*!
 * \def   M1_MODE0_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M1_MODE0_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M1_MODE1_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M1_MODE1_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M1_MODE2_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M1_MODE2_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M1_IN1_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M1_IN1_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M1_DIR_DEFAULT
 * \brief Default value for motor 1 direction
 */
/******************************************************************************/
#define 	M1_DIR_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M1_DECAY_DEFAULT
 * \brief Default value for motor 1 current decay
 */
/******************************************************************************/
#define 	M1_DECAY_DEFAULT			1

/******************************************************************************/
/*!
 * \def   M1_FAULT_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M1_FAULT_DEFAULT			1



//#define 	M2_STEP				PWM6A
/******************************************************************************/
/*!
 * \def   M2_MODE0_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M2_MODE0_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M2_MODE1_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M2_MODE1_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M2_MODE2_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M2_MODE2_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M2_ENBL_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M2_ENBL_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M2_DIR_DEFAULT
 * \brief Default value for motor 2 direction
 */
/******************************************************************************/
#define 	M2_DIR_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M2_DECAY_DEFAULT
 * \brief Default value for motor 2 current decay
 */
/******************************************************************************/
#define 	M2_DECAY_DEFAULT			1

/******************************************************************************/
/*!
 * \def   M2_FAULT_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M2_FAULT_DEFAULT			1


//#define 	M3_STEP				PWM2A
/******************************************************************************/
/*!
 * \def   M3_MODE0_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M3_MODE0_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M3_MODE1_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M3_MODE1_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M3_MODE2_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M3_MODE2_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M3_ENBL_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M3_ENBL_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M3_DIR_DEFAULT
 * \brief Default value for motor 3 direction
 */
/******************************************************************************/
#define 	M3_DIR_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M3_DECAY_DEFAULT
 * \brief Default value for motor 3 current decay
 */
/******************************************************************************/
#define 	M3_DECAY_DEFAULT			1

/******************************************************************************/
/*!
 * \def   M3_FAULT_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M3_FAULT_DEFAULT			1


//#define 	M4_STEP				PWM5A
/******************************************************************************/
/*!
 * \def   M4_MODE0_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M4_MODE0_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M4_MODE1_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M4_MODE1_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M4_MODE2_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M4_MODE2_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M4_ENBL_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M4_ENBL_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M4_DIR_DEFAULT
 * \brief Default value for motor 4 direction
 */
/******************************************************************************/
#define 	M4_DIR_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M4_DECAY_DEFAULT
 * \brief Default value for motor 4 current decay
 */
/******************************************************************************/
#define 	M4_DECAY_DEFAULT			1

/******************************************************************************/
/*!
 * \def   M4_FAULT_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M4_FAULT_DEFAULT			1


//#define 	M5_STEP				PWM1A
/******************************************************************************/
/*!
 * \def   M5_MODE0_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M5_MODE0_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M5_MODE1_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M5_MODE1_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M5_MODE2_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M5_MODE2_DEFAULT			0

/******************************************************************************/
/*!
 * \def   M5_ENBL_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M5_ENBL_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M5_DIR_DEFAULT
 * \brief Default value for motor 5 direction
 */
/******************************************************************************/
#define 	M5_DIR_DEFAULT				1

/******************************************************************************/
/*!
 * \def   M5_DECAY_DEFAULT
 * \brief Default value for motor 5 current decay
 */
/******************************************************************************/
#define 	M5_DECAY_DEFAULT			1

/******************************************************************************/
/*!
 * \def   M5_FAULT_DEFAULT
 * \brief Not used
 */
/******************************************************************************/
#define 	M5_FAULT_DEFAULT			1

/******************************************************************************/
/*!
 * \def   MOTOR_ENABLE
 * \brief To enable individual motors
 */
/******************************************************************************/
#define 	MOTOR_ENABLE				0

/******************************************************************************/
/*!
 * \def   MOTOR_DISABLE
 * \brief To disable individual motors
 */
/******************************************************************************/
#define 	MOTOR_DISABLE				1

/******************************************************************************/
/*!
 * \brief Function prototypes:
 * \fn void Configure_Motor1(void) - not used
 * \fn void Configure_Motor2(void) - not used
 * \fn void Configure_Motor3(void) - not used
 * \fn void Configure_Motor4(void) - not used
 * \fn void Configure_Motor5(void) - not used
 * \fn void SMDStart_Motor1(unsigned int  usnDisplacement, unsigned int usnDirection,
 *                          unsigned int StepFrequency);
 * \brief Function start motor 1 using Displacement, Direction and Frequency
 *      parameters
 * \fn void SMDStart_Motor2(unsigned int  usnDisplacement, unsigned int usnDirection,
 *                          unsigned int StepFrequency);
 * \brief Function start motor 2 using Displacement, Direction and Frequency
 *      parameters
 * \fn void SMDStart_Motor3(unsigned int  usnDisplacement, unsigned int usnDirection,
 *                          unsigned int StepFrequency);
 * \brief Function start motor 3 using Displacement, Direction and Frequency
 *      parameters
 * \fn void SMDStart_Motor4(unsigned int  usnDisplacement, unsigned int usnDirection,
 *                          unsigned int StepFrequency);
 * \brief Function start motor 4 using Displacement, Direction and Frequency
 *      parameters
 * \fn void SMDStart_Motor5(unsigned int  usnDisplacement, unsigned int usnDirection,
 *                          unsigned int StepFrequency);
 * \brief Function start motor 5 using Displacement, Direction and Frequency
 *      parameters
 * \fn void ConfigureMotor1Direction(unsigned int usnDirection) - not used
 * \fn void ConfigureMotor2Direction(unsigned int usnDirection) - not used
 * \fn void ConfigureMotor3Direction(unsigned int usnDirection) - not used
 * \fn void ConfigureMotor4Direction(unsigned int usnDirection) - not used
 * \fn void ConfigureMotor5Direction(unsigned int usnDirection) - not used
 * \fn unsigned int CheckFaultStauts_Motor1(void) - not used
 * \fn unsigned int CheckFaultStauts_Motor2(void) - not used
 * \fn unsigned int CheckFaultStauts_Motor3(void) - not used
 * \fn unsigned int CheckFaultStauts_Motor4(void) - not used
 * \fn unsigned int CheckFaultStauts_Motor5(void) - not used
 * \fn void Config_Motor1_GPIO(unsigned int Step) - not used
 * \fn void Config_Motor2_GPIO(unsigned int Step) - not used
 * \fn void Config_Motor3_GPIO(unsigned int Step) - not used
 * \fn void Config_Motor4_GPIO(unsigned int Step) - not used
 * \fn void Config_Motor5_GPIO(unsigned int Step) - not used
 * \fn void Config_Motor1_Step(unsigned int Step) - not used
 * \fn void Config_Motor2_Step(unsigned int Step) - not used
 * \fn void Config_Motor3_Step(unsigned int Step) - not used
 * \fn void Config_Motor4_Step(unsigned int Step) - not used
 * \fn void Config_Motor5_Step(unsigned int Step) - not used
 * \fn void Disable_Motor1(void) - not used
 * \fn void Disable_Motor2(void) - not used
 * \fn void Disable_Motor3(void) - not used
 * \fn void Disable_Motor4(void) - not used
 * \fn void Disable_Motor5(void) - not used
 * \fn void Enable_Motor1(void) - not used
 * \fn void Enable_Motor2(void) - not used
 * \fn void Enable_Motor3(void) - not used
 * \fn void Enable_Motor4(void) - not used
 * \fn void Enable_Motor5(void) - not used
 * \fn void MotorReset_GpioConfig(void) - not used
 * \fn void SMDGpioConfig_MotorDefaultState(void)
 * \brief Function configures for Motor GPIO pin to default state
 * \fn void SMDInitialize_MotorGpio(void)
 * \brief Function configures gpio mux and resgistery for Motor GPIO pin
 * \fn void SMDSetMotorStepSize(Uint16 Motor, Uint16 StepSize)
 * \brief Function configures motor step size
 * \fn void SMDSetMotorEnable(Uint16 Motor)
 * \brief Function configures enable gpio pin to enable the motor
 * \fn void SMDSetMotorDisable(Uint16 Motor)
 * \brief Function configures gpio pin to disable the motor
 * \fn void SMDSetMotorDirection(Uint16 Motor, Uint16 Direction)
 * \brief Function configures diection of gpio pin of the motor
 * \fn void SMDSetMotorDecay(Uint16 Motor, Uint16 Decay)
 * \brief Function configures decay of gpio pin of the motor
 * \fn void SMDMotorReset(void)
 * \brief Function resets the Motor
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     void Configure_Motor1(void)
 *  \brief  Not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Configure_Motor1(void);

/******************************************************************************/
/*!
 *  \fn     void Configure_Motor2(void)
 *  \brief  Not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Configure_Motor2(void);

/******************************************************************************/
/*!
 *  \fn      void Configure_Motor3(void)
 *  \brief   Not used
 *  \param   void
 *  \return  void
 */
/******************************************************************************/
void Configure_Motor3(void);

/******************************************************************************/
/*!
 *  \fn     void Configure_Motor4(void)
 *  \brief  Not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Configure_Motor4(void);

/******************************************************************************/
/*!
 *  \fn     void Configure_Motor5(void)
 *  \brief  Not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Configure_Motor5(void);

/******************************************************************************/
/*!
 *  \fn     void SMDStart_Motor1(unsigned int  , unsigned int , unsigned int )
 *  \brief  This function start motor 1 using Displacement, Direction and Frequency
 *          parameters
 *  \param  unsigned int,unsigned int,unsigned int
 *  \return void
 */
/******************************************************************************/
void SMDStart_Motor1(unsigned int  , unsigned int , unsigned int );

/******************************************************************************/
/*!
 *  \fn     void SMDStart_Motor2(unsigned int  , unsigned int , unsigned int )
 *  \brief  This function start motor 2 using Displacement, Direction and Frequency
 *          parameters
 *  \param  unsigned int,unsigned int,unsigned int
 *  \return void
 */
/******************************************************************************/
void SMDStart_Motor2(unsigned int  , unsigned int , unsigned int );

/******************************************************************************/
/*!
 *  \fn     void SMDStart_Motor3(unsigned int  , unsigned int , unsigned int )
 *  \brief  This function start motor 3 using Displacement, Direction and Frequency
 *          parameters
 *  \param  unsigned int,unsigned int,unsigned int
 *  \return void
 */
/******************************************************************************/
void SMDStart_Motor3(unsigned int  , unsigned int , unsigned int );

/******************************************************************************/
/*!
 *  \fn     void SMDStart_Motor4(unsigned int  , unsigned int , unsigned int )
 *  \brief  This function start motor 4 using Displacement, Direction and Frequency
 *          parameters
 *  \param  unsigned int,unsigned int,unsigned int
 *  \return void
 */
/******************************************************************************/
void SMDStart_Motor4(unsigned int  , unsigned int , unsigned int );

/******************************************************************************/
/*!
 *  \fn     void SMDStart_Motor5(unsigned int  , unsigned int , unsigned int )
 *  \brief  This function start motor 5 using Displacement, Direction and Frequency
 *          parameters
 *  \param  unsigned int,unsigned int,unsigned int
 *  \return void
 */
/******************************************************************************/
void SMDStart_Motor5(unsigned int  , unsigned int , unsigned int );


/******************************************************************************/
/*!
 *  \fn     void ConfigureMotor1Direction(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void ConfigureMotor1Direction(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void ConfigureMotor2Direction(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void ConfigureMotor2Direction(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void ConfigureMotor3Direction(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void ConfigureMotor3Direction(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void ConfigureMotor4Direction(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void ConfigureMotor4Direction(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void ConfigureMotor5Direction(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void ConfigureMotor5Direction(unsigned int );

/******************************************************************************/
/*!
 *  \fn     unsigned int CheckFaultStauts_Motor1(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
unsigned int CheckFaultStauts_Motor1(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int CheckFaultStauts_Motor2(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
unsigned int CheckFaultStauts_Motor2(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int CheckFaultStauts_Motor3(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
unsigned int CheckFaultStauts_Motor3(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int CheckFaultStauts_Motor4(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
unsigned int CheckFaultStauts_Motor4(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int CheckFaultStauts_Motor5(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
unsigned int CheckFaultStauts_Motor5(void);


/******************************************************************************/
/*!
 *  \fn     void Config_Motor1_GPIO(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor1_GPIO(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void Config_Motor2_GPIO(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor2_GPIO(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void Config_Motor3_GPIO(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor3_GPIO(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void Config_Motor4_GPIO(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor4_GPIO(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void Config_Motor5_GPIO(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor5_GPIO(unsigned int );


/******************************************************************************/
/*!
 *  \fn     void Config_Motor1_Step(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor1_Step(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void Config_Motor2_Step(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor2_Step(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void Config_Motor3_Step(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor3_Step(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void Config_Motor4_Step(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor4_Step(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void Config_Motor5_Step(unsigned int )
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Config_Motor5_Step(unsigned int );

/******************************************************************************/
/*!
 *  \fn     void Disable_Motor1(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Disable_Motor1(void);

/******************************************************************************/
/*!
 *  \fn     void Disable_Motor2(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Disable_Motor2(void);

/******************************************************************************/
/*!
 *  \fn     void Disable_Motor3(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Disable_Motor3(void);

/******************************************************************************/
/*!
 *  \fn     void Disable_Motor4(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Disable_Motor4(void);

/******************************************************************************/
/*!
 *  \fn     void Disable_Motor5(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Disable_Motor5(void);

/******************************************************************************/
/*!
 *  \fn     void Enable_Motor1(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Enable_Motor1(void);

/******************************************************************************/
/*!
 *  \fn     void Enable_Motor2(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Enable_Motor2(void);

/******************************************************************************/
/*!
 *  \fn     void Enable_Motor3(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Enable_Motor3(void);

/******************************************************************************/
/*!
 *  \fn     void Enable_Motor4(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Enable_Motor4(void);

/******************************************************************************/
/*!
 *  \fn     void Enable_Motor5(void)
 *  \brief  not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Enable_Motor5(void);


/******************************************************************************/
/*!
 *  \fn     void MotorReset_GpioConfig(void)
 *  \brief  not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void MotorReset_GpioConfig(void);

/******************************************************************************/
/*!
 *  \fn     void SMDGpioConfig_MotorDefaultState(void)
 *  \brief  This function configures for Motor GPIO pin to default state
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SMDGpioConfig_MotorDefaultState(void);

/******************************************************************************/
/*!
 *  \fn     void SMDInitialize_MotorGpio(void)
 *  \brief  This function configures gpio mux and resgistery for Motor GPIO pin
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SMDInitialize_MotorGpio(void);

/******************************************************************************/
/*!
 *  \fn     void SMDSetMotorStepSize(Uint16 Motor, Uint16 StepSize)
 *  \brief  This function configures motor step size
 *  \param  Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void SMDSetMotorStepSize(Uint16 Motor, Uint16 StepSize);

/******************************************************************************/
/*!
 *  \fn     void SMDSetMotorEnable(Uint16 Motor)
 *  \brief  This function configures enable gpio pin to enable the motor
 *  \param  Uint16
 *  \return void
 */
/******************************************************************************/
void SMDSetMotorEnable(Uint16 Motor);

/******************************************************************************/
/*!
 *  \fn     void SMDSetMotorDisable(Uint16 Motor)
 *  \brief  This function configures enable gpio pin to disable the motor
 *  \param  Uint16
 *  \return void
 */
/******************************************************************************/
void SMDSetMotorDisable(Uint16 Motor);

/******************************************************************************/
/*!
 *  \fn     void SMDSetMotorDirection(Uint16 Motor, Uint16 Direction)
 *  \brief  This function configures diection of gpio pin of the motor
 *  \param  Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void SMDSetMotorDirection(Uint16 Motor, Uint16 Direction);

/******************************************************************************/
/*!
 *  \fn     void SMDSetMotorDecay(Uint16 Motor, Uint16 Decay)
 *  \brief  This function configures decay of gpio pin of the motor
 *  \param  Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void SMDSetMotorDecay(Uint16 Motor, Uint16 Decay);

/******************************************************************************/
/*!
 *  \fn     void SMDMotorReset(void)
 *  \brief  This function resets the Motor
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SMDMotorReset(void);

/******************************************************************************/
/*!
 * \fn extern void PWMConfigMotor1Param(unsigned int  usnDisplacement,
 *                      unsigned int usnDirection, unsigned int StepFrequency);
 * \fn extern void PWMConfigMotor2Param(unsigned int  usnDisplacement,
 *                      unsigned int usnDirection, unsigned int StepFrequency);
 * \fn extern void PWMConfigMotor3Param(unsigned int  usnDisplacement,
 *                      unsigned int usnDirection, unsigned int StepFrequency);
 * \fn extern void PWMConfigMotor4Param(unsigned int  usnDisplacement,
 *                      unsigned int usnDirection, unsigned int StepFrequency);
 * \fn extern void PWMConfigMotor5Param(unsigned int  usnDisplacement,
 *                      unsigned int usnDirection, unsigned int StepFrequency);
 * \fn extern unsigned int PWMGetMotor1Status(void);
 * \fn extern unsigned int PWMGetMotor2Status(void);
 * \fn extern unsigned int PWMGetMotor3Status(void);
 * \fn extern unsigned int PWMGetMotor4Status(void);
 * \fn extern unsigned int PWMGetMotor5Status(void);
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     extern void PWMConfigMotor1Param(float fDisplacement, Uint16 usnDirection, \
 *                              Uint16 StepFrequency)
 *  \brief  extern declaration of the function
 */
/******************************************************************************/
extern void PWMConfigMotor1Param(float fDisplacement, Uint16 usnDirection, \
                                Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn     extern void PWMConfigMotor2Param(float fDisplacement, Uint16 usnDirection, \
 *                               Uint16 StepFrequency)
 *  \brief  extern declaration of the function
 */
/******************************************************************************/
extern void PWMConfigMotor2Param(float fDisplacement, Uint16 usnDirection, \
                                Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn    extern void PWMConfigMotor3Param(float fDisplacement, Uint16 usnDirection, \
 *                              Uint16 StepFrequency)
 *  \brief extern declaration of the function
 */
/******************************************************************************/
extern void PWMConfigMotor3Param(float fDisplacement, Uint16 usnDirection, \
                                Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn     extern void PWMConfigMotor4Param(float fDisplacement, Uint16 usnDirection, \
 *                              Uint16 StepFrequency)
 *  \brief  extern declaration of the function
 */
/******************************************************************************/
extern void PWMConfigMotor4Param(float fDisplacement, Uint16 usnDirection, \
                                Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn     extern void PWMConfigMotor5Param(float fDisplacement, Uint16 usnDirection, \
 *                              Uint16 StepFrequency)
 *  \brief  extern declaration of the function
 */
/******************************************************************************/
extern void PWMConfigMotor5Param(float fDisplacement, Uint16 usnDirection, \
                                Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn     extern Uint16 PWMGetMotor1Status(void)
 *  \brief  extern declaration of the function
 */
/******************************************************************************/
extern Uint16 PWMGetMotor1Status(void);

/******************************************************************************/
/*!
 *  \fn     extern Uint16 PWMGetMotor2Status(void)
 *  \brief  extern declaration of the function
 */
/******************************************************************************/
extern Uint16 PWMGetMotor2Status(void);

/******************************************************************************/
/*!
 *  \fn     extern Uint16 PWMGetMotor3Status(void)
 *  \brief  extern declaration of the function
 */
/******************************************************************************/
extern Uint16 PWMGetMotor3Status(void);

/******************************************************************************/
/*!
 *  \fn     extern Uint16 PWMGetMotor4Status(void)
 *  \brief  extern declaration of the function
 */
/******************************************************************************/
extern Uint16 PWMGetMotor4Status(void);

/******************************************************************************/
/*!
 *  \fn     extern Uint16 PWMGetMotor5Status(void)
 *  \brief  extern declaration of the function
 */
/******************************************************************************/
extern Uint16 PWMGetMotor5Status(void);

#endif /* STEPPER_MOTOR_DRIVER_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

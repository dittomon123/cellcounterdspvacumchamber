/******************************************************************************/
/*! \file Timer.h
 *
 *  \brief Header file for Timer.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.2
 *   $Date:    $ Jan-21-2015
 *   $Author:  $ 20040919, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_timer_module
//! @{
//******************************************************************************
#ifndef TIMER_H_
#define TIMER_H_

#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File


/******************************************************************************/
/*!
 * \brief Function prototypes:
 * \fn void Initialize_Timer(void);
 * \brief Function to initialize CPU timers
 * \fn void Start_Timer0(void);
 * \brief Function to start CPU timer 0
 * \fn void Stop_Timer0(void);
 * \brief Function to stop CPU timer 0
 * \fn void Configure_Timer0(void);
 * \brief Function to configure CPU timer 0
 * \fn void Start_Timer1(void);
 * \brief Function to start CPU timer 1
 * \fn void Stop_Timer1(void);
 * \brief Function to stop CPU timer 1
 * \fn void Configure_Timer1(void);
 * \brief Function to configure CPU timer 1
 */
/******************************************************************************/
void Initialize_Timer(void);
void Start_Timer0(void);
void Stop_Timer0(void);
void Configure_Timer0(void);
void Start_Timer1(void);
void Stop_Timer1(void);
void Configure_Timer1(void);

/******************************************************************************/
/*!
 * \def Preprocessor definitions
 * \def TIMER1_CYCLE_TIME_uS - 20mSec timer
 * \def TIMER2_CYCLE_TIME_uS - 100mSec timer
 */
/******************************************************************************/
#define TIMER1_CYCLE_TIME_uS	5000//20000	Changed to 5ms by Hari//20 mSec cycle time
//#define TIMER1_CYCLE_TIME_uS	200000	//2 Sec cycle time
#define TIMER2_CYCLE_TIME_uS	1000000	//100 mSec cycle time

// Prototype statements for functions found within this file.
//__interrupt void cpu_timer0_isr(void);
//__interrupt void cpu_timer1_isr(void);
//__interrupt void cpu_timer2_isr(void);

#endif /* TIMER_H_ */

//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

/*
 * DebugDefines.h
 *
 *  Created on: 22-Aug-2016
 *      Author: 20033200
 */

#ifndef INCLUDE_DEBUGDEFINES_H_
#define INCLUDE_DEBUGDEFINES_H_

#include "stdio.h"

#ifndef MODULE_DEBUG_CONTROL
	#define LOG_CRITICAL_ENABLED
	#define LOG_INFO_ENABLED
	#define LOG_DEBUG_ENABLED
	#undef DEBUG_DISABLED
#endif

#ifndef MODULE_NAME
	#define MODULE_NAME       __PRETTY_FUNCTION__
#endif

#ifdef WIN32
#undef LOG_CRITICAL_ENABLED
#undef LOG_INFO_ENABLED
#undef LOG_DEBUG_ENABLED
#undef MODULE_NAME
#endif

#ifndef DEBUG_DISABLED

	#if defined LOG_CRITICAL_ENABLED
		#define LOG_CRITICAL( args... )							\
			printf( MODULE_NAME ); printf(":<CRITICAL>:"args ); printf("\n");
	#else
		#ifdef WIN32
		#define LOG_CRITICAL( va_list )		/* do nothing */
		#elif
		#define LOG_CRITICAL( args... )		/* do nothing */
		#endif
	#endif


	#if defined LOG_INFO_ENABLED
		#define LOG_INFO( args... )							\
			printf( MODULE_NAME ); printf( ":<INFO>:"args ); printf("\n");
	#else
		#ifdef WIN32
		#define LOG_INFO( va_list )		/* do nothing */
		#elif
		#define LOG_INFO( args... )			/* do nothing */
		#endif
	#endif


	#if defined LOG_DEBUG_ENABLED
		#define LOG_DEBUG( args... )							\
			printf( MODULE_NAME ); printf( ":<DEBUG>:"args ); printf("\n");
	#else
		#ifdef WIN32
		#define LOG_DEBUG( va_list )		/* do nothing */
		#elif
		#define LOG_DEBUG( args... )		/* do nothing */
		#endif
	#endif


#else

	#ifdef WIN32
	#define LOG_CRITICAL( va_list )		/* do nothing */
	#elif
	#define LOG_CRITICAL( args... )			/* do nothing */
	#endif

	#ifdef WIN32
	#define LOG_INFO( va_list )		/* do nothing */
	#elif
	#define LOG_INFO( args... )				/* do nothing */
	#endif

	#ifdef WIN32
	#define LOG_DEBUG( va_list )		/* do nothing */
	#elif
	#define LOG_DEBUG( args... )			/* do nothing */
	#endif

#endif

#endif

#endif /* INCLUDE_DEBUGDEFINES_H_ */

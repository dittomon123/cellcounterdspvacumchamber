/******************************************************************************/
/*! \file IpcModule.h
 *
 *  \brief Header file for IpcModule.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-04-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_ipc_communication_module
//! @{
//******************************************************************************
#ifndef IPC_MODULE_H_
#define IPC_MODULE_H_
#include "F2837xD_device.h"     // Headerfile Include File
#include "F2837xD_Examples.h"   // Examples Include File
#include "F2837xD_Ipc_drivers.h"
#include "Structure.h"

//******************** IPC contents ********************************************
//------------------------------IPC contents----------------------------------//
//******************************************************************************
//! Definitions used
//******************************************************************************
/******************************************************************************/
/*!
 * \def CPU02TOCPU01_PASSMSG
 * \def CPU01TOCPU02_PASSMSG
 * \brief CPU02 to CPU01 and CPU01 to CPU02  MSG RAM offsets for passing address
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   CPU02TOCPU01_PASSMSG
 * \brief CPU02 to CPU01 MSG RAM offsets for passing address
 */
/******************************************************************************/
#define CPU02TOCPU01_PASSMSG  										0x0003FBF4     //! CPU02 to CPU01 MSG RAM offsets

/******************************************************************************/
/*!
 * \def   CPU01TOCPU02_PASSMSG
 * \brief CPU01 to CPU02 MSG RAM offsets for passing address
 */
/******************************************************************************/
#define CPU01TOCPU02_PASSMSG  										0x0003FFF4     //! CPU01 to CPU02 MSG RAM offsets

/******************************************************************************/
/*!
 * \def GS4SARAM_CPU2_ADC_BUF   Adc buffer memory of CPU2
 * \def GS10SARAM_CPU1_WRITE    RAM CPU1 write
 * \def GS13SARAM_CPU2_WRITE    RAM CPU2 write
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   GS4SARAM_CPU2_ADC_BUF
 * \brief Adc buffer memory of CPU2
 */
/******************************************************************************/
#define GS4SARAM_CPU2_ADC_BUF										0x010000

/******************************************************************************/
/*!
 * \def   GS4SARAM_CPU2_ADC_BUF
 * \brief RAM CPU1 write
 */
/******************************************************************************/
#define GS10SARAM_CPU1_WRITE										0x016000
//#define GS13SARAM_CPU2_WRITE										0x019040

/******************************************************************************/
/*!
 * \def IPC Commands from CPU1 to CPU2
 * \def CPU1_IPC_ABORT_CMD - not used
 * \def CPU2_IPC_DATA_ACQUISITION_ERROR - not used
 * \def CPU1_IPC_FIRST_COUNT_DATA_ACQUISITION_CMD : To acquire data for first \
 *                                                  cycle
 * \def CPU1_IPC_SECOND_COUNT_DATA_ACQUISITION_CMD : To acquire data for second \
 *                                                  cycle - not used
 * \def CPU1_IPC_DATA_ACQUISITION_STATUS_REQ - not used
 * \def CPU1_IPC_COUNTING_CYCLE_COMPLETION - not used
 * \def CPU1_IPC_ADC_CAPTURED_DATA_PRINT_REQ - not used
 * \def CPU1_IPC_DEFAULT_WBC_THRESHOLDS_REQ - not used
 * \def CPU1_IPC_CPU2_FW_VERSION_REQ : To get CPU2 firmware version
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   CPU1_IPC_ABORT_CMD
 * \brief not used
 */
/******************************************************************************/
#define CPU1_IPC_ABORT_CMD											0x10001FFF

/******************************************************************************/
/*!
 * \def   CPU2_IPC_DATA_ACQUISITION_ERROR
 * \brief not used
 */
/******************************************************************************/
#define CPU2_IPC_DATA_ACQUISITION_ERROR								0x20001FFF

/******************************************************************************/
/*!
 * \def   CPU1_IPC_FIRST_COUNT_DATA_ACQUISITION_CMD
 * \brief To acquire data for first cycle
 */
/******************************************************************************/
#define CPU1_IPC_FIRST_COUNT_DATA_ACQUISITION_CMD					0x10001001

/******************************************************************************/
/*!
 * \def   CPU1_IPC_SECOND_COUNT_DATA_ACQUISITION_CMD
 * \brief To acquire data for second cycle which is not used currently
 */
/******************************************************************************/
#define CPU1_IPC_SECOND_COUNT_DATA_ACQUISITION_CMD					0x10001002

/******************************************************************************/
/*!
 * \def   CPU1_IPC_DATA_ACQUISITION_STATUS_REQ
 * \brief not used
 */
/******************************************************************************/
#define CPU1_IPC_DATA_ACQUISITION_STATUS_REQ						0x10001003

/******************************************************************************/
/*!
 * \def   CPU1_IPC_COUNTING_CYCLE_COMPLETION
 * \brief not used
 */
/******************************************************************************/
#define CPU1_IPC_COUNTING_CYCLE_COMPLETION							0x10001004

/******************************************************************************/
/*!
 * \def   CPU1_IPC_COUNTING_CYCLE_COMPLETION
 * \brief not used
 */
/******************************************************************************/
#define CPU1_IPC_ADC_CAPTURED_DATA_PRINT_REQ						0x10001005

/******************************************************************************/
/*!
 * \def   CPU1_IPC_RESET_FOR_ACQ
 * \brief To Reset the parameter of the CPU2 for next aquisition
 */
/******************************************************************************/
#define CPU1_IPC_RESET_FOR_ACQ                                      0x10001006

/******************************************************************************/
/*!
 * \def   CPU1_IPC_RESET_FLAG
 * \brief not used
 */
/******************************************************************************/
#define CPU1_IPC_RESET_FLAG                                         0x10001007

/******************************************************************************/
/*!
 * \def   CPU1_IPC_DEFAULT_WBC_THRESHOLDS_REQ
 * \brief not used
 */
/******************************************************************************/
#define CPU1_IPC_DEFAULT_WBC_THRESHOLDS_REQ							0x10003001

/******************************************************************************/
/*!
 * \def   CPU1_IPC_CPU2_FW_VERSION_REQ
 * \brief To Request for the firmware version of CPU2
 */
/******************************************************************************/
#define CPU1_IPC_CPU2_FW_VERSION_REQ								0x10002001

/******************************************************************************/
/*!
 * \def IPC Commands from CPU2 to CPU1
 * \def CPU2_IPC_CPU2_FW_VERSION_RESPONSE : To send CPU2 firmware version to CPU1
 * \def CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_STARTED : To indicate first cycle \
 * data acquisition started to CPU1
 * \def CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED : To indicate first \
 * cycle data acquisition completed to CPU1
 * \def CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_STARTED : To indicate second \
 * cycle data acquisition started to CPU1
 * \def CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_COMPLETED : To indicate second \
 * cycle data acquisition completed to CPU1
 * \def CPU2_IPC_FIRST_COUNT_DATA_ANALYSIS_COMPLETED : To indicate first cycle \
 * data acquisition completed to CPU1
 * \def CPU2_IPC_SECOND_COUNT_DATA_ANALYSIS_COMPLETED : To indicate second cycle \
 * data acquisition completed to CPU1
 * \def CPU2_IPC_VOLUMETRIC_INT_ERROR : To indicate volumetric error to CPU1
 * \def CPU2_IPC_COMMUNICATION_ERROR : To indicate IPC communication error to CPU1
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   CPU2_IPC_CPU2_FW_VERSION_RESPONSE
 * \brief To send CPU2 firmware version to CPU1
 */
/******************************************************************************/
#define CPU2_IPC_CPU2_FW_VERSION_RESPONSE							0x20002002

/******************************************************************************/
/*!
 * \def   CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_STARTED
 * \brief To indicate the first cycle data acquisition started to CPU1
 */
/******************************************************************************/
#define CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_STARTED				0x20001001

/******************************************************************************/
/*!
 * \def   CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED
 * \brief To indicate the first cycle data acquisition completed to CPU1
 */
/******************************************************************************/
#define CPU2_IPC_FIRST_COUNT_DATA_ACQUISITION_COMPLETED				0x20001002

/******************************************************************************/
/*!
 * \def   CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_STARTED
 * \brief To indicate the second cycle data acquisition started to CPU1
 */
/******************************************************************************/
#define CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_STARTED				0x20001003

/******************************************************************************/
/*!
 * \def   CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_COMPLETED
 * \brief To indicate the second cycle data acquisition completed to CPU1
 */
/******************************************************************************/
#define CPU2_IPC_SECOND_COUNT_DATA_ACQUISITION_COMPLETED			0x20001004

/******************************************************************************/
/*!
 * \def   CPU2_IPC_FIRST_COUNT_DATA_ANALYSIS_COMPLETED
 * \brief To indicate the first count data anaylysis completed to CPU1
 */
/******************************************************************************/
#define CPU2_IPC_FIRST_COUNT_DATA_ANALYSIS_COMPLETED				0x20001005

/******************************************************************************/
/*!
 * \def   CPU2_IPC_SECOND_COUNT_DATA_ANALYSIS_COMPLETED
 * \brief To indicate the second count data anaylysis completed to CPU1
 */
/******************************************************************************/
#define CPU2_IPC_SECOND_COUNT_DATA_ANALYSIS_COMPLETED				0x20001006

/******************************************************************************/
/*!
 * \def   CPU2_IPC_VOLUMETRIC_INT_ERROR
 * \brief To indicate volumetric error to CPU1
 */
/******************************************************************************/
#define CPU2_IPC_VOLUMETRIC_INT_ERROR                               0x20001007

/******************************************************************************/
/*!
 * \def   CPU2_IPC_RESET_DONE
 * \brief Currently not used
 */
/******************************************************************************/
#define CPU2_IPC_RESET_DONE                                         0x20001008

/******************************************************************************/
/*!
 * \def   CPU2_WBC_ANALYSIS_START
 * \brief To indicate that analysis of WBC started from CPU2
 */
/******************************************************************************/
#define CPU2_WBC_ANALYSIS_START                                     0x20001009

/******************************************************************************/
/*!
 * \def   CPU2_RBC_PLT_ANALYSIS_START
 * \brief To indicate that analysis of RBC and PLT started from CPU2
 */
/******************************************************************************/
#define CPU2_RBC_PLT_ANALYSIS_START                                 0x2000100A

/******************************************************************************/
/*!
 * \def   CPU2_WBC_ANALYSIS_END
 * \brief To indicate that analysis of WBC completed from CPU2
 */
/******************************************************************************/
#define CPU2_WBC_ANALYSIS_END                                       0x2000100B

/******************************************************************************/
/*!
 * \def   CPU2_RBC_PLT_ANALYSIS_END
 * \brief To indicate that analysis of RBC and PLT completed from CPU2
 */
/******************************************************************************/
#define CPU2_RBC_PLT_ANALYSIS_END                                   0x2000100C

/******************************************************************************/
/*!
 * \def CPU2_VOLUMETRIC_SENSOR_DETECTION
 * \brief  To define IPC command to CPU1 indicating the detection of volumetric
 * sensor
 */
/******************************************************************************/
#define CPU2_VOLUMETRIC_SENSOR_DETECTION                            0x2000100D

/******************************************************************************/
/*!
 * \def CPU2_ONE_SECOND_DATA
 * \brief  To define IPC command to CPU1 to send one second data of WBC, RBC & PLT
 */
/******************************************************************************/
#define CPU2_ONE_SECOND_DATA                                        0x2000100E

/******************************************************************************/
/*!
 * \def   CPU2_IPC_COMMUNICATION_ERROR
 * \brief To indicate IPC communication error to CPU1
 */
/******************************************************************************/
#define CPU2_IPC_COMMUNICATION_ERROR                                0x30001FFF




/******************************************************************************/
/*!
 * \def FLAG1 - not used
 * \def FLAG17 - IPC syncing between CPU1 and CPU2
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   FLAG1
 * \brief not used
 */
/******************************************************************************/
#define FLAG1														1

/******************************************************************************/
/*!
 * \def   FLAG17
 * \brief FLAG17 - IPC syncing between CPU1 and CPU2
 */
/******************************************************************************/
#define FLAG17														17

/******************************************************************************/
/*!
 *
 * \fn void IPCInitialize_IPC(void);
 * \brief To initialize IPC communication
 * \fn void IPCCpu1toCpu2IpcCommSync(void)
 * \brief To syncronize CPU1 to CPU2 IPC communication
 * \fn uint16_t IPCCpu1toCpu2IpcCmdData(stIpcParameters *Cpu1IpcPacket,
 * uint16_t dataLength);
 * \brief To send data from CPU1 to CPU2
 * \fn uint16_t IPCCpu1toCpu2IpcCmd(stIpcParameters *Cpu1IpcPacket);
 * \brief To send command from CPU1 to CPU2
 * \fn void IPCCpu1IpcReqMemAccess(void);
 * \brief CPU1 requests memory access for RW
 * \fn void IPCCpu2IpcReqMemAccess(void);
 * \brief CPU2 requests memory access for RW
 * \fn stIpcParameters* IPCGetCpu1IpcParam(void);
 * \brief To get IPC parameters address
 * \fn void IPCClearCpu1IpcPacketTxBuf(stIpcParameters *Cpu1IpcPacket);
 * \brief To clear the Transmit buffer of CPU1
 * \fn void IPCClearCpu1IpcPacketRxBuf(stIpcParameters *Cpu1IpcPacket);
 * \brief To clear the Revieve buffer of CPU1
 * \fn void IPCCpu1IpcPacketToTxMsgQue(stIpcParameters *Cpu1IpcPacket);
 * \brief To update the message queue with the transmit data
 * \fn void IPCCpu1IpcPacketToRxMsgQue(stIpcParameters *Cpu1IpcPacket);
 * \brief To update the message queue with the recied data from CPU2
 * \fn void IPCCpu1toCpu2IpcInterrupt(uint16_t Flag);
 * \brief Interrupt to send data from CPU1 to CPU2
 * \fn __interrupt void CPU02toCPU01IPC0IntHandler(void);
 * \brief ISR0 of CPU2 to CPU1
 * \fn __interrupt void CPU02toCPU01IPC1IntHandler(void);
 * \brief ISR1 of CPU2 to CPU1
 * \brief Function prototypes:
 */
/******************************************************************************/

/******************************************************************************/
/*!
 *  \fn     void IPCInitialize_IPC(void)
 *  \brief  To initialize IPC communication
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IPCInitialize_IPC(void);

/******************************************************************************/
/*!
 *  \fn     void IPCCpu1toCpu2IpcCommSync(void)
 *  \brief  To syncronize CPU1 to CPU2 IPC communication
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IPCCpu1toCpu2IpcCommSync(void);

/******************************************************************************/
/*!
 *  \fn     uint16_t IPCCpu1toCpu2IpcCmdData(stIpcParameters *Cpu1IpcPacket, \
 *          uint16_t dataLength)
 *  \brief  To Transmit data packet to from CPU1 to CPU2
 *  \param  stIpcParameters,uint16_t
 *  \return uint16_t
 */
/******************************************************************************/
uint16_t IPCCpu1toCpu2IpcCmdData(stIpcParameters *Cpu1IpcPacket, uint16_t dataLength);

/******************************************************************************/
/*!
 *  \fn     uint16_t IPCCpu1toCpu2IpcCmd(stIpcParameters *Cpu1IpcPacket)
 *  \brief  To Transmit data packet to from CPU2 to CPU1
 *  \param  stIpcParameters *
 *  \return uint16_t
 */
/******************************************************************************/
uint16_t IPCCpu1toCpu2IpcCmd(stIpcParameters *Cpu1IpcPacket);

/******************************************************************************/
/*!
 *  \fn     void IPCCpu1IpcReqMemAccess(void)
 *  \brief  To request for shared memory access
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IPCCpu1IpcReqMemAccess(void);

/******************************************************************************/
/*!
 *  \fn     void IPCCpu2IpcReqMemAccess(void)
 *  \brief  To request for shared memory access
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IPCCpu2IpcReqMemAccess(void);

/******************************************************************************/
/*!
 *  \fn     stIpcParameters* IPCGetCpu1IpcParam(void)
 *  \brief  To get the address of CPU1 Ipc paramaters
 *  \param  void
 *  \return stIpcParameters*
 */
/******************************************************************************/
stIpcParameters* IPCGetCpu1IpcParam(void);

/******************************************************************************/
/*!
 *  \fn     void IPCClearCpu1IpcPacketTxBuf(stIpcParameters *Cpu1IpcPacket)
 *  \brief  To clear the Tx buffer of stIpcParameters
 *  \param  stIpcParameters*
 *  \return void
 */
/******************************************************************************/
void IPCClearCpu1IpcPacketTxBuf(stIpcParameters *Cpu1IpcPacket);

/******************************************************************************/
/*!
 *  \fn     void IPCClearCpu1IpcPacketRxBuf(stIpcParameters *Cpu1IpcPacket)
 *  \brief  To clear the Rx buffer of stIpcParameters
 *  \param  stIpcParameters *
 *  \return void
 */
/******************************************************************************/
void IPCClearCpu1IpcPacketRxBuf(stIpcParameters *Cpu1IpcPacket);

/******************************************************************************/
/*!
 *  \fn     void IPCCpu1IpcPacketToTxMsgQue(stIpcParameters *Cpu1IpcPacket)
 *  \brief  TBD
 *  \param  stIpcParameters *
 *  \return void
 */
/******************************************************************************/
void IPCCpu1IpcPacketToTxMsgQue(stIpcParameters *Cpu1IpcPacket);

/******************************************************************************/
/*!
 *  \fn     void IPCCpu1IpcPacketToRxMsgQue(void)
 *  \brief  TBD
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IPCCpu1IpcPacketToRxMsgQue(void);

/******************************************************************************/
/*!
 *  \fn     void IPCCpu1toCpu2IpcInterrupt(uint16_t Flag)
 *  \brief  To generate interrupt at Cpu2
 *  \param  uint16_t
 *  \return void
 */
/******************************************************************************/
void IPCCpu1toCpu2IpcInterrupt(uint16_t Flag);

/******************************************************************************/
/*!
 *  \fn     __interrupt void CPU02toCPU01IPC0IntHandler(void)
 *  \brief  CPU02 to CPU01 IPC INT0 Interrupt Handler
 *  \param  void
 *  \return void
 */
/******************************************************************************/
//void Error(void);
__interrupt void CPU02toCPU01IPC0IntHandler(void);

/******************************************************************************/
/*!
 *  \fn     __interrupt void CPU02toCPU01IPC1IntHandler(void)
 *  \brief  CPU01 to CPU02 IPC INT1 Interrupt Handler
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void CPU02toCPU01IPC1IntHandler(void);

//******************** IPC contents ends ***************************************


#endif /* IPC_MODULE_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

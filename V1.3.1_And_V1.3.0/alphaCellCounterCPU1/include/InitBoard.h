/******************************************************************************/
/*! \file InitBoard.h
 *
 *  \brief Header file for InitBoard.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-23-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_cpu_initialization_module
//! @{
//*****************************************************************************
#ifndef INIT_BOARD_H_
#define INIT_BOARD_H_

#include "F2837xD_device.h"     // Headerfile Include File
#include "F2837xD_Examples.h"   // Examples Include File


/******************************************************************************/
/*!
 * \brief Function prototypes:
 * \fn void IBInitialize_Board();
 * \brief to initialize the board
 * \fn void IBTimer_Interrupt();
 * \brief to initialize the timer interrupts
 * \fn void IBWatchDog_Timer_Interrupt();
 * \brief to initalize the watchdog timer
 * \fn Uint16 IBGetInterruptRegStatus();
 * \brief to get the interrupt register status
 * \fn void IBSetInterruptRegStatus(Uint16 InterruptReg);
 * \fn __interrupt void wakeint_isr(void);
 * \fn __interrupt void cpu_timer0_isr(void);
 * \fn __interrupt void cpu_timer1_isr(void);
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     void IBInitialize_Board()
 *  \brief  To initialize the board
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IBInitialize_Board();

/******************************************************************************/
/*!
 *  \fn     void IBTimer_Interrupt()
 *  \brief  This function initializes the timer interrupts
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IBTimer_Interrupt();

/******************************************************************************/
/*!
 *  \fn     void IBWatchDog_Timer_Interrupt()
 *  \brief  To initalize the watchdog timer
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IBWatchDog_Timer_Interrupt(void);

/******************************************************************************/
/*!
 *  \fn     Uint16 IBGetInterruptRegStatus()
 *  \brief  To get the interrupt register status
 *  \param  void
 *  \return Uint16
 */
/******************************************************************************/
Uint16 IBGetInterruptRegStatus();

/******************************************************************************/
/*!
 *  \fn     void IBSetInterruptRegStatus(Uint16 InterruptReg)
 *  \brief  This function sets IER register
 *  \param  void
 *  \return Uint16
 */
/******************************************************************************/
void IBSetInterruptRegStatus(Uint16 InterruptReg);

/******************************************************************************/
/*!
 *  \fn     wakeint_isr()
 *  \brief  wakeup ISR
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void wakeint_isr(void);

/******************************************************************************/
/*!
 *  \fn     cpu_timer0_isr()
 *  \brief  cpu timer0 ISR
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void cpu_timer0_isr(void);

/******************************************************************************/
/*!
 *  \fn     cpu_timer1_isr()
 *  \brief  cpu timer1 ISR
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void cpu_timer1_isr(void);

/******************************************************************************/
/*!
 * \def   JTAG_ENABLE
 * \brief Defined to enable the debug mode
 */
/******************************************************************************/
#define	JTAG_ENABLE		TRUE

/******************************************************************************/
/*!
 * \def   JTAG_DISABLE
 * \brief Defined to disable the debug mode
 */
/******************************************************************************/
#define	JTAG_DISABLE	FALSE


#endif /* INIT_BOARD_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

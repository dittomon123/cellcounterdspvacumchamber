/******************************************************************************/
/******************************************************************************/
/*! \file SpiInterface.h
 *
 *  \brief Header file for SpiInterface.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Apr-09-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_spi_module
//! @{
//******************************************************************************
#ifndef SPI_INTERFACE_H_
#define SPI_INTERFACE_H_

#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File

/******************************************************************************/
/*!
 * \brief Function prototypes:
 * \fn void SIInitialize_SpiValve(void)
 * \brief Function initializes SPI periheral registers
 * \fn void SISPI_Fifo_Initialize(void)
 * \brief Function initializes SPI periheral FIFO registers
 * \fn void SISPI_WriteData(Uint16 x)
 * \brief Function initializes SPI periheral FIFO registers
 * \fn Uint16 SISPI_ReadData(Uint16 rdata1)
 * \brief Function read data to SPI receive buffer
 * \fn void SIDelay_Loop(void)
 * \brief Function for Delay
 * \fn void SIError(void) - not used
 * \fn void SIInitSpiaGpio(void) - not used
 * \fn void SISend_Data(void) - not used
 * \fn void SIUpdateValveStatus(Uint16 ValveStatus_2, Uint16 ValveStatus_1);
 * \brief Function writes valve state to Valve driver
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     void SIInitialize_SpiValve(void)
 *  \brief  Function initializes SPI periheral registers
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SIInitialize_SpiValve(void);

/******************************************************************************/
/*!
 *  \fn     void SISPI_Fifo_Initialize(void)
 *  \brief  Function initializes SPI periheral FIFO registers
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SISPI_Fifo_Initialize(void);

/******************************************************************************/
/*!
 *  \fn     void SISPI_WriteData(Uint16 )
 *  \brief  SPI Write function
 *  \param  Uint16
 *  \return void
 */
/******************************************************************************/
void SISPI_WriteData(Uint16 );

/******************************************************************************/
/*!
 *  \fn     Uint16 SISPI_ReadData(Uint16 )
 *  \brief  SPI read function
 *  \param  Uint16
 *  \return Uint16
 */
/******************************************************************************/
Uint16 SISPI_ReadData(Uint16 );

/******************************************************************************/
/*!
 *  \fn     void SIDelay_Loop(void)
 *  \brief  Delay function
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SIDelay_Loop(void);

/******************************************************************************/
/*!
 *  \fn     void SIError(void)
 *  \brief  Not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SIError(void);

/******************************************************************************/
/*!
 *  \fn     void SIInitSpiaGpio(void)
 *  \brief  Not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SIInitSpiaGpio(void);

/******************************************************************************/
/*!
 *  \fn     void SISend_Data(void)
 *  \brief  Not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SISend_Data(void);

/******************************************************************************/
/*!
 *  \fn     void SIUpdateValveStatus(void)
 *  \brief  Write Valve state
 *  \param  Uint16
 *  \param  Uint16
 *  \return void
 */
/******************************************************************************/
void SIUpdateValveStatus(Uint16 , Uint16 );

#endif /* SPI_INTERFACE_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

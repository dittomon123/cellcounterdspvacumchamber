/*
 * CommInterface.h
 *
 *  Created on: 04-Aug-2016
 *      Author: 20033200
 */

#ifndef INCLUDE_COMMINTERFACE_H_
#define INCLUDE_COMMINTERFACE_H_

#include "CommProtocol.h"

/******************************************************************************/
/*!
 *  \fn     CIProcessCommandAttribute()
 *  \brief  To Process the Commands received from SPI Master
 *  \param1  uint16_t usnMajorCmd - Major Command
 *  \param2  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessCommandAttribute(uint16_t usnMajorCmd, uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     CIProcessPatientHandlingCmd()
 *  \brief  To process the Sitara Patient Handling Commands
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessPatientHandlingCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     CIProcessServiceHandlingCmd()
 *  \brief  To process Sitara Service handling commands
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessServiceHandlingCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     CIProcessACKCmd()
 *  \brief  To process Acknowledgement command received from Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessACKCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     CIProcessNACKCmd()
 *  \brief  To process the Negative Acknowledgement command received from Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessNACKCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     CIFormServiceHandlingPacket()
 *  \brief  To form the service handling packet to be sent to Sitara
 *  \param  uint16_t usnServicehandled -Service handled
 *          bool_t bStartEnd
 *          True for Service Handling start, and false for Service Handling
 *          Completed
 *  \return void
 */
/******************************************************************************/
void CIFormServiceHandlingPacket(uint16_t usnServicehandled, bool_t bStartEnd);

/******************************************************************************/
/*!
 *  \fn     CIProcessFirmwareVersionReq()
 *  \brief  To process the Firmware Version Command request from Sitara
 *  \return  void
 */
/******************************************************************************/
void CIProcessFirmwareVersionReq(void);

/******************************************************************************/
/*!
 *  \fn     CIValveTestCompleted()
 *  \brief  To send the completion of valve test to Sitara
 *  \param  unsigned char ucValveNumber
 *  \return  void
 */
/******************************************************************************/
void CIValveTestCompleted(unsigned char ucValveNumber);

/******************************************************************************/
/*!
 *  \fn     void CIFormACKPacket()
 *  \brief  To frame and send an Acknowledgement packet to sitara
 *  \param  bool_t bTransmitNACK
 *  \return  void
 */
/******************************************************************************/
void CIFormACKPacket(bool_t bTransmitNACK);

/******************************************************************************/
/*!
 *  \fn     void CIFormPatientHandlingPacket()
 *  \brief  To form Patient handling packet
 *  \param  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormPatientHandlingPacket(uint16_t usnSubCmd);

/******************************************************************************/
/*!
 *  \fn     CIFormSyncCmd()
 *  \brief  To form the Sync Command to Sitara
 *  \return  void
 */
/******************************************************************************/
void CIFormSyncCmd(void);

/******************************************************************************/
/*!
 *  \fn void CIReInitialiseOnNewMeasurement()
 *  \brief  To initialise the variable on every new measurement
 *  \param void
 *  \return void
 */
/******************************************************************************/
void CIReInitialiseOnNewMeasurement(void);

/******************************************************************************/
/*!
 *  \fn     CIProcessCalibrationHandlingCmd()
 *  \brief  To process the Calibration Handling sent by Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessCalibrationHandlingCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     void CIFormCalibrationHandlingPacket()
 *  \brief  To form Calibration handling packet
 *  \param  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormCalibrationHandlingPacket(uint16_t usnSubCmd);

/******************************************************************************/
/*!
 *  \fn     CIProcessQualityHandlingCmd()
 *  \brief  To process the Quality Handling sent by Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessQualityHandlingCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     void CIFormQualityHandlingPacket()
 *  \brief  To form Quality handling packet
 *  \param  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormQualityHandlingPacket(uint16_t usnSubCmd);

/******************************************************************************/
/*!
 *  \fn     void CIFormAbortProcessPacket()
 *  \brief  To form packet for abort command
 *  \param  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormAbortProcessPacket(uint16_t usnSubCmd);

/******************************************************************************/
/*!
 *  \fn     void CIFormSequenceStatusPacket()
 *  \brief  To form Sequence Status packet to be sent to Sitara
 *  \param  uint16_t usnSequenceState
 *  \return  void
 */
/******************************************************************************/
void CIFormSequenceStatusPacket(uint16_t usnSequenceState);

/******************************************************************************/
/*!
 *  \fn     void CIFormSequenceStatusMinorCmd()
 *  \brief  To form Sequence Status major and minor command
 * depending upon the current mode
 *  \param  uint16_t usnSequenceState
 *  \return  uint16_t usnMinorCmd=0;
 */
/******************************************************************************/
uint16_t CIFormSequenceStatusMinorCmd(uint16_t usnSequenceState);

/******************************************************************************/
/*!
 *  \fn     CIProcessShutdownCmd()
 *  \brief  To to Form the Shutdown Sequence
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessShutdownCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn void CIFormSequneceStartedCmd()
 *  \brief  To form minor command for starting of the sequence based on the mode
 *  selected
 *  \param void
 *  \return void
 */
/******************************************************************************/
void CIFormSequneceStartedCmd(void);

/******************************************************************************/
/*!
 *  \fn     CIFormShutDownSequencePacket()
 *  \brief  To form and send Shutdown completed command to Sitara
 *  \param  uint16_t usnShutdownhandled - Current Mode
 *  \return void
 */
/******************************************************************************/
void CIFormShutDownSequencePacket(uint16_t usnShutdownhandled);

/******************************************************************************/
/*!
 *  \fn     CIProcessAbortMainProcessCmd()
 *  \brief  To process the abort command(cancel) sent by Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessAbortMainProcessCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     CIProcessSystemSettingsCmd()
 *  \brief  To process the System Settings command sent by Sitara
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessSystemSettingsCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     void CIFormSystemSettingPacket()
 *  \brief  To form system setting packet
 *  \param  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CIFormSystemSettingPacket(uint16_t usnSubCmd);

/******************************************************************************/
/*!
 *  \fn     void CIFormStartupHandlingPacket()
 *  \brief  To form Startup handling packet
 *  \param  uint16_t usnServicehandled -Service handled
 *          bool_t bStartEnd
 *          True for Service Handling start, and false for Service Handling
 *          Completed
 *  \return void
 */
/******************************************************************************/
void CIFormStartupHandlingPacket(uint16_t usnServicehandled, bool_t bStartEnd);

/******************************************************************************/
/*!
 *  \fn     CIProcessStartupHandlingCmd()
 *  \brief  To process the Sitara startup Handling Commands
 *  \param  uint16_t usnMinorCmd - Minor Command
 *  \return void
 */
/******************************************************************************/
void CIProcessStartupHandlingCmd(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     CIFormErrorPacket()
 *  \brief  To form and Error command to Sitara
 *  \param  uint16_t usnShutdownhandled - Current Mode
 *  \return void
 */
/******************************************************************************/
void CIFormErrorPacket(uint16_t usnMinorCmd);

/******************************************************************************/
/*!
 *  \fn     CIProcessSystemSettingsCmd()
 *  \brief  To process system setting command
 *  \param  unsigned char usnMinorCmd
 *  \return  void
 */
/******************************************************************************/
void CIProcessErrorCommand(uint16_t usnMinorCmd);

extern void SPISetCommCtrlBusyInt();
extern void SPIClearCommCtrlBusyInt();

extern void CPReInitialiseOnNewMeasurement();
extern void SysDisableMotorMon();
extern void SysReInit(void);
#endif /* INCLUDE_COMMINTERFACE_H_ */

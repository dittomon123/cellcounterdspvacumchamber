/******************************************************************************/
/*! \file UartDebug.h
 *
 *  \brief Header file for UartDebug.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.2
 *   $Date:    $ Aug-18-2015
 *   $Author:  $ GURUDUTT R
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_uart_module
//! @{
//******************************************************************************
#ifndef CELLCOUNTERCPU1_INCLUDE_UARTDEBUG_H_
#define CELLCOUNTERCPU1_INCLUDE_UARTDEBUG_H_

#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File
#include "driverlib/sysctl.h"
#include<stdio.h>
#include<string.h>


/******************************************************************************/
/*!
 * \brief Function prototypes:
 * \fn int configureUART(uint32_t );
 * \brief Function to initialize UART periheral registers
 * \fn void configureUART0(void); - not used
 * \fn void scia_init(void);
 * \brief  This function initializes UART periheral registers
 *      115200 baud, 1 stop bit,  No loopback, async mode, idle-line protocol
 *      enable TX, RX, internal SCICLK, isable RX ERR, SLEEP, TXWAKE
 * \fn void scia_xmit(int );
 * \brief Function transmits data using configured baud
 * \fn void scia_fifo_init(void);
 * \brief Function configures FIFO
 * \fn void scid_init(); - not used
 * \fn void scid_xmit(int ); - not used
 * \fn void scid_fifo_init(void); - not used
 * \fn void testUART(void);
 * \brief This function tests UART peripheral by sending initial
 *      string with device info
 * \fn void UARTSendDataBytes16bit(int *, int );
 * \brief This function sends data from buffer and number of byts parameters
 *      passed to the function
 * \fn Uint16 UARTReceiveDataBytes16bit(volatile int *);
 * \brief This function receives data from external device/PC using
 *      configured baud
 * \fn void UartDebugPrint(int *DataBuffer, int NoOfBytes);
 * \brief This function sends data from buffer and number of byts parameters
 *      passed to the function
 * \fn int16 getUartRxData(int *DataBuffer);
 * \brief This function receives the data from PC passed to the function
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     int configureUART(uint32_t )
 *  \brief  UART periheral initialization
 *  \param
 *  \return int
 */
/******************************************************************************/
int configureUART(uint32_t );

/******************************************************************************/
/*!
 *  \fn     void configureUART0()
 *  \brief  not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void configureUART0();

/******************************************************************************/
/*!
 *  \fn     void scia_init()
 *  \brief  This function initializes UART periheral registers
 *          115200 baud, 1 stop bit,  No loopback, async mode, idle-line protocol
 *          enable TX, RX, internal SCICLK, isable RX ERR, SLEEP, TXWAKE
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void scia_init();

/******************************************************************************/
/*!
 *  \fn     void scia_xmit(int )
 *  \brief  UART periheral transmit
 *  \param  int
 *  \return void
 */
/******************************************************************************/
void scia_xmit(int );

/******************************************************************************/
/*!
 *  \fn     void scia_fifo_init(void)
 *  \brief  This function configures FIFO
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void scia_fifo_init(void);

/******************************************************************************/
/*!
 *  \fn     void scid_init()
 *  \brief  not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void scid_init();

/******************************************************************************/
/*!
 *  \fn     void scid_xmit(int )
 *  \brief  not used
 *  \param  int
 *  \return void
 */
/******************************************************************************/
void scid_xmit(int );

/******************************************************************************/
/*!
 *  \fn     void scid_fifo_init(void)
 *  \brief  not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void scid_fifo_init(void);

/******************************************************************************/
/*!
 *  \fn     void testUART(void)
 *  \brief  UART test function
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void testUART(void);

/******************************************************************************/
/*!
 *  \fn     void UARTSendDataBytes16bit(int *, int )
 *  \brief  wrapper function to Send data via UART
 *  \param  int * ,int
 *  \return void
 */
/******************************************************************************/
void UARTSendDataBytes16bit(int *, int );

/******************************************************************************/
/*!
 *  \fn     void UARTSendADCRawData16bit(int Data)
 *  \brief  Send data wrapper function
 *  \param  int
 *  \return void
 */
/******************************************************************************/
void UARTSendADCRawData16bit(int Data);

/******************************************************************************/
/*!
 *  \fn     Uint16 UARTReceiveDataBytes16bit(volatile int *)
 *  \brief  UART receive
 *  \param  volatile int
 *  \return Uint16
 */
/******************************************************************************/
Uint16 UARTReceiveDataBytes16bit(volatile int *);

/******************************************************************************/
/*!
 *  \fn     void UartDebugPrint(int *DataBuffer, int NoOfBytes)
 *  \brief  Send data wrapper function
 *  \param  int *,int
 *  \return void
 */
/******************************************************************************/
void UartDebugPrint(int *DataBuffer, int NoOfBytes);

/******************************************************************************/
/*!
 *  \fn     int16 getUartRxData(int *DataBuffer)
 *  \brief  Send data wrapper function
 *  \param  int *
 *  \return int16
 */
/******************************************************************************/
int16 getUartRxData(int *DataBuffer);


#endif /* CELLCOUNTERCPU1_INCLUDE_UARTDEBUG_H_ */

//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

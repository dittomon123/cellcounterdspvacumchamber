/******************************************************************************/
/*! \file AdcDriver.h
 *
 *  \brief Header file for AdcDriver.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Jul-21-2015
 *   $Author:  $ ADITYA B G, GURUDUTT R
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//*****************************************************************************
//! \addtogroup cpu1_data_acquisition_module
//! @{
//*****************************************************************************
#ifndef ADC_DRIVER_H_
#define ADC_DRIVER_H_

#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File
#include "Structure.h"
#include "Defines.h"


/******************************************************************************/
/*!
 *  \fn     ADAdcd_Init()
 *  \brief  The function initializes ADCD registers and associated SOC registers
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void ADAdcd_Init(void);

/******************************************************************************/
/*!
 *  \fn     ADConfigureAdcd()
 *  \brief  This function configures adc resolution, signal mode, clock
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void ADConfigureAdcd(void);

/******************************************************************************/
/*!
 *  \fn     ADConfigureAdcdEpwm(void)
 *  \brief  This function configures pwm soc to trigger SOC
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void ADConfigureAdcdEpwm(void);

/******************************************************************************/
/*!
 *  \fn     ADSetupAdcdEpwm()
 *  \brief  This function configures pwm trigger selection,aquisition window
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void ADSetupAdcdEpwm(void);

/******************************************************************************/
/*!
 *  \fn     ADAcquirePressureData(Uint16 DataSize)
 *  \brief  This function configures and initiates the PWM trigger to start
 *          Aquisition
 *  \param  Uint16 DataSize
 *  \return void
 */
/******************************************************************************/
void ADAcquirePressureData(Uint16 DataSize);

/******************************************************************************/
/*!
 *  \fn     ADCpu2Ownership()
 *  \brief  CPU2 ownership of ADC modules
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void ADCpu2Ownership(void);

/******************************************************************************/
/*!
 *  \fn     adcd1_isr()
 *  \brief  ADCd channel 1 ISR handler
 *  \param  void
 *  \return void
 */
/******************************************************************************/
interrupt void adcd1_isr(void);

/******************************************************************************/
/*!
 *  \fn     ADAdcAcquireHealthStatus()
 *  \brief  To Start the ADC for health status
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void ADAdcAcquireHealthStatus(void);

/******************************************************************************/
/*!
 * \def    ACQUIRE_IDLE
 * \brief  Acquire IDLE status
 */
/******************************************************************************/
#define		ACQUIRE_IDLE				0

/******************************************************************************/
/*!
 * \def    ACQUIRE_START
 * \brief  Acquire START status
 */
/******************************************************************************/
#define		ACQUIRE_START				1

/******************************************************************************/
/*!
 * \def    ACQUIRE_COMPLETE
 * \brief  Acquire COMPLETE status
 */
/******************************************************************************/
#define		ACQUIRE_COMPLETE			2

/******************************************************************************/
/*!
 * \def    MASKING_12_BIT
 * \brief  Mask the 12 bit ADC sampled data
 */
/******************************************************************************/
#define     MASKING_12_BIT              0x0FFF

/******************************************************************************/
/*!
 * \typedef ST_DATA_AQUISITION_PARAMETER
 * \struct  SDataAquisitionParamer
 * \brief   structure that defines the ADC data aquisition instance
 */
/******************************************************************************/
extern ST_DATA_AQUISITION_PARAMETER StructDataAqParam;

/******************************************************************************/
/*!
 *  \fn     ADGetHealthStatus()
 *  \brief  To get Structure address of ST_HEALTH_STATUS
 *  \param  void
 *  \return &ST_HEALTH_STATUS
 */
/******************************************************************************/
ST_HEALTH_STATUS* ADGetHealthStatus(void);

#endif /* ADC_DRIVER_H_ */
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

/******************************************************************************/
/*! \file PressureSense.h
 *
 *  \brief Header file for PressureSense.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Aug-19-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_pressure_sensor_module
//! @{
//******************************************************************************
#ifndef PRESSURE_SENSE_H_
#define PRESSURE_SENSE_H_

#include "F2837xD_device.h"     // Headerfile Include File
#include "F2837xD_Examples.h"   // Examples Include File
#include "Structure.h"



/******************************************************************************/
/*!
 * \fn void PSInitPressureModule(void);
 * \brief To initialise the pressure related variables
 * \fn float PSADCtoPressureMap(unsigned int ADCRawData);
 * \brief To calculate pressure from the ADC value
 * \fn float PSProcessPressureData(void);
 * \brief To process the obtaine dpressure data
 * \fn unsigned int PSGetPressureData(unsigned int *Data, unsigned int DataBufSize);
 * \brief To get the pressure data
 * \fn PressureParameter* PSGetPressureSense(void)
 * \brief To get the address of th pressure structure
 * \fn extern unsigned int* ADGetAdcBufAddr(void)
 * \brief Function prototypes:
 */
/******************************************************************************/

/******************************************************************************/
/*!
 *  \fn     void PSInitPressureModule(void)
 *  \brief  Initialize pressure related variables
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void PSInitPressureModule(void);


/******************************************************************************/
/*!
 *  \fn     float PSADCtoPressureMap(unsigned int )
 *  \brief  ADC raw data to pressure conversion mapping function
 *  \param  unsigned int
 *  \return float
 */
/******************************************************************************/
float PSADCtoPressureMap(unsigned int );

/******************************************************************************/
/*!
 *  \fn     float PSProcessPressureData(void)
 *  \brief  ADCD raw data to Pressure data
 *  \param  void
 *  \return float
 */
/******************************************************************************/
float PSProcessPressureData(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int PSGetPressureData(unsigned int *, unsigned int )
 *  \brief  To get pressure data from ADC interrupt
 *  \param  unsigned int *, unsigned int
 *  \return unsigned int
 */
/******************************************************************************/
unsigned int PSGetPressureData(unsigned int *, unsigned int );

/******************************************************************************/
/*!
 *  \fn     extern unsigned int* ADGetAdcBufAddr(void)
 *  \brief  Extern definition of the function
 *  \param  void
 *  \return unsigned int*
 */
/******************************************************************************/
extern unsigned int* ADGetAdcBufAddr(void);

/******************************************************************************/
/*!
 *  \fn     PressureParameter* PSGetPressureSense(void)
 *  \brief  To get Structure address of PressureSense
 *  \param  void
 *  \return PressureParameter*
 */
/******************************************************************************/
PressureParameter* PSGetPressureSense(void);

#endif
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

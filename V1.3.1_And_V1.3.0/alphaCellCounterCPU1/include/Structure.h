/*
 * Structure.h
 *
 *  Created on: 29-Jul-2016
 *      Author: 20033200
 */

#ifndef INCLUDE_STRUCTURE_H_
#define INCLUDE_STRUCTURE_H_

#include "Defines.h"



/******************************************************************************/
/*!
 * \brief structure for SPI packet frame
 * \typedef ST_SPI_PACKET_FRAME
 * \struct SSPIPacketFrame
 *!  usnStartOfPacket_MSW - Start of Packet Most Significant Word
 *!  usnStartOfPacket_LSW - Start of Packet Least Significant Word
 *!  usnReservedWord - Reserved Word for future Use
 *!  usnDataLength - Amount of Data present in packet
 *!  usnMajorCommand - Major Command
 *!  usnMinorCommand - Minor Command
 *!  arrusnData - Array to store the data to be sent
 *!  usnRemainingPacketsforTX - No. of Packets to be transmitted to indicate
 *!                             Master SPI to send clock
 *!  usnCRC - CRC
 *!  usnEndOfPacket_MSW - End of Packet Most Significant Word
 *!  usnEndOfPacket_LSW - End of Packet Least Significant Word
 */
/******************************************************************************/
typedef struct SSPIPacketFrame
{
    uint16_t usnStartOfPacket_MSW;
    uint16_t usnStartOfPacket_LSW;
    uint16_t usnReservedWord;
    uint16_t usnDataLength;
    uint16_t usnMajorCommand;
    uint16_t usnMinorCommand;
    uint16_t arrusnData[MAXIMUM_DATA_SIZE];
    uint16_t usnRemainingPacketsforTX;
    uint16_t usnCRC;
    uint16_t usnEndOfPacket_MSW;
    uint16_t usnEndOfPacket_LSW;
}ST_SPI_PACKET_FRAME;

/******************************************************************************/
/*!
 * \brief structure ST_QUEUE
 * \struct stQueue
 *!  nFront, nRear to store the first and last command in queue
 *!  ST_SPI_PACKET_FRAME - command structure
 */
/******************************************************************************/
typedef struct ST_QUEUE
{
   int nFront, nRear;
   int nCurrentSize;
   ST_SPI_PACKET_FRAME SSPIPacketFrame[MAX_COMMAND_QUEUE_LENGTH];
}stQueue;

/******************************************************************************/
/*!
 * \brief union UT_SYS_STATUS_MON_EN
 * \union utSysStatusMon
 * ucSysStatusMon to reset the system status monitoring
 * \struct ST_SYS_STATUS_MON_EN
 *!  bitwise storing of system status monitoring enable
 *!  ucPressureMon - to enable/disable pressure monitoring
 *!  ucTemperatureMon - to enable/disable temperature monitoring
 *!  uc5VMon - to enable/disable 5V monitoring
 *!  uc24VMon - to enable/disable 24V monitoring
 *!  usHgBMon - to enable/disable HgB monitoring
 */
/******************************************************************************/
typedef union UT_SYS_STATUS_MON_EN
{
    struct ST_SYS_STATUS_MON_EN
    {
        unsigned char ucPressureMon:1;
        unsigned char ucTemperatureMon:1;
        unsigned char uc5VMon:1;
        unsigned char uc24VMon:1;
        unsigned char usHgBMon:1;
        unsigned char ucReserved:3;
    }stSysStatusMonEn;
    unsigned char ucSysStatusMon;
}utSysStatusMonEn;

/******************************************************************************/
/*!
 * \brief structure for ADC parameters for ADC api
 * \struct StructDataAqParam
 *!  ADCSampleCounter member of StructDataAqParam structure used to count the
 *!  ADC samples
 *!  AquisitionStatus member of StructDataAqParam structure used to update
 *!  the status of the ADC aquisition
 */
/******************************************************************************/
typedef struct SDataAquisitionParamer {
	unsigned long ADCSampleCounter;
    unsigned int AquisitionStatus;

}ST_DATA_AQUISITION_PARAMETER;

//SKM_CHANGE - START
/******************************************************************************/
/*!
 *
 * \struct HealthStatus
 * \brief struct to acquire health status
 *  Parameter status variables are used to calculate adc values of different
 *  parameters such as temperature,pressure,24voltage sense,5voltage sense
 *  and haemoglobin raw data.
 */
/******************************************************************************/

typedef struct SHealthStatus {
    uint16_t m_arrusnPressureAdc[HEALTH_STATUS_ARRAY_SIZE];//SKM_CHANGE_HGB_ADC
    uint16_t m_arrusnTemperatureAdc[HEALTH_STATUS_ARRAY_SIZE];
    uint16_t m_arrusnHgbAdc[HEALTH_STATUS_ARRAY_SIZE];
    uint16_t m_arrusnSense24Voltage[HEALTH_STATUS_ARRAY_SIZE];
    uint16_t m_arrusnSense5Voltage[HEALTH_STATUS_ARRAY_SIZE];
}ST_HEALTH_STATUS;

/******************************************************************************/
 /*!
  *
  * \struct stBloodCellVoltVal
  * \brief structure for Blood Data analysis
  *! m_fCellLowVolt - Peak detection low threshold
  *! m_fCellHighVolt - Peak detection high threshold
  *! m_deltaMinMax - peak detection threshold
  *! m_pulseWidthMin - pulse width minimum threshold
  *! m_pulseWidthMax - pulse width maximum threshold
  */
 /*****************************************************************************/
typedef struct SBloodCellVoltVal
{
    unsigned int m_fMinVolt;
    unsigned int m_fMaxVolt;
    //unsigned int m_fCellLowVolt;
    unsigned long m_fCellLowVolt;
    //unsigned int m_fCellHighVolt;
    unsigned long m_fCellHighVolt;
    unsigned int m_fApertureLength;
    unsigned int m_fApertureDia;
    unsigned int m_fApertureCorrectedLen;
    unsigned int  m_ucLookForMax;
    unsigned long  m_ulnCellCount;
    unsigned int m_PulseWidth;
    unsigned int m_Flag;
    unsigned int m_deltaMinMax;
    unsigned int m_pulseWidthMin;
    unsigned int m_pulseWidthMax;
}ST_BLOOD_CELL_VOLT_VAL;


/******************************************************************************/
/*!
 * \struct SCpuRegStatus
 * \brief InterruptReg: Variable is used to hold interrupt register values
 * \brief AquisitionStatusRegADCA: not used
 */
/******************************************************************************/
typedef struct SCpuRegStatus
{
	unsigned int InterruptReg;
	unsigned int AquisitionStatusRegADCA;

}ST_CPU_REGISTER_STATUS;

/******************************************************************************/
/*!
 *
 * \struct GpioParameter
 * \brief struct GPIO_PARAMETERS
 * \brief GpioPin: Variable to select GPIO pin
 * \brief GpioFlags: Varialbe to set the flags of the GPIO
 * \brief GpioCpuMuxSel: Variable to configure the mux selection of the GPIO
 * \brief GpioInputOutput: Variable to configure the GPIO as Input / Output
 * \brief GpioPeripheral: Variable to configure the peripheral of the GPIO
 */
/******************************************************************************/
typedef struct GPIO_PARAMETERS
{
	Uint16 GpioPin;
	Uint16 GpioFlags;
	Uint16 GpioCpuMuxSel;
	Uint16 GpioInputOutput;
	Uint16 GpioPeripheral;
}GpioPinConfig;



/******************************************************************************/
/*!
 * \brief structure for Ipc parameters
 * \struct IpcParams
 * \brief unCpu1ToCpu2TxBuf: Data to be transmitted from CPU1 to CPU2
 * \brief unCpu2ToCpu1RxBuf: Data received from CPU2 to CPU1
 * \brief RemoteResponseFlag: not used
 * \brief LocalReceiveFlag: Variable to set if data is received from other CPU
 * \brief ErrFlag: Variable to set the error flags if any
 * \brief INT1Flag: Variable to set the interrupt flag during data transfer
 *                  between the CPUs
 * \brief TxMsgQueCurrentPtr: Varialbe to set the current transmit message queue
 *                              pointer
 * \brief RxMsgQueCurrentPtr: Varialbe to set the current Recieve message queue
 *                              pointer
 */
/******************************************************************************/
typedef struct IPC_PARAMETER {
	uint32_t unCpu1ToCpu2TxBuf[10];
	uint32_t unCpu2ToCpu1RxBuf[10];
	Uint16 RemoteResponseFlag;
	Uint16 LocalReceiveFlag;
	Uint16 ErrFlag;
	Uint16 INT1Flag;
	Uint16 TxMsgQueCurrentPtr;
//	Uint16 TxMsgQueProcessedPtr;
//	Uint16 TxMsgQueOverflowFlag;
	Uint16 RxMsgQueCurrentPtr;
//	Uint16 RxMsgQueProcessedPtr;
//	Uint16 RxMsgQueOverFlowFlag;
}stIpcParameters;

/******************************************************************************/
/*!
 *
 * \struct PRESSURE_ERROR_BITS
 * \brief struct PRESSURE_ERROR_BITS - not used
 */
/******************************************************************************/
struct PRESSURE_ERROR_BITS{
    unsigned int LOW_THRESHOLD:1;
    unsigned int HIGH_THRESHOLD:1;
    unsigned int FAULT_STATE:1;
    unsigned int AC_STATUS:2;
    unsigned int VALUE:11;
};
/******************************************************************************/
/*!
 *
 * \union PRESSURE_ERROR_STATUS
 * \brief union PRESSURE_ERROR_STATUS bits - not used
 */
/******************************************************************************/
union PRESSURE_ERROR_STATUS {
    unsigned int PressureErrStatus;
    struct PRESSURE_ERROR_BITS bit;
};

/******************************************************************************/
/*!
 *
 * \struct PressureParameter
 * \brief struct ADC_PRAMETER_PRESSURE
 *  Pressure module variables used to calculate pressure in terms of psi
 *  and error message
 */
/******************************************************************************/

typedef struct ADC_PRAMETER_PRESSURE {
	unsigned int AdcCounter;
    unsigned int AcquireStatus;
    unsigned int DataSize;
    float   ADCSpan;
    volatile float	PressureData;
    float	PressureSpan;
	float	PressureValueMin;
    float	PressureValueMax;
    float 	ADCMin;
    float	ADCMax;
    union PRESSURE_ERROR_STATUS PRESSURE_Err;
}PressureParameter;


/******************************************************************************/
/*!
 *
 * \struct stPwmParameter
 * \brief struct MOTOR_PWM_PARAMETERS
 * \brief Fequency: Variable to set the operating frequency of the Motor
 * \brief NumberOfSteps: Variable to set the no. of steps the motor should rotate
 * \brief Direction: Variable to set the Motor running direction
 * \brief Status: Variable to set the Motor status (IDLE / RUN / COMPLETED)
 * \brief TimerCounter: Variable to check the timer counter of the PWM of motor
 * \brief TimeStamp: Variable to set the time stamp of the PWM to motor
 * \brief Displacement: Varialbe to set the Dispalcement to move the motor
 * \brief StepSize: Varialbe to set the step size of the motor
 * \brief StartBandPeriod: Variable to store SCurve start band period
 * \brief StopBandPeriod: Variable to store SCurve Stop band period
 * \brief SCurveLookUp: Variable to store SCurve look up table
 * \brief SCurveLookUpCounter: Variable to check the SCurve look up table
 * \brief HomeSensorControl: Variable to check whether home status to be checked or not
 * \brief HomePosSenseStatus: Variable to set the home position sensor status
 * \brief uiMotorMinFreq: Varialbe to set the minimum frequency of the motor
 * \brief iMotorPosition: Varialbe to indicate the current motor position
 */
/******************************************************************************/
typedef struct MOTOR_PWM_PARAMETERS
{
	volatile unsigned int Fequency;
    volatile unsigned long NumberOfSteps;
    unsigned int Direction ;
    volatile unsigned int Status;
    volatile unsigned long TimerCounter;
    volatile unsigned long FBTimerCounter;
    volatile unsigned long TimeStamp[3];
    unsigned int Displacement;
    unsigned int StepSize;
	volatile unsigned int StartBandPeriod;
	volatile unsigned int StopBandPeriod;
	unsigned int SCurveLookUp[256];
	unsigned int SCurveLookUpCounter;
	unsigned int HomeSensorControl;
	Uint16 HomePosSenseStatus;
	Uint16 uiMotorMinFreq;
	long iMotorPosition;
}stPwmParameter;

/******************************************************************************/
/*!
 * \struct SpiComm
 * \brief struct SPI_COMM_PARAMETERS
 * \brief TxStatus: Variable to set the SPI transmit status
 * \brief RxStatus: Variable to set the SPI recieve status
 */
/******************************************************************************/
typedef struct SPI_COMM_PARAMETERS
{
	volatile Uint16 TxStatus;
	volatile Uint16 RxStatus;
}SpiCommParam;

/******************************************************************************/
/*!
 *
 * \struct SpiCommBuf
 * \brief struct SPI_COMM_BUFFER
 * \brief SpiDmaTxData: Array to store the SPI transmit data
 * \brief SpiDmaRxData: Array to store the SPI recieve data
 */
/******************************************************************************/
typedef struct SPI_COMM_BUFFER
{
	Uint16 SpiDmaTxData[MEM_BUFFER_SIZE * COMM_QUEUE_LENGTH];
	Uint16 SpiDmaRxData[MEM_BUFFER_SIZE * COMM_QUEUE_LENGTH];
}SpiCommBuffer;

/******************************************************************************/
/*!
 *
 * \union UT_MOTOR_HOME_POSITION
 * \struct ST_MOTOR_HOME_POSITION
 * \brief To set the home position status of all the motors
 */
/******************************************************************************/
typedef union UT_MOTOR_HOME_POSITION
{
    struct ST_MOTOR_HOME_POSITION
    {
        Uint16 usnXHomePosition:1;
        Uint16 usnYHomePosition:1;
        Uint16 usnDiluentHomePosition:1;
        Uint16 usnWasteHomePosition:1;
        Uint16 usnSampleHomePosition:1;
        Uint16 usnReserved:11;
    }stMotorHomePosition;
    Uint16 usnMotorHomePosition;
}utMotorHomePosition;
/******************************************************************************/
/*!
 *
 * \union UT_MOTOR_HOME_POSITION
 * \struct ST_MOTOR_HOME_POSITION
 * \brief To set the home position status of all the motors
 */
/******************************************************************************/
typedef union UT_ERROR_INFO
{
    struct ST_ERROR_INFO
    {
        Uint64 ulnSequenceAborted:1;
        Uint64 ulnX_Motor:1;
        Uint64 ulnDiluentSyringe:1;
        Uint64 ulnSampleSyringe:1;
        Uint64 ulnWasteSyringe:1;
        Uint64 ulnY_Motor:1;
        Uint64 ulnVacWBCDraining:1;
        Uint64 ulnVacRBCDraining:1;
        Uint64 ulnVacRBCDrainingEnd:1;
        Uint64 ulnPresWBCBubblingStart:1;
        Uint64 ulnPresWBCBubblingEnd:1;
        Uint64 ulnPresWBCBubblingStartLyse:1;
        Uint64 ulnPresRBCBubblingStartLyse:1;
        Uint64 ulnPresRBCBubblingStartLyseEnd:1;
        Uint64 ulnVacCountingStart:1;
        Uint64 ulnVacCountingEnd:1;
        Uint64 ulnVacWBCDrainingAC:1;
        Uint64 ulnVacRBCDrainingAC:1;
        Uint64 ulnVacRBCDrainingACEnd:1;
        Uint64 ulnPresBackflushStart:1;
        Uint64 ulnPresBackflushEnd:1;
        Uint64 ulnVacWBCDrainingFD:1;
        Uint64 ulnVacRBCDrainingFD:1;
        Uint64 ulnVacRBCDrainingFDEnd:1;
        Uint64 ulnAspirationNotCompleted:1;
        Uint64 ulnAspirationCompletedImproper:1;
        Uint64 ulnWBCDispensingNotCompleted:1;
        Uint64 ulnWBCDispensingCompletedImproper:1;
        Uint64 ulnRBCDispensingNotCompleted:1;
        Uint64 ulnRBCDispensingCompletedImproper:1;
        Uint64 ulnWBCRBCBubblingNotCompleted:1;
        Uint64 ulnWBCRBCBubblingCompletedImproper:1;
        Uint64 ulnBackFlushNotCompleted:1;
        Uint64 ulnBackFlushCompletedImproper:1;
        Uint64 ulnBathFillingNotCompleted:1;
        Uint64 ulnBathFillingCompletedImproper:1;
        Uint64 ulnWasteBinFull:1;
        Uint64 ulnDiluentEmpty:1;
        Uint64 ulnLyseEmpty:1;
        Uint64 ulnRinseEmpty:1;
        Uint64 ulnTemperature:1;
        Uint64 uln24V_Check:1;
        Uint64 uln5V_Check:1;
        Uint64 ulnReAcq:1;
        Uint64 ulnPressureSensorFail:1;
        Uint64 ulnLowVacuum:1;
        Uint64 ulnHighPressure:1;
        Uint64 ulnRBCStartSensor:1;
        Uint64 ulnRBCStopSensor:1;
        Uint64 ulnWBCStartSensor:1;
        Uint64 ulnWBCStopSensor:1;
        Uint64 ulnSequenceArrested:1;
        Uint64 ulnY_MotorFault:1;
        Uint64 ulnX_MotorFault:1;
        Uint64 ulnDiluent_MotorFault:1;
        Uint64 ulnSample_MotorFault:1;
        Uint64 ulnWaste_MotorFault:1;
        Uint64 PressureCalCmpltd:1;
        Uint64 ulnReserved:12;
    }stErrorInfo;
    Uint64 ulnErrorInfo;
}utErrorCheck;

typedef union UT_VOLUMETRIC_ERROR
{
    struct ST_VOLUMETRIC_ERROR
    {
        uint32_t unWBCCountingNotStarted:1;
        uint32_t unWBCCountingStartedImproper:1;
        uint32_t unRBCCountingNotStarted:1;
        uint32_t unRBCCountingStartedImproper:1;
        uint32_t unWBCCountingNotCompleted:1;
        uint32_t unWBCCountingCompletedImproper:1;
        uint32_t unRBCCountingNotCompleted:1;
        uint32_t unRBCCountingCompletedImproper:1;
        uint32_t unWBCEarlyCompletion:1;
        uint32_t unWBCLateCompletion:1;
        uint32_t unRBCEarlyCompletion:1;
        uint32_t unRBCLateCompletion:1;
//DS_TESTING
        uint32_t unWBCImproperAbortion:1;
        uint32_t unRBCImproperAbortion:1;
        uint32_t unWBCFlowCalibAbortion:1;
        uint32_t unRBCFlowCalibAbortion:1;
//DS_TESTING
        uint32_t unReserved:20;
    }stVolumetricError;
    uint32_t unVolumetricError;
}utVolumetricError;

typedef union UT_VOLUMETRIC_ERROR_RE_ACQ
{
    struct ST_VOLUMETRIC_ERROR_RE_ACQ
    {
        uint32_t unWBCCountingNotStarted:1;
        uint32_t unWBCCountingStartedImproper:1;
        uint32_t unRBCCountingNotStarted:1;
        uint32_t unRBCCountingStartedImproper:1;
        uint32_t unWBCCountingNotCompleted:1;
        uint32_t unWBCCountingCompletedImproper:1;
        uint32_t unRBCCountingNotCompleted:1;
        uint32_t unRBCCountingCompletedImproper:1;
        uint32_t unWBCEarlyCompletion:1;
        uint32_t unWBCLateCompletion:1;
        uint32_t unRBCEarlyCompletion:1;
        uint32_t unRBCLateCompletion:1;
//DS_TESTING
        uint32_t unWBCImproperAbortion:1;
        uint32_t unRBCImproperAbortion:1;
        uint32_t unWBCFlowCalibAbortion:1;
        uint32_t unRBCFlowCalibAbortion:1;
//DS_TESTING
        uint32_t usnReserved:20;
    }stVolumetricErrorReAcq;
    uint32_t unVolumetricError;
}utVolumetricErrorReAcq;

/******************************************************************************/
/*!
 * \brief structure for SPI packet frame
 * \typedef ST_SPI_PACKET_FRAME
 * \struct SSPIPacketFrame
 *!  usnStartOfPacket_MSW - Start of Packet Most Significant Word
 *!  usnStartOfPacket_LSW - Start of Packet Least Significant Word
 *!  usnReservedWord - Reserved Word for future Use
 *!  usnDataLength - Amount of Data present in packet
 *!  usnMajorCommand - Major Command
 *!  usnMinorCommand - Minor Command
 *!  arrusnData - Array to store the data to be sent
 *!  usnRemainingPacketsforTX - No. of Packets to be transmitted to indicate
 *!                             Master SPI to send clock
 *!  usnCRC - CRC
 *!  usnEndOfPacket_MSW - End of Packet Most Significant Word
 *!  usnEndOfPacket_LSW - End of Packet Least Significant Word
 */
/******************************************************************************/
typedef struct ST_SEQ_MON_TIME
{
    uint16_t usnAspCompletedHighLimit;
    uint16_t usnAspCompletedLowLimit;
    uint16_t usnFirstDilutionHighLimit;
    uint16_t usnFirstDilutionLowLimit;
    uint16_t usnRBCDispensationHighLimit;
    uint16_t usnRBCDispensationLowLimit;
    uint16_t usnWBCRBCBubblingHighLimit;
    uint16_t usnWBCRBCBubblingLowLimit;
    uint16_t usnCountingStartedHighLimit;
    uint16_t usnCountingStartedLowLimit;
    uint16_t usnCountingCompletedHighLimit;
    uint16_t usnCountingCompletedLowLimit;
    uint16_t usnBackFlushCompletedHighLimit;
    uint16_t usnBackFlushCompletedLowLimit;
    uint16_t usnBathFillingCompletedHighLimit;
    uint16_t usnBathFillingCompletedLowLimit;
}stSequenceMonitoringTime;
#endif /* INCLUDE_STRUCTURE_H_ */




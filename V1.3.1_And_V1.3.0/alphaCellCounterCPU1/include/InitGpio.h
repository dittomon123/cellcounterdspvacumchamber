/******************************************************************************/
/*! \file InitGpio.h
 *
 *  \brief Header file for InitGpio.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-05-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_gpio_module
//! @{
//******************************************************************************
#ifndef INIT_GPIO_H_
#define INIT_GPIO_H_

#include "F2837xD_device.h"     // Headerfile Include File
#include "F2837xD_Examples.h"   // Examples Include File
#include "Pwm.h"
#include "Structure.h"
#define	_ALPHA_2_BOARD_
#define _ALPHA_5_BOARD_
#define _X_AXIS_HELICAL_
#define _RPT_YAXIS_

/******************************************************************************/
/*!
 * \def SPI_SIMO_MP SPI Delfino Slave In Sitara Master Out pin GPIO58
 * \def SPI_SOMI_MP SPI Delfino Slave Out Sitara Master In pin GPIO59
 * \def SPI_CLK_MP SPI Sitara Clock pin GPIO60
 * \def SPI_CS_MP SPI Sitara Chip Select pin GPIO61
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   SPI_SIMO_MP
 * \brief Delfino Slave In Sitara Master Out pin GPIO58
 */
/******************************************************************************/
#define	SPI_SIMO_MP													58

/******************************************************************************/
/*!
 * \def   SPI_SOMI_MP
 * \brief Delfino Slave Out Sitara Master In pin GPIO59
 */
/******************************************************************************/
#define	SPI_SOMI_MP													59

/******************************************************************************/
/*!
 * \def   SPI_CLK_MP
 * \brief SPI Sitara Clock pin GPIO60
 */
/******************************************************************************/
#define	SPI_CLK_MP													60

/******************************************************************************/
/*!
 * \def   SPI_CS_MP
 * \brief Sitara Chip Select pin GPIO61
 */
/******************************************************************************/
#define	SPI_CS_MP													61

/******************************************************************************/
/*!
 * \def   SPI_PERIPHERAL_MP
 * \brief TBD
 */
/******************************************************************************/
#define SPI_PERIPHERAL_MP											15


/******************************************************************************/
/*!
 * \def SPI_COMM_DELFINO_TXR_INT_REQ	SPI Delfino Transfer interrupt \
 *                                      request pin GPIO165
 * \def SPI_COMM_DELFINO_BUSY_STATUS	SPI Delfino Busy state pin GPIO166
 * \def SPI_COMM_SITARA_BUSY_STATUS		SPI Sitara Busy state pin GPIO167
 * \def SPI_COMM_SITARA_ABORT_REQ		SPI Sitara Abort Request pin GPIO168
 *
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   SPI_COMM_CTRL_DELFINO_TXR_INT_REQ
 * \brief SPI Delfino Transfer interrupt request pin GPIO165
 */
/******************************************************************************/
#define	SPI_COMM_CTRL_DELFINO_TXR_INT_REQ							165

/******************************************************************************/
/*!
 * \def   SPI_COMM_CTRL_DELFINO_BUSY_STATUS
 * \brief SPI Delfino Busy state pin GPIO166
 */
/******************************************************************************/
#define	SPI_COMM_CTRL_DELFINO_BUSY_STATUS							166

/******************************************************************************/
/*!
 * \def   SPI_COMM_CTRL_SITARA_BUSY_STATUS
 * \brief SPI Sitara Busy state pin GPIO167
 */
/******************************************************************************/
#define	SPI_COMM_CTRL_SITARA_BUSY_STATUS							167

/******************************************************************************/
/*!
 * \def   SPI_COMM_CTRL_SITARA_ABORT_REQ
 * \brief SPI Sitara Abort Request pin GPIO168
 */
/******************************************************************************/
#define	SPI_COMM_CTRL_SITARA_ABORT_REQ								168

/******************************************************************************/
/*!
 * \def   GPIO_PERIPHERAL_MP
 * \brief TBD
 */
/******************************************************************************/
#define GPIO_PERIPHERAL_MP											0

/******************************************************************************/
/*!
 * \def SPI interface and pin configuration of valve control
 * \def SPI_SIMO_VALVE    Pin no of Slave input and Master output pin
 * \def SPI_SOMI_VALVE    Pin no of Slave output and Master input pin
 * \def SPI_CLK_VALVE     Pin no of SPI clock pin
 * \def SPI_CS_VALVE      Pin no of SPI chip select
 * \def SPI_RESET_VALVE   Pin no of SPI reset
 * \def SPI_PERIPHERAL_VALVE   Pin no of SPI peripheral
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   SPI_SIMO_VALVE
 * \brief Pin no of Slave input and Master output pin in SPI interface of valve
 *        control
 */
/******************************************************************************/
#define	SPI_SIMO_VALVE												122

/******************************************************************************/
/*!
 * \def   SPI_SOMI_VALVE
 * \brief Pin no of Slave output and Master input pin in SPI interface of valve
 *        control
 */
/******************************************************************************/
#define	SPI_SOMI_VALVE												123

/******************************************************************************/
/*!
 * \def   SPI_CLK_VALVE
 * \brief Pin no of SPI clock pin in SPI interface of valve control
 */
/******************************************************************************/
#define	SPI_CLK_VALVE												124

/******************************************************************************/
/*!
 * \def   SPI_CS_VALVE
 * \brief Pin no of SPI chip select in SPI interface of valve control
 */
/******************************************************************************/
#define	SPI_CS_VALVE												125

/******************************************************************************/
/*!
 * \def   SPI_RESET_VALVE
 * \brief Pin no of SPI reset in SPI interface of valve control
 */
/******************************************************************************/
#define	SPI_RESET_VALVE												5

/******************************************************************************/
/*!
 * \def   SPI_PERIPHERAL_VALVE
 * \brief Pin no of SPI peripheral in SPI interface of valve control
 */
/******************************************************************************/
#define SPI_PERIPHERAL_VALVE										6

/******************************************************************************/
/*!
 * \def SPI interface and pin configuration of Home sensor
 * \def SPI_SIMO_HOME_SENSE    Pin no of Slave input and Master output pin
 * \def SPI_SOMI_HOME_SENSE    Pin no of Slave output and Master input pin
 * \def SPI_CLK_HOME_SENSE     Pin no of SPI clock pin
 * \def SPI_CS_HOME_SENSE      Pin no of SPI chip select
 * \def SPI_PERIPHERAL_HOME_SENSE   Pin no of SPI peripheral
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   SPI_SIMO_HOME_SENSE
 * \brief Currently it is not used
 */
/******************************************************************************/
#define	SPI_SIMO_HOME_SENSE											63

/******************************************************************************/
/*!
 * \def   SPI_SOMI_HOME_SENSE
 * \brief Currently it is not used
 */
/******************************************************************************/
#define	SPI_SOMI_HOME_SENSE											64

/******************************************************************************/
/*!
 * \def   SPI_CLK_HOME_SENSE
 * \brief Currently it is not used
 */
/******************************************************************************/
#define	SPI_CLK_HOME_SENSE											65

/******************************************************************************/
/*!
 * \def   SPI_CS_HOME_SENSE
 * \brief Currently it is not used
 */
/******************************************************************************/
#define	SPI_CS_HOME_SENSE											118//66

/******************************************************************************/
/*!
 * \def   SPI_PERIPHERAL_HOME_SENSE
 * \brief Currently it is not used
 */
/******************************************************************************/
#define SPI_PERIPHERAL_HOME_SENSE									15

/******************************************************************************/
/*!
 * \brief Pin configuration of uart port for debug messages
 * \def DEBUG_PORT_UART_TX
 * \brief For transmit
 * \def DEBUG_PORT_UART_RX
 * \brief for receive from PC
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   DEBUG_PORT_UART_TX
 * \brief Transmit Pin in uart port for debug messages
 */
/******************************************************************************/
#define	DEBUG_PORT_UART_TX											8

/******************************************************************************/
/*!
 * \def   DEBUG_PORT_UART_RX
 * \brief Receive Pin in uart port for debug messages
 */
/******************************************************************************/
#define	DEBUG_PORT_UART_RX											9

/******************************************************************************/
/*!
 * \def   DEBUG_PORT_UART_PERIPHERAL
 * \brief TBD
 */
/******************************************************************************/
#define DEBUG_PORT_UART_PERIPHERAL									6

/******************************************************************************/
/*!
 * \def definitions of Home Position Sensor detection pins
 * \def GPIO_HOME_POS1  Pin no. of home position sensor: X-Axis
 * \def GPIO_HOME_POS2  Pin no. of home position sensor: Diluent
 * \def GPIO_HOME_POS3  Pin no. of home position sensor: Sample
 * \def GPIO_HOME_POS4  Pin no. of home position sensor: Waste
 * \def GPIO_HOME_POS5  Pin no. of home position sensor: Y-Axis
 * \def GPIO_HOME_POS_INT    Pin no. of home position interrupt
 */
/******************************************************************************/
#ifdef _ALPHA_2_BOARD_
#ifdef _ALPHA_5_BOARD_
/******************************************************************************/
/*!
 * \def   GPIO_HOME_POS1
 * \brief Pin no. of home position sensor: Waste
 */
/******************************************************************************/
#define     GPIO_HOME_POS1                                          114 //Waste

/******************************************************************************/
/*!
 * \def   GPIO_HOME_POS2
 * \brief Pin no. of home position sensor: Diluent
 */
/******************************************************************************/
#define     GPIO_HOME_POS2                                          115 //Diluent

/******************************************************************************/
/*!
 * \def   GPIO_HOME_POS3
 * \brief Pin no. of home position sensor: Sample
 */
/******************************************************************************/
#define     GPIO_HOME_POS3                                          113 //Sample

/******************************************************************************/
/*!
 * \def   GPIO_HOME_POS4
 * \brief Pin no. of home position sensor: X-Axis
 */
/******************************************************************************/
#define     GPIO_HOME_POS4                                          111 //X-Axis

/******************************************************************************/
/*!
 * \def   GPIO_HOME_POS5
 * \brief Pin no. of home position sensor: Y-Axis
 */
/******************************************************************************/
#define     GPIO_HOME_POS5                                          112 //Y-Axis

#else
#define 	GPIO_HOME_POS1											111
#define 	GPIO_HOME_POS2											112
#define 	GPIO_HOME_POS3											113
#define 	GPIO_HOME_POS4											114
#define 	GPIO_HOME_POS5											115
#endif
#define 	GPIO_HOME_POS_INT										106
#else
#define 	GPIO_HOME_POS1											107
#define 	GPIO_HOME_POS2											66
#define 	GPIO_HOME_POS3											109
#define 	GPIO_HOME_POS4											110
#define 	GPIO_HOME_POS5											111
#endif

/******************************************************************************/
/*!
 * \def definitions of Volumetric Sensor detection pins
 * \def GPIO_VOL1  Pin no. of volumetric interrupt: RBC starting
 * \def GPIO_VOL2  Pin no. of volumetric interrupt: RBC ending
 * \def GPIO_VOL3  Pin no. of volumetric interrupt: WBC starting
 * \def GPIO_VOL4  Pin no. of volumetric interrupt: WBC ending
 */
/******************************************************************************/
#ifdef _ALPHA_2_BOARD_

/******************************************************************************/
/*!
 * \def   GPIO_VOL1
 * \brief Pin no. of volumetric interrupt: RBC starting
 */
/******************************************************************************/
#define 	GPIO_VOL1												107

/******************************************************************************/
/*!
 * \def   GPIO_VOL2
 * \brief Pin no. of volumetric interrupt: RBC ending
 */
/******************************************************************************/

#define 	GPIO_VOL2												108

/******************************************************************************/
/*!
 * \def   GPIO_VOL3
 * \brief Pin no. of volumetric interrupt: WBC starting
 */
/******************************************************************************/
#define 	GPIO_VOL3												109

/******************************************************************************/
/*!
 * \def   GPIO_VOL3
 * \brief Pin no. of volumetric interrupt: WBC ending
 */
/******************************************************************************/
#define 	GPIO_VOL4												110
#endif

/******************************************************************************/
/*!
 * \def Definitions of PWM pins for Solenoid & HGB
 * \def S_PWM_VALVE  Pin no. of pwm control of solinoid valve
 * \def HB_PWM_HGB  Pin no. of pwm control of HgB
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   S_PWM_VALVE
 * \brief Pin no. of pwm control of solinoid valve
 */
/******************************************************************************/
#define	S_PWM_VALVE													151

/******************************************************************************/
/*!
 * \def   HB_PWM_HGB
 * \brief Pin no. of pwm control of HgB
 */
/******************************************************************************/
#define	HB_PWM_HGB													157

/******************************************************************************/
/*!
 * \def Definitions of Solenoid valve & HGB peripheral pins
 * \def S_PWM_VALVE_PERIPHERAL  Pin no. of SPI valve peripheral
 * \def HB_PWM_HGB_PERIPHERAL  Pin no. of HgB peripheral
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   S_PWM_VALVE_PERIPHERAL
 * \brief Pin no. of SPI valve peripheral
 */
/******************************************************************************/
#define S_PWM_VALVE_PERIPHERAL										1

/******************************************************************************/
/*!
 * \def   HB_PWM_HGB_PERIPHERAL
 * \brief Pin no. of HgB peripheral
 */
/******************************************************************************/
#define HB_PWM_HGB_PERIPHERAL										1

/******************************************************************************/
/*!
 * \def Definitions of LED pins
 * \def LED1_GPIO  Pin no. of LED1
 * \def LED2_GPIO  Pin no. of LED2
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   LED1_GPIO
 * \brief Pin no. of LED1
 */
/******************************************************************************/
#define	LED1_GPIO													116

/******************************************************************************/
/*!
 * \def   LED2_GPIO
 * \brief Pin no. of LED2
 */
/******************************************************************************/
#define	LED2_GPIO													117

/******************************************************************************/
/*!
 * \def   SAMPLE_START_BUTTON_GPIO
 * \brief Pin no. of sample start button
 */
/******************************************************************************/
#define	SAMPLE_START_BUTTON_GPIO									126

/******************************************************************************/
/*!
 * \def Definitions of cycle counter GPIO pins to count no. of cycles run in \
 * sample mode
 * \def CYCLE_COUNTER_GPIO_OUTPUT1 Pin no. of cycle counter 1
 * \def CYCLE_COUNTER_GPIO_OUTPUT2 Pin no. of cycle counter 2
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   CYCLE_COUNTER_GPIO_OUTPUT1
 * \brief Pin no. of cycle counter 1 which uses to count no.of cycles run in
 *        sample mode
 */
/******************************************************************************/
#define CYCLE_COUNTER_GPIO_OUTPUT1									53

/******************************************************************************/
/*!
 * \def   CYCLE_COUNTER_GPIO_OUTPUT2
 * \brief Pin no. of cycle counter 2 which uses to count no.of cycles run in
 *        sample mode
 */
/******************************************************************************/
#define CYCLE_COUNTER_GPIO_OUTPUT2									54

/******************************************************************************/
/*!
 * \brief definitions of Zapping voltage pin selection
 * \def HIGH_VOLTAGE_SELECT_GPIO_OUTPUT
 * \brief Pin no. of high voltage pin selection
 * \def ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT
 * \brief Pin no. of Zap voltage switch selection
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   HIGH_VOLTAGE_SELECT_GPIO_OUTPUT
 * \brief Pin no. of high voltage pin selection for Zapping voltage
 */
/******************************************************************************/
#define	HIGH_VOLTAGE_SELECT_GPIO_OUTPUT								161

/******************************************************************************/
/*!
 * \def   ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT
 * \brief Pin no. of Zap voltage switch selection
 */
/******************************************************************************/
#define	ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT								162

/******************************************************************************/
/*!
 * \def   HIGH_VOLTAGE_SELECT_GPIO_OUTPUT
 * \brief Pin no. of high voltage pin selection for Zapping voltage
 */
/******************************************************************************/
#define HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT                         163

/******************************************************************************/
/*!
 * \brief definitions of waste bin full detection pin selection
 * \def WASTE_DETECT_GPIO
 * \brief Pin no. of waste bun full detection
 */
/******************************************************************************/
#define WASTE_DETECT_GPIO                                           100

/******************************************************************************/
/*!
 * \brief definitions of motor reset pin
 * \def M_RESET
 * \brief Pin no. of motor reset
 */
/******************************************************************************/
#define 	M_RESET													10


/******************************************************************************/
/*!
 * \brief definitions of Motor 1 configuration - X-Axis
 * \def M1_MODE0
 * \brief Pin no. of Mode 0
 * \def M1_MODE1    Pin no. of Mode 1
 * \def M1_MODE2    Pin no. of Mode 2
 * \def M1_IN1     Pin no. of Motor Enable
 * \def M1_IN2      Pin no. of Motor Direction
 * \def M1_DECAY    Pin no. of Motor Decay
 * \def M1_FAULT    Pin no. of Motor Fault
 */
/******************************************************************************/
//#define 	M1_STEP													14		//PWM8A defined in Pwm.c

/******************************************************************************/
/*!
 * \brief definitions of Motor 1 configuration - X-Axis
 * \def   M1_MODE0
 * \brief Pin no. of Mode 0
 */
/******************************************************************************/
#define 	M1_MODE0												4

/******************************************************************************/
/*!
 * \brief definitions of Motor 1 configuration - X-Axis
 * \def   M1_MODE1
 * \brief Pin no. of Mode 1
 */
/******************************************************************************/
#define 	M1_MODE1												3

/******************************************************************************/
/*!
 * \brief definition of Motor 1 configuration - X-Axis
 * \def   M1_MODE2
 * \brief Pin no. of Mode 2
 */
/******************************************************************************/
#define 	M1_MODE2												2

/******************************************************************************/
/*!
 * \brief definition of Motor 1 configuration - X-Axis
 * \def   M1_IN1
 * \brief Pin no. of Motor enable
 */
/******************************************************************************/
#define 	M1_IN1													6

/******************************************************************************/
/*!
 * \brief definition of Motor 1 configuration - X-Axis
 * \def   M1_IN2
 * \brief Pin no. of Motor direction
 */
/******************************************************************************/
#define 	M1_IN2													7

/******************************************************************************/
/*!
 * \brief definition of Motor 1 configuration - X-Axis
 * \def   M1_DECAY
 * \brief Pin no. of Motor current decay
 */
/******************************************************************************/
#define 	M1_DECAY												16

/******************************************************************************/
/*!
 * \brief definition of Motor 1 configuration - X-Axis
 * \def   M1_FAULT
 * \brief Pin no. of Motor fault status
 */
/******************************************************************************/
#define 	M1_FAULT												17

/******************************************************************************/
/*!
 * \def Definitions of Motor 2 configuration - Diluent
 * \def M2_MODE0    Pin no. of Mode 0
 * \def M2_MODE1    Pin no. of Mode 1
 * \def M2_MODE2    Pin no. of Mode 2
 * \def M2_ENBL     Pin no. of Motor Enable
 * \def M2_DIR      Pin no. of Motor Direction
 * \def M2_DECAY    Pin no. of Motor Decay
 * \def M2_FAULT    Pin no. of Motor Fault
 */
/******************************************************************************/
//#define 	M2_STEP													155		//PWM6A defined in Pwm.c
/******************************************************************************/
/*!
 * \brief definition of Motor 2 configuration - Diluent
 * \def   M2_MODE0
 * \brief Pin no. of Motor mode 0
 */
/******************************************************************************/
#define 	M2_MODE0												13

/******************************************************************************/
/*!
 * \brief definition of Motor 2 configuration - Diluent
 * \def   M2_MODE1
 * \brief Pin no. of Motor mode 1
 */
/******************************************************************************/
#define 	M2_MODE1												12

/******************************************************************************/
/*!
 * \brief definition of Motor 2 configuration - Diluent
 * \def   M2_MODE2
 * \brief Pin no. of Motor mode 2
 */
/******************************************************************************/
#define 	M2_MODE2												11

/******************************************************************************/
/*!
 * \brief definition of Motor 2 configuration - Diluent
 * \def   M2_ENBL
 * \brief Pin no. of Motor enable
 */
/******************************************************************************/
#define 	M2_ENBL													15

/******************************************************************************/
/*!
 * \brief definition of Motor 2 configuration - Diluent
 * \def   M2_DIR
 * \brief Pin no. of Motor direction
 */
/******************************************************************************/
#define 	M2_DIR													20

/******************************************************************************/
/*!
 * \brief definition of Motor 2 configuration - Diluent
 * \def   M2_DECAY
 * \brief Pin no. of Motor current decay
 */
/******************************************************************************/
#define 	M2_DECAY												21

/******************************************************************************/
/*!
 * \brief definition of Motor 2 configuration - Diluent
 * \def   M2_FAULT
 * \brief Pin no. of Motor fault status
 */
/******************************************************************************/
#define 	M2_FAULT												22

/******************************************************************************/
/*!
 * \def Definitions of Motor 3 configuration - Sample
 * \def M3_MODE0    Pin no. of Mode 0
 * \def M3_MODE1    Pin no. of Mode 1
 * \def M3_MODE2    Pin no. of Mode 2
 * \def M3_ENBL     Pin no. of Motor Enable
 * \def M3_DIR      Pin no. of Motor Direction
 * \def M3_DECAY    Pin no. of Motor Decay
 * \def M3_FAULT    Pin no. of Motor Fault
 */
/******************************************************************************/
//#define 	M3_STEP													147		//PWM2A defined in Pwm.c
/******************************************************************************/
/*!
 * \brief definition of Motor 3 configuration - Sample
 * \def   M3_MODE0
 * \brief Pin no. of Motor mode 0
 */
/******************************************************************************/
#define 	M3_MODE0												25

/******************************************************************************/
/*!
 * \brief definition of Motor 3 configuration - Sample
 * \def   M3_MODE1
 * \brief Pin no. of Motor mode 1
 */
/******************************************************************************/
#define 	M3_MODE1												24

/******************************************************************************/
/*!
 * \brief definition of Motor 3 configuration - Sample
 * \def   M3_MODE2
 * \brief Pin no. of Motor mode 2
 */
/******************************************************************************/
#define 	M3_MODE2												23

/******************************************************************************/
/*!
 * \brief definition of Motor 3 configuration - Sample
 * \def   M3_ENBL
 * \brief Pin no. of Motor enable
 */
/******************************************************************************/
#define 	M3_ENBL													27

/******************************************************************************/
/*!
 * \brief definition of Motor 3 configuration - Sample
 * \def   M3_DIR
 * \brief Pin no. of Motor direction
 */
/******************************************************************************/
#define 	M3_DIR													28

/******************************************************************************/
/*!
 * \brief definition of Motor 3 configuration - Sample
 * \def   M3_DECAY
 * \brief Pin no. of Motor current decay
 */
/******************************************************************************/
#define 	M3_DECAY												33

/******************************************************************************/
/*!
 * \brief definition of Motor 3 configuration - Sample
 * \def   M3_FAULT
 * \brief Pin no. of Motor fault status
 */
/******************************************************************************/
#define 	M3_FAULT												19

/******************************************************************************/
/*!
 * \def Definitions of Motor 4 configuration - Waste
 * \def M4_MODE0    Pin no. of Mode 0
 * \def M4_MODE1    Pin no. of Mode 1
 * \def M4_MODE2    Pin no. of Mode 2
 * \def M4_ENBL     Pin no. of Motor Enable
 * \def M4_DIR      Pin no. of Motor Direction
 * \def M4_DECAY    Pin no. of Motor Decay
 * \def M4_FAULT    Pin no. of Motor Fault
 */
/******************************************************************************/
//#define 	M4_STEP													153		//PWM5A defined in Pwm.c

/******************************************************************************/
/*!
 * \brief definition of Motor 4 configuration - Waste
 * \def   M4_MODE0
 * \brief Pin no. of Motor mode 0
 */
/******************************************************************************/
#define 	M4_MODE0												37

/******************************************************************************/
/*!
 * \brief definition of Motor 4 configuration - Waste
 * \def   M4_MODE1
 * \brief Pin no. of Motor mode 1
 */
/******************************************************************************/
#define 	M4_MODE1												35

/******************************************************************************/
/*!
 * \brief definition of Motor 4 configuration - Waste
 * \def   M4_MODE2
 * \brief Pin no. of Motor mode 2
 */
/******************************************************************************/
#define 	M4_MODE2												34

/******************************************************************************/
/*!
 * \brief definition of Motor 4 configuration - Waste
 * \def   M4_ENBL
 * \brief Pin no. of Motor enable
 */
/******************************************************************************/
#define 	M4_ENBL													91

/******************************************************************************/
/*!
 * \brief definition of Motor 4 configuration - Waste
 * \def   M4_DIR
 * \brief Pin no. of Motor direction
 */
/******************************************************************************/
#define 	M4_DIR													150

/******************************************************************************/
/*!
 * \brief definition of Motor 4 configuration - Waste
 * \def   M4_DECAY
 * \brief Pin no. of Motor current decay
 */
/******************************************************************************/
#define 	M4_DECAY												95

/******************************************************************************/
/*!
 * \brief definition of Motor 4 configuration - Waste
 * \def   M4_FAULT
 * \brief Pin no. of Motor fault status
 */
/******************************************************************************/
#define 	M4_FAULT												96

/******************************************************************************/
/*!
 * \def Definitions of Motor 5 configuration - Y-Axis
 * \def M5_MODE0    Pin no. of Mode 0
 * \def M5_MODE1    Pin no. of Mode 1
 * \def M5_MODE2    Pin no. of Mode 2
 * \def M5_ENBL     Pin no. of Motor Enable
 * \def M5_DIR      Pin no. of Motor Direction
 * \def M5_DECAY    Pin no. of Motor Decay
 * \def M5_FAULT    Pin no. of Motor Fault
 */
/******************************************************************************/
//#define 	M5_STEP													0		//PWM1A defined in Pwm.c
/******************************************************************************/
/*!
 * \brief definition of Motor 5 configuration - Y-Axis
 * \def   M5_MODE0
 * \brief Pin no. of Motor mode 0
 */
/******************************************************************************/
#define 	M5_MODE0												99

/******************************************************************************/
/*!
 * \brief definition of Motor 5 configuration - Y-Axis
 * \def   M5_MODE1
 * \brief Pin no. of Motor mode 1
 */
/******************************************************************************/
#define 	M5_MODE1												98

/******************************************************************************/
/*!
 * \brief definition of Motor 5 configuration - Y-Axis
 * \def   M5_MODE2
 * \brief Pin no. of Motor mode 2
 */
/******************************************************************************/
#define 	M5_MODE2												97

/******************************************************************************/
/*!
 * \brief definition of Motor 5 configuration - Y-Axis
 * \def   M5_ENBL
 * \brief Pin no. of Motor enable
 */
/******************************************************************************/
#define 	M5_ENBL													101

/******************************************************************************/
/*!
 * \brief definition of Motor 5 configuration - Y-Axis
 * \def   M5_DIR
 * \brief Pin no. of Motor direction
 */
/******************************************************************************/
#define 	M5_DIR													102

/******************************************************************************/
/*!
 * \brief definition of Motor 5 configuration - Y-Axis
 * \def   M5_DECAY
 * \brief Pin no. of Motor current decay
 */
/******************************************************************************/
#define 	M5_DECAY												103

/******************************************************************************/
/*!
 * \brief definition of Motor 5 configuration - Y-Axis
 * \def   M5_FAULT
 * \brief Pin no. of Motor fault status
 */
/******************************************************************************/
#define 	M5_FAULT												94

/******************************************************************************/
/*!
 * \def definitions of GPIO to monitor the reagent priming line
 * \def DILUENT_PRIME_CHECK     Pin no. of Diluent line monitoring
 * \def LYSE_PRIME_CHECK        Pin no. of Lyse line monitoring
 * \def RINSE_PRIME_CHECK       Pin no. of Rinse line monitoring
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \brief definitions of GPIO to monitor the reagent priming line
 * \def   DILUENT_PRIME_CHECK
 * \brief Pin no. of Diluent line monitoring
 */
/******************************************************************************/
#define     DILUENT_PRIME_CHECK                                     128

/******************************************************************************/
/*!
 * \brief definitions of GPIO to monitor the reagent priming line
 * \def   LYSE_PRIME_CHECK
 * \brief Pin no. of Lyse line monitoring
 */
/******************************************************************************/
#define     LYSE_PRIME_CHECK                                        132

/******************************************************************************/
/*!
 * \brief definitions of GPIO to monitor the reagent priming line
 * \def   RINSE_PRIME_CHECK
 * \brief Pin no. of RINSE line monitoring
 */
/******************************************************************************/
#define     RINSE_PRIME_CHECK                                       134

/******************************************************************************/
/*!
 * \def Definitions of sample start button GPIO
 * \def SAMPLE_START_BTN_PRESS  Pin no. of sample start button
 */
/******************************************************************************/
#define		SAMPLE_START_BTN_PRESS		(GpioDataRegs.GPDDAT.bit.GPIO126)




/******************************************************************************/
/*!
 *
 * \fn void IGInitialize_Gpio(void);
 * \brief Calls all GPIO configuration function
 * \fn void IGBlinkLED( unsigned int  mSecLEDBlink);
 * \brief Blink LED function
 * \fn void IGConfigLED( unsigned int  Pin,  unsigned int  Status);
 * \brief Led GPIO configuration to write 0 and 1 to the pin
 * \fn void IGGpioConfig_LED(void);
 * \brief GPIO configuration of the LED 1 and LED 2
 * \fn void IGGpioConfig_HighVoltPin(void);
 * \brief GPIO configuration of the High voltage pin for Zapping
 * \fn void IGConfigGpioPin(GpioPinConfig SetGpioPin);
 * \brief GPIO Mux configuration
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     void IGInitialize_Gpio()
 *  \brief  Calls all GPIO configuration function
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGInitialize_Gpio(void);

/******************************************************************************/
/*!
 *  \fn     void IGBlinkLED(unsigned int  mSecLEDBlink)
 *  \brief  Blink LED function
 *  \param  unsigned int
 *  \return void
 */
/******************************************************************************/
void IGBlinkLED( unsigned int  mSecLEDBlink);

/******************************************************************************/
/*!
 *  \fn     void IGConfigLED( unsigned int  Pin,  unsigned int  State)
 *  \brief  Led GPIO configuration to write 0 and 1 to the pin
 *  \param  unsigned int,unsigned int
 *  \return void
 */
/******************************************************************************/
void IGConfigLED( unsigned int  Pin,  unsigned int  State);

/******************************************************************************/
/*!
 *  \fn     void IGConfigLED( unsigned int  Pin,  unsigned int  State)
 *  \brief  Wrapper function for High Voltage operation
 *  \param  unsigned int,unsigned int
 *  \return void
 */
/******************************************************************************/
void IGConfigHighVoltageModule( unsigned int  Pin,  unsigned int  State);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_DefaultState(void)
 *  \brief  Wrapper function for  GPIO pin default state setup
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_DefaultState(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_LED(void)
 *  \brief  Wrapper function for LED GPIO pin setup
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_LED(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_HighVoltPin(void)
 *  \brief  Wrapper function for High Voltage GPIO pin setup
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_HighVoltPin(void);

/******************************************************************************/
/*!
 *  \fn     void IGConfigGpioPin(GpioPinConfig SetGpioPin)
 *  \brief  Wrapper function for High Voltage GPIO pin setup
 *  \param  GpioPinConfig(structure)
 *  \return void
 */
/******************************************************************************/
void IGConfigGpioPin(GpioPinConfig SetGpioPin);

/******************************************************************************/
/*!
 *
 * \fn void IGGpioConfig_Motor1(void);
 * \brief Motor 1 GPIO Mux configuration
 * \fn void IGGpioConfig_Motor2(void);
 * \brief Motor 2 GPIO Mux configuration
 * \fn void IGGpioConfig_Motor3(void);
 * \brief Motor 3 GPIO Mux configuration
 * \fn void IGGpioConfig_Motor4(void);
 * \brief Motor 4 GPIO Mux configuration
 * \fn void IGGpioConfig_Motor5(void);
 * \brief Motor 5 GPIO Mux configuration
 * \fn void IGGpioConfig_MotorReset(void);
 * \brief Motor Reset GPIO Mux configuration
 * \fn void IGGpioConfig_SampleStartButton(void);
 * \brief Sample start button GPIO Mux configuration
 */
/******************************************************************************/

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_Motor1(void)
 *  \brief  Wrapper function for Motor 1 GPIO pin setup
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor1(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_Motor2(void)
 *  \brief  Wrapper function for Motor 2 GPIO pin setup
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor2(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_Motor3(void)
 *  \brief  Wrapper function for Motor 3 GPIO pin setup
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor3(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_Motor4(void)
 *  \brief  Wrapper function for Motor 4 GPIO pin setup
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor4(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_Motor5(void)
 *  \brief  Wrapper function for Motor 5 GPIO pin setup
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_Motor5(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_MotorReset(void)
 *  \brief  Wrapper function for Motor input gpio for reset
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_MotorReset(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_MotorReset(void)
 *  \brief  Wrapper function for  GPIO pin default state setup for Sample
 *          Start Button
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_SampleStartButton(void);

/******************************************************************************/
/*!
 * \fn int IGWriteMotorGpio( unsigned int  Pin,  unsigned int  State)
 * \brief To write 0 and 1 to GPIO
 * \fn Uint16 IGReadMotorGpio( unsigned int  Pin)
 * \brief To read motor fault pins status
 * \fn void IGGpioConfig_CycleCounterPin(void)
 * \brief Cycle counter pins mux configuration
 * \fn void IGWriteCycleCounterGpio( unsigned int  Pin,  unsigned int  State)
 * \brief Write the status (0 or 1) to cycle counter pin
 * \fn void IGGpioConfig_SpiComm(void)
 * \brief SPI comm pins mux configuration
 * \fn void IGGpioConfig_SpiCommControlPin(void)
 * \brief SPI comm control pins mux configuration
 * \fn void IGGpioConfig_UartComm(void)
 * \brief UART comm pins mux configuration
 * \fn void IGGpioConfig_SpiValve(void)
 * \brief SPI valve comm pins mux configuration
 * \fn void IGWriteSpiValveResetPin( unsigned int  Pin,  unsigned int  State)
 * \brief SPI valve reset pins mux configuration
 * \fn void IGGpioConfig_SpiHomeSense(void)
 * \brief SPI home position sensor pins mux configuration
 * \fn void IGGpioConfig_ValvePwm(void)
 * \brief Valve PWM pins mux configuration
 * \fn void IGGpioConfig_HgbPwm(void)
 * \brief HgB PWM pins mux configuration
 * \fn void IGGpioConfig_WasteDetectPin(void)
 * \brief Waste bin full detection pin mux configuration
 * \fn void IGConfigHomeSensCS( unsigned int  Pin,  unsigned int  State)
 * \brief Reading the pin status of Homeposition sensor
 * \fn void IGInit_GPIO_HomePositionSensor(void)
 * \brief Home position sensor GPIO configuration
 * \fn void IGGpioConfig_ReagentPrimingCheck(void)
 * \brief Reagent priming sensing GPIO configuration
 */
/******************************************************************************/

/******************************************************************************/
/*!
 *  \fn     int IGWriteMotorGpio( unsigned int  Pin,  unsigned int  State)
 *  \brief  Wrapper function for Motor input gpio
 *          Start Button
 *  \param  unsigned int
 *  \param  unsigned int
 *  \return int
 */
/******************************************************************************/
int IGWriteMotorGpio( unsigned int  Pin,  unsigned int  State);

/******************************************************************************/
/*!
 *  \fn     Uint16 IGReadMotorGpio( unsigned int  Pin)
 *  \brief  To read motor fault pins status
 *  \param  unsigned int
 *  \return Uint16
 */
/******************************************************************************/
Uint16 IGReadMotorGpio( unsigned int  Pin);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_CycleCounterPin(void)
 *  \brief  TWrapper function for Cycle counter input GPIO pin setup
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_CycleCounterPin(void);

/******************************************************************************/
/*!
 *  \fn     void IGWriteCycleCounterGpio(unsigned int  Pin, unsigned int  State)
 *  \brief  TWrapper function for Cycle counter input GPIO pin setup
 *  \param  unsigned int
 *  \param  unsigned int
 *  \return void
 */
/******************************************************************************/
void IGWriteCycleCounterGpio( unsigned int  Pin,  unsigned int  State);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_SpiComm(void)
 *  \brief  Wrapper function for  GPIO pin default state setup for
 *          SPI configuration
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_SpiComm(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_SpiCommControlPin(void)
 *  \brief  Wrapper function for  GPIO pin default state setup for
 *          SPI commn control pins
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_SpiCommControlPin(void);

/******************************************************************************/
/*!
 *  \fn     void IGWriteSpiCommControlPin(unsigned int Pin,unsigned int State)
 *  \brief  Wrapper function for spi communication control gpio
 *  \param  unsigned int
 *  \param  unsigned int
 *  \return void
 */
/******************************************************************************/
void IGWriteSpiCommControlPin( unsigned int  Pin,  unsigned int  State);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_UartComm(void)
 *  \brief  Wrapper function for  GPIO pin default state setup for
 *          UART configuration
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_UartComm(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_SpiValve(void)
 *  \brief  Wrapper function for  GPIO pin default state setup for
 *          SPI Valve configuration
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_SpiValve(void);

/******************************************************************************/
/*!
 *  \fn     void IGWriteSpiValveResetPin( unsigned int Pin,unsigned int State)
 *  \brief  Wrapper function for  GPIO pin default state setup for
 *          SPI Valve configuration
 *  \param  unsigned int
 *  \param  unsigned int
 *  \return void
 */
/******************************************************************************/
void IGWriteSpiValveResetPin( unsigned int  Pin,  unsigned int  State);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_SpiHomeSense(void)
 *  \brief  Wrapper function for  GPIO pin default state setup for
 *          SPI home sensor configuration
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_SpiHomeSense(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_ValvePwm(void)
 *  \brief  Wrapper function for  GPIO pin default state setup for Valve
 *          PWM configuration
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_ValvePwm(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_ValvePwm(void)
 *  \brief  Wrapper function for  GPIO pin default state setup for Hgb
 *          PWM configuration
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_HgbPwm(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_WasteDetectPin(void)
 *  \brief  Wrapper function for  GPIO pin default state setup for
 *          Waste Detect pin
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_WasteDetectPin(void);//SKM_WASTE_DETECT_CHANGE

/******************************************************************************/
/*!
 *  \fn     void IGConfigHomeSensCS( unsigned int  Pin,  unsigned int  State)
 *  \brief  Wrapper function for LED operation
 *  \param  unsigned int
 *  \param  unsigned int
 *  \return void
 */
/******************************************************************************/
void IGConfigHomeSensCS( unsigned int  Pin,  unsigned int  State);

/******************************************************************************/
/*!
 *  \fn     void IGConfigHomeSensCS( unsigned int  Pin,  unsigned int  State)
 *  \brief  Configure GPIO's pinmux
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGInit_GPIO_HomePositionSensor(void);

/******************************************************************************/
/*!
 *  \fn     void IGGpioConfig_ReagentPrimingCheck(void)
 *  \brief  function to configure the GPIOs for detecting the Dilunet,
 *          Lyse and Rinse Priming
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void IGGpioConfig_ReagentPrimingCheck(void);
#endif /* INIT_GPIO_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

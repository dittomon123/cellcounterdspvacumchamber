/******************************************************************************/
/*! \file InitSDRAM.h
 *
 *  \brief Header file for InitSDRAM.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Aug-18-2015
 *   $Author:  $ GURUDUTT R, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_sdram_initialization_module
//! @{
//******************************************************************************


#ifndef INCLUDE_DATAACQUISTION_H_
#define INCLUDE_DATAACQUISTION_H_
#include "F28x_Project.h"


/******************************************************************************/
/*!
 *
 * \def LOCAL_MEM_BUFFER_SIZE
 * \brief Local RAM buffer size definition
 * \def SDRAM_CS0_MEM_START_ADDR
 * \brief SDRAM CS0 Start address location
 * \def SDRAM_CS0_MEM_END_ADDR
 * \brief SDRAM CS0 End address location
 * \def SDRAM_CS0_MEM_SIZE_64MB
 * \brief SDRAM CS0 memory size of 64MB
 * \def SDRAM_CS0_MEM_SIZE_5MB
 * \brief SDRAM CS0 memory size of 5MB
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   LOCAL_MEM_BUFFER_SIZE
 * \brief Local RAM buffer size definition
 */
/******************************************************************************/
#define LOCAL_MEM_BUFFER_SIZE         			256 // 32-Bit Word

/******************************************************************************/
/*!
 * \def   SDRAM_CS0_MEM_START_ADDR
 * \brief Start address location
 */
/******************************************************************************/
#define SDRAM_CS0_MEM_START_ADDR	 			0x80000000

/******************************************************************************/
/*!
 * \def   SDRAM_CS0_MEM_SIZE_64MB
 * \brief memory size of 64MB
 */
/******************************************************************************/
#define SDRAM_CS0_MEM_SIZE_64MB 		 		0x2000000

/******************************************************************************/
/*!
 * \def   SDRAM_CS0_MEM_SIZE_5MB
 * \brief memory size of 5MB
 */
/******************************************************************************/
#define	SDRAM_CS0_MEM_SIZE_5MB					(1024*1024*5)

/******************************************************************************/
/*!
 * \def   SDRAM_CS0_MEM_SIZE_5MB
 * \brief End address location
 */
/******************************************************************************/
#define SDRAM_CS0_MEM_END_ADDR	 				0x82000000



/******************************************************************************/
/*!
 *
 * \def TRANSFER_SIZE_256
 * \brief DMA transfer size
 * \def SDRAM_ADC_BUFFER_SIZE_WBC
 * \brief SDRAM buffer size for ADC data of WBC
 * \def SDRAM_ADC_BUFFER_SIZE_RBC
 * \brief SDRAM buffer size for ADC data of RBC
 * \def SDRAM_ADC_BUFFER_SIZE_PLT
 * \brief SDRAM buffer size for ADC data of PLT
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   TRANSFER_SIZE_256
 * \brief DMA transfer size
 */
/******************************************************************************/
#define TRANSFER_SIZE_256  32767


/******************************************************************************/
/*!
 * \def   SDRAM_ADC_BUFFER_SIZE_WBC
 * \brief size for ADC data of WBC
 */
/******************************************************************************/
#define SDRAM_ADC_BUFFER_SIZE_WBC			(1048576 * 5)  // 5 MB

/******************************************************************************/
/*!
 * \def   SDRAM_ADC_BUFFER_SIZE_RBC
 * \brief size for ADC data of RBC
 */
/******************************************************************************/
#define SDRAM_ADC_BUFFER_SIZE_RBC			(1048576 * 5)  // 5 MB

/******************************************************************************/
/*!
 * \def   SDRAM_ADC_BUFFER_SIZE_PLT
 * \brief size for ADC data of Platelets
 */
/******************************************************************************/
#define SDRAM_ADC_BUFFER_SIZE_PLT			(1048576 * 5)  // 5 MB



/******************************************************************************/
/*!
 *
 * \def DMA_TRANSFER_SIZE_WBC
 * \brief RAM buffer size for ADC data of WBC
 * \def DMA_TRANSFER_SIZE_RBC
 * \brief RAM buffer size for ADC data of RBC
 * \def DMA_TRANSFER_SIZE_PLT
 * \brief RAM buffer size for ADC data of PLT
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   DMA_TRANSFER_SIZE_WBC
 * \brief RAM buffer size for ADC data of WBC
 */
/******************************************************************************/
#define DMA_TRANSFER_SIZE_WBC	32768

/******************************************************************************/
/*!
 * \def   DMA_TRANSFER_SIZE_RBC
 * \brief RAM buffer size for ADC data of RBC
 */
/******************************************************************************/
#define DMA_TRANSFER_SIZE_RBC	32768

/******************************************************************************/
/*!
 * \def   DMA_TRANSFER_SIZE_PLT
 * \brief RAM buffer size for ADC data of Platelets
 */
/******************************************************************************/
#define DMA_TRANSFER_SIZE_PLT	32768



/******************************************************************************/
/*!
 *
 * \fn void ISInitializeSDRAM(void)
 * \brief To initialize SDRAM
 * \fn int	ISConfigureEMIF(void)
 * \brief To configure External memory interface
 * \fn ISConfigureSDRAMRegisters(void)
 * \brief To configure SDRAM registers
 * \fn int16 ISConfigEmif1CPU(Uint16 CpuConfig)
 * \brief To configure external memory interface of CPU1
 * \fn Uint16 ISGetEmif1Status(void)
 * \brief To get external memory interface status
 * \fn int16 ISEraseSdramMemoryDWORD(Uint32 StartAddr, Uint32 MemSize)
 * \brief To erase SDRAM memory
 * \fn int16 ISEraseSdramMemoryWORD(Uint32 StartAddr, Uint32 MemSize)
 * \brief To erase the SDRAM
 * \fn void ISSdramWriteWORD(Uint32 MemAddr, Uint16 Data)
 * \brief To write data to SDRAM
 * \fn void ISSdramWriteDWORD(Uint32 MemAddr, Uint32 Data)
 * \brief To write data to SDRAM
 * \fn Uint16 ISSdramReadWORD(Uint32 MemAddr)
 * \brief To read data from SDRAM
 * \fn Uint16 ISSdramReadDWORD(Uint32 MemAddr);
 * \brief To read data from SDRAM
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     void ISInitializeSDRAM(void)
 *  \brief  To initialize SDRAM
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void ISInitializeSDRAM(void);

/******************************************************************************/
/*!
 *  \fn     int ISConfigureEMIF(void)
 *  \brief  To configure External memory interface
 *  \param  void
 *  \return int
 */
/******************************************************************************/
int	ISConfigureEMIF(void);

/******************************************************************************/
/*!
 *  \fn     void ISConfigureSDRAMRegisters(void)
 *  \brief  To configure SDRAM registers
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void ISConfigureSDRAMRegisters(void);

/******************************************************************************/
/*!
 *  \fn     int16 ISConfigEmif1CPU(Uint16 CpuConfig)
 *  \brief  To configure external memory interface of CPU1
 *  \param  void
 *  \return Uint16
 */
/******************************************************************************/
int16 ISConfigEmif1CPU(Uint16 CpuConfig);

/******************************************************************************/
/*!
 *  \fn     Uint16 ISGetEmif1Status(void)
 *  \brief  To get external memory interface status
 *  \param  void
 *  \return Uint16
 */
/******************************************************************************/
Uint16 ISGetEmif1Status(void);

/******************************************************************************/
/*!
 *  \fn     int16 ISEraseSdramMemoryDWORD(Uint32 StartAddr, Uint32 MemSize)
 *  \brief  To erase SDRAM memory
 *  \param  Uint32
 *  \param  Uint32
 *  \return int16
 */
/******************************************************************************/
int16 ISEraseSdramMemoryDWORD(Uint32 StartAddr, Uint32 MemSize);

/******************************************************************************/
/*!
 *  \fn     int16 ISEraseSdramMemoryWORD(Uint32 StartAddr, Uint32 MemSize)
 *  \brief  To erase SDRAM
 *  \param  Uint32
 *  \param  Uint32
 *  \return int16
 */
/******************************************************************************/
int16 ISEraseSdramMemoryWORD(Uint32 StartAddr, Uint32 MemSize);

/******************************************************************************/
/*!
 *  \fn     int16 ISEraseSdramMemoryWORD(Uint32 StartAddr, Uint32 MemSize)
 *  \brief  To write word to the memory address
 *  \param  Uint32
 *  \param  Uint16
 *  \return void
 */
/******************************************************************************/
void ISSdramWriteWORD(Uint32 MemAddr, Uint16 Data);

/******************************************************************************/
/*!
 *  \fn     void ISSdramWriteDWORD(Uint32 MemAddr,Uint32 Data)
 *  \brief  To write word to the memory address
 *  \param  Uint32
 *  \param  Uint32
 *  \return void
 */
/******************************************************************************/
void ISSdramWriteDWORD(Uint32 MemAddr,Uint32 Data);

/******************************************************************************/
/*!
 *  \fn     Uint16 ISSdramReadWORD(Uint32 MemAddr)
 *  \brief  To read word from the memory address
 *  \param  Uint32
 *  \return Uint16
 */
/******************************************************************************/
Uint16 ISSdramReadWORD(Uint32 MemAddr);

/******************************************************************************/
/*!
 *  \fn     Uint16 ISSdramReadWORD(Uint32 MemAddr)
 *  \brief  To read word from the memory address
 *  \param  Uint32
 *  \return Uint32
 */
/******************************************************************************/
Uint32 ISSdramReadDWORD(Uint32 MemAddr);

/******************************************************************************/

/******************************************************************************/
/*!
 *
 * \fn extern void setup_emif1_pinmux_sdram_16bit(Uint16)
 * \brief To configure the external memory interface pin mux
 */
/******************************************************************************/
extern void setup_emif1_pinmux_sdram_16bit(Uint16);

#endif /* INCLUDE_DATAACQUISTION_H_ */

//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

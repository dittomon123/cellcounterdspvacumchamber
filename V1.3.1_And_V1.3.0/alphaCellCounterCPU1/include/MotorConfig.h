/******************************************************************************/
/*! \file MotorConfig.h
 *
 *  \brief Header file for PWM & MOTOR definitions.
 *
 *  \b Description:
 *      pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Mar-05-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/

//******************************************************************************
//! \addtogroup cpu1_motor_module
//! @{
//******************************************************************************

#ifndef MOTORCONFIG_H_
#define MOTORCONFIG_H_

#include "F2837xD_device.h"     // F2837xD Headerfile Include File
//#include "F2837xD_Examples.h"   // F2837xD Examples Include File
#include "InitGPIO.h"

//---------------------DEBUG PRINT CONTROL------------------------------------//
/******************************************************************************/
/*!
 * \def   MOTOR_TEST
 * \brief not used
 */
/******************************************************************************/
#define MOTOR_TEST								0

/******************************************************************************/
/*!
 * \def   SPI_DEBUG_PRINT_ENABLE
 * \brief To enable printf in SPI related functions
 */
/******************************************************************************/
#define SPI_DEBUG_PRINT_ENABLE					1

/******************************************************************************/
/*!
 * \def   INIT_DEBUG_PRINT_ENABLE
 * \brief TBD
 */
/******************************************************************************/
#define INIT_DEBUG_PRINT_ENABLE					0

/******************************************************************************/
/*!
 * \def   MOTOR_DEBUG_PRINT_ENABLE
 * \brief TBD
 */
/******************************************************************************/
#define MOTOR_DEBUG_PRINT_ENABLE				0

/******************************************************************************/
/*!
 * \def   PWM_DEBUG_PRINT_ENABLE
 * \brief TBD
 */
/******************************************************************************/
#define PWM_DEBUG_PRINT_ENABLE					0

/******************************************************************************/
/*!
 * \def   MOTOR_PWM_DEBUG_PRINT_ENABLE
 * \brief TBD
 */
/******************************************************************************/
#define MOTOR_PWM_DEBUG_PRINT_ENABLE			0


/******************************************************************************/
/*!
 * \brief Preprocessor definitions
 * \def HOME_SENSORS_ENABLE
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   HOME_SENSORS_ENABLE
 * \brief To enable the home position sensors
 */
/******************************************************************************/
#define HOME_SENSORS_ENABLE			1             //1 for New HPS 0 For old HPS


//---------------------STEPPERMOTORCONTROL------------------------------------//
#ifdef _ALPHA_5_BOARD_

/******************************************************************************/
/*!
 * \def To determine the home position direction of the motors
 * \def MOTOR1_HOME_DIRECTION : 0 indicates Moving to left is home direction for \
 *                              X-Axis motor
 * \def MOTOR2_HOME_DIRECTION : 0 indicates Moving up is home direction for \
 *                              Diluent syringe
 * \def MOTOR3_HOME_DIRECTION : 0 indicates Moving up is home direction for \
 *                              Sample lyse syringe
 * \def MOTOR4_HOME_DIRECTION : 0 indicates Moving up is home direction for \
 *                              Waste syringe
 * \def MOTOR5_HOME_DIRECTION : 0 indicates Moving up is home direction for \
 *                              Y-Axis
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   MOTOR1_HOME_DIRECTION
 * \brief 0 indicates Moving to left is home direction for X-Axis motor
 *
 */
/******************************************************************************/
#define MOTOR1_HOME_DIRECTION       0//1        //UP    // DOWN Direction   // Moving towards Motor side    //X axis
//#define MOTOR1_HOME_DIRECTION       1                 //New Waste motor syringe(26_11_2018)
/******************************************************************************/
/*!
 * \def   MOTOR2_HOME_DIRECTION
 * \brief 0 indicates Moving up is home direction for Diluent syringe
 *
 */
/******************************************************************************/
#define MOTOR2_HOME_DIRECTION       0       //LEFT  // UP Direction     // Moving Away from Motor side      //Diluent

/******************************************************************************/
/*!
 * \def   MOTOR3_HOME_DIRECTION
 * \brief 0 indicates Moving up is home direction for sample syringe
 */
/******************************************************************************/
#define MOTOR3_HOME_DIRECTION       0       //UP    // UP Direction // Moving away from Motor side          //Sample

/******************************************************************************/
/*!
 * \def   MOTOR4_HOME_DIRECTION
 * \brief 0 indicates Moving up is home direction for waste syringe
 */
/******************************************************************************/
#define MOTOR4_HOME_DIRECTION       0//0       //UP    // UP Direction // Moving Away from Motor side       //Waste

/******************************************************************************/
/*!
 * \def   MOTOR5_HOME_DIRECTION
 * \brief 1 indicates Moving up is home direction for Y axis
 */
/******************************************************************************/
#define MOTOR5_HOME_DIRECTION       1       //UP

#else
#define MOTOR1_HOME_DIRECTION		0//1		//UP	// DOWN Direction	// Moving towards Motor side
#define MOTOR2_HOME_DIRECTION		1		//LEFT	// UP Direction		// Moving Away from Motor side
#define MOTOR3_HOME_DIRECTION		0		//UP	// UP Direction	// Moving away from Motor side
#define MOTOR4_HOME_DIRECTION		0		//UP	// UP Direction	// Moving Away from Motor side
#define MOTOR5_HOME_DIRECTION		0		//UP	// UP Direction	// Moving away from Motor side

#endif

/******************************************************************************/
/*!
 * \def for Motor Sensors
 * \def MOTOR1_SENSOR : not used
 * \def MOTOR2_SENSOR : not used
 * \def MOTOR3_SENSOR : not used
 * \def MOTOR4_SENSOR : not used
 * \def MOTOR5_SENSOR : not used
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   MOTOR1_SENSOR
 * \brief not used
 */
/******************************************************************************/
#define MOTOR1_SENSOR				1

/******************************************************************************/
/*!
 * \def   MOTOR2_SENSOR
 * \brief not used
 */
/******************************************************************************/
#define MOTOR2_SENSOR				2

/******************************************************************************/
/*!
 * \def   MOTOR3_SENSOR
 * \brief not used
 */
/******************************************************************************/
#define MOTOR3_SENSOR				3

/******************************************************************************/
/*!
 * \def   MOTOR4_SENSOR
 * \brief not used
 */
/******************************************************************************/
#define MOTOR4_SENSOR				4

/******************************************************************************/
/*!
 * \def   MOTOR5_SENSOR
 * \brief not used
 */
/******************************************************************************/
#define MOTOR5_SENSOR				5


/******************************************************************************/
/*!
 * \def Defines for selection of motor
 * \def MOTOR1_SELECT : To select Motor 1 - Waste
 * \def MOTOR2_SELECT : To select Motor 2 - Diluent
 * \def MOTOR3_SELECT : To select Motor 3 - Sample Lyse
 * \def MOTOR4_SELECT : To select Motor 4 - X-axis
 * \def MOTOR5_SELECT : To select Motor 5 - Y-Axis
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   MOTOR1_SELECT
 * \brief To select Motor 1 - Waste Syringe
 */
/******************************************************************************/
#define MOTOR1_SELECT				1

/******************************************************************************/
/*!
 * \def   MOTOR2_SELECT
 * \brief To select Motor 2 - Diluent syringe
 */
/******************************************************************************/
#define MOTOR2_SELECT				2

/******************************************************************************/
/*!
 * \def   MOTOR3_SELECT
 * \brief To select Motor 3 - sample syringe
 */
/******************************************************************************/
#define MOTOR3_SELECT				3

/******************************************************************************/
/*!
 * \def   MOTOR4_SELECT
 * \brief To select Motor 4 - X Axis
 */
/******************************************************************************/
#define MOTOR4_SELECT				4

/******************************************************************************/
/*!
 * \def   MOTOR5_SELECT
 * \brief To select Motor 5 - Y Axis
 */
/******************************************************************************/
#define MOTOR5_SELECT				5


/******************************************************************************/
/*!
 * \def Home state definition for each motor
 * \def SENSOR1_HOME_STATE : To identify home state for X-Axis
 * \def SENSOR2_HOME_STATE : To identify home state for Diluent
 * \def SENSOR3_HOME_STATE : To identify home state for Sample Lyse
 * \def SENSOR4_HOME_STATE : To identify home state for Waste
 * \def SENSOR5_HOME_STATE : To identify home state for Y-Axis
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   SENSOR1_HOME_STATE
 * \brief To identify home state for X-Axis
 */
/******************************************************************************/
#define SENSOR1_HOME_STATE			1//0 // for alpha home sensor detection level

/******************************************************************************/
/*!
 * \def   SENSOR2_HOME_STATE
 * \brief To identify home state for Diluent syringe
 */
/******************************************************************************/
#define SENSOR2_HOME_STATE			1//0

/******************************************************************************/
/*!
 * \def   SENSOR3_HOME_STATE
 * \brief To identify home state for sample syringe
 */
/******************************************************************************/
#define SENSOR3_HOME_STATE			1//0

/******************************************************************************/
/*!
 * \def   SENSOR4_HOME_STATE
 * \brief To identify home state for Waste syringe
 */
/******************************************************************************/
#define SENSOR4_HOME_STATE			1//0

/******************************************************************************/
/*!
 * \def   SENSOR5_HOME_STATE
 * \brief To identify home state for Y axis
 */
/******************************************************************************/
#define SENSOR5_HOME_STATE			1//0

/******************************************************************************/
/*!
 * \def Home position definition for each motor
 * \def SENSOR1_HOME_POSITION : not used
 * \def SENSOR2_HOME_POSITION : not used
 * \def SENSOR3_HOME_POSITION : not used
 * \def SENSOR4_HOME_POSITION : not used
 * \def SENSOR5_HOME_POSITION : not used
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   SENSOR1_HOME_POSITION
 * \brief not used
 */
/******************************************************************************/
#define SENSOR1_HOME_POSITION		0x0001

/******************************************************************************/
/*!
 * \def   SENSOR2_HOME_POSITION
 * \brief not used
 */
/******************************************************************************/
#define SENSOR2_HOME_POSITION		0x0002

/******************************************************************************/
/*!
 * \def   SENSOR3_HOME_POSITION
 * \brief not used
 */
/******************************************************************************/
#define SENSOR3_HOME_POSITION		0x0004

/******************************************************************************/
/*!
 * \def   SENSOR4_HOME_POSITION
 * \brief not used
 */
/******************************************************************************/
#define SENSOR4_HOME_POSITION		0x0008

/******************************************************************************/
/*!
 * \def   SENSOR5_HOME_POSITION
 * \brief not used
 */
/******************************************************************************/
#define SENSOR5_HOME_POSITION		0x0010

/******************************************************************************/
/*!
 * \def Pointer refering to the specific motor which to be controlled
 * \def Y_AXIS_MOTOR : points to the Y-Axis motor to be operated
 * \def X_AXIS_MOTOR : points to the X-Axis motor to be operated
 * \def SAMPLE_MOTOR : points to the Sample motor to be operated
 * \def WASTE_MOTOR : points to the Waste motor to be operated
 * \def DILUENT_MOTOR : points to the Diluent motor to be operated
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   Y_AXIS_MOTOR
 * \brief Index points to the Y-Axis motor to be operated
 */
/******************************************************************************/
#define Y_AXIS_MOTOR				0

/******************************************************************************/
/*!
 * \def   X_AXIS_MOTOR
 * \brief Index points to the X-Axis motor to be operated
 */
/******************************************************************************/
#define X_AXIS_MOTOR				1

/******************************************************************************/
/*!
 * \def   SAMPLE_MOTOR
 * \brief Index points to the Sampole syringe motor to be operated
 */
/******************************************************************************/
#define SAMPLE_MOTOR				2

/******************************************************************************/
/*!
 * \def   WASTE_MOTOR
 * \brief Index points to the waste syringe motor to be operated
 */
/******************************************************************************/
#define WASTE_MOTOR					3

/******************************************************************************/
/*!
 * \def   DILUENT_MOTOR
 * \brief Index points to the Diluent syringe motor to be operated
 */
/******************************************************************************/
#define DILUENT_MOTOR				4


/******************************************************************************/
/*!
 * \def HGB LED frequency and duty cycle control
 * \def HGB_PWM_FREQUENCY : HGB led controlling frequency
 * \def HGB_PWM_PERIOD_FACTOR : Duty cycle to be operated
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   HGB_PWM_FREQUENCY
 * \brief HGB led controlling frequency
 */
/******************************************************************************/
#define	HGB_PWM_FREQUENCY							1000	//frequency Hz

/******************************************************************************/
/*!
 * \def   HGB_PWM_PERIOD_FACTOR
 * \brief Duty cycle to be operated for HGB LED
 */
/******************************************************************************/
#define HGB_PWM_PERIOD_FACTOR						625000

/******************************************************************************/
/*!
 * \def   VALVE_OPERATION_STEP_TIME :
 * \brief not used
 */
/******************************************************************************/
#define	VALVE_OPERATION_STEP_TIME		2000

/******************************************************************************/
/*!
 * \def Home sensor interrupt enable and disable
 * \def SENSOR_INTERRUPT_ENABLE : Home sensor interrupt enable
 * \def SENSOR_INTERRUPT_DISABLE : Home sensor interrupt disable
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   SENSOR_INTERRUPT_ENABLE
 * \brief Home sensor interrupt enable
 */
/******************************************************************************/
#define SENSOR_INTERRUPT_ENABLE			1

/******************************************************************************/
/*!
 * \def   SENSOR_INTERRUPT_DISABLE
 * \brief Home sensor interrupt disable
 */
/******************************************************************************/
#define SENSOR_INTERRUPT_DISABLE		0

/******************************************************************************/
/*!
 * \def To define Waste bin detection
 * \def WASTE_DETECTED : Indicates waste bin is detected
 * \def WASTE_SESNE_INTERVAL : The time interval to monitor the waste bin
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   WASTE_DETECTED
 * \brief Indicates waste bin is detected
 */
/******************************************************************************/
#define WASTE_DETECTED                  0

/******************************************************************************/
/*!
 * \def   WASTE_SESNE_INTERVAL
 * \brief The time interval to monitor the waste bin
 */
/******************************************************************************/
#define WASTE_SESNE_INTERVAL            50

//---------------------STEPPERMOTORCONTROL------------------------------------//
/******************************************************************************/
/*!
 * \def Stepper motor control defines
 * \def RUN : Indicates Motor is in running state
 * \def IDLE : Indicates Motor is in idle state
 * \def START : Indicates Motor to start
 * \def STOP : Indicates Motor to stop
 * \def COMPLETED : Indicates Motor has completed the required no. of rotation
 * \def MOTOR_UP_DIRECTION : Indicated motor is moving in up direction
 * \def MOTOR_DOWN_DIRECTION : Indicated motor is moving in down direction
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def    RUN
 * \brief  Indicates Motor is in running state
 */
/******************************************************************************/
#define RUN					2

/******************************************************************************/
/*!
 * \def   IDLE
 * \brief Indicates Motor is in idle state
 */
/******************************************************************************/
#define IDLE				3
//#define	TIMER1_INTR_FREQ	200 //in uSec
/******************************************************************************/
/*!
 * \def   START
 * \brief Indicates Motor to start
 */
/******************************************************************************/
#define START				1

/******************************************************************************/
/*!
 * \def   STOP
 * \brief Indicates Motor to stop
 */
/******************************************************************************/
#define STOP				0

/******************************************************************************/
/*!
 * \def   COMPLETED
 * \brief Indicates Motor has completed the required no. of rotations
 */
/******************************************************************************/
#define COMPLETED			4

/******************************************************************************/
/*!
 * \def   MOTOR_UP_DIRECTION
 * \brief Indicated motor is moving in up direction
 */
/******************************************************************************/
#define MOTOR_UP_DIRECTION			0

/******************************************************************************/
/*!
 * \def   MOTOR_DOWN_DIRECTION
 * \brief Indicated motor is moving in down direction
 */
/******************************************************************************/
#define MOTOR_DOWN_DIRECTION		1

/******************************************************************************/
/*!
 * \def Stepper motor step controls
 * \def FULL_STEP Indicates one micro step for full rotation
 * \def TWO_MICRO_STEP Indicates 2 micro step for full rotation
 * \def FOUR_MICRO_STEP Indicates 4 micro step for full rotation
 * \def EIGHT_MICRO_STEP Indicates 8 micro step for full rotation
 * \def SIXTEEN_MICRO_STEP Indicates 16 micro step for full rotation
 * \def THIRTYTWO_MICRO_STEP Indicates 32 micro step for full rotation
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   FULL_STEP
 * \brief Indicates one micro step for full rotation
 */
/******************************************************************************/
#define FULL_STEP						1

/******************************************************************************/
/*!
 * \def   TWO_MICRO_STEP
 * \brief Indicates 2 micro step for full rotation
 */
/******************************************************************************/
#define	TWO_MICRO_STEP					2

/******************************************************************************/
/*!
 * \def   FOUR_MICRO_STEP
 * \brief Indicates 4 micro step for full rotation
 */
/******************************************************************************/
#define	FOUR_MICRO_STEP					4

/******************************************************************************/
/*!
 * \def   EIGHT_MICRO_STEP
 * \brief Indicates 8 micro step for full rotation
 */
/******************************************************************************/
#define	EIGHT_MICRO_STEP				8

/******************************************************************************/
/*!
 * \def   SIXTEEN_MICRO_STEP
 * \brief Indicates 16 micro step for full rotation
 */
/******************************************************************************/
#define	SIXTEEN_MICRO_STEP				16

/******************************************************************************/
/*!
 * \def   THIRTYTWO_MICRO_STEP
 * \brief Indicates 32 micro step for full rotation
 */
/******************************************************************************/
#define THIRTYTWO_MICRO_STEP			32

//---------------------MOTOR STEP MODE configuration--------------------------//
/******************************************************************************/
/*!
 * \def Stepper motor mode select for each specified step
 * \def FULLL_STEP_MODE0 : Mode 0 select for full micro step
 * \def FULLL_STEP_MODE1 : Mode 1 select for full step
 * \def FULLL_STEP_MODE2 : Mode 2 select for full step
 *
 * \def TWO_MICRO_STEP_MODE0 : Mode 0 select for 2 micro step
 * \def TWO_MICRO_STEP_MODE1 : Mode 1 select for 2 micro step
 * \def TWO_MICRO_STEP_MODE2 : Mode 2 select for 2 micro step
 *
 * \def FOUR_MICRO_STEP_MODE0 : Mode 0 select for 4 micro step
 * \def FOUR_MICRO_STEP_MODE1 : Mode 1 select for 4 micro step
 * \def FOUR_MICRO_STEP_MODE2 : Mode 2 select for 4 micro step
 *
 * \def EIGHT_MICRO_STEP_MODE0 : Mode 0 select for 8 micro step
 * \def EIGHT_MICRO_STEP_MODE1 : Mode 1 select for 8 micro step
 * \def EIGHT_MICRO_STEP_MODE2 : Mode 2 select for 8 micro step
 *
 * \def SIXTEEN_MICRO_STEP_MODE0 : Mode 0 select for 16 micro step
 * \def SIXTEEN_MICRO_STEP_MODE1 : Mode 1 select for 16 micro step
 * \def SIXTEEN_MICRO_STEP_MODE2 : Mode 2 select for 16 micro step
 *
 * \def THIRTYTWO_MICRO_STEP_MODE0 : Mode 0 select for 32 micro step
 * \def THIRTYTWO_MICRO_STEP_MODE1 : Mode 1 select for 32 micro step
 * \def THIRTYTWO_MICRO_STEP_MODE2 : Mode 2 select for 32 micro step
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   FULLL_STEP_MODE0
 * \brief Mode 0 select for full micro step
 */
/******************************************************************************/
#define FULLL_STEP_MODE0							0

/******************************************************************************/
/*!
 * \def   FULLL_STEP_MODE1
 * \brief Mode 1 select for full micro step
 */
/******************************************************************************/
#define FULLL_STEP_MODE1							0

/******************************************************************************/
/*!
 * \def   FULLL_STEP_MODE2
 * \brief Mode 2 select for full micro step
 */
/******************************************************************************/
#define FULLL_STEP_MODE2							0

/******************************************************************************/
/*!
 * \def   TWO_MICRO_STEP_MODE0
 * \brief Mode 0 select for 2 micro step
 */
/******************************************************************************/
#define TWO_MICRO_STEP_MODE0						1

/******************************************************************************/
/*!
 * \def   TWO_MICRO_STEP_MODE1
 * \brief Mode 1 select for 2 micro step
 */
/******************************************************************************/
#define TWO_MICRO_STEP_MODE1						0

/******************************************************************************/
/*!
 * \def   TWO_MICRO_STEP_MODE2
 * \brief Mode 2 select for 2 micro step
 */
/******************************************************************************/
#define TWO_MICRO_STEP_MODE2						0

/******************************************************************************/
/*!
 * \def   FOUR_MICRO_STEP_MODE0
 * \brief Mode 0 select for 4 micro step
 */
/******************************************************************************/
#define FOUR_MICRO_STEP_MODE0						0

/******************************************************************************/
/*!
 * \def   FOUR_MICRO_STEP_MODE1
 * \brief Mode 1 select for 4 micro step
 */
/******************************************************************************/
#define FOUR_MICRO_STEP_MODE1						1

/******************************************************************************/
/*!
 * \def   FOUR_MICRO_STEP_MODE2
 * \brief Mode 2 select for 4 micro step
 */
/******************************************************************************/
#define FOUR_MICRO_STEP_MODE2						0

/******************************************************************************/
/*!
 * \def   EIGHT_MICRO_STEP_MODE0
 * \brief Mode 0 select for 8 micro step
 */
/******************************************************************************/
#define EIGHT_MICRO_STEP_MODE0						1

/******************************************************************************/
/*!
 * \def   EIGHT_MICRO_STEP_MODE1
 * \brief Mode 1 select for 8 micro step
 */
/******************************************************************************/
#define EIGHT_MICRO_STEP_MODE1						1

/******************************************************************************/
/*!
 * \def   EIGHT_MICRO_STEP_MODE2
 * \brief Mode 2 select for 8 micro step
 */
/******************************************************************************/
#define EIGHT_MICRO_STEP_MODE2						0

/******************************************************************************/
/*!
 * \def   SIXTEEN_MICRO_STEP_MODE0
 * \brief Mode 0 select for 16 micro step
 */
/******************************************************************************/
#define SIXTEEN_MICRO_STEP_MODE0					0

/******************************************************************************/
/*!
 * \def   SIXTEEN_MICRO_STEP_MODE1
 * \brief Mode 1 select for 16 micro step
 */
/******************************************************************************/
#define SIXTEEN_MICRO_STEP_MODE1					0

/******************************************************************************/
/*!
 * \def   SIXTEEN_MICRO_STEP_MODE2
 * \brief Mode 2 select for 16 micro step
 */
/******************************************************************************/
#define SIXTEEN_MICRO_STEP_MODE2					1

/******************************************************************************/
/*!
 * \def   THIRTYTWO_MICRO_STEP_MODE0
 * \brief Mode 0 select for 32 micro step
 */
/******************************************************************************/
#define THIRTYTWO_MICRO_STEP_MODE0					1

/******************************************************************************/
/*!
 * \def   THIRTYTWO_MICRO_STEP_MODE1
 * \brief Mode 1 select for 32 micro step
 */
/******************************************************************************/
#define THIRTYTWO_MICRO_STEP_MODE1					1

/******************************************************************************/
/*!
 * \def   THIRTYTWO_MICRO_STEP_MODE2
 * \brief Mode 2 select for 32 micro step
 */
/******************************************************************************/
#define THIRTYTWO_MICRO_STEP_MODE2					1

/******************************************************************************/
/*!
 * \def Stepper motor step configuration
 * \def MOTOR1_STEP_CONFIGURATION : X-Axis motor is configured to EIGHT_MICRO_STEP
 * \def MOTOR2_STEP_CONFIGURATION : Diluent motor is configured to FOUR_MICRO_STEP
 * \def MOTOR3_STEP_CONFIGURATION : Sample motor is configured to FOUR_MICRO_STEP
 * \def MOTOR4_STEP_CONFIGURATION : Waste motor is configured to FOUR_MICRO_STEP
 * \def MOTOR5_STEP_CONFIGURATION : Y-Axis motor is configured to FULL_STEP
 */
/******************************************************************************/
#ifdef _ALPHA_5_BOARD_
#ifdef _X_AXIS_HELICAL_

/******************************************************************************/
/*!
 * \def   MOTOR1_STEP_CONFIGURATION
 * \brief X-Axis motor is configured to EIGHT_MICRO_STEP
 */
/******************************************************************************/
#define MOTOR1_STEP_CONFIGURATION                   FOUR_MICRO_STEP  //HN_Change
#else
#define MOTOR1_STEP_CONFIGURATION                   TWO_MICRO_STEP
#endif

/******************************************************************************/
/*!
 * \def   MOTOR2_STEP_CONFIGURATION
 * \brief Diluent motor is configured to FOUR_MICRO_STEP
 */
/******************************************************************************/
#define MOTOR2_STEP_CONFIGURATION                   FOUR_MICRO_STEP

/******************************************************************************/
/*!
 * \def   MOTOR3_STEP_CONFIGURATION
 * \brief Sample motor is configured to FOUR_MICRO_STEP
 */
/******************************************************************************/
#define MOTOR3_STEP_CONFIGURATION                   FOUR_MICRO_STEP

/******************************************************************************/
/*!
 * \def   MOTOR4_STEP_CONFIGURATION
 * \brief Waste motor is configured to FOUR_MICRO_STEP
 */
/******************************************************************************/
#define MOTOR4_STEP_CONFIGURATION                   SIXTEEN_MICRO_STEP //FOUR_MICRO_STEP HN

/******************************************************************************/
/*!
 * \def   MOTOR5_STEP_CONFIGURATION
 * \brief Y-Axis motor is configured to TWO_MICRO_STEP
 */
/******************************************************************************/
#define MOTOR5_STEP_CONFIGURATION                   EIGHT_MICRO_STEP //FOUR_MICRO_STEP//HN change TWO_MICRO_STEP//FULL_STEP

#else
#define MOTOR1_STEP_CONFIGURATION					FULL_STEP
#define MOTOR2_STEP_CONFIGURATION					TWO_MICRO_STEP
#define MOTOR3_STEP_CONFIGURATION					FOUR_MICRO_STEP
#define MOTOR4_STEP_CONFIGURATION					FOUR_MICRO_STEP
#define MOTOR5_STEP_CONFIGURATION					FOUR_MICRO_STEP
#endif

/******************************************************************************/
/*!
 * \def Stepper motor frequency configuration
 * \def MOTOR1_FREQUENCY_FACTOR1 : X-Axis motor is configured to 1.5KHz
 * \def MOTOR2_FREQUENCY_FACTOR1 : Diluent motor is configured to 0.6KHz
 * \def MOTOR3_FREQUENCY_FACTOR1 : Sample motor is configured to 1KHz
 * \def MOTOR4_FREQUENCY_FACTOR1 : Waste motor is configured to 1KHz
 * \def MOTOR5_FREQUENCY_FACTOR1 : Y-Axis motor is configured to 1KHz
 */
/******************************************************************************/
#ifdef _ALPHA_5_BOARD_
#ifdef _X_AXIS_HELICAL_

/******************************************************************************/
/*!
 * \def   MOTOR1_FREQUENCY_FACTOR1
 * \brief X-Axis motor is configured to 1.5KHz
 */
/******************************************************************************/
#define MOTOR1_FREQUENCY_FACTOR1        (MOTOR1_STEP_CONFIGURATION * 1)
#else
#define MOTOR1_FREQUENCY_FACTOR1        (MOTOR1_STEP_CONFIGURATION * 2.25)      //X-axis
#endif

/******************************************************************************/
/*!
 * \def   MOTOR2_FREQUENCY_FACTOR1
 * \brief Diluent motor is configured to 0.6KHz
 */
/******************************************************************************/
#define MOTOR2_FREQUENCY_FACTOR1        (MOTOR2_STEP_CONFIGURATION * 0.6)       //Diluent

/******************************************************************************/
/*!
 * \def   MOTOR3_FREQUENCY_FACTOR1
 * \brief Sample motor is configured to 1KHz
 */
/******************************************************************************/
#define MOTOR3_FREQUENCY_FACTOR1        (MOTOR3_STEP_CONFIGURATION * 1)         //Sample

/******************************************************************************/
/*!
 * \def   MOTOR4_FREQUENCY_FACTOR1
 * \brief Waste motor is configured to 1KHz
 */
/******************************************************************************/
#define MOTOR4_FREQUENCY_FACTOR1        (MOTOR4_STEP_CONFIGURATION * 1.5)         //Waste

/******************************************************************************/
/*!
 * \def   MOTOR5_FREQUENCY_FACTOR1
 * \brief Y-Axis motor is configured to 1KHz
 */
/******************************************************************************/
#define MOTOR5_FREQUENCY_FACTOR1        (MOTOR5_STEP_CONFIGURATION * 1)         //Y-Axis
#else
#define MOTOR1_FREQUENCY_FACTOR1        (MOTOR1_STEP_CONFIGURATION * 3.00)
#define MOTOR2_FREQUENCY_FACTOR1		(MOTOR2_STEP_CONFIGURATION * 1)
#define MOTOR3_FREQUENCY_FACTOR1		(MOTOR3_STEP_CONFIGURATION * 1)
#define MOTOR4_FREQUENCY_FACTOR1		(MOTOR4_STEP_CONFIGURATION * 1)
#define MOTOR5_FREQUENCY_FACTOR1		(MOTOR5_STEP_CONFIGURATION * 0.6)
#endif

/******************************************************************************/
/*!
 * \def Stepper motor pitch configuration
 * \def MOTOR1_FREQUENCY_FACTOR1 : X-Axis motor is configured to 0.0595 pitch
 * \def MOTOR2_FREQUENCY_FACTOR1 : Diluent motor is configured to 0.024384 pitch
 * \def MOTOR3_FREQUENCY_FACTOR1 : Sample motor is configured to 0.024384 pitch
 * \def MOTOR4_FREQUENCY_FACTOR1 : Waste motor is configured to 0.024384 pitch
 * \def MOTOR5_FREQUENCY_FACTOR1 : Y-Axis motor is configured to 0.18 pitch
 */
/******************************************************************************/
#ifdef _ALPHA_5_BOARD_
#ifdef _X_AXIS_HELICAL_

/******************************************************************************/
/*!
 * \def   MOTOR1_STEP_DISPLACEMENT
 * \brief X-Axis motor is configured to 0.0595 pitch
 */
/******************************************************************************/
#define MOTOR1_STEP_DISPLACEMENT            0.024384           //Full Rotation (360 deg) = 12mm Linear displacement    //0.for
#else
#define MOTOR1_STEP_DISPLACEMENT            0.015           //Full Rotation (360 deg) = 3mm Linear displacement         //X-Axis
#endif

/******************************************************************************/
/*!
 * \def   MOTOR2_STEP_DISPLACEMENT
 * \brief Diluent motor is configured to 0.024384 pitch
 */
/******************************************************************************/
#define MOTOR2_STEP_DISPLACEMENT            0.024384        //Full Rotation (360 deg) = 4.8768mm Linear displacement    //Diluent

/******************************************************************************/
/*!
 * \def   MOTOR3_STEP_DISPLACEMENT
 * \brief Sample motor is configured to 0.024384 pitch
 */
/******************************************************************************/
#define MOTOR3_STEP_DISPLACEMENT            0.024384        //Full Rotation (360 deg) = 4.8768mm Linear displacement    //Sample

/******************************************************************************/
/*!
 * \def   MOTOR4_STEP_DISPLACEMENT
 * \brief Waste motor is configured to 0.024384 pitch
 */
/******************************************************************************/
#define MOTOR4_STEP_DISPLACEMENT             0.0595       //Full Rotation (360 deg) = 4.8768mm Linear displacement    //Waste
#ifdef _RPT_YAXIS_
#define MOTOR5_STEP_DISPLACEMENT            0.162338       //Full Rotation (360 deg) = 32.46mm Linear displacement        //Y-Axis
#else

/******************************************************************************/
/*!
 * \def   MOTOR5_STEP_DISPLACEMENT
 * \brief Y-Axis motor is configured to 0.18 pitch
 */
/******************************************************************************/
#define MOTOR5_STEP_DISPLACEMENT            0.18           //Full Rotation (360 deg) = 36mm Linear displacement        //Y-Axis
#endif
#else
#define MOTOR1_STEP_DISPLACEMENT			0.015			//Full Rotation (360 deg) = 36mm Linear displacement
#define MOTOR2_STEP_DISPLACEMENT			0.18			//Full Rotation (360 deg) = 3mm Linear displacement
#define MOTOR3_STEP_DISPLACEMENT			0.024384		//Full Rotation (360 deg) = 4.8768mm Linear displacement
#define MOTOR4_STEP_DISPLACEMENT			0.024384		//Full Rotation (360 deg) = 4.8768mm Linear displacement
#define MOTOR5_STEP_DISPLACEMENT			0.024384		//Full Rotation (360 deg) = 4.8768mm Linear displacement
#endif

/******************************************************************************/
/*!
 * \def Stepper motor step size configuration
 * \def MOTOR1_STEP_SIZE : X-Axis motor step size configuration
 * \def MOTOR2_STEP_SIZE : Diluent motor step size configuration
 * \def MOTOR3_STEP_SIZE : Sample motor step size configuration
 * \def MOTOR4_STEP_SIZE : Waste motor step size configuration
 * \def MOTOR5_STEP_SIZE : Y-Axis motor step size configuration
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   MOTOR1_STEP_SIZE
 * \brief X-Axis motor step size configuration
 */
/******************************************************************************/
#define MOTOR1_STEP_SIZE				(MOTOR1_STEP_DISPLACEMENT*100/MOTOR1_STEP_CONFIGURATION)	//Full Rotation (360 deg) = 36mm Linear displacement

/******************************************************************************/
/*!
 * \def   MOTOR2_STEP_SIZE
 * \brief Diluent motor step size configuration
 */
/******************************************************************************/
#define MOTOR2_STEP_SIZE				(MOTOR2_STEP_DISPLACEMENT*100/MOTOR2_STEP_CONFIGURATION)		//Full Rotation (360 deg) = 3mm Linear displacement

/******************************************************************************/
/*!
 * \def   MOTOR3_STEP_SIZE
 * \brief Sample motor step size configuration
 */
/******************************************************************************/
#define MOTOR3_STEP_SIZE				(MOTOR3_STEP_DISPLACEMENT*100/MOTOR3_STEP_CONFIGURATION)	//Full Rotation (360 deg) = 4.8768mm Linear displacement

/******************************************************************************/
/*!
 * \def   MOTOR4_STEP_SIZE
 * \brief Waste motor step size configuration
 */
/******************************************************************************/
#define MOTOR4_STEP_SIZE				(MOTOR4_STEP_DISPLACEMENT*100/MOTOR4_STEP_CONFIGURATION)	//Full Rotation (360 deg) = 4.8768mm Linear displacement

/******************************************************************************/
/*!
 * \def   MOTOR5_STEP_SIZE
 * \brief Y-Axis motor step size configuration
 */
/******************************************************************************/
#define MOTOR5_STEP_SIZE				(MOTOR5_STEP_DISPLACEMENT*100/MOTOR5_STEP_CONFIGURATION)	//Full Rotation (360 deg) = 4.8768mm Linear displacement

/******************************************************************************/
/*!
 * \def Stepper motor minimum frequency setting
 * \def MIN_MOTOR1_FREQUENCY : X-Axis motor step size configuration
 * \def MIN_MOTOR2_FREQUENCY : Diluent motor step size configuration
 * \def MIN_MOTOR3_FREQUENCY : Sample motor step size configuration
 * \def MIN_MOTOR4_FREQUENCY : Waste motor step size configuration
 * \def MIN_MOTOR5_FREQUENCY : Y-Axis motor step size configuration
 */
/******************************************************************************/
//#define MIN_MOTOR_FREQUENCY				250.0		//in PPS
#ifdef _ALPHA_5_BOARD_

/******************************************************************************/
/*!
 * \def   MIN_MOTOR1_FREQUENCY
 * \brief X-Axis motor step size configuration
 */
/******************************************************************************/
#define MIN_MOTOR1_FREQUENCY                100     //in PPS                        //X-Axis

/******************************************************************************/
/*!
 * \def   MIN_MOTOR2_FREQUENCY
 * \brief Diluent motor step size configuration
 */
/******************************************************************************/
#define MIN_MOTOR2_FREQUENCY                100     //in PPS                        //Dilueunt

/******************************************************************************/
/*!
 * \def   MIN_MOTOR3_FREQUENCY
 * \brief Sample motor step size configuration
 */
/******************************************************************************/
#define MIN_MOTOR3_FREQUENCY                100     //in PPS                        //Sample

/******************************************************************************/
/*!
 * \def   MIN_MOTOR4_FREQUENCY
 * \brief Waste motor step size configuration
 */
/******************************************************************************/
#define MIN_MOTOR4_FREQUENCY                250     //250//in PPS SKM_CHANGE_MOTORS //Waste

/******************************************************************************/
/*!
 * \def   MIN_MOTOR5_FREQUENCY
 * \brief Y-Axis motor step size configuration
 */
/******************************************************************************/
#define MIN_MOTOR5_FREQUENCY                250     //in PPS                        //Y-Axis
#else
#define MIN_MOTOR1_FREQUENCY				250		//in PPS
#define MIN_MOTOR2_FREQUENCY				250		//in PPS
#define MIN_MOTOR3_FREQUENCY				100		//in PPS
#define MIN_MOTOR4_FREQUENCY				100		//250//in PPS SKM_CHANGE_MOTORS
#define MIN_MOTOR5_FREQUENCY				100		//in PPS
#endif

/******************************************************************************/
/*!
 * \def Stepper motor band factor
 * \def LOW_BAND_FACTOR : not used
 * \def MID_BAND_FACTOR : not used
 * \def HIGH_BAND_FACTOR : not used
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   LOW_BAND_FACTOR
 * \brief not used
 */
/******************************************************************************/
#define LOW_BAND_FACTOR		0.1

/******************************************************************************/
/*!
 * \def   MID_BAND_FACTOR
 * \brief not used
 */
/******************************************************************************/
#define MID_BAND_FACTOR		0.07

/******************************************************************************/
/*!
 * \def   HIGH_BAND_FACTOR
 * \brief not used
 */
/******************************************************************************/
#define HIGH_BAND_FACTOR	0.04

/******************************************************************************/
/*!
 * \def S curve enable for the operation of Stepper motor to strat and stop \
 * the motor smoothly (1 to enable and 0) to disable
 * \def MOTOR1_SCURVE_ENABLE : S-Curve configuration for X-Axis
 * \def MOTOR2_SCURVE_ENABLE : S-Curve configuration for Diluent
 * \def MOTOR3_SCURVE_ENABLE : S-Curve configuration for Sample
 * \def MOTOR4_SCURVE_ENABLE : S-Curve configuration for Waste
 * \def MOTOR5_SCURVE_ENABLE : S-Curve configuration for Y-Axis
 */
/******************************************************************************/
#ifdef _ALPHA_5_BOARD_

/******************************************************************************/
/*!
 * \def   MOTOR1_SCURVE_ENABLE
 * \brief S-Curve configuration for X-Axis
 */
/******************************************************************************/
#define MOTOR1_SCURVE_ENABLE        1   //X-axis

/******************************************************************************/
/*!
 * \def   MOTOR2_SCURVE_ENABLE
 * \brief S-Curve configuration for Diluent syringe
 */
/******************************************************************************/
#define MOTOR2_SCURVE_ENABLE        1   //Dileunt

/******************************************************************************/
/*!
 * \def   MOTOR3_SCURVE_ENABLE
 * \brief S-Curve configuration for sample syringe
 */
/******************************************************************************/
#define MOTOR3_SCURVE_ENABLE        0   //Sample

/******************************************************************************/
/*!
 * \def   MOTOR4_SCURVE_ENABLE
 * \brief S-Curve configuration for Waste syringe
 */
/******************************************************************************/
#define MOTOR4_SCURVE_ENABLE        1   //Waste

/******************************************************************************/
/*!
 * \def   MOTOR5_SCURVE_ENABLE
 * \brief S-Curve configuration for Y-Axis
 */
/******************************************************************************/
#define MOTOR5_SCURVE_ENABLE        1   //Y-Axis

#else
#define MOTOR1_SCURVE_ENABLE		1
#define MOTOR2_SCURVE_ENABLE		0
#define MOTOR3_SCURVE_ENABLE		0
#define MOTOR4_SCURVE_ENABLE		1
#define MOTOR5_SCURVE_ENABLE		1
#endif

/******************************************************************************/
/*!
 * \def Stepper motor PWM frequency setting
 * \def MOTOR_PWM_FREQUENCY : PWM frequency is set to 1KHz
 * \def MOTOR_PWM_PERIOD_FACTOR : PWM period factor is set to 625000
 * \def PWM_PERIOD : not used
 * \def MOTOR_PWM_PERIOD : To configure the PWM duty cycle
 * \def START_MOTOR_PWM_PERIOD : To configure the PWM duty cycle while starting \
 *                              the motor
 * \def STOP_MOTOR_PWM_PERIOD : To configure the PWM duty cycle while stopping \
 *                              the motor
 * \def DUTY_PERCENT_0 : To indicate the zero percent duty cycle
 * \def DUTY_PERCENT_50 : To indicate the 50 percent duty cycle
 * \def DUTY_PERCENT_100 : To indicate the 100 percent duty cycle
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   MOTOR_PWM_FREQUENCY
 * \brief PWM frequency is set to 1KHz
 */
/******************************************************************************/
#define MOTOR_PWM_FREQUENCY				1000	//in Hz

/******************************************************************************/
/*!
 * \def   MOTOR_PWM_PERIOD_FACTOR
 * \brief PWM period factor is set to 625000
 */
/******************************************************************************/
#define MOTOR_PWM_PERIOD_FACTOR			625000		//HSPCLKDIV = TB_DIV10;CLKDIV = TB_DIV8;

/******************************************************************************/
/*!
 * \def   PWM_PERIOD
 * \brief not used
 */
/******************************************************************************/
#define PWM_PERIOD						(MOTOR_PWM_PERIOD_FACTOR/MOTOR_PWM_FREQUENCY)

/******************************************************************************/
/*!
 * \def   MOTOR_PWM_PERIOD
 * \brief To configure the PWM duty cycle
 */
/******************************************************************************/
#define MOTOR_PWM_PERIOD				20000		//period for PWM

/******************************************************************************/
/*!
 * \def   START_MOTOR_PWM_PERIOD
 * \brief To configure the PWM duty cycle while starting the motor
 */
/******************************************************************************/
#define START_MOTOR_PWM_PERIOD			20000		//period for PWM

/******************************************************************************/
/*!
 * \def   STOP_MOTOR_PWM_PERIOD
 * \brief To configure the PWM duty cycle while stopping the motor
 */
/******************************************************************************/
#define STOP_MOTOR_PWM_PERIOD			20000		//period for PWM

/******************************************************************************/
/*!
 * \def   DUTY_PERCENT_0
 * \brief To indicate the zero percent duty cycle
 */
/******************************************************************************/
#define DUTY_PERCENT_0					0

/******************************************************************************/
/*!
 * \def   DUTY_PERCENT_50
 * \brief To indicate the fifty percent duty cycle
 */
/******************************************************************************/
#define DUTY_PERCENT_50					(MOTOR_PWM_PERIOD/2)

/******************************************************************************/
/*!
 * \def   DUTY_PERCENT_100
 * \brief To indicate the hundred percent duty cycle
 */
/******************************************************************************/
#define DUTY_PERCENT_100				(MOTOR_PWM_PERIOD)

/******************************************************************************/
/*!
 * \def PWM reference for controlling
 * \def PWM1A : Y-Axis motor PWM control
 * \def PWM2A : Sample motor PWM control
 * \def PWM5A : Waste motor PWM control
 * \def PWM6A : Diluent motor PWM control
 * \def PWM8A : X-Axis motor PWM control
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   PWM1A
 * \brief Y-Axis motor PWM control
 */
/******************************************************************************/
#define PWM1A							0

/******************************************************************************/
/*!
 * \def   PWM2A
 * \brief Sample motor PWM control
 */
/******************************************************************************/
#define PWM2A							1

/******************************************************************************/
/*!
 * \def   PWM5A
 * \brief Waste motor PWM control
 */
/******************************************************************************/
#define PWM5A							2

/******************************************************************************/
/*!
 * \def   PWM6A
 * \brief Diluent motor PWM control
 */
/******************************************************************************/
#define PWM6A							3

/******************************************************************************/
/*!
 * \def   PWM8A
 * \brief X-Axis motor PWM control
 */
/******************************************************************************/
#define PWM8A							4

/******************************************************************************/
/*!
 * \brief High Voltage reference for controlling - not used
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   HIGH_VOLTAGE_60V
 * \brief not used
 */
/******************************************************************************/
#define HIGH_VOLTAGE_60V					1

/******************************************************************************/
/*!
 * \def   HIGH_VOLTAGE_100V
 * \brief not used
 */
/******************************************************************************/
#define HIGH_VOLTAGE_100V					0

/******************************************************************************/
/*!
 * \def   HIGH_VOLTAGE_SELECTION
 * \brief not used
 */
/******************************************************************************/
#define HIGH_VOLTAGE_SELECTION              HIGH_VOLTAGE_60V

/******************************************************************************/
/*!
 * \def   HIGH_VOLTAGE_SELECTION_60V
 * \brief not used
 */
/******************************************************************************/
#define HIGH_VOLTAGE_SELECTION_60V          HIGH_VOLTAGE_60V

/******************************************************************************/
/*!
 * \def   HIGH_VOLTAGE_SELECTION_100V
 * \brief not used
 */
/******************************************************************************/
#define HIGH_VOLTAGE_SELECTION_100V         HIGH_VOLTAGE_100V
#ifndef HIGH_VOLTAGE_SELECTION
#define HIGH_VOLTAGE_SELECTION              HIGH_VOLTAGE_100V
#endif

/******************************************************************************/
/*!
 * \def ap voltage on and off controlling
 * \def ZAP_VOLTAGE_ON : To define Zap voltage is on (Active low)
 * \def ZAP_VOLTAGE_OFF : To define Zap voltage is off
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   ZAP_VOLTAGE_ON
 * \brief To define Zap voltage is on
 */
/******************************************************************************/
#define ZAP_VOLTAGE_ON						0 // Active Low

/******************************************************************************/
/*!
 * \def   ZAP_VOLTAGE_OFF
 * \brief To define Zap voltage is off
 */
/******************************************************************************/
#define ZAP_VOLTAGE_OFF						1

/******************************************************************************/
/*!
 * \def High voltage on and off controlling
 * \def HIGH_VOLTAGE_SWITCH_OFF : To define high voltage off
 * \def HIGH_VOLTAGE_SWITCH_ON : To define high voltage on
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   HIGH_VOLTAGE_SWITCH_OFF
 * \brief To define high voltage off
 */
/******************************************************************************/
#define HIGH_VOLTAGE_SWITCH_OFF				0

/******************************************************************************/
/*!
 * \def   HIGH_VOLTAGE_SWITCH_ON
 * \brief To define high voltage on
 */
/******************************************************************************/
#define HIGH_VOLTAGE_SWITCH_ON				1

/******************************************************************************/
/*!
 * \def To read and write data
 * \def READ_DATA - not used
 * \def WRITE_DATA - not used
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   READ_DATA
 * \brief not used
 */
/******************************************************************************/
#define READ_DATA					0

/******************************************************************************/
/*!
 * \def   WRITE_DATA
 * \brief not used
 */
/******************************************************************************/
#define WRITE_DATA					1

/******************************************************************************/
/*!
 * \def To SPI Home position sensor - presently not used
 * \def SPI_HOME_POS_REG_ADDR_IODIR - not used
 * \def SPI_HOME_POS_REG_ADDR_IPOL - not used
 * \def SPI_HOME_POS_REG_ADDR_GPINTEN - not used
 * \def SPI_HOME_POS_REG_ADDR_DEFVAL - not used
 * \def SPI_HOME_POS_REG_ADDR_INTCON - not used
 * \def SPI_HOME_POS_REG_ADDR_IOCON - not used
 * \def SPI_HOME_POS_REG_ADDR_GPPU - not used
 * \def SPI_HOME_POS_REG_ADDR_INTF - not used
 * \def SPI_HOME_POS_REG_ADDR_INTCAP - not used
 * \def SPI_HOME_POS_REG_ADDR_GPIO - not used
 * \def SPI_HOME_POS_REG_ADDR_OLAT - not used
 * \def SPI_HOME_POS_DEV_OPCODE_READ_CMD - not used
 * \def SPI_HOME_POS_DEV_OPCODE_WRITE_CMD - not used
 * \def READ_CMD_IODIR - not used
 * \def WRITE_CMD_IODIR - not used
 * \def READ_CMD_IPOL - not used
 * \def WRITE_CMD_IPOL - not used
 * \def READ_CMD_GPINTEN - not used
 * \def WRITE_CMD_GPINTEN - not used
 * \def READ_CMD_DEFVAL - not used
 * \def WRITE_CMD_DEFVAL - not used
 * \def READ_CMD_INTCON - not used
 * \def WRITE_CMD_INTCON - not used
 * \def READ_CMD_IOCON - not used
 * \def WRITE_CMD_IOCON - not used
 * \def READ_CMD_GPPU - not used
 * \def WRITE_CMD_GPPU - not used
 * \def READ_CMD_INTF - not used
 * \def READ_CMD_INTCAP - not used
 * \def READ_CMD_GPIO - not used
 * \def READ_CMD_OLAT - not used
 */
/******************************************************************************/
/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_IODIR
 * \brief not used
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_IODIR								0x00

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_IPOL
 * \brief not used
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_IPOL								0x01

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_GPINTEN
 * \brief not used
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_GPINTEN							0x02

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_DEFVAL
 * \brief not used
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_DEFVAL							0x03

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_INTCON
 * \brief not used
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_INTCON							0x04

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_IOCON
 * \brief not used
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_IOCON								0x05

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_GPPU
 * \brief
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_GPPU								0x06

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_INTF
 * \brief
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_INTF								0x07

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_INTCAP
 * \brief
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_INTCAP							0x08

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_GPIO
 * \brief
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_GPIO								0x09

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_REG_ADDR_OLAT
 * \brief not used
 */
/******************************************************************************/
#define	SPI_HOME_POS_REG_ADDR_OLAT								0x0A

#if 1

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_DEV_OPCODE_READ_CMD
 * \brief not used
 */
/******************************************************************************/
#define	SPI_HOME_POS_DEV_OPCODE_READ_CMD						(0x41 << 8)

/******************************************************************************/
/*!
 * \def   SPI_HOME_POS_DEV_OPCODE_WRITE_CMD
 * \brief not used
 */
/******************************************************************************/
#define	SPI_HOME_POS_DEV_OPCODE_WRITE_CMD						(0x40 << 8)


/******************************************************************************/
/*!
 * \def   READ_CMD_IODIR
 * \brief not used
 */
/******************************************************************************/
#define READ_CMD_IODIR					((SPI_HOME_POS_REG_ADDR_IODIR) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))

/******************************************************************************/
/*!
 * \def   WRITE_CMD_IODIR
 * \brief not used
 */
/******************************************************************************/
#define WRITE_CMD_IODIR					((SPI_HOME_POS_REG_ADDR_IODIR) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

/******************************************************************************/
/*!
 * \def   READ_CMD_IPOL
 * \brief not used
 */
/******************************************************************************/

#define READ_CMD_IPOL					((SPI_HOME_POS_REG_ADDR_IPOL) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))

/******************************************************************************/
/*!
 * \def   WRITE_CMD_IPOL
 * \brief not used
 */
/******************************************************************************/
#define WRITE_CMD_IPOL					((SPI_HOME_POS_REG_ADDR_IPOL) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

/******************************************************************************/
/*!
 * \def   READ_CMD_GPINTEN
 * \brief not used
 */
/******************************************************************************/
#define READ_CMD_GPINTEN				((SPI_HOME_POS_REG_ADDR_GPINTEN) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))

/******************************************************************************/
/*!
 * \def   WRITE_CMD_GPINTEN
 * \brief not used
 */
/******************************************************************************/
#define WRITE_CMD_GPINTEN				((SPI_HOME_POS_REG_ADDR_GPINTEN) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

/******************************************************************************/
/*!
 * \def   READ_CMD_DEFVAL
 * \brief not used
 */
/******************************************************************************/
#define READ_CMD_DEFVAL					((SPI_HOME_POS_REG_ADDR_DEFVAL) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))

/******************************************************************************/
/*!
 * \def   WRITE_CMD_DEFVAL
 * \brief not used
 */
/******************************************************************************/
#define WRITE_CMD_DEFVAL				((SPI_HOME_POS_REG_ADDR_DEFVAL) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

/******************************************************************************/
/*!
 * \def   READ_CMD_INTCON
 * \brief not used
 */
/******************************************************************************/

#define READ_CMD_INTCON					((SPI_HOME_POS_REG_ADDR_INTCON) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))

/******************************************************************************/
/*!
 * \def   WRITE_CMD_INTCON
 * \brief not used
 */
/******************************************************************************/
#define WRITE_CMD_INTCON				((SPI_HOME_POS_REG_ADDR_INTCON) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

/******************************************************************************/
/*!
 * \def   READ_CMD_IOCON
 * \brief not used
 */
/******************************************************************************/
#define READ_CMD_IOCON					((SPI_HOME_POS_REG_ADDR_IOCON) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))

/******************************************************************************/
/*!
 * \def   WRITE_CMD_IOCON
 * \brief not used
 */
/******************************************************************************/
#define WRITE_CMD_IOCON					((SPI_HOME_POS_REG_ADDR_IOCON) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

/******************************************************************************/
/*!
 * \def   READ_CMD_GPPU
 * \brief not used
 */
/******************************************************************************/

#define READ_CMD_GPPU					((SPI_HOME_POS_REG_ADDR_GPPU) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))

/******************************************************************************/
/*!
 * \def   WRITE_CMD_GPPU
 * \brief not used
 */
/******************************************************************************/
#define WRITE_CMD_GPPU					((SPI_HOME_POS_REG_ADDR_GPPU) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

/******************************************************************************/
/*!
 * \def   READ_CMD_INTF
 * \brief not used
 */
/******************************************************************************/

#define READ_CMD_INTF					((SPI_HOME_POS_REG_ADDR_INTF) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
//#define WRITE_CMD_INTF				((SPI_HOME_POS_REG_ADDR_INTF) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))
/******************************************************************************/
/*!
 * \def   READ_CMD_INTCAP
 * \brief not used
 */
/******************************************************************************/
#define READ_CMD_INTCAP					((SPI_HOME_POS_REG_ADDR_INTCAP) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))

/******************************************************************************/
/*!
 * \def   READ_CMD_GPIO
 * \brief not used
 */
/******************************************************************************/
//#define WRITE_CMD_INTCAP				((SPI_HOME_POS_REG_ADDR_INTCAP) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_GPIO					((SPI_HOME_POS_REG_ADDR_GPIO) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))


//#define WRITE_CMD_GPIO				((SPI_HOME_POS_REG_ADDR_GPIO) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))
/******************************************************************************/
/*!
 * \def   READ_CMD_OLAT
 * \brief not used
 */
/******************************************************************************/
#define READ_CMD_OLAT					((SPI_HOME_POS_REG_ADDR_OLAT) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
//#define WRITE_CMD_OLAT				((SPI_HOME_POS_REG_ADDR_OLAT) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))
#endif


#if 0
#define READ_CMD_IODIR					(0x00FF & (SPI_HOME_POS_REG_ADDR_IODIR) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
#define WRITE_CMD_IODIR					(0x00FF & (SPI_HOME_POS_REG_ADDR_IODIR) |
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_IPOL					(0x00FF & (SPI_HOME_POS_REG_ADDR_IPOL) | \
                                        (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
#define WRITE_CMD_IPOL					(0x00FF & (SPI_HOME_POS_REG_ADDR_IPOL) | \
                                        (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_GPINTEN				(0x00FF & (SPI_HOME_POS_REG_ADDR_GPINTEN) | (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
#define WRITE_CMD_GPINTEN				(0x00FF & (SPI_HOME_POS_REG_ADDR_GPINTEN) | (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_DEFVAL					(0x00FF & (SPI_HOME_POS_REG_ADDR_DEFVAL) | (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
#define WRITE_CMD_DEFVAL				(0x00FF & (SPI_HOME_POS_REG_ADDR_DEFVAL) | (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_INTCON					(0x00FF & (SPI_HOME_POS_REG_ADDR_INTCON) | (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
#define WRITE_CMD_INTCON				(0x00FF & (SPI_HOME_POS_REG_ADDR_INTCON) | (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_IOCON					(0x00FF & (SPI_HOME_POS_REG_ADDR_IOCON) | (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
#define WRITE_CMD_IOCON					(0x00FF & (SPI_HOME_POS_REG_ADDR_IOCON) | (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_GPPU					(0x00FF & (SPI_HOME_POS_REG_ADDR_GPPU) | (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
#define WRITE_CMD_GPPU					(0x00FF & (SPI_HOME_POS_REG_ADDR_GPPU) | (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_INTF					(0x00FF & (SPI_HOME_POS_REG_ADDR_INTF) | (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
//#define WRITE_CMD_INTF					(0x00FF & (SPI_HOME_POS_REG_ADDR_INTF) | (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_INTCAP					(0x00FF & (SPI_HOME_POS_REG_ADDR_INTCAP) | (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
//#define WRITE_CMD_INTCAP				(0x00FF & (SPI_HOME_POS_REG_ADDR_INTCAP) | (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_GPIO					(0x00FF & (SPI_HOME_POS_REG_ADDR_GPIO) | (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
//#define WRITE_CMD_GPIO					(0x00FF & (SPI_HOME_POS_REG_ADDR_GPIO) | (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#define READ_CMD_OLAT					(0x00FF & (SPI_HOME_POS_REG_ADDR_OLAT) | (SPI_HOME_POS_DEV_OPCODE_READ_CMD))
//#define WRITE_CMD_OLAT					(0x00FF & (SPI_HOME_POS_REG_ADDR_OLAT) | (SPI_HOME_POS_DEV_OPCODE_WRITE_CMD))

#endif

/******************************************************************************/
/*!
 * SPI_HOME_POS_CONFIG_IODIR - not used
 * Bit7 :0 = Pin is configured as an input	GND
 * Bit6 :0 = Pin is configured as an input	GND
 * Bit5 :0 = Pin is configured as an input	GND
 * Bit4 :1 = Pin is configured as an input	DILUENT_MOTOR_POS_SENSE
 * Bit3 :1 = Pin is configured as an input	WASTE_MOTOR_POS_SENSE
 * Bit2 :1 = Pin is configured as an input	SAMPLE_LYSE_MOTOR_POS_SENSE
 * Bit1 :1 = Pin is configured as an input	Y_MOTOR_POS_SENSE
 * Bit0 :1 = Pin is configured as an input	X_MOTOR_POS_SENSE
 */
/******************************************************************************/
#define	SPI_HOME_POS_CONFIG_IODIR								(0x1F << 8)

/******************************************************************************/
/*!
 * SPI_HOME_POS_CONFIG_IPOL - not used
 * Bit7 :0 = GPIO register bit will reflect the same logic state of the input pin.
 * Bit6 :0 = GPIO register bit will reflect the same logic state of the input pin.
 * Bit5 :0 = GPIO register bit will reflect the same logic state of the input pin.
 * Bit4 :0 = GPIO register bit will reflect the same logic state of the input pin.
 * Bit3 :0 = GPIO register bit will reflect the same logic state of the input pin.
 * Bit2 :0 = GPIO register bit will reflect the same logic state of the input pin.
 * Bit1 :0 = GPIO register bit will reflect the same logic state of the input pin.
 * Bit0 :0 = GPIO register bit will reflect the same logic state of the input pin.
 */
/******************************************************************************/


#define	SPI_HOME_POS_CONFIG_IPOL								(0x00 << 8)

/******************************************************************************/
/*!
 * SPI_HOME_POS_CONFIG_GPINTEN -  not used
 * Bit7 :0
 * Bit6 :0
 * Bit5 :0 = Disable GPIO input pin for interrupt-on-change event
 * Bit4 :1
 * Bit3 :1
 * Bit2 :1
 * Bit1 :1
 * Bit0 :1 = Enable GPIO input pin for interrupt-on-change event
 */
/******************************************************************************/
#define	SPI_HOME_POS_CONFIG_GPINTEN								(0x1F << 8)

/******************************************************************************/
/*!
 * SPI_HOME_POS_CONFIG_DEFVAL - not used
 * If the associated pin level is the opposite from the register bit,
 * an interrupt occurs
 * Bit7 :0
 * Bit6 :0
 * Bit5 :0
 * Bit4 :1
 * Bit3 :1
 * Bit2 :1
 * Bit1 :1
 * Bit0 :1 = These bits set the compare value for pins configured for
 * interrupt-on-change from defaults <7:0>. Refer to INTCON
 */
/******************************************************************************/
#define	SPI_HOME_POS_CONFIG_DEFVAL								(0x1F << 8)

/******************************************************************************/
/*!
 * SPI_HOME_POS_CONFIG_INTCON -  not used
 * Bit7 :0
 * Bit6 :0
 * Bit5 :0
 * Bit4 :1
 * Bit3 :1
 * Bit2 :1
 * Bit1 :1
 * Bit0 :1 = Controls how the associated pin value is compared for
 * interrupt-on-change
 */
/******************************************************************************/
#define	SPI_HOME_POS_CONFIG_INTCON								(0x1F << 8)

/******************************************************************************/
/*!
 * SPI_HOME_POS_CONFIG_IOCON -  not used
 * Bit7 :0 = Unimplemented
 * Bit6 :0 = Unimplemented
 * Bit5 :1 = Sequential operation disabled, address pointer does not increment
 * Bit4 :0 = Slew rate enabled.
 * Bit3 :0 = Disables the MCP23S08 address pins.
 * Bit2 :0 = Active driver output (INTPOL bit sets the polarity)
 * Bit1 :0 = Active-low
 * Bit0 :0 = Unimplemented
 */
/******************************************************************************/
#define	SPI_HOME_POS_CONFIG_IOCON								(0x20 << 8)

/******************************************************************************/
/*!
 * SPI_HOME_POS_CONFIG_GPPU - not used
 * Bit7 :0
 * Bit6 :0
 * Bit5 :0
 * Bit4 :0
 * Bit3 :0
 * Bit2 :0
 * Bit1 :0
 * Bit0 :0 = Pull-up disabled
 */
/******************************************************************************/
#define	SPI_HOME_POS_CONFIG_GPPU								(0x00 << 8)


//#define	SPI_HOME_POS_CONFIG_INTF								0x0000

//#define	SPI_HOME_POS_CONFIG_INTCAP								0x0000

//#define	SPI_HOME_POS_CONFIG_GPIO								0x0000

//#define	SPI_HOME_POS_CONFIG_OLAT								0x0000


#endif /* GLOBAL_VARIABLES_H_ */
//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

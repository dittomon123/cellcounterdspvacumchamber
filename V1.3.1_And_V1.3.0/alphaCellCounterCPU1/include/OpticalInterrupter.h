/******************************************************************************/
/*! \file OpticalInterrupter.h
 *
 *  \brief Header file for OpticalInterrupter.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ May-12-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_optical_sensor_module
//! @{
//******************************************************************************
#ifndef POC2V2CELLCOUNTERCPU1_INCLUDE_OPTICALINTERRUPTER_H_
#define POC2V2CELLCOUNTERCPU1_INCLUDE_OPTICALINTERRUPTER_H_

#include "F2837xD_device.h"     // Headerfile Include File
#include "F2837xD_Examples.h"   // Examples Include File


/******************************************************************************/
/*!
 * \fn void OIInitialize_EXTINT(void) - not used
 * \fn void OIInit_GPIO_EXTINT(void);
 * \brief GPIO Configuration of Volumetric interrupt
 * \fn interrupt void xint1_isr(void)
 * \brief RBC start Volumetric ISR used in CPU2
 * \fn interrupt void xint2_isr(void)
 * \brief RBC stop Volumetric ISR used in CPU2
 * \fn interrupt void xint3_isr(void)
 * \brief WBC start Volumetric ISR used in CPU2
 * \fn interrupt void xint4_isr(void)
 * \brief WBC stop Volumetric ISR used in CPU2
 * \fn interrupt void xint5_isr(void) - not used
 * \fn unsigned int OIHomeSensor1State(unsigned int ReadWrite);
 * \brief To read home position sensor 1 status
 * \fn unsigned int OIHomeSensor2State(unsigned int ReadWrite);
 * \brief To read home position sensor 2 status
 * \fn unsigned int OIHomeSensor3State(unsigned int ReadWrite);
 * \brief To read home position sensor 3 status
 * \fn unsigned int OIHomeSensor4State(unsigned int ReadWrite);
 * \brief To read home position sensor 4 status
 * \fn unsigned int OIHomeSensor5State(unsigned int ReadWrite);
 * \brief To read home position sensor 5 status
 * \fn void Motor1SensorISR() - not used
 * \fn void Motor2SensorISR() - not used
 * \fn void Motor3SensorISR() - not used
 * \fn void Motor4SensorISR() - not used
 * \fn void Motor5SensorISR() - not used
 * \fn int16 PWMMotor1SensorPoll(void)
 * \brief To sense the motor rotation status
 * \fn int16 PWMMotor2SensorPoll(void)
 * \brief To sense the motor rotation status
 * \fn int16 PWMMotor3SensorPoll(void)
 * \brief To sense the motor rotation status
 * \fn int16 PWMMotor4SensorPoll(void)
 * \brief To sense the motor rotation status
 * \fn int16 PWMMotor5SensorPoll(void)
 * \brief To sense the motor rotation status
 *
 * \fn void OIInitSpiHomeSensor(void) - not used
 * \fn void OIInitFifoSpiHomeSensor(void) - not used
 * \fn Uint32 OIWriteDataSpiHomeSensorReg(Uint16 writeCmdData,
 *                      Uint16 writeRegData) - not used
 * \fn Uint16 OIReadSpiHomeSensorReg(Uint16 writeData) - not used
 * \fn void OIConfigSpiHomeSensor(void) - not used
 * \brief Function prototypes:
 */
/******************************************************************************/
// Prototype statements for functions found within this file.

/******************************************************************************/
/*!
 *  \fn     void OIInitialize_EXTINT(void)
 *  \brief  not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void OIInitialize_EXTINT(void);

/******************************************************************************/
/*!
 *  \fn     void OIInit_GPIO_EXTINT(void)
 *  \brief  GPIO Configuration of Volumetric interrupt
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void OIInit_GPIO_EXTINT(void);

/******************************************************************************/
/*!
 *  \fn     interrupt void xint1_isr(void)
 *  \brief  RBC start Volumetric ISR used in CPU2
 *  \param  void
 *  \return void
 */
/******************************************************************************/
interrupt void xint1_isr(void);

/******************************************************************************/
/*!
 *  \fn     interrupt void xint2_isr(void)
 *  \brief  RBC stop Volumetric ISR used in CPU2
 *  \param  void
 *  \return void
 */
/******************************************************************************/
interrupt void xint2_isr(void);

/******************************************************************************/
/*!
 *  \fn     interrupt void xint3_isr(void)
 *  \brief  WBC start Volumetric ISR used in CPU2
 *  \param  void
 *  \return void
 */
/******************************************************************************/
interrupt void xint3_isr(void);

/******************************************************************************/
/*!
 *  \fn     interrupt void xint4_isr(void)
 *  \brief  WBC start Volumetric ISR used in CPU2
 *  \param  void
 *  \return void
 */
/******************************************************************************/
interrupt void xint4_isr(void);

/******************************************************************************/
/*!
 *  \fn     interrupt void xint5_isr(void)
 *  \brief  not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
interrupt void xint5_isr(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int OIHomeSensor1State(void)
 *  \brief  Reads Sensor 1 state
 *  \param  void
 *  \return unsigned int
 */
/******************************************************************************/
unsigned int OIHomeSensor1State(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int OIHomeSensor2State(void)
 *  \brief  Reads Sensor 2 state
 *  \param  void
 *  \return unsigned int
 */
/******************************************************************************/
unsigned int OIHomeSensor2State(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int OIHomeSensor3State(void)
 *  \brief  Reads Sensor 3 state
 *  \param  void
 *  \return unsigned int
 */
/******************************************************************************/
unsigned int OIHomeSensor3State(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int OIHomeSensor4State(void)
 *  \brief  Reads Sensor 4 state
 *  \param  void
 *  \return unsigned int
 */
/******************************************************************************/
unsigned int OIHomeSensor4State(void);

/******************************************************************************/
/*!
 *  \fn     unsigned int OIHomeSensor5State(void)
 *  \brief  Reads Sensor 5 state
 *  \param  void
 *  \return unsigned int
 */
/******************************************************************************/
unsigned int OIHomeSensor5State(void);

/******************************************************************************/
/*!
 *  \fn     void Motor1SensorISR(void)
 *  \brief  Presently not used
 *  \param
 *  \return
 */
/******************************************************************************/
void Motor1SensorISR(void);

/******************************************************************************/
/*!
 *  \fn     void Motor2SensorISR(void)
 *  \brief  Presently not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Motor2SensorISR(void);

/******************************************************************************/
/*!
 *  \fn     void Motor3SensorISR(void)
 *  \brief  Presently not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Motor3SensorISR(void);

/******************************************************************************/
/*!
 *  \fn     void Motor4SensorISR(void)
 *  \brief  Presently not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Motor4SensorISR(void);

/******************************************************************************/
/*!
 *  \fn     void Motor5SensorISR(void)
 *  \brief  Presently not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Motor5SensorISR(void);

/******************************************************************************/
/*!
 *  \fn     int16 PWMMotor1SensorPoll(void)
 *  \brief  Optical sensor 1 state
 *  \param  void
 *  \return int16
 */
/******************************************************************************/
int16 PWMMotor1SensorPoll(void);

/******************************************************************************/
/*!
 *  \fn     int16 PWMMotor2SensorPoll(void)
 *  \brief  Optical sensor 2 state
 *  \param  void
 *  \return int16
 */
/******************************************************************************/
int16 PWMMotor2SensorPoll(void);

/******************************************************************************/
/*!
 *  \fn     int16 PWMMotor3SensorPoll(void)
 *  \brief  Optical sensor 3 state
 *  \param  void
 *  \return int16
 */
/******************************************************************************/
int16 PWMMotor3SensorPoll(void);

/******************************************************************************/
/*!
 *  \fn     int16 PWMMotor4SensorPoll(void)
 *  \brief  Optical sensor 4 state
 *  \param  void
 *  \return int16
 */
/******************************************************************************/
int16 PWMMotor4SensorPoll(void);

/******************************************************************************/
/*!
 *  \fn     int16 PWMMotor5SensorPoll(void)
 *  \brief  Optical sensor 5 state
 *  \param  void
 *  \return int16
 */
/******************************************************************************/
int16 PWMMotor5SensorPoll(void);

/******************************************************************************/
/*!
 *  \fn     void OIInitSpiHomeSensor(void)
 *  \brief  not used - IO Expander is removed from the board
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void OIInitSpiHomeSensor(void);

/******************************************************************************/
/*!
 *  \fn     void OIInitFifoSpiHomeSensor(void)
 *  \brief  Presently not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void OIInitFifoSpiHomeSensor(void);

/******************************************************************************/
/*!
 *  \fn     Uint32 OIWriteDataSpiHomeSensorReg(Uint16 writeCmdData, Uint16 writeRegData)
 *  \brief  Presently not used
 *  \param  Uint16,Uint16
 *  \return Uint32
 */
/******************************************************************************/
Uint32 OIWriteDataSpiHomeSensorReg(Uint16 writeCmdData, Uint16 writeRegData);

/******************************************************************************/
/*!
 *  \fn     Uint16 OIReadSpiHomeSensorReg(Uint16 writeData)
 *  \brief  Presently not used
 *  \param  Uint16
 *  \return Uint16
 */
/******************************************************************************/
Uint16 OIReadSpiHomeSensorReg(Uint16 writeData);

/******************************************************************************/
/*!
 *  \fn     void OIConfigSpiHomeSensor(void)
 *  \brief  Presently not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void OIConfigSpiHomeSensor(void);

#endif /* OPTICAL_INTERRUPTER_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

/*
 * SpiComm.h
 *
 *  Created on: 09-Dec-2015
 *      Author:  R Gurudutt
 */

#ifndef POC2V2CELLCOUNTERCPU1_INCLUDE_SPICOMM_H_
#define POC2V2CELLCOUNTERCPU1_INCLUDE_SPICOMM_H_


#include "F28x_Project.h"
#include "CommInterface.h"
#include <stdbool.h>
#include "Structure.h"


/******************************************************************************/
/*!
 * \def To define the register configuration of SPI
 * \def TX_FIFO_LVL - Transmit FIFO level
 * \def BURST - Burst size to transmit data through SPI
 * \def TX_TRANSFER - Data to be transmitted in each burst
 * \def RX_FIFO_LVL - Recieve FIFO level
 * \def RX_BURST - Burst size of received data through SPI
 * \def RX_TRANSFER - Data to be received in each burst
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   TX_FIFO_LVL
 * \brief Transmit FIFO level
 */
/******************************************************************************/
#define TX_FIFO_LVL 			8					// FIFO Interrupt Level

/******************************************************************************/
/*!
 * \def   BURST
 * \brief Burst size to transmit data through SPI
 */
/******************************************************************************/
#define BURST 					(TX_FIFO_LVL-1)    	// burst size should be less than 8

/******************************************************************************/
/*!
 * \def   TX_TRANSFER
 * \brief Data to be transmitted in each burst
 */
/******************************************************************************/
#define TX_TRANSFER  			((MEM_BUFFER_SIZE/TX_FIFO_LVL)-1)		//63

/******************************************************************************/
/*!
 * \def   RX_FIFO_LVL
 * \brief Recieve FIFO level
 */
/******************************************************************************/
#define RX_FIFO_LVL 			8					// FIFO Interrupt Level

/******************************************************************************/
/*!
 * \def   RX_BURST
 * \brief Burst size of received data through SPI
 */
/******************************************************************************/
#define RX_BURST				(RX_FIFO_LVL -1)

/******************************************************************************/
/*!
 * \def   RX_TRANSFER
 * \brief Data to be received in each burst
 */
/******************************************************************************/
#define RX_TRANSFER				((MEM_BUFFER_SIZE/RX_FIFO_LVL)-1)	//63


/******************************************************************************/
/*!
 * \breif Function prototypes
 * \fn void SPIConfigCommPortAndDMA()
 * \brief Function to configure the SPI A port and DMA
 * \fn void SPIInitCommGpioReg(void)
 * \breif Function to Initialise the GPIO Registers
 * \fn void SPIConfigCommFifoReg(void)
 * \brief Function to configure the SPI FIFO Registers
 * \fn void SPIConfigCommReg(void)
 * \brief Function to configure the SPI A Registers
 * \fn void SPISetCommCtrlTxrIntReq(void)
 * \brief Function to Set the SPI Transmit INT GPIO
 * \fn void SPISetCommCtrlBusyInt(void);
 * \brief Function to set the SPI Busy INT GPIO
 * \fn void SPIClearCommCtrlBusyInt(void);
 * \brief Function to clear the SPI Busy INT GPIO
 * \fn void SPIConfigCommDmaRx(void);
 * \brief Function to Configure the DMA Rx Registers
 * \fn void SPIConfigCommDmaTx(void);
 * \brief Function to Configure the DMA Tx Registers
 * \fn void SPIConfigCommDmaReg(void);
 * \brief Function to Configure the DMA Registers
 * \fn void SPIReConfigCommDmaReg(void);
 * \brief Function to ReConfigureDMA
 * \fn void SPIResetCommDmaAddr(void);
 * \brief Function to Reset the DMA Address
 * \fn void SPIResetCh5DmaAdd(void);
 * \brief Function to reset Channel 5 DMA Address
 * \fn void SPIResetCh6DmaAdd(void);
 * \brief Function to reset the Channel 6 DMA Address
 * \fn bool_t SPIGetCommRxStatus(void);
 * \brief Function to get the SPI Rx Status
 * \fn bool_t SPIGetCommTxStatus(void);
 * \brief Function to get the SPI tx Status
 * \fn void SPIClearCommTxStatus(void);
 * \brief Function to clear the SPI tx Status
 * \fn void SPIClearCommRxStatus(void);
 * \brief Function to clear the SPI Rx Status
 * \fn void SPISetCommTxStatus(void);
 * \brief Function to set the SPI Tx Status
 * \fn void SPISetCommRxStatus(void);
 * \brief Function to set the SPI Rx Status
 * \fn void SPIStopDMACH5(void);
 * \brief Function to stop the DMA Channel 5
 * \fn void SPIStopDMACH6(void);
 * \brief Function to stop the DMA Channel 6
 * \fn int16 SPICommTest(void);
 * \brief Function to test the SPI Comm test
 * \fn void SPISetTxTestDataPacket(Uint16* DataBuf, Uint16 unCmd, Uint16 unLength);
 * \brief Function to Set the SPI Tx Data Packet for testing
 * \fn __interrupt void local_DMACH5_ISR(void);
 * \brief Channel 5 DMA Interrupt Service Routine
 * \fn __interrupt void local_DMACH6_ISR(void);
 * \brief Channel 6 DMA Interrupt Service Routine
 * \fn void SPIClearCommBuffer(Uint16* unOrigin, Uint16 unLength);
 * \brief Function to clear the SPI buffer
 * \fn void SPIActivateCommPort(void);
 * \brief Function to activate the SPI DMA Channels
 * \fn void SPIDeactivateCommPort(void);
 * \brief Function to deactivate the SPI DMA Channels
 * \fn bool_t SPICommRead(void);
 * \brief Function to Read the Data from DMA Rx
 * \fn bool_t SPIDmaWrite(uint16_t *data);
 * \brief Function to Write Data to DMA
 * \fn bool_t SPICommWrite(void);
 * \brief Function to write the data the SPI
 * \fn bool_t SPIDmaTxRx(void);
 * \brief Function to Read and Write Data from/to DMA
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     void SPIConfigCommPortAndDMA(void)
 *  \brief  This function initializes the SPI GPIO Registers and the DMA Registers
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIConfigCommPortAndDMA(void);

 /******************************************************************************/
 /*!
  *  \fn     void SPIInitCommGpioReg(void)
  *  \brief  This function initializes the SPIA engine
  *  \param  void
  *  \return void
  */
 /******************************************************************************/
void SPIInitCommGpioReg(void);

/******************************************************************************/
/*!
 *  \fn     void SPIConfigCommFifoReg(void)
 *  \brief  Configuration of SPI A FIFO registers
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIConfigCommFifoReg(void);

/******************************************************************************/
/*!
 *  \fn     void SPIConfigCommReg(void)
 *  \brief  Configuration of SPI A Communication egisters
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIConfigCommReg(void);

/******************************************************************************/
/*!
 *  \fn     void SPISetCommCtrlTxrIntReq(void)
 *  \brief  Set the SPI Communication Transmit Interrupt GPIO to HIGH
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPISetCommCtrlTxrIntReq(void);

/******************************************************************************/
/*!
 *  \fn     void SPIClearCommCtrlTxrIntReq(void)
 *  \brief  Clear the SPI Communication Transmit Interrupt GPIO to Low
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIClearCommCtrlTxrIntReq(void);

/******************************************************************************/
/*!
 *  \fn     void SPISetCommCtrlBusyInt(void)
 *  \brief  Set the SPI Communication BUSY Interrupt GPIO to HIGH
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPISetCommCtrlBusyInt(void);

/******************************************************************************/
/*!
 *  \fn     void SPIClearCommCtrlBusyInt(void)
 *  \brief  Clear the SPI Communication BUSY Interrupt GPIO to Low
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIClearCommCtrlBusyInt(void);

/******************************************************************************/
/*!
 *  \fn     void SPIConfigCommDmaRx(void)
 *  \brief  Configuration of SPI DMA Receive registers
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIConfigCommDmaRx(void);

/******************************************************************************/
/*!
 *  \fn     void SPIConfigCommDmaTx(void)
 *  \brief  Configuration of SPI DMA Transmission registers
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIConfigCommDmaTx(void);

/******************************************************************************/
/*!
 *  \fn     void SPIConfigCommDmaReg(void)
 *  \brief  This function configures Spi DMA register
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIConfigCommDmaReg(void);

/******************************************************************************/
/*!
 *  \fn     void SPIReConfigCommDmaReg(void)
 *  \brief  This function is called to reinitialise DMA
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIReConfigCommDmaReg(void);

/******************************************************************************/
/*!
 *  \fn     void SPIResetCommDmaAddr(void)
 *  \brief  This function configures Spi DMA address
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIResetCommDmaAddr(void);

/******************************************************************************/
/*!
 *  \fn     void SPIResetCh5DmaAdd(void)
 *  \brief  This function configures Spi DMA Channel5 address
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIResetCh5DmaAdd(void);

/******************************************************************************/
/*!
 *  \fn     void SPIResetCh6DmaAdd(void)
 *  \brief  This function configures Spi DMA Channel6 address
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIResetCh6DmaAdd(void);

/******************************************************************************/
/*!
 *  \fn     bool_t SPIGetCommRxStatus(void)
 *  \brief  This function returns the state of Reception
 *  \param  void
 *  \return bool_t
 */
/******************************************************************************/
bool_t SPIGetCommRxStatus(void);

/******************************************************************************/
/*!
 *  \fn     bool_t SPIGetCommTxStatus(void)
 *  \brief  This function returns the state of transmission
 *  \param  void
 *  \return bool_t
 */
/******************************************************************************/
bool_t SPIGetCommTxStatus(void);

/******************************************************************************/
/*!
 *  \fn     void SPIClearCommTxStatus(void)
 *  \brief  This function Clear the state of transmission
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIClearCommTxStatus(void);

/******************************************************************************/
/*!
 *  \fn     void SPIClearCommRxStatus(void)
 *  \brief  This function Clear the state of Reception
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIClearCommRxStatus(void);

/******************************************************************************/
/*!
 *  \fn     void SPISetCommTxStatus(void)
 *  \brief  This function sets the state of transmission when Channel 5 ISR
 *          is serviced
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPISetCommTxStatus(void);

/******************************************************************************/
/*!
 *  \fn     void SPISetCommRxStatus(void)
 *  \brief  This function sets the state of Reception when Channel 6 ISR
 *          is serviced
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPISetCommRxStatus(void);

/******************************************************************************/
/*!
 *  \fn     void SPIStopDMACH5(void)
 *  \brief  Stop the running of Channel 5 DMA
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIStopDMACH5(void);

/******************************************************************************/
/*!
 *  \fn     void SPIStopDMACH6(void)
 *  \brief  Stop the running of Channel 6 DMA
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIStopDMACH6(void);

/******************************************************************************/
/*!
 *  \fn     int16 SPICommTest(void)
 *  \brief  This function tests the SPI communication
 *  \param  void
 *  \return int16
 */
/******************************************************************************/
int16 SPICommTest(void);

/******************************************************************************/
/*!
 *  \fn     SPISetTxTestDataPacket(Uint16* DataBuf, Uint16 unCmd, Uint16 unLength)
 *  \brief  Set the Test Data Packet
 *  \param  Uint16*,Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void SPISetTxTestDataPacket(Uint16* DataBuf, Uint16 unCmd, Uint16 unLength);

/******************************************************************************/
/*!
 *  \fn     __interrupt void local_DMACH5_ISR(void)
 *  \brief  Interrupt routine function for DMA Channel5
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void local_DMACH5_ISR(void);

/******************************************************************************/
/*!
 *  \fn     __interrupt void local_DMACH6_ISR(void)
 *  \brief  Interrupt routine function for DMA Channel6
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void local_DMACH6_ISR(void);

/******************************************************************************/
/*!
 *  \fn     void SPIClearCommBuffer(Uint16* unOrigin, Uint16 unLength)
 *  \brief  This function Clear the SPI Buffer
 *  \param  Uint16*,Uint16
 *  \return void
 */
/******************************************************************************/
void SPIClearCommBuffer(Uint16* unOrigin, Uint16 unLength);

/******************************************************************************/
/*!
 *  \fn     void SPIActivateCommPort(void)
 *  \brief  Start DMA Channel 6 and Channel 5
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIActivateCommPort(void);

/******************************************************************************/
/*!
 *  \fn     void SPIDeactivateCommPort(void)
 *  \brief  Stop the running of DMA Channel 6 and Channel 5
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void SPIDeactivateCommPort(void);

/******************************************************************************/
/*!
 *  \fn     bool_t SPICommRead(void)
 *  \brief  This function reads the data received
 *  \param  void
 *  \return bool_t
 */
/******************************************************************************/
bool_t SPICommRead(void);

/******************************************************************************/
/*!
 *  \fn     bool_t SPIDmaWrite(uint16_t *data)
 *  \brief  This function is called to write the data to DMA
 *  \param  uint16_t *
 *  \return bool_t
 */
/******************************************************************************/
bool_t SPIDmaWrite(uint16_t *data);

/******************************************************************************/
/*!
 *  \fn     bool_t SPICommWrite(void)
 *  \brief  This function is used to send data to Sitara through SPI
 *  \param  void
 *  \return bool_t
 */
/******************************************************************************/
bool_t SPICommWrite(void);

/******************************************************************************/
/*!
 *  \fn     bool_t SPIDmaTxRx(void)
 *  \brief  This function to decode the received data and to start the recieve DMA
 *  \param  void
 *  \return bool_t
 */
/******************************************************************************/
bool_t SPIDmaTxRx(void);

/******************************************************************************/
/*!
 *  \fn     extern void CPFrameDataPacket(Uint16 usnMajorCmd, Uint16 usnSubCmd, \
 *                              bool_t bRemainingPackets)
 *  \brief  extern function declaration of the function
 */
/******************************************************************************/
extern void CPFrameDataPacket(Uint16 usnMajorCmd, Uint16 usnSubCmd, \
                                bool_t bRemainingPackets);


/******************************************************************************/
/*!
 *  \fn     extern void CIFormACKPacket(bool_t bTransmitNACK)
 *  \brief  extern function declaration of the function
 */
/******************************************************************************/
extern void CIFormACKPacket(bool_t bTransmitNACK);

#endif /* POC2V2CELLCOUNTERCPU1_INCLUDE_SPICOMM_H_ */

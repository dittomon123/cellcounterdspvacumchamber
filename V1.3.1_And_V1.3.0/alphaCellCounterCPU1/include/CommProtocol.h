/*
 * CommProtocol.h
 *
 *  Created on: 15-Dec-2015
 *      Author: R Gurudutt
 */

#ifndef POC2V2CELLCOUNTERCPU1_INCLUDE_COMMPROTOCOL_H_
#define POC2V2CELLCOUNTERCPU1_INCLUDE_COMMPROTOCOL_H_

/******************************************************************************/
/*! \file CommProtocol.h
 *
 *  \brief Implementation of class SPI Declaration.
 *
 *  \b Description:
 *      This file contains the complete SPI Class Declaration
 *
 *   $Version: $ 0.1
 *   $Date:    $ 2015-11-06
 *   $Author:  $ Gurudutt R and Sudipta Kumar Sahoo
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Diagnostics Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics Pvt Ltd.
 *  The copyright notice does not evidence any actual or intended publication.
 */
/******************************************************************************/
#include "rtwtypes.h"
#include "F28x_Project.h"
#include "commands.h"
#include "UartDebug.h"
#include "Defines.h"
#include "Structure.h"
#include "Enums.h"

//SAM_SPI_START
#define FRAME_HEADER_HIGH 					1
#define FRAME_HEADER_LOW					2
#define FRAME_RESERVED_BITS					3
#define FRAME_DATA_LENGTH					4
#define FRAME_MAJOR_COMMAND					5
#define FRAME_MINOR_COMMAND					6
#define FRAME_DATA							7
#define FRAME_REMAINING_PACKETS_FOR_TX		8
#define FRAME_PACKET_CRC					9
#define FRAME_FOOTER_HIGH					10
#define FRAME_FOOTER_LOW					11


/******************************************************************************/
/*!
 *  \fn     void CPDecodeReceivedDataPacket()
 *  \brief  Decoding of the packet received from SPI Master
 *  \param  uint16_t* type address of the framePacket
 *  \return void
 */
/******************************************************************************/
void CPDecodeReceivedDataPacket(uint16_t*);

/******************************************************************************/
/*!
 *  \fn     CPFrameDataPacket()
 *  \brief  To frame the data packet to be transmitted to SPI Master and to Sitara.
 *  \param  Major Command.
 *  \param  Sub Command.
 *  \param  No of Bytes in the dta field.
 */
/******************************************************************************/
void CPFrameDataPacket( Uint16 usnMajorCmd, Uint16 usnSubCmd, \
        bool_t bRemainingPackets);

/******************************************************************************/
/*!
 *  \fn     CPWriteDataToSPI()
 *  \brief  Writes the data to SPI File Descriptor
 *
 *  \return void
 */
/******************************************************************************/
bool_t CPWriteDataToSPI();

/******************************************************************************/
/*!
 *  \fn     void CPPAbort(char *errString)
 *  \brief  Returns the Error String and aborts the process
 *
 *  \return void
 */
/******************************************************************************/
void CPPAbort(char*);

/******************************************************************************/
/*!
 *  \fn     CPGetRawADCOrHeightData()
 *  \brief  To update the ST_SPI_PACKET_FRAME structure with the
 *              pulse height data
 *  \param1  E_CELL_TYPE eTypeofCell
 *  \param2  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param3  uint16_t usnSubCmd
 *  \return  uint16_t
 */
/******************************************************************************/
uint16_t CPGetRawADCOrHeightData(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnSubCmd, bool_t bRawADCDataReq);

/******************************************************************************/
/*!
 *  \fn     CPGetHeightDataForReTransmission()
 *  \brief  To get the Pulse Height data for retransmission
 *  \param1  E_CELL_TYPE eTypeofCell
 *  \param2  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param3  uint16_t usnSubCmd
 *  \return uint32_t
 */
/******************************************************************************/
uint16_t CPGetHeightDataForReTransmission(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnSubCmd);

/******************************************************************************/
/*!
 *  \fn     CPCalculatePacketCRC()
 *  \brief  calculates the CRC of the updated data and returns it.
 *
 *  \param  ST_SPI_PACKET_FRAME  stPtrParsedPacket receives the value
 *          of the parsedPacketFrame
 *  \return Returns the calculated CRC.
 */
/******************************************************************************/
uint16_t CPCalculatePacketCRC(ST_SPI_PACKET_FRAME stPacketforCRC);

/******************************************************************************/
/*!
 *  \fn     CPUpdateCRC()
 *  \brief  calculates the CRC of the Individual Packet field.
 *  \param  Address of the Value for which CRC to be calculated.
 *  \param  No of Bytes in the dta field.
 *  \param  Calculated CRC that has to be updated after each CRC Calculation.
 */
/******************************************************************************/
void CPUpdateCRC(uint16_t *nData, uint16_t nByteCnt, uint16_t* usnCalculatedCRC);

/******************************************************************************/
/*!
 *  \fn     CPSetStartOfPacket()
 *  \brief  The START_OF_PACKET is written to the Packet.
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return void
 */
/******************************************************************************/
void CPSetStartOfPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket);

/******************************************************************************/
/*!
 *  \fn     CPSetPacketDataLength()
 *  \brief  To write to the Packet data field of the SPI Packet Frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return void
 */
/******************************************************************************/
void CPSetPacketDataLength(ST_SPI_PACKET_FRAME *stPtrParsedPacket, \
        Uint16 dataLength);

/******************************************************************************/
/*!
 *  \fn     CPSetCommandsForPacket()
 *  \brief  Writes the  Commands to the Packet Frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \param  usnMajorCmd Major command
 *  \param  usnSubCmd Sub command
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
void CPSetCommandsForPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket, \
        Uint16 usnMajorCmd, Uint16 usnSubCmd);

/******************************************************************************/
/*!
 *  \fn     CPWritePHDataToPacket()
 *  \brief  Writes the Patient handling data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWritePHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket, \
        bool_t bNAKReceived);

/******************************************************************************/
/*!
 *  \fn     CPWriteSHDataToPacket()
 *  \brief  To write the Service handled packet to the SPI Packet
 *  \param  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param  bool_t bNAKReceived
 *  \return  uint16_t return the data length
 */
/******************************************************************************/
uint16_t CPWriteSHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket, \
        bool_t bNAKReceived);

/******************************************************************************/
/*!
 *  \fn     CPWriteFirmwareVerToPacket()
 *  \brief  To write the firmware version to the SPI Packet
 *  \param  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param  bool_t bNAKReceived
 *  \return  uint16_t return the data length
 */
/******************************************************************************/
uint16_t CPWriteFirmwareVerToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket, \
        bool_t bNAKReceived);

/******************************************************************************/
/*!
 *  \fn     CPSetTotalNoOfPackets()
 *  \brief  To set the total no of packets for transmission
 *  \param  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \return void
 */
/******************************************************************************/
void CPSetTotalNoOfPackets(ST_SPI_PACKET_FRAME *stPtrParsedPacket);

/******************************************************************************/
/*!
 *  \fn     CPSetPacketCRC()
 *  \brief to write to the CRC field of the SPI Packet Frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \param uint16_t usnCalculatedCRC CRC calculated
 *  \return void
 */
/******************************************************************************/
void CPSetPacketCRC(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnCalculatedCRC );

/******************************************************************************/
/*!
 *  \fn     CPSetEndOfPacket()
 *  \brief  The END_OF_PACKET is written to the SPI Packet Frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return void
 */
/******************************************************************************/
void CPSetEndOfPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket);

/******************************************************************************/
/*!
 *  \fn     CPSetPacketAttributesForTx()
 *  \brief  To set the message attributes for transmission
 *  \param1  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \param2  uint16_t usnMajorCmd
 *  \param3  uint16_t usnSubCmd
 *  \return  void
 */
/******************************************************************************/
void CPSetPacketAttributesForTx(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnMajorCmd,uint16_t usnSubCmd);

/******************************************************************************/
/*!
 *  \fn     CPGetCountData()
 *  \brief  To write the counts data to the SPI Packet
 *  \param  ST_SPI_PACKET_FRAME *stPtrParsedPacket
 *  \return  uint16_t return the data length
 */
/******************************************************************************/
uint16_t CPGetCountData(ST_SPI_PACKET_FRAME *stPtrParsedPacket);

/******************************************************************************/
/*!
 *  \fn     CPSetNextIndexForADCData()
 *  \brief  To set the Index for ADC data
 *  \param  E_CELL_TYPE eTypeofCell
 *  \param  uint32_t uslPreviousIndex
 *  \return  void
 */
/******************************************************************************/
void CPSetNextIndexForADCData(E_CELL_TYPE eTypeofCell, uint32_t uslNextIndex);

/******************************************************************************/
/*!
 *  \fn     CPGetNextIndexForADCData()
 *  \brief  To get the next Index for ADC data
 *  \param  E_CELL_TYPE eTypeofCell
 *  \return  uint32_t previous index based on the cell type
 */
/******************************************************************************/
uint32_t CPGetNextIndexForADCData(E_CELL_TYPE eTypeofCell);

/******************************************************************************/
/*!
 *  \fn     void CPReInitialiseOnNewMeasurement()
 *  \brief  To initialise the variable on every new measurement
 *  \param  void
 *  \return  void
 */
/******************************************************************************/
void CPReInitialiseOnNewMeasurement(void);

/******************************************************************************/
/*!
 *  \fn     CPReTransmistADCRawData()
 *  \brief  To retransmit ADC Raw data
 *  \param  void
 *  \return  uint32_t
 */
/******************************************************************************/
uint16_t CPReTransmistADCRawData(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        uint16_t usnSubCmd);

/*! Function to fill the data field for Calibration handling of the SPI Packet Frame*/
/******************************************************************************/
/*!
 *  \fn     CPWriteCHDataToPacket()
 *  \brief  Writes the data field for Calibration handling of the SPI Packet Frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteCHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket, \
        bool_t bNAKReceived);

/******************************************************************************/
/*!
 *  \fn     CPWriteQHDataToPacket()
 *  \brief  Writes the Quality Control handling data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteQHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket, \
        bool_t bNAKReceived);

/******************************************************************************/
/*!
 *  \fn     CPWriteSeqMonitoringDataToPacket()
 *  \brief  Writes the Sequence Monitoring data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteSeqMonitoringDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived);

/******************************************************************************/
/*!
 *  \fn     CPWriteAbortProcessDataToPacket()
 *  \brief  Writes the Abort Processdata to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteAbortProcessDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived);

/******************************************************************************/
/*!
 *  \fn     CPWriteErrorDataToPacket()
 *  \brief  Writes the Error details to the SPI packet frame
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPGetErrorData(ST_SPI_PACKET_FRAME *stPtrParsedPacket);

/******************************************************************************/
/*!
 *  \fn     CPWriteSystemSettingDataToPacket()
 *  \brief  Writes the System setting data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteSystemSettingDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived);

/******************************************************************************/
/*!
 *  \fn     CPWriteSTPHDataToPacket()
 *  \brief  Writes the Startup handling data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
uint16_t CPWriteSTPHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
        bool_t bNAKReceived);

/******************************************************************************/
/*!
 *  \fn     CPWriteSTPHDataToPacket()
 *  \brief  Writes the Startup handling data to the SPI Packet Frame
 *
 *  \param  ST_SPI_PACKET_FRAME* type address of the parsedPacketFrame
 *  \return Returns the no of bytes written to the Packet.
 */
/******************************************************************************/
void CPInitQueue();

/******************************************************************************/
/*!
 *  \fn     CPWriteSTPHDataToPacket()
 *  \brief  To check whether the queue is full
 *
 *  \param  void
 *  \return int
 */
/******************************************************************************/
int CPIsFull(void);

/******************************************************************************/
/*!
 *  \fn     CPIsEmpty()
 *  \brief  To check whether the queue is empty
 *  \param  void
 *  \return int
 */
/******************************************************************************/
int CPIsEmpty(void);

/******************************************************************************/
/*!
 *  \fn     CPEnqueue()
 *  \brief  To send update the queue with the latest command
 *  \param  int item
 *  \return void
 */
/******************************************************************************/
void CPEnqueue(ST_SPI_PACKET_FRAME *stPacketFrame);

/******************************************************************************/
/*!
 *  \fn     CPDequeue()
 *  \brief  To send first command from the queue
 *  \param  void
 *  \return int
 */
/******************************************************************************/
int CPDequeue();

/******************************************************************************/
/*!
 *  \fn     CPSendPacketToSitara()
 *  \brief  To send packet to processor
 *  \param  void
 *  \return int
 */
/******************************************************************************/
void CPSendPacketToSitara(void);

extern bool_t SPIDmaWrite(uint16_t *data);
extern void SPISetCommCtrlBusyInt();
extern void SPIClearCommCtrlBusyInt();
extern uint32_t SysGetADCDataLength(uint16_t usnSubCmd);
extern uint16_t SysGetAdcCountData(uint16_t* pData);
extern void SysInitGetHGBData(uint16_t* pusnData, uint16_t usnDataLength);
extern void SysInitGetFirmwareVer(uint16_t* pData);
extern uint32_t SysGetPulseHeightDataFrmSDRAM(E_CELL_TYPE eTypeofCell,\
        uint16_t* parrusnHeightData,uint32_t rusnPrevIndex,\
        const uint16_t kusnSize);
extern uint32_t SysGetADCRawDataFrmSDRAM(E_CELL_TYPE eTypeofCell,\
        uint16_t* parrusnHeightData,uint32_t rusnPrevHeightIndex,\
        const uint16_t kusnMaxSize);
extern void SysInitGetSystemStatus();
extern void SysInitSystemHealthData(uint16_t* pData);
extern void SysInitValveTest(unsigned char ucValveNumber);
extern void SysInitWasteBinReplaced(void);
extern uint16_t SysInitGetCountingTime(uint16_t* pData);
extern void CIProcessCommandAttribute(uint16_t usnMajorCmd, uint16_t usnMinorCmd);
extern void CIFormACKPacket(bool_t bTransmitNACK);
extern void SysDisableMotorMon();


#endif /* POC2V2CELLCOUNTERCPU1_INCLUDE_COMMPROTOCOL_H_ */

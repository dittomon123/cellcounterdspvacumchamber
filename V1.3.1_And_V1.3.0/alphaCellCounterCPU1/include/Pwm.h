/******************************************************************************/
/*! \file Pwm.h
 *
 *  \brief Header file for Pwm.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.1
 *   $Date:    $ Apr-09-2015
 *   $Author:  $ ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_pwm_module
//! @{
//******************************************************************************
#ifndef PWM_H_
#define PWM_H_
//#include "F28x_Project.h"     // Device Headerfile and Examples Include File
#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File
#include "Structure.h"

/******************************************************************************/
/*!
 * \def   CHECK_BIT(var,pos)
 * \brief Currently not used
 */
/******************************************************************************/
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))



/******************************************************************************/
/*!
 * \brief Preprocessor definitions
 * \def PWM_TB_DIV1
 * \def PWM_TB_DIV2
 * \def PWM_TB_DIV4
 * \def PWM_TB_DIV8
 * \def PWM_TB_DIV10
 */
/******************************************************************************/

/******************************************************************************/
/*!
 * \def   PWM_TB_DIV1
 * \brief High speed clk division by 1
 */
/******************************************************************************/
#define	PWM_TB_DIV1			0x0

/******************************************************************************/
/*!
 * \def   PWM_TB_DIV2
 * \brief High speed clk division by 2
 */
/******************************************************************************/
#define	PWM_TB_DIV2			0x1

/******************************************************************************/
/*!
 * \def   PWM_TB_DIV4
 * \brief High speed clk division by 4
 */
/******************************************************************************/
#define	PWM_TB_DIV4			0x2

/******************************************************************************/
/*!
 * \def   PWM_TB_DIV8
 * \brief High speed clk division by 8
 */
/******************************************************************************/
#define	PWM_TB_DIV8			0x4

/******************************************************************************/
/*!
 * \def   PWM_TB_DIV10
 * \brief High speed clk division by 10
 */
/******************************************************************************/
#define	PWM_TB_DIV10		0x5

/******************************************************************************/
/*!
 *
 * \fn extern void Motor1SensorISR();
 * \fn extern void Motor2SensorISR();
 * \fn extern void Motor3SensorISR();
 * \fn extern void Motor4SensorISR();
 * \fn extern void Motor5SensorISR();
 * \fn extern unsigned int OIHomeSensor1State(unsigned int ReadWrite);
 * \fn extern unsigned int OIHomeSensor2State(unsigned int ReadWrite);
 * \fn extern unsigned int OIHomeSensor3State(unsigned int ReadWrite);
 * \fn extern unsigned int OIHomeSensor4State(unsigned int ReadWrite);
 * \fn extern unsigned int OIHomeSensor5State(unsigned int ReadWrite);
 * \brief extern Function definitions:
 */
/******************************************************************************/


/******************************************************************************/
/*!
 *  \fn    extern void Motor1SensorISR()
 *  \brief extern Function definition of the function
 */
/******************************************************************************/
extern void Motor1SensorISR();

/******************************************************************************/
 /*!
  *  \fn    extern void Motor2SensorISR()
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern void Motor2SensorISR();

/******************************************************************************/
 /*!
  *  \fn    extern void Motor3SensorISR()
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern void Motor3SensorISR();

/******************************************************************************/
 /*!
  *  \fn    extern void Motor4SensorISR()
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern void Motor4SensorISR();

/******************************************************************************/
 /*!
  *  \fn    extern void Motor5SensorISR()
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern void Motor5SensorISR();

/******************************************************************************/
 /*!
  *  \fn    extern int16 PWMMotor1SensorPoll(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern int16 PWMMotor1SensorPoll(void);

/******************************************************************************/
 /*!
  *  \fn    extern int16 PWMMotor2SensorPoll(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern int16 PWMMotor2SensorPoll(void);

/******************************************************************************/
 /*!
  *  \fn    extern int16 PWMMotor3SensorPoll(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern int16 PWMMotor3SensorPoll(void);

/******************************************************************************/
 /*!
  *  \fn    extern int16 PWMMotor4SensorPoll(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern int16 PWMMotor4SensorPoll(void);

/******************************************************************************/
 /*!
  *  \fn    extern int16 PWMMotor5SensorPoll(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern int16 PWMMotor5SensorPoll(void);

/******************************************************************************/
 /*!
  *  \fn    extern unsigned int OIHomeSensor1State(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern unsigned int OIHomeSensor1State(void);

/******************************************************************************/
 /*!
  *  \fn    extern unsigned int OIHomeSensor2State(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern unsigned int OIHomeSensor2State(void);

/******************************************************************************/
 /*!
  *  \fn    extern unsigned int OIHomeSensor3State(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern unsigned int OIHomeSensor3State(void);

/******************************************************************************/
 /*!
  *  \fn    extern unsigned int OIHomeSensor4State(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern unsigned int OIHomeSensor4State(void);

/******************************************************************************/
 /*!
  *  \fn    extern unsigned int OIHomeSensor5State(void)
  *  \brief extern Function definition of the function
  */
/******************************************************************************/
extern unsigned int OIHomeSensor5State(void);

/******************************************************************************/
/*!
 * \fn void PWMInitialize()
 * \brief To initalize the PWM for motor
 * \fn void PWMConfig(Uint16 Channel, Uint16 period)
 * \brief To configure the PWM registers for Motors
 * \fn void PWMControl(Uint16 Channel, Uint16 State, Uint16 period)
 * \brief To configure the duty cycle for the PWM used for Motors
 * \fn void StartPWMCLK() - not used
 * \fn void PWMConfigMotor1Param(unsigned int  , unsigned int , unsigned int )
 * \brief To start and stop the X-Axis Motor on the given displacement and home
 * position sensor
 * \fn void PWMConfigMotor2Param(unsigned int  , unsigned int , unsigned int )
 * \brief To start and stop the Diluent Motor on the given displacement and home
 * position sensor
 * \fn void PWMConfigMotor3Param(unsigned int  , unsigned int , unsigned int )
 * \brief To start and stop the Sample Motor on the given displacement and home
 * position sensor
 * \fn void PWMConfigMotor4Param(unsigned int  , unsigned int , unsigned int )
 * \brief To start and stop the Waste Motor on the given displacement and home
 * position sensor
 * \fn void PWMConfigMotor5Param(unsigned int  , unsigned int , unsigned int )
 * \brief To start and stop the Y-Axis Motor on the given displacement and home
 * position sensor
 * \fn unsigned int PWMGetMotor1Status()
 * \brief To get the status of the X-Axis motor
 * \fn unsigned int PWMGetMotor2Status()
 * \brief To get the status of the Diluent motor
 * \fn unsigned int PWMGetMotor3Status()
 * \brief To get the status of the Sample motor
 * \fn unsigned int PWMGetMotor4Status()
 * \brief To get the status of the Waste motor
 * \fn unsigned int PWMGetMotor5Status()
 * \brief To get the status of the Y-Axis motor
 * \fn __interrupt void epwm1_isr(void)
 * \brief ISR of PWM1 - Y-Axis Motor
 * \fn __interrupt void epwm2_isr(void)
 * \brief ISR of PWM2 - Sample Motor
 * \fn __interrupt void epwm5_isr(void)
 * \brief ISR of PWM5 - Waste Motor
 * \fn __interrupt void epwm6_isr(void)
 * \brief ISR of PWM6 - Diluent Motor
 * \fn __interrupt void epwm8_isr(void)
 * \brief ISR of PWM8 - X-Axis Motor
 * \fn stPwmParameter* PWMGetMotor1Pwm8(void)
 * \brief To get Motor and PWM status of X-Axis Motor
 * \fn stPwmParameter* PWMGetMotor2Pwm6(void)
 * \brief To get Motor and PWM status of Diluent Motor
 * \fn stPwmParameter* PWMGetMotor3Pwm2(void)
 * \brief To get Motor and PWM status of Sample Motor
 * \fn stPwmParameter* PWMGetMotor4Pwm5(void)
 * \brief To get Motor and PWM status of Waste Motor
 * \fn stPwmParameter* PWMGetMotor5Pwm1(void)
 * \brief To get Motor and PWM status of Y-Axis Motor
 * \fn void PWMGetSCurveLookUp(stPwmParameter *PwmParam)
 * \brief To get S-Cureve look up parameters
 * \fn void PWM1FreqControl(Uint16 period)
 * \brief To Configure PWM 1 frequency
 * \fn void PWM2FreqControl(Uint16 period)
 * \brief To Configure PWM 2 frequency
 * \fn void PWM5FreqControl(Uint16 period)
 * \brief To Configure PWM 5 frequency
 * \fn void PWM6FreqControl(Uint16 period)
 * \brief To Configure PWM 6 frequency
 * \fn void PWM8FreqControl(Uint16 period)
 * \brief To Configure PWM 8 frequency
 * \fn void PWMInitHgbPwm(void)
 * \brief To initialize & configure the PWM used for HgB
 * \fn void PWMHgbPwmControl(Uint16 State, Uint16 period)
 * \brief To configure the duty cycle of PWM used for HgB
 * \fn void PWMSpiHomePosSense(volatile Uint16 ) - not used
 * \brief Function prototypes:
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     void PWMInitialize(void)
 *  \brief  Initialize 5 pwm engines for 5 stepper motor
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void PWMInitialize(void);

/******************************************************************************/
/*!
 *  \fn     void PWMConfig(Uint16 , Uint16 )
 *  \brief  Initialize individual pwm channels
 *  \param  Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void PWMConfig(Uint16 , Uint16 );

/******************************************************************************/
/*!
 *  \fn     void PWMControl(Uint16 , Uint16 , Uint16 )
 *  \brief  Controls individual pwm channels
 *  \param  Uint16,Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void PWMControl(Uint16 , Uint16 , Uint16 );

/******************************************************************************/
/*!
 *  \fn     void StartPWMCLK(void)
 *  \brief  not used
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void StartPWMCLK(void);

/******************************************************************************/
/*!
 *  \fn     void PWMParamDefaultState(void)
 *  \brief  Initialize pwm variables
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void PWMParamDefaultState(void);

/******************************************************************************/
/*!
 *  \fn     void PWMConfigMotor1Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency)
 *  \brief  Configure Motor 1 PWM parameters
 *  \param  float,Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void PWMConfigMotor1Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn     void PWMConfigMotor2Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency)
 *  \brief  Configure Motor 2 PWM parameters
 *  \param  float,Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void PWMConfigMotor2Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn     void PWMConfigMotor3Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency)
 *  \brief  Configure Motor 3 PWM parameters
 *  \param  float,Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void PWMConfigMotor3Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn     void PWMConfigMotor4Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency)
 *  \brief  Configure Motor 4 PWM parameters
 *  \param  float,Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void PWMConfigMotor4Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn     void PWMConfigMotor5Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency)
 *  \brief  Configure Motor 5 PWM parameters
 *  \param  float,Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void PWMConfigMotor5Param(float fDisplacement, Uint16 usnDirection, \
                            Uint16 StepFrequency);

/******************************************************************************/
/*!
 *  \fn     Uint16 PWMGetMotor1Status(void)
 *  \brief  Motor 1 pwm status
 *  \param  void
 *  \return Uint16
 */
/******************************************************************************/
Uint16 PWMGetMotor1Status(void);

/******************************************************************************/
/*!
 *  \fn     Uint16 PWMGetMotor2Status(void)
 *  \brief  Motor 2 pwm status
 *  \param  void
 *  \return Uint16
 */
/******************************************************************************/
Uint16 PWMGetMotor2Status(void);

/******************************************************************************/
/*!
 *  \fn     Uint16 PWMGetMotor3Status(void)
 *  \brief  Motor 3 pwm status
 *  \param  void
 *  \return Uint16
 */
/******************************************************************************/
Uint16 PWMGetMotor3Status(void);

/******************************************************************************/
/*!
 *  \fn     Uint16 PWMGetMotor4Status(void)
 *  \brief  Motor 4 pwm status
 *  \param  void
 *  \return Uint16
 */
/******************************************************************************/
Uint16 PWMGetMotor4Status(void);

/******************************************************************************/
/*!
 *  \fn     Uint16 PWMGetMotor5Status(void)
 *  \brief  Motor 5 pwm status
 *  \param  void
 *  \return Uint16
 */
/******************************************************************************/
Uint16 PWMGetMotor5Status(void);

/******************************************************************************/
/*!
 *  \fn     __interrupt void epwm1_isr(void)
 *  \brief  PWM interrupt handler for epwm1
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void epwm1_isr(void);

/******************************************************************************/
/*!
 *  \fn     __interrupt void epwm2_isr(void)
 *  \brief  PWM interrupt handler for epwm2
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void epwm2_isr(void);

/******************************************************************************/
/*!
 *  \fn     __interrupt void epwm5_isr(void)
 *  \brief  PWM interrupt handler for epwm5
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void epwm5_isr(void);

/******************************************************************************/
/*!
 *  \fn     __interrupt void epwm6_isr(void)
 *  \brief  PWM interrupt handler for epwm6
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void epwm6_isr(void);

/******************************************************************************/
/*!
 *  \fn     __interrupt void epwm8_isr(void)
 *  \brief  PWM interrupt handler for epwm8
 *  \param  void
 *  \return void
 */
/******************************************************************************/
__interrupt void epwm8_isr(void);


/******************************************************************************/
/*!
 *  \fn     stPwmParameter* PWMGetMotor1Pwm8(void)
 *  \brief  To get Structure address of Motor1Pwm8
 *  \param  void
 *  \return stPwmParameter*
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor1Pwm8(void);

/******************************************************************************/
/*!
 *  \fn     stPwmParameter* PWMGetMotor2Pwm6(void)
 *  \brief  To get Structure address of Motor2Pwm6
 *  \param  void
 *  \return stPwmParameter*
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor2Pwm6(void);

/******************************************************************************/
/*!
 *  \fn     stPwmParameter* PWMGetMotor3Pwm2(void)
 *  \brief  To get Structure address of Motor3Pwm2
 *  \param  void
 *  \return stPwmParameter*
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor3Pwm2(void);

/******************************************************************************/
/*!
 *  \fn     stPwmParameter* PWMGetMotor4Pwm5(void)
 *  \brief  To get Structure address of Motor4Pwm5
 *  \param  void
 *  \return stPwmParameter*
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor4Pwm5(void);

/******************************************************************************/
/*!
 *  \fn     stPwmParameter* PWMGetMotor5Pwm1(void)
 *  \brief  To get Structure address of Motor5Pwm1
 *  \param  void
 *  \return stPwmParameter*
 */
/******************************************************************************/
stPwmParameter* PWMGetMotor5Pwm1(void);


/******************************************************************************/
/*!
 *  \fn     void PWMGetSCurveLookUp(stPwmParameter *)
 *  \brief  S-Curve lookup table generation
 *  \param  stPwmParameter *
 *  \return void
 */
/******************************************************************************/
void PWMGetSCurveLookUp(stPwmParameter *);

/******************************************************************************/
/*!
 *  \fn     void PWM1FreqControl(Uint16 period)
 *  \brief  Controls pwm1 frequency
 *  \param
 *  \return void
 */
/******************************************************************************/
void PWM1FreqControl(Uint16 period);

/******************************************************************************/
/*!
 *  \fn     void PWM2FreqControl(Uint16 period)
 *  \brief  Controls pwm2 frequency
 *  \param  Uint16
 *  \return void
 */
/******************************************************************************/
void PWM2FreqControl(Uint16 period);

/******************************************************************************/
/*!
 *  \fn     void PWM5FreqControl(Uint16 period)
 *  \brief  Controls pwm5 frequency
 *  \param  Uint16
 *  \return void
 */
/******************************************************************************/
void PWM5FreqControl(Uint16 period);

/******************************************************************************/
/*!
 *  \fn     void PWM6FreqControl(Uint16 period)
 *  \brief  Controls pwm6 frequency
 *  \param  Uint16
 *  \return void
 */
/******************************************************************************/
void PWM6FreqControl(Uint16 period);


/******************************************************************************/
/*!
 *  \fn     void PWM8FreqControl(Uint16 period)
 *  \brief  Controls pwm8 frequency
 *  \param  Uint16
 *  \return void
 */
/******************************************************************************/
void PWM8FreqControl(Uint16 period);

/******************************************************************************/
/*!
 *  \fn     void PWMInitHgbPwm(void)
 *  \brief  Initialize individual pwm channels
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void PWMInitHgbPwm(void);

/******************************************************************************/
/*!
 *  \fn     void PWMHgbPwmControl(Uint16 State, Uint16 period)
 *  \brief  Controls pwm channel for HGB-LED
 *  \param  Uint16,Uint16
 *  \return void
 */
/******************************************************************************/
void PWMHgbPwmControl(Uint16 State, Uint16 period);

/******************************************************************************/
/*!
 *  \fn     void PWMSpiHomePosSense(volatile Uint16 )
 *  \brief  Reads Sensor state - not used
 *  \param  volatile Uint16
 *  \return void
 */
/******************************************************************************/
void PWMSpiHomePosSense(volatile Uint16 );

//void PWMGetSCurveLookUp(unsigned int Motor);
#endif /* PWM_H_ */
//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

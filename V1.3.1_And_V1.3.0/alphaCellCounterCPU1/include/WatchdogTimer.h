/******************************************************************************/
/*! \file WatchdogTimer.h
 *
 *  \brief Header file for WatchdogTimer.c.
 *
 *  \b Description:
 *     Function prototypes, variable declaration and pre-processor definitions
 *
 *   $Version: $ 0.2
 *   $Date:    $ Jan-21-2015
 *   $Author:  $ 20040919, ADITYA B G
 *
 *  \b Company:
 *
 *       Agappe Diagnostics Pvt Ltd.
 *
 *  (C) Copyright 2015 by Agappe Pvt Ltd. All rights reserved.
 *  This is unpublished proprietary source code of Agappe Diagnostics.
 *  The copyright notice does not evidence any actual or intended publication.
 *
 */
/******************************************************************************/
//******************************************************************************
//! \addtogroup cpu1_watchdog_module
//! @{
//******************************************************************************
#ifndef WATCHDOG_TIMER_H_
#define WATCHDOG_TIMER_H_

#include "F2837xD_device.h"     // F2837xD Headerfile Include File
#include "F2837xD_Examples.h"   // F2837xD Examples Include File



/******************************************************************************/
/*!
 * \brief Function prototypes:
 * \fn void void Service_WatchdogTimer(void);
 * \brief This function initialize CPU watchdog
 * \fn void void Enable_WatchdogTimer(void);
 * \brief This function enables CPU watchdog
 * \fn void void Disable_WatchdogTimer(void);
 * \brief This function disables CPU watchdog
 */
/******************************************************************************/
/******************************************************************************/
/*!
 *  \fn     void Service_WatchdogTimer(void)
 *  \brief  This function initialize CPU watchdog
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Service_WatchdogTimer(void);

/******************************************************************************/
/*!
 *  \fn     void Enable_WatchdogTimer(void)
 *  \brief  This function enables CPU watchdog
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Enable_WatchdogTimer(void);

/******************************************************************************/
/*!
 *  \fn     void Disable_WatchdogTimer(void)
 *  \brief  This function disables CPU watchdog
 *  \param  void
 *  \return void
 */
/******************************************************************************/
void Disable_WatchdogTimer(void);

#endif /* WATCHDOG_TIMER_H_ */

//******************************************************************************
// Close the Doxygen group.
//! @}
//******************************************************************************

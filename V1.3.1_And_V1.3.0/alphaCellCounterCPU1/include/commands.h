/*
 * commands.h
 *
 *  Created on: 29-Dec-2015
 *      Author: 20006324
 */

#ifndef COMMANDS_H_
#define COMMANDS_H_

/*!---------------------SITARA MAIN COMMANDS-------------------------*/
/******************************************************************************/
/*!
 * \def    SITARA_SYNC_MAIN_CMD
 * \brief  TBD
 */
/******************************************************************************/
#define SITARA_SYNC_MAIN_CMD							0x1000

/******************************************************************************/
/*!
 * \def    SITARA_ACK_MAIN_CMD
 * \brief  The acknowledgement command from the Processor
 */
/******************************************************************************/
#define SITARA_ACK_MAIN_CMD								0x1FFF

/******************************************************************************/
/*!
 * \def    SITARA_NACK_MAIN_CMD
 * \brief  The Negative acknowledgement command from the Processor
 */
/******************************************************************************/
#define SITARA_NACK_MAIN_CMD							0x1FFE

/******************************************************************************/
/*!
 * \def    SITARA_PATIENT_HANDLING
 * \brief  The Major command for Patient Handling sequences from the Processor
 */
/******************************************************************************/
#define SITARA_PATIENT_HANDLING							0x1001

/******************************************************************************/
/*!
 * \def    SITARA_CALIBRATION_HANDLING
 * \brief  The Major command for Calibration Handling sequences from
 *         the Processor
 */
/******************************************************************************/
#define SITARA_CALIBRATION_HANDLING						0x1002

/******************************************************************************/
/*!
 * \def    SITARA_FIRMWARE_VERSION_CMD
 * \brief  The Major command for Firmare version request from the Processor
 */
/******************************************************************************/
#define SITARA_FIRMWARE_VERSION_CMD						0x1003

/******************************************************************************/
/*!
 * \def    SITARA_SERVICE_HANDLING
 * \brief  The Major command for Service Handling sequences from the Processor
 */
/******************************************************************************/
#define SITARA_SERVICE_HANDLING							0x1004

/******************************************************************************/
/*!
 * \def    SITARA_QUALITY_HANDLING
 * \brief  The Major command for QC sequences from the Processor
 */
/******************************************************************************/
#define SITARA_QUALITY_HANDLING							0x1005

/******************************************************************************/
/*!
 * \def    SITARA_ABORT_MAIN_PROCESS_CMD
 * \brief  The Major command for aborting the current sequence,
 *         from the Processor
 */
/******************************************************************************/
#define SITARA_ABORT_MAIN_PROCESS_CMD					0x1006

/******************************************************************************/
/*!
 * \def    SITARA_SHUTDOWN_CMD
 * \brief  The Major command for SHUTDOWN process from the Processor
 */
/******************************************************************************/
#define SITARA_SHUTDOWN_CMD                             0x1007

/******************************************************************************/
/*!
 * \def    SITARA_SETTINGS_CMD
 * \brief  The Major command from the Processor for setting some parameters
 */
/******************************************************************************/
#define SITARA_SETTINGS_CMD                             0x1008

/******************************************************************************/
/*!
 * \def    SITARA_STARTUP_CMD
 * \brief  The Major command from the Processor for Startup to perform start up
 *          sequence
 */
/******************************************************************************/
#define SITARA_STARTUP_CMD                              0x1009

/******************************************************************************/
/*!
 * \def    SITARA_ERROR_CMD
 * \brief  The Major command from the Processor for error command
 */
/******************************************************************************/
#define SITARA_ERROR_CMD                                0x100A
//------------------------------------------------------------------

/*!---------------------SITARA SUB COMMANDS-------------------------*/

/******************************************************************************/
/*!
 * \def    SITARA_SYNC_SUB_CMD
 * \brief  TBD
 */
/******************************************************************************/
#define SITARA_SYNC_SUB_CMD								0xAAAA

/******************************************************************************/
/*!
 * \def    SITARA_ACK_SUB_CMD
 * \brief  Sub command for acknwoledgement from the processor
 */
/******************************************************************************/
#define SITARA_ACK_SUB_CMD								0x1FFC

/******************************************************************************/
/*!
 * \def    SITARA_NACK_SUB_CMD
 * \brief  Sub command for negetive acknwoledgement from the processor
 */
/******************************************************************************/
#define SITARA_NACK_SUB_CMD								0x1FFD

/******************************************************************************/
/*!
 * \def    SITARA_ABORT_PROCESS_SUB_CMD
 * \brief  Sub command from the processor for the abort process
 */
/******************************************************************************/
#define SITARA_ABORT_PROCESS_SUB_CMD					0x1BBB

/******************************************************************************/
/*!
 * \def    SITARA_ABORT_PROCESS_SUB_CMD_ERROR
 * \brief  Sub command from the processor for indicating the error in ABORT
 */
/******************************************************************************/
#define SITARA_ABORT_PROCESS_SUB_CMD_ERROR              0x1BBC

/******************************************************************************/
/*!
 * \def    SITARA_ABORT_PROCESS_RESET_COMMAND
 * \brief  Sub command from the processor to arrest the present sequence.
 */
/******************************************************************************/
#define SITARA_ABORT_PROCESS_RESET_COMMAND              0x1BCD

/******************************************************************************/
/*!
 * \def    SITARA_ABORT_Y_AXIS_PROCESS
 * \brief  Sub command from the processor to deactivate the Y AXIS
 */
/******************************************************************************/
#define SITARA_ABORT_Y_AXIS_PROCESS                     0x1BBE

/*!------------SITARA PATIENT HANDLING SUB COMMANDS-----------------*/
/******************************************************************************/
/*!
 * \def    SITARA_WHOLE_BLOOD_WBC_HGB
 * \brief  Sub command from the processor for whole blood sequence with only
 *         WBC and HGB count
 */
/******************************************************************************/
#define SITARA_WHOLE_BLOOD_WBC_HGB						0x1001

/******************************************************************************/
/*!
 * \def    SITARA_WHOLE_BLOOD_RBC_PLT
 * \brief  Sub command from the processor for whole blood sequence with only
 *         RBC and Platelet count
 */
/******************************************************************************/
#define SITARA_WHOLE_BLOOD_RBC_PLT						0x1002

/******************************************************************************/
/*!
 * \def    SITARA_WHOLE_BLOOD_RBC_WBC_PLT
 * \brief  Sub command from the processor for whole blood sequence
 */
/******************************************************************************/
#define SITARA_WHOLE_BLOOD_RBC_WBC_PLT					0x1003

/******************************************************************************/
/*!
 * \def    SITARA_PRE_DILUENT_MODE
 * \brief  Sub command for prediluent mode of operation from the processor
 */
/******************************************************************************/
#define SITARA_PRE_DILUENT_MODE							0x1004

/******************************************************************************/
/*!
 * \def    SITARA_BODY_FLUID
 * \brief  Sub command for body fluid mode of operation from the processor
 */
/******************************************************************************/
#define SITARA_BODY_FLUID								0x1005

/******************************************************************************/
/*!
 * \def    SITARA_WB_WBC_PULSE_HEIGHT_CMD
 * \brief  Sub command for WBC Pulse height data request from the processor
 */
/******************************************************************************/
#define SITARA_WB_WBC_PULSE_HEIGHT_CMD					0x1006

/******************************************************************************/
/*!
 * \def    SITARA_WB_RBC_PULSE_HEIGHT_CMD
 * \brief  Sub command for RBC Pulse height data request from the processor
 */
/******************************************************************************/
#define SITARA_WB_RBC_PULSE_HEIGHT_CMD					0x1007

/******************************************************************************/
/*!
 * \def    SITARA_WB_PLT_PULSE_HEIGHT_CMD
 * \brief  Sub command for Platelets Pulse height data request from
 *         the processor
 */
/******************************************************************************/
#define SITARA_WB_PLT_PULSE_HEIGHT_CMD					0x1008

/******************************************************************************/
/*!
 * \def    SITARA_WBC_RAW_DATA_1SEC
 * \brief  Sub command for 1 second Raw Data request of WBC from the processor
 */
/******************************************************************************/
#define SITARA_WBC_RAW_DATA_1SEC						0x1009

/******************************************************************************/
/*!
 * \def    SITARA_RBC_RAW_DATA_1SEC
 * \brief  Sub command for 1 second Raw Data request of RBC from the processor
 */
/******************************************************************************/
#define SITARA_RBC_RAW_DATA_1SEC						0x1010

/******************************************************************************/
/*!
 * \def    SITARA_PLT_RAW_DATA_1SEC
 * \brief  Sub command for 1 second Raw Data request of Platelets from
 *         the processor
 */
/******************************************************************************/
#define SITARA_PLT_RAW_DATA_1SEC						0x1011

/******************************************************************************/
/*!
 * \def    SITARA_START_DISPENSE_CMD
 * \brief  Sub Command from the processor to dispense the diluent
 *         in pre-diluent mode
 */
/******************************************************************************/
#define SITARA_START_DISPENSE_CMD						0x100A

/******************************************************************************/
/*!
 * \def    SITARA_START_PRE_DILUENT_COUNTING_CMD
 * \brief  Sub Command from the processor to dispense the diluent
 *         in pre-diluent mode
 */
/******************************************************************************/
#define SITARA_START_PRE_DILUENT_COUNTING_CMD			0x100B

/******************************************************************************/
/*!
 * \def    SITARA_WBC_RAW_DATA_NEXT_SEC
 * \brief  Sub command for Raw Data Of WBC for the next second from
 *         the processor
 */
/******************************************************************************/
#define SITARA_WBC_RAW_DATA_NEXT_SEC					0x100C

/******************************************************************************/
/*!
 * \def    SITARA_WBC_RAW_DATA_NEXT_SEC
 * \brief  Sub command for Raw Data Of RBC for the next second from
 *         the processor
 */
/******************************************************************************/
#define SITARA_RBC_RAW_DATA_NEXT_SEC					0x100D

/******************************************************************************/
/*!
 * \def    SITARA_PLT_RAW_DATA_NEXT_SEC
 * \brief  Sub command for Raw Data Of Platelets for the next second from
 *         the processor
 */
/******************************************************************************/
#define SITARA_PLT_RAW_DATA_NEXT_SEC					0x100E

/******************************************************************************/
/*!
 * \def    SITARA_START_POLLING_START_BUTTON_CMD
 * \brief  Sub command from the processor to Start the polling of Start Button
 */
/******************************************************************************/
#define SITARA_START_POLLING_START_BUTTON_CMD			0x100F

/******************************************************************************/
/*!
 * \def    SITARA_STOP_POLLING_START_BUTTON_CMD
 * \brief  Sub command from the processor to Stop the polling of Start Button
 */
/******************************************************************************/
#define SITARA_STOP_POLLING_START_BUTTON_CMD			0x1012

/******************************************************************************/
/*!
 * \def    SITARA_PULSE_HEIGHT_DATA_RECEIVED
 * \brief  Sub command from the processor to indicate WBC, RBC &  PLT
 *         pulse height data received
 */
/******************************************************************************/
#define SITARA_PULSE_HEIGHT_DATA_RECEIVED               0x1013

/*!------------SITARA CALIBRATION HANDLING SUB COMMANDS-----------------*/

/******************************************************************************/
/*!
 * \def    SITARA_CALIBRATION_AUTO_WB
 * \brief  Sub command for AUTO CALIBRATION from the processor
 */
/******************************************************************************/
#define SITARA_CALIBRATION_AUTO_WB						0x1101

/******************************************************************************/
/*!
 * \def    SITARA_CALIBRATION_COMMERCIAL_WBC
 * \brief  Sub command for COMMERCIAL CALIBRATION of WBC from the processor
 */
/******************************************************************************/
#define SITARA_CALIBRATION_COMMERCIAL_WBC				0x1102

/******************************************************************************/
/*!
 * \def    SITARA_CALIBRATION_COMMERCIAL_RBC
 * \brief  Sub command for COMMERCIAL CALIBRATION of RBC from the processor
 */
/******************************************************************************/
#define SITARA_CALIBRATION_COMMERCIAL_RBC               0x1103

/******************************************************************************/
/*!
 * \def    SITARA_CALIBRATION_COMMERCIAL_PLT
 * \brief  Sub command for COMMERCIAL CALIBRATION of Platelets from
 *         the processor
 */
/******************************************************************************/
#define SITARA_CALIBRATION_COMMERCIAL_PLT               0x1104

/*!------------SITARA SERVICE HANDLING SUB COMMANDS-----------------*/
/*!---------------Maintenance Commands------------------*/
/******************************************************************************/
/*!
 * \def    SITARA_PRIME_WITH_RINSE_CMD
 * \brief  Sub command from the processor to prime with rinse
 */
/******************************************************************************/
#define SITARA_PRIME_WITH_RINSE_CMD						0x1001

/******************************************************************************/
/*!
 * \def    SITARA_PRIME_WITH_LYSE_CMD
 * \brief  Sub command from the processor to prime with lyse
 */
/******************************************************************************/
#define SITARA_PRIME_WITH_LYSE_CMD						0x1002

/******************************************************************************/
/*!
 * \def    SITARA_PRIME_WITH_DILUENT_CMD
 * \brief  Sub command from the processor to prime with diluent
 */
/******************************************************************************/
#define SITARA_PRIME_WITH_DILUENT_CMD					0x1003

/******************************************************************************/
/*!
 * \def    SITARA_PRIME_ALL_CMD
 * \brief  Sub command from the processor to prime with all 3 reagents
 */
/******************************************************************************/
#define SITARA_PRIME_ALL_CMD							0x1004

/******************************************************************************/
/*!
 * \def    SITARA_BACK_FLUSH_CMD
 * \brief  Sub command from the processor to backflush the orifices
 */
/******************************************************************************/
#define SITARA_BACK_FLUSH_CMD							0x1005

/******************************************************************************/
/*!
 * \def    SITARA_PRIME_WITH_EZ_CLEANSER_CMD
 * \brief  Sub command from the processor to clean with EZ cleaner
 */
/******************************************************************************/
#define SITARA_PRIME_WITH_EZ_CLEANSER_CMD				0x1006

/******************************************************************************/
/*!
 * \def    SITARA_ZAP_APERTURE
 * \brief  Sub command from the processor for Zapping the electrodes
 */
/******************************************************************************/
#define SITARA_ZAP_APERTURE                     		0x1007

/******************************************************************************/
/*!
 * \def    SITARA_DRAIN_BATH
 * \brief  Sub command from the processor to drain both the baths
 */
/******************************************************************************/
#define SITARA_DRAIN_BATH                       		0x1008

/******************************************************************************/
/*!
 * \def    SITARA_DRAIN_ALL
 * \brief  Sub command from the processor to drain out the tubings and the baths
 */
/******************************************************************************/
#define SITARA_DRAIN_ALL                        		0x1009

/******************************************************************************/
/*!
 * \def    SITARA_CLEAN_BATH
 * \brief  Sub command from the processor to clean both the baths
 */
/******************************************************************************/
#define SITARA_CLEAN_BATH                       		0x100A

/*!---------------Valve Test Commands------------------*/
/******************************************************************************/
/*!
 * \def    SITARA_VALVE_TEST_CMD
 * \brief  The sub command from the processor to test each valve in the assembly
 */
/******************************************************************************/
#define SITARA_VALVE_TEST_CMD							0x100B

/*!--------------System Status Commands----------------*/
/******************************************************************************/
/*!
 * \def    SITARA_SYSTEM_STATUS_CMD
 * \brief  The sub command from the processor to monitor the system status
 */
/******************************************************************************/
#define SITARA_SYSTEM_STATUS_CMD						0x100C

/******************************************************************************/
/*!
 * \def    SITARA_WASTE_BIN_REPLACED_CMD
 * \brief  The sub command from the processor to indicate that
 *         Waste bin is replaced
 */
/******************************************************************************/
#define SITARA_WASTE_BIN_REPLACED_CMD					0x100D

/******************************************************************************/
/*!
 * \def    SITARA_START_UP_SEQUENCE_CMD
 * \brief  The sub command from the processor to start the START-UP sequence
 */
/******************************************************************************/
#define SITARA_START_UP_SEQUENCE_CMD					0x100E

/******************************************************************************/
/*!
 * \def    SITARA_STARTUP_FAILED_CMD
 * \brief  The command from the processor to perform the startup
 */
/******************************************************************************/

#define SITARA_STARTUP_FAILED_CMD                       0x100F  //HN_Added

/******************************************************************************/
/*!
 * \def    SITARA_HEAD_RINSING_CMD
 * \brief  The sub command from the processor to start the Head Rinsing sequence
 */
/******************************************************************************/
#define SITARA_HEAD_RINSING_CMD							0x100F

/******************************************************************************/
/*!
 * \def    SITARA_PROBE_CLEANING_CMD
 * \brief  The sub command from the processor to start the probe cleaning
 *         sequence
 */
/******************************************************************************/
#define SITARA_PROBE_CLEANING_CMD						0x1010

/******************************************************************************/
/*!
 * \def    SITARA_X_AXIS_MOTOR_CHECK_CMD
 * \brief  The sub command from the processor to initiate the homing of X axis
 */
/******************************************************************************/
#define SITARA_X_AXIS_MOTOR_CHECK_CMD					0x1011

/******************************************************************************/
/*!
 * \def    SITARA_Y_AXIS_MOTOR_CHECK_CMD
 * \brief  The sub command from the processor to initiate the homing of Y axis
 */
/******************************************************************************/
#define SITARA_Y_AXIS_MOTOR_CHECK_CMD					0x1012

/******************************************************************************/
/*!
 * \def    SITARA_DILUENT_SYRINGE_MOTOR_CHECK_CMD
 * \brief  The sub command from the processor to initiate the homing of DILUENT
 *         SYRINGE MOTOR
 */
/******************************************************************************/
#define SITARA_DILUENT_SYRINGE_MOTOR_CHECK_CMD			0x1013

/******************************************************************************/
/*!
 * \def    SITARA_WASTE_SYRINGE_MOTOR_CHECK_CMD
 * \brief  The sub command from the processor to initiate the homing of WASTE
 *         SYRINGE MOTOR
 */
/******************************************************************************/
#define SITARA_WASTE_SYRINGE_MOTOR_CHECK_CMD			0x1014

/******************************************************************************/
/*!
 * \def    SITARA_SAMPLE_LYSE_SYRINGE_MOTOR_CHECK_CMD
 * \brief  The sub command from the processor to initiate the homing of SAMPLE
 *         SYRINGE MOTOR
 */
/******************************************************************************/
#define SITARA_SAMPLE_LYSE_SYRINGE_MOTOR_CHECK_CMD		0x1015

/******************************************************************************/
/*!
 * \def    SITARA_COUNTING_TIME_SEQUENCE_START_CMD
 * \brief  The sub command from the processor to start the Counting Time
 *         Sequence
 */
/******************************************************************************/
#define SITARA_COUNTING_TIME_SEQUENCE_START_CMD			0x1016

/******************************************************************************/
/*!
 * \def    SITARA_WAKE_UP_SEQUENCE_CMD
 * \brief  The sub command from the processor to start the WAKE-UP Sequence
 */
/******************************************************************************/
#define SITARA_WAKE_UP_SEQUENCE_CMD                     0x1019 //HN_Change(0x1019)

/******************************************************************************/
/*!
 * \def    SITARA_HOME_POSITION_STATUS_CMD
 * \brief  The sub command which requests for Home position status of motors
 */
/******************************************************************************/
#define SITARA_HOME_POSITION_STATUS_CMD                 0x1018

/******************************************************************************/
/*!
 * \def    SITARA_START_TOSHIP_SEQUENCE_CMD
 * \brief  The sub command from the processor to start the TOSHIP Sequence
 */
/******************************************************************************/
#define SITARA_START_TOSHIP_SEQUENCE_CMD                 0x1017  //HN_added

/******************************************************************************/
/*!
 * \def    SITARA_STARTUP_CMD
 * \brief  The sub command from the processor to start the start up Sequence
 */
/******************************************************************************/
#define SITARA_NORMAL_STARTUP_CMD                         0x1027 //0x101A (9/1/19)


/******************************************************************************/
/*!
 * \def    SITARA_ABNORMAL_STARTUP_CMD
 * \brief  The sub command from the processor to start the start up during
 *          abnormal shutdown
 */
/******************************************************************************/
#define SITARA_ABNORMAL_STARTUP_CMD                     0x101B

/******************************************************************************/
/*!
 * \def    SITARA_ABNORMAL_STARTUP_CMD
 * \brief  The sub command from the processor to start the start up during
 *          abnormal shutdown
 */
/******************************************************************************/
#define SITARA_VOLUMETRIC_CHECK_CMD                     0x101C

/******************************************************************************/
/*!
 * \def    SITARA_SLEEP_SEQUENCE_CMD
 * \brief  The sub command from the processor to start the sleep sequence
 */
/******************************************************************************/
#define SITARA_SLEEP_SEQUENCE_CMD                       0x101D

/******************************************************************************/
/*!
 * \def    SITARA_ALL_MOTOR_Y_AXIS_CMD
 * \brief  The sub command from the processor to Y-Axis Check for All motor
 * check sequence
 */
/******************************************************************************/
#define SITARA_ALL_MOTOR_Y_AXIS_CMD                     0x101E

/******************************************************************************/
/*!
 * \def    SITARA_ALL_MOTOR_X_AXIS_CMD
 * \brief  The sub command from the processor to X-Axis Check for All motor
 * check sequence
 */
/******************************************************************************/
#define SITARA_ALL_MOTOR_X_AXIS_CMD                     0x101F

/******************************************************************************/
/*!
 * \def    SITARA_ALL_MOTOR_SAMPLE_CMD
 * \brief  The sub command from the processor to Sample syringe Check for All
 * motor check sequence
 */
/******************************************************************************/
#define SITARA_ALL_MOTOR_SAMPLE_CMD                     0x1020

/******************************************************************************/
/*!
 * \def    SITARA_ALL_MOTOR_DILUENT_CMD
 * \brief  The sub command from the processor to Diluent syringe Check for All
 * motor check sequence
 */
/******************************************************************************/
#define SITARA_ALL_MOTOR_DILUENT_CMD                    0x1021

/******************************************************************************/
/*!
 * \def    SITARA_ALL_MOTOR_WASTE_CMD
 * \brief  The sub command from the processor to Waste syringe Check for All
 * motor check sequence
 */
/******************************************************************************/
#define SITARA_ALL_MOTOR_WASTE_CMD                      0x1022
/******************************************************************************/
/*!
 * \def    SITARA_VOLUMETRIC_TUBE_CLEAN_CMD
 * \brief  The sub command from the processor to
 * clean the volumetric tubes
 */
/******************************************************************************/
#define SITARA_VOLUMETRIC_TUBE_CLEAN_CMD                0x1023

/******************************************************************************/
/*!
 * \def    SITARA_BATH_FILL_CMD
 * \brief  The sub command from the processor to
 *          fill the baths
 */
/******************************************************************************/
#define SITARA_BATH_FILL_CMD                          0x1024      //HN_Added(4/01/19)

/******************************************************************************/
/*!
 * \def    SITARA_START_UP_FROM_SERVICE_CMD
 * \brief  The sub command from the processor to
 *          perform the startup
 */
/******************************************************************************/
#define SITARA_START_UP_FROM_SERVICE_CMD               0x1025      //HN_Added(4/01/19)
/******************************************************************************/
/*!
 * \def    SITARA_STARTUP_BACKFLUSH
 * \brief  The sub command from the processor to
 *          perform the startup_backfludh
 */
/******************************************************************************/

#define  SITARA_STARTUP_BACKFLUSH                       0x1026      //HN_Added(8/01/19)

/******************************************************************************/
/*!
 * \def    SITARA_INITIATE_ZAP
 * \brief  The sub command from the processor to
 *          perform the zap
 */
/******************************************************************************/

#define  SITARA_INITIATE_ZAP                             0x102A      //HN_Added(15/05/19)

/******************************************************************************//******************************************************************************/
/*!
 * \def    GLASS_TUBE_FILLING
 * \brief  The sub command from the processor to
 *          perform the zap
 */
/******************************************************************************/

#define  SITARA_GLASSTUBE_FILLING                             0x102B      //jojo added

/******************************************************************************/
/*!
 * \def    Fill all the line
 * \brief  The sub command from the processor to
 *          perform the zap
 */
/******************************************************************************/

#define  SITARA_COUNT_FAIL                                      0x102C      //jojo added0x102C      //jojo added

/******************************************************************************/
/******************************************************************************/
/*!
 * \def    Fill all the linea and probe
 * \brief  The sub command from the processor to
 *          perform the zap
 */
/******************************************************************************/

/******************************************************************************/

#define  SITARA_HYPO_CLEANER_FAILED                             0x102D      //jojo added 0x102D      //jojo added

/******************************************************************************/

//---------------------------------------------------------------------
/*!----------------CPU1 & CPU2 FIRMWARE VERSION NUMBER----------------*/

/******************************************************************************/
/*!
 * \def    SITARA_CPU1_CPU2_VERSION_CMD
 * \brief  The sub command which requests for CPU1 and CPU2 versions
 */
/******************************************************************************/
#define SITARA_CPU1_CPU2_VERSION_CMD					0x1001

/*!----------------QUALITY HANDLING----------------*/
/******************************************************************************/
/*!
 * \def    SITARA_WHOLE_BLOOD_CONTROL
 * \brief  The sub command for whole blood mode in QC mode
 */
/******************************************************************************/
#define SITARA_WHOLE_BLOOD_CONTROL						0x1201

/******************************************************************************/
/*!
 * \def    SITARA_BODY_FLUID_CONTROL
 * \brief  The sub command for body fluid mode in QC mode
 */
/******************************************************************************/
#define SITARA_BODY_FLUID_CONTROL   					0x1202
/*!------------------------------------------------------------------*/

/*!----------------SHUTDOWN SEQUENCE----------------*/

/******************************************************************************/
/*!
 * \def    SITARA_SHUTDOWN_SEQUENCE_START_CMD
 * \brief  The sub command for SHUT-DOWN process from the processor
 */
/******************************************************************************/
#define SITARA_SHUTDOWN_SEQUENCE_START_CMD              0x1301
/*!------------------------------------------------------------------*/

/*!----------------SYSTEM SETTING COMMANDS----------------*/

/******************************************************************************/
/*!
 * \def    SITARA_BUBBLING_TIME_CMD
 * \brief  The sub command from the processor to set the bubbling frequencies
 */
/******************************************************************************/
#define SITARA_BUBBLING_TIME_CMD                        0x1401

/******************************************************************************/
/*!
 * \def    SITARA_FLOW_CALIBRATION_CMD
 * \brief  The sub command from the processor to perform WBC & RBC acquisition time
 */
/******************************************************************************/
#define SITARA_FLOW_CALIBRATION_CMD                     0x1402
/******************************************************************************/
/*!
 * \def    SITARA_FLOW_CALIBRATION_RESULTS_CMD
 * \brief  The sub command from the processor to perform WBC & RBC acquisition time
 */
/******************************************************************************/
#define SITARA_FLOW_CALIBRATION_RESULTS_CMD             0x1403
/******************************************************************************/
/******************************************************************************/
/*!
 * \def    SITARA_TEST_DATA_FROM_UI
 * \brief
 */
/******************************************************************************/
#define SITARA_TEST_DATA_FROM_UI                       0x1404
/*!
 * \def    SITARA_PRESSURE_CALIBRATION_CMD
 * \brief  The sub command from the processor to perform the pressure calibration
 */
/******************************************************************************/

#define SITARA_PRESSURE_CALIBRATION_CMD                  0x1029  //HN_Added

/******************************************************************************/

/*!------------------------------------------------------------------*/

/******************************************************************************/
/*!
 * \def    SITARA_TRIGGER_BUTTON_PRESSED
 * \brief  TBD
 */
/******************************************************************************/
#define SITARA_TRIGGER_BUTTON_PRESSED                   0x1CCC


/******************************************************************************/
/*!
 * \def    SITARA_ERROR_REQUEST
 * \brief  The minor command from the Processor for error request
 */
/******************************************************************************/
#define SITARA_ERROR_REQUEST                                0x1001
/*!---------------------DELFINO MAIN COMMANDS-------------------------*/

/******************************************************************************/
/*!
 * \def    DELFINO_SYNC_MAIN_CMD
 * \brief  TBD
 */
/******************************************************************************/
#define DELFINO_SYNC_MAIN_CMD							0x2000

/******************************************************************************/
/*!
 * \def    DELFINO_ACK_MAIN_CMD
 * \brief  The acknowledgment command from the controller
 */
/******************************************************************************/
#define DELFINO_ACK_MAIN_CMD							0x2FFF

/******************************************************************************/
/*!
 * \def    DELFINO_NACK_MAIN_CMD
 * \brief  The Negative acknowledgment command from the controller
 */
/******************************************************************************/
#define DELFINO_NACK_MAIN_CMD 							0x2FFE

/******************************************************************************/
/*!
 * \def    DELFINO_BUSY_CMD
 * \brief  TBD
 */
/******************************************************************************/
#define DELFINO_BUSY_CMD								0x0001

/******************************************************************************/
/*!
 * \def    DELFINO_PATIENT_HANDLING
 * \brief  The Major command for Patient Handling sequences from the controller
 */
/******************************************************************************/
#define DELFINO_PATIENT_HANDLING						0x2001

/******************************************************************************/
/*!
 * \def    DELFINO_CALIBRATION_HANDLING
 * \brief  The Major command for CALIBRATION_HANDLING sequences from the
 *         controller
 */
/******************************************************************************/
#define DELFINO_CALIBRATION_HANDLING					0x2002

/******************************************************************************/
/*!
 * \def    DELFINO_FIRMWARE_VERSION_CMD
 * \brief  The Major command for CPU1_CPU2 Version from the controller
 */
/******************************************************************************/
#define DELFINO_FIRMWARE_VERSION_CMD					0x2003

/******************************************************************************/
/*!
 * \def    DELFINO_SERVICE_HANDLING
 * \brief  The Major command for Service handling from the controller
 */
/******************************************************************************/
#define DELFINO_SERVICE_HANDLING						0x2004

/******************************************************************************/
/*!
 * \def    DELFINO_QUALITY_HANDLING
 * \brief  The Major command for Quality control from the controller
 */
/******************************************************************************/
#define DELFINO_QUALITY_HANDLING						0x2005

/******************************************************************************/
/*!
 * \def    DELFINO_ABORT_PROCESS_MAIN_CMD
 * \brief  The Major command from the controller to abort the process
 */
/******************************************************************************/
#define DELFINO_ABORT_PROCESS_MAIN_CMD					0x200A

/******************************************************************************/
/*!
 * \def    DELFINO_SHUTDOWN_SEQUENCE_CMD
 * \brief  The Major command from the controller to initiate SHUTDOWN
 */
/******************************************************************************/
#define DELFINO_SHUTDOWN_SEQUENCE_CMD                   0x2007

/******************************************************************************/
/*!
 * \def    DELFINO_MONITOR_SEQUENCE
 * \brief  The Major command for Sequence Monitoring from the controller
 */
/******************************************************************************/
#define DELFINO_MONITOR_SEQUENCE                        0x2006

/******************************************************************************/
/*!
 * \def    DELFINO_SYSTEM_SETTING_CMD
 * \brief  The Major command for system setting handling from the controller
 */
/******************************************************************************/
#define DELFINO_SYSTEM_SETTING_CMD                      0x2008

/******************************************************************************/
/*!
 * \def    DELFINO_STARTUP_HANDLING_CMD
 * \brief  The Major command for startup handling from the controller
 */
/******************************************************************************/
#define DELFINO_STARTUP_HANDLING_CMD                    0x2009

/******************************************************************************/
/*!
 * \def    DELFINO_ERROR_HANDLING_CMD
 * \brief  The Major command for error handling from the controller
 */
/******************************************************************************/
#define DELFINO_ERROR_HANDLING_CMD                      0x200B

/*!---------------------DELFINO SUB COMMANDS-------------------------*/
/******************************************************************************/
/*!
 * \def    DELFINO_SYNC_SUB_CMD
 * \brief  TBD
 */
/******************************************************************************/
#define DELFINO_SYNC_SUB_CMD							0xDDDD

/******************************************************************************/
/*!
 * \def    DELFINO_ACK_SUB_CMD
 * \brief  The sub command for acknowledgment from the controller
 */
/******************************************************************************/
#define DELFINO_ACK_SUB_CMD								0x2FFC

/******************************************************************************/
/*!
 * \def    DELFINO_ACK_SUB_CMD
 * \brief  The sub command for negative acknowledgment from the controller
 */
/******************************************************************************/
#define	DELFINO_NACK_SUB_CMD							0x2FFD

/******************************************************************************/
/*!
 * \def    DELFINO_ABORT_PROCESS_SUB_CMD
 * \brief  The sub command for abort process from the controller
 */
/******************************************************************************/
#define DELFINO_ABORT_PROCESS_SUB_CMD					0x2BBB

/*!------------DELFINO PATIENT HANDLING SUB COMMANDS-----------------*/
/******************************************************************************/
/*!
 * \def    DELFINO_WBC_PULSE_HEIGHT_DATA
 * \brief  The sub command for WBC PULSE HEIGHT DATA from the controller
 */
/******************************************************************************/
#define DELFINO_WBC_PULSE_HEIGHT_DATA					0x2001

/******************************************************************************/
/*!
 * \def    DELFINO_RBC_PULSE_HEIGHT_DATA
 * \brief  The sub command for RBC PULSE HEIGHT DATA from the controller
 */
/******************************************************************************/
#define DELFINO_RBC_PULSE_HEIGHT_DATA					0x2002

/******************************************************************************/
/*!
 * \def    DELFINO_PLT_PULSE_HEIGHT_DATA
 * \brief  The sub command for PLT PULSE HEIGHT DATA from the controller
 */
/******************************************************************************/
#define DELFINO_PLT_PULSE_HEIGHT_DATA					0x2003

/******************************************************************************/
/*!
 * \def    DELFINO_WBC_RBC_PLT_HGB_COUNT
 * \brief  The sub command from the controller to send the count of WBC RBC and
 *         Platelets
 */
/******************************************************************************/
#define DELFINO_WBC_RBC_PLT_HGB_COUNT					0x2004


/******************************************************************************/
/*!
 * \def    DELFINO_WBC_ADC_RAW_DATA
 * \brief  The sub command from the controller to send the WBC raw data
 */
/******************************************************************************/
#define DELFINO_WBC_ADC_RAW_DATA						0x2006

/******************************************************************************/
/*!
 * \def    DELFINO_RBC_ADC_RAW_DATA
 * \brief  The sub command from the controller to send the RBC raw data
 */
/******************************************************************************/
#define DELFINO_RBC_ADC_RAW_DATA						0x2007

/******************************************************************************/
/*!
 * \def    DELFINO_RBC_ADC_RAW_DATA
 * \brief  The sub command from the controller to send the PLATELETS raw data
 */
/******************************************************************************/
#define DELFINO_PLT_ADC_RAW_DATA						0x2008

/******************************************************************************/
/*!
 * \def    DELFINO_ASPIRATION_COMPLETED
 * \brief  The sub command from the controller to indicate that aspiration is
 *         completed
 */
/******************************************************************************/
#define DELFINO_ASPIRATION_COMPLETED           			0x2009

/******************************************************************************/
/*!
 * \def    DELFINO_NEEDLE_READY_FOR_DISPENSING_CMD
 * \brief  The sub command from the controller to indicate that needle is ready
 *         to dispense
 */
/******************************************************************************/
#define DELFINO_NEEDLE_READY_FOR_DISPENSING_CMD			0x200A

/******************************************************************************/
/*!
 * \def    DELFINO_DISPENSE_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that dispensaton is
 *         done
 */
/******************************************************************************/
#define DELFINO_DISPENSE_COMPLETED_CMD					0x200B

/******************************************************************************/
/*!
 * \def    DELFINO_READY_FOR_ASPIRATION
 * \brief  The sub command from the controller to indicate that ready for
 *         aspiration
 */
/******************************************************************************/
#define DELFINO_READY_FOR_ASPIRATION					0x200C

/******************************************************************************/
/*!
 * \def    DELFINO_ASPIRATION_KEY_PRESSED
 * \brief  The sub command from the controller to indicate that Aspiration key
 *         press was received from Sitara
 */
/******************************************************************************/
#define DELFINO_ASPIRATION_KEY_PRESS_RECIEVED           0x202E

/******************************************************************************/
/*!
 * \def    DELFINO_SEQUENCE_STARTED
 * \brief  The sub command from the controller to indicate that the sequence is
 *         started
 */
/******************************************************************************/
#define DELFINO_SHUTDOWN_SEQUENCE_STARTED               0x2302

/******************************************************************************/
/*!
 * \def    DELFINO_SEQUENCE_COMPLETION
 * \brief  The sub command from the controller to indicate that the sequence is
 *         completed
 */
/******************************************************************************/
#define DELFINO_SEQUENCE_COMPLETION                     0X200E

//--------------------------------------------------------------------
/*!------------DELFINO SERVICE HANDLING SUB COMMANDS-----------------*/
	/*!---------------Maintainence Commands------------------*/

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the prime with
 *         rinse sequence is completed
 */
/******************************************************************************/
#define DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD				0x2001

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the prime with
 *         lyse sequence is completed
 */
/******************************************************************************/
#define DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD				0x2002

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the prime with
 *         diluent sequence is completed
 */
/******************************************************************************/
#define DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD			0x2003

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_ALL_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the prime with
 *         all reagents sequence is completed
 */
/******************************************************************************/
#define DELFINO_PRIME_ALL_COMPLETED_CMD						0x2004

/******************************************************************************/
/*!
 * \def    DELFINO_BACK_FLUSH_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the backflush
 *         sequence is completed
 */
/******************************************************************************/
#define DELFINO_BACK_FLUSH_COMPLETED_CMD					0x2005

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_WITH_EZ_CLEANSER_CMD
 * \brief  The sub command from the controller to indicate that the prime with
 *         EZ cleaner sequence is completed
 */
/******************************************************************************/
#define DELFINO_PRIME_WITH_EZ_CLEANSER_CMD					0x2006

/******************************************************************************/
/*!
 * \def    DELFINO_ZAP_APERTURE_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the Electrode
 *         zapping sequence is completed
 */
/******************************************************************************/
#define DELFINO_ZAP_APERTURE_COMPLETED_CMD              	0x2007

/******************************************************************************/
/*!
 * \def    DELFINO_DRAIN_BATH_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the sequence
 *         for draining out both the baths is completed
 */
/******************************************************************************/
#define DELFINO_DRAIN_BATH_COMPLETED_CMD                	0x2008

/******************************************************************************/
/*!
 * \def    DELFINO_DRAIN_ALL_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the sequence
 *         for draining out both the baths and tubings is completed
 */
/******************************************************************************/
#define DELFINO_DRAIN_ALL_COMPLETED_CMD                 	0x2009

/******************************************************************************/
/*!
 * \def    DELFINO_CLEAN_BATH_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the sequence
 *         for cleaning both the baths is completed
 */
/******************************************************************************/
#define DELFINO_CLEAN_BATH_COMPLETED_CMD                	0x200A

/*!---------------Valve Test Commands------------------*/

/******************************************************************************/
/*!
 * \def    DELFINO_VALVE_TEST_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the testing
 *         of each valve is completed
 */
/******************************************************************************/
#define DELFINO_VALVE_TEST_COMPLETED_CMD					0x200B
/*!--------------System Status Commands----------------*/
/******************************************************************************/
/*!
 * \def    DELFINO_PRESSURE_CALIBRATED_DATA
 * \brief  The sub command from the controller to send the pressure calibrated
 *          value to sitara
 */
/******************************************************************************/
#define DELFINO_PRESSURE_CALIBRATED_DATA                    0x2004

/******************************************************************************/
/*!
 * \def    DELFINO_SYSTEM_STATUS_DATA
 * \brief  The sub command from the controller to monitor the system status such
 *         as pressure,temperature,hgb blank,24V,5V and waste bin status
 */
/******************************************************************************/
#define DELFINO_SYSTEM_STATUS_DATA							0x2029 //0x200C //PH_CHANGE_SAMPLE_START_BUTTON

/******************************************************************************/
/*!
 * \def    DELFINO_START_UP_SEQUENCE_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that START-UP
 *         sequence is completed.
 *
 */
/******************************************************************************/
#define DELFINO_START_UP_SEQUENCE_COMPLETED_CMD				0x200E

/******************************************************************************/
/*!
 * \def    DELFINO_HEAD_RINSING_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that HEAD RINSING
 *         sequence is completed.
 *
 */
/******************************************************************************/
#define DELFINO_HEAD_RINSING_COMPLETED_CMD					0x200F

/******************************************************************************/
/*!
 * \def    DELFINO_PROBE_CLEANING_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that PROBE CLEANING
 *         sequence is completed.
 *
 */
/******************************************************************************/
#define DELFINO_PROBE_CLEANING_COMPLETED_CMD				0x2010

/******************************************************************************/
/*!
 * \def    DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that X AXIS MOTOR
 *         CHECK sequence is completed.
 *
 */
/******************************************************************************/
#define DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD				0x2011

/******************************************************************************/
/*!
 * \def    DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that Y AXIS MOTOR
 *         CHECK sequence is completed.
 *
 */
/******************************************************************************/
#define DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD				0x2012

/******************************************************************************/
/*!
 * \def    DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that DILUENT SYRINGE
 *         MOTOR CHECK sequence is completed.
 *
 */
/******************************************************************************/
#define DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD		0x2013

/******************************************************************************/
/*!
 * \def    DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that WASTE SYRINGE
 *         MOTOR CHECK sequence is completed.
 *
 */
/******************************************************************************/
#define DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD		0x2014

/******************************************************************************/
/*!
 * \def    DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that SAMPLE SYRINGE
 *         MOTOR CHECK sequence is completed.
 *
 */
/******************************************************************************/
#define DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD	0x2015

/******************************************************************************/
/*!
 * \def    DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that COUNTING TIME
 *         sequence is completed.
 *
 */
/******************************************************************************/
#define DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD		0x2016

/******************************************************************************/
/*!
 * \def    DELFINO_SENSOR_CHECK
 * \brief  The sub command from the controller to check the sensors TBD
 */
/******************************************************************************/
#define DELFINO_SENSOR_CHECK                                0x2017

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_WITH_RINSE_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the PRIME WITH
 *         RINSE sequence is started
 */
/******************************************************************************/
#define DELFINO_PRIME_WITH_RINSE_STARTED_CMD                0x2018

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_WITH_LYSE_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the PRIME WITH
 *         LYSE sequence is started
 */
/******************************************************************************/
#define DELFINO_PRIME_WITH_LYSE_STARTED_CMD                 0x2019

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_WITH_DILUENT_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the PRIME WITH
 *         DILUENT sequence is started
 */
/******************************************************************************/
#define DELFINO_PRIME_WITH_DILUENT_STARTED_CMD              0x201A

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_ALL_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the prime with
 *         all reagents sequence is started
 */
/******************************************************************************/
#define DELFINO_PRIME_ALL_STARTED_CMD                       0x201B

/******************************************************************************/
/*!
 * \def    DELFINO_BACK_FLUSH_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the backflush
 *         sequence is started
 */
/******************************************************************************/
#define DELFINO_BACK_FLUSH_STARTED_CMD                      0x201C

/******************************************************************************/
/*!
 * \def    DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the prime with
 *         EZ cleaner sequence is started
 */
/******************************************************************************/
#define DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD          0x201D

/******************************************************************************/
/*!
 * \def    DELFINO_ZAP_APERTURE_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the electrode
 *         zapping sequence is started
 */
/******************************************************************************/
#define DELFINO_ZAP_APERTURE_STARTED_CMD                    0x201E

/******************************************************************************/
/*!
 * \def    DELFINO_DRAIN_BATH_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the drain bath
 *         sequence is started
 */
/******************************************************************************/
#define DELFINO_DRAIN_BATH_STARTED_CMD                      0x201F

/******************************************************************************/
/*!
 * \def    DELFINO_DRAIN_ALL_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the drain all
 *         sequence is started
 */
/******************************************************************************/
#define DELFINO_DRAIN_ALL_STARTED_CMD                       0x2020

/******************************************************************************/
/*!
 * \def    DELFINO_CLEAN_BATH_STARTED_CMD
 * \brief  The sub command from the controller to indicate that the clean baths
 *         sequence is started
 */
/******************************************************************************/
#define DELFINO_CLEAN_BATH_STARTED_CMD                      0x2021

/******************************************************************************/
/*!
 * \def    DELFINO_WAKE_UP_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that the WAKE-UP
 *         sequence is completed
 */
/******************************************************************************/
#define DELFINO_WAKE_UP_COMPLETED_CMD                       0x2022 //SKM_WAKEUP

/******************************************************************************/
/*!
 * \def    DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD
 * \brief  The sub command from the controller to indicate that X AXIS MOTOR
 *         CHECK sequence is started
 */
/******************************************************************************/
#define DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD                0x2023

/******************************************************************************/
/*!
 * \def    DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD
 * \brief  The sub command from the controller to indicate that Y AXIS MOTOR
 *         CHECK sequence is started
 */
/******************************************************************************/
#define DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD                0x2024

/******************************************************************************/
/*!
 * \def    DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD
 * \brief  The sub command from the controller to indicate that DILUENT SYRINGE
 *         MOTOR CHECK sequence is started
 */
/******************************************************************************/
#define DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD       0x2025

/******************************************************************************/
/*!
 * \def    DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD
 * \brief  The sub command from the controller to indicate that WASTE SYRINGE
 *         MOTOR CHECK sequence is started
 */
/******************************************************************************/
#define DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD         0x2026

/******************************************************************************/
/*!
 * \def    DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD
 * \brief  The sub command from the controller to indicate that SAMPLE SYRINGE
 *         MOTOR CHECK sequence is started
 */
/******************************************************************************/
#define DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD   0x2027

/******************************************************************************/
/*!
 * \def    DELFINO_HOME_POSITION_SENSOR_CHECK
 * \brief  The sub command from the controller for HOME POSITION SENSOR CHECK
 */
/******************************************************************************/
#define DELFINO_HOME_POSITION_SENSOR_CHECK                  0x2028

/******************************************************************************/
/*!
 * \def    DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD
 * \brief  The sub command from the controller to indicate that COUNTING TIME
 *         sequence has started
 *
 */
/******************************************************************************/
#define DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD        0x202A//SKM_CHANGE_COUNTSTART

/******************************************************************************/
/*!
 * \def    DELFINO_START_UP_SEQUENCE_STARTED_CMD
 * \brief  The sub command from the controller to indicate that Start up
 *         sequence has started
 *
 */
/******************************************************************************/
#define DELFINO_START_UP_SEQUENCE_STARTED_CMD             0x202D //HN_Change 0x202B

/******************************************************************************/
/*!
 * \def    DELFINO_VOLUMETRIC_CHECK_STARTED
 * \brief  The sub command from the controller to indicate that volumetric board
 *         check sequence has started
 *
 */
/******************************************************************************/
#define DELFINO_VOLUMETRIC_CHECK_STARTED                0x202E

/******************************************************************************/
/*!
 * \def    DELFINO_VOLUMETRIC_CHECK_COMPLETED
 * \brief  The sub command from the controller to indicate that voluemtric board
 *         check sequence has completed
 *
 */
/******************************************************************************/
#define DELFINO_VOLUMETRIC_CHECK_COMPLETED             0x202F

/******************************************************************************/
/*!
 * \def    DELFINO_SLEEP_SEQUENCE_STARTED
 * \brief  The sub command from the controller to indicate that sleep
 *         sequence has started
 *
 */
/******************************************************************************/
#define DELFINO_SLEEP_SEQUENCE_STARTED                0x2030

/******************************************************************************/
/*!
 * \def    DELFINO_SLEEP_SEQUENCE_COMPLETED
 * \brief  The sub command from the controller to indicate that sleep
 *         sequence has completed
 *
 */
/******************************************************************************/
#define DELFINO_SLEEP_SEQUENCE_COMPLETED             0x2031

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_Y_AXIS_CHECK_SEQUENCE_STARTED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Started
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_Y_AXIS_CHECK_SEQUENCE_STARTED         0x2032

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_Y_AXIS_CHECK_SEQUENCE_COMPLETED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Completed
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_Y_AXIS_CHECK_SEQUENCE_COMPLETED       0x2033

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_X_AXIS_CHECK_SEQUENCE_STARTED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Started
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_X_AXIS_CHECK_SEQUENCE_STARTED         0x2034

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_X_AXIS_CHECK_SEQUENCE_COMPLETED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Completed
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_X_AXIS_CHECK_SEQUENCE_COMPLETED       0x2035

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_SAMPLE_CHECK_SEQUENCE_STARTED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Started
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_SAMPLE_CHECK_SEQUENCE_STARTED         0x2036

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_SAMPLE_CHECK_SEQUENCE_COMPLETED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Completed
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_SAMPLE_CHECK_SEQUENCE_COMPLETED       0x2037

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_DILUENT_CHECK_SEQUENCE_STARTED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Started
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_DILUENT_CHECK_SEQUENCE_STARTED        0x2038

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_DILUENT_CHECK_SEQUENCE_COMPLETED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Completed
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_DILUENT_CHECK_SEQUENCE_COMPLETED      0x2039

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_WASTE_CHECK_SEQUENCE_STARTED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Started
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_WASTE_CHECK_SEQUENCE_STARTED          0x203A

/******************************************************************************/
/*!
 * \def    DELFINO_ALL_MOTOR_WSATE_CHECK_SEQUENCE_COMPLETED
 * \brief  The sub command from the controller to indicate that All Motor
 *          check for Y-Axis Completed
 */
/******************************************************************************/
#define DELFINO_ALL_MOTOR_WSATE_CHECK_SEQUENCE_COMPLETED        0x203B

/******************************************************************************/
/*!
 * \def    DELFINO_VOL_TUBE_EMPTY_SEQUENCE_STARTED
 * \brief  The sub command from the controller to indicate that Volumetric
 *          tube empty sequence started
 */
/******************************************************************************/
#define DELFINO_VOL_TUBE_EMPTY_SEQUENCE_STARTED                 0x203C

/******************************************************************************/
/*!
 * \def    DELFINO_VOL_TUBE_EMPTY_SEQUENCE_COMPLETED
 * \brief  The sub command from the controller to indicate that Volumetric
 *          tube empty sequence Completed
 */
/******************************************************************************/
#define DELFINO_VOL_TUBE_EMPTY_SEQUENCE_COMPLETED               0x203D

/******************************************************************************/
/*!
 * \def    DELFINO_WAKEUP_SEQUENCE_STARTED
 * \brief  The sub command from the controller to indicate that wakeup
 *         sequence has started
 *
 */
/******************************************************************************/
#define DELFINO_WAKEUP_SEQUENCE_STARTED                          0x203E

/******************************************************************************/
/*!
 * \def    DELFINO_WASTE_BIN_FULL
 * \brief  The sub command from the controller to indicate that waste bin full is set
 *
 */
/******************************************************************************/
#define DELFINO_SET_WASTE_BIN_FULL                                  0x2043
/******************************************************************************/
/*!
 * \def    DELFINO_CLEAR_WASTE_BIN_FULL
 * \brief  The sub command from the controller to indicate that waste bin full is cleared
 *
 */
/******************************************************************************/
#define DELFINO_CLEAR_WASTE_BIN_FULL                                 0x2044

/******************************************************************************/
/*!
 * \def    DELFINO_SPI_ERROR_CHECK
 * \brief  The sub command from the controller to SPI communication line is fine
 *
 */
/******************************************************************************/
#define DELFINO_SPI_ERROR_CHECK                                  0x2222 //Changed on(7/1/19) 0x203F
//------------------------------------------------------------------------

/*!------------DELFINO FIRMWARE VERSION SUB COMMAND-----------------*/

/******************************************************************************/
/*!
 * \def    DELFINO_HOME_POSITION_SENSOR_CHECK
 * \brief  The sub command for CPU1_CPU2 Version from the controller
 */
/******************************************************************************/
#define DELFINO_CPU1_CPU2_VERSION						    0x2001
//CRH_SEQUENCE_STATE - START
/*!------------DELFINO SHUTDOWN SEQUENCE SUB COMMAND-----------------*/

/******************************************************************************/
/*!
 * \def    DELFINO_HOME_POSITION_SENSOR_CHECK
 * \brief  The sub command for SHUT-DOWN PROCESS from the controller
 */
/******************************************************************************/
#define DELFINO_SHUTDOWN_SEQUENCE_COMPLETED_CMD             0x2301
/******************************************************************************/
/*!
 * \def    DELFINO_BATH_FILL_SEQUENCE_STARTED_CMD
 * \brief  The sub command from the controller to send the bath
 *  filling started to sitara
 */
/******************************************************************************/
#define DELFINO_BATH_FILL_SEQUENCE_STARTED_CMD               0x203F//0x2305 //HN_added(4/01/19)

/******************************************************************************/
/*!
 * \def    DELFINO_BATH_FILL_SEQUENCE_COMPLETED_CMD
 * \brief  The sub command from the controller to send bath
 * fill cpmted cmd to sitara
 */
/******************************************************************************/
#define DELFINO_BATH_FILL_SEQUENCE_COMPLETED_CMD             0x2040 //0x2306 //HN_added(4/01/19)
//CRH_SEQUENCE_STATE - END

//PH_NOV_02 - START
/*!------------DELFINO MONITOR SEQUENCE SUB COMMAND-----------------*/

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_SEQUENCE_STARTED
 * \brief  The sub command from the controller to monitor that the sequence is
 *         started
 */
/******************************************************************************/
#define DELFINO_STATE_SEQUENCE_STARTED                      0x2001

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_READY_FOR_DISPENSING
 * \brief  The sub command from the controller to monitor that the needle is
 *         ready for dispensation
 */
/******************************************************************************/
#define DELFINO_STATE_READY_FOR_DISPENSING                  0x2002

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_DISPENSE_COMPLETED
 * \brief  The sub command from the controller to monitor that the needle
 *         dispensation is completed
 */
/******************************************************************************/
#define DELFINO_STATE_DISPENSE_COMPLETED                    0x2003

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_READY_FOR_ASPIRATION
 * \brief  The sub command from the controller to monitor that the needle
 *         is ready for aspiration.
 */
/******************************************************************************/
#define DELFINO_STATE_READY_FOR_ASPIRATION                  0x2004

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_ASPIRATION_COMPLETED
 * \brief  The sub command from the controller to monitor that the aspiration
 *         of sample is completed
 */
/******************************************************************************/
#define DELFINO_STATE_ASPIRATION_COMPLETED                  0x2005

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_WBC_DISPENSATION_COMPLETED
 * \brief  The sub command from the controller to monitor that the dispensation
 *         of sample with diluent in WBC bath is completed.
 */
/******************************************************************************/
#define DELFINO_STATE_WBC_DISPENSATION_COMPLETED            0x2006

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_RBC_DISPENSATION_COMPLETED
 * \brief  The sub command from the controller to monitor that the aspiration
 *         of sample is completed
 */
/******************************************************************************/
#define DELFINO_STATE_RBC_DISPENSATION_COMPLETED            0x2007

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_WBC_RBC_BUBBLING_COMPLETED
 * \brief  The sub command from the controller to monitor that the bubbling
 *         at RBC and WBC are completed.
 */
/******************************************************************************/
#define DELFINO_STATE_WBC_RBC_BUBBLING_COMPLETED            0x2008

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_COUNTING_STARTED
 * \brief  The sub command from the controller to monitor that the acquisition
 *         of data is started
 */
/******************************************************************************/
#define DELFINO_STATE_COUNTING_STARTED                      0x2009

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_COUNTING_COMPLETED
 * \brief  The sub command from the controller to monitor that the acquisition
 *         of data is completed
 */
/******************************************************************************/
#define DELFINO_STATE_COUNTING_COMPLETED                    0x200A

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_BACKFLUSH_COMPLETED
 * \brief  The sub command from the controller to monitor that the backflush
 *         after counting is completed
 */
/******************************************************************************/
#define DELFINO_STATE_BACKFLUSH_COMPLETED                   0x200B

/******************************************************************************/
/*!
 * \def    DELFINO_STATE_BATH_FILLING_COMPLETED
 * \brief  The sub command from the controller to monitor that the both the
 *         bath filling after counting is completed
 */
/******************************************************************************/
#define DELFINO_STATE_BATH_FILLING_COMPLETED                0x200C

/*!------------DELFINO ABORT SEQUENCE SUB COMMAND-----------------*/

/******************************************************************************/
/*!
 * \def    DELFINO_ABORT_SEQUENCE_STARTED_CMD
 * \brief  The sub command from the controller to monitor that the abort
 *         sequence is completed
 */
/******************************************************************************/
#define DELFINO_ABORT_SEQUENCE_STARTED_CMD                 0x2301

/******************************************************************************/
/*!
 * \def    DELFINO_ABORT_SEQUENCE_COMPLETED_CMD
 * \brief  The sub command from the controller to monitor that the abort
 *         sequence is completed
 */
/******************************************************************************/
#define DELFINO_ABORT_SEQUENCE_COMPLETED_CMD                0x2302

/******************************************************************************/
/*!
 * \def    DELFINO_ABORT_SEQUENCE_STARTED_CMD
 * \brief  The sub command from the controller to monitor that the abort
 *         sequence is completed
 */
/******************************************************************************/
#define DELFINO_Y_AXIS_ABORT_SEQUENCE_STARTED_CMD           0x2303

/******************************************************************************/
/*!
 * \def    DELFINO_Y_AXIS_ABORT_SEQUENCE_COMPLETED_CMD
 * \brief  The sub command from the controller to monitor that the abort
 *         sequence is completed
 */
/******************************************************************************/
#define DELFINO_Y_AXIS_ABORT_SEQUENCE_COMPLETED_CMD         0x2304

#define DELFINO_ZAP_INITIATE_STARTED_CMD                    0x2041
#define DELFINO_ZAP_INITIATE_COMPLETED_CMD                  0x2042

#define GLASS_TUBE_FILLING_STARTED                          0x2045 //added by Jojo
#define GLASS_TUBE_FILLING_COMPLETED                        0x2046 //added by Jojo
#define COUNT_FAIL_SEQUNCE_STARTED                          0x2047 //added by Jojo
#define COUNT_FAIL_SEQUNCE_COMPLETED                        0x2048 //added by Jojo
#define HYPO_CLEANER_FAIL_SEQUNCE_STARTED                   0x2049 //added by Jojo
#define HYPO_CLEANER_FAIL_SEQUNCE_COMPLETED                 0x2050 //added by Jojo



/*!------------DELFINO ERROR HANDLING SUB COMMAND-----------------*/

/******************************************************************************/
/*!
 * \def    DELFINO_ERROR_MINOR_CMD
 * \brief  The sub command for error handling from the controller
 *
 */
/******************************************************************************/
#define DELFINO_ERROR_MINOR_CMD                             0x2401

/******************************************************************************/
/*!
 * \def    DELFINO_ERROR_MINOR_CMD
 * \brief  The sub command for error handling from the controller
 *
 */
/******************************************************************************/
#define DELFINO_FLOW_CALIBRATION_CMD                        0x2003

/******************************************************************************/
/*!
 * \def    DELFINO_FLOW_CALIBRATION_STARTED
 * \brief  The sub command for indicating flow calibration from the controller
 *
 */
/******************************************************************************/
#define DELFINO_FLOW_CALIBRATION_STARTED                    0x2002

/******************************************************************************/
/*!
 * \def    DELFINO_RINSE_PRIME_COMPLETED
 * \brief  The sub command for indicating flow calibration rinse prime
 *         completed from the controller
 *
 */
/******************************************************************************/
#define DELFINO_RINSE_PRIME_COMPLETED                       0x2001

/******************************************************************************/
/*!
 * \def    DELFINO_TO_SHIP_STARTED
 * \brief  The sub command for indicating To_ship sequence is
 *         started from the controller
 *
 */
/******************************************************************************/
#define DELFINO_TO_SHIP_STARTED                            0x202B  //HN_Added

/******************************************************************************/
/*!
 * \def    DELFINO_TO_SHIP_COMPLETED_CMD
 * \brief  The sub command for indicating To_ship sequence is
 *         completed from the controller
 *
 */
/******************************************************************************/
#define DELFINO_TO_SHIP_COMPLETED_CMD                        0x202C     //HN_Added

//------------------------------------------------------------------------
#endif /* COMMANDS_H_ */

;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Fri Jan 29 15:14:27 2021                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../source/InitGpio.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cell Counter\V1.3.1_And_V1.3.0\alphaCellCounterCPU1\CPU1_FLASH")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_stLEDToggleCounter$2+0,32
	.bits	0,16			; _stLEDToggleCounter$2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_stLEDToggle$1+0,32
	.bits	1,16			; _stLEDToggle$1 @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("GPIO_SetupPinOptions")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_GPIO_SetupPinOptions")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$19)

$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$19)

$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$19)

	.dwendtag $C$DW$1


$C$DW$5	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$5, DW_AT_name("GPIO_SetupPinMux")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_GPIO_SetupPinMux")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$19)

$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$19)

$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$19)

	.dwendtag $C$DW$5


$C$DW$9	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$9, DW_AT_name("GPIO_WritePin")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_GPIO_WritePin")
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$19)

$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$19)

	.dwendtag $C$DW$9


$C$DW$12	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$12, DW_AT_name("InitGpio")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_InitGpio")
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
	.dwendtag $C$DW$12

	.global	_usnSensor1_HomeState
_usnSensor1_HomeState:	.usect	".ebss",1,1,0
$C$DW$13	.dwtag  DW_TAG_variable
	.dwattr $C$DW$13, DW_AT_name("usnSensor1_HomeState")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_usnSensor1_HomeState")
	.dwattr $C$DW$13, DW_AT_location[DW_OP_addr _usnSensor1_HomeState]
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$13, DW_AT_external

_stLEDToggleCounter$2:	.usect	".ebss",1,1,0
_stLEDToggle$1:	.usect	".ebss",1,1,0

$C$DW$14	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$14, DW_AT_name("GPIO_ReadPin")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_GPIO_ReadPin")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$19)

	.dwendtag $C$DW$14


$C$DW$16	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$16, DW_AT_name("OIHomeSensor1State")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_OIHomeSensor1State")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
	.dwendtag $C$DW$16

;	D:\Softwares\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\DITTOM~1.DEV\\AppData\\Local\\Temp\\1712012 
	.sect	".text:_IGInitialize_Gpio"
	.clink
	.global	_IGInitialize_Gpio

$C$DW$17	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$17, DW_AT_name("IGInitialize_Gpio")
	.dwattr $C$DW$17, DW_AT_low_pc(_IGInitialize_Gpio)
	.dwattr $C$DW$17, DW_AT_high_pc(0x00)
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_IGInitialize_Gpio")
	.dwattr $C$DW$17, DW_AT_external
	.dwattr $C$DW$17, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$17, DW_AT_TI_begin_line(0x43)
	.dwattr $C$DW$17, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$17, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/InitGpio.c",line 68,column 1,is_stmt,address _IGInitialize_Gpio,isa 0

	.dwfde $C$DW$CIE, _IGInitialize_Gpio
;----------------------------------------------------------------------
;  67 | void IGInitialize_Gpio(void)                                           
;  70 | //!To initialize the GPIO ports                                        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGInitialize_Gpio            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_IGInitialize_Gpio:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/InitGpio.c",line 71,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  71 | InitGpio();                                                            
;  72 | //!To initialize the GPIO ports related to LED                         
;----------------------------------------------------------------------
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_name("_InitGpio")
	.dwattr $C$DW$18, DW_AT_TI_call

        LCR       #_InitGpio            ; [CPU_] |71| 
        ; call occurs [#_InitGpio] ; [] |71| 
	.dwpsn	file "../source/InitGpio.c",line 73,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  73 | IGGpioConfig_LED();                                                    
;  74 | //!To initialize the GPIO ports related to High voltage pins used to ex
;     | cite                                                                   
;  75 | //!during data acquisition                                             
;----------------------------------------------------------------------
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("_IGGpioConfig_LED")
	.dwattr $C$DW$19, DW_AT_TI_call

        LCR       #_IGGpioConfig_LED    ; [CPU_] |73| 
        ; call occurs [#_IGGpioConfig_LED] ; [] |73| 
	.dwpsn	file "../source/InitGpio.c",line 76,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  76 | IGGpioConfig_HighVoltPin();                                            
;  77 | //!To initialize the GPIO ports related to Cycle counter pins to increm
;     | ent                                                                    
;  78 | //!counter when whole blood sequence is running - presently not used   
;----------------------------------------------------------------------
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_name("_IGGpioConfig_HighVoltPin")
	.dwattr $C$DW$20, DW_AT_TI_call

        LCR       #_IGGpioConfig_HighVoltPin ; [CPU_] |76| 
        ; call occurs [#_IGGpioConfig_HighVoltPin] ; [] |76| 
	.dwpsn	file "../source/InitGpio.c",line 79,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  79 | IGGpioConfig_CycleCounterPin();                                        
;  81 | //!To initialize the GPIO ports related to PWM valve control           
;----------------------------------------------------------------------
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("_IGGpioConfig_CycleCounterPin")
	.dwattr $C$DW$21, DW_AT_TI_call

        LCR       #_IGGpioConfig_CycleCounterPin ; [CPU_] |79| 
        ; call occurs [#_IGGpioConfig_CycleCounterPin] ; [] |79| 
	.dwpsn	file "../source/InitGpio.c",line 82,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  82 | IGGpioConfig_ValvePwm();                                               
;  83 | //!To initialize the GPIO ports related to PWM of HgB                  
;----------------------------------------------------------------------
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("_IGGpioConfig_ValvePwm")
	.dwattr $C$DW$22, DW_AT_TI_call

        LCR       #_IGGpioConfig_ValvePwm ; [CPU_] |82| 
        ; call occurs [#_IGGpioConfig_ValvePwm] ; [] |82| 
	.dwpsn	file "../source/InitGpio.c",line 84,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  84 | IGGpioConfig_HgbPwm();                                                 
;  86 | //!To initialize the GPIO ports related to SPI Communication pins      
;----------------------------------------------------------------------
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("_IGGpioConfig_HgbPwm")
	.dwattr $C$DW$23, DW_AT_TI_call

        LCR       #_IGGpioConfig_HgbPwm ; [CPU_] |84| 
        ; call occurs [#_IGGpioConfig_HgbPwm] ; [] |84| 
	.dwpsn	file "../source/InitGpio.c",line 87,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  87 | IGGpioConfig_SpiComm();                                                
;  88 | //!To initialize the GPIO ports related to SPI communciation control bu
;     | sy pin                                                                 
;----------------------------------------------------------------------
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("_IGGpioConfig_SpiComm")
	.dwattr $C$DW$24, DW_AT_TI_call

        LCR       #_IGGpioConfig_SpiComm ; [CPU_] |87| 
        ; call occurs [#_IGGpioConfig_SpiComm] ; [] |87| 
	.dwpsn	file "../source/InitGpio.c",line 89,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  89 | IGGpioConfig_SpiCommControlPin();                                      
;  91 | //!To initialize the GPIO ports related to SPI valve control pins      
;----------------------------------------------------------------------
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("_IGGpioConfig_SpiCommControlPin")
	.dwattr $C$DW$25, DW_AT_TI_call

        LCR       #_IGGpioConfig_SpiCommControlPin ; [CPU_] |89| 
        ; call occurs [#_IGGpioConfig_SpiCommControlPin] ; [] |89| 
	.dwpsn	file "../source/InitGpio.c",line 92,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  92 | IGGpioConfig_SpiValve();                                               
;  94 | //!To initialize the GPIO ports related to Sample start button         
;----------------------------------------------------------------------
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_IGGpioConfig_SpiValve")
	.dwattr $C$DW$26, DW_AT_TI_call

        LCR       #_IGGpioConfig_SpiValve ; [CPU_] |92| 
        ; call occurs [#_IGGpioConfig_SpiValve] ; [] |92| 
	.dwpsn	file "../source/InitGpio.c",line 95,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  95 | IGGpioConfig_SampleStartButton();                                      
;  97 | //!To initialize the GPIO ports related to Waste full detect pin       
;----------------------------------------------------------------------
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("_IGGpioConfig_SampleStartButton")
	.dwattr $C$DW$27, DW_AT_TI_call

        LCR       #_IGGpioConfig_SampleStartButton ; [CPU_] |95| 
        ; call occurs [#_IGGpioConfig_SampleStartButton] ; [] |95| 
	.dwpsn	file "../source/InitGpio.c",line 98,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  98 | IGGpioConfig_WasteDetectPin();//SKM_WASTE_DETECT_CHANGE                
;  99 | #ifdef IO_EXPANDER_USED                                                
; 100 | IGGpioConfig_SpiHomeSense();                                           
; 101 | #else                                                                  
; 102 | //!To initialize the GPIO ports related to Home position detector pins 
;----------------------------------------------------------------------
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_IGGpioConfig_WasteDetectPin")
	.dwattr $C$DW$28, DW_AT_TI_call

        LCR       #_IGGpioConfig_WasteDetectPin ; [CPU_] |98| 
        ; call occurs [#_IGGpioConfig_WasteDetectPin] ; [] |98| 
	.dwpsn	file "../source/InitGpio.c",line 103,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 103 | IGInit_GPIO_HomePositionSensor();                                      
; 104 | #endif                                                                 
; 106 | //!To initialize the GPIO ports related to UART communication pins     
;----------------------------------------------------------------------
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("_IGInit_GPIO_HomePositionSensor")
	.dwattr $C$DW$29, DW_AT_TI_call

        LCR       #_IGInit_GPIO_HomePositionSensor ; [CPU_] |103| 
        ; call occurs [#_IGInit_GPIO_HomePositionSensor] ; [] |103| 
	.dwpsn	file "../source/InitGpio.c",line 107,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 107 | IGGpioConfig_UartComm();                                               
; 109 | //!To initialize the GPIOs used to default state                       
;----------------------------------------------------------------------
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("_IGGpioConfig_UartComm")
	.dwattr $C$DW$30, DW_AT_TI_call

        LCR       #_IGGpioConfig_UartComm ; [CPU_] |107| 
        ; call occurs [#_IGGpioConfig_UartComm] ; [] |107| 
	.dwpsn	file "../source/InitGpio.c",line 110,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 110 | IGGpioConfig_DefaultState();                                           
;----------------------------------------------------------------------
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_name("_IGGpioConfig_DefaultState")
	.dwattr $C$DW$31, DW_AT_TI_call

        LCR       #_IGGpioConfig_DefaultState ; [CPU_] |110| 
        ; call occurs [#_IGGpioConfig_DefaultState] ; [] |110| 
	.dwpsn	file "../source/InitGpio.c",line 112,column 1,is_stmt,isa 0
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$17, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$17, DW_AT_TI_end_line(0x70)
	.dwattr $C$DW$17, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$17

	.sect	".text:_IGGpioConfig_ValvePwm"
	.clink
	.global	_IGGpioConfig_ValvePwm

$C$DW$33	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$33, DW_AT_name("IGGpioConfig_ValvePwm")
	.dwattr $C$DW$33, DW_AT_low_pc(_IGGpioConfig_ValvePwm)
	.dwattr $C$DW$33, DW_AT_high_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_IGGpioConfig_ValvePwm")
	.dwattr $C$DW$33, DW_AT_external
	.dwattr $C$DW$33, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$33, DW_AT_TI_begin_line(0x7d)
	.dwattr $C$DW$33, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$33, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/InitGpio.c",line 126,column 1,is_stmt,address _IGGpioConfig_ValvePwm,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_ValvePwm
;----------------------------------------------------------------------
; 125 | void IGGpioConfig_ValvePwm(void)                                       
; 128 | GpioPinConfig GpioSetupValvePwm;                                       
; 130 | //!Set the following parameters of the PWM valve GPIO pin:             
; 131 | //!GPIO pin no. to S_PWM_VALVE                                         
; 132 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 133 | //!GPIO Pin as output pin                                              
; 134 | //!GPIO pin as GPIO_PUSHPULL                                           
; 135 | //!GPIO peripheral as high                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_ValvePwm        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_ValvePwm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$34	.dwtag  DW_TAG_variable
	.dwattr $C$DW$34, DW_AT_name("GpioSetupValvePwm")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_GpioSetupValvePwm")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -5]

	.dwpsn	file "../source/InitGpio.c",line 136,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 136 | GpioSetupValvePwm.GpioPin = S_PWM_VALVE;                               
;----------------------------------------------------------------------
        MOVB      *-SP[5],#151,UNC      ; [CPU_] |136| 
	.dwpsn	file "../source/InitGpio.c",line 137,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 137 | GpioSetupValvePwm.GpioCpuMuxSel = GPIO_MUX_CPU1;                       
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |137| 
	.dwpsn	file "../source/InitGpio.c",line 138,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 138 | GpioSetupValvePwm.GpioInputOutput = GPIO_OUTPUT;                       
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |138| 
	.dwpsn	file "../source/InitGpio.c",line 139,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 139 | GpioSetupValvePwm.GpioFlags = GPIO_PUSHPULL;                           
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |139| 
	.dwpsn	file "../source/InitGpio.c",line 140,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 140 | GpioSetupValvePwm.GpioPeripheral = S_PWM_VALVE_PERIPHERAL;             
; 142 | //!After setting all the configurations of the pin,                    
; 143 | //!Update the configurations of the PWM valve GPIO                     
;----------------------------------------------------------------------
        MOVB      *-SP[1],#1,UNC        ; [CPU_] |140| 
	.dwpsn	file "../source/InitGpio.c",line 144,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 144 | IGConfigGpioPin(GpioSetupValvePwm);                                    
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |144| 
        SUBB      XAR4,#5               ; [CPU_U] |144| 
        MOVZ      AR4,AR4               ; [CPU_] |144| 
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$35, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |144| 
        ; call occurs [#_IGConfigGpioPin] ; [] |144| 
	.dwpsn	file "../source/InitGpio.c",line 147,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$33, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$33, DW_AT_TI_end_line(0x93)
	.dwattr $C$DW$33, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$33

	.sect	".text:_IGInit_GPIO_HomePositionSensor"
	.clink
	.global	_IGInit_GPIO_HomePositionSensor

$C$DW$37	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$37, DW_AT_name("IGInit_GPIO_HomePositionSensor")
	.dwattr $C$DW$37, DW_AT_low_pc(_IGInit_GPIO_HomePositionSensor)
	.dwattr $C$DW$37, DW_AT_high_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_IGInit_GPIO_HomePositionSensor")
	.dwattr $C$DW$37, DW_AT_external
	.dwattr $C$DW$37, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$37, DW_AT_TI_begin_line(0xa0)
	.dwattr $C$DW$37, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$37, DW_AT_TI_max_frame_size(-28)
	.dwpsn	file "../source/InitGpio.c",line 161,column 1,is_stmt,address _IGInit_GPIO_HomePositionSensor,isa 0

	.dwfde $C$DW$CIE, _IGInit_GPIO_HomePositionSensor
;----------------------------------------------------------------------
; 160 | void IGInit_GPIO_HomePositionSensor(void)                              
; 162 | GpioPinConfig GpioSetupHomePosGpio[5];                                 
; 164 | //!Set the following parameters of the Home position 1 GPIO pin:       
; 165 | //!GPIO pin no. to GPIO_HOME_POS1                                      
; 166 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 167 | //!GPIO Pin as input pin                                               
; 168 | //!GPIO pin as GPIO_QUAL6                                              
; 169 | //!GPIO peripheral as low                                              
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGInit_GPIO_HomePositionSensor FR SIZE:  26           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 25 Auto,  0 SOE     *
;***************************************************************

_IGInit_GPIO_HomePositionSensor:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -28
$C$DW$38	.dwtag  DW_TAG_variable
	.dwattr $C$DW$38, DW_AT_name("GpioSetupHomePosGpio")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_GpioSetupHomePosGpio")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -25]

	.dwpsn	file "../source/InitGpio.c",line 170,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 170 | GpioSetupHomePosGpio[0].GpioPin = GPIO_HOME_POS1;                      
;----------------------------------------------------------------------
        MOVB      *-SP[25],#114,UNC     ; [CPU_] |170| 
	.dwpsn	file "../source/InitGpio.c",line 171,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 171 | GpioSetupHomePosGpio[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                 
;----------------------------------------------------------------------
        MOV       *-SP[23],#0           ; [CPU_] |171| 
	.dwpsn	file "../source/InitGpio.c",line 172,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 172 | GpioSetupHomePosGpio[0].GpioInputOutput = GPIO_INPUT;                  
;----------------------------------------------------------------------
        MOV       *-SP[22],#0           ; [CPU_] |172| 
	.dwpsn	file "../source/InitGpio.c",line 173,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 173 | GpioSetupHomePosGpio[0].GpioFlags = GPIO_QUAL6;                        
;----------------------------------------------------------------------
        MOVB      *-SP[24],#32,UNC      ; [CPU_] |173| 
	.dwpsn	file "../source/InitGpio.c",line 174,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 174 | GpioSetupHomePosGpio[0].GpioPeripheral = 0;                            
; 176 | //!Set the following parameters of the Home position 2 GPIO pin:       
; 177 | //!GPIO pin no. to GPIO_HOME_POS2                                      
; 178 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 179 | //!GPIO Pin as input pin                                               
; 180 | //!GPIO pin as GPIO_QUAL6                                              
; 181 | //!GPIO peripheral as low                                              
;----------------------------------------------------------------------
        MOV       *-SP[21],#0           ; [CPU_] |174| 
	.dwpsn	file "../source/InitGpio.c",line 182,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 182 | GpioSetupHomePosGpio[1].GpioPin = GPIO_HOME_POS2;                      
;----------------------------------------------------------------------
        MOVB      *-SP[20],#115,UNC     ; [CPU_] |182| 
	.dwpsn	file "../source/InitGpio.c",line 183,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 183 | GpioSetupHomePosGpio[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                 
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |183| 
	.dwpsn	file "../source/InitGpio.c",line 184,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 184 | GpioSetupHomePosGpio[1].GpioInputOutput = GPIO_INPUT;                  
;----------------------------------------------------------------------
        MOV       *-SP[17],#0           ; [CPU_] |184| 
	.dwpsn	file "../source/InitGpio.c",line 185,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 185 | GpioSetupHomePosGpio[1].GpioFlags = GPIO_QUAL6;                        
;----------------------------------------------------------------------
        MOVB      *-SP[19],#32,UNC      ; [CPU_] |185| 
	.dwpsn	file "../source/InitGpio.c",line 186,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 186 | GpioSetupHomePosGpio[1].GpioPeripheral = 0;                            
; 188 | //!Set the following parameters of the Home position 3 GPIO pin:       
; 189 | //!GPIO pin no. to GPIO_HOME_POS3                                      
; 190 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 191 | //!GPIO Pin as input pin                                               
; 192 | //!GPIO pin as GPIO_QUAL6                                              
; 193 | //!GPIO peripheral as low                                              
;----------------------------------------------------------------------
        MOV       *-SP[16],#0           ; [CPU_] |186| 
	.dwpsn	file "../source/InitGpio.c",line 194,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 194 | GpioSetupHomePosGpio[2].GpioPin = GPIO_HOME_POS3;                      
;----------------------------------------------------------------------
        MOVB      *-SP[15],#113,UNC     ; [CPU_] |194| 
	.dwpsn	file "../source/InitGpio.c",line 195,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 195 | GpioSetupHomePosGpio[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                 
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |195| 
	.dwpsn	file "../source/InitGpio.c",line 196,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 196 | GpioSetupHomePosGpio[2].GpioInputOutput = GPIO_INPUT;                  
;----------------------------------------------------------------------
        MOV       *-SP[12],#0           ; [CPU_] |196| 
	.dwpsn	file "../source/InitGpio.c",line 197,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 197 | GpioSetupHomePosGpio[2].GpioFlags = GPIO_QUAL6;                        
;----------------------------------------------------------------------
        MOVB      *-SP[14],#32,UNC      ; [CPU_] |197| 
	.dwpsn	file "../source/InitGpio.c",line 198,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 198 | GpioSetupHomePosGpio[2].GpioPeripheral = 0;                            
; 200 | //!Set the following parameters of the Home position 4 GPIO pin:       
; 201 | //!GPIO pin no. to GPIO_HOME_POS4                                      
; 202 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 203 | //!GPIO Pin as input pin                                               
; 204 | //!GPIO pin as GPIO_QUAL6                                              
; 205 | //!GPIO peripheral as low                                              
;----------------------------------------------------------------------
        MOV       *-SP[11],#0           ; [CPU_] |198| 
	.dwpsn	file "../source/InitGpio.c",line 206,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 206 | GpioSetupHomePosGpio[3].GpioPin = GPIO_HOME_POS4;                      
;----------------------------------------------------------------------
        MOVB      *-SP[10],#111,UNC     ; [CPU_] |206| 
	.dwpsn	file "../source/InitGpio.c",line 207,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 207 | GpioSetupHomePosGpio[3].GpioCpuMuxSel = GPIO_MUX_CPU1;                 
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |207| 
	.dwpsn	file "../source/InitGpio.c",line 208,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 208 | GpioSetupHomePosGpio[3].GpioInputOutput = GPIO_INPUT;                  
;----------------------------------------------------------------------
        MOV       *-SP[7],#0            ; [CPU_] |208| 
	.dwpsn	file "../source/InitGpio.c",line 209,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 209 | GpioSetupHomePosGpio[3].GpioFlags = GPIO_QUAL6;                        
;----------------------------------------------------------------------
        MOVB      *-SP[9],#32,UNC       ; [CPU_] |209| 
	.dwpsn	file "../source/InitGpio.c",line 210,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 210 | GpioSetupHomePosGpio[3].GpioPeripheral = 0;                            
; 212 | //!Set the following parameters of the Home position 5 GPIO pin:       
; 213 | //!GPIO pin no. to GPIO_HOME_POS5                                      
; 214 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 215 | //!GPIO Pin as input pin                                               
; 216 | //!GPIO pin as GPIO_QUAL6                                              
; 217 | //!GPIO peripheral as low                                              
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |210| 
	.dwpsn	file "../source/InitGpio.c",line 218,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 218 | GpioSetupHomePosGpio[4].GpioPin = GPIO_HOME_POS5;                      
;----------------------------------------------------------------------
        MOVB      *-SP[5],#112,UNC      ; [CPU_] |218| 
	.dwpsn	file "../source/InitGpio.c",line 219,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 219 | GpioSetupHomePosGpio[4].GpioCpuMuxSel = GPIO_MUX_CPU1;                 
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |219| 
	.dwpsn	file "../source/InitGpio.c",line 220,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 220 | GpioSetupHomePosGpio[4].GpioInputOutput = GPIO_INPUT;                  
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |220| 
	.dwpsn	file "../source/InitGpio.c",line 221,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 221 | GpioSetupHomePosGpio[4].GpioFlags = GPIO_QUAL6;                        
;----------------------------------------------------------------------
        MOVB      *-SP[4],#32,UNC       ; [CPU_] |221| 
	.dwpsn	file "../source/InitGpio.c",line 222,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 222 | GpioSetupHomePosGpio[4].GpioPeripheral = 0;                            
; 224 | //!After setting all the configurations of the pin,                    
; 225 | //!Update the configurations of the Home position GPIO                 
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |222| 
	.dwpsn	file "../source/InitGpio.c",line 226,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 226 | IGConfigGpioPin(GpioSetupHomePosGpio[0]);                              
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |226| 
        SUBB      XAR4,#25              ; [CPU_U] |226| 
        MOVZ      AR4,AR4               ; [CPU_] |226| 
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$39, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |226| 
        ; call occurs [#_IGConfigGpioPin] ; [] |226| 
	.dwpsn	file "../source/InitGpio.c",line 227,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 227 | IGConfigGpioPin(GpioSetupHomePosGpio[1]);                              
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |227| 
        SUBB      XAR4,#20              ; [CPU_U] |227| 
        MOVZ      AR4,AR4               ; [CPU_] |227| 
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$40, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |227| 
        ; call occurs [#_IGConfigGpioPin] ; [] |227| 
	.dwpsn	file "../source/InitGpio.c",line 228,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 228 | IGConfigGpioPin(GpioSetupHomePosGpio[2]);                              
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |228| 
        SUBB      XAR4,#15              ; [CPU_U] |228| 
        MOVZ      AR4,AR4               ; [CPU_] |228| 
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$41, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |228| 
        ; call occurs [#_IGConfigGpioPin] ; [] |228| 
	.dwpsn	file "../source/InitGpio.c",line 229,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 229 | IGConfigGpioPin(GpioSetupHomePosGpio[3]);                              
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |229| 
        SUBB      XAR4,#10              ; [CPU_U] |229| 
        MOVZ      AR4,AR4               ; [CPU_] |229| 
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$42, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |229| 
        ; call occurs [#_IGConfigGpioPin] ; [] |229| 
	.dwpsn	file "../source/InitGpio.c",line 230,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 230 | IGConfigGpioPin(GpioSetupHomePosGpio[4]);                              
; 232 | //! Home sensor 1 satus read. This check is used to set X motor in know
;     | n                                                                      
; 233 | //!position during startup                                             
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |230| 
        SUBB      XAR4,#5               ; [CPU_U] |230| 
        MOVZ      AR4,AR4               ; [CPU_] |230| 
$C$DW$43	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$43, DW_AT_low_pc(0x00)
	.dwattr $C$DW$43, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$43, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |230| 
        ; call occurs [#_IGConfigGpioPin] ; [] |230| 
	.dwpsn	file "../source/InitGpio.c",line 234,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 234 | usnSensor1_HomeState = OIHomeSensor1State();                           
;----------------------------------------------------------------------
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_name("_OIHomeSensor1State")
	.dwattr $C$DW$44, DW_AT_TI_call

        LCR       #_OIHomeSensor1State  ; [CPU_] |234| 
        ; call occurs [#_OIHomeSensor1State] ; [] |234| 
        MOVW      DP,#_usnSensor1_HomeState ; [CPU_U] 
        MOV       @_usnSensor1_HomeState,AL ; [CPU_] |234| 
	.dwpsn	file "../source/InitGpio.c",line 235,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 235 | if(usnSensor1_HomeState == 0)                                          
;----------------------------------------------------------------------
        CMPB      AL,#0                 ; [CPU_] |235| 
        B         $C$L1,NEQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
	.dwpsn	file "../source/InitGpio.c",line 237,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 237 | usnSensor1_HomeState = 2;                                              
;----------------------------------------------------------------------
        MOVB      @_usnSensor1_HomeState,#2,UNC ; [CPU_] |237| 
	.dwpsn	file "../source/InitGpio.c",line 239,column 1,is_stmt,isa 0
$C$L1:    
        SUBB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$45	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$45, DW_AT_low_pc(0x00)
	.dwattr $C$DW$45, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$37, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$37, DW_AT_TI_end_line(0xef)
	.dwattr $C$DW$37, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$37

	.sect	".text:_IGGpioConfig_HgbPwm"
	.clink
	.global	_IGGpioConfig_HgbPwm

$C$DW$46	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$46, DW_AT_name("IGGpioConfig_HgbPwm")
	.dwattr $C$DW$46, DW_AT_low_pc(_IGGpioConfig_HgbPwm)
	.dwattr $C$DW$46, DW_AT_high_pc(0x00)
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_IGGpioConfig_HgbPwm")
	.dwattr $C$DW$46, DW_AT_external
	.dwattr $C$DW$46, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$46, DW_AT_TI_begin_line(0xfc)
	.dwattr $C$DW$46, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$46, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/InitGpio.c",line 253,column 1,is_stmt,address _IGGpioConfig_HgbPwm,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_HgbPwm
;----------------------------------------------------------------------
; 252 | void IGGpioConfig_HgbPwm(void)                                         
; 255 | GpioPinConfig GpioSetupHgbPwm;                                         
; 257 | //!Set the following parameters of the HGB PWM GPIO pin:               
; 258 | //!GPIO pin no. to HB_PWM_HGB                                          
; 259 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 260 | //!GPIO Pin as output pin                                              
; 261 | //!GPIO pin as GPIO_PUSHPULL                                           
; 262 | //!GPIO peripheral as high                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_HgbPwm          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_HgbPwm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$47	.dwtag  DW_TAG_variable
	.dwattr $C$DW$47, DW_AT_name("GpioSetupHgbPwm")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_GpioSetupHgbPwm")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -5]

	.dwpsn	file "../source/InitGpio.c",line 263,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 263 | GpioSetupHgbPwm.GpioPin = HB_PWM_HGB;                                  
;----------------------------------------------------------------------
        MOVB      *-SP[5],#157,UNC      ; [CPU_] |263| 
	.dwpsn	file "../source/InitGpio.c",line 264,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 264 | GpioSetupHgbPwm.GpioCpuMuxSel = GPIO_MUX_CPU1;                         
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |264| 
	.dwpsn	file "../source/InitGpio.c",line 265,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 265 | GpioSetupHgbPwm.GpioInputOutput = GPIO_OUTPUT;                         
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |265| 
	.dwpsn	file "../source/InitGpio.c",line 266,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 266 | GpioSetupHgbPwm.GpioFlags = GPIO_PUSHPULL;                             
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |266| 
	.dwpsn	file "../source/InitGpio.c",line 267,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 267 | GpioSetupHgbPwm.GpioPeripheral = HB_PWM_HGB_PERIPHERAL;                
; 269 | //!After setting all the configurations of the pin,                    
; 270 | //!Update the configurations of the HGB PWM GPIO                       
;----------------------------------------------------------------------
        MOVB      *-SP[1],#1,UNC        ; [CPU_] |267| 
	.dwpsn	file "../source/InitGpio.c",line 271,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 271 | IGConfigGpioPin(GpioSetupHgbPwm);                                      
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |271| 
        SUBB      XAR4,#5               ; [CPU_U] |271| 
        MOVZ      AR4,AR4               ; [CPU_] |271| 
$C$DW$48	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$48, DW_AT_low_pc(0x00)
	.dwattr $C$DW$48, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$48, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |271| 
        ; call occurs [#_IGConfigGpioPin] ; [] |271| 
	.dwpsn	file "../source/InitGpio.c",line 274,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$46, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$46, DW_AT_TI_end_line(0x112)
	.dwattr $C$DW$46, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$46

	.sect	".text:_IGGpioConfig_UartComm"
	.clink
	.global	_IGGpioConfig_UartComm

$C$DW$50	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$50, DW_AT_name("IGGpioConfig_UartComm")
	.dwattr $C$DW$50, DW_AT_low_pc(_IGGpioConfig_UartComm)
	.dwattr $C$DW$50, DW_AT_high_pc(0x00)
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_IGGpioConfig_UartComm")
	.dwattr $C$DW$50, DW_AT_external
	.dwattr $C$DW$50, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$50, DW_AT_TI_begin_line(0x121)
	.dwattr $C$DW$50, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$50, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../source/InitGpio.c",line 290,column 1,is_stmt,address _IGGpioConfig_UartComm,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_UartComm
;----------------------------------------------------------------------
; 289 | void IGGpioConfig_UartComm(void)                                       
; 291 | #if 1                                                                  
; 292 | GpioPinConfig GpioSetupDebugPortUartComm[2];                           
; 294 | //!Set the following parameters of the Debug port UART TX GPIO pin:    
; 295 | //!GPIO pin no. to DEBUG_PORT_UART_TX                                  
; 296 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 297 | //!GPIO Pin as output pin                                              
; 298 | //!GPIO pin as GPIO_ASYNC                                              
; 299 | //!GPIO peripheral as DEBUG_PORT_UART_PERIPHERAL                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_UartComm        FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_UartComm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$51	.dwtag  DW_TAG_variable
	.dwattr $C$DW$51, DW_AT_name("GpioSetupDebugPortUartComm")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_GpioSetupDebugPortUartComm")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -10]

	.dwpsn	file "../source/InitGpio.c",line 300,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 300 | GpioSetupDebugPortUartComm[0].GpioPin = DEBUG_PORT_UART_TX;            
;----------------------------------------------------------------------
        MOVB      *-SP[10],#8,UNC       ; [CPU_] |300| 
	.dwpsn	file "../source/InitGpio.c",line 301,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 301 | GpioSetupDebugPortUartComm[0].GpioCpuMuxSel = GPIO_MUX_CPU1;           
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |301| 
	.dwpsn	file "../source/InitGpio.c",line 302,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 302 | GpioSetupDebugPortUartComm[0].GpioInputOutput = GPIO_OUTPUT;           
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |302| 
	.dwpsn	file "../source/InitGpio.c",line 303,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 303 | GpioSetupDebugPortUartComm[0].GpioFlags = GPIO_ASYNC;                  
;----------------------------------------------------------------------
        MOVB      *-SP[9],#48,UNC       ; [CPU_] |303| 
	.dwpsn	file "../source/InitGpio.c",line 304,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 304 | GpioSetupDebugPortUartComm[0].GpioPeripheral = DEBUG_PORT_UART_PERIPHER
;     | AL;                                                                    
; 306 | //!Set the following parameters of the Debug port UART RX GPIO pin:    
; 307 | //!GPIO pin no. to DEBUG_PORT_UART_RX                                  
; 308 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 309 | //!GPIO Pin as input pin                                               
; 310 | //!GPIO pin as GPIO_PUSHPULL                                           
; 311 | //!GPIO peripheral as DEBUG_PORT_UART_PERIPHERAL                       
;----------------------------------------------------------------------
        MOVB      *-SP[6],#6,UNC        ; [CPU_] |304| 
	.dwpsn	file "../source/InitGpio.c",line 312,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 312 | GpioSetupDebugPortUartComm[1].GpioPin = DEBUG_PORT_UART_RX;            
;----------------------------------------------------------------------
        MOVB      *-SP[5],#9,UNC        ; [CPU_] |312| 
	.dwpsn	file "../source/InitGpio.c",line 313,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 313 | GpioSetupDebugPortUartComm[1].GpioCpuMuxSel = GPIO_MUX_CPU1;           
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |313| 
	.dwpsn	file "../source/InitGpio.c",line 314,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 314 | GpioSetupDebugPortUartComm[1].GpioInputOutput = GPIO_INPUT;            
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |314| 
	.dwpsn	file "../source/InitGpio.c",line 315,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 315 | GpioSetupDebugPortUartComm[1].GpioFlags = GPIO_PUSHPULL;               
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |315| 
	.dwpsn	file "../source/InitGpio.c",line 316,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 316 | GpioSetupDebugPortUartComm[1].GpioPeripheral = DEBUG_PORT_UART_PERIPHER
;     | AL;                                                                    
; 318 | //!After setting all the configurations of the pin,                    
; 319 | //!Update the configurations of the UART RX & TX  GPIO                 
;----------------------------------------------------------------------
        MOVB      *-SP[1],#6,UNC        ; [CPU_] |316| 
	.dwpsn	file "../source/InitGpio.c",line 320,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 320 | IGConfigGpioPin(GpioSetupDebugPortUartComm[1]);                        
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |320| 
        SUBB      XAR4,#5               ; [CPU_U] |320| 
        MOVZ      AR4,AR4               ; [CPU_] |320| 
$C$DW$52	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$52, DW_AT_low_pc(0x00)
	.dwattr $C$DW$52, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$52, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |320| 
        ; call occurs [#_IGConfigGpioPin] ; [] |320| 
	.dwpsn	file "../source/InitGpio.c",line 321,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 321 | IGConfigGpioPin(GpioSetupDebugPortUartComm[0]);                        
; 323 | #endif                                                                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |321| 
        SUBB      XAR4,#10              ; [CPU_U] |321| 
        MOVZ      AR4,AR4               ; [CPU_] |321| 
$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$53, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |321| 
        ; call occurs [#_IGConfigGpioPin] ; [] |321| 
	.dwpsn	file "../source/InitGpio.c",line 325,column 1,is_stmt,isa 0
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$50, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$50, DW_AT_TI_end_line(0x145)
	.dwattr $C$DW$50, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$50

	.sect	".text:_IGGpioConfig_SpiValve"
	.clink
	.global	_IGGpioConfig_SpiValve

$C$DW$55	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$55, DW_AT_name("IGGpioConfig_SpiValve")
	.dwattr $C$DW$55, DW_AT_low_pc(_IGGpioConfig_SpiValve)
	.dwattr $C$DW$55, DW_AT_high_pc(0x00)
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_IGGpioConfig_SpiValve")
	.dwattr $C$DW$55, DW_AT_external
	.dwattr $C$DW$55, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$55, DW_AT_TI_begin_line(0x155)
	.dwattr $C$DW$55, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$55, DW_AT_TI_max_frame_size(-28)
	.dwpsn	file "../source/InitGpio.c",line 342,column 1,is_stmt,address _IGGpioConfig_SpiValve,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_SpiValve
;----------------------------------------------------------------------
; 341 | void IGGpioConfig_SpiValve(void)                                       
; 343 | #if 1                                                                  
; 344 | GpioPinConfig GpioSetupSpiValve[5];                                    
; 346 | //!Set the following parameters of the SPI Communciation SIMO GPIO pin:
; 347 | //!GPIO pin no. to SPI_SIMO_VALVE                                      
; 348 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 349 | //!GPIO Pin as output pin                                              
; 350 | //!GPIO pin as GPIO_ASYNC                                              
; 351 | //!GPIO peripheral as SPI_PERIPHERAL_VALVE                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_SpiValve        FR SIZE:  26           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 25 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_SpiValve:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -28
$C$DW$56	.dwtag  DW_TAG_variable
	.dwattr $C$DW$56, DW_AT_name("GpioSetupSpiValve")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_GpioSetupSpiValve")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_breg20 -25]

	.dwpsn	file "../source/InitGpio.c",line 352,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 352 | GpioSetupSpiValve[0].GpioPin = SPI_SIMO_VALVE;                         
;----------------------------------------------------------------------
        MOVB      *-SP[25],#122,UNC     ; [CPU_] |352| 
	.dwpsn	file "../source/InitGpio.c",line 353,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 353 | GpioSetupSpiValve[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                    
;----------------------------------------------------------------------
        MOV       *-SP[23],#0           ; [CPU_] |353| 
	.dwpsn	file "../source/InitGpio.c",line 354,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 354 | GpioSetupSpiValve[0].GpioInputOutput = GPIO_OUTPUT;                    
;----------------------------------------------------------------------
        MOVB      *-SP[22],#1,UNC       ; [CPU_] |354| 
	.dwpsn	file "../source/InitGpio.c",line 355,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 355 | GpioSetupSpiValve[0].GpioFlags = GPIO_ASYNC;                           
;----------------------------------------------------------------------
        MOVB      *-SP[24],#48,UNC      ; [CPU_] |355| 
	.dwpsn	file "../source/InitGpio.c",line 356,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 356 | GpioSetupSpiValve[0].GpioPeripheral = SPI_PERIPHERAL_VALVE;            
; 358 | //!Set the following parameters of the SPI Communciation SOMI GPIO pin:
; 359 | //!GPIO pin no. to SPI_SOMI_VALVE                                      
; 360 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 361 | //!GPIO Pin as input pin                                               
; 362 | //!GPIO pin as GPIO_ASYNC                                              
; 363 | //!GPIO peripheral as SPI_PERIPHERAL_VALVE                             
;----------------------------------------------------------------------
        MOVB      *-SP[21],#6,UNC       ; [CPU_] |356| 
	.dwpsn	file "../source/InitGpio.c",line 364,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 364 | GpioSetupSpiValve[1].GpioPin = SPI_SOMI_VALVE;                         
;----------------------------------------------------------------------
        MOVB      *-SP[20],#123,UNC     ; [CPU_] |364| 
	.dwpsn	file "../source/InitGpio.c",line 365,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 365 | GpioSetupSpiValve[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                    
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |365| 
	.dwpsn	file "../source/InitGpio.c",line 366,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 366 | GpioSetupSpiValve[1].GpioInputOutput = GPIO_INPUT;                     
;----------------------------------------------------------------------
        MOV       *-SP[17],#0           ; [CPU_] |366| 
	.dwpsn	file "../source/InitGpio.c",line 367,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 367 | GpioSetupSpiValve[1].GpioFlags = GPIO_ASYNC;                           
;----------------------------------------------------------------------
        MOVB      *-SP[19],#48,UNC      ; [CPU_] |367| 
	.dwpsn	file "../source/InitGpio.c",line 368,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 368 | GpioSetupSpiValve[1].GpioPeripheral = SPI_PERIPHERAL_VALVE;            
; 370 | //!Set the following parameters of the SPI Communciation CLK GPIO pin: 
; 371 | //!GPIO pin no. to SPI_CLK_VALVE                                       
; 372 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 373 | //!GPIO Pin as output pin                                              
; 374 | //!GPIO pin as GPIO_ASYNC                                              
; 375 | //!GPIO peripheral as SPI_PERIPHERAL_VALVE                             
;----------------------------------------------------------------------
        MOVB      *-SP[16],#6,UNC       ; [CPU_] |368| 
	.dwpsn	file "../source/InitGpio.c",line 376,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 376 | GpioSetupSpiValve[2].GpioPin = SPI_CLK_VALVE;                          
;----------------------------------------------------------------------
        MOVB      *-SP[15],#124,UNC     ; [CPU_] |376| 
	.dwpsn	file "../source/InitGpio.c",line 377,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 377 | GpioSetupSpiValve[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                    
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |377| 
	.dwpsn	file "../source/InitGpio.c",line 378,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 378 | GpioSetupSpiValve[2].GpioInputOutput = GPIO_OUTPUT;                    
;----------------------------------------------------------------------
        MOVB      *-SP[12],#1,UNC       ; [CPU_] |378| 
	.dwpsn	file "../source/InitGpio.c",line 379,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 379 | GpioSetupSpiValve[2].GpioFlags = GPIO_ASYNC;                           
;----------------------------------------------------------------------
        MOVB      *-SP[14],#48,UNC      ; [CPU_] |379| 
	.dwpsn	file "../source/InitGpio.c",line 380,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 380 | GpioSetupSpiValve[2].GpioPeripheral = SPI_PERIPHERAL_VALVE;            
; 382 | //!Set the following parameters of the SPI Communciation CS GPIO pin:  
; 383 | //!GPIO pin no. to SPI_CS_VALVE                                        
; 384 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 385 | //!GPIO Pin as output pin                                              
; 386 | //!GPIO pin as GPIO_ASYNC                                              
; 387 | //!GPIO peripheral as SPI_PERIPHERAL_VALVE                             
;----------------------------------------------------------------------
        MOVB      *-SP[11],#6,UNC       ; [CPU_] |380| 
	.dwpsn	file "../source/InitGpio.c",line 388,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 388 | GpioSetupSpiValve[3].GpioPin = SPI_CS_VALVE;                           
;----------------------------------------------------------------------
        MOVB      *-SP[10],#125,UNC     ; [CPU_] |388| 
	.dwpsn	file "../source/InitGpio.c",line 389,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 389 | GpioSetupSpiValve[3].GpioCpuMuxSel = GPIO_MUX_CPU1;                    
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |389| 
	.dwpsn	file "../source/InitGpio.c",line 390,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 390 | GpioSetupSpiValve[3].GpioInputOutput = GPIO_OUTPUT;                    
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |390| 
	.dwpsn	file "../source/InitGpio.c",line 391,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 391 | GpioSetupSpiValve[3].GpioFlags = GPIO_ASYNC;                           
;----------------------------------------------------------------------
        MOVB      *-SP[9],#48,UNC       ; [CPU_] |391| 
	.dwpsn	file "../source/InitGpio.c",line 392,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 392 | GpioSetupSpiValve[3].GpioPeripheral = SPI_PERIPHERAL_VALVE;            
; 394 | //!Set the following parameters of the SPI Communciation RESET GPIO pin
;     | :                                                                      
; 395 | //!GPIO pin no. to SPI_RESET_VALVE                                     
; 396 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 397 | //!GPIO Pin as output pin                                              
; 398 | //!GPIO pin as GPIO_ASYNC                                              
; 399 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOVB      *-SP[6],#6,UNC        ; [CPU_] |392| 
	.dwpsn	file "../source/InitGpio.c",line 400,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 400 | GpioSetupSpiValve[4].GpioPin = SPI_RESET_VALVE;                        
;----------------------------------------------------------------------
        MOVB      *-SP[5],#5,UNC        ; [CPU_] |400| 
	.dwpsn	file "../source/InitGpio.c",line 401,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 401 | GpioSetupSpiValve[4].GpioCpuMuxSel = GPIO_MUX_CPU1;                    
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |401| 
	.dwpsn	file "../source/InitGpio.c",line 402,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 402 | GpioSetupSpiValve[4].GpioInputOutput = GPIO_OUTPUT;                    
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |402| 
	.dwpsn	file "../source/InitGpio.c",line 403,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 403 | GpioSetupSpiValve[4].GpioFlags = GPIO_PUSHPULL;                        
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |403| 
	.dwpsn	file "../source/InitGpio.c",line 404,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 404 | GpioSetupSpiValve[4].GpioPeripheral = 0;                               
; 406 | //!After setting all the configurations of the pin,                    
; 407 | //!Update the configurations of the SPI communcation GPIOs             
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |404| 
	.dwpsn	file "../source/InitGpio.c",line 408,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 408 | IGConfigGpioPin(GpioSetupSpiValve[0]);                                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |408| 
        SUBB      XAR4,#25              ; [CPU_U] |408| 
        MOVZ      AR4,AR4               ; [CPU_] |408| 
$C$DW$57	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$57, DW_AT_low_pc(0x00)
	.dwattr $C$DW$57, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$57, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |408| 
        ; call occurs [#_IGConfigGpioPin] ; [] |408| 
	.dwpsn	file "../source/InitGpio.c",line 409,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 409 | IGConfigGpioPin(GpioSetupSpiValve[1]);                                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |409| 
        SUBB      XAR4,#20              ; [CPU_U] |409| 
        MOVZ      AR4,AR4               ; [CPU_] |409| 
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$58, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |409| 
        ; call occurs [#_IGConfigGpioPin] ; [] |409| 
	.dwpsn	file "../source/InitGpio.c",line 410,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 410 | IGConfigGpioPin(GpioSetupSpiValve[2]);                                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |410| 
        SUBB      XAR4,#15              ; [CPU_U] |410| 
        MOVZ      AR4,AR4               ; [CPU_] |410| 
$C$DW$59	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$59, DW_AT_low_pc(0x00)
	.dwattr $C$DW$59, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$59, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |410| 
        ; call occurs [#_IGConfigGpioPin] ; [] |410| 
	.dwpsn	file "../source/InitGpio.c",line 411,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 411 | IGConfigGpioPin(GpioSetupSpiValve[3]);                                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |411| 
        SUBB      XAR4,#10              ; [CPU_U] |411| 
        MOVZ      AR4,AR4               ; [CPU_] |411| 
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$60, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |411| 
        ; call occurs [#_IGConfigGpioPin] ; [] |411| 
	.dwpsn	file "../source/InitGpio.c",line 412,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 412 | IGConfigGpioPin(GpioSetupSpiValve[4]);                                 
; 413 | #endif                                                                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |412| 
        SUBB      XAR4,#5               ; [CPU_U] |412| 
        MOVZ      AR4,AR4               ; [CPU_] |412| 
$C$DW$61	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$61, DW_AT_low_pc(0x00)
	.dwattr $C$DW$61, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$61, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |412| 
        ; call occurs [#_IGConfigGpioPin] ; [] |412| 
	.dwpsn	file "../source/InitGpio.c",line 414,column 1,is_stmt,isa 0
        SUBB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$62	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$62, DW_AT_low_pc(0x00)
	.dwattr $C$DW$62, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$55, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$55, DW_AT_TI_end_line(0x19e)
	.dwattr $C$DW$55, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$55

	.sect	".text:_IGGpioConfig_SpiHomeSense"
	.clink
	.global	_IGGpioConfig_SpiHomeSense

$C$DW$63	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$63, DW_AT_name("IGGpioConfig_SpiHomeSense")
	.dwattr $C$DW$63, DW_AT_low_pc(_IGGpioConfig_SpiHomeSense)
	.dwattr $C$DW$63, DW_AT_high_pc(0x00)
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_IGGpioConfig_SpiHomeSense")
	.dwattr $C$DW$63, DW_AT_external
	.dwattr $C$DW$63, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$63, DW_AT_TI_begin_line(0x1ac)
	.dwattr $C$DW$63, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$63, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../source/InitGpio.c",line 429,column 1,is_stmt,address _IGGpioConfig_SpiHomeSense,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_SpiHomeSense
;----------------------------------------------------------------------
; 428 | void IGGpioConfig_SpiHomeSense(void)                                   
; 431 | GpioPinConfig GpioSetupSpiHomeSense[4];                                
; 433 | //!Set the following parameters of the SPI Communciation for Home posit
;     | ion                                                                    
; 434 | //!sensor SIMO GPIO pin:                                               
; 435 | //!GPIO pin no. to SPI_SIMO_HOME_SENSE                                 
; 436 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 437 | //!GPIO Pin as output pin                                              
; 438 | //!GPIO pin as GPIO_ASYNC                                              
; 439 | //!GPIO peripheral as SPI_PERIPHERAL_HOME_SENSE                        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_SpiHomeSense    FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 20 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_SpiHomeSense:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$64	.dwtag  DW_TAG_variable
	.dwattr $C$DW$64, DW_AT_name("GpioSetupSpiHomeSense")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_GpioSetupSpiHomeSense")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg20 -20]

	.dwpsn	file "../source/InitGpio.c",line 440,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 440 | GpioSetupSpiHomeSense[0].GpioPin = SPI_SIMO_HOME_SENSE;                
;----------------------------------------------------------------------
        MOVB      *-SP[20],#63,UNC      ; [CPU_] |440| 
	.dwpsn	file "../source/InitGpio.c",line 441,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 441 | GpioSetupSpiHomeSense[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |441| 
	.dwpsn	file "../source/InitGpio.c",line 442,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 442 | GpioSetupSpiHomeSense[0].GpioInputOutput = GPIO_OUTPUT;                
;----------------------------------------------------------------------
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |442| 
	.dwpsn	file "../source/InitGpio.c",line 443,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 443 | GpioSetupSpiHomeSense[0].GpioFlags = GPIO_ASYNC;                       
;----------------------------------------------------------------------
        MOVB      *-SP[19],#48,UNC      ; [CPU_] |443| 
	.dwpsn	file "../source/InitGpio.c",line 444,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 444 | GpioSetupSpiHomeSense[0].GpioPeripheral = SPI_PERIPHERAL_HOME_SENSE;   
; 446 | //!Set the following parameters of the SPI Communciation for Home posit
;     | ion                                                                    
; 447 | //!sensor SOMI GPIO pin:                                               
; 448 | //!GPIO pin no. to SPI_SOMI_HOME_SENSE                                 
; 449 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 450 | //!GPIO Pin as input pin                                               
; 451 | //!GPIO pin as GPIO_ASYNC                                              
; 452 | //!GPIO peripheral as SPI_PERIPHERAL_HOME_SENSE                        
;----------------------------------------------------------------------
        MOVB      *-SP[16],#15,UNC      ; [CPU_] |444| 
	.dwpsn	file "../source/InitGpio.c",line 453,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 453 | GpioSetupSpiHomeSense[1].GpioPin = SPI_SOMI_HOME_SENSE;                
;----------------------------------------------------------------------
        MOVB      *-SP[15],#64,UNC      ; [CPU_] |453| 
	.dwpsn	file "../source/InitGpio.c",line 454,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 454 | GpioSetupSpiHomeSense[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |454| 
	.dwpsn	file "../source/InitGpio.c",line 455,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 455 | GpioSetupSpiHomeSense[1].GpioInputOutput = GPIO_INPUT;                 
;----------------------------------------------------------------------
        MOV       *-SP[12],#0           ; [CPU_] |455| 
	.dwpsn	file "../source/InitGpio.c",line 456,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 456 | GpioSetupSpiHomeSense[1].GpioFlags = GPIO_ASYNC;                       
;----------------------------------------------------------------------
        MOVB      *-SP[14],#48,UNC      ; [CPU_] |456| 
	.dwpsn	file "../source/InitGpio.c",line 457,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 457 | GpioSetupSpiHomeSense[1].GpioPeripheral = SPI_PERIPHERAL_HOME_SENSE;   
; 459 | //!Set the following parameters of the SPI Communciation for Home posit
;     | ion                                                                    
; 460 | //!sensor CLK GPIO pin:                                                
; 461 | //!GPIO pin no. to SPI_SOMI_HOME_SENSE                                 
; 462 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 463 | //!GPIO Pin as output pin                                              
; 464 | //!GPIO pin as GPIO_ASYNC                                              
; 465 | //!GPIO peripheral as SPI_PERIPHERAL_HOME_SENSE                        
;----------------------------------------------------------------------
        MOVB      *-SP[11],#15,UNC      ; [CPU_] |457| 
	.dwpsn	file "../source/InitGpio.c",line 466,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 466 | GpioSetupSpiHomeSense[2].GpioPin = SPI_CLK_HOME_SENSE;                 
;----------------------------------------------------------------------
        MOVB      *-SP[10],#65,UNC      ; [CPU_] |466| 
	.dwpsn	file "../source/InitGpio.c",line 467,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 467 | GpioSetupSpiHomeSense[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |467| 
	.dwpsn	file "../source/InitGpio.c",line 468,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 468 | GpioSetupSpiHomeSense[2].GpioInputOutput = GPIO_OUTPUT;                
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |468| 
	.dwpsn	file "../source/InitGpio.c",line 469,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 469 | GpioSetupSpiHomeSense[2].GpioFlags = GPIO_ASYNC;                       
;----------------------------------------------------------------------
        MOVB      *-SP[9],#48,UNC       ; [CPU_] |469| 
	.dwpsn	file "../source/InitGpio.c",line 470,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 470 | GpioSetupSpiHomeSense[2].GpioPeripheral = SPI_PERIPHERAL_HOME_SENSE;   
; 472 | //!Set the following parameters of the SPI Communciation for Home posit
;     | ion                                                                    
; 473 | //!sensor CS GPIO pin:                                                 
; 474 | //!GPIO pin no. to SPI_CS_HOME_SENSE                                   
; 475 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 476 | //!GPIO Pin as output pin                                              
; 477 | //!GPIO pin as GPIO_ASYNC                                              
; 478 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOVB      *-SP[6],#15,UNC       ; [CPU_] |470| 
	.dwpsn	file "../source/InitGpio.c",line 479,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 479 | GpioSetupSpiHomeSense[3].GpioPin = SPI_CS_HOME_SENSE;                  
;----------------------------------------------------------------------
        MOVB      *-SP[5],#118,UNC      ; [CPU_] |479| 
	.dwpsn	file "../source/InitGpio.c",line 480,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 480 | GpioSetupSpiHomeSense[3].GpioCpuMuxSel = GPIO_MUX_CPU1;                
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |480| 
	.dwpsn	file "../source/InitGpio.c",line 481,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 481 | GpioSetupSpiHomeSense[3].GpioInputOutput = GPIO_OUTPUT;                
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |481| 
	.dwpsn	file "../source/InitGpio.c",line 482,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 482 | GpioSetupSpiHomeSense[3].GpioFlags = GPIO_ASYNC;                       
;----------------------------------------------------------------------
        MOVB      *-SP[4],#48,UNC       ; [CPU_] |482| 
	.dwpsn	file "../source/InitGpio.c",line 483,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 483 | GpioSetupSpiHomeSense[3].GpioPeripheral = 0;                           
; 485 | //!After setting all the configurations of the pin,                    
; 486 | //!Update the configurations of the Valve control SPI communcation GPIO
;     | s                                                                      
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |483| 
	.dwpsn	file "../source/InitGpio.c",line 487,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 487 | IGConfigGpioPin(GpioSetupSpiHomeSense[0]);                             
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |487| 
        SUBB      XAR4,#20              ; [CPU_U] |487| 
        MOVZ      AR4,AR4               ; [CPU_] |487| 
$C$DW$65	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$65, DW_AT_low_pc(0x00)
	.dwattr $C$DW$65, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$65, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |487| 
        ; call occurs [#_IGConfigGpioPin] ; [] |487| 
	.dwpsn	file "../source/InitGpio.c",line 488,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 488 | IGConfigGpioPin(GpioSetupSpiHomeSense[1]);                             
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |488| 
        SUBB      XAR4,#15              ; [CPU_U] |488| 
        MOVZ      AR4,AR4               ; [CPU_] |488| 
$C$DW$66	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$66, DW_AT_low_pc(0x00)
	.dwattr $C$DW$66, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$66, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |488| 
        ; call occurs [#_IGConfigGpioPin] ; [] |488| 
	.dwpsn	file "../source/InitGpio.c",line 489,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 489 | IGConfigGpioPin(GpioSetupSpiHomeSense[2]);                             
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |489| 
        SUBB      XAR4,#10              ; [CPU_U] |489| 
        MOVZ      AR4,AR4               ; [CPU_] |489| 
$C$DW$67	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$67, DW_AT_low_pc(0x00)
	.dwattr $C$DW$67, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$67, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |489| 
        ; call occurs [#_IGConfigGpioPin] ; [] |489| 
	.dwpsn	file "../source/InitGpio.c",line 490,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 490 | IGConfigGpioPin(GpioSetupSpiHomeSense[3]);                             
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |490| 
        SUBB      XAR4,#5               ; [CPU_U] |490| 
        MOVZ      AR4,AR4               ; [CPU_] |490| 
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$68, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |490| 
        ; call occurs [#_IGConfigGpioPin] ; [] |490| 
	.dwpsn	file "../source/InitGpio.c",line 493,column 1,is_stmt,isa 0
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$63, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$63, DW_AT_TI_end_line(0x1ed)
	.dwattr $C$DW$63, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$63

	.sect	".text:_IGConfigHomeSensCS"
	.clink
	.global	_IGConfigHomeSensCS

$C$DW$70	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$70, DW_AT_name("IGConfigHomeSensCS")
	.dwattr $C$DW$70, DW_AT_low_pc(_IGConfigHomeSensCS)
	.dwattr $C$DW$70, DW_AT_high_pc(0x00)
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_IGConfigHomeSensCS")
	.dwattr $C$DW$70, DW_AT_external
	.dwattr $C$DW$70, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$70, DW_AT_TI_begin_line(0x1fa)
	.dwattr $C$DW$70, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$70, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/InitGpio.c",line 507,column 1,is_stmt,address _IGConfigHomeSensCS,isa 0

	.dwfde $C$DW$CIE, _IGConfigHomeSensCS
$C$DW$71	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$71, DW_AT_name("Pin")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_reg0]

$C$DW$72	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$72, DW_AT_name("State")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 506 | void IGConfigHomeSensCS( unsigned int  Pin,  unsigned int  State)      
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGConfigHomeSensCS           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_IGConfigHomeSensCS:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$73	.dwtag  DW_TAG_variable
	.dwattr $C$DW$73, DW_AT_name("Pin")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_breg20 -1]

$C$DW$74	.dwtag  DW_TAG_variable
	.dwattr $C$DW$74, DW_AT_name("State")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 508 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |507| 
        MOV       *-SP[1],AL            ; [CPU_] |507| 
	.dwpsn	file "../source/InitGpio.c",line 509,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 509 | GPIO_WritePin(Pin, State);                                             
;----------------------------------------------------------------------
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$75, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |509| 
        ; call occurs [#_GPIO_WritePin] ; [] |509| 
	.dwpsn	file "../source/InitGpio.c",line 510,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$70, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$70, DW_AT_TI_end_line(0x1fe)
	.dwattr $C$DW$70, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$70

	.sect	".text:_IGGpioConfig_SpiComm"
	.clink
	.global	_IGGpioConfig_SpiComm

$C$DW$77	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$77, DW_AT_name("IGGpioConfig_SpiComm")
	.dwattr $C$DW$77, DW_AT_low_pc(_IGGpioConfig_SpiComm)
	.dwattr $C$DW$77, DW_AT_high_pc(0x00)
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_IGGpioConfig_SpiComm")
	.dwattr $C$DW$77, DW_AT_external
	.dwattr $C$DW$77, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$77, DW_AT_TI_begin_line(0x20d)
	.dwattr $C$DW$77, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$77, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../source/InitGpio.c",line 526,column 1,is_stmt,address _IGGpioConfig_SpiComm,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_SpiComm
;----------------------------------------------------------------------
; 525 | void IGGpioConfig_SpiComm(void)                                        
; 527 | #if 1                                                                  
; 528 | GpioPinConfig GpioSetupSpiComm[4];                                     
; 530 | //!Set the following parameters of the SPI Communciation SIMO GPIO pin:
; 531 | //!GPIO pin no. to SPI_SIMO_MP                                         
; 532 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 533 | //!GPIO Pin as input pin                                               
; 534 | //!GPIO pin as GPIO_ASYNC                                              
; 535 | //!GPIO peripheral as SPI_PERIPHERAL_MP                                
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_SpiComm         FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 20 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_SpiComm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$78	.dwtag  DW_TAG_variable
	.dwattr $C$DW$78, DW_AT_name("GpioSetupSpiComm")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_GpioSetupSpiComm")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_breg20 -20]

	.dwpsn	file "../source/InitGpio.c",line 536,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 536 | GpioSetupSpiComm[0].GpioPin = SPI_SIMO_MP;                             
;----------------------------------------------------------------------
        MOVB      *-SP[20],#58,UNC      ; [CPU_] |536| 
	.dwpsn	file "../source/InitGpio.c",line 537,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 537 | GpioSetupSpiComm[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                     
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |537| 
	.dwpsn	file "../source/InitGpio.c",line 538,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 538 | GpioSetupSpiComm[0].GpioInputOutput = GPIO_INPUT;                      
;----------------------------------------------------------------------
        MOV       *-SP[17],#0           ; [CPU_] |538| 
	.dwpsn	file "../source/InitGpio.c",line 539,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 539 | GpioSetupSpiComm[0].GpioFlags = GPIO_ASYNC;                            
;----------------------------------------------------------------------
        MOVB      *-SP[19],#48,UNC      ; [CPU_] |539| 
	.dwpsn	file "../source/InitGpio.c",line 540,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 540 | GpioSetupSpiComm[0].GpioPeripheral = SPI_PERIPHERAL_MP;                
; 542 | //!Set the following parameters of the SPI Communciation SOMI GPIO pin:
; 543 | //!GPIO pin no. to SPI_SOMI_MP                                         
; 544 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 545 | //!GPIO Pin as output pin                                              
; 546 | //!GPIO pin as GPIO_ASYNC                                              
; 547 | //!GPIO peripheral as SPI_PERIPHERAL_MP                                
;----------------------------------------------------------------------
        MOVB      *-SP[16],#15,UNC      ; [CPU_] |540| 
	.dwpsn	file "../source/InitGpio.c",line 548,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 548 | GpioSetupSpiComm[1].GpioPin = SPI_SOMI_MP;                             
;----------------------------------------------------------------------
        MOVB      *-SP[15],#59,UNC      ; [CPU_] |548| 
	.dwpsn	file "../source/InitGpio.c",line 549,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 549 | GpioSetupSpiComm[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                     
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |549| 
	.dwpsn	file "../source/InitGpio.c",line 550,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 550 | GpioSetupSpiComm[1].GpioInputOutput = GPIO_OUTPUT;                     
;----------------------------------------------------------------------
        MOVB      *-SP[12],#1,UNC       ; [CPU_] |550| 
	.dwpsn	file "../source/InitGpio.c",line 551,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 551 | GpioSetupSpiComm[1].GpioFlags = GPIO_ASYNC;                            
;----------------------------------------------------------------------
        MOVB      *-SP[14],#48,UNC      ; [CPU_] |551| 
	.dwpsn	file "../source/InitGpio.c",line 552,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 552 | GpioSetupSpiComm[1].GpioPeripheral = SPI_PERIPHERAL_MP;                
; 554 | //!Set the following parameters of the SPI Communciation CLK GPIO pin: 
; 555 | //!GPIO pin no. to SPI_CLK_MP                                          
; 556 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 557 | //!GPIO Pin as input pin                                               
; 558 | //!GPIO pin as GPIO_ASYNC                                              
; 559 | //!GPIO peripheral as SPI_PERIPHERAL_MP                                
;----------------------------------------------------------------------
        MOVB      *-SP[11],#15,UNC      ; [CPU_] |552| 
	.dwpsn	file "../source/InitGpio.c",line 560,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 560 | GpioSetupSpiComm[2].GpioPin = SPI_CLK_MP;                              
;----------------------------------------------------------------------
        MOVB      *-SP[10],#60,UNC      ; [CPU_] |560| 
	.dwpsn	file "../source/InitGpio.c",line 561,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 561 | GpioSetupSpiComm[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                     
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |561| 
	.dwpsn	file "../source/InitGpio.c",line 562,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 562 | GpioSetupSpiComm[2].GpioInputOutput = GPIO_INPUT;                      
;----------------------------------------------------------------------
        MOV       *-SP[7],#0            ; [CPU_] |562| 
	.dwpsn	file "../source/InitGpio.c",line 563,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 563 | GpioSetupSpiComm[2].GpioFlags = GPIO_ASYNC;                            
;----------------------------------------------------------------------
        MOVB      *-SP[9],#48,UNC       ; [CPU_] |563| 
	.dwpsn	file "../source/InitGpio.c",line 564,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 564 | GpioSetupSpiComm[2].GpioPeripheral = SPI_PERIPHERAL_MP;                
; 566 | //!Set the following parameters of the SPI Communciation CS GPIO pin:  
; 567 | //!GPIO pin no. to SPI_CS_MP                                           
; 568 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 569 | //!GPIO Pin as input pin                                               
; 570 | //!GPIO pin as GPIO_ASYNC                                              
; 571 | //!GPIO peripheral as SPI_PERIPHERAL_MP                                
;----------------------------------------------------------------------
        MOVB      *-SP[6],#15,UNC       ; [CPU_] |564| 
	.dwpsn	file "../source/InitGpio.c",line 572,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 572 | GpioSetupSpiComm[3].GpioPin = SPI_CS_MP;                               
;----------------------------------------------------------------------
        MOVB      *-SP[5],#61,UNC       ; [CPU_] |572| 
	.dwpsn	file "../source/InitGpio.c",line 573,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 573 | GpioSetupSpiComm[3].GpioCpuMuxSel = GPIO_MUX_CPU1;                     
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |573| 
	.dwpsn	file "../source/InitGpio.c",line 574,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 574 | GpioSetupSpiComm[3].GpioInputOutput = GPIO_INPUT;                      
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |574| 
	.dwpsn	file "../source/InitGpio.c",line 575,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 575 | GpioSetupSpiComm[3].GpioFlags = GPIO_ASYNC;                            
;----------------------------------------------------------------------
        MOVB      *-SP[4],#48,UNC       ; [CPU_] |575| 
	.dwpsn	file "../source/InitGpio.c",line 576,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 576 | GpioSetupSpiComm[3].GpioPeripheral = SPI_PERIPHERAL_MP;                
; 578 | //!After setting all the configurations of the pin,                    
; 579 | //!Update the configurations of the SPI communcation GPIOs             
;----------------------------------------------------------------------
        MOVB      *-SP[1],#15,UNC       ; [CPU_] |576| 
	.dwpsn	file "../source/InitGpio.c",line 580,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 580 | IGConfigGpioPin(GpioSetupSpiComm[0]);                                  
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |580| 
        SUBB      XAR4,#20              ; [CPU_U] |580| 
        MOVZ      AR4,AR4               ; [CPU_] |580| 
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$79, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |580| 
        ; call occurs [#_IGConfigGpioPin] ; [] |580| 
	.dwpsn	file "../source/InitGpio.c",line 581,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 581 | IGConfigGpioPin(GpioSetupSpiComm[1]);                                  
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |581| 
        SUBB      XAR4,#15              ; [CPU_U] |581| 
        MOVZ      AR4,AR4               ; [CPU_] |581| 
$C$DW$80	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$80, DW_AT_low_pc(0x00)
	.dwattr $C$DW$80, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$80, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |581| 
        ; call occurs [#_IGConfigGpioPin] ; [] |581| 
	.dwpsn	file "../source/InitGpio.c",line 582,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 582 | IGConfigGpioPin(GpioSetupSpiComm[2]);                                  
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |582| 
        SUBB      XAR4,#10              ; [CPU_U] |582| 
        MOVZ      AR4,AR4               ; [CPU_] |582| 
$C$DW$81	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$81, DW_AT_low_pc(0x00)
	.dwattr $C$DW$81, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$81, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |582| 
        ; call occurs [#_IGConfigGpioPin] ; [] |582| 
	.dwpsn	file "../source/InitGpio.c",line 583,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 583 | IGConfigGpioPin(GpioSetupSpiComm[3]);                                  
; 584 | #endif                                                                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |583| 
        SUBB      XAR4,#5               ; [CPU_U] |583| 
        MOVZ      AR4,AR4               ; [CPU_] |583| 
$C$DW$82	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$82, DW_AT_low_pc(0x00)
	.dwattr $C$DW$82, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$82, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |583| 
        ; call occurs [#_IGConfigGpioPin] ; [] |583| 
	.dwpsn	file "../source/InitGpio.c",line 587,column 1,is_stmt,isa 0
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$83	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$83, DW_AT_low_pc(0x00)
	.dwattr $C$DW$83, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$77, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$77, DW_AT_TI_end_line(0x24b)
	.dwattr $C$DW$77, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$77

	.sect	".text:_IGGpioConfig_SpiCommControlPin"
	.clink
	.global	_IGGpioConfig_SpiCommControlPin

$C$DW$84	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$84, DW_AT_name("IGGpioConfig_SpiCommControlPin")
	.dwattr $C$DW$84, DW_AT_low_pc(_IGGpioConfig_SpiCommControlPin)
	.dwattr $C$DW$84, DW_AT_high_pc(0x00)
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_IGGpioConfig_SpiCommControlPin")
	.dwattr $C$DW$84, DW_AT_external
	.dwattr $C$DW$84, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$84, DW_AT_TI_begin_line(0x25a)
	.dwattr $C$DW$84, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$84, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../source/InitGpio.c",line 603,column 1,is_stmt,address _IGGpioConfig_SpiCommControlPin,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_SpiCommControlPin
;----------------------------------------------------------------------
; 602 | void IGGpioConfig_SpiCommControlPin(void)                              
; 604 | #if 1                                                                  
; 605 | GpioPinConfig GpioSetupSpiCommControlPin[4];                           
; 607 | //!Set the following parameters of the SPI Communciation Control GPIO p
;     | in:                                                                    
; 608 | //!GPIO pin no. to SPI_COMM_CTRL_DELFINO_TXR_INT_REQ                   
; 609 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 610 | //!GPIO Pin as output pin                                              
; 611 | //!GPIO pin as GPIO_PUSHPULL                                           
; 612 | //!GPIO peripheral as GPIO_PERIPHERAL_MP                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_SpiCommControlPin FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 20 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_SpiCommControlPin:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$85	.dwtag  DW_TAG_variable
	.dwattr $C$DW$85, DW_AT_name("GpioSetupSpiCommControlPin")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_GpioSetupSpiCommControlPin")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -20]

	.dwpsn	file "../source/InitGpio.c",line 613,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 613 | GpioSetupSpiCommControlPin[0].GpioPin = SPI_COMM_CTRL_DELFINO_TXR_INT_R
;     | EQ;                                                                    
;----------------------------------------------------------------------
        MOVB      *-SP[20],#165,UNC     ; [CPU_] |613| 
	.dwpsn	file "../source/InitGpio.c",line 614,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 614 | GpioSetupSpiCommControlPin[0].GpioCpuMuxSel = GPIO_MUX_CPU1;           
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |614| 
	.dwpsn	file "../source/InitGpio.c",line 615,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 615 | GpioSetupSpiCommControlPin[0].GpioInputOutput = GPIO_OUTPUT;           
;----------------------------------------------------------------------
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |615| 
	.dwpsn	file "../source/InitGpio.c",line 616,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 616 | GpioSetupSpiCommControlPin[0].GpioFlags = GPIO_PUSHPULL;               
;----------------------------------------------------------------------
        MOV       *-SP[19],#0           ; [CPU_] |616| 
	.dwpsn	file "../source/InitGpio.c",line 617,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 617 | GpioSetupSpiCommControlPin[0].GpioPeripheral = GPIO_PERIPHERAL_MP;     
; 619 | //!Set the following parameters of the SPI Communciation Control GPIO p
;     | in:                                                                    
; 620 | //!GPIO pin no. to SPI_COMM_CTRL_DELFINO_BUSY_STATUS                   
; 621 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 622 | //!GPIO Pin as output pin                                              
; 623 | //!GPIO pin as GPIO_PUSHPULL                                           
; 624 | //!GPIO peripheral as GPIO_PERIPHERAL_MP                               
;----------------------------------------------------------------------
        MOV       *-SP[16],#0           ; [CPU_] |617| 
	.dwpsn	file "../source/InitGpio.c",line 625,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 625 | GpioSetupSpiCommControlPin[1].GpioPin = SPI_COMM_CTRL_DELFINO_BUSY_STAT
;     | US;                                                                    
;----------------------------------------------------------------------
        MOVB      *-SP[15],#166,UNC     ; [CPU_] |625| 
	.dwpsn	file "../source/InitGpio.c",line 626,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 626 | GpioSetupSpiCommControlPin[1].GpioCpuMuxSel = GPIO_MUX_CPU1;           
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |626| 
	.dwpsn	file "../source/InitGpio.c",line 627,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 627 | GpioSetupSpiCommControlPin[1].GpioInputOutput = GPIO_OUTPUT;           
;----------------------------------------------------------------------
        MOVB      *-SP[12],#1,UNC       ; [CPU_] |627| 
	.dwpsn	file "../source/InitGpio.c",line 628,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 628 | GpioSetupSpiCommControlPin[1].GpioFlags = GPIO_PUSHPULL;               
;----------------------------------------------------------------------
        MOV       *-SP[14],#0           ; [CPU_] |628| 
	.dwpsn	file "../source/InitGpio.c",line 629,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 629 | GpioSetupSpiCommControlPin[1].GpioPeripheral = GPIO_PERIPHERAL_MP;     
; 631 | //!Set the following parameters of the SPI Communciation Control GPIO p
;     | in:                                                                    
; 632 | //!GPIO pin no. to SPI_COMM_CTRL_SITARA_BUSY_STATUS                    
; 633 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 634 | //!GPIO Pin as output pin                                              
; 635 | //!GPIO pin as GPIO_PUSHPULL                                           
; 636 | //!GPIO peripheral as GPIO_PERIPHERAL_MP                               
;----------------------------------------------------------------------
        MOV       *-SP[11],#0           ; [CPU_] |629| 
	.dwpsn	file "../source/InitGpio.c",line 637,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 637 | GpioSetupSpiCommControlPin[2].GpioPin = SPI_COMM_CTRL_SITARA_BUSY_STATU
;     | S;                                                                     
;----------------------------------------------------------------------
        MOVB      *-SP[10],#167,UNC     ; [CPU_] |637| 
	.dwpsn	file "../source/InitGpio.c",line 638,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 638 | GpioSetupSpiCommControlPin[2].GpioCpuMuxSel = GPIO_MUX_CPU1;           
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |638| 
	.dwpsn	file "../source/InitGpio.c",line 639,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 639 | GpioSetupSpiCommControlPin[2].GpioInputOutput = GPIO_OUTPUT;           
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |639| 
	.dwpsn	file "../source/InitGpio.c",line 640,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 640 | GpioSetupSpiCommControlPin[2].GpioFlags = GPIO_PUSHPULL;               
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |640| 
	.dwpsn	file "../source/InitGpio.c",line 641,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 641 | GpioSetupSpiCommControlPin[2].GpioPeripheral = GPIO_PERIPHERAL_MP;     
; 643 | //!Set the following parameters of the SPI Communciation Control GPIO p
;     | in:                                                                    
; 644 | //!GPIO pin no. to SPI_COMM_CTRL_SITARA_ABORT_REQ                      
; 645 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 646 | //!GPIO Pin as input pin                                               
; 647 | //!GPIO pin as GPIO_PUSHPULL                                           
; 648 | //!GPIO peripheral as GPIO_PERIPHERAL_MP                               
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |641| 
	.dwpsn	file "../source/InitGpio.c",line 649,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 649 | GpioSetupSpiCommControlPin[3].GpioPin = SPI_COMM_CTRL_SITARA_ABORT_REQ;
;----------------------------------------------------------------------
        MOVB      *-SP[5],#168,UNC      ; [CPU_] |649| 
	.dwpsn	file "../source/InitGpio.c",line 650,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 650 | GpioSetupSpiCommControlPin[3].GpioCpuMuxSel = GPIO_MUX_CPU1;           
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |650| 
	.dwpsn	file "../source/InitGpio.c",line 651,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 651 | GpioSetupSpiCommControlPin[3].GpioInputOutput = GPIO_INPUT;            
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |651| 
	.dwpsn	file "../source/InitGpio.c",line 652,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 652 | GpioSetupSpiCommControlPin[3].GpioFlags = GPIO_PUSHPULL;               
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |652| 
	.dwpsn	file "../source/InitGpio.c",line 653,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 653 | GpioSetupSpiCommControlPin[3].GpioPeripheral = GPIO_PERIPHERAL_MP;     
; 655 | //!After setting all the configurations of the pin,                    
; 656 | //!Update the configurations of the SPI communcation control GPIOs     
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |653| 
	.dwpsn	file "../source/InitGpio.c",line 657,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 657 | IGConfigGpioPin(GpioSetupSpiCommControlPin[0]);                        
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |657| 
        SUBB      XAR4,#20              ; [CPU_U] |657| 
        MOVZ      AR4,AR4               ; [CPU_] |657| 
$C$DW$86	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$86, DW_AT_low_pc(0x00)
	.dwattr $C$DW$86, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$86, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |657| 
        ; call occurs [#_IGConfigGpioPin] ; [] |657| 
	.dwpsn	file "../source/InitGpio.c",line 658,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 658 | IGConfigGpioPin(GpioSetupSpiCommControlPin[1]);                        
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |658| 
        SUBB      XAR4,#15              ; [CPU_U] |658| 
        MOVZ      AR4,AR4               ; [CPU_] |658| 
$C$DW$87	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$87, DW_AT_low_pc(0x00)
	.dwattr $C$DW$87, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$87, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |658| 
        ; call occurs [#_IGConfigGpioPin] ; [] |658| 
	.dwpsn	file "../source/InitGpio.c",line 659,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 659 | IGConfigGpioPin(GpioSetupSpiCommControlPin[2]);                        
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |659| 
        SUBB      XAR4,#10              ; [CPU_U] |659| 
        MOVZ      AR4,AR4               ; [CPU_] |659| 
$C$DW$88	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$88, DW_AT_low_pc(0x00)
	.dwattr $C$DW$88, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$88, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |659| 
        ; call occurs [#_IGConfigGpioPin] ; [] |659| 
	.dwpsn	file "../source/InitGpio.c",line 660,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 660 | IGConfigGpioPin(GpioSetupSpiCommControlPin[3]);                        
; 661 | #endif                                                                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |660| 
        SUBB      XAR4,#5               ; [CPU_U] |660| 
        MOVZ      AR4,AR4               ; [CPU_] |660| 
$C$DW$89	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$89, DW_AT_low_pc(0x00)
	.dwattr $C$DW$89, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$89, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |660| 
        ; call occurs [#_IGConfigGpioPin] ; [] |660| 
	.dwpsn	file "../source/InitGpio.c",line 662,column 1,is_stmt,isa 0
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$90	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$90, DW_AT_low_pc(0x00)
	.dwattr $C$DW$90, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$84, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$84, DW_AT_TI_end_line(0x296)
	.dwattr $C$DW$84, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$84

	.sect	".text:_IGGpioConfig_DefaultState"
	.clink
	.global	_IGGpioConfig_DefaultState

$C$DW$91	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$91, DW_AT_name("IGGpioConfig_DefaultState")
	.dwattr $C$DW$91, DW_AT_low_pc(_IGGpioConfig_DefaultState)
	.dwattr $C$DW$91, DW_AT_high_pc(0x00)
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_IGGpioConfig_DefaultState")
	.dwattr $C$DW$91, DW_AT_external
	.dwattr $C$DW$91, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$91, DW_AT_TI_begin_line(0x2a3)
	.dwattr $C$DW$91, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$91, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/InitGpio.c",line 676,column 1,is_stmt,address _IGGpioConfig_DefaultState,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_DefaultState
;----------------------------------------------------------------------
; 675 | void IGGpioConfig_DefaultState(void)                                   
; 677 | //!Set LED 1 to high state                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_DefaultState    FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_DefaultState:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/InitGpio.c",line 678,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 678 | IGConfigLED(LED1_GPIO, 1);//ON                                         
; 679 | //    IGConfigLED(LED2_GPIO, 0);//ON                                   
; 681 | //MHN_TEST-inter changed HV & ZAP pins                                 
; 682 | //!Set High voltage pin state to low                                   
;----------------------------------------------------------------------
        MOVB      AL,#116               ; [CPU_] |678| 
        MOVB      AH,#1                 ; [CPU_] |678| 
$C$DW$92	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$92, DW_AT_low_pc(0x00)
	.dwattr $C$DW$92, DW_AT_name("_IGConfigLED")
	.dwattr $C$DW$92, DW_AT_TI_call

        LCR       #_IGConfigLED         ; [CPU_] |678| 
        ; call occurs [#_IGConfigLED] ; [] |678| 
	.dwpsn	file "../source/InitGpio.c",line 683,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 683 | IGConfigHighVoltageModule(HIGH_VOLTAGE_SELECT_GPIO_OUTPUT, \           
; 684 |         HIGH_VOLTAGE_SWITCH_OFF);                                      
; 685 | //!Set ZAP voltage pin state to low                                    
;----------------------------------------------------------------------
        MOVB      AL,#161               ; [CPU_] |683| 
        MOVB      AH,#0                 ; [CPU_] |683| 
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("_IGConfigHighVoltageModule")
	.dwattr $C$DW$93, DW_AT_TI_call

        LCR       #_IGConfigHighVoltageModule ; [CPU_] |683| 
        ; call occurs [#_IGConfigHighVoltageModule] ; [] |683| 
	.dwpsn	file "../source/InitGpio.c",line 686,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 686 | IGConfigHighVoltageModule(ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT, ZAP_VOLTAGE_O
;     | FF);                                                                   
; 688 | //!Set cycle counter pin 1 to low                                      
;----------------------------------------------------------------------
        MOVB      AL,#162               ; [CPU_] |686| 
        MOVB      AH,#1                 ; [CPU_] |686| 
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("_IGConfigHighVoltageModule")
	.dwattr $C$DW$94, DW_AT_TI_call

        LCR       #_IGConfigHighVoltageModule ; [CPU_] |686| 
        ; call occurs [#_IGConfigHighVoltageModule] ; [] |686| 
	.dwpsn	file "../source/InitGpio.c",line 689,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 689 | IGWriteCycleCounterGpio(CYCLE_COUNTER_GPIO_OUTPUT1, 0);                
; 690 | //!Set cycle counter pin 2 to low                                      
;----------------------------------------------------------------------
        MOVB      AL,#53                ; [CPU_] |689| 
        MOVB      AH,#0                 ; [CPU_] |689| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("_IGWriteCycleCounterGpio")
	.dwattr $C$DW$95, DW_AT_TI_call

        LCR       #_IGWriteCycleCounterGpio ; [CPU_] |689| 
        ; call occurs [#_IGWriteCycleCounterGpio] ; [] |689| 
	.dwpsn	file "../source/InitGpio.c",line 691,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 691 | IGWriteCycleCounterGpio(CYCLE_COUNTER_GPIO_OUTPUT2, 0);                
; 693 | //   IGWriteSpiCommControlPin(SPI_COMM_CTRL_DELFINO_TXR_INT_REQ, 0);   
; 694 | //   IGWriteSpiCommControlPin(SPI_COMM_CTRL_DELFINO_BUSY_STATUS, 0);   
; 696 | //!Set SPI Valve reset pin to high                                     
;----------------------------------------------------------------------
        MOVB      AL,#54                ; [CPU_] |691| 
        MOVB      AH,#0                 ; [CPU_] |691| 
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_name("_IGWriteCycleCounterGpio")
	.dwattr $C$DW$96, DW_AT_TI_call

        LCR       #_IGWriteCycleCounterGpio ; [CPU_] |691| 
        ; call occurs [#_IGWriteCycleCounterGpio] ; [] |691| 
	.dwpsn	file "../source/InitGpio.c",line 697,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 697 | IGWriteSpiValveResetPin(SPI_RESET_VALVE, 1);                           
;----------------------------------------------------------------------
        MOVB      AL,#5                 ; [CPU_] |697| 
        MOVB      AH,#1                 ; [CPU_] |697| 
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_name("_IGWriteSpiValveResetPin")
	.dwattr $C$DW$97, DW_AT_TI_call

        LCR       #_IGWriteSpiValveResetPin ; [CPU_] |697| 
        ; call occurs [#_IGWriteSpiValveResetPin] ; [] |697| 
	.dwpsn	file "../source/InitGpio.c",line 698,column 1,is_stmt,isa 0
$C$DW$98	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$98, DW_AT_low_pc(0x00)
	.dwattr $C$DW$98, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$91, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$91, DW_AT_TI_end_line(0x2ba)
	.dwattr $C$DW$91, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$91

	.sect	".text:_IGGpioConfig_LED"
	.clink
	.global	_IGGpioConfig_LED

$C$DW$99	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$99, DW_AT_name("IGGpioConfig_LED")
	.dwattr $C$DW$99, DW_AT_low_pc(_IGGpioConfig_LED)
	.dwattr $C$DW$99, DW_AT_high_pc(0x00)
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_IGGpioConfig_LED")
	.dwattr $C$DW$99, DW_AT_external
	.dwattr $C$DW$99, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$99, DW_AT_TI_begin_line(0x2ca)
	.dwattr $C$DW$99, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$99, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../source/InitGpio.c",line 715,column 1,is_stmt,address _IGGpioConfig_LED,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_LED
;----------------------------------------------------------------------
; 714 | void IGGpioConfig_LED(void)                                            
; 716 | GpioPinConfig GpioSetupLED[2];                                         
; 718 | //!Set the following parameters of the LED 1 GPIO pin:                 
; 719 | //!GPIO pin no. to LED1_GPIO                                           
; 720 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 721 | //!GPIO Pin as output pin                                              
; 722 | //!GPIO pin as GPIO_PUSHPULL                                           
; 723 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_LED             FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_LED:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$100	.dwtag  DW_TAG_variable
	.dwattr $C$DW$100, DW_AT_name("GpioSetupLED")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_GpioSetupLED")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_breg20 -10]

	.dwpsn	file "../source/InitGpio.c",line 724,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 724 | GpioSetupLED[0].GpioPin = LED1_GPIO;                                   
;----------------------------------------------------------------------
        MOVB      *-SP[10],#116,UNC     ; [CPU_] |724| 
	.dwpsn	file "../source/InitGpio.c",line 725,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 725 | GpioSetupLED[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                         
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |725| 
	.dwpsn	file "../source/InitGpio.c",line 726,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 726 | GpioSetupLED[0].GpioInputOutput = GPIO_OUTPUT;                         
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |726| 
	.dwpsn	file "../source/InitGpio.c",line 727,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 727 | GpioSetupLED[0].GpioFlags = GPIO_PUSHPULL;                             
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |727| 
	.dwpsn	file "../source/InitGpio.c",line 728,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 728 | GpioSetupLED[0].GpioPeripheral = 0;                                    
; 730 | //!Set the following parameters of the LED 2 GPIO pin:                 
; 731 | //!GPIO pin no. to LED2_GPIO                                           
; 732 | //!GPIO MUX select to GPIO_MUX_CPU2 to control from CPU2               
; 733 | //!GPIO Pin as output pin                                              
; 734 | //!GPIO pin as GPIO_PUSHPULL                                           
; 735 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |728| 
	.dwpsn	file "../source/InitGpio.c",line 736,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 736 | GpioSetupLED[1].GpioPin = LED2_GPIO;                                   
;----------------------------------------------------------------------
        MOVB      *-SP[5],#117,UNC      ; [CPU_] |736| 
	.dwpsn	file "../source/InitGpio.c",line 737,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 737 | GpioSetupLED[1].GpioCpuMuxSel = GPIO_MUX_CPU2;                         
;----------------------------------------------------------------------
        MOVB      *-SP[3],#2,UNC        ; [CPU_] |737| 
	.dwpsn	file "../source/InitGpio.c",line 738,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 738 | GpioSetupLED[1].GpioInputOutput = GPIO_OUTPUT;                         
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |738| 
	.dwpsn	file "../source/InitGpio.c",line 739,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 739 | GpioSetupLED[1].GpioFlags = GPIO_PUSHPULL;                             
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |739| 
	.dwpsn	file "../source/InitGpio.c",line 740,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 740 | GpioSetupLED[1].GpioPeripheral = 0;                                    
; 742 | //!After setting all the configurations of the pin,                    
; 743 | //!Update the configurations of the LED control GPIOs                  
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |740| 
	.dwpsn	file "../source/InitGpio.c",line 744,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 744 | IGConfigGpioPin(GpioSetupLED[0]);                                      
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |744| 
        SUBB      XAR4,#10              ; [CPU_U] |744| 
        MOVZ      AR4,AR4               ; [CPU_] |744| 
$C$DW$101	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$101, DW_AT_low_pc(0x00)
	.dwattr $C$DW$101, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$101, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |744| 
        ; call occurs [#_IGConfigGpioPin] ; [] |744| 
	.dwpsn	file "../source/InitGpio.c",line 745,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 745 | IGConfigGpioPin(GpioSetupLED[1]);                                      
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |745| 
        SUBB      XAR4,#5               ; [CPU_U] |745| 
        MOVZ      AR4,AR4               ; [CPU_] |745| 
$C$DW$102	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$102, DW_AT_low_pc(0x00)
	.dwattr $C$DW$102, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$102, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |745| 
        ; call occurs [#_IGConfigGpioPin] ; [] |745| 
	.dwpsn	file "../source/InitGpio.c",line 747,column 1,is_stmt,isa 0
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$103	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$103, DW_AT_low_pc(0x00)
	.dwattr $C$DW$103, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$99, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$99, DW_AT_TI_end_line(0x2eb)
	.dwattr $C$DW$99, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$99

	.sect	".text:_IGGpioConfig_MotorReset"
	.clink
	.global	_IGGpioConfig_MotorReset

$C$DW$104	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$104, DW_AT_name("IGGpioConfig_MotorReset")
	.dwattr $C$DW$104, DW_AT_low_pc(_IGGpioConfig_MotorReset)
	.dwattr $C$DW$104, DW_AT_high_pc(0x00)
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_IGGpioConfig_MotorReset")
	.dwattr $C$DW$104, DW_AT_external
	.dwattr $C$DW$104, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$104, DW_AT_TI_begin_line(0x2f7)
	.dwattr $C$DW$104, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$104, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/InitGpio.c",line 760,column 1,is_stmt,address _IGGpioConfig_MotorReset,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_MotorReset
;----------------------------------------------------------------------
; 759 | void IGGpioConfig_MotorReset( void)                                    
; 761 | GpioPinConfig GpioSetupMotorReset;                                     
; 763 | //!Set the following parameters of the Reset GPIO pin:                 
; 764 | //!GPIO pin no. to M_RESET                                             
; 765 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 766 | //!GPIO Pin as output pin                                              
; 767 | //!GPIO pin as GPIO_PUSHPULL                                           
; 768 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_MotorReset      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_MotorReset:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$105	.dwtag  DW_TAG_variable
	.dwattr $C$DW$105, DW_AT_name("GpioSetupMotorReset")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_GpioSetupMotorReset")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -5]

	.dwpsn	file "../source/InitGpio.c",line 769,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 769 | GpioSetupMotorReset.GpioPin = M_RESET;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[5],#10,UNC       ; [CPU_] |769| 
	.dwpsn	file "../source/InitGpio.c",line 770,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 770 | GpioSetupMotorReset.GpioCpuMuxSel = GPIO_MUX_CPU1;                     
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |770| 
	.dwpsn	file "../source/InitGpio.c",line 771,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 771 | GpioSetupMotorReset.GpioInputOutput = GPIO_OUTPUT;                     
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |771| 
	.dwpsn	file "../source/InitGpio.c",line 772,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 772 | GpioSetupMotorReset.GpioFlags = GPIO_PUSHPULL;                         
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |772| 
	.dwpsn	file "../source/InitGpio.c",line 773,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 773 | GpioSetupMotorReset.GpioPeripheral = 0;                                
; 775 | //!After setting all the configurations of the pin,                    
; 776 | //!Update the configurations of the reset control GPIO                 
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |773| 
	.dwpsn	file "../source/InitGpio.c",line 777,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 777 | IGConfigGpioPin(GpioSetupMotorReset);                                  
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |777| 
        SUBB      XAR4,#5               ; [CPU_U] |777| 
        MOVZ      AR4,AR4               ; [CPU_] |777| 
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$106, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |777| 
        ; call occurs [#_IGConfigGpioPin] ; [] |777| 
	.dwpsn	file "../source/InitGpio.c",line 778,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$104, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$104, DW_AT_TI_end_line(0x30a)
	.dwattr $C$DW$104, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$104

	.sect	".text:_IGGpioConfig_Motor1"
	.clink
	.global	_IGGpioConfig_Motor1

$C$DW$108	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$108, DW_AT_name("IGGpioConfig_Motor1")
	.dwattr $C$DW$108, DW_AT_low_pc(_IGGpioConfig_Motor1)
	.dwattr $C$DW$108, DW_AT_high_pc(0x00)
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_IGGpioConfig_Motor1")
	.dwattr $C$DW$108, DW_AT_external
	.dwattr $C$DW$108, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$108, DW_AT_TI_begin_line(0x31b)
	.dwattr $C$DW$108, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$108, DW_AT_TI_max_frame_size(-38)
	.dwpsn	file "../source/InitGpio.c",line 796,column 1,is_stmt,address _IGGpioConfig_Motor1,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_Motor1
;----------------------------------------------------------------------
; 795 | void IGGpioConfig_Motor1(void)                                         
; 797 | GpioPinConfig GpioSetupMotor1[7];                                      
; 799 | //!Set the following parameters of the Motor1 Mode 0 GPIO pin:         
; 800 | //!GPIO pin no. to M1_MODE0                                            
; 801 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 802 | //!GPIO Pin as output pin                                              
; 803 | //!GPIO pin as GPIO_PUSHPULL                                           
; 804 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_Motor1          FR SIZE:  36           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 35 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_Motor1:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -38
$C$DW$109	.dwtag  DW_TAG_variable
	.dwattr $C$DW$109, DW_AT_name("GpioSetupMotor1")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_GpioSetupMotor1")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_breg20 -35]

	.dwpsn	file "../source/InitGpio.c",line 805,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 805 | GpioSetupMotor1[0].GpioPin = M1_MODE0;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[35],#4,UNC       ; [CPU_] |805| 
	.dwpsn	file "../source/InitGpio.c",line 806,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 806 | GpioSetupMotor1[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[33],#0           ; [CPU_] |806| 
	.dwpsn	file "../source/InitGpio.c",line 807,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 807 | GpioSetupMotor1[0].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[32],#1,UNC       ; [CPU_] |807| 
	.dwpsn	file "../source/InitGpio.c",line 808,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 808 | GpioSetupMotor1[0].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[34],#0           ; [CPU_] |808| 
	.dwpsn	file "../source/InitGpio.c",line 809,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 809 | GpioSetupMotor1[0].GpioPeripheral = 0;                                 
; 811 | //!Set the following parameters of the Motor1 Mode 1 GPIO pin:         
; 812 | //!GPIO pin no. to M1_MODE1                                            
; 813 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 814 | //!GPIO Pin as output pin                                              
; 815 | //!GPIO pin as GPIO_PUSHPULL                                           
; 816 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[31],#0           ; [CPU_] |809| 
	.dwpsn	file "../source/InitGpio.c",line 817,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 817 | GpioSetupMotor1[1].GpioPin = M1_MODE1;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[30],#3,UNC       ; [CPU_] |817| 
	.dwpsn	file "../source/InitGpio.c",line 818,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 818 | GpioSetupMotor1[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[28],#0           ; [CPU_] |818| 
	.dwpsn	file "../source/InitGpio.c",line 819,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 819 | GpioSetupMotor1[1].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |819| 
	.dwpsn	file "../source/InitGpio.c",line 820,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 820 | GpioSetupMotor1[1].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[29],#0           ; [CPU_] |820| 
	.dwpsn	file "../source/InitGpio.c",line 821,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 821 | GpioSetupMotor1[1].GpioPeripheral = 0;                                 
; 823 | //!Set the following parameters of the Motor1 Mode 2 GPIO pin:         
; 824 | //!GPIO pin no. to M1_MODE2                                            
; 825 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 826 | //!GPIO Pin as output pin                                              
; 827 | //!GPIO pin as GPIO_PUSHPULL                                           
; 828 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[26],#0           ; [CPU_] |821| 
	.dwpsn	file "../source/InitGpio.c",line 829,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 829 | GpioSetupMotor1[2].GpioPin = M1_MODE2;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[25],#2,UNC       ; [CPU_] |829| 
	.dwpsn	file "../source/InitGpio.c",line 830,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 830 | GpioSetupMotor1[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[23],#0           ; [CPU_] |830| 
	.dwpsn	file "../source/InitGpio.c",line 831,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 831 | GpioSetupMotor1[2].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[22],#1,UNC       ; [CPU_] |831| 
	.dwpsn	file "../source/InitGpio.c",line 832,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 832 | GpioSetupMotor1[2].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[24],#0           ; [CPU_] |832| 
	.dwpsn	file "../source/InitGpio.c",line 833,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 833 | GpioSetupMotor1[2].GpioPeripheral = 0;                                 
; 835 | //!Set the following parameters of the Motor1 Enable GPIO pin:         
; 836 | //!GPIO pin no. to M1_ENBL                                             
; 837 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 838 | //!GPIO Pin as output pin                                              
; 839 | //!GPIO pin as GPIO_PUSHPULL                                           
; 840 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[21],#0           ; [CPU_] |833| 
	.dwpsn	file "../source/InitGpio.c",line 841,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 841 | GpioSetupMotor1[3].GpioPin = M1_ENBL;                                  
;----------------------------------------------------------------------
        MOVB      *-SP[20],#6,UNC       ; [CPU_] |841| 
	.dwpsn	file "../source/InitGpio.c",line 842,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 842 | GpioSetupMotor1[3].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |842| 
	.dwpsn	file "../source/InitGpio.c",line 843,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 843 | GpioSetupMotor1[3].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |843| 
	.dwpsn	file "../source/InitGpio.c",line 844,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 844 | GpioSetupMotor1[3].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[19],#0           ; [CPU_] |844| 
	.dwpsn	file "../source/InitGpio.c",line 845,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 845 | GpioSetupMotor1[3].GpioPeripheral = 0;                                 
; 847 | //!Set the following parameters of the Motor1 Direction GPIO pin:      
; 848 | //!GPIO pin no. to M1_DIR                                              
; 849 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 850 | //!GPIO Pin as output pin                                              
; 851 | //!GPIO pin as GPIO_PUSHPULL                                           
; 852 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[16],#0           ; [CPU_] |845| 
	.dwpsn	file "../source/InitGpio.c",line 853,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 853 | GpioSetupMotor1[4].GpioPin = M1_DIR;                                   
;----------------------------------------------------------------------
        MOVB      *-SP[15],#7,UNC       ; [CPU_] |853| 
	.dwpsn	file "../source/InitGpio.c",line 854,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 854 | GpioSetupMotor1[4].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |854| 
	.dwpsn	file "../source/InitGpio.c",line 855,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 855 | GpioSetupMotor1[4].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[12],#1,UNC       ; [CPU_] |855| 
	.dwpsn	file "../source/InitGpio.c",line 856,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 856 | GpioSetupMotor1[4].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[14],#0           ; [CPU_] |856| 
	.dwpsn	file "../source/InitGpio.c",line 857,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 857 | GpioSetupMotor1[4].GpioPeripheral = 0;                                 
; 859 | //!Set the following parameters of the Motor1 Decay GPIO pin:          
; 860 | //!GPIO pin no. to M1_DECAY                                            
; 861 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 862 | //!GPIO Pin as output pin                                              
; 863 | //!GPIO pin as GPIO_PUSHPULL                                           
; 864 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[11],#0           ; [CPU_] |857| 
	.dwpsn	file "../source/InitGpio.c",line 865,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 865 | GpioSetupMotor1[5].GpioPin = M1_DECAY;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[10],#16,UNC      ; [CPU_] |865| 
	.dwpsn	file "../source/InitGpio.c",line 866,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 866 | GpioSetupMotor1[5].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |866| 
	.dwpsn	file "../source/InitGpio.c",line 867,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 867 | GpioSetupMotor1[5].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |867| 
	.dwpsn	file "../source/InitGpio.c",line 868,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 868 | GpioSetupMotor1[5].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |868| 
	.dwpsn	file "../source/InitGpio.c",line 869,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 869 | GpioSetupMotor1[5].GpioPeripheral = 0;                                 
; 871 | //!Set the following parameters of the Motor1 Fault GPIO pin:          
; 872 | //!GPIO pin no. to M1_FAULT                                            
; 873 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 874 | //!GPIO Pin as input pin                                               
; 875 | //!GPIO pin as GPIO_PUSHPULL                                           
; 876 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |869| 
	.dwpsn	file "../source/InitGpio.c",line 877,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 877 | GpioSetupMotor1[6].GpioPin = M1_FAULT;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[5],#17,UNC       ; [CPU_] |877| 
	.dwpsn	file "../source/InitGpio.c",line 878,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 878 | GpioSetupMotor1[6].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |878| 
	.dwpsn	file "../source/InitGpio.c",line 879,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 879 | GpioSetupMotor1[6].GpioInputOutput = GPIO_INPUT;                       
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |879| 
	.dwpsn	file "../source/InitGpio.c",line 880,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 880 | GpioSetupMotor1[6].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |880| 
	.dwpsn	file "../source/InitGpio.c",line 881,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 881 | GpioSetupMotor1[6].GpioPeripheral = 0;                                 
; 883 | //!After setting all the configurations of the pin,                    
; 884 | //!Update the configurations of the Motor1 control GPIOs               
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |881| 
	.dwpsn	file "../source/InitGpio.c",line 885,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 885 | IGConfigGpioPin(GpioSetupMotor1[0]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |885| 
        SUBB      XAR4,#35              ; [CPU_U] |885| 
        MOVZ      AR4,AR4               ; [CPU_] |885| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$110, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |885| 
        ; call occurs [#_IGConfigGpioPin] ; [] |885| 
	.dwpsn	file "../source/InitGpio.c",line 886,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 886 | IGConfigGpioPin(GpioSetupMotor1[1]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |886| 
        SUBB      XAR4,#30              ; [CPU_U] |886| 
        MOVZ      AR4,AR4               ; [CPU_] |886| 
$C$DW$111	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$111, DW_AT_low_pc(0x00)
	.dwattr $C$DW$111, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$111, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |886| 
        ; call occurs [#_IGConfigGpioPin] ; [] |886| 
	.dwpsn	file "../source/InitGpio.c",line 887,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 887 | IGConfigGpioPin(GpioSetupMotor1[2]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |887| 
        SUBB      XAR4,#25              ; [CPU_U] |887| 
        MOVZ      AR4,AR4               ; [CPU_] |887| 
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$112, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |887| 
        ; call occurs [#_IGConfigGpioPin] ; [] |887| 
	.dwpsn	file "../source/InitGpio.c",line 888,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 888 | IGConfigGpioPin(GpioSetupMotor1[3]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |888| 
        SUBB      XAR4,#20              ; [CPU_U] |888| 
        MOVZ      AR4,AR4               ; [CPU_] |888| 
$C$DW$113	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$113, DW_AT_low_pc(0x00)
	.dwattr $C$DW$113, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$113, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |888| 
        ; call occurs [#_IGConfigGpioPin] ; [] |888| 
	.dwpsn	file "../source/InitGpio.c",line 889,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 889 | IGConfigGpioPin(GpioSetupMotor1[4]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |889| 
        SUBB      XAR4,#15              ; [CPU_U] |889| 
        MOVZ      AR4,AR4               ; [CPU_] |889| 
$C$DW$114	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$114, DW_AT_low_pc(0x00)
	.dwattr $C$DW$114, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$114, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |889| 
        ; call occurs [#_IGConfigGpioPin] ; [] |889| 
	.dwpsn	file "../source/InitGpio.c",line 890,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 890 | IGConfigGpioPin(GpioSetupMotor1[5]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |890| 
        SUBB      XAR4,#10              ; [CPU_U] |890| 
        MOVZ      AR4,AR4               ; [CPU_] |890| 
$C$DW$115	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$115, DW_AT_low_pc(0x00)
	.dwattr $C$DW$115, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$115, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |890| 
        ; call occurs [#_IGConfigGpioPin] ; [] |890| 
	.dwpsn	file "../source/InitGpio.c",line 891,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 891 | IGConfigGpioPin(GpioSetupMotor1[6]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |891| 
        SUBB      XAR4,#5               ; [CPU_U] |891| 
        MOVZ      AR4,AR4               ; [CPU_] |891| 
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$116, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |891| 
        ; call occurs [#_IGConfigGpioPin] ; [] |891| 
	.dwpsn	file "../source/InitGpio.c",line 892,column 1,is_stmt,isa 0
        SUBB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$108, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$108, DW_AT_TI_end_line(0x37c)
	.dwattr $C$DW$108, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$108

	.sect	".text:_IGGpioConfig_Motor2"
	.clink
	.global	_IGGpioConfig_Motor2

$C$DW$118	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$118, DW_AT_name("IGGpioConfig_Motor2")
	.dwattr $C$DW$118, DW_AT_low_pc(_IGGpioConfig_Motor2)
	.dwattr $C$DW$118, DW_AT_high_pc(0x00)
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_IGGpioConfig_Motor2")
	.dwattr $C$DW$118, DW_AT_external
	.dwattr $C$DW$118, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$118, DW_AT_TI_begin_line(0x389)
	.dwattr $C$DW$118, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$118, DW_AT_TI_max_frame_size(-38)
	.dwpsn	file "../source/InitGpio.c",line 906,column 1,is_stmt,address _IGGpioConfig_Motor2,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_Motor2
;----------------------------------------------------------------------
; 905 | void IGGpioConfig_Motor2(void)                                         
; 907 | GpioPinConfig GpioSetupMotor2[7];                                      
; 909 | //!Set the following parameters of the Motor2 Mode 0 GPIO pin:         
; 910 | //!GPIO pin no. to M2_MODE0                                            
; 911 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 912 | //!GPIO Pin as output pin                                              
; 913 | //!GPIO pin as GPIO_PUSHPULL                                           
; 914 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_Motor2          FR SIZE:  36           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 35 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_Motor2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -38
$C$DW$119	.dwtag  DW_TAG_variable
	.dwattr $C$DW$119, DW_AT_name("GpioSetupMotor2")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_GpioSetupMotor2")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$119, DW_AT_location[DW_OP_breg20 -35]

	.dwpsn	file "../source/InitGpio.c",line 915,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 915 | GpioSetupMotor2[0].GpioPin = M2_MODE0;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[35],#13,UNC      ; [CPU_] |915| 
	.dwpsn	file "../source/InitGpio.c",line 916,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 916 | GpioSetupMotor2[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[33],#0           ; [CPU_] |916| 
	.dwpsn	file "../source/InitGpio.c",line 917,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 917 | GpioSetupMotor2[0].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[32],#1,UNC       ; [CPU_] |917| 
	.dwpsn	file "../source/InitGpio.c",line 918,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 918 | GpioSetupMotor2[0].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[34],#0           ; [CPU_] |918| 
	.dwpsn	file "../source/InitGpio.c",line 919,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 919 | GpioSetupMotor2[0].GpioPeripheral = 0;                                 
; 921 | //!Set the following parameters of the Motor2 Mode 1 GPIO pin:         
; 922 | //!GPIO pin no. to M2_MODE1                                            
; 923 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 924 | //!GPIO Pin as output pin                                              
; 925 | //!GPIO pin as GPIO_PUSHPULL                                           
; 926 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[31],#0           ; [CPU_] |919| 
	.dwpsn	file "../source/InitGpio.c",line 927,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 927 | GpioSetupMotor2[1].GpioPin = M2_MODE1;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[30],#12,UNC      ; [CPU_] |927| 
	.dwpsn	file "../source/InitGpio.c",line 928,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 928 | GpioSetupMotor2[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[28],#0           ; [CPU_] |928| 
	.dwpsn	file "../source/InitGpio.c",line 929,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 929 | GpioSetupMotor2[1].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |929| 
	.dwpsn	file "../source/InitGpio.c",line 930,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 930 | GpioSetupMotor2[1].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[29],#0           ; [CPU_] |930| 
	.dwpsn	file "../source/InitGpio.c",line 931,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 931 | GpioSetupMotor2[1].GpioPeripheral = 0;                                 
; 933 | //!Set the following parameters of the Motor2 Mode 2 GPIO pin:         
; 934 | //!GPIO pin no. to M2_MODE2                                            
; 935 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 936 | //!GPIO Pin as output pin                                              
; 937 | //!GPIO pin as GPIO_PUSHPULL                                           
; 938 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[26],#0           ; [CPU_] |931| 
	.dwpsn	file "../source/InitGpio.c",line 939,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 939 | GpioSetupMotor2[2].GpioPin = M2_MODE2;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[25],#11,UNC      ; [CPU_] |939| 
	.dwpsn	file "../source/InitGpio.c",line 940,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 940 | GpioSetupMotor2[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[23],#0           ; [CPU_] |940| 
	.dwpsn	file "../source/InitGpio.c",line 941,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 941 | GpioSetupMotor2[2].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[22],#1,UNC       ; [CPU_] |941| 
	.dwpsn	file "../source/InitGpio.c",line 942,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 942 | GpioSetupMotor2[2].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[24],#0           ; [CPU_] |942| 
	.dwpsn	file "../source/InitGpio.c",line 943,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 943 | GpioSetupMotor2[2].GpioPeripheral = 0;                                 
; 945 | //!Set the following parameters of the Motor2 Enable GPIO pin:         
; 946 | //!GPIO pin no. to M2_ENBL                                             
; 947 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 948 | //!GPIO Pin as output pin                                              
; 949 | //!GPIO pin as GPIO_PUSHPULL                                           
; 950 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[21],#0           ; [CPU_] |943| 
	.dwpsn	file "../source/InitGpio.c",line 951,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 951 | GpioSetupMotor2[3].GpioPin = M2_ENBL;                                  
;----------------------------------------------------------------------
        MOVB      *-SP[20],#15,UNC      ; [CPU_] |951| 
	.dwpsn	file "../source/InitGpio.c",line 952,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 952 | GpioSetupMotor2[3].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |952| 
	.dwpsn	file "../source/InitGpio.c",line 953,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 953 | GpioSetupMotor2[3].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |953| 
	.dwpsn	file "../source/InitGpio.c",line 954,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 954 | GpioSetupMotor2[3].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[19],#0           ; [CPU_] |954| 
	.dwpsn	file "../source/InitGpio.c",line 955,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 955 | GpioSetupMotor2[3].GpioPeripheral = 0;                                 
; 957 | //!Set the following parameters of the Motor2 Direction GPIO pin:      
; 958 | //!GPIO pin no. to M2_DIR                                              
; 959 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 960 | //!GPIO Pin as output pin                                              
; 961 | //!GPIO pin as GPIO_PUSHPULL                                           
; 962 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[16],#0           ; [CPU_] |955| 
	.dwpsn	file "../source/InitGpio.c",line 963,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 963 | GpioSetupMotor2[4].GpioPin = M2_DIR;                                   
;----------------------------------------------------------------------
        MOVB      *-SP[15],#20,UNC      ; [CPU_] |963| 
	.dwpsn	file "../source/InitGpio.c",line 964,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 964 | GpioSetupMotor2[4].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |964| 
	.dwpsn	file "../source/InitGpio.c",line 965,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 965 | GpioSetupMotor2[4].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[12],#1,UNC       ; [CPU_] |965| 
	.dwpsn	file "../source/InitGpio.c",line 966,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 966 | GpioSetupMotor2[4].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[14],#0           ; [CPU_] |966| 
	.dwpsn	file "../source/InitGpio.c",line 967,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 967 | GpioSetupMotor2[4].GpioPeripheral = 0;                                 
; 969 | //!Set the following parameters of the Motor2 Decay GPIO pin:          
; 970 | //!GPIO pin no. to M2_DECAY                                            
; 971 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 972 | //!GPIO Pin as output pin                                              
; 973 | //!GPIO pin as GPIO_PUSHPULL                                           
; 974 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[11],#0           ; [CPU_] |967| 
	.dwpsn	file "../source/InitGpio.c",line 975,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 975 | GpioSetupMotor2[5].GpioPin = M2_DECAY;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[10],#21,UNC      ; [CPU_] |975| 
	.dwpsn	file "../source/InitGpio.c",line 976,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 976 | GpioSetupMotor2[5].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |976| 
	.dwpsn	file "../source/InitGpio.c",line 977,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 977 | GpioSetupMotor2[5].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |977| 
	.dwpsn	file "../source/InitGpio.c",line 978,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 978 | GpioSetupMotor2[5].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |978| 
	.dwpsn	file "../source/InitGpio.c",line 979,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 979 | GpioSetupMotor2[5].GpioPeripheral = 0;                                 
; 981 | //!Set the following parameters of the Motor2 Fault GPIO pin:          
; 982 | //!GPIO pin no. to M2_FAULT                                            
; 983 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 984 | //!GPIO Pin as input pin                                               
; 985 | //!GPIO pin as GPIO_PUSHPULL                                           
; 986 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |979| 
	.dwpsn	file "../source/InitGpio.c",line 987,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 987 | GpioSetupMotor2[6].GpioPin = M2_FAULT;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[5],#22,UNC       ; [CPU_] |987| 
	.dwpsn	file "../source/InitGpio.c",line 988,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 988 | GpioSetupMotor2[6].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |988| 
	.dwpsn	file "../source/InitGpio.c",line 989,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 989 | GpioSetupMotor2[6].GpioInputOutput = GPIO_INPUT;                       
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |989| 
	.dwpsn	file "../source/InitGpio.c",line 990,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 990 | GpioSetupMotor2[6].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |990| 
	.dwpsn	file "../source/InitGpio.c",line 991,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 991 | GpioSetupMotor2[6].GpioPeripheral = 0;                                 
; 993 | //!After setting all the configurations of the pin,                    
; 994 | //!Update the configurations of the Motor2 control GPIOs               
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |991| 
	.dwpsn	file "../source/InitGpio.c",line 995,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 995 | IGConfigGpioPin(GpioSetupMotor2[0]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |995| 
        SUBB      XAR4,#35              ; [CPU_U] |995| 
        MOVZ      AR4,AR4               ; [CPU_] |995| 
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$120, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |995| 
        ; call occurs [#_IGConfigGpioPin] ; [] |995| 
	.dwpsn	file "../source/InitGpio.c",line 996,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 996 | IGConfigGpioPin(GpioSetupMotor2[1]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |996| 
        SUBB      XAR4,#30              ; [CPU_U] |996| 
        MOVZ      AR4,AR4               ; [CPU_] |996| 
$C$DW$121	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$121, DW_AT_low_pc(0x00)
	.dwattr $C$DW$121, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$121, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |996| 
        ; call occurs [#_IGConfigGpioPin] ; [] |996| 
	.dwpsn	file "../source/InitGpio.c",line 997,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 997 | IGConfigGpioPin(GpioSetupMotor2[2]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |997| 
        SUBB      XAR4,#25              ; [CPU_U] |997| 
        MOVZ      AR4,AR4               ; [CPU_] |997| 
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$122, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |997| 
        ; call occurs [#_IGConfigGpioPin] ; [] |997| 
	.dwpsn	file "../source/InitGpio.c",line 998,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 998 | IGConfigGpioPin(GpioSetupMotor2[3]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |998| 
        SUBB      XAR4,#20              ; [CPU_U] |998| 
        MOVZ      AR4,AR4               ; [CPU_] |998| 
$C$DW$123	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$123, DW_AT_low_pc(0x00)
	.dwattr $C$DW$123, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$123, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |998| 
        ; call occurs [#_IGConfigGpioPin] ; [] |998| 
	.dwpsn	file "../source/InitGpio.c",line 999,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 999 | IGConfigGpioPin(GpioSetupMotor2[4]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |999| 
        SUBB      XAR4,#15              ; [CPU_U] |999| 
        MOVZ      AR4,AR4               ; [CPU_] |999| 
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$124, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |999| 
        ; call occurs [#_IGConfigGpioPin] ; [] |999| 
	.dwpsn	file "../source/InitGpio.c",line 1000,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1000 | IGConfigGpioPin(GpioSetupMotor2[5]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1000| 
        SUBB      XAR4,#10              ; [CPU_U] |1000| 
        MOVZ      AR4,AR4               ; [CPU_] |1000| 
$C$DW$125	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$125, DW_AT_low_pc(0x00)
	.dwattr $C$DW$125, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$125, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1000| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1000| 
	.dwpsn	file "../source/InitGpio.c",line 1001,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1001 | IGConfigGpioPin(GpioSetupMotor2[6]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1001| 
        SUBB      XAR4,#5               ; [CPU_U] |1001| 
        MOVZ      AR4,AR4               ; [CPU_] |1001| 
$C$DW$126	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$126, DW_AT_low_pc(0x00)
	.dwattr $C$DW$126, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$126, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1001| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1001| 
	.dwpsn	file "../source/InitGpio.c",line 1002,column 1,is_stmt,isa 0
        SUBB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$127	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$127, DW_AT_low_pc(0x00)
	.dwattr $C$DW$127, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$118, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$118, DW_AT_TI_end_line(0x3ea)
	.dwattr $C$DW$118, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$118

	.sect	".text:_IGGpioConfig_Motor3"
	.clink
	.global	_IGGpioConfig_Motor3

$C$DW$128	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$128, DW_AT_name("IGGpioConfig_Motor3")
	.dwattr $C$DW$128, DW_AT_low_pc(_IGGpioConfig_Motor3)
	.dwattr $C$DW$128, DW_AT_high_pc(0x00)
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_IGGpioConfig_Motor3")
	.dwattr $C$DW$128, DW_AT_external
	.dwattr $C$DW$128, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$128, DW_AT_TI_begin_line(0x3f7)
	.dwattr $C$DW$128, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$128, DW_AT_TI_max_frame_size(-38)
	.dwpsn	file "../source/InitGpio.c",line 1016,column 1,is_stmt,address _IGGpioConfig_Motor3,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_Motor3
;----------------------------------------------------------------------
; 1015 | void IGGpioConfig_Motor3(void)                                         
; 1017 | GpioPinConfig GpioSetupMotor3[7];                                      
; 1019 | //!Set the following parameters of the Motor3 Mode 0 GPIO pin:         
; 1020 | //!GPIO pin no. to M3_MODE0                                            
; 1021 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1022 | //!GPIO Pin as output pin                                              
; 1023 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1024 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_Motor3          FR SIZE:  36           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 35 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_Motor3:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -38
$C$DW$129	.dwtag  DW_TAG_variable
	.dwattr $C$DW$129, DW_AT_name("GpioSetupMotor3")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_GpioSetupMotor3")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_breg20 -35]

	.dwpsn	file "../source/InitGpio.c",line 1025,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1025 | GpioSetupMotor3[0].GpioPin = M3_MODE0;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[35],#25,UNC      ; [CPU_] |1025| 
	.dwpsn	file "../source/InitGpio.c",line 1026,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1026 | GpioSetupMotor3[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[33],#0           ; [CPU_] |1026| 
	.dwpsn	file "../source/InitGpio.c",line 1027,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1027 | GpioSetupMotor3[0].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[32],#1,UNC       ; [CPU_] |1027| 
	.dwpsn	file "../source/InitGpio.c",line 1028,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1028 | GpioSetupMotor3[0].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[34],#0           ; [CPU_] |1028| 
	.dwpsn	file "../source/InitGpio.c",line 1029,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1029 | GpioSetupMotor3[0].GpioPeripheral = 0;                                 
; 1031 | //!Set the following parameters of the Motor3 Mode 1 GPIO pin:         
; 1032 | //!GPIO pin no. to M3_MODE1                                            
; 1033 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1034 | //!GPIO Pin as output pin                                              
; 1035 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1036 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[31],#0           ; [CPU_] |1029| 
	.dwpsn	file "../source/InitGpio.c",line 1037,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1037 | GpioSetupMotor3[1].GpioPin = M3_MODE1;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[30],#24,UNC      ; [CPU_] |1037| 
	.dwpsn	file "../source/InitGpio.c",line 1038,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1038 | GpioSetupMotor3[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[28],#0           ; [CPU_] |1038| 
	.dwpsn	file "../source/InitGpio.c",line 1039,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1039 | GpioSetupMotor3[1].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |1039| 
	.dwpsn	file "../source/InitGpio.c",line 1040,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1040 | GpioSetupMotor3[1].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[29],#0           ; [CPU_] |1040| 
	.dwpsn	file "../source/InitGpio.c",line 1041,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1041 | GpioSetupMotor3[1].GpioPeripheral = 0;                                 
; 1043 | //!Set the following parameters of the Motor3 Mode 2 GPIO pin:         
; 1044 | //!GPIO pin no. to M3_MODE2                                            
; 1045 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1046 | //!GPIO Pin as output pin                                              
; 1047 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1048 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[26],#0           ; [CPU_] |1041| 
	.dwpsn	file "../source/InitGpio.c",line 1049,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1049 | GpioSetupMotor3[2].GpioPin = M3_MODE2;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[25],#23,UNC      ; [CPU_] |1049| 
	.dwpsn	file "../source/InitGpio.c",line 1050,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1050 | GpioSetupMotor3[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[23],#0           ; [CPU_] |1050| 
	.dwpsn	file "../source/InitGpio.c",line 1051,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1051 | GpioSetupMotor3[2].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[22],#1,UNC       ; [CPU_] |1051| 
	.dwpsn	file "../source/InitGpio.c",line 1052,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1052 | GpioSetupMotor3[2].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[24],#0           ; [CPU_] |1052| 
	.dwpsn	file "../source/InitGpio.c",line 1053,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1053 | GpioSetupMotor3[2].GpioPeripheral = 0;                                 
; 1055 | //!Set the following parameters of the Motor3 Enable GPIO pin:         
; 1056 | //!GPIO pin no. to M3_ENBL                                             
; 1057 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1058 | //!GPIO Pin as output pin                                              
; 1059 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1060 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[21],#0           ; [CPU_] |1053| 
	.dwpsn	file "../source/InitGpio.c",line 1061,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1061 | GpioSetupMotor3[3].GpioPin = M3_ENBL;                                  
;----------------------------------------------------------------------
        MOVB      *-SP[20],#27,UNC      ; [CPU_] |1061| 
	.dwpsn	file "../source/InitGpio.c",line 1062,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1062 | GpioSetupMotor3[3].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |1062| 
	.dwpsn	file "../source/InitGpio.c",line 1063,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1063 | GpioSetupMotor3[3].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |1063| 
	.dwpsn	file "../source/InitGpio.c",line 1064,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1064 | GpioSetupMotor3[3].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[19],#0           ; [CPU_] |1064| 
	.dwpsn	file "../source/InitGpio.c",line 1065,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1065 | GpioSetupMotor3[3].GpioPeripheral = 0;                                 
; 1067 | //!Set the following parameters of the Motor3 Direction GPIO pin:      
; 1068 | //!GPIO pin no. to M3_DIR                                              
; 1069 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1070 | //!GPIO Pin as output pin                                              
; 1071 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1072 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[16],#0           ; [CPU_] |1065| 
	.dwpsn	file "../source/InitGpio.c",line 1073,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1073 | GpioSetupMotor3[4].GpioPin = M3_DIR;                                   
;----------------------------------------------------------------------
        MOVB      *-SP[15],#28,UNC      ; [CPU_] |1073| 
	.dwpsn	file "../source/InitGpio.c",line 1074,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1074 | GpioSetupMotor3[4].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |1074| 
	.dwpsn	file "../source/InitGpio.c",line 1075,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1075 | GpioSetupMotor3[4].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[12],#1,UNC       ; [CPU_] |1075| 
	.dwpsn	file "../source/InitGpio.c",line 1076,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1076 | GpioSetupMotor3[4].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[14],#0           ; [CPU_] |1076| 
	.dwpsn	file "../source/InitGpio.c",line 1077,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1077 | GpioSetupMotor3[4].GpioPeripheral = 0;                                 
; 1079 | //!Set the following parameters of the Motor3 Decay GPIO pin:          
; 1080 | //!GPIO pin no. to M3_DECAY                                            
; 1081 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1082 | //!GPIO Pin as output pin                                              
; 1083 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1084 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[11],#0           ; [CPU_] |1077| 
	.dwpsn	file "../source/InitGpio.c",line 1085,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1085 | GpioSetupMotor3[5].GpioPin = M3_DECAY;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[10],#33,UNC      ; [CPU_] |1085| 
	.dwpsn	file "../source/InitGpio.c",line 1086,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1086 | GpioSetupMotor3[5].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |1086| 
	.dwpsn	file "../source/InitGpio.c",line 1087,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1087 | GpioSetupMotor3[5].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |1087| 
	.dwpsn	file "../source/InitGpio.c",line 1088,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1088 | GpioSetupMotor3[5].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |1088| 
	.dwpsn	file "../source/InitGpio.c",line 1089,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1089 | GpioSetupMotor3[5].GpioPeripheral = 0;                                 
; 1091 | //!Set the following parameters of the Motor3 Fault GPIO pin:          
; 1092 | //!GPIO pin no. to M3_FAULT                                            
; 1093 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1094 | //!GPIO Pin as input pin                                               
; 1095 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1096 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1089| 
	.dwpsn	file "../source/InitGpio.c",line 1097,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1097 | GpioSetupMotor3[6].GpioPin = M3_FAULT;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[5],#19,UNC       ; [CPU_] |1097| 
	.dwpsn	file "../source/InitGpio.c",line 1098,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1098 | GpioSetupMotor3[6].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1098| 
	.dwpsn	file "../source/InitGpio.c",line 1099,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1099 | GpioSetupMotor3[6].GpioInputOutput = GPIO_INPUT;                       
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |1099| 
	.dwpsn	file "../source/InitGpio.c",line 1100,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1100 | GpioSetupMotor3[6].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |1100| 
	.dwpsn	file "../source/InitGpio.c",line 1101,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1101 | GpioSetupMotor3[6].GpioPeripheral = 0;                                 
; 1103 | //!After setting all the configurations of the pin,                    
; 1104 | //!Update the configurations of the Motor3 control GPIOs               
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |1101| 
	.dwpsn	file "../source/InitGpio.c",line 1105,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1105 | IGConfigGpioPin(GpioSetupMotor3[0]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1105| 
        SUBB      XAR4,#35              ; [CPU_U] |1105| 
        MOVZ      AR4,AR4               ; [CPU_] |1105| 
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$130, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1105| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1105| 
	.dwpsn	file "../source/InitGpio.c",line 1106,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1106 | IGConfigGpioPin(GpioSetupMotor3[1]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1106| 
        SUBB      XAR4,#30              ; [CPU_U] |1106| 
        MOVZ      AR4,AR4               ; [CPU_] |1106| 
$C$DW$131	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$131, DW_AT_low_pc(0x00)
	.dwattr $C$DW$131, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$131, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1106| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1106| 
	.dwpsn	file "../source/InitGpio.c",line 1107,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1107 | IGConfigGpioPin(GpioSetupMotor3[2]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1107| 
        SUBB      XAR4,#25              ; [CPU_U] |1107| 
        MOVZ      AR4,AR4               ; [CPU_] |1107| 
$C$DW$132	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$132, DW_AT_low_pc(0x00)
	.dwattr $C$DW$132, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$132, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1107| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1107| 
	.dwpsn	file "../source/InitGpio.c",line 1108,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1108 | IGConfigGpioPin(GpioSetupMotor3[3]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1108| 
        SUBB      XAR4,#20              ; [CPU_U] |1108| 
        MOVZ      AR4,AR4               ; [CPU_] |1108| 
$C$DW$133	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$133, DW_AT_low_pc(0x00)
	.dwattr $C$DW$133, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$133, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1108| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1108| 
	.dwpsn	file "../source/InitGpio.c",line 1109,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1109 | IGConfigGpioPin(GpioSetupMotor3[4]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1109| 
        SUBB      XAR4,#15              ; [CPU_U] |1109| 
        MOVZ      AR4,AR4               ; [CPU_] |1109| 
$C$DW$134	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$134, DW_AT_low_pc(0x00)
	.dwattr $C$DW$134, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$134, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1109| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1109| 
	.dwpsn	file "../source/InitGpio.c",line 1110,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1110 | IGConfigGpioPin(GpioSetupMotor3[5]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1110| 
        SUBB      XAR4,#10              ; [CPU_U] |1110| 
        MOVZ      AR4,AR4               ; [CPU_] |1110| 
$C$DW$135	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$135, DW_AT_low_pc(0x00)
	.dwattr $C$DW$135, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$135, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1110| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1110| 
	.dwpsn	file "../source/InitGpio.c",line 1111,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1111 | IGConfigGpioPin(GpioSetupMotor3[6]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1111| 
        SUBB      XAR4,#5               ; [CPU_U] |1111| 
        MOVZ      AR4,AR4               ; [CPU_] |1111| 
$C$DW$136	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$136, DW_AT_low_pc(0x00)
	.dwattr $C$DW$136, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$136, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1111| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1111| 
	.dwpsn	file "../source/InitGpio.c",line 1112,column 1,is_stmt,isa 0
        SUBB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$137	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$137, DW_AT_low_pc(0x00)
	.dwattr $C$DW$137, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$128, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$128, DW_AT_TI_end_line(0x458)
	.dwattr $C$DW$128, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$128

	.sect	".text:_IGGpioConfig_Motor4"
	.clink
	.global	_IGGpioConfig_Motor4

$C$DW$138	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$138, DW_AT_name("IGGpioConfig_Motor4")
	.dwattr $C$DW$138, DW_AT_low_pc(_IGGpioConfig_Motor4)
	.dwattr $C$DW$138, DW_AT_high_pc(0x00)
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_IGGpioConfig_Motor4")
	.dwattr $C$DW$138, DW_AT_external
	.dwattr $C$DW$138, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$138, DW_AT_TI_begin_line(0x465)
	.dwattr $C$DW$138, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$138, DW_AT_TI_max_frame_size(-38)
	.dwpsn	file "../source/InitGpio.c",line 1126,column 1,is_stmt,address _IGGpioConfig_Motor4,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_Motor4
;----------------------------------------------------------------------
; 1125 | void IGGpioConfig_Motor4(void)                                         
; 1127 | GpioPinConfig GpioSetupMotor4[7];                                      
; 1129 | //!Set the following parameters of the Motor4 Mode 0 GPIO pin:         
; 1130 | //!GPIO pin no. to M4_MODE0                                            
; 1131 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1132 | //!GPIO Pin as output pin                                              
; 1133 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1134 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_Motor4          FR SIZE:  36           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 35 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_Motor4:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -38
$C$DW$139	.dwtag  DW_TAG_variable
	.dwattr $C$DW$139, DW_AT_name("GpioSetupMotor4")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_GpioSetupMotor4")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$139, DW_AT_location[DW_OP_breg20 -35]

	.dwpsn	file "../source/InitGpio.c",line 1135,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1135 | GpioSetupMotor4[0].GpioPin = M4_MODE0;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[35],#37,UNC      ; [CPU_] |1135| 
	.dwpsn	file "../source/InitGpio.c",line 1136,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1136 | GpioSetupMotor4[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[33],#0           ; [CPU_] |1136| 
	.dwpsn	file "../source/InitGpio.c",line 1137,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1137 | GpioSetupMotor4[0].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[32],#1,UNC       ; [CPU_] |1137| 
	.dwpsn	file "../source/InitGpio.c",line 1138,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1138 | GpioSetupMotor4[0].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[34],#0           ; [CPU_] |1138| 
	.dwpsn	file "../source/InitGpio.c",line 1139,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1139 | GpioSetupMotor4[0].GpioPeripheral = 0;                                 
; 1141 | //!Set the following parameters of the Motor4 Mode 1 GPIO pin:         
; 1142 | //!GPIO pin no. to M4_MODE1                                            
; 1143 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1144 | //!GPIO Pin as output pin                                              
; 1145 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1146 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[31],#0           ; [CPU_] |1139| 
	.dwpsn	file "../source/InitGpio.c",line 1147,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1147 | GpioSetupMotor4[1].GpioPin = M4_MODE1;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[30],#35,UNC      ; [CPU_] |1147| 
	.dwpsn	file "../source/InitGpio.c",line 1148,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1148 | GpioSetupMotor4[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[28],#0           ; [CPU_] |1148| 
	.dwpsn	file "../source/InitGpio.c",line 1149,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1149 | GpioSetupMotor4[1].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |1149| 
	.dwpsn	file "../source/InitGpio.c",line 1150,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1150 | GpioSetupMotor4[1].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[29],#0           ; [CPU_] |1150| 
	.dwpsn	file "../source/InitGpio.c",line 1151,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1151 | GpioSetupMotor4[1].GpioPeripheral = 0;                                 
; 1153 | //!Set the following parameters of the Motor4 Mode 2 GPIO pin:         
; 1154 | //!GPIO pin no. to M4_MODE2                                            
; 1155 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1156 | //!GPIO Pin as output pin                                              
; 1157 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1158 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[26],#0           ; [CPU_] |1151| 
	.dwpsn	file "../source/InitGpio.c",line 1159,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1159 | GpioSetupMotor4[2].GpioPin = M4_MODE2;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[25],#34,UNC      ; [CPU_] |1159| 
	.dwpsn	file "../source/InitGpio.c",line 1160,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1160 | GpioSetupMotor4[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[23],#0           ; [CPU_] |1160| 
	.dwpsn	file "../source/InitGpio.c",line 1161,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1161 | GpioSetupMotor4[2].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[22],#1,UNC       ; [CPU_] |1161| 
	.dwpsn	file "../source/InitGpio.c",line 1162,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1162 | GpioSetupMotor4[2].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[24],#0           ; [CPU_] |1162| 
	.dwpsn	file "../source/InitGpio.c",line 1163,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1163 | GpioSetupMotor4[2].GpioPeripheral = 0;                                 
; 1165 | //!Set the following parameters of the Motor4 Enable GPIO pin:         
; 1166 | //!GPIO pin no. to M4_ENBL                                             
; 1167 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1168 | //!GPIO Pin as output pin                                              
; 1169 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1170 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[21],#0           ; [CPU_] |1163| 
	.dwpsn	file "../source/InitGpio.c",line 1171,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1171 | GpioSetupMotor4[3].GpioPin = M4_ENBL;                                  
;----------------------------------------------------------------------
        MOVB      *-SP[20],#91,UNC      ; [CPU_] |1171| 
	.dwpsn	file "../source/InitGpio.c",line 1172,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1172 | GpioSetupMotor4[3].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |1172| 
	.dwpsn	file "../source/InitGpio.c",line 1173,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1173 | GpioSetupMotor4[3].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |1173| 
	.dwpsn	file "../source/InitGpio.c",line 1174,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1174 | GpioSetupMotor4[3].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[19],#0           ; [CPU_] |1174| 
	.dwpsn	file "../source/InitGpio.c",line 1175,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1175 | GpioSetupMotor4[3].GpioPeripheral = 0;                                 
; 1177 | //!Set the following parameters of the Motor4 Direction GPIO pin:      
; 1178 | //!GPIO pin no. to M4_DIR                                              
; 1179 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1180 | //!GPIO Pin as output pin                                              
; 1181 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1182 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[16],#0           ; [CPU_] |1175| 
	.dwpsn	file "../source/InitGpio.c",line 1183,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1183 | GpioSetupMotor4[4].GpioPin = M4_DIR;                                   
;----------------------------------------------------------------------
        MOVB      *-SP[15],#150,UNC     ; [CPU_] |1183| 
	.dwpsn	file "../source/InitGpio.c",line 1184,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1184 | GpioSetupMotor4[4].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |1184| 
	.dwpsn	file "../source/InitGpio.c",line 1185,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1185 | GpioSetupMotor4[4].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[12],#1,UNC       ; [CPU_] |1185| 
	.dwpsn	file "../source/InitGpio.c",line 1186,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1186 | GpioSetupMotor4[4].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[14],#0           ; [CPU_] |1186| 
	.dwpsn	file "../source/InitGpio.c",line 1187,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1187 | GpioSetupMotor4[4].GpioPeripheral = 0;                                 
; 1189 | //!Set the following parameters of the Motor4 Decay GPIO pin:          
; 1190 | //!GPIO pin no. to M4_DECAY                                            
; 1191 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1192 | //!GPIO Pin as output pin                                              
; 1193 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1194 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[11],#0           ; [CPU_] |1187| 
	.dwpsn	file "../source/InitGpio.c",line 1195,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1195 | GpioSetupMotor4[5].GpioPin = M4_DECAY;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[10],#95,UNC      ; [CPU_] |1195| 
	.dwpsn	file "../source/InitGpio.c",line 1196,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1196 | GpioSetupMotor4[5].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |1196| 
	.dwpsn	file "../source/InitGpio.c",line 1197,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1197 | GpioSetupMotor4[5].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |1197| 
	.dwpsn	file "../source/InitGpio.c",line 1198,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1198 | GpioSetupMotor4[5].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |1198| 
	.dwpsn	file "../source/InitGpio.c",line 1199,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1199 | GpioSetupMotor4[5].GpioPeripheral = 0;                                 
; 1201 | //!Set the following parameters of the Motor4 Fault GPIO pin:          
; 1202 | //!GPIO pin no. to M4_FAULT                                            
; 1203 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1204 | //!GPIO Pin as input pin                                               
; 1205 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1206 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1199| 
	.dwpsn	file "../source/InitGpio.c",line 1207,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1207 | GpioSetupMotor4[6].GpioPin = M4_FAULT;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[5],#96,UNC       ; [CPU_] |1207| 
	.dwpsn	file "../source/InitGpio.c",line 1208,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1208 | GpioSetupMotor4[6].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1208| 
	.dwpsn	file "../source/InitGpio.c",line 1209,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1209 | GpioSetupMotor4[6].GpioInputOutput = GPIO_INPUT;                       
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |1209| 
	.dwpsn	file "../source/InitGpio.c",line 1210,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1210 | GpioSetupMotor4[6].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |1210| 
	.dwpsn	file "../source/InitGpio.c",line 1211,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1211 | GpioSetupMotor4[6].GpioPeripheral = 0;                                 
; 1213 | //!After setting all the configurations of the pin,                    
; 1214 | //!Update the configurations of the Motor4 control GPIOs               
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |1211| 
	.dwpsn	file "../source/InitGpio.c",line 1215,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1215 | IGConfigGpioPin(GpioSetupMotor4[0]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1215| 
        SUBB      XAR4,#35              ; [CPU_U] |1215| 
        MOVZ      AR4,AR4               ; [CPU_] |1215| 
$C$DW$140	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$140, DW_AT_low_pc(0x00)
	.dwattr $C$DW$140, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$140, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1215| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1215| 
	.dwpsn	file "../source/InitGpio.c",line 1216,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1216 | IGConfigGpioPin(GpioSetupMotor4[1]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1216| 
        SUBB      XAR4,#30              ; [CPU_U] |1216| 
        MOVZ      AR4,AR4               ; [CPU_] |1216| 
$C$DW$141	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$141, DW_AT_low_pc(0x00)
	.dwattr $C$DW$141, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$141, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1216| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1216| 
	.dwpsn	file "../source/InitGpio.c",line 1217,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1217 | IGConfigGpioPin(GpioSetupMotor4[2]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1217| 
        SUBB      XAR4,#25              ; [CPU_U] |1217| 
        MOVZ      AR4,AR4               ; [CPU_] |1217| 
$C$DW$142	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$142, DW_AT_low_pc(0x00)
	.dwattr $C$DW$142, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$142, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1217| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1217| 
	.dwpsn	file "../source/InitGpio.c",line 1218,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1218 | IGConfigGpioPin(GpioSetupMotor4[3]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1218| 
        SUBB      XAR4,#20              ; [CPU_U] |1218| 
        MOVZ      AR4,AR4               ; [CPU_] |1218| 
$C$DW$143	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$143, DW_AT_low_pc(0x00)
	.dwattr $C$DW$143, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$143, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1218| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1218| 
	.dwpsn	file "../source/InitGpio.c",line 1219,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1219 | IGConfigGpioPin(GpioSetupMotor4[4]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1219| 
        SUBB      XAR4,#15              ; [CPU_U] |1219| 
        MOVZ      AR4,AR4               ; [CPU_] |1219| 
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$144, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1219| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1219| 
	.dwpsn	file "../source/InitGpio.c",line 1220,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1220 | IGConfigGpioPin(GpioSetupMotor4[5]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1220| 
        SUBB      XAR4,#10              ; [CPU_U] |1220| 
        MOVZ      AR4,AR4               ; [CPU_] |1220| 
$C$DW$145	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$145, DW_AT_low_pc(0x00)
	.dwattr $C$DW$145, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$145, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1220| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1220| 
	.dwpsn	file "../source/InitGpio.c",line 1221,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1221 | IGConfigGpioPin(GpioSetupMotor4[6]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1221| 
        SUBB      XAR4,#5               ; [CPU_U] |1221| 
        MOVZ      AR4,AR4               ; [CPU_] |1221| 
$C$DW$146	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$146, DW_AT_low_pc(0x00)
	.dwattr $C$DW$146, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$146, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1221| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1221| 
	.dwpsn	file "../source/InitGpio.c",line 1222,column 1,is_stmt,isa 0
        SUBB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$147	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$147, DW_AT_low_pc(0x00)
	.dwattr $C$DW$147, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$138, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$138, DW_AT_TI_end_line(0x4c6)
	.dwattr $C$DW$138, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$138

	.sect	".text:_IGGpioConfig_Motor5"
	.clink
	.global	_IGGpioConfig_Motor5

$C$DW$148	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$148, DW_AT_name("IGGpioConfig_Motor5")
	.dwattr $C$DW$148, DW_AT_low_pc(_IGGpioConfig_Motor5)
	.dwattr $C$DW$148, DW_AT_high_pc(0x00)
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_IGGpioConfig_Motor5")
	.dwattr $C$DW$148, DW_AT_external
	.dwattr $C$DW$148, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$148, DW_AT_TI_begin_line(0x4d3)
	.dwattr $C$DW$148, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$148, DW_AT_TI_max_frame_size(-38)
	.dwpsn	file "../source/InitGpio.c",line 1236,column 1,is_stmt,address _IGGpioConfig_Motor5,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_Motor5
;----------------------------------------------------------------------
; 1235 | void IGGpioConfig_Motor5(void)                                         
; 1237 | GpioPinConfig GpioSetupMotor5[7];                                      
; 1239 | //!Set the following parameters of the Motor5 Mode 0 GPIO pin:         
; 1240 | //!GPIO pin no. to M5_MODE0                                            
; 1241 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1242 | //!GPIO Pin as output pin                                              
; 1243 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1244 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_Motor5          FR SIZE:  36           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 35 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_Motor5:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -38
$C$DW$149	.dwtag  DW_TAG_variable
	.dwattr $C$DW$149, DW_AT_name("GpioSetupMotor5")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_GpioSetupMotor5")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$149, DW_AT_location[DW_OP_breg20 -35]

	.dwpsn	file "../source/InitGpio.c",line 1245,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1245 | GpioSetupMotor5[0].GpioPin = M5_MODE0;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[35],#99,UNC      ; [CPU_] |1245| 
	.dwpsn	file "../source/InitGpio.c",line 1246,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1246 | GpioSetupMotor5[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[33],#0           ; [CPU_] |1246| 
	.dwpsn	file "../source/InitGpio.c",line 1247,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1247 | GpioSetupMotor5[0].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[32],#1,UNC       ; [CPU_] |1247| 
	.dwpsn	file "../source/InitGpio.c",line 1248,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1248 | GpioSetupMotor5[0].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[34],#0           ; [CPU_] |1248| 
	.dwpsn	file "../source/InitGpio.c",line 1249,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1249 | GpioSetupMotor5[0].GpioPeripheral = 0;                                 
; 1251 | //!Set the following parameters of the Motor5 Mode 1 GPIO pin:         
; 1252 | //!GPIO pin no. to M5_MODE1                                            
; 1253 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1254 | //!GPIO Pin as output pin                                              
; 1255 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1256 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[31],#0           ; [CPU_] |1249| 
	.dwpsn	file "../source/InitGpio.c",line 1257,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1257 | GpioSetupMotor5[1].GpioPin = M5_MODE1;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[30],#98,UNC      ; [CPU_] |1257| 
	.dwpsn	file "../source/InitGpio.c",line 1258,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1258 | GpioSetupMotor5[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[28],#0           ; [CPU_] |1258| 
	.dwpsn	file "../source/InitGpio.c",line 1259,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1259 | GpioSetupMotor5[1].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |1259| 
	.dwpsn	file "../source/InitGpio.c",line 1260,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1260 | GpioSetupMotor5[1].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[29],#0           ; [CPU_] |1260| 
	.dwpsn	file "../source/InitGpio.c",line 1261,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1261 | GpioSetupMotor5[1].GpioPeripheral = 0;                                 
; 1263 | //!Set the following parameters of the Motor5 Mode 2 GPIO pin:         
; 1264 | //!GPIO pin no. to M5_MODE2                                            
; 1265 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1266 | //!GPIO Pin as output pin                                              
; 1267 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1268 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[26],#0           ; [CPU_] |1261| 
	.dwpsn	file "../source/InitGpio.c",line 1269,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1269 | GpioSetupMotor5[2].GpioPin = M5_MODE2;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[25],#97,UNC      ; [CPU_] |1269| 
	.dwpsn	file "../source/InitGpio.c",line 1270,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1270 | GpioSetupMotor5[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[23],#0           ; [CPU_] |1270| 
	.dwpsn	file "../source/InitGpio.c",line 1271,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1271 | GpioSetupMotor5[2].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[22],#1,UNC       ; [CPU_] |1271| 
	.dwpsn	file "../source/InitGpio.c",line 1272,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1272 | GpioSetupMotor5[2].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[24],#0           ; [CPU_] |1272| 
	.dwpsn	file "../source/InitGpio.c",line 1273,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1273 | GpioSetupMotor5[2].GpioPeripheral = 0;                                 
; 1275 | //!Set the following parameters of the Motor5 Enable GPIO pin:         
; 1276 | //!GPIO pin no. to M5_ENBL                                             
; 1277 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1278 | //!GPIO Pin as output pin                                              
; 1279 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1280 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[21],#0           ; [CPU_] |1273| 
	.dwpsn	file "../source/InitGpio.c",line 1281,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1281 | GpioSetupMotor5[3].GpioPin = M5_ENBL;                                  
;----------------------------------------------------------------------
        MOVB      *-SP[20],#101,UNC     ; [CPU_] |1281| 
	.dwpsn	file "../source/InitGpio.c",line 1282,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1282 | GpioSetupMotor5[3].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[18],#0           ; [CPU_] |1282| 
	.dwpsn	file "../source/InitGpio.c",line 1283,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1283 | GpioSetupMotor5[3].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |1283| 
	.dwpsn	file "../source/InitGpio.c",line 1284,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1284 | GpioSetupMotor5[3].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[19],#0           ; [CPU_] |1284| 
	.dwpsn	file "../source/InitGpio.c",line 1285,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1285 | GpioSetupMotor5[3].GpioPeripheral = 0;                                 
; 1287 | //!Set the following parameters of the Motor5 Direction GPIO pin:      
; 1288 | //!GPIO pin no. to M5_DIR                                              
; 1289 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1290 | //!GPIO Pin as output pin                                              
; 1291 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1292 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[16],#0           ; [CPU_] |1285| 
	.dwpsn	file "../source/InitGpio.c",line 1293,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1293 | GpioSetupMotor5[4].GpioPin = M5_DIR;                                   
;----------------------------------------------------------------------
        MOVB      *-SP[15],#102,UNC     ; [CPU_] |1293| 
	.dwpsn	file "../source/InitGpio.c",line 1294,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1294 | GpioSetupMotor5[4].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |1294| 
	.dwpsn	file "../source/InitGpio.c",line 1295,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1295 | GpioSetupMotor5[4].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[12],#1,UNC       ; [CPU_] |1295| 
	.dwpsn	file "../source/InitGpio.c",line 1296,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1296 | GpioSetupMotor5[4].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[14],#0           ; [CPU_] |1296| 
	.dwpsn	file "../source/InitGpio.c",line 1297,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1297 | GpioSetupMotor5[4].GpioPeripheral = 0;                                 
; 1299 | //!Set the following parameters of the Motor5 Decay GPIO pin:          
; 1300 | //!GPIO pin no. to M5_DECAY                                            
; 1301 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1302 | //!GPIO Pin as output pin                                              
; 1303 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1304 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[11],#0           ; [CPU_] |1297| 
	.dwpsn	file "../source/InitGpio.c",line 1305,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1305 | GpioSetupMotor5[5].GpioPin = M5_DECAY;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[10],#103,UNC     ; [CPU_] |1305| 
	.dwpsn	file "../source/InitGpio.c",line 1306,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1306 | GpioSetupMotor5[5].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |1306| 
	.dwpsn	file "../source/InitGpio.c",line 1307,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1307 | GpioSetupMotor5[5].GpioInputOutput = GPIO_OUTPUT;                      
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |1307| 
	.dwpsn	file "../source/InitGpio.c",line 1308,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1308 | GpioSetupMotor5[5].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |1308| 
	.dwpsn	file "../source/InitGpio.c",line 1309,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1309 | GpioSetupMotor5[5].GpioPeripheral = 0;                                 
; 1311 | //!Set the following parameters of the Motor5 Fault GPIO pin:          
; 1312 | //!GPIO pin no. to M5_FAULT                                            
; 1313 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1314 | //!GPIO Pin as input pin                                               
; 1315 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1316 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1309| 
	.dwpsn	file "../source/InitGpio.c",line 1317,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1317 | GpioSetupMotor5[6].GpioPin = M5_FAULT;                                 
;----------------------------------------------------------------------
        MOVB      *-SP[5],#94,UNC       ; [CPU_] |1317| 
	.dwpsn	file "../source/InitGpio.c",line 1318,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1318 | GpioSetupMotor5[6].GpioCpuMuxSel = GPIO_MUX_CPU1;                      
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1318| 
	.dwpsn	file "../source/InitGpio.c",line 1319,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1319 | GpioSetupMotor5[6].GpioInputOutput = GPIO_INPUT;                       
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |1319| 
	.dwpsn	file "../source/InitGpio.c",line 1320,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1320 | GpioSetupMotor5[6].GpioFlags = GPIO_PUSHPULL;                          
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |1320| 
	.dwpsn	file "../source/InitGpio.c",line 1321,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1321 | GpioSetupMotor5[6].GpioPeripheral = 0;                                 
; 1323 | //!After setting all the configurations of the pin,                    
; 1324 | //!Update the configurations of the Motor5 control GPIOs               
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |1321| 
	.dwpsn	file "../source/InitGpio.c",line 1325,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1325 | IGConfigGpioPin(GpioSetupMotor5[0]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1325| 
        SUBB      XAR4,#35              ; [CPU_U] |1325| 
        MOVZ      AR4,AR4               ; [CPU_] |1325| 
$C$DW$150	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$150, DW_AT_low_pc(0x00)
	.dwattr $C$DW$150, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$150, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1325| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1325| 
	.dwpsn	file "../source/InitGpio.c",line 1326,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1326 | IGConfigGpioPin(GpioSetupMotor5[1]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1326| 
        SUBB      XAR4,#30              ; [CPU_U] |1326| 
        MOVZ      AR4,AR4               ; [CPU_] |1326| 
$C$DW$151	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$151, DW_AT_low_pc(0x00)
	.dwattr $C$DW$151, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$151, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1326| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1326| 
	.dwpsn	file "../source/InitGpio.c",line 1327,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1327 | IGConfigGpioPin(GpioSetupMotor5[2]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1327| 
        SUBB      XAR4,#25              ; [CPU_U] |1327| 
        MOVZ      AR4,AR4               ; [CPU_] |1327| 
$C$DW$152	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$152, DW_AT_low_pc(0x00)
	.dwattr $C$DW$152, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$152, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1327| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1327| 
	.dwpsn	file "../source/InitGpio.c",line 1328,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1328 | IGConfigGpioPin(GpioSetupMotor5[3]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1328| 
        SUBB      XAR4,#20              ; [CPU_U] |1328| 
        MOVZ      AR4,AR4               ; [CPU_] |1328| 
$C$DW$153	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$153, DW_AT_low_pc(0x00)
	.dwattr $C$DW$153, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$153, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1328| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1328| 
	.dwpsn	file "../source/InitGpio.c",line 1329,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1329 | IGConfigGpioPin(GpioSetupMotor5[4]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1329| 
        SUBB      XAR4,#15              ; [CPU_U] |1329| 
        MOVZ      AR4,AR4               ; [CPU_] |1329| 
$C$DW$154	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$154, DW_AT_low_pc(0x00)
	.dwattr $C$DW$154, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$154, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1329| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1329| 
	.dwpsn	file "../source/InitGpio.c",line 1330,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1330 | IGConfigGpioPin(GpioSetupMotor5[5]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1330| 
        SUBB      XAR4,#10              ; [CPU_U] |1330| 
        MOVZ      AR4,AR4               ; [CPU_] |1330| 
$C$DW$155	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$155, DW_AT_low_pc(0x00)
	.dwattr $C$DW$155, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$155, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1330| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1330| 
	.dwpsn	file "../source/InitGpio.c",line 1331,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1331 | IGConfigGpioPin(GpioSetupMotor5[6]);                                   
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1331| 
        SUBB      XAR4,#5               ; [CPU_U] |1331| 
        MOVZ      AR4,AR4               ; [CPU_] |1331| 
$C$DW$156	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$156, DW_AT_low_pc(0x00)
	.dwattr $C$DW$156, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$156, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1331| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1331| 
	.dwpsn	file "../source/InitGpio.c",line 1332,column 1,is_stmt,isa 0
        SUBB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$157	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$157, DW_AT_low_pc(0x00)
	.dwattr $C$DW$157, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$148, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$148, DW_AT_TI_end_line(0x534)
	.dwattr $C$DW$148, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$148

	.sect	".text:_IGGpioConfig_HighVoltPin"
	.clink
	.global	_IGGpioConfig_HighVoltPin

$C$DW$158	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$158, DW_AT_name("IGGpioConfig_HighVoltPin")
	.dwattr $C$DW$158, DW_AT_low_pc(_IGGpioConfig_HighVoltPin)
	.dwattr $C$DW$158, DW_AT_high_pc(0x00)
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_IGGpioConfig_HighVoltPin")
	.dwattr $C$DW$158, DW_AT_external
	.dwattr $C$DW$158, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$158, DW_AT_TI_begin_line(0x542)
	.dwattr $C$DW$158, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$158, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "../source/InitGpio.c",line 1347,column 1,is_stmt,address _IGGpioConfig_HighVoltPin,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_HighVoltPin
;----------------------------------------------------------------------
; 1346 | void IGGpioConfig_HighVoltPin(void)                                    
; 1348 | //GpioPinConfig GpioSetupHV[2];                                        
; 1350 | GpioPinConfig GpioSetupHV[3];                                          
; 1352 | //!Set the following parameters of the High Voltage GPIO pin:          
; 1353 | //!GPIO pin no. to HIGH_VOLTAGE_SELECT_GPIO_OUTPUT                     
; 1354 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1355 | //!GPIO Pin as output pin                                              
; 1356 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1357 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_HighVoltPin     FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 15 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_HighVoltPin:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$159	.dwtag  DW_TAG_variable
	.dwattr $C$DW$159, DW_AT_name("GpioSetupHV")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_GpioSetupHV")
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$159, DW_AT_location[DW_OP_breg20 -15]

	.dwpsn	file "../source/InitGpio.c",line 1358,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1358 | GpioSetupHV[0].GpioPin = HIGH_VOLTAGE_SELECT_GPIO_OUTPUT;              
;----------------------------------------------------------------------
        MOVB      *-SP[15],#161,UNC     ; [CPU_] |1358| 
	.dwpsn	file "../source/InitGpio.c",line 1359,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1359 | GpioSetupHV[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                          
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |1359| 
	.dwpsn	file "../source/InitGpio.c",line 1360,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1360 | GpioSetupHV[0].GpioInputOutput = GPIO_OUTPUT;                          
;----------------------------------------------------------------------
        MOVB      *-SP[12],#1,UNC       ; [CPU_] |1360| 
	.dwpsn	file "../source/InitGpio.c",line 1361,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1361 | GpioSetupHV[0].GpioFlags = GPIO_PUSHPULL;                              
;----------------------------------------------------------------------
        MOV       *-SP[14],#0           ; [CPU_] |1361| 
	.dwpsn	file "../source/InitGpio.c",line 1362,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1362 | GpioSetupHV[0].GpioPeripheral = 0;                                     
; 1364 | //!Set the following parameters of the ZAP voltage GPIO pin:           
; 1365 | //!GPIO pin no. to ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT                      
; 1366 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1367 | //!GPIO Pin as output pin                                              
; 1368 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1369 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[11],#0           ; [CPU_] |1362| 
	.dwpsn	file "../source/InitGpio.c",line 1370,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1370 | GpioSetupHV[1].GpioPin = ZAP_VOLTAGE_SWITCH_GPIO_OUTPUT;               
;----------------------------------------------------------------------
        MOVB      *-SP[10],#162,UNC     ; [CPU_] |1370| 
	.dwpsn	file "../source/InitGpio.c",line 1371,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1371 | GpioSetupHV[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                          
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |1371| 
	.dwpsn	file "../source/InitGpio.c",line 1372,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1372 | GpioSetupHV[1].GpioInputOutput = GPIO_OUTPUT;                          
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |1372| 
	.dwpsn	file "../source/InitGpio.c",line 1373,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1373 | GpioSetupHV[1].GpioFlags = GPIO_PUSHPULL;                              
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |1373| 
	.dwpsn	file "../source/InitGpio.c",line 1374,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1374 | GpioSetupHV[1].GpioPeripheral = 0;                                     
; 1377 | //!Set the following parameters of the High voltage RBC GPIO pin:      
; 1378 | //!GPIO pin no. to HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT                 
; 1379 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1380 | //!GPIO Pin as output pin                                              
; 1381 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1382 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1374| 
	.dwpsn	file "../source/InitGpio.c",line 1383,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1383 | GpioSetupHV[2].GpioPin = HIGH_VOLTAGE_SELECT_RBC_GPIO_OUTPUT;          
;----------------------------------------------------------------------
        MOVB      *-SP[5],#163,UNC      ; [CPU_] |1383| 
	.dwpsn	file "../source/InitGpio.c",line 1384,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1384 | GpioSetupHV[2].GpioCpuMuxSel = GPIO_MUX_CPU1;                          
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1384| 
	.dwpsn	file "../source/InitGpio.c",line 1385,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1385 | GpioSetupHV[2].GpioInputOutput = GPIO_OUTPUT;                          
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |1385| 
	.dwpsn	file "../source/InitGpio.c",line 1386,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1386 | GpioSetupHV[2].GpioFlags = GPIO_PUSHPULL;                              
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |1386| 
	.dwpsn	file "../source/InitGpio.c",line 1387,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1387 | GpioSetupHV[2].GpioPeripheral = 0;                                     
; 1389 | //!After setting all the configurations of the pin,                    
; 1390 | //!Update the configurations of the High voltage and ZAP voltage contro
;     | l GPIOs                                                                
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |1387| 
	.dwpsn	file "../source/InitGpio.c",line 1391,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1391 | IGConfigGpioPin(GpioSetupHV[0]);                                       
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1391| 
        SUBB      XAR4,#15              ; [CPU_U] |1391| 
        MOVZ      AR4,AR4               ; [CPU_] |1391| 
$C$DW$160	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$160, DW_AT_low_pc(0x00)
	.dwattr $C$DW$160, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$160, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1391| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1391| 
	.dwpsn	file "../source/InitGpio.c",line 1392,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1392 | IGConfigGpioPin(GpioSetupHV[1]);                                       
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1392| 
        SUBB      XAR4,#10              ; [CPU_U] |1392| 
        MOVZ      AR4,AR4               ; [CPU_] |1392| 
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$161, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1392| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1392| 
	.dwpsn	file "../source/InitGpio.c",line 1393,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1393 | IGConfigGpioPin(GpioSetupHV[2]);                                       
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1393| 
        SUBB      XAR4,#5               ; [CPU_U] |1393| 
        MOVZ      AR4,AR4               ; [CPU_] |1393| 
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$162, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1393| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1393| 
	.dwpsn	file "../source/InitGpio.c",line 1395,column 1,is_stmt,isa 0
        SUBB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$158, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$158, DW_AT_TI_end_line(0x573)
	.dwattr $C$DW$158, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$158

	.sect	".text:_IGGpioConfig_CycleCounterPin"
	.clink
	.global	_IGGpioConfig_CycleCounterPin

$C$DW$164	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$164, DW_AT_name("IGGpioConfig_CycleCounterPin")
	.dwattr $C$DW$164, DW_AT_low_pc(_IGGpioConfig_CycleCounterPin)
	.dwattr $C$DW$164, DW_AT_high_pc(0x00)
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_IGGpioConfig_CycleCounterPin")
	.dwattr $C$DW$164, DW_AT_external
	.dwattr $C$DW$164, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$164, DW_AT_TI_begin_line(0x581)
	.dwattr $C$DW$164, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$164, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../source/InitGpio.c",line 1410,column 1,is_stmt,address _IGGpioConfig_CycleCounterPin,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_CycleCounterPin
;----------------------------------------------------------------------
; 1409 | void IGGpioConfig_CycleCounterPin(void)                                
; 1411 | GpioPinConfig GpioSetupCycleCounter[2];                                
; 1413 | //!Set the following parameters of the Cycle counter GPIO pin 1:       
; 1414 | //!GPIO pin no. to CYCLE_COUNTER_GPIO_OUTPUT1                          
; 1415 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1416 | //!GPIO Pin as output pin                                              
; 1417 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1418 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_CycleCounterPin FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_CycleCounterPin:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$165	.dwtag  DW_TAG_variable
	.dwattr $C$DW$165, DW_AT_name("GpioSetupCycleCounter")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_GpioSetupCycleCounter")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_breg20 -10]

	.dwpsn	file "../source/InitGpio.c",line 1419,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1419 | GpioSetupCycleCounter[0].GpioPin = CYCLE_COUNTER_GPIO_OUTPUT1;         
;----------------------------------------------------------------------
        MOVB      *-SP[10],#53,UNC      ; [CPU_] |1419| 
	.dwpsn	file "../source/InitGpio.c",line 1420,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1420 | GpioSetupCycleCounter[0].GpioCpuMuxSel = GPIO_MUX_CPU1;                
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |1420| 
	.dwpsn	file "../source/InitGpio.c",line 1421,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1421 | GpioSetupCycleCounter[0].GpioInputOutput = GPIO_OUTPUT;                
;----------------------------------------------------------------------
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |1421| 
	.dwpsn	file "../source/InitGpio.c",line 1422,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1422 | GpioSetupCycleCounter[0].GpioFlags = GPIO_PUSHPULL;                    
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |1422| 
	.dwpsn	file "../source/InitGpio.c",line 1423,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1423 | GpioSetupCycleCounter[0].GpioPeripheral = 0;                           
; 1425 | //!Set the following parameters of the Cycle counter GPIO pin 2:       
; 1426 | //!GPIO pin no. to CYCLE_COUNTER_GPIO_OUTPUT2                          
; 1427 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1428 | //!GPIO Pin as output pin                                              
; 1429 | //!GPIO pin as GPIO_PUSHPULL                                           
; 1430 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1423| 
	.dwpsn	file "../source/InitGpio.c",line 1431,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1431 | GpioSetupCycleCounter[1].GpioPin = CYCLE_COUNTER_GPIO_OUTPUT2;         
;----------------------------------------------------------------------
        MOVB      *-SP[5],#54,UNC       ; [CPU_] |1431| 
	.dwpsn	file "../source/InitGpio.c",line 1432,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1432 | GpioSetupCycleCounter[1].GpioCpuMuxSel = GPIO_MUX_CPU1;                
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1432| 
	.dwpsn	file "../source/InitGpio.c",line 1433,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1433 | GpioSetupCycleCounter[1].GpioInputOutput = GPIO_OUTPUT;                
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |1433| 
	.dwpsn	file "../source/InitGpio.c",line 1434,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1434 | GpioSetupCycleCounter[1].GpioFlags = GPIO_PUSHPULL;                    
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |1434| 
	.dwpsn	file "../source/InitGpio.c",line 1435,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1435 | GpioSetupCycleCounter[1].GpioPeripheral = 0;                           
; 1437 | //!After setting all the configurations of the pin,                    
; 1438 | //!Update the configurations of thecycle counter control GPIOs         
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |1435| 
	.dwpsn	file "../source/InitGpio.c",line 1439,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1439 | IGConfigGpioPin(GpioSetupCycleCounter[0]);                             
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1439| 
        SUBB      XAR4,#10              ; [CPU_U] |1439| 
        MOVZ      AR4,AR4               ; [CPU_] |1439| 
$C$DW$166	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$166, DW_AT_low_pc(0x00)
	.dwattr $C$DW$166, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$166, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1439| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1439| 
	.dwpsn	file "../source/InitGpio.c",line 1440,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1440 | IGConfigGpioPin(GpioSetupCycleCounter[1]);                             
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1440| 
        SUBB      XAR4,#5               ; [CPU_U] |1440| 
        MOVZ      AR4,AR4               ; [CPU_] |1440| 
$C$DW$167	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$167, DW_AT_low_pc(0x00)
	.dwattr $C$DW$167, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$167, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1440| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1440| 
	.dwpsn	file "../source/InitGpio.c",line 1442,column 1,is_stmt,isa 0
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$168	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$168, DW_AT_low_pc(0x00)
	.dwattr $C$DW$168, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$164, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$164, DW_AT_TI_end_line(0x5a2)
	.dwattr $C$DW$164, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$164

	.sect	".text:_IGConfigGpioPin"
	.clink
	.global	_IGConfigGpioPin

$C$DW$169	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$169, DW_AT_name("IGConfigGpioPin")
	.dwattr $C$DW$169, DW_AT_low_pc(_IGConfigGpioPin)
	.dwattr $C$DW$169, DW_AT_high_pc(0x00)
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_IGConfigGpioPin")
	.dwattr $C$DW$169, DW_AT_external
	.dwattr $C$DW$169, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$169, DW_AT_TI_begin_line(0x5af)
	.dwattr $C$DW$169, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$169, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/InitGpio.c",line 1456,column 1,is_stmt,address _IGConfigGpioPin,isa 0

	.dwfde $C$DW$CIE, _IGConfigGpioPin
$C$DW$170	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$170, DW_AT_name("SetGpioPin")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_SetGpioPin")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1455 | void IGConfigGpioPin(GpioPinConfig SetGpioPin)                         
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGConfigGpioPin              FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_IGConfigGpioPin:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$171	.dwtag  DW_TAG_variable
	.dwattr $C$DW$171, DW_AT_name("SetGpioPin")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_SetGpioPin")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_breg20 -2]

$C$DW$172	.dwtag  DW_TAG_variable
	.dwattr $C$DW$172, DW_AT_name("SetGpioPin")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_SetGpioPin")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_breg20 -7]

        MOVZ      AR5,SP                ; [CPU_] |1456| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1456| 
        MOVL      XAR7,*-SP[2]          ; [CPU_] |1456| 
        SUBB      XAR5,#7               ; [CPU_U] |1456| 
        MOVU      ACC,AR5               ; [CPU_] |1456| 
        MOVL      XAR4,ACC              ; [CPU_] |1456| 
        RPT       #4
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |1456| 
	.dwpsn	file "../source/InitGpio.c",line 1457,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1457 | EALLOW;                                                                
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../source/InitGpio.c",line 1458,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1458 | GPIO_SetupPinMux(SetGpioPin.GpioPin, SetGpioPin.GpioCpuMuxSel,\        
; 1459 |         SetGpioPin.GpioPeripheral);                                    
;----------------------------------------------------------------------
        SPM       #0                    ; [CPU_] 
        MOVZ      AR4,*-SP[3]           ; [CPU_] |1458| 
        MOV       AL,*-SP[7]            ; [CPU_] |1458| 
        MOV       AH,*-SP[5]            ; [CPU_] |1458| 
$C$DW$173	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$173, DW_AT_low_pc(0x00)
	.dwattr $C$DW$173, DW_AT_name("_GPIO_SetupPinMux")
	.dwattr $C$DW$173, DW_AT_TI_call

        LCR       #_GPIO_SetupPinMux    ; [CPU_] |1458| 
        ; call occurs [#_GPIO_SetupPinMux] ; [] |1458| 
	.dwpsn	file "../source/InitGpio.c",line 1460,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1460 | GPIO_SetupPinOptions(SetGpioPin.GpioPin, SetGpioPin.GpioInputOutput,\  
; 1461 |         SetGpioPin.GpioFlags);                                         
;----------------------------------------------------------------------
        MOV       AL,*-SP[7]            ; [CPU_] |1460| 
        MOV       AH,*-SP[4]            ; [CPU_] |1460| 
        MOVZ      AR4,*-SP[6]           ; [CPU_] |1460| 
$C$DW$174	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$174, DW_AT_low_pc(0x00)
	.dwattr $C$DW$174, DW_AT_name("_GPIO_SetupPinOptions")
	.dwattr $C$DW$174, DW_AT_TI_call

        LCR       #_GPIO_SetupPinOptions ; [CPU_] |1460| 
        ; call occurs [#_GPIO_SetupPinOptions] ; [] |1460| 
	.dwpsn	file "../source/InitGpio.c",line 1462,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1462 | EDIS;                                                                  
;----------------------------------------------------------------------
 EDIS
	.dwpsn	file "../source/InitGpio.c",line 1463,column 1,is_stmt,isa 0
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$175	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$175, DW_AT_low_pc(0x00)
	.dwattr $C$DW$175, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$169, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$169, DW_AT_TI_end_line(0x5b7)
	.dwattr $C$DW$169, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$169

	.sect	".text:_IGBlinkLED"
	.clink
	.global	_IGBlinkLED

$C$DW$176	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$176, DW_AT_name("IGBlinkLED")
	.dwattr $C$DW$176, DW_AT_low_pc(_IGBlinkLED)
	.dwattr $C$DW$176, DW_AT_high_pc(0x00)
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_IGBlinkLED")
	.dwattr $C$DW$176, DW_AT_external
	.dwattr $C$DW$176, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$176, DW_AT_TI_begin_line(0x5c3)
	.dwattr $C$DW$176, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$176, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/InitGpio.c",line 1476,column 1,is_stmt,address _IGBlinkLED,isa 0

	.dwfde $C$DW$CIE, _IGBlinkLED
$C$DW$177	.dwtag  DW_TAG_variable
	.dwattr $C$DW$177, DW_AT_name("stLEDToggleCounter")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_stLEDToggleCounter$2")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$177, DW_AT_location[DW_OP_addr _stLEDToggleCounter$2]

$C$DW$178	.dwtag  DW_TAG_variable
	.dwattr $C$DW$178, DW_AT_name("stLEDToggle")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_stLEDToggle$1")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$178, DW_AT_location[DW_OP_addr _stLEDToggle$1]

$C$DW$179	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$179, DW_AT_name("mSecLEDBlink")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_mSecLEDBlink")
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$179, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1475 | void IGBlinkLED( unsigned int  mSecLEDBlink)                           
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGBlinkLED                   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_IGBlinkLED:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$180	.dwtag  DW_TAG_variable
	.dwattr $C$DW$180, DW_AT_name("mSecLEDBlink")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_mSecLEDBlink")
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$180, DW_AT_location[DW_OP_breg20 -1]

$C$DW$181	.dwtag  DW_TAG_variable
	.dwattr $C$DW$181, DW_AT_name("LoopExecutionTime")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_LoopExecutionTime")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$181, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 1477 | static unsigned int  stLEDToggle=1, stLEDToggleCounter=0;              
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |1476| 
	.dwpsn	file "../source/InitGpio.c",line 1478,column 34,is_stmt,isa 0
;----------------------------------------------------------------------
; 1478 | unsigned int  LoopExecutionTime = 20;//in mSec                         
;----------------------------------------------------------------------
        MOVB      *-SP[2],#20,UNC       ; [CPU_] |1478| 
	.dwpsn	file "../source/InitGpio.c",line 1481,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1481 | if(stLEDToggleCounter >= mSecLEDBlink)                                 
; 1483 |     //!Reset the toggle counter to zero, if the toggle counter reaches
;     | the                                                                    
; 1484 |     //!the blink count value                                           
;----------------------------------------------------------------------
        MOVW      DP,#_stLEDToggleCounter$2 ; [CPU_U] 
        CMP       AL,@_stLEDToggleCounter$2 ; [CPU_] |1481| 
        B         $C$L3,HI              ; [CPU_] |1481| 
        ; branchcc occurs ; [] |1481| 
	.dwpsn	file "../source/InitGpio.c",line 1485,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1485 | stLEDToggleCounter = 0;                                                
; 1486 | //!If the toggle value is set to one, then set the LED 1 GPIO status to
;     |  high                                                                  
;----------------------------------------------------------------------
        MOV       @_stLEDToggleCounter$2,#0 ; [CPU_] |1485| 
	.dwpsn	file "../source/InitGpio.c",line 1487,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1487 | if(stLEDToggle == 1 )                                                  
;----------------------------------------------------------------------
        MOV       AL,@_stLEDToggle$1    ; [CPU_] |1487| 
        CMPB      AL,#1                 ; [CPU_] |1487| 
        B         $C$L2,NEQ             ; [CPU_] |1487| 
        ; branchcc occurs ; [] |1487| 
	.dwpsn	file "../source/InitGpio.c",line 1489,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 1489 | stLEDToggle = 0;                                                       
; 1490 | //GPIO_WritePin(LED1_GPIO, 1);                                         
; 1491 | //                        GPIO_WritePin(LED2_GPIO, 0);                 
;----------------------------------------------------------------------
        MOV       @_stLEDToggle$1,#0    ; [CPU_] |1489| 
	.dwpsn	file "../source/InitGpio.c",line 1493,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1494 | //!If the toggle value is set to one, then set the LED 1 GPIO status to
;     |  low                                                                   
;----------------------------------------------------------------------
        B         $C$L4,UNC             ; [CPU_] |1493| 
        ; branch occurs ; [] |1493| 
$C$L2:    
	.dwpsn	file "../source/InitGpio.c",line 1495,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1495 | else if(stLEDToggle == 0 )                                             
;----------------------------------------------------------------------
        CMPB      AL,#0                 ; [CPU_] |1495| 
        B         $C$L4,NEQ             ; [CPU_] |1495| 
        ; branchcc occurs ; [] |1495| 
	.dwpsn	file "../source/InitGpio.c",line 1497,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 1497 | stLEDToggle = 1;                                                       
; 1498 | //GPIO_WritePin(LED1_GPIO, 0);                                         
; 1499 | //                        GPIO_WritePin(LED2_GPIO, 1);                 
;----------------------------------------------------------------------
        MOVB      @_stLEDToggle$1,#1,UNC ; [CPU_] |1497| 
	.dwpsn	file "../source/InitGpio.c",line 1501,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1502 | else                                                                   
; 1504 |     //!Increment the toggle counter                                    
;----------------------------------------------------------------------
        B         $C$L4,UNC             ; [CPU_] |1501| 
        ; branch occurs ; [] |1501| 
$C$L3:    
	.dwpsn	file "../source/InitGpio.c",line 1505,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1505 | stLEDToggleCounter = stLEDToggleCounter + LoopExecutionTime;           
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |1505| 
        ADD       @_stLEDToggleCounter$2,AL ; [CPU_] |1505| 
	.dwpsn	file "../source/InitGpio.c",line 1508,column 1,is_stmt,isa 0
$C$L4:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$176, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$176, DW_AT_TI_end_line(0x5e4)
	.dwattr $C$DW$176, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$176

	.sect	".text:_IGConfigLED"
	.clink
	.global	_IGConfigLED

$C$DW$183	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$183, DW_AT_name("IGConfigLED")
	.dwattr $C$DW$183, DW_AT_low_pc(_IGConfigLED)
	.dwattr $C$DW$183, DW_AT_high_pc(0x00)
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_IGConfigLED")
	.dwattr $C$DW$183, DW_AT_external
	.dwattr $C$DW$183, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$183, DW_AT_TI_begin_line(0x5f1)
	.dwattr $C$DW$183, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$183, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/InitGpio.c",line 1522,column 1,is_stmt,address _IGConfigLED,isa 0

	.dwfde $C$DW$CIE, _IGConfigLED
$C$DW$184	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$184, DW_AT_name("Pin")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$184, DW_AT_location[DW_OP_reg0]

$C$DW$185	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$185, DW_AT_name("State")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$185, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 1521 | void IGConfigLED( unsigned int  Pin,  unsigned int  State)             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGConfigLED                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_IGConfigLED:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$186	.dwtag  DW_TAG_variable
	.dwattr $C$DW$186, DW_AT_name("Pin")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$186, DW_AT_location[DW_OP_breg20 -1]

$C$DW$187	.dwtag  DW_TAG_variable
	.dwattr $C$DW$187, DW_AT_name("State")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$187, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 1523 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |1522| 
        MOV       *-SP[1],AL            ; [CPU_] |1522| 
	.dwpsn	file "../source/InitGpio.c",line 1524,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1524 | GPIO_WritePin(Pin, State);                                             
;----------------------------------------------------------------------
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$188, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1524| 
        ; call occurs [#_GPIO_WritePin] ; [] |1524| 
	.dwpsn	file "../source/InitGpio.c",line 1525,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$189	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$189, DW_AT_low_pc(0x00)
	.dwattr $C$DW$189, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$183, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$183, DW_AT_TI_end_line(0x5f5)
	.dwattr $C$DW$183, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$183

	.sect	".text:_IGWriteMotorGpio"
	.clink
	.global	_IGWriteMotorGpio

$C$DW$190	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$190, DW_AT_name("IGWriteMotorGpio")
	.dwattr $C$DW$190, DW_AT_low_pc(_IGWriteMotorGpio)
	.dwattr $C$DW$190, DW_AT_high_pc(0x00)
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_IGWriteMotorGpio")
	.dwattr $C$DW$190, DW_AT_external
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$190, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$190, DW_AT_TI_begin_line(0x603)
	.dwattr $C$DW$190, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$190, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/InitGpio.c",line 1540,column 1,is_stmt,address _IGWriteMotorGpio,isa 0

	.dwfde $C$DW$CIE, _IGWriteMotorGpio
$C$DW$191	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$191, DW_AT_name("Pin")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$191, DW_AT_location[DW_OP_reg0]

$C$DW$192	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$192, DW_AT_name("State")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$192, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 1539 | int IGWriteMotorGpio( unsigned int  Pin,  unsigned int  State)         
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGWriteMotorGpio             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_IGWriteMotorGpio:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$193	.dwtag  DW_TAG_variable
	.dwattr $C$DW$193, DW_AT_name("Pin")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$193, DW_AT_location[DW_OP_breg20 -1]

$C$DW$194	.dwtag  DW_TAG_variable
	.dwattr $C$DW$194, DW_AT_name("State")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_breg20 -2]

$C$DW$195	.dwtag  DW_TAG_variable
	.dwattr $C$DW$195, DW_AT_name("Err")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_Err")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_breg20 -3]

        MOV       *-SP[2],AH            ; [CPU_] |1540| 
        MOV       *-SP[1],AL            ; [CPU_] |1540| 
	.dwpsn	file "../source/InitGpio.c",line 1541,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1541 | int Err = -1;                                                          
;----------------------------------------------------------------------
        MOV       *-SP[3],#-1           ; [CPU_] |1541| 
	.dwpsn	file "../source/InitGpio.c",line 1542,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1542 | switch(Pin)                                                            
; 1545 | case M1_MODE0:                                                         
; 1546 | case M1_MODE1:                                                         
; 1547 | case M1_MODE2:                                                         
; 1548 | case M1_ENBL:                                                          
; 1549 | case M1_DIR:                                                           
; 1550 | case M1_DECAY:                                                         
; 1551 |     //!If the input is M1_MODE0, M1_MODE1, M1_MODE2, M1_ENBL, M1_DIR, M
;     | 1_DECAY                                                                
; 1552 |     //!Control the GPIO to high or low with respect to the pin indicate
;     | d                                                                      
;----------------------------------------------------------------------
        B         $C$L11,UNC            ; [CPU_] |1542| 
        ; branch occurs ; [] |1542| 
$C$L5:    
	.dwpsn	file "../source/InitGpio.c",line 1553,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1553 | GPIO_WritePin(Pin, State);                                             
; 1554 | //!Reset the error state                                               
;----------------------------------------------------------------------
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$196, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1553| 
        ; call occurs [#_GPIO_WritePin] ; [] |1553| 
	.dwpsn	file "../source/InitGpio.c",line 1555,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1555 | Err = 0;                                                               
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1555| 
	.dwpsn	file "../source/InitGpio.c",line 1556,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1556 | break;                                                                 
; 1558 | case M2_MODE0:                                                         
; 1559 | case M2_MODE1:                                                         
; 1560 | case M2_MODE2:                                                         
; 1561 | case M2_ENBL:                                                          
; 1562 | case M2_DIR:                                                           
; 1563 | case M2_DECAY:                                                         
; 1564 | //!If the input is M2_MODE0, M2_MODE1, M2_MODE2, M2_ENBL, M2_DIR, M2_DE
;     | CAY                                                                    
; 1565 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |1556| 
        ; branch occurs ; [] |1556| 
$C$L6:    
	.dwpsn	file "../source/InitGpio.c",line 1566,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1566 | GPIO_WritePin(Pin, State);                                             
; 1567 | //!Reset the error state                                               
;----------------------------------------------------------------------
$C$DW$197	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$197, DW_AT_low_pc(0x00)
	.dwattr $C$DW$197, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$197, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1566| 
        ; call occurs [#_GPIO_WritePin] ; [] |1566| 
	.dwpsn	file "../source/InitGpio.c",line 1568,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1568 | Err = 0;                                                               
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1568| 
	.dwpsn	file "../source/InitGpio.c",line 1569,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1569 | break;                                                                 
; 1571 | case M3_MODE0:                                                         
; 1572 | case M3_MODE1:                                                         
; 1573 | case M3_MODE2:                                                         
; 1574 | case M3_ENBL:                                                          
; 1575 | case M3_DIR:                                                           
; 1576 | case M3_DECAY:                                                         
; 1577 | //!If the input is M3_MODE0, M3_MODE1, M3_MODE2, M3_ENBL, M3_DIR, M3_DE
;     | CAY                                                                    
; 1578 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |1569| 
        ; branch occurs ; [] |1569| 
$C$L7:    
	.dwpsn	file "../source/InitGpio.c",line 1579,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1579 | GPIO_WritePin(Pin, State);                                             
; 1580 | //!Reset the error state                                               
;----------------------------------------------------------------------
$C$DW$198	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$198, DW_AT_low_pc(0x00)
	.dwattr $C$DW$198, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$198, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1579| 
        ; call occurs [#_GPIO_WritePin] ; [] |1579| 
	.dwpsn	file "../source/InitGpio.c",line 1581,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1581 | Err = 0;                                                               
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1581| 
	.dwpsn	file "../source/InitGpio.c",line 1582,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1582 | break;                                                                 
; 1584 | case M4_MODE0:                                                         
; 1585 | case M4_MODE1:                                                         
; 1586 | case M4_MODE2:                                                         
; 1587 | case M4_ENBL:                                                          
; 1588 | case M4_DIR:                                                           
; 1589 | case M4_DECAY:                                                         
; 1590 | //!If the input is M4_MODE0, M4_MODE1, M4_MODE2, M4_ENBL, M4_DIR, M4_DE
;     | CAY                                                                    
; 1591 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |1582| 
        ; branch occurs ; [] |1582| 
$C$L8:    
	.dwpsn	file "../source/InitGpio.c",line 1592,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1592 | GPIO_WritePin(Pin, State);                                             
; 1593 | //!Reset the error state                                               
;----------------------------------------------------------------------
$C$DW$199	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$199, DW_AT_low_pc(0x00)
	.dwattr $C$DW$199, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$199, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1592| 
        ; call occurs [#_GPIO_WritePin] ; [] |1592| 
	.dwpsn	file "../source/InitGpio.c",line 1594,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1594 | Err = 0;                                                               
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1594| 
	.dwpsn	file "../source/InitGpio.c",line 1595,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1595 | break;                                                                 
; 1597 | case M5_MODE0:                                                         
; 1598 | case M5_MODE1:                                                         
; 1599 | case M5_MODE2:                                                         
; 1600 | case M5_ENBL:                                                          
; 1601 | case M5_DIR:                                                           
; 1602 | case M5_DECAY:                                                         
; 1603 | //!If the input is M5_MODE0, M5_MODE1, M5_MODE2, M5_ENBL, M5_DIR, M5_DE
;     | CAY                                                                    
; 1604 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |1595| 
        ; branch occurs ; [] |1595| 
$C$L9:    
	.dwpsn	file "../source/InitGpio.c",line 1605,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1605 | GPIO_WritePin(Pin, State);                                             
; 1606 | //!Reset the error state                                               
;----------------------------------------------------------------------
$C$DW$200	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$200, DW_AT_low_pc(0x00)
	.dwattr $C$DW$200, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$200, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1605| 
        ; call occurs [#_GPIO_WritePin] ; [] |1605| 
	.dwpsn	file "../source/InitGpio.c",line 1607,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1607 | Err = 0;                                                               
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1607| 
	.dwpsn	file "../source/InitGpio.c",line 1608,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1608 | break;                                                                 
; 1610 | case M_RESET:                                                          
; 1611 | //!If the input is M_RESET                                             
; 1612 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |1608| 
        ; branch occurs ; [] |1608| 
$C$L10:    
	.dwpsn	file "../source/InitGpio.c",line 1613,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1613 | GPIO_WritePin(Pin, State);                                             
; 1614 | //!Reset the error state                                               
;----------------------------------------------------------------------
$C$DW$201	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$201, DW_AT_low_pc(0x00)
	.dwattr $C$DW$201, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$201, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1613| 
        ; call occurs [#_GPIO_WritePin] ; [] |1613| 
	.dwpsn	file "../source/InitGpio.c",line 1615,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1615 | Err = 0;                                                               
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1615| 
	.dwpsn	file "../source/InitGpio.c",line 1616,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1616 | break;                                                                 
; 1618 | default:                                                               
; 1619 | break;                                                                 
; 1623 | //!Return the error state                                              
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |1616| 
        ; branch occurs ; [] |1616| 
$C$L11:    
	.dwpsn	file "../source/InitGpio.c",line 1542,column 2,is_stmt,isa 0
        CMPB      AL,#25                ; [CPU_] |1542| 
        B         $C$L15,GT             ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#25                ; [CPU_] |1542| 
        B         $C$L7,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#12                ; [CPU_] |1542| 
        B         $C$L13,GT             ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#12                ; [CPU_] |1542| 
        B         $C$L6,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#6                 ; [CPU_] |1542| 
        B         $C$L12,GT             ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#6                 ; [CPU_] |1542| 
        B         $C$L5,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#2                 ; [CPU_] |1542| 
        B         $C$L5,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#3                 ; [CPU_] |1542| 
        B         $C$L5,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#4                 ; [CPU_] |1542| 
        B         $C$L5,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        B         $C$L19,UNC            ; [CPU_] |1542| 
        ; branch occurs ; [] |1542| 
$C$L12:    
        CMPB      AL,#7                 ; [CPU_] |1542| 
        B         $C$L5,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#10                ; [CPU_] |1542| 
        B         $C$L10,EQ             ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#11                ; [CPU_] |1542| 
        B         $C$L6,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        B         $C$L19,UNC            ; [CPU_] |1542| 
        ; branch occurs ; [] |1542| 
$C$L13:    
        CMPB      AL,#20                ; [CPU_] |1542| 
        B         $C$L14,GT             ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#20                ; [CPU_] |1542| 
        B         $C$L6,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#13                ; [CPU_] |1542| 
        B         $C$L6,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#15                ; [CPU_] |1542| 
        B         $C$L6,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#16                ; [CPU_] |1542| 
        B         $C$L5,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        B         $C$L19,UNC            ; [CPU_] |1542| 
        ; branch occurs ; [] |1542| 
$C$L14:    
        CMPB      AL,#21                ; [CPU_] |1542| 
        B         $C$L6,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#23                ; [CPU_] |1542| 
        B         $C$L7,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#24                ; [CPU_] |1542| 
        B         $C$L7,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        B         $C$L19,UNC            ; [CPU_] |1542| 
        ; branch occurs ; [] |1542| 
$C$L15:    
        CMPB      AL,#95                ; [CPU_] |1542| 
        B         $C$L17,GT             ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#95                ; [CPU_] |1542| 
        B         $C$L8,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#34                ; [CPU_] |1542| 
        B         $C$L16,GT             ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#34                ; [CPU_] |1542| 
        B         $C$L8,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#27                ; [CPU_] |1542| 
        B         $C$L7,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#28                ; [CPU_] |1542| 
        B         $C$L7,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#33                ; [CPU_] |1542| 
        B         $C$L7,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        B         $C$L19,UNC            ; [CPU_] |1542| 
        ; branch occurs ; [] |1542| 
$C$L16:    
        CMPB      AL,#35                ; [CPU_] |1542| 
        B         $C$L8,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#37                ; [CPU_] |1542| 
        B         $C$L8,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#91                ; [CPU_] |1542| 
        B         $C$L8,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        B         $C$L19,UNC            ; [CPU_] |1542| 
        ; branch occurs ; [] |1542| 
$C$L17:    
        CMPB      AL,#101               ; [CPU_] |1542| 
        B         $C$L18,GT             ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#101               ; [CPU_] |1542| 
        B         $C$L9,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#97                ; [CPU_] |1542| 
        B         $C$L9,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#98                ; [CPU_] |1542| 
        B         $C$L9,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#99                ; [CPU_] |1542| 
        B         $C$L9,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        B         $C$L19,UNC            ; [CPU_] |1542| 
        ; branch occurs ; [] |1542| 
$C$L18:    
        CMPB      AL,#102               ; [CPU_] |1542| 
        B         $C$L9,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#103               ; [CPU_] |1542| 
        B         $C$L9,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
        CMPB      AL,#150               ; [CPU_] |1542| 
        B         $C$L8,EQ              ; [CPU_] |1542| 
        ; branchcc occurs ; [] |1542| 
$C$L19:    
	.dwpsn	file "../source/InitGpio.c",line 1624,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1624 | return Err;                                                            
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |1624| 
	.dwpsn	file "../source/InitGpio.c",line 1625,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$190, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$190, DW_AT_TI_end_line(0x659)
	.dwattr $C$DW$190, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$190

	.sect	".text:_IGReadMotorGpio"
	.clink
	.global	_IGReadMotorGpio

$C$DW$203	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$203, DW_AT_name("IGReadMotorGpio")
	.dwattr $C$DW$203, DW_AT_low_pc(_IGReadMotorGpio)
	.dwattr $C$DW$203, DW_AT_high_pc(0x00)
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_IGReadMotorGpio")
	.dwattr $C$DW$203, DW_AT_external
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$203, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$203, DW_AT_TI_begin_line(0x667)
	.dwattr $C$DW$203, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$203, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/InitGpio.c",line 1640,column 1,is_stmt,address _IGReadMotorGpio,isa 0

	.dwfde $C$DW$CIE, _IGReadMotorGpio
$C$DW$204	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$204, DW_AT_name("Pin")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1639 | Uint16 IGReadMotorGpio( unsigned int  Pin)                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGReadMotorGpio              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_IGReadMotorGpio:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$205	.dwtag  DW_TAG_variable
	.dwattr $C$DW$205, DW_AT_name("Pin")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$205, DW_AT_location[DW_OP_breg20 -1]

$C$DW$206	.dwtag  DW_TAG_variable
	.dwattr $C$DW$206, DW_AT_name("Status")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_Status")
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$206, DW_AT_location[DW_OP_breg20 -2]

        MOV       *-SP[1],AL            ; [CPU_] |1640| 
	.dwpsn	file "../source/InitGpio.c",line 1641,column 16,is_stmt,isa 0
;----------------------------------------------------------------------
; 1641 | Uint16 Status = 0x01;                                                  
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |1641| 
	.dwpsn	file "../source/InitGpio.c",line 1642,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1642 | switch(Pin)                                                            
; 1645 | case M1_FAULT:                                                         
; 1646 | //!If the input is M1_FAULT                                            
; 1647 | //!Read the GPIO status of the pin indicated                           
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |1642| 
        ; branch occurs ; [] |1642| 
$C$L20:    
	.dwpsn	file "../source/InitGpio.c",line 1648,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1648 | Status = GPIO_ReadPin(Pin);                                            
;----------------------------------------------------------------------
$C$DW$207	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$207, DW_AT_low_pc(0x00)
	.dwattr $C$DW$207, DW_AT_name("_GPIO_ReadPin")
	.dwattr $C$DW$207, DW_AT_TI_call

        LCR       #_GPIO_ReadPin        ; [CPU_] |1648| 
        ; call occurs [#_GPIO_ReadPin] ; [] |1648| 
        MOV       *-SP[2],AL            ; [CPU_] |1648| 
	.dwpsn	file "../source/InitGpio.c",line 1650,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1650 | break;                                                                 
; 1652 | case M2_FAULT:                                                         
; 1653 | //!If the input is M2_FAULT                                            
; 1654 | //!Read the GPIO status of the pin indicated                           
;----------------------------------------------------------------------
        B         $C$L27,UNC            ; [CPU_] |1650| 
        ; branch occurs ; [] |1650| 
$C$L21:    
	.dwpsn	file "../source/InitGpio.c",line 1655,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1655 | Status = GPIO_ReadPin(Pin);                                            
;----------------------------------------------------------------------
$C$DW$208	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$208, DW_AT_low_pc(0x00)
	.dwattr $C$DW$208, DW_AT_name("_GPIO_ReadPin")
	.dwattr $C$DW$208, DW_AT_TI_call

        LCR       #_GPIO_ReadPin        ; [CPU_] |1655| 
        ; call occurs [#_GPIO_ReadPin] ; [] |1655| 
        MOV       *-SP[2],AL            ; [CPU_] |1655| 
	.dwpsn	file "../source/InitGpio.c",line 1657,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1657 | break;                                                                 
; 1659 | case M3_FAULT:                                                         
; 1660 | //!If the input is M3_FAULT                                            
; 1661 | //!Read the GPIO status of the pin indicated                           
;----------------------------------------------------------------------
        B         $C$L27,UNC            ; [CPU_] |1657| 
        ; branch occurs ; [] |1657| 
$C$L22:    
	.dwpsn	file "../source/InitGpio.c",line 1662,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1662 | Status = GPIO_ReadPin(Pin);                                            
;----------------------------------------------------------------------
$C$DW$209	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$209, DW_AT_low_pc(0x00)
	.dwattr $C$DW$209, DW_AT_name("_GPIO_ReadPin")
	.dwattr $C$DW$209, DW_AT_TI_call

        LCR       #_GPIO_ReadPin        ; [CPU_] |1662| 
        ; call occurs [#_GPIO_ReadPin] ; [] |1662| 
        MOV       *-SP[2],AL            ; [CPU_] |1662| 
	.dwpsn	file "../source/InitGpio.c",line 1664,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1664 | break;                                                                 
; 1666 | case M4_FAULT:                                                         
; 1667 | //!If the input is M4_FAULT                                            
; 1668 | //!Read the GPIO status of the pin indicated                           
;----------------------------------------------------------------------
        B         $C$L27,UNC            ; [CPU_] |1664| 
        ; branch occurs ; [] |1664| 
$C$L23:    
	.dwpsn	file "../source/InitGpio.c",line 1669,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1669 | Status = GPIO_ReadPin(Pin);                                            
;----------------------------------------------------------------------
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_name("_GPIO_ReadPin")
	.dwattr $C$DW$210, DW_AT_TI_call

        LCR       #_GPIO_ReadPin        ; [CPU_] |1669| 
        ; call occurs [#_GPIO_ReadPin] ; [] |1669| 
        MOV       *-SP[2],AL            ; [CPU_] |1669| 
	.dwpsn	file "../source/InitGpio.c",line 1671,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1671 | break;                                                                 
; 1673 | case M5_FAULT:                                                         
; 1674 | //!If the input is M5_FAULT                                            
; 1675 | //!Read the GPIO status of the pin indicated                           
;----------------------------------------------------------------------
        B         $C$L27,UNC            ; [CPU_] |1671| 
        ; branch occurs ; [] |1671| 
$C$L24:    
	.dwpsn	file "../source/InitGpio.c",line 1676,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1676 | Status = GPIO_ReadPin(Pin);                                            
;----------------------------------------------------------------------
$C$DW$211	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$211, DW_AT_low_pc(0x00)
	.dwattr $C$DW$211, DW_AT_name("_GPIO_ReadPin")
	.dwattr $C$DW$211, DW_AT_TI_call

        LCR       #_GPIO_ReadPin        ; [CPU_] |1676| 
        ; call occurs [#_GPIO_ReadPin] ; [] |1676| 
        MOV       *-SP[2],AL            ; [CPU_] |1676| 
	.dwpsn	file "../source/InitGpio.c",line 1678,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1678 | break;                                                                 
; 1680 | default:                                                               
; 1681 | break;                                                                 
; 1684 | //!Return the status of the GPIO pin status                            
;----------------------------------------------------------------------
        B         $C$L27,UNC            ; [CPU_] |1678| 
        ; branch occurs ; [] |1678| 
$C$L25:    
	.dwpsn	file "../source/InitGpio.c",line 1642,column 2,is_stmt,isa 0
        CMPB      AL,#22                ; [CPU_] |1642| 
        B         $C$L26,GT             ; [CPU_] |1642| 
        ; branchcc occurs ; [] |1642| 
        CMPB      AL,#22                ; [CPU_] |1642| 
        B         $C$L21,EQ             ; [CPU_] |1642| 
        ; branchcc occurs ; [] |1642| 
        CMPB      AL,#17                ; [CPU_] |1642| 
        B         $C$L20,EQ             ; [CPU_] |1642| 
        ; branchcc occurs ; [] |1642| 
        CMPB      AL,#19                ; [CPU_] |1642| 
        B         $C$L22,EQ             ; [CPU_] |1642| 
        ; branchcc occurs ; [] |1642| 
        B         $C$L27,UNC            ; [CPU_] |1642| 
        ; branch occurs ; [] |1642| 
$C$L26:    
        CMPB      AL,#94                ; [CPU_] |1642| 
        B         $C$L24,EQ             ; [CPU_] |1642| 
        ; branchcc occurs ; [] |1642| 
        CMPB      AL,#96                ; [CPU_] |1642| 
        B         $C$L23,EQ             ; [CPU_] |1642| 
        ; branchcc occurs ; [] |1642| 
$C$L27:    
	.dwpsn	file "../source/InitGpio.c",line 1685,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1685 | return Status;                                                         
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |1685| 
	.dwpsn	file "../source/InitGpio.c",line 1686,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$212	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$212, DW_AT_low_pc(0x00)
	.dwattr $C$DW$212, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$203, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$203, DW_AT_TI_end_line(0x696)
	.dwattr $C$DW$203, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$203

	.sect	".text:_IGConfigHighVoltageModule"
	.clink
	.global	_IGConfigHighVoltageModule

$C$DW$213	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$213, DW_AT_name("IGConfigHighVoltageModule")
	.dwattr $C$DW$213, DW_AT_low_pc(_IGConfigHighVoltageModule)
	.dwattr $C$DW$213, DW_AT_high_pc(0x00)
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_IGConfigHighVoltageModule")
	.dwattr $C$DW$213, DW_AT_external
	.dwattr $C$DW$213, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$213, DW_AT_TI_begin_line(0x6a3)
	.dwattr $C$DW$213, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$213, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/InitGpio.c",line 1700,column 1,is_stmt,address _IGConfigHighVoltageModule,isa 0

	.dwfde $C$DW$CIE, _IGConfigHighVoltageModule
$C$DW$214	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$214, DW_AT_name("Pin")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_reg0]

$C$DW$215	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$215, DW_AT_name("State")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$215, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 1699 | void IGConfigHighVoltageModule( unsigned int  Pin,  unsigned int  State
;     | )                                                                      
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGConfigHighVoltageModule    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_IGConfigHighVoltageModule:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$216	.dwtag  DW_TAG_variable
	.dwattr $C$DW$216, DW_AT_name("Pin")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$216, DW_AT_location[DW_OP_breg20 -1]

$C$DW$217	.dwtag  DW_TAG_variable
	.dwattr $C$DW$217, DW_AT_name("State")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$217, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 1701 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |1700| 
        MOV       *-SP[1],AL            ; [CPU_] |1700| 
	.dwpsn	file "../source/InitGpio.c",line 1702,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1702 | GPIO_WritePin(Pin, State);                                             
;----------------------------------------------------------------------
$C$DW$218	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$218, DW_AT_low_pc(0x00)
	.dwattr $C$DW$218, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$218, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1702| 
        ; call occurs [#_GPIO_WritePin] ; [] |1702| 
	.dwpsn	file "../source/InitGpio.c",line 1703,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$219	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$219, DW_AT_low_pc(0x00)
	.dwattr $C$DW$219, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$213, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$213, DW_AT_TI_end_line(0x6a7)
	.dwattr $C$DW$213, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$213

	.sect	".text:_IGWriteCycleCounterGpio"
	.clink
	.global	_IGWriteCycleCounterGpio

$C$DW$220	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$220, DW_AT_name("IGWriteCycleCounterGpio")
	.dwattr $C$DW$220, DW_AT_low_pc(_IGWriteCycleCounterGpio)
	.dwattr $C$DW$220, DW_AT_high_pc(0x00)
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_IGWriteCycleCounterGpio")
	.dwattr $C$DW$220, DW_AT_external
	.dwattr $C$DW$220, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$220, DW_AT_TI_begin_line(0x6b4)
	.dwattr $C$DW$220, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$220, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/InitGpio.c",line 1717,column 1,is_stmt,address _IGWriteCycleCounterGpio,isa 0

	.dwfde $C$DW$CIE, _IGWriteCycleCounterGpio
$C$DW$221	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$221, DW_AT_name("Pin")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_reg0]

$C$DW$222	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$222, DW_AT_name("State")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 1716 | void IGWriteCycleCounterGpio( unsigned int  Pin,  unsigned int  State) 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGWriteCycleCounterGpio      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_IGWriteCycleCounterGpio:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$223	.dwtag  DW_TAG_variable
	.dwattr $C$DW$223, DW_AT_name("Pin")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$223, DW_AT_location[DW_OP_breg20 -1]

$C$DW$224	.dwtag  DW_TAG_variable
	.dwattr $C$DW$224, DW_AT_name("State")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 1718 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |1717| 
        MOV       *-SP[1],AL            ; [CPU_] |1717| 
	.dwpsn	file "../source/InitGpio.c",line 1719,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1719 | GPIO_WritePin(Pin, State);                                             
;----------------------------------------------------------------------
$C$DW$225	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$225, DW_AT_low_pc(0x00)
	.dwattr $C$DW$225, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$225, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1719| 
        ; call occurs [#_GPIO_WritePin] ; [] |1719| 
	.dwpsn	file "../source/InitGpio.c",line 1720,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$226	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$226, DW_AT_low_pc(0x00)
	.dwattr $C$DW$226, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$220, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$220, DW_AT_TI_end_line(0x6b8)
	.dwattr $C$DW$220, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$220

	.sect	".text:_IGWriteSpiCommControlPin"
	.clink
	.global	_IGWriteSpiCommControlPin

$C$DW$227	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$227, DW_AT_name("IGWriteSpiCommControlPin")
	.dwattr $C$DW$227, DW_AT_low_pc(_IGWriteSpiCommControlPin)
	.dwattr $C$DW$227, DW_AT_high_pc(0x00)
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_IGWriteSpiCommControlPin")
	.dwattr $C$DW$227, DW_AT_external
	.dwattr $C$DW$227, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$227, DW_AT_TI_begin_line(0x6c5)
	.dwattr $C$DW$227, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$227, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/InitGpio.c",line 1734,column 1,is_stmt,address _IGWriteSpiCommControlPin,isa 0

	.dwfde $C$DW$CIE, _IGWriteSpiCommControlPin
$C$DW$228	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$228, DW_AT_name("Pin")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$228, DW_AT_location[DW_OP_reg0]

$C$DW$229	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$229, DW_AT_name("State")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$229, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 1733 | void IGWriteSpiCommControlPin( unsigned int  Pin,  unsigned int  State)
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGWriteSpiCommControlPin     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_IGWriteSpiCommControlPin:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$230	.dwtag  DW_TAG_variable
	.dwattr $C$DW$230, DW_AT_name("Pin")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$230, DW_AT_location[DW_OP_breg20 -1]

$C$DW$231	.dwtag  DW_TAG_variable
	.dwattr $C$DW$231, DW_AT_name("State")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$231, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 1735 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |1734| 
        MOV       *-SP[1],AL            ; [CPU_] |1734| 
	.dwpsn	file "../source/InitGpio.c",line 1736,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1736 | GPIO_WritePin(Pin, State);                                             
;----------------------------------------------------------------------
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$232, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1736| 
        ; call occurs [#_GPIO_WritePin] ; [] |1736| 
	.dwpsn	file "../source/InitGpio.c",line 1737,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$227, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$227, DW_AT_TI_end_line(0x6c9)
	.dwattr $C$DW$227, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$227

	.sect	".text:_IGWriteSpiValveResetPin"
	.clink
	.global	_IGWriteSpiValveResetPin

$C$DW$234	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$234, DW_AT_name("IGWriteSpiValveResetPin")
	.dwattr $C$DW$234, DW_AT_low_pc(_IGWriteSpiValveResetPin)
	.dwattr $C$DW$234, DW_AT_high_pc(0x00)
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_IGWriteSpiValveResetPin")
	.dwattr $C$DW$234, DW_AT_external
	.dwattr $C$DW$234, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$234, DW_AT_TI_begin_line(0x6d6)
	.dwattr $C$DW$234, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$234, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/InitGpio.c",line 1751,column 1,is_stmt,address _IGWriteSpiValveResetPin,isa 0

	.dwfde $C$DW$CIE, _IGWriteSpiValveResetPin
$C$DW$235	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$235, DW_AT_name("Pin")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$235, DW_AT_location[DW_OP_reg0]

$C$DW$236	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$236, DW_AT_name("State")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$236, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 1750 | void IGWriteSpiValveResetPin( unsigned int  Pin,  unsigned int  State) 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGWriteSpiValveResetPin      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_IGWriteSpiValveResetPin:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$237	.dwtag  DW_TAG_variable
	.dwattr $C$DW$237, DW_AT_name("Pin")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_Pin")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_breg20 -1]

$C$DW$238	.dwtag  DW_TAG_variable
	.dwattr $C$DW$238, DW_AT_name("State")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_State")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 1752 | //!Control the GPIO to high or low with respect to the pin indicated   
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |1751| 
        MOV       *-SP[1],AL            ; [CPU_] |1751| 
	.dwpsn	file "../source/InitGpio.c",line 1753,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1753 | GPIO_WritePin(Pin, State);                                             
;----------------------------------------------------------------------
$C$DW$239	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$239, DW_AT_low_pc(0x00)
	.dwattr $C$DW$239, DW_AT_name("_GPIO_WritePin")
	.dwattr $C$DW$239, DW_AT_TI_call

        LCR       #_GPIO_WritePin       ; [CPU_] |1753| 
        ; call occurs [#_GPIO_WritePin] ; [] |1753| 
	.dwpsn	file "../source/InitGpio.c",line 1754,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$240	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$240, DW_AT_low_pc(0x00)
	.dwattr $C$DW$240, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$234, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$234, DW_AT_TI_end_line(0x6da)
	.dwattr $C$DW$234, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$234

	.sect	".text:_IGGpioConfig_SampleStartButton"
	.clink
	.global	_IGGpioConfig_SampleStartButton

$C$DW$241	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$241, DW_AT_name("IGGpioConfig_SampleStartButton")
	.dwattr $C$DW$241, DW_AT_low_pc(_IGGpioConfig_SampleStartButton)
	.dwattr $C$DW$241, DW_AT_high_pc(0x00)
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_IGGpioConfig_SampleStartButton")
	.dwattr $C$DW$241, DW_AT_external
	.dwattr $C$DW$241, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$241, DW_AT_TI_begin_line(0x6e7)
	.dwattr $C$DW$241, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$241, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/InitGpio.c",line 1768,column 1,is_stmt,address _IGGpioConfig_SampleStartButton,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_SampleStartButton
;----------------------------------------------------------------------
; 1767 | void IGGpioConfig_SampleStartButton(void)                              
; 1770 | GpioPinConfig GpioSetupSampleStartBtn;                                 
; 1772 | //!Set the following parameters of the Sample start GPIO pin:          
; 1773 | //!GPIO pin no. to SAMPLE_START_BUTTON_GPIO                            
; 1774 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1775 | //!GPIO Pin as input pin                                               
; 1776 | //!GPIO pin as GPIO_QUAL6                                              
; 1777 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_SampleStartButton FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_SampleStartButton:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$242	.dwtag  DW_TAG_variable
	.dwattr $C$DW$242, DW_AT_name("GpioSetupSampleStartBtn")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_GpioSetupSampleStartBtn")
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$242, DW_AT_location[DW_OP_breg20 -5]

	.dwpsn	file "../source/InitGpio.c",line 1778,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1778 | GpioSetupSampleStartBtn.GpioPin = SAMPLE_START_BUTTON_GPIO;            
;----------------------------------------------------------------------
        MOVB      *-SP[5],#126,UNC      ; [CPU_] |1778| 
	.dwpsn	file "../source/InitGpio.c",line 1779,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1779 | GpioSetupSampleStartBtn.GpioCpuMuxSel = GPIO_MUX_CPU1;                 
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1779| 
	.dwpsn	file "../source/InitGpio.c",line 1780,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1780 | GpioSetupSampleStartBtn.GpioInputOutput = GPIO_INPUT;                  
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |1780| 
	.dwpsn	file "../source/InitGpio.c",line 1781,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1781 | GpioSetupSampleStartBtn.GpioFlags = GPIO_QUAL6;                        
;----------------------------------------------------------------------
        MOVB      *-SP[4],#32,UNC       ; [CPU_] |1781| 
	.dwpsn	file "../source/InitGpio.c",line 1782,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1782 | GpioSetupSampleStartBtn.GpioPeripheral = 0;                            
; 1784 | //!After setting all the configurations of the pin,                    
; 1785 | //!Update the configurations of the Sample start control GPIO          
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |1782| 
	.dwpsn	file "../source/InitGpio.c",line 1786,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1786 | IGConfigGpioPin(GpioSetupSampleStartBtn);                              
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1786| 
        SUBB      XAR4,#5               ; [CPU_U] |1786| 
        MOVZ      AR4,AR4               ; [CPU_] |1786| 
$C$DW$243	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$243, DW_AT_low_pc(0x00)
	.dwattr $C$DW$243, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$243, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1786| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1786| 
	.dwpsn	file "../source/InitGpio.c",line 1788,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$244	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$244, DW_AT_low_pc(0x00)
	.dwattr $C$DW$244, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$241, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$241, DW_AT_TI_end_line(0x6fc)
	.dwattr $C$DW$241, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$241

	.sect	".text:_IGGpioConfig_WasteDetectPin"
	.clink
	.global	_IGGpioConfig_WasteDetectPin

$C$DW$245	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$245, DW_AT_name("IGGpioConfig_WasteDetectPin")
	.dwattr $C$DW$245, DW_AT_low_pc(_IGGpioConfig_WasteDetectPin)
	.dwattr $C$DW$245, DW_AT_high_pc(0x00)
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_IGGpioConfig_WasteDetectPin")
	.dwattr $C$DW$245, DW_AT_external
	.dwattr $C$DW$245, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$245, DW_AT_TI_begin_line(0x70b)
	.dwattr $C$DW$245, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$245, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/InitGpio.c",line 1804,column 1,is_stmt,address _IGGpioConfig_WasteDetectPin,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_WasteDetectPin
;----------------------------------------------------------------------
; 1803 | void IGGpioConfig_WasteDetectPin(void)                                 
; 1805 | GpioPinConfig GpioSetupWasteDetectPin;                                 
; 1807 | //!Set the following parameters of the Waste bin detect GPIO pin:      
; 1808 | //!GPIO pin no. to WASTE_DETECT_GPIO                                   
; 1809 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1810 | //!GPIO Pin as input pin                                               
; 1811 | //!GPIO pin as GPIO_QUAL6                                              
; 1812 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_WasteDetectPin  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_WasteDetectPin:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$246	.dwtag  DW_TAG_variable
	.dwattr $C$DW$246, DW_AT_name("GpioSetupWasteDetectPin")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_GpioSetupWasteDetectPin")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_breg20 -5]

	.dwpsn	file "../source/InitGpio.c",line 1813,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1813 | GpioSetupWasteDetectPin.GpioPin = WASTE_DETECT_GPIO;                   
;----------------------------------------------------------------------
        MOVB      *-SP[5],#100,UNC      ; [CPU_] |1813| 
	.dwpsn	file "../source/InitGpio.c",line 1814,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1814 | GpioSetupWasteDetectPin.GpioCpuMuxSel = GPIO_MUX_CPU1;                 
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1814| 
	.dwpsn	file "../source/InitGpio.c",line 1815,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1815 | GpioSetupWasteDetectPin.GpioInputOutput = GPIO_INPUT;                  
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |1815| 
	.dwpsn	file "../source/InitGpio.c",line 1816,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1816 | GpioSetupWasteDetectPin.GpioFlags = GPIO_QUAL6;                        
;----------------------------------------------------------------------
        MOVB      *-SP[4],#32,UNC       ; [CPU_] |1816| 
	.dwpsn	file "../source/InitGpio.c",line 1817,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1817 | GpioSetupWasteDetectPin.GpioPeripheral = 0;                            
; 1819 | //!After setting all the configurations of the pin,                    
; 1820 | //!Update the configurations of the waste bin detect GPIO              
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |1817| 
	.dwpsn	file "../source/InitGpio.c",line 1821,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1821 | IGConfigGpioPin(GpioSetupWasteDetectPin);                              
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1821| 
        SUBB      XAR4,#5               ; [CPU_U] |1821| 
        MOVZ      AR4,AR4               ; [CPU_] |1821| 
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$247, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1821| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1821| 
	.dwpsn	file "../source/InitGpio.c",line 1822,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$248	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$248, DW_AT_low_pc(0x00)
	.dwattr $C$DW$248, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$245, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$245, DW_AT_TI_end_line(0x71e)
	.dwattr $C$DW$245, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$245

	.sect	".text:_IGGpioConfig_ReagentPrimingCheck"
	.clink
	.global	_IGGpioConfig_ReagentPrimingCheck

$C$DW$249	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$249, DW_AT_name("IGGpioConfig_ReagentPrimingCheck")
	.dwattr $C$DW$249, DW_AT_low_pc(_IGGpioConfig_ReagentPrimingCheck)
	.dwattr $C$DW$249, DW_AT_high_pc(0x00)
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_IGGpioConfig_ReagentPrimingCheck")
	.dwattr $C$DW$249, DW_AT_external
	.dwattr $C$DW$249, DW_AT_TI_begin_file("../source/InitGpio.c")
	.dwattr $C$DW$249, DW_AT_TI_begin_line(0x72e)
	.dwattr $C$DW$249, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$249, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "../source/InitGpio.c",line 1839,column 1,is_stmt,address _IGGpioConfig_ReagentPrimingCheck,isa 0

	.dwfde $C$DW$CIE, _IGGpioConfig_ReagentPrimingCheck
;----------------------------------------------------------------------
; 1838 | void IGGpioConfig_ReagentPrimingCheck(void)                            
; 1840 | GpioPinConfig GpioSetupDiluentDetectPin;                               
; 1842 | //!Set the following parameters of the Diluent line prime GPIO pin:    
; 1843 | //!GPIO pin no. to DILUENT_PRIME_CHECK                                 
; 1844 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1845 | //!GPIO Pin as input pin                                               
; 1846 | //!GPIO pin as GPIO_QUAL6                                              
; 1847 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IGGpioConfig_ReagentPrimingCheck FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 15 Auto,  0 SOE     *
;***************************************************************

_IGGpioConfig_ReagentPrimingCheck:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$250	.dwtag  DW_TAG_variable
	.dwattr $C$DW$250, DW_AT_name("GpioSetupDiluentDetectPin")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_GpioSetupDiluentDetectPin")
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$250, DW_AT_location[DW_OP_breg20 -5]

$C$DW$251	.dwtag  DW_TAG_variable
	.dwattr $C$DW$251, DW_AT_name("GpioSetupLyseDetectPin")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_GpioSetupLyseDetectPin")
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$251, DW_AT_location[DW_OP_breg20 -10]

$C$DW$252	.dwtag  DW_TAG_variable
	.dwattr $C$DW$252, DW_AT_name("GpioSetupRinseDetectPin")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_GpioSetupRinseDetectPin")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_breg20 -15]

	.dwpsn	file "../source/InitGpio.c",line 1848,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1848 | GpioSetupDiluentDetectPin.GpioPin = DILUENT_PRIME_CHECK;               
;----------------------------------------------------------------------
        MOVB      *-SP[5],#128,UNC      ; [CPU_] |1848| 
	.dwpsn	file "../source/InitGpio.c",line 1849,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1849 | GpioSetupDiluentDetectPin.GpioCpuMuxSel = GPIO_MUX_CPU1;               
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1849| 
	.dwpsn	file "../source/InitGpio.c",line 1850,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1850 | GpioSetupDiluentDetectPin.GpioInputOutput = GPIO_INPUT;                
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |1850| 
	.dwpsn	file "../source/InitGpio.c",line 1851,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1851 | GpioSetupDiluentDetectPin.GpioFlags = GPIO_QUAL6;                      
;----------------------------------------------------------------------
        MOVB      *-SP[4],#32,UNC       ; [CPU_] |1851| 
	.dwpsn	file "../source/InitGpio.c",line 1852,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1852 | GpioSetupDiluentDetectPin.GpioPeripheral = 0;                          
; 1854 | //!After setting all the configurations of the pin,                    
; 1855 | //!Update the configurations of the Diluent prime detect GPIO          
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |1852| 
	.dwpsn	file "../source/InitGpio.c",line 1856,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1856 | IGConfigGpioPin(GpioSetupDiluentDetectPin);                            
; 1858 | GpioPinConfig GpioSetupLyseDetectPin;                                  
; 1860 | //!Set the following parameters of the Lyse line prime GPIO pin:       
; 1861 | //!GPIO pin no. to LYSE_PRIME_CHECK                                    
; 1862 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1863 | //!GPIO Pin as input pin                                               
; 1864 | //!GPIO pin as GPIO_QUAL6                                              
; 1865 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1856| 
        SUBB      XAR4,#5               ; [CPU_U] |1856| 
        MOVZ      AR4,AR4               ; [CPU_] |1856| 
$C$DW$253	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$253, DW_AT_low_pc(0x00)
	.dwattr $C$DW$253, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$253, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1856| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1856| 
	.dwpsn	file "../source/InitGpio.c",line 1866,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1866 | GpioSetupLyseDetectPin.GpioPin = LYSE_PRIME_CHECK;                     
;----------------------------------------------------------------------
        MOVB      *-SP[10],#132,UNC     ; [CPU_] |1866| 
	.dwpsn	file "../source/InitGpio.c",line 1867,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1867 | GpioSetupLyseDetectPin.GpioCpuMuxSel = GPIO_MUX_CPU1;                  
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |1867| 
	.dwpsn	file "../source/InitGpio.c",line 1868,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1868 | GpioSetupLyseDetectPin.GpioInputOutput = GPIO_INPUT;                   
;----------------------------------------------------------------------
        MOV       *-SP[7],#0            ; [CPU_] |1868| 
	.dwpsn	file "../source/InitGpio.c",line 1869,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1869 | GpioSetupLyseDetectPin.GpioFlags = GPIO_QUAL6;                         
;----------------------------------------------------------------------
        MOVB      *-SP[9],#32,UNC       ; [CPU_] |1869| 
	.dwpsn	file "../source/InitGpio.c",line 1870,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1870 | GpioSetupLyseDetectPin.GpioPeripheral = 0;                             
; 1872 | //!After setting all the configurations of the pin,                    
; 1873 | //!Update the configurations of the Lyse prime detect GPIO             
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1870| 
	.dwpsn	file "../source/InitGpio.c",line 1874,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1874 | IGConfigGpioPin(GpioSetupLyseDetectPin);                               
; 1876 | GpioPinConfig GpioSetupRinseDetectPin;                                 
; 1878 | //!Set the following parameters of the Rinse line prime GPIO pin:      
; 1879 | //!GPIO pin no. to RINSE_PRIME_CHECK                                   
; 1880 | //!GPIO MUX select to GPIO_MUX_CPU1 to control from CPU1               
; 1881 | //!GPIO Pin as input pin                                               
; 1882 | //!GPIO pin as GPIO_QUAL6                                              
; 1883 | //!GPIO peripheral as ZERO                                             
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1874| 
        SUBB      XAR4,#10              ; [CPU_U] |1874| 
        MOVZ      AR4,AR4               ; [CPU_] |1874| 
$C$DW$254	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$254, DW_AT_low_pc(0x00)
	.dwattr $C$DW$254, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$254, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1874| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1874| 
	.dwpsn	file "../source/InitGpio.c",line 1884,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1884 | GpioSetupRinseDetectPin.GpioPin = RINSE_PRIME_CHECK;                   
;----------------------------------------------------------------------
        MOVB      *-SP[15],#134,UNC     ; [CPU_] |1884| 
	.dwpsn	file "../source/InitGpio.c",line 1885,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1885 | GpioSetupRinseDetectPin.GpioCpuMuxSel = GPIO_MUX_CPU1;                 
;----------------------------------------------------------------------
        MOV       *-SP[13],#0           ; [CPU_] |1885| 
	.dwpsn	file "../source/InitGpio.c",line 1886,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1886 | GpioSetupRinseDetectPin.GpioInputOutput = GPIO_INPUT;                  
;----------------------------------------------------------------------
        MOV       *-SP[12],#0           ; [CPU_] |1886| 
	.dwpsn	file "../source/InitGpio.c",line 1887,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1887 | GpioSetupRinseDetectPin.GpioFlags = GPIO_QUAL6;                        
;----------------------------------------------------------------------
        MOVB      *-SP[14],#32,UNC      ; [CPU_] |1887| 
	.dwpsn	file "../source/InitGpio.c",line 1888,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1888 | GpioSetupRinseDetectPin.GpioPeripheral = 0;                            
; 1890 | //!After setting all the configurations of the pin,                    
; 1891 | //!Update the configurations of the Rinse prime detect GPIO            
;----------------------------------------------------------------------
        MOV       *-SP[11],#0           ; [CPU_] |1888| 
	.dwpsn	file "../source/InitGpio.c",line 1892,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1892 | IGConfigGpioPin(GpioSetupRinseDetectPin);                              
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |1892| 
        SUBB      XAR4,#15              ; [CPU_U] |1892| 
        MOVZ      AR4,AR4               ; [CPU_] |1892| 
$C$DW$255	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$255, DW_AT_low_pc(0x00)
	.dwattr $C$DW$255, DW_AT_name("_IGConfigGpioPin")
	.dwattr $C$DW$255, DW_AT_TI_call

        LCR       #_IGConfigGpioPin     ; [CPU_] |1892| 
        ; call occurs [#_IGConfigGpioPin] ; [] |1892| 
	.dwpsn	file "../source/InitGpio.c",line 1895,column 2,is_stmt,isa 0
        SUBB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$256	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$256, DW_AT_low_pc(0x00)
	.dwattr $C$DW$256, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$249, DW_AT_TI_end_file("../source/InitGpio.c")
	.dwattr $C$DW$249, DW_AT_TI_end_line(0x767)
	.dwattr $C$DW$249, DW_AT_TI_end_column(0x02)
	.dwendentry
	.dwendtag $C$DW$249

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_GPIO_SetupPinOptions
	.global	_GPIO_SetupPinMux
	.global	_GPIO_WritePin
	.global	_InitGpio
	.global	_GPIO_ReadPin
	.global	_OIHomeSensor1State

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_name("GPIO_PARAMETERS")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x05)
$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$257, DW_AT_name("GpioPin")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_GpioPin")
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$258, DW_AT_name("GpioFlags")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_GpioFlags")
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$259, DW_AT_name("GpioCpuMuxSel")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_GpioCpuMuxSel")
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$260, DW_AT_name("GpioInputOutput")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_GpioInputOutput")
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$261, DW_AT_name("GpioPeripheral")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_GpioPeripheral")
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$20

$C$DW$T$21	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$21, DW_AT_name("GpioPinConfig")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_address_class(0x20)


$C$DW$T$23	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x19)
$C$DW$262	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$262, DW_AT_upper_bound(0x04)

	.dwendtag $C$DW$T$23


$C$DW$T$24	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x0a)
$C$DW$263	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$263, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x14)
$C$DW$264	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$264, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x23)
$C$DW$265	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$265, DW_AT_upper_bound(0x06)

	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x0f)
$C$DW$266	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$266, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$27

$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$19	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$19, DW_AT_name("Uint16")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$267	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$267, DW_AT_name("AL")
	.dwattr $C$DW$267, DW_AT_location[DW_OP_reg0]

$C$DW$268	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$268, DW_AT_name("AH")
	.dwattr $C$DW$268, DW_AT_location[DW_OP_reg1]

$C$DW$269	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$269, DW_AT_name("PL")
	.dwattr $C$DW$269, DW_AT_location[DW_OP_reg2]

$C$DW$270	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$270, DW_AT_name("PH")
	.dwattr $C$DW$270, DW_AT_location[DW_OP_reg3]

$C$DW$271	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$271, DW_AT_name("SP")
	.dwattr $C$DW$271, DW_AT_location[DW_OP_reg20]

$C$DW$272	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$272, DW_AT_name("XT")
	.dwattr $C$DW$272, DW_AT_location[DW_OP_reg21]

$C$DW$273	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$273, DW_AT_name("T")
	.dwattr $C$DW$273, DW_AT_location[DW_OP_reg22]

$C$DW$274	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$274, DW_AT_name("ST0")
	.dwattr $C$DW$274, DW_AT_location[DW_OP_reg23]

$C$DW$275	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$275, DW_AT_name("ST1")
	.dwattr $C$DW$275, DW_AT_location[DW_OP_reg24]

$C$DW$276	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$276, DW_AT_name("PC")
	.dwattr $C$DW$276, DW_AT_location[DW_OP_reg25]

$C$DW$277	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$277, DW_AT_name("RPC")
	.dwattr $C$DW$277, DW_AT_location[DW_OP_reg26]

$C$DW$278	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$278, DW_AT_name("FP")
	.dwattr $C$DW$278, DW_AT_location[DW_OP_reg28]

$C$DW$279	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$279, DW_AT_name("DP")
	.dwattr $C$DW$279, DW_AT_location[DW_OP_reg29]

$C$DW$280	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$280, DW_AT_name("SXM")
	.dwattr $C$DW$280, DW_AT_location[DW_OP_reg30]

$C$DW$281	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$281, DW_AT_name("PM")
	.dwattr $C$DW$281, DW_AT_location[DW_OP_reg31]

$C$DW$282	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$282, DW_AT_name("OVM")
	.dwattr $C$DW$282, DW_AT_location[DW_OP_regx 0x20]

$C$DW$283	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$283, DW_AT_name("PAGE0")
	.dwattr $C$DW$283, DW_AT_location[DW_OP_regx 0x21]

$C$DW$284	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$284, DW_AT_name("AMODE")
	.dwattr $C$DW$284, DW_AT_location[DW_OP_regx 0x22]

$C$DW$285	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$285, DW_AT_name("INTM")
	.dwattr $C$DW$285, DW_AT_location[DW_OP_regx 0x23]

$C$DW$286	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$286, DW_AT_name("IFR")
	.dwattr $C$DW$286, DW_AT_location[DW_OP_regx 0x24]

$C$DW$287	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$287, DW_AT_name("IER")
	.dwattr $C$DW$287, DW_AT_location[DW_OP_regx 0x25]

$C$DW$288	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$288, DW_AT_name("V")
	.dwattr $C$DW$288, DW_AT_location[DW_OP_regx 0x26]

$C$DW$289	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$289, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$289, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$290	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$290, DW_AT_name("VOL")
	.dwattr $C$DW$290, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$291	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$291, DW_AT_name("AR0")
	.dwattr $C$DW$291, DW_AT_location[DW_OP_reg4]

$C$DW$292	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$292, DW_AT_name("XAR0")
	.dwattr $C$DW$292, DW_AT_location[DW_OP_reg5]

$C$DW$293	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$293, DW_AT_name("AR1")
	.dwattr $C$DW$293, DW_AT_location[DW_OP_reg6]

$C$DW$294	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$294, DW_AT_name("XAR1")
	.dwattr $C$DW$294, DW_AT_location[DW_OP_reg7]

$C$DW$295	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$295, DW_AT_name("AR2")
	.dwattr $C$DW$295, DW_AT_location[DW_OP_reg8]

$C$DW$296	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$296, DW_AT_name("XAR2")
	.dwattr $C$DW$296, DW_AT_location[DW_OP_reg9]

$C$DW$297	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$297, DW_AT_name("AR3")
	.dwattr $C$DW$297, DW_AT_location[DW_OP_reg10]

$C$DW$298	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$298, DW_AT_name("XAR3")
	.dwattr $C$DW$298, DW_AT_location[DW_OP_reg11]

$C$DW$299	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$299, DW_AT_name("AR4")
	.dwattr $C$DW$299, DW_AT_location[DW_OP_reg12]

$C$DW$300	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$300, DW_AT_name("XAR4")
	.dwattr $C$DW$300, DW_AT_location[DW_OP_reg13]

$C$DW$301	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$301, DW_AT_name("AR5")
	.dwattr $C$DW$301, DW_AT_location[DW_OP_reg14]

$C$DW$302	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$302, DW_AT_name("XAR5")
	.dwattr $C$DW$302, DW_AT_location[DW_OP_reg15]

$C$DW$303	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$303, DW_AT_name("AR6")
	.dwattr $C$DW$303, DW_AT_location[DW_OP_reg16]

$C$DW$304	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$304, DW_AT_name("XAR6")
	.dwattr $C$DW$304, DW_AT_location[DW_OP_reg17]

$C$DW$305	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$305, DW_AT_name("AR7")
	.dwattr $C$DW$305, DW_AT_location[DW_OP_reg18]

$C$DW$306	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$306, DW_AT_name("XAR7")
	.dwattr $C$DW$306, DW_AT_location[DW_OP_reg19]

$C$DW$307	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$307, DW_AT_name("R0HL")
	.dwattr $C$DW$307, DW_AT_location[DW_OP_regx 0x29]

$C$DW$308	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$308, DW_AT_name("R0H")
	.dwattr $C$DW$308, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$309	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$309, DW_AT_name("R1HL")
	.dwattr $C$DW$309, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$310	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$310, DW_AT_name("R1H")
	.dwattr $C$DW$310, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$311	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$311, DW_AT_name("R2HL")
	.dwattr $C$DW$311, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$312	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$312, DW_AT_name("R2H")
	.dwattr $C$DW$312, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$313	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$313, DW_AT_name("R3HL")
	.dwattr $C$DW$313, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$314	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$314, DW_AT_name("R3H")
	.dwattr $C$DW$314, DW_AT_location[DW_OP_regx 0x30]

$C$DW$315	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$315, DW_AT_name("R4HL")
	.dwattr $C$DW$315, DW_AT_location[DW_OP_regx 0x31]

$C$DW$316	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$316, DW_AT_name("R4H")
	.dwattr $C$DW$316, DW_AT_location[DW_OP_regx 0x32]

$C$DW$317	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$317, DW_AT_name("R5HL")
	.dwattr $C$DW$317, DW_AT_location[DW_OP_regx 0x33]

$C$DW$318	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$318, DW_AT_name("R5H")
	.dwattr $C$DW$318, DW_AT_location[DW_OP_regx 0x34]

$C$DW$319	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$319, DW_AT_name("R6HL")
	.dwattr $C$DW$319, DW_AT_location[DW_OP_regx 0x35]

$C$DW$320	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$320, DW_AT_name("R6H")
	.dwattr $C$DW$320, DW_AT_location[DW_OP_regx 0x36]

$C$DW$321	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$321, DW_AT_name("R7HL")
	.dwattr $C$DW$321, DW_AT_location[DW_OP_regx 0x37]

$C$DW$322	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$322, DW_AT_name("R7H")
	.dwattr $C$DW$322, DW_AT_location[DW_OP_regx 0x38]

$C$DW$323	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$323, DW_AT_name("RBL")
	.dwattr $C$DW$323, DW_AT_location[DW_OP_regx 0x49]

$C$DW$324	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$324, DW_AT_name("RB")
	.dwattr $C$DW$324, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$325	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$325, DW_AT_name("STFL")
	.dwattr $C$DW$325, DW_AT_location[DW_OP_regx 0x27]

$C$DW$326	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$326, DW_AT_name("STF")
	.dwattr $C$DW$326, DW_AT_location[DW_OP_regx 0x28]

$C$DW$327	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$327, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$327, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


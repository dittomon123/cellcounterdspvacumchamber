;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Fri Jan 29 15:14:06 2021                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cell Counter\V1.3.1_And_V1.3.0\alphaCellCounterCPU1\CPU1_FLASH")
	.global	_g_usGetWriteIndexes
_g_usGetWriteIndexes:	.usect	"GETWRITEIDX",4,1,0
$C$DW$1	.dwtag  DW_TAG_variable
	.dwattr $C$DW$1, DW_AT_name("g_usGetWriteIndexes")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_g_usGetWriteIndexes")
	.dwattr $C$DW$1, DW_AT_location[DW_OP_addr _g_usGetWriteIndexes]
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$1, DW_AT_external

	.global	_g_usPutWriteIndexes
_g_usPutWriteIndexes:	.usect	"PUTWRITEIDX",4,1,0
$C$DW$2	.dwtag  DW_TAG_variable
	.dwattr $C$DW$2, DW_AT_name("g_usPutWriteIndexes")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_g_usPutWriteIndexes")
	.dwattr $C$DW$2, DW_AT_location[DW_OP_addr _g_usPutWriteIndexes]
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$2, DW_AT_external

	.global	_g_usPutReadIndexes
_g_usPutReadIndexes:	.usect	"PUTREADIDX",4,1,0
$C$DW$3	.dwtag  DW_TAG_variable
	.dwattr $C$DW$3, DW_AT_name("g_usPutReadIndexes")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_g_usPutReadIndexes")
	.dwattr $C$DW$3, DW_AT_location[DW_OP_addr _g_usPutReadIndexes]
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$3, DW_AT_external

	.global	_g_usGetReadIndexes
_g_usGetReadIndexes:	.usect	"GETREADIDX",4,1,0
$C$DW$4	.dwtag  DW_TAG_variable
	.dwattr $C$DW$4, DW_AT_name("g_usGetReadIndexes")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_g_usGetReadIndexes")
	.dwattr $C$DW$4, DW_AT_location[DW_OP_addr _g_usGetReadIndexes]
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$4, DW_AT_external

$C$DW$5	.dwtag  DW_TAG_variable
	.dwattr $C$DW$5, DW_AT_name("IpcRegs")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_IpcRegs")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external

	.global	_g_asIPCCPU1toCPU2Buffers
_g_asIPCCPU1toCPU2Buffers:	.usect	"PUTBUFFER",128,1,1
$C$DW$6	.dwtag  DW_TAG_variable
	.dwattr $C$DW$6, DW_AT_name("g_asIPCCPU1toCPU2Buffers")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_g_asIPCCPU1toCPU2Buffers")
	.dwattr $C$DW$6, DW_AT_location[DW_OP_addr _g_asIPCCPU1toCPU2Buffers]
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$6, DW_AT_external

	.global	_g_asIPCCPU2toCPU1Buffers
_g_asIPCCPU2toCPU1Buffers:	.usect	"GETBUFFER",128,1,1
$C$DW$7	.dwtag  DW_TAG_variable
	.dwattr $C$DW$7, DW_AT_name("g_asIPCCPU2toCPU1Buffers")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_g_asIPCCPU2toCPU1Buffers")
	.dwattr $C$DW$7, DW_AT_location[DW_OP_addr _g_asIPCCPU2toCPU1Buffers]
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$7, DW_AT_external

;	D:\Softwares\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\DITTOM~1.DEV\\AppData\\Local\\Temp\\1940012 
	.sect	".text:_IPCInitialize"
	.clink
	.global	_IPCInitialize

$C$DW$8	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$8, DW_AT_name("IPCInitialize")
	.dwattr $C$DW$8, DW_AT_low_pc(_IPCInitialize)
	.dwattr $C$DW$8, DW_AT_high_pc(0x00)
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_IPCInitialize")
	.dwattr $C$DW$8, DW_AT_external
	.dwattr $C$DW$8, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$8, DW_AT_TI_begin_line(0x69)
	.dwattr $C$DW$8, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$8, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 107,column 1,is_stmt,address _IPCInitialize,isa 0

	.dwfde $C$DW$CIE, _IPCInitialize
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_name("psController")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_reg12]

$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_name("usCPU2IpcInterrupt")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_usCPU2IpcInterrupt")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg0]

$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_name("usCPU1IpcInterrupt")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_usCPU1IpcInterrupt")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 105 | IPCInitialize (volatile tIpcController *psController, uint16_t usCPU2Ip
;     | cInterrupt,                                                            
; 106 | uint16_t usCPU1IpcInterrupt)                                           
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCInitialize                FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_IPCInitialize:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$12	.dwtag  DW_TAG_variable
	.dwattr $C$DW$12, DW_AT_name("psController")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_breg20 -2]

$C$DW$13	.dwtag  DW_TAG_variable
	.dwattr $C$DW$13, DW_AT_name("usCPU2IpcInterrupt")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_usCPU2IpcInterrupt")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_breg20 -3]

$C$DW$14	.dwtag  DW_TAG_variable
	.dwattr $C$DW$14, DW_AT_name("usCPU1IpcInterrupt")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_usCPU1IpcInterrupt")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -4]

;----------------------------------------------------------------------
; 109 | #if defined(CPU1)                                                      
; 110 | // CPU1toCPU2PutBuffer and Index Initialization                        
;----------------------------------------------------------------------
        MOV       *-SP[4],AH            ; [CPU_] |107| 
        MOV       *-SP[3],AL            ; [CPU_] |107| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |107| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 111,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 111 | psController->psPutBuffer = &g_asIPCCPU1toCPU2Buffers[usCPU2IpcInterrup
;     | t-1][0];                                                               
;----------------------------------------------------------------------
        CLRC      SXM                   ; [CPU_] 
        ADDB      AL,#-1                ; [CPU_] |111| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |111| 
        MOVL      XAR4,#_g_asIPCCPU1toCPU2Buffers ; [CPU_U] |111| 
        MOV       ACC,AL << 5           ; [CPU_] |111| 
        ADDL      XAR4,ACC              ; [CPU_] |111| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |111| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 112,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 112 | psController->pusPutWriteIndex = &g_usPutWriteIndexes[usCPU2IpcInterrup
;     | t-1];                                                                  
;----------------------------------------------------------------------
        MOVZ      AR5,*-SP[3]           ; [CPU_] |112| 
        MOVL      XAR6,*-SP[2]          ; [CPU_] |112| 
        MOVL      XAR4,#_g_usPutWriteIndexes ; [CPU_U] |112| 
        SUBB      XAR5,#1               ; [CPU_U] |112| 
        MOVL      ACC,XAR4              ; [CPU_] |112| 
        ADDU      ACC,AR5               ; [CPU_] |112| 
        MOVL      *+XAR6[4],ACC         ; [CPU_] |112| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 113,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 113 | psController->pusGetReadIndex = &g_usGetReadIndexes[usCPU1IpcInterrupt-
;     | 1];                                                                    
;----------------------------------------------------------------------
        MOVZ      AR5,*-SP[4]           ; [CPU_] |113| 
        MOVL      XAR4,#_g_usGetReadIndexes ; [CPU_U] |113| 
        MOVL      ACC,XAR4              ; [CPU_] |113| 
        SUBB      XAR5,#1               ; [CPU_U] |113| 
        ADDU      ACC,AR5               ; [CPU_] |113| 
        MOVL      XAR4,ACC              ; [CPU_] |113| 
        MOVB      ACC,#12               ; [CPU_] |113| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |113| 
        MOVL      XAR5,ACC              ; [CPU_] |113| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |113| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 114,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 114 | psController->ulPutFlag = (uint32_t)(1 << (usCPU2IpcInterrupt - 1));   
; 116 | // CPU1toCPU2GetBuffer and Index Initialization                        
;----------------------------------------------------------------------
        SETC      SXM                   ; [CPU_] 
        MOV       AL,*-SP[3]            ; [CPU_] |114| 
        ADDB      AL,#-1                ; [CPU_] |114| 
        MOV       T,AL                  ; [CPU_] |114| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |114| 
        MOVB      AL,#1                 ; [CPU_] |114| 
        LSL       AL,T                  ; [CPU_] |114| 
        MOV       ACC,AL                ; [CPU_] |114| 
        MOVL      *+XAR4[2],ACC         ; [CPU_] |114| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 117,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 117 | psController->psGetBuffer = &g_asIPCCPU2toCPU1Buffers[usCPU1IpcInterrup
;     | t-1][0];                                                               
;----------------------------------------------------------------------
        CLRC      SXM                   ; [CPU_] 
        MOV       AL,*-SP[4]            ; [CPU_] |117| 
        MOVL      XAR5,#_g_asIPCCPU2toCPU1Buffers ; [CPU_U] |117| 
        ADDB      AL,#-1                ; [CPU_] |117| 
        MOV       ACC,AL << 5           ; [CPU_] |117| 
        ADDL      XAR5,ACC              ; [CPU_] |117| 
        MOVB      ACC,#8                ; [CPU_] |117| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |117| 
        MOVL      XAR4,ACC              ; [CPU_] |117| 
        MOVL      *+XAR4[0],XAR5        ; [CPU_] |117| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 118,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 118 | psController->pusGetWriteIndex = &g_usGetWriteIndexes[usCPU1IpcInterrup
;     | t-1];                                                                  
;----------------------------------------------------------------------
        MOVZ      AR5,*-SP[4]           ; [CPU_] |118| 
        MOVL      XAR4,#_g_usGetWriteIndexes ; [CPU_U] |118| 
        MOVL      ACC,XAR4              ; [CPU_] |118| 
        SUBB      XAR5,#1               ; [CPU_U] |118| 
        ADDU      ACC,AR5               ; [CPU_] |118| 
        MOVL      XAR4,ACC              ; [CPU_] |118| 
        MOVB      ACC,#10               ; [CPU_] |118| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |118| 
        MOVL      XAR5,ACC              ; [CPU_] |118| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |118| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 119,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 119 | psController->pusPutReadIndex = &g_usPutReadIndexes[usCPU2IpcInterrupt-
;     | 1];                                                                    
; 120 | #elif defined(CPU2)                                                    
; 121 | // CPU2toCPU1PutBuffer and Index Initialization                        
; 122 | psController->psPutBuffer = &g_asIPCCPU2toCPU1Buffers[usCPU1IpcInterrup
;     | t-1][0];                                                               
; 123 | psController->pusPutWriteIndex = &g_usPutWriteIndexes[usCPU1IpcInterrup
;     | t-1];                                                                  
; 124 | psController->pusGetReadIndex = &g_usGetReadIndexes[usCPU2IpcInterrupt-
;     | 1];                                                                    
; 125 | psController->ulPutFlag = (uint32_t)(1 << (usCPU1IpcInterrupt - 1));   
; 127 | // CPU1toCPU2GetBuffer and Index Initialization                        
; 128 | psController->psGetBuffer = &g_asIPCCPU1toCPU2Buffers[usCPU2IpcInterrup
;     | t-1][0];                                                               
; 129 | psController->pusGetWriteIndex = &g_usGetWriteIndexes[usCPU2IpcInterrup
;     | t-1];                                                                  
; 130 | psController->pusPutReadIndex = &g_usPutReadIndexes[usCPU1IpcInterrupt-
;     | 1];                                                                    
; 131 | #endif                                                                 
; 132 | // Initialize PutBuffer WriteIndex = 0 and GetBuffer ReadIndex = 0     
;----------------------------------------------------------------------
        MOVZ      AR5,*-SP[3]           ; [CPU_] |119| 
        MOVL      XAR4,#_g_usPutReadIndexes ; [CPU_U] |119| 
        MOVL      XAR6,*-SP[2]          ; [CPU_] |119| 
        MOVL      ACC,XAR4              ; [CPU_] |119| 
        SUBB      XAR5,#1               ; [CPU_U] |119| 
        ADDU      ACC,AR5               ; [CPU_] |119| 
        MOVL      *+XAR6[6],ACC         ; [CPU_] |119| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 133,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 133 | *(psController->pusPutWriteIndex) = 0;                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |133| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |133| 
        MOV       *+XAR4[0],#0          ; [CPU_] |133| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 134,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 134 | *(psController->pusGetReadIndex) = 0;                                  
;----------------------------------------------------------------------
        MOVB      ACC,#12               ; [CPU_] |134| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |134| 
        MOVL      XAR4,ACC              ; [CPU_] |134| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |134| 
        MOV       *+XAR4[0],#0          ; [CPU_] |134| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 136,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$15	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$15, DW_AT_low_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$8, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$8, DW_AT_TI_end_line(0x88)
	.dwattr $C$DW$8, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$8

	.sect	".text:_IpcPut"
	.clink
	.global	_IpcPut

$C$DW$16	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$16, DW_AT_name("IpcPut")
	.dwattr $C$DW$16, DW_AT_low_pc(_IpcPut)
	.dwattr $C$DW$16, DW_AT_high_pc(0x00)
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_IpcPut")
	.dwattr $C$DW$16, DW_AT_external
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$16, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$16, DW_AT_TI_begin_line(0x9f)
	.dwattr $C$DW$16, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$16, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 161,column 1,is_stmt,address _IpcPut,isa 0

	.dwfde $C$DW$CIE, _IpcPut
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_name("psController")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg12]

$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_name("psMessage")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg14]

$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_name("bBlock")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 159 | IpcPut (volatile tIpcController *psController, tIpcMessage *psMessage, 
; 160 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IpcPut                       FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_IpcPut:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$20	.dwtag  DW_TAG_variable
	.dwattr $C$DW$20, DW_AT_name("psController")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -2]

$C$DW$21	.dwtag  DW_TAG_variable
	.dwattr $C$DW$21, DW_AT_name("psMessage")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_breg20 -4]

$C$DW$22	.dwtag  DW_TAG_variable
	.dwattr $C$DW$22, DW_AT_name("bBlock")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_breg20 -5]

$C$DW$23	.dwtag  DW_TAG_variable
	.dwattr $C$DW$23, DW_AT_name("writeIndex")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_writeIndex")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -6]

$C$DW$24	.dwtag  DW_TAG_variable
	.dwattr $C$DW$24, DW_AT_name("readIndex")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_readIndex")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -7]

$C$DW$25	.dwtag  DW_TAG_variable
	.dwattr $C$DW$25, DW_AT_name("returnStatus")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_returnStatus")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -8]

;----------------------------------------------------------------------
; 162 | uint16_t writeIndex;                                                   
; 163 | uint16_t readIndex;                                                    
;----------------------------------------------------------------------
        MOV       *-SP[5],AL            ; [CPU_] |161| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |161| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |161| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 164,column 27,is_stmt,isa 0
;----------------------------------------------------------------------
; 164 | uint16_t returnStatus = STATUS_PASS;                                   
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |164| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 166,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 166 | writeIndex = *(psController->pusPutWriteIndex);                        
;----------------------------------------------------------------------
        MOVL      XAR7,*+XAR4[4]        ; [CPU_] |166| 
        MOV       AL,*+XAR7[0]          ; [CPU_] |166| 
        MOV       *-SP[6],AL            ; [CPU_] |166| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 167,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 167 | readIndex = *(psController->pusPutReadIndex);                          
; 169 | // Wait until Put Buffer slot is free                                  
;----------------------------------------------------------------------
        MOVL      XAR7,*+XAR4[6]        ; [CPU_] |167| 
        MOV       AL,*+XAR7[0]          ; [CPU_] |167| 
        MOV       *-SP[7],AL            ; [CPU_] |167| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 170,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 170 | while (((writeIndex + 1) & MAX_BUFFER_INDEX) == readIndex)             
; 172 |     // If designated as a "Blocking" function, and Put buffer is full, 
; 173 |     // return immediately with fail status.                            
; 174 |     //                                                                 
;----------------------------------------------------------------------
        B         $C$L3,UNC             ; [CPU_] |170| 
        ; branch occurs ; [] |170| 
$C$L1:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 175,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 175 | if (!bBlock)                                                           
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |175| 
        B         $C$L2,NEQ             ; [CPU_] |175| 
        ; branchcc occurs ; [] |175| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 177,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 177 | returnStatus = STATUS_FAIL;                                            
;----------------------------------------------------------------------
        MOVB      *-SP[8],#1,UNC        ; [CPU_] |177| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 178,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 178 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L4,UNC             ; [CPU_] |178| 
        ; branch occurs ; [] |178| 
$C$L2:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 181,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 181 | readIndex = *(psController->pusPutReadIndex);                          
;----------------------------------------------------------------------
        MOVL      XAR7,*+XAR4[6]        ; [CPU_] |181| 
        MOV       AL,*+XAR7[0]          ; [CPU_] |181| 
        MOV       *-SP[7],AL            ; [CPU_] |181| 
$C$L3:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 170,column 12,is_stmt,isa 0
        MOVU      ACC,*-SP[7]           ; [CPU_] |170| 
        MOVL      XAR6,ACC              ; [CPU_] |170| 
        MOV       AL,*-SP[6]            ; [CPU_] |170| 
        ADDB      AL,#1                 ; [CPU_] |170| 
        ANDB      AL,#0x03              ; [CPU_] |170| 
        MOVZ      AR7,AL                ; [CPU_] |170| 
        MOVL      ACC,XAR6              ; [CPU_] |170| 
        CMPL      ACC,XAR7              ; [CPU_] |170| 
        B         $C$L1,EQ              ; [CPU_] |170| 
        ; branchcc occurs ; [] |170| 
$C$L4:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 184,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 184 | if (returnStatus != STATUS_FAIL)                                       
; 186 |     // When slot is free, Write Message to PutBuffer, update PutWriteIn
;     | dex,                                                                   
; 187 |     // and set the CPU IPC INT Flag                                    
;----------------------------------------------------------------------
        MOV       AL,*-SP[8]            ; [CPU_] |184| 
        CMPB      AL,#1                 ; [CPU_] |184| 
        B         $C$L5,EQ              ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 188,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 188 | psController->psPutBuffer[writeIndex] = *psMessage;                    
;----------------------------------------------------------------------
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR7,*-SP[4]          ; [CPU_] |188| 
        MOV       ACC,*-SP[6] << 3      ; [CPU_] |188| 
        ADDL      ACC,*+XAR4[0]         ; [CPU_] |188| 
        MOVL      XAR4,ACC              ; [CPU_] |188| 
        RPT       #7
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |188| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 190,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 190 | writeIndex = (writeIndex + 1) & MAX_BUFFER_INDEX;                      
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |190| 
        ADDB      AL,#1                 ; [CPU_] |190| 
        ANDB      AL,#0x03              ; [CPU_] |190| 
        MOV       *-SP[6],AL            ; [CPU_] |190| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 191,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 191 | *(psController->pusPutWriteIndex) = writeIndex;                        
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |191| 
        MOVL      XAR7,*+XAR4[4]        ; [CPU_] |191| 
        MOV       *+XAR7[0],AL          ; [CPU_] |191| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 193,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 193 | IpcRegs.IPCSET.all |= psController->ulPutFlag;                         
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |193| 
        MOVW      DP,#_IpcRegs+4        ; [CPU_U] 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |193| 
        OR        @_IpcRegs+4,AL        ; [CPU_] |193| 
        OR        @_IpcRegs+5,AH        ; [CPU_] |193| 
$C$L5:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 196,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 196 | return returnStatus;                                                   
;----------------------------------------------------------------------
        MOV       AL,*-SP[8]            ; [CPU_] |196| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 197,column 1,is_stmt,isa 0
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$16, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$16, DW_AT_TI_end_line(0xc5)
	.dwattr $C$DW$16, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$16

	.sect	".text:_IpcGet"
	.clink
	.global	_IpcGet

$C$DW$27	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$27, DW_AT_name("IpcGet")
	.dwattr $C$DW$27, DW_AT_low_pc(_IpcGet)
	.dwattr $C$DW$27, DW_AT_high_pc(0x00)
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_IpcGet")
	.dwattr $C$DW$27, DW_AT_external
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$27, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$27, DW_AT_TI_begin_line(0xdc)
	.dwattr $C$DW$27, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$27, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 222,column 1,is_stmt,address _IpcGet,isa 0

	.dwfde $C$DW$CIE, _IpcGet
$C$DW$28	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$28, DW_AT_name("psController")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg12]

$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_name("psMessage")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg14]

$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_name("bBlock")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 220 | IpcGet (volatile tIpcController *psController, tIpcMessage *psMessage, 
; 221 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IpcGet                       FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_IpcGet:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$31	.dwtag  DW_TAG_variable
	.dwattr $C$DW$31, DW_AT_name("psController")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -2]

$C$DW$32	.dwtag  DW_TAG_variable
	.dwattr $C$DW$32, DW_AT_name("psMessage")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -4]

$C$DW$33	.dwtag  DW_TAG_variable
	.dwattr $C$DW$33, DW_AT_name("bBlock")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -5]

$C$DW$34	.dwtag  DW_TAG_variable
	.dwattr $C$DW$34, DW_AT_name("writeIndex")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_writeIndex")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -6]

$C$DW$35	.dwtag  DW_TAG_variable
	.dwattr $C$DW$35, DW_AT_name("readIndex")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_readIndex")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -7]

$C$DW$36	.dwtag  DW_TAG_variable
	.dwattr $C$DW$36, DW_AT_name("returnStatus")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_returnStatus")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -8]

;----------------------------------------------------------------------
; 223 | uint16_t writeIndex;                                                   
; 224 | uint16_t readIndex;                                                    
;----------------------------------------------------------------------
        MOV       *-SP[5],AL            ; [CPU_] |222| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |222| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |222| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 225,column 27,is_stmt,isa 0
;----------------------------------------------------------------------
; 225 | uint16_t returnStatus = STATUS_PASS;                                   
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |225| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 227,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 227 | writeIndex = *(psController->pusGetWriteIndex);                        
;----------------------------------------------------------------------
        MOVB      ACC,#10               ; [CPU_] |227| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |227| 
        MOVL      XAR4,ACC              ; [CPU_] |227| 
        MOVL      XAR7,*+XAR4[0]        ; [CPU_] |227| 
        MOV       AL,*+XAR7[0]          ; [CPU_] |227| 
        MOV       *-SP[6],AL            ; [CPU_] |227| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 228,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 228 | readIndex = *(psController->pusGetReadIndex);                          
; 230 | // Loop while GetBuffer is empty                                       
;----------------------------------------------------------------------
        MOVB      ACC,#12               ; [CPU_] |228| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |228| 
        MOVL      XAR4,ACC              ; [CPU_] |228| 
        MOVL      XAR7,*+XAR4[0]        ; [CPU_] |228| 
        MOV       AL,*+XAR7[0]          ; [CPU_] |228| 
        MOV       *-SP[7],AL            ; [CPU_] |228| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 231,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 231 | while (writeIndex == readIndex)                                        
; 233 |     // If designated as a "Blocking" function, and Get buffer is empty,
; 234 |     // return immediately with fail status.                            
;----------------------------------------------------------------------
        MOVZ      AR6,*-SP[6]           ; [CPU_] |231| 
        MOVU      ACC,*-SP[7]           ; [CPU_] |231| 
        CMPL      ACC,XAR6              ; [CPU_] |231| 
        B         $C$L8,NEQ             ; [CPU_] |231| 
        ; branchcc occurs ; [] |231| 
$C$L6:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 235,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 235 | if (!bBlock)                                                           
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |235| 
        B         $C$L7,NEQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 237,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 237 | returnStatus = STATUS_FAIL;                                            
;----------------------------------------------------------------------
        MOVB      *-SP[8],#1,UNC        ; [CPU_] |237| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 238,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 238 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L8,UNC             ; [CPU_] |238| 
        ; branch occurs ; [] |238| 
$C$L7:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 241,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 241 | writeIndex = *(psController->pusGetWriteIndex);                        
;----------------------------------------------------------------------
        MOVB      ACC,#10               ; [CPU_] |241| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |241| 
        MOVL      XAR4,ACC              ; [CPU_] |241| 
        MOVL      XAR7,*+XAR4[0]        ; [CPU_] |241| 
        MOV       AL,*+XAR7[0]          ; [CPU_] |241| 
        MOV       *-SP[6],AL            ; [CPU_] |241| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 231,column 12,is_stmt,isa 0
        MOVZ      AR6,*-SP[6]           ; [CPU_] |231| 
        MOVU      ACC,*-SP[7]           ; [CPU_] |231| 
        CMPL      ACC,XAR6              ; [CPU_] |231| 
        B         $C$L6,EQ              ; [CPU_] |231| 
        ; branchcc occurs ; [] |231| 
$C$L8:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 244,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 244 | if (returnStatus != STATUS_FAIL)                                       
; 246 |     // If there is a message in GetBuffer, Read Message and update     
; 247 |     // the ReadIndex                                                   
;----------------------------------------------------------------------
        MOV       AL,*-SP[8]            ; [CPU_] |244| 
        CMPB      AL,#1                 ; [CPU_] |244| 
        B         $C$L9,EQ              ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 248,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 248 | *psMessage = psController->psGetBuffer[readIndex];                     
;----------------------------------------------------------------------
        MOVB      ACC,#8                ; [CPU_] |248| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |248| 
        MOVL      XAR5,ACC              ; [CPU_] |248| 
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |248| 
        MOV       ACC,*-SP[7] << 3      ; [CPU_] |248| 
        ADDL      ACC,*+XAR5[0]         ; [CPU_] |248| 
        MOVL      XAR7,ACC              ; [CPU_] |248| 
        RPT       #7
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |248| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 250,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 250 | readIndex = (readIndex + 1) & MAX_BUFFER_INDEX;                        
;----------------------------------------------------------------------
        MOV       AL,*-SP[7]            ; [CPU_] |250| 
        ADDB      AL,#1                 ; [CPU_] |250| 
        ANDB      AL,#0x03              ; [CPU_] |250| 
        MOV       *-SP[7],AL            ; [CPU_] |250| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 251,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 251 | *(psController->pusGetReadIndex) = readIndex;                          
;----------------------------------------------------------------------
        MOVB      ACC,#12               ; [CPU_] |251| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |251| 
        MOVL      XAR4,ACC              ; [CPU_] |251| 
        MOVL      XAR7,*+XAR4[0]        ; [CPU_] |251| 
        MOV       AL,*-SP[7]            ; [CPU_] |251| 
        MOV       *+XAR7[0],AL          ; [CPU_] |251| 
$C$L9:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 254,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 254 | return returnStatus;                                                   
;----------------------------------------------------------------------
        MOV       AL,*-SP[8]            ; [CPU_] |254| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 255,column 1,is_stmt,isa 0
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$27, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$27, DW_AT_TI_end_line(0xff)
	.dwattr $C$DW$27, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$27

	.sect	".text:_IPCLtoRDataRead"
	.clink
	.global	_IPCLtoRDataRead

$C$DW$38	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$38, DW_AT_name("IPCLtoRDataRead")
	.dwattr $C$DW$38, DW_AT_low_pc(_IPCLtoRDataRead)
	.dwattr $C$DW$38, DW_AT_high_pc(0x00)
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_IPCLtoRDataRead")
	.dwattr $C$DW$38, DW_AT_external
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$38, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$38, DW_AT_TI_begin_line(0x123)
	.dwattr $C$DW$38, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$38, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 294,column 1,is_stmt,address _IPCLtoRDataRead,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRDataRead
$C$DW$39	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$39, DW_AT_name("psController")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg12]

$C$DW$40	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$40, DW_AT_name("ulAddress")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg0]

$C$DW$41	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$41, DW_AT_name("pvData")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_pvData")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg14]

$C$DW$42	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$42, DW_AT_name("usLength")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -19]

$C$DW$43	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$43, DW_AT_name("bBlock")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_breg20 -20]

$C$DW$44	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$44, DW_AT_name("ulResponseFlag")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_ulResponseFlag")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_breg20 -22]

;----------------------------------------------------------------------
; 291 | IPCLtoRDataRead (volatile tIpcController *psController, uint32_t ulAddr
;     | ess,                                                                   
; 292 | void *pvData, uint16_t usLength, uint16_t bBlock,                      
; 293 | uint32_t ulResponseFlag)                                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRDataRead              FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 15 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRDataRead:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$45	.dwtag  DW_TAG_variable
	.dwattr $C$DW$45, DW_AT_name("sMessage")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_breg20 -8]

$C$DW$46	.dwtag  DW_TAG_variable
	.dwattr $C$DW$46, DW_AT_name("psController")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -10]

$C$DW$47	.dwtag  DW_TAG_variable
	.dwattr $C$DW$47, DW_AT_name("ulAddress")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -12]

$C$DW$48	.dwtag  DW_TAG_variable
	.dwattr $C$DW$48, DW_AT_name("pvData")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_pvData")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -14]

$C$DW$49	.dwtag  DW_TAG_variable
	.dwattr $C$DW$49, DW_AT_name("status")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg20 -15]

;----------------------------------------------------------------------
; 296 | uint16_t status;                                                       
; 297 | tIpcMessage sMessage;                                                  
; 299 | // Set up read command, address, dataw1 = ResponseFlag | word length, d
;     | ataw2                                                                  
; 300 | // = address where word                                                
; 301 | // should be written to when returned.                                 
;----------------------------------------------------------------------
        MOVL      *-SP[14],XAR5         ; [CPU_] |294| 
        MOVL      *-SP[12],ACC          ; [CPU_] |294| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |294| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 302,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 302 | sMessage.ulcommand = IPC_DATA_READ;                                    
;----------------------------------------------------------------------
        MOVB      ACC,#8                ; [CPU_] |302| 
        MOVL      *-SP[8],ACC           ; [CPU_] |302| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 303,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 303 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[12]          ; [CPU_] |303| 
        MOVL      *-SP[6],ACC           ; [CPU_] |303| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 304,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 304 | sMessage.uldataw1 = (ulResponseFlag & 0xFFFF0000)|(uint32_t)usLength;  
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[22]          ; [CPU_] |304| 
        ANDB      AL,#0                 ; [CPU_] |304| 
        OR        ACC,*-SP[19]          ; [CPU_] |304| 
        MOVL      *-SP[4],ACC           ; [CPU_] |304| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 305,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 305 | sMessage.uldataw2 = (uint32_t)pvData;                                  
; 307 | // Set ResponseFlag (cleared once data is read into address at pvData) 
; 308 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[14]          ; [CPU_] |305| 
        MOVL      *-SP[2],ACC           ; [CPU_] |305| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 309,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 309 | IpcRegs.IPCSET.all |= (ulResponseFlag & 0xFFFF0000);                   
;----------------------------------------------------------------------
        MOVW      DP,#_IpcRegs+4        ; [CPU_U] 
        MOVL      ACC,*-SP[22]          ; [CPU_] |309| 
        ANDB      AL,#0                 ; [CPU_] |309| 
        OR        @_IpcRegs+4,AL        ; [CPU_] |309| 
        OR        @_IpcRegs+5,AH        ; [CPU_] |309| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 310,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 310 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |310| 
        MOV       AL,*-SP[20]           ; [CPU_] |310| 
        SUBB      XAR4,#8               ; [CPU_U] |310| 
        MOVZ      AR5,AR4               ; [CPU_] |310| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |310| 
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_name("_IpcPut")
	.dwattr $C$DW$50, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |310| 
        ; call occurs [#_IpcPut] ; [] |310| 
        MOV       *-SP[15],AL           ; [CPU_] |310| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 312,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 312 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 321,column 1,is_stmt,isa 0
        SUBB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$51	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$51, DW_AT_low_pc(0x00)
	.dwattr $C$DW$51, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$38, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$38, DW_AT_TI_end_line(0x141)
	.dwattr $C$DW$38, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$38

	.sect	".text:_IPCLtoRDataRead_Protected"
	.clink
	.global	_IPCLtoRDataRead_Protected

$C$DW$52	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$52, DW_AT_name("IPCLtoRDataRead_Protected")
	.dwattr $C$DW$52, DW_AT_low_pc(_IPCLtoRDataRead_Protected)
	.dwattr $C$DW$52, DW_AT_high_pc(0x00)
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_IPCLtoRDataRead_Protected")
	.dwattr $C$DW$52, DW_AT_external
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$52, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$52, DW_AT_TI_begin_line(0x165)
	.dwattr $C$DW$52, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$52, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 361,column 1,is_stmt,address _IPCLtoRDataRead_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRDataRead_Protected
$C$DW$53	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$53, DW_AT_name("psController")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg12]

$C$DW$54	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$54, DW_AT_name("ulAddress")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg0]

$C$DW$55	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$55, DW_AT_name("pvData")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_pvData")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg14]

$C$DW$56	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$56, DW_AT_name("usLength")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_breg20 -19]

$C$DW$57	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$57, DW_AT_name("bBlock")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_breg20 -20]

$C$DW$58	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$58, DW_AT_name("ulResponseFlag")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_ulResponseFlag")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_breg20 -22]

;----------------------------------------------------------------------
; 357 | IPCLtoRDataRead_Protected (volatile tIpcController *psController,      
; 358 | uint32_t ulAddress, void *pvData, uint16_t usLength,                   
; 359 | uint16_t bBlock,                                                       
; 360 | uint32_t ulResponseFlag)                                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRDataRead_Protected    FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 15 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRDataRead_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$59	.dwtag  DW_TAG_variable
	.dwattr $C$DW$59, DW_AT_name("sMessage")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_breg20 -8]

$C$DW$60	.dwtag  DW_TAG_variable
	.dwattr $C$DW$60, DW_AT_name("psController")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_breg20 -10]

$C$DW$61	.dwtag  DW_TAG_variable
	.dwattr $C$DW$61, DW_AT_name("ulAddress")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -12]

$C$DW$62	.dwtag  DW_TAG_variable
	.dwattr $C$DW$62, DW_AT_name("pvData")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_pvData")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_breg20 -14]

$C$DW$63	.dwtag  DW_TAG_variable
	.dwattr $C$DW$63, DW_AT_name("status")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -15]

;----------------------------------------------------------------------
; 363 | uint16_t status;                                                       
; 364 | tIpcMessage sMessage;                                                  
; 366 | // Set up read command, address, dataw1 = ResponseFlag | word length, d
;     | ataw2                                                                  
; 367 | // = address where word                                                
; 368 | // should be written to when returned.                                 
;----------------------------------------------------------------------
        MOVL      *-SP[14],XAR5         ; [CPU_] |361| 
        MOVL      *-SP[12],ACC          ; [CPU_] |361| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |361| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 369,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 369 | sMessage.ulcommand = IPC_DATA_READ_PROTECTED;                          
;----------------------------------------------------------------------
        MOVL      XAR4,#65543           ; [CPU_U] |369| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |369| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 370,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 370 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |370| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 371,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 371 | sMessage.uldataw1 = (ulResponseFlag & 0xFFFF0000)|(uint32_t)usLength;  
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[22]          ; [CPU_] |371| 
        ANDB      AL,#0                 ; [CPU_] |371| 
        OR        ACC,*-SP[19]          ; [CPU_] |371| 
        MOVL      *-SP[4],ACC           ; [CPU_] |371| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 372,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 372 | sMessage.uldataw2 = (uint32_t)pvData;                                  
; 374 | // Set ResponseFlag (cleared once data is read into address at pvData) 
; 375 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[14]          ; [CPU_] |372| 
        MOVL      *-SP[2],ACC           ; [CPU_] |372| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 376,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 376 | IpcRegs.IPCSET.all |= (ulResponseFlag & 0xFFFF0000);                   
;----------------------------------------------------------------------
        MOVW      DP,#_IpcRegs+4        ; [CPU_U] 
        MOVL      ACC,*-SP[22]          ; [CPU_] |376| 
        ANDB      AL,#0                 ; [CPU_] |376| 
        OR        @_IpcRegs+4,AL        ; [CPU_] |376| 
        OR        @_IpcRegs+5,AH        ; [CPU_] |376| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 377,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 377 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |377| 
        MOV       AL,*-SP[20]           ; [CPU_] |377| 
        SUBB      XAR4,#8               ; [CPU_U] |377| 
        MOVZ      AR5,AR4               ; [CPU_] |377| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |377| 
$C$DW$64	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$64, DW_AT_low_pc(0x00)
	.dwattr $C$DW$64, DW_AT_name("_IpcPut")
	.dwattr $C$DW$64, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |377| 
        ; call occurs [#_IpcPut] ; [] |377| 
        MOV       *-SP[15],AL           ; [CPU_] |377| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 379,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 379 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 388,column 1,is_stmt,isa 0
        SUBB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$65	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$65, DW_AT_low_pc(0x00)
	.dwattr $C$DW$65, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$52, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$52, DW_AT_TI_end_line(0x184)
	.dwattr $C$DW$52, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$52

	.sect	".text:_IPCLtoRSetBits"
	.clink
	.global	_IPCLtoRSetBits

$C$DW$66	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$66, DW_AT_name("IPCLtoRSetBits")
	.dwattr $C$DW$66, DW_AT_low_pc(_IPCLtoRSetBits)
	.dwattr $C$DW$66, DW_AT_high_pc(0x00)
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_IPCLtoRSetBits")
	.dwattr $C$DW$66, DW_AT_external
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$66, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$66, DW_AT_TI_begin_line(0x19f)
	.dwattr $C$DW$66, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$66, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 418,column 1,is_stmt,address _IPCLtoRSetBits,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRSetBits
$C$DW$67	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$67, DW_AT_name("psController")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg12]

$C$DW$68	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$68, DW_AT_name("ulAddress")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg0]

$C$DW$69	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$69, DW_AT_name("ulMask")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ulMask")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_breg20 -18]

$C$DW$70	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$70, DW_AT_name("usLength")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_reg14]

$C$DW$71	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$71, DW_AT_name("bBlock")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_breg20 -19]

;----------------------------------------------------------------------
; 415 | IPCLtoRSetBits(volatile tIpcController *psController, uint32_t ulAddres
;     | s,                                                                     
; 416 | uint32_t ulMask, uint16_t usLength,                                    
; 417 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRSetBits               FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRSetBits:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$72	.dwtag  DW_TAG_variable
	.dwattr $C$DW$72, DW_AT_name("sMessage")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_breg20 -8]

$C$DW$73	.dwtag  DW_TAG_variable
	.dwattr $C$DW$73, DW_AT_name("psController")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_breg20 -10]

$C$DW$74	.dwtag  DW_TAG_variable
	.dwattr $C$DW$74, DW_AT_name("ulAddress")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -12]

$C$DW$75	.dwtag  DW_TAG_variable
	.dwattr $C$DW$75, DW_AT_name("usLength")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_breg20 -13]

$C$DW$76	.dwtag  DW_TAG_variable
	.dwattr $C$DW$76, DW_AT_name("status")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 419 | uint16_t status;                                                       
; 420 | tIpcMessage sMessage;                                                  
; 422 | // Set up set bits command, address, dataw1 = word length, dataw2 =    
; 423 | // 16/32-bit mask                                                      
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |418| 
        MOVL      *-SP[12],ACC          ; [CPU_] |418| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |418| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 424,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 424 | sMessage.ulcommand = IPC_SET_BITS;                                     
;----------------------------------------------------------------------
        MOVL      XAR4,#65537           ; [CPU_U] |424| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |424| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 425,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 425 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |425| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 426,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 426 | sMessage.uldataw1 = (uint32_t)usLength;                                
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[13]          ; [CPU_] |426| 
        MOVL      *-SP[4],ACC           ; [CPU_] |426| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 427,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 427 | sMessage.uldataw2 = ulMask;                                            
; 429 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |427| 
        MOVL      *-SP[2],ACC           ; [CPU_] |427| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 430,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 430 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |430| 
        MOV       AL,*-SP[19]           ; [CPU_] |430| 
        SUBB      XAR4,#8               ; [CPU_U] |430| 
        MOVZ      AR5,AR4               ; [CPU_] |430| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |430| 
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("_IpcPut")
	.dwattr $C$DW$77, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |430| 
        ; call occurs [#_IpcPut] ; [] |430| 
        MOV       *-SP[14],AL           ; [CPU_] |430| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 431,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 431 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 432,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$66, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$66, DW_AT_TI_end_line(0x1b0)
	.dwattr $C$DW$66, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$66

	.sect	".text:_IPCLtoRSetBits_Protected"
	.clink
	.global	_IPCLtoRSetBits_Protected

$C$DW$79	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$79, DW_AT_name("IPCLtoRSetBits_Protected")
	.dwattr $C$DW$79, DW_AT_low_pc(_IPCLtoRSetBits_Protected)
	.dwattr $C$DW$79, DW_AT_high_pc(0x00)
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_IPCLtoRSetBits_Protected")
	.dwattr $C$DW$79, DW_AT_external
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$79, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$79, DW_AT_TI_begin_line(0x1cb)
	.dwattr $C$DW$79, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$79, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 462,column 1,is_stmt,address _IPCLtoRSetBits_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRSetBits_Protected
$C$DW$80	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$80, DW_AT_name("psController")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg12]

$C$DW$81	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$81, DW_AT_name("ulAddress")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg0]

$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_name("ulMask")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_ulMask")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_breg20 -18]

$C$DW$83	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$83, DW_AT_name("usLength")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg14]

$C$DW$84	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$84, DW_AT_name("bBlock")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_breg20 -19]

;----------------------------------------------------------------------
; 459 | IPCLtoRSetBits_Protected(volatile tIpcController *psController,        
; 460 | uint32_t ulAddress, uint32_t ulMask, uint16_t usLength,                
; 461 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRSetBits_Protected     FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRSetBits_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$85	.dwtag  DW_TAG_variable
	.dwattr $C$DW$85, DW_AT_name("sMessage")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -8]

$C$DW$86	.dwtag  DW_TAG_variable
	.dwattr $C$DW$86, DW_AT_name("psController")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -10]

$C$DW$87	.dwtag  DW_TAG_variable
	.dwattr $C$DW$87, DW_AT_name("ulAddress")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -12]

$C$DW$88	.dwtag  DW_TAG_variable
	.dwattr $C$DW$88, DW_AT_name("usLength")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_breg20 -13]

$C$DW$89	.dwtag  DW_TAG_variable
	.dwattr $C$DW$89, DW_AT_name("status")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 463 | uint16_t status;                                                       
; 464 | tIpcMessage sMessage;                                                  
; 466 | // Set up set bits command, address, dataw1 = word length, dataw2 =    
; 467 | // 16/32-bit mask                                                      
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |462| 
        MOVL      *-SP[12],ACC          ; [CPU_] |462| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |462| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 468,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 468 | sMessage.ulcommand = IPC_SET_BITS_PROTECTED;                           
;----------------------------------------------------------------------
        MOVL      XAR4,#65544           ; [CPU_U] |468| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |468| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 469,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 469 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |469| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 470,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 470 | sMessage.uldataw1 = (uint32_t)usLength;                                
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[13]          ; [CPU_] |470| 
        MOVL      *-SP[4],ACC           ; [CPU_] |470| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 471,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 471 | sMessage.uldataw2 = ulMask;                                            
; 473 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |471| 
        MOVL      *-SP[2],ACC           ; [CPU_] |471| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 474,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 474 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |474| 
        MOV       AL,*-SP[19]           ; [CPU_] |474| 
        SUBB      XAR4,#8               ; [CPU_U] |474| 
        MOVZ      AR5,AR4               ; [CPU_] |474| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |474| 
$C$DW$90	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$90, DW_AT_low_pc(0x00)
	.dwattr $C$DW$90, DW_AT_name("_IpcPut")
	.dwattr $C$DW$90, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |474| 
        ; call occurs [#_IpcPut] ; [] |474| 
        MOV       *-SP[14],AL           ; [CPU_] |474| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 475,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 475 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 476,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$91	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$91, DW_AT_low_pc(0x00)
	.dwattr $C$DW$91, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$79, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$79, DW_AT_TI_end_line(0x1dc)
	.dwattr $C$DW$79, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$79

	.sect	".text:_IPCLtoRClearBits"
	.clink
	.global	_IPCLtoRClearBits

$C$DW$92	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$92, DW_AT_name("IPCLtoRClearBits")
	.dwattr $C$DW$92, DW_AT_low_pc(_IPCLtoRClearBits)
	.dwattr $C$DW$92, DW_AT_high_pc(0x00)
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_IPCLtoRClearBits")
	.dwattr $C$DW$92, DW_AT_external
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$92, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$92, DW_AT_TI_begin_line(0x1f7)
	.dwattr $C$DW$92, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$92, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 506,column 1,is_stmt,address _IPCLtoRClearBits,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRClearBits
$C$DW$93	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$93, DW_AT_name("psController")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_reg12]

$C$DW$94	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$94, DW_AT_name("ulAddress")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$94, DW_AT_location[DW_OP_reg0]

$C$DW$95	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$95, DW_AT_name("ulMask")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_ulMask")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$95, DW_AT_location[DW_OP_breg20 -18]

$C$DW$96	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$96, DW_AT_name("usLength")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg14]

$C$DW$97	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$97, DW_AT_name("bBlock")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_breg20 -19]

;----------------------------------------------------------------------
; 503 | IPCLtoRClearBits(volatile tIpcController *psController, uint32_t ulAddr
;     | ess,                                                                   
; 504 | uint32_t ulMask, uint16_t usLength,                                    
; 505 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRClearBits             FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRClearBits:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$98	.dwtag  DW_TAG_variable
	.dwattr $C$DW$98, DW_AT_name("sMessage")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_breg20 -8]

$C$DW$99	.dwtag  DW_TAG_variable
	.dwattr $C$DW$99, DW_AT_name("psController")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_breg20 -10]

$C$DW$100	.dwtag  DW_TAG_variable
	.dwattr $C$DW$100, DW_AT_name("ulAddress")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_breg20 -12]

$C$DW$101	.dwtag  DW_TAG_variable
	.dwattr $C$DW$101, DW_AT_name("usLength")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_breg20 -13]

$C$DW$102	.dwtag  DW_TAG_variable
	.dwattr $C$DW$102, DW_AT_name("status")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 507 | uint16_t status;                                                       
; 508 | tIpcMessage sMessage;                                                  
; 510 | // Set up clear bits command, address, dataw1 = word length, dataw2 =  
; 511 | // 16/32-bit mask                                                      
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |506| 
        MOVL      *-SP[12],ACC          ; [CPU_] |506| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |506| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 512,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 512 | sMessage.ulcommand = IPC_CLEAR_BITS;                                   
;----------------------------------------------------------------------
        MOVL      XAR4,#65538           ; [CPU_U] |512| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |512| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 513,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 513 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |513| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 514,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 514 | sMessage.uldataw1 = (uint32_t)usLength;                                
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[13]          ; [CPU_] |514| 
        MOVL      *-SP[4],ACC           ; [CPU_] |514| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 515,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 515 | sMessage.uldataw2 = ulMask;                                            
; 517 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |515| 
        MOVL      *-SP[2],ACC           ; [CPU_] |515| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 518,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 518 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |518| 
        MOV       AL,*-SP[19]           ; [CPU_] |518| 
        SUBB      XAR4,#8               ; [CPU_U] |518| 
        MOVZ      AR5,AR4               ; [CPU_] |518| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |518| 
$C$DW$103	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$103, DW_AT_low_pc(0x00)
	.dwattr $C$DW$103, DW_AT_name("_IpcPut")
	.dwattr $C$DW$103, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |518| 
        ; call occurs [#_IpcPut] ; [] |518| 
        MOV       *-SP[14],AL           ; [CPU_] |518| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 519,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 519 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 520,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$104	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$104, DW_AT_low_pc(0x00)
	.dwattr $C$DW$104, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$92, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$92, DW_AT_TI_end_line(0x208)
	.dwattr $C$DW$92, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$92

	.sect	".text:_IPCLtoRClearBits_Protected"
	.clink
	.global	_IPCLtoRClearBits_Protected

$C$DW$105	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$105, DW_AT_name("IPCLtoRClearBits_Protected")
	.dwattr $C$DW$105, DW_AT_low_pc(_IPCLtoRClearBits_Protected)
	.dwattr $C$DW$105, DW_AT_high_pc(0x00)
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_IPCLtoRClearBits_Protected")
	.dwattr $C$DW$105, DW_AT_external
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$105, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$105, DW_AT_TI_begin_line(0x223)
	.dwattr $C$DW$105, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$105, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 550,column 1,is_stmt,address _IPCLtoRClearBits_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRClearBits_Protected
$C$DW$106	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$106, DW_AT_name("psController")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_reg12]

$C$DW$107	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$107, DW_AT_name("ulAddress")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg0]

$C$DW$108	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$108, DW_AT_name("ulMask")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_ulMask")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_breg20 -18]

$C$DW$109	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$109, DW_AT_name("usLength")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg14]

$C$DW$110	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$110, DW_AT_name("bBlock")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_breg20 -19]

;----------------------------------------------------------------------
; 547 | IPCLtoRClearBits_Protected(volatile tIpcController *psController,      
; 548 | uint32_t ulAddress, uint32_t ulMask, uint16_t usLength,                
; 549 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRClearBits_Protected   FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRClearBits_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$111	.dwtag  DW_TAG_variable
	.dwattr $C$DW$111, DW_AT_name("sMessage")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_breg20 -8]

$C$DW$112	.dwtag  DW_TAG_variable
	.dwattr $C$DW$112, DW_AT_name("psController")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_breg20 -10]

$C$DW$113	.dwtag  DW_TAG_variable
	.dwattr $C$DW$113, DW_AT_name("ulAddress")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$113, DW_AT_location[DW_OP_breg20 -12]

$C$DW$114	.dwtag  DW_TAG_variable
	.dwattr $C$DW$114, DW_AT_name("usLength")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_breg20 -13]

$C$DW$115	.dwtag  DW_TAG_variable
	.dwattr $C$DW$115, DW_AT_name("status")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 551 | uint16_t status;                                                       
; 552 | tIpcMessage sMessage;                                                  
; 554 | // Set up clear bits command, address, dataw1 = word length, dataw2 =  
; 555 | // 16/32-bit mask                                                      
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |550| 
        MOVL      *-SP[12],ACC          ; [CPU_] |550| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |550| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 556,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 556 | sMessage.ulcommand = IPC_CLEAR_BITS_PROTECTED;                         
;----------------------------------------------------------------------
        MOVL      XAR4,#65545           ; [CPU_U] |556| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |556| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 557,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 557 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |557| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 558,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 558 | sMessage.uldataw1 = (uint32_t)usLength;                                
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[13]          ; [CPU_] |558| 
        MOVL      *-SP[4],ACC           ; [CPU_] |558| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 559,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 559 | sMessage.uldataw2 = ulMask;                                            
; 561 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |559| 
        MOVL      *-SP[2],ACC           ; [CPU_] |559| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 562,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 562 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |562| 
        MOV       AL,*-SP[19]           ; [CPU_] |562| 
        SUBB      XAR4,#8               ; [CPU_U] |562| 
        MOVZ      AR5,AR4               ; [CPU_] |562| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |562| 
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_name("_IpcPut")
	.dwattr $C$DW$116, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |562| 
        ; call occurs [#_IpcPut] ; [] |562| 
        MOV       *-SP[14],AL           ; [CPU_] |562| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 563,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 563 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 564,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$105, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$105, DW_AT_TI_end_line(0x234)
	.dwattr $C$DW$105, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$105

	.sect	".text:_IPCLtoRDataWrite"
	.clink
	.global	_IPCLtoRDataWrite

$C$DW$118	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$118, DW_AT_name("IPCLtoRDataWrite")
	.dwattr $C$DW$118, DW_AT_low_pc(_IPCLtoRDataWrite)
	.dwattr $C$DW$118, DW_AT_high_pc(0x00)
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_IPCLtoRDataWrite")
	.dwattr $C$DW$118, DW_AT_external
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$118, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$118, DW_AT_TI_begin_line(0x255)
	.dwattr $C$DW$118, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$118, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 600,column 1,is_stmt,address _IPCLtoRDataWrite,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRDataWrite
$C$DW$119	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$119, DW_AT_name("psController")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg12]

$C$DW$120	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$120, DW_AT_name("ulAddress")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg0]

$C$DW$121	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$121, DW_AT_name("ulData")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_ulData")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$121, DW_AT_location[DW_OP_breg20 -18]

$C$DW$122	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$122, DW_AT_name("usLength")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg14]

$C$DW$123	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$123, DW_AT_name("bBlock")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$123, DW_AT_location[DW_OP_breg20 -19]

$C$DW$124	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$124, DW_AT_name("ulResponseFlag")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_ulResponseFlag")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$124, DW_AT_location[DW_OP_breg20 -22]

;----------------------------------------------------------------------
; 597 | IPCLtoRDataWrite(volatile tIpcController *psController, uint32_t ulAddr
;     | ess,                                                                   
; 598 | uint32_t ulData, uint16_t usLength, uint16_t bBlock,                   
; 599 | uint32_t ulResponseFlag)                                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRDataWrite             FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRDataWrite:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$125	.dwtag  DW_TAG_variable
	.dwattr $C$DW$125, DW_AT_name("sMessage")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$125, DW_AT_location[DW_OP_breg20 -8]

$C$DW$126	.dwtag  DW_TAG_variable
	.dwattr $C$DW$126, DW_AT_name("psController")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_breg20 -10]

$C$DW$127	.dwtag  DW_TAG_variable
	.dwattr $C$DW$127, DW_AT_name("ulAddress")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_breg20 -12]

$C$DW$128	.dwtag  DW_TAG_variable
	.dwattr $C$DW$128, DW_AT_name("usLength")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_breg20 -13]

$C$DW$129	.dwtag  DW_TAG_variable
	.dwattr $C$DW$129, DW_AT_name("status")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 601 | uint16_t status;                                                       
; 602 | tIpcMessage sMessage;                                                  
; 604 | // Set up write command, address, dataw1 = ResponseFlag | word length, 
; 605 | // dataw2 = data to write                                              
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |600| 
        MOVL      *-SP[12],ACC          ; [CPU_] |600| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |600| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 606,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 606 | sMessage.ulcommand = IPC_DATA_WRITE;                                   
;----------------------------------------------------------------------
        MOVL      XAR4,#65539           ; [CPU_U] |606| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |606| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 607,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 607 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |607| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 608,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 608 | sMessage.uldataw1 = ulResponseFlag |(uint32_t)usLength;                
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[22]          ; [CPU_] |608| 
        OR        ACC,*-SP[13]          ; [CPU_] |608| 
        MOVL      *-SP[4],ACC           ; [CPU_] |608| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 609,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 609 | sMessage.uldataw2 = ulData;                                            
; 611 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |609| 
        MOVL      *-SP[2],ACC           ; [CPU_] |609| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 612,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 612 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |612| 
        MOV       AL,*-SP[19]           ; [CPU_] |612| 
        SUBB      XAR4,#8               ; [CPU_U] |612| 
        MOVZ      AR5,AR4               ; [CPU_] |612| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |612| 
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_name("_IpcPut")
	.dwattr $C$DW$130, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |612| 
        ; call occurs [#_IpcPut] ; [] |612| 
        MOV       *-SP[14],AL           ; [CPU_] |612| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 613,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 613 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 614,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$131	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$131, DW_AT_low_pc(0x00)
	.dwattr $C$DW$131, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$118, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$118, DW_AT_TI_end_line(0x266)
	.dwattr $C$DW$118, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$118

	.sect	".text:_IPCLtoRDataWrite_Protected"
	.clink
	.global	_IPCLtoRDataWrite_Protected

$C$DW$132	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$132, DW_AT_name("IPCLtoRDataWrite_Protected")
	.dwattr $C$DW$132, DW_AT_low_pc(_IPCLtoRDataWrite_Protected)
	.dwattr $C$DW$132, DW_AT_high_pc(0x00)
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_IPCLtoRDataWrite_Protected")
	.dwattr $C$DW$132, DW_AT_external
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$132, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$132, DW_AT_TI_begin_line(0x288)
	.dwattr $C$DW$132, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$132, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 652,column 1,is_stmt,address _IPCLtoRDataWrite_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRDataWrite_Protected
$C$DW$133	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$133, DW_AT_name("psController")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_reg12]

$C$DW$134	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$134, DW_AT_name("ulAddress")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg0]

$C$DW$135	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$135, DW_AT_name("ulData")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_ulData")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_breg20 -18]

$C$DW$136	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$136, DW_AT_name("usLength")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_reg14]

$C$DW$137	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$137, DW_AT_name("bBlock")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_breg20 -19]

$C$DW$138	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$138, DW_AT_name("ulResponseFlag")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_ulResponseFlag")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$138, DW_AT_location[DW_OP_breg20 -22]

;----------------------------------------------------------------------
; 648 | IPCLtoRDataWrite_Protected(volatile tIpcController *psController,      
; 649 | uint32_t ulAddress, uint32_t ulData, uint16_t usLength,                
; 650 | uint16_t bBlock,                                                       
; 651 | uint32_t ulResponseFlag)                                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRDataWrite_Protected   FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRDataWrite_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$139	.dwtag  DW_TAG_variable
	.dwattr $C$DW$139, DW_AT_name("sMessage")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$139, DW_AT_location[DW_OP_breg20 -8]

$C$DW$140	.dwtag  DW_TAG_variable
	.dwattr $C$DW$140, DW_AT_name("psController")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$140, DW_AT_location[DW_OP_breg20 -10]

$C$DW$141	.dwtag  DW_TAG_variable
	.dwattr $C$DW$141, DW_AT_name("ulAddress")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$141, DW_AT_location[DW_OP_breg20 -12]

$C$DW$142	.dwtag  DW_TAG_variable
	.dwattr $C$DW$142, DW_AT_name("usLength")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$142, DW_AT_location[DW_OP_breg20 -13]

$C$DW$143	.dwtag  DW_TAG_variable
	.dwattr $C$DW$143, DW_AT_name("status")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 653 | uint16_t status;                                                       
; 654 | tIpcMessage sMessage;                                                  
; 656 | // Set up write command, address, dataw1 = ResponseFlag | word length, 
; 657 | // dataw2 = data to write                                              
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |652| 
        MOVL      *-SP[12],ACC          ; [CPU_] |652| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |652| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 658,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 658 | sMessage.ulcommand = IPC_DATA_WRITE_PROTECTED;                         
;----------------------------------------------------------------------
        MOVL      XAR4,#65546           ; [CPU_U] |658| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |658| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 659,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 659 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |659| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 660,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 660 | sMessage.uldataw1 = ulResponseFlag |(uint32_t)usLength;                
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[22]          ; [CPU_] |660| 
        OR        ACC,*-SP[13]          ; [CPU_] |660| 
        MOVL      *-SP[4],ACC           ; [CPU_] |660| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 661,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 661 | sMessage.uldataw2 = ulData;                                            
; 663 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |661| 
        MOVL      *-SP[2],ACC           ; [CPU_] |661| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 665,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 665 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |665| 
        MOV       AL,*-SP[19]           ; [CPU_] |665| 
        SUBB      XAR4,#8               ; [CPU_U] |665| 
        MOVZ      AR5,AR4               ; [CPU_] |665| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |665| 
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_name("_IpcPut")
	.dwattr $C$DW$144, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |665| 
        ; call occurs [#_IpcPut] ; [] |665| 
        MOV       *-SP[14],AL           ; [CPU_] |665| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 666,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 666 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 667,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$145	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$145, DW_AT_low_pc(0x00)
	.dwattr $C$DW$145, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$132, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$132, DW_AT_TI_end_line(0x29b)
	.dwattr $C$DW$132, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$132

	.sect	".text:_IPCLtoRBlockRead"
	.clink
	.global	_IPCLtoRBlockRead

$C$DW$146	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$146, DW_AT_name("IPCLtoRBlockRead")
	.dwattr $C$DW$146, DW_AT_low_pc(_IPCLtoRBlockRead)
	.dwattr $C$DW$146, DW_AT_high_pc(0x00)
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_IPCLtoRBlockRead")
	.dwattr $C$DW$146, DW_AT_external
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$146, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$146, DW_AT_TI_begin_line(0x2bb)
	.dwattr $C$DW$146, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$146, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 702,column 1,is_stmt,address _IPCLtoRBlockRead,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRBlockRead
$C$DW$147	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$147, DW_AT_name("psController")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg12]

$C$DW$148	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$148, DW_AT_name("ulAddress")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg0]

$C$DW$149	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$149, DW_AT_name("ulShareAddress")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_ulShareAddress")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$149, DW_AT_location[DW_OP_breg20 -18]

$C$DW$150	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$150, DW_AT_name("usLength")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$150, DW_AT_location[DW_OP_reg14]

$C$DW$151	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$151, DW_AT_name("bBlock")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$151, DW_AT_location[DW_OP_breg20 -19]

$C$DW$152	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$152, DW_AT_name("ulResponseFlag")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_ulResponseFlag")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$152, DW_AT_location[DW_OP_breg20 -22]

;----------------------------------------------------------------------
; 699 | IPCLtoRBlockRead(volatile tIpcController *psController, uint32_t ulAddr
;     | ess,                                                                   
; 700 | uint32_t ulShareAddress, uint16_t usLength, uint16_t bBlock,           
; 701 | uint32_t ulResponseFlag)                                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRBlockRead             FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRBlockRead:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$153	.dwtag  DW_TAG_variable
	.dwattr $C$DW$153, DW_AT_name("sMessage")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$153, DW_AT_location[DW_OP_breg20 -8]

$C$DW$154	.dwtag  DW_TAG_variable
	.dwattr $C$DW$154, DW_AT_name("psController")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$154, DW_AT_location[DW_OP_breg20 -10]

$C$DW$155	.dwtag  DW_TAG_variable
	.dwattr $C$DW$155, DW_AT_name("ulAddress")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$155, DW_AT_location[DW_OP_breg20 -12]

$C$DW$156	.dwtag  DW_TAG_variable
	.dwattr $C$DW$156, DW_AT_name("usLength")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$156, DW_AT_location[DW_OP_breg20 -13]

$C$DW$157	.dwtag  DW_TAG_variable
	.dwattr $C$DW$157, DW_AT_name("status")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$157, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 703 | uint16_t status;                                                       
; 704 | tIpcMessage sMessage;                                                  
; 706 | // Set up block read command, address, dataw1 = ResponseFlag | block le
;     | ngth,                                                                  
; 707 | // dataw2 = remote CPU address in shared memory                        
; 708 | // where block data should be read to (corresponding to local CPU ulSha
;     | reAddress).                                                            
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |702| 
        MOVL      *-SP[12],ACC          ; [CPU_] |702| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |702| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 709,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 709 | sMessage.ulcommand = IPC_BLOCK_READ;                                   
;----------------------------------------------------------------------
        MOVL      XAR4,#65540           ; [CPU_U] |709| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |709| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 710,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 710 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |710| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 711,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 711 | sMessage.uldataw1 = (ulResponseFlag & 0xFFFF0000) |(uint32_t)usLength; 
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[22]          ; [CPU_] |711| 
        ANDB      AL,#0                 ; [CPU_] |711| 
        OR        ACC,*-SP[13]          ; [CPU_] |711| 
        MOVL      *-SP[4],ACC           ; [CPU_] |711| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 712,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 712 | sMessage.uldataw2 = ulShareAddress;                                    
; 714 | // Set ResponseFlag (cleared once data is read into Share Address locat
;     | ion)                                                                   
; 715 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |712| 
        MOVL      *-SP[2],ACC           ; [CPU_] |712| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 716,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 716 | IpcRegs.IPCSET.all |= (ulResponseFlag & 0xFFFF0000);                   
;----------------------------------------------------------------------
        MOVW      DP,#_IpcRegs+4        ; [CPU_U] 
        MOVL      ACC,*-SP[22]          ; [CPU_] |716| 
        ANDB      AL,#0                 ; [CPU_] |716| 
        OR        @_IpcRegs+4,AL        ; [CPU_] |716| 
        OR        @_IpcRegs+5,AH        ; [CPU_] |716| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 717,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 717 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |717| 
        MOV       AL,*-SP[19]           ; [CPU_] |717| 
        SUBB      XAR4,#8               ; [CPU_U] |717| 
        MOVZ      AR5,AR4               ; [CPU_] |717| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |717| 
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("_IpcPut")
	.dwattr $C$DW$158, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |717| 
        ; call occurs [#_IpcPut] ; [] |717| 
        MOV       *-SP[14],AL           ; [CPU_] |717| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 719,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 719 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 723,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$146, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$146, DW_AT_TI_end_line(0x2d3)
	.dwattr $C$DW$146, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$146

	.sect	".text:_IPCLtoRBlockWrite"
	.clink
	.global	_IPCLtoRBlockWrite

$C$DW$160	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$160, DW_AT_name("IPCLtoRBlockWrite")
	.dwattr $C$DW$160, DW_AT_low_pc(_IPCLtoRBlockWrite)
	.dwattr $C$DW$160, DW_AT_high_pc(0x00)
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_IPCLtoRBlockWrite")
	.dwattr $C$DW$160, DW_AT_external
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$160, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$160, DW_AT_TI_begin_line(0x2f9)
	.dwattr $C$DW$160, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$160, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 764,column 1,is_stmt,address _IPCLtoRBlockWrite,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRBlockWrite
$C$DW$161	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$161, DW_AT_name("psController")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$161, DW_AT_location[DW_OP_reg12]

$C$DW$162	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$162, DW_AT_name("ulAddress")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$162, DW_AT_location[DW_OP_reg0]

$C$DW$163	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$163, DW_AT_name("ulShareAddress")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_ulShareAddress")
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$163, DW_AT_location[DW_OP_breg20 -18]

$C$DW$164	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$164, DW_AT_name("usLength")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$164, DW_AT_location[DW_OP_reg14]

$C$DW$165	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$165, DW_AT_name("usWordLength")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_usWordLength")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_breg20 -19]

$C$DW$166	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$166, DW_AT_name("bBlock")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$166, DW_AT_location[DW_OP_breg20 -20]

;----------------------------------------------------------------------
; 761 | IPCLtoRBlockWrite(volatile tIpcController *psController, uint32_t ulAdd
;     | ress,                                                                  
; 762 | uint32_t ulShareAddress, uint16_t usLength, uint16_t usWordLength,     
; 763 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRBlockWrite            FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRBlockWrite:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$167	.dwtag  DW_TAG_variable
	.dwattr $C$DW$167, DW_AT_name("sMessage")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$167, DW_AT_location[DW_OP_breg20 -8]

$C$DW$168	.dwtag  DW_TAG_variable
	.dwattr $C$DW$168, DW_AT_name("psController")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$168, DW_AT_location[DW_OP_breg20 -10]

$C$DW$169	.dwtag  DW_TAG_variable
	.dwattr $C$DW$169, DW_AT_name("ulAddress")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$169, DW_AT_location[DW_OP_breg20 -12]

$C$DW$170	.dwtag  DW_TAG_variable
	.dwattr $C$DW$170, DW_AT_name("usLength")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_breg20 -13]

$C$DW$171	.dwtag  DW_TAG_variable
	.dwattr $C$DW$171, DW_AT_name("status")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 766 | uint16_t status;                                                       
; 767 | tIpcMessage sMessage;                                                  
; 769 | // Set up block write command, address, dataw1 = block length,         
; 770 |     // dataw2 = remote CPU shared mem address                          
; 771 | // where write data resides                                            
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |764| 
        MOVL      *-SP[12],ACC          ; [CPU_] |764| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |764| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 772,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 772 | sMessage.ulcommand = IPC_BLOCK_WRITE;                                  
;----------------------------------------------------------------------
        MOVL      XAR4,#65541           ; [CPU_U] |772| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |772| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 773,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 773 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |773| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 774,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 774 | sMessage.uldataw1 = ((uint32_t)(usWordLength)<<16) + (uint32_t)usLength
;     | ;                                                                      
;----------------------------------------------------------------------
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[19] << 16    ; [CPU_] |774| 
        ADDU      ACC,*-SP[13]          ; [CPU_] |774| 
        MOVL      *-SP[4],ACC           ; [CPU_] |774| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 775,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 775 | sMessage.uldataw2 = ulShareAddress;                                    
; 777 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |775| 
        MOVL      *-SP[2],ACC           ; [CPU_] |775| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 778,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 778 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |778| 
        MOV       AL,*-SP[20]           ; [CPU_] |778| 
        SUBB      XAR4,#8               ; [CPU_U] |778| 
        MOVZ      AR5,AR4               ; [CPU_] |778| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |778| 
$C$DW$172	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$172, DW_AT_low_pc(0x00)
	.dwattr $C$DW$172, DW_AT_name("_IpcPut")
	.dwattr $C$DW$172, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |778| 
        ; call occurs [#_IpcPut] ; [] |778| 
        MOV       *-SP[14],AL           ; [CPU_] |778| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 779,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 779 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 781,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$173	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$173, DW_AT_low_pc(0x00)
	.dwattr $C$DW$173, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$160, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$160, DW_AT_TI_end_line(0x30d)
	.dwattr $C$DW$160, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$160

	.sect	".text:_IPCLtoRBlockWrite_Protected"
	.clink
	.global	_IPCLtoRBlockWrite_Protected

$C$DW$174	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$174, DW_AT_name("IPCLtoRBlockWrite_Protected")
	.dwattr $C$DW$174, DW_AT_low_pc(_IPCLtoRBlockWrite_Protected)
	.dwattr $C$DW$174, DW_AT_high_pc(0x00)
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_IPCLtoRBlockWrite_Protected")
	.dwattr $C$DW$174, DW_AT_external
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$174, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$174, DW_AT_TI_begin_line(0x333)
	.dwattr $C$DW$174, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$174, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 823,column 1,is_stmt,address _IPCLtoRBlockWrite_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRBlockWrite_Protected
$C$DW$175	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$175, DW_AT_name("psController")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$175, DW_AT_location[DW_OP_reg12]

$C$DW$176	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$176, DW_AT_name("ulAddress")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_reg0]

$C$DW$177	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$177, DW_AT_name("ulShareAddress")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_ulShareAddress")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$177, DW_AT_location[DW_OP_breg20 -18]

$C$DW$178	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$178, DW_AT_name("usLength")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$178, DW_AT_location[DW_OP_reg14]

$C$DW$179	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$179, DW_AT_name("usWordLength")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_usWordLength")
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$179, DW_AT_location[DW_OP_breg20 -19]

$C$DW$180	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$180, DW_AT_name("bBlock")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$180, DW_AT_location[DW_OP_breg20 -20]

;----------------------------------------------------------------------
; 819 | IPCLtoRBlockWrite_Protected(volatile tIpcController *psController,     
; 820 | uint32_t ulAddress, uint32_t ulShareAddress,                           
; 821 | uint16_t usLength, uint16_t usWordLength,                              
; 822 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRBlockWrite_Protected  FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRBlockWrite_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$181	.dwtag  DW_TAG_variable
	.dwattr $C$DW$181, DW_AT_name("sMessage")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$181, DW_AT_location[DW_OP_breg20 -8]

$C$DW$182	.dwtag  DW_TAG_variable
	.dwattr $C$DW$182, DW_AT_name("psController")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$182, DW_AT_location[DW_OP_breg20 -10]

$C$DW$183	.dwtag  DW_TAG_variable
	.dwattr $C$DW$183, DW_AT_name("ulAddress")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$183, DW_AT_location[DW_OP_breg20 -12]

$C$DW$184	.dwtag  DW_TAG_variable
	.dwattr $C$DW$184, DW_AT_name("usLength")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$184, DW_AT_location[DW_OP_breg20 -13]

$C$DW$185	.dwtag  DW_TAG_variable
	.dwattr $C$DW$185, DW_AT_name("status")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$185, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 825 | uint16_t status;                                                       
; 826 | tIpcMessage sMessage;                                                  
; 828 | // Set up block write command, address, dataw1 = block length,         
; 829 |     // dataw2 = remote CPU shared mem address                          
; 830 | // where write data resides                                            
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |823| 
        MOVL      *-SP[12],ACC          ; [CPU_] |823| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |823| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 831,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 831 | sMessage.ulcommand = IPC_BLOCK_WRITE_PROTECTED;                        
;----------------------------------------------------------------------
        MOVL      XAR4,#65547           ; [CPU_U] |831| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |831| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 832,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 832 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[6],ACC           ; [CPU_] |832| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 833,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 833 | sMessage.uldataw1 = ((uint32_t)(usWordLength)<<16) + (uint32_t)usLength
;     | ;                                                                      
;----------------------------------------------------------------------
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[19] << 16    ; [CPU_] |833| 
        ADDU      ACC,*-SP[13]          ; [CPU_] |833| 
        MOVL      *-SP[4],ACC           ; [CPU_] |833| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 834,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 834 | sMessage.uldataw2 = ulShareAddress;                                    
; 836 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |834| 
        MOVL      *-SP[2],ACC           ; [CPU_] |834| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 837,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 837 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |837| 
        MOV       AL,*-SP[20]           ; [CPU_] |837| 
        SUBB      XAR4,#8               ; [CPU_U] |837| 
        MOVZ      AR5,AR4               ; [CPU_] |837| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |837| 
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("_IpcPut")
	.dwattr $C$DW$186, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |837| 
        ; call occurs [#_IpcPut] ; [] |837| 
        MOV       *-SP[14],AL           ; [CPU_] |837| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 838,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 838 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 840,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$174, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$174, DW_AT_TI_end_line(0x348)
	.dwattr $C$DW$174, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$174

	.sect	".text:_IPCLtoRFunctionCall"
	.clink
	.global	_IPCLtoRFunctionCall

$C$DW$188	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$188, DW_AT_name("IPCLtoRFunctionCall")
	.dwattr $C$DW$188, DW_AT_low_pc(_IPCLtoRFunctionCall)
	.dwattr $C$DW$188, DW_AT_high_pc(0x00)
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_IPCLtoRFunctionCall")
	.dwattr $C$DW$188, DW_AT_external
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$188, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$188, DW_AT_TI_begin_line(0x35f)
	.dwattr $C$DW$188, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$188, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 866,column 1,is_stmt,address _IPCLtoRFunctionCall,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRFunctionCall
$C$DW$189	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$189, DW_AT_name("psController")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$189, DW_AT_location[DW_OP_reg12]

$C$DW$190	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$190, DW_AT_name("ulAddress")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$190, DW_AT_location[DW_OP_reg0]

$C$DW$191	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$191, DW_AT_name("ulParam")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_ulParam")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$191, DW_AT_location[DW_OP_breg20 -18]

$C$DW$192	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$192, DW_AT_name("bBlock")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$192, DW_AT_location[DW_OP_reg14]

;----------------------------------------------------------------------
; 863 | IPCLtoRFunctionCall(volatile tIpcController *psController, uint32_t ulA
;     | ddress,                                                                
; 864 | uint32_t ulParam,                                                      
; 865 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRFunctionCall          FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRFunctionCall:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$193	.dwtag  DW_TAG_variable
	.dwattr $C$DW$193, DW_AT_name("sMessage")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$193, DW_AT_location[DW_OP_breg20 -8]

$C$DW$194	.dwtag  DW_TAG_variable
	.dwattr $C$DW$194, DW_AT_name("psController")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_breg20 -10]

$C$DW$195	.dwtag  DW_TAG_variable
	.dwattr $C$DW$195, DW_AT_name("ulAddress")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_breg20 -12]

$C$DW$196	.dwtag  DW_TAG_variable
	.dwattr $C$DW$196, DW_AT_name("bBlock")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$196, DW_AT_location[DW_OP_breg20 -13]

$C$DW$197	.dwtag  DW_TAG_variable
	.dwattr $C$DW$197, DW_AT_name("status")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$197, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 867 | uint16_t status;                                                       
; 868 | tIpcMessage sMessage;                                                  
; 870 | // Set up function call command, address, dataw1 = 32-bit parameter    
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |866| 
        MOVL      *-SP[12],ACC          ; [CPU_] |866| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |866| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 871,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 871 | sMessage.ulcommand = IPC_FUNC_CALL;                                    
;----------------------------------------------------------------------
        MOVB      ACC,#18               ; [CPU_] |871| 
        MOVL      *-SP[8],ACC           ; [CPU_] |871| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 872,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 872 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[12]          ; [CPU_] |872| 
        MOVL      *-SP[6],ACC           ; [CPU_] |872| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 873,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 873 | sMessage.uldataw1 = ulParam;                                           
; 875 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |873| 
        MOVL      *-SP[4],ACC           ; [CPU_] |873| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 876,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 876 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |876| 
        SUBB      XAR4,#8               ; [CPU_U] |876| 
        MOV       AL,*-SP[13]           ; [CPU_] |876| 
        MOVZ      AR5,AR4               ; [CPU_] |876| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |876| 
$C$DW$198	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$198, DW_AT_low_pc(0x00)
	.dwattr $C$DW$198, DW_AT_name("_IpcPut")
	.dwattr $C$DW$198, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |876| 
        ; call occurs [#_IpcPut] ; [] |876| 
        MOV       *-SP[14],AL           ; [CPU_] |876| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 877,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 877 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 878,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$199	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$199, DW_AT_low_pc(0x00)
	.dwattr $C$DW$199, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$188, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$188, DW_AT_TI_end_line(0x36e)
	.dwattr $C$DW$188, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$188

	.sect	".text:_IPCLtoRSendMessage"
	.clink
	.global	_IPCLtoRSendMessage

$C$DW$200	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$200, DW_AT_name("IPCLtoRSendMessage")
	.dwattr $C$DW$200, DW_AT_low_pc(_IPCLtoRSendMessage)
	.dwattr $C$DW$200, DW_AT_high_pc(0x00)
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_IPCLtoRSendMessage")
	.dwattr $C$DW$200, DW_AT_external
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$200, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$200, DW_AT_TI_begin_line(0x389)
	.dwattr $C$DW$200, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$200, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 908,column 1,is_stmt,address _IPCLtoRSendMessage,isa 0

	.dwfde $C$DW$CIE, _IPCLtoRSendMessage
$C$DW$201	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$201, DW_AT_name("psController")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$201, DW_AT_location[DW_OP_reg12]

$C$DW$202	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$202, DW_AT_name("ulCommand")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_ulCommand")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$202, DW_AT_location[DW_OP_reg0]

$C$DW$203	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$203, DW_AT_name("ulAddress")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_ulAddress")
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$203, DW_AT_location[DW_OP_breg20 -18]

$C$DW$204	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$204, DW_AT_name("ulDataW1")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_ulDataW1")
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$204, DW_AT_location[DW_OP_breg20 -20]

$C$DW$205	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$205, DW_AT_name("ulDataW2")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_ulDataW2")
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$205, DW_AT_location[DW_OP_breg20 -22]

$C$DW$206	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$206, DW_AT_name("bBlock")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$206, DW_AT_location[DW_OP_reg14]

;----------------------------------------------------------------------
; 905 | IPCLtoRSendMessage(volatile tIpcController *psController, uint32_t ulCo
;     | mmand,                                                                 
; 906 | uint32_t ulAddress, uint32_t ulDataW1, uint32_t ulDataW2,              
; 907 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCLtoRSendMessage           FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_IPCLtoRSendMessage:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$207	.dwtag  DW_TAG_variable
	.dwattr $C$DW$207, DW_AT_name("sMessage")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_breg20 -8]

$C$DW$208	.dwtag  DW_TAG_variable
	.dwattr $C$DW$208, DW_AT_name("psController")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_breg20 -10]

$C$DW$209	.dwtag  DW_TAG_variable
	.dwattr $C$DW$209, DW_AT_name("ulCommand")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_ulCommand")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_breg20 -12]

$C$DW$210	.dwtag  DW_TAG_variable
	.dwattr $C$DW$210, DW_AT_name("bBlock")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$210, DW_AT_location[DW_OP_breg20 -13]

$C$DW$211	.dwtag  DW_TAG_variable
	.dwattr $C$DW$211, DW_AT_name("status")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$211, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 910 | uint16_t status;                                                       
; 911 | tIpcMessage sMessage;                                                  
; 913 | // Package message to send                                             
;----------------------------------------------------------------------
        MOV       *-SP[13],AR5          ; [CPU_] |908| 
        MOVL      *-SP[12],ACC          ; [CPU_] |908| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |908| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 914,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 914 | sMessage.ulcommand = ulCommand;                                        
;----------------------------------------------------------------------
        MOVL      *-SP[8],ACC           ; [CPU_] |914| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 915,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 915 | sMessage.uladdress = ulAddress;                                        
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[18]          ; [CPU_] |915| 
        MOVL      *-SP[6],ACC           ; [CPU_] |915| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 916,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 916 | sMessage.uldataw1 = ulDataW1;                                          
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[20]          ; [CPU_] |916| 
        MOVL      *-SP[4],ACC           ; [CPU_] |916| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 917,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 917 | sMessage.uldataw2 = ulDataW2;                                          
; 919 | // Put Message into PutBuffer and set IPC INT flag                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[22]          ; [CPU_] |917| 
        MOVL      *-SP[2],ACC           ; [CPU_] |917| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 920,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 920 | status = IpcPut (psController, &sMessage, bBlock);                     
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |920| 
        SUBB      XAR4,#8               ; [CPU_U] |920| 
        MOV       AL,*-SP[13]           ; [CPU_] |920| 
        MOVZ      AR5,AR4               ; [CPU_] |920| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |920| 
$C$DW$212	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$212, DW_AT_low_pc(0x00)
	.dwattr $C$DW$212, DW_AT_name("_IpcPut")
	.dwattr $C$DW$212, DW_AT_TI_call

        LCR       #_IpcPut              ; [CPU_] |920| 
        ; call occurs [#_IpcPut] ; [] |920| 
        MOV       *-SP[14],AL           ; [CPU_] |920| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 921,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 921 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 922,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$213	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$213, DW_AT_low_pc(0x00)
	.dwattr $C$DW$213, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$200, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$200, DW_AT_TI_end_line(0x39a)
	.dwattr $C$DW$200, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$200

	.sect	".text:_IPCRtoLDataWrite"
	.clink
	.global	_IPCRtoLDataWrite

$C$DW$214	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$214, DW_AT_name("IPCRtoLDataWrite")
	.dwattr $C$DW$214, DW_AT_low_pc(_IPCRtoLDataWrite)
	.dwattr $C$DW$214, DW_AT_high_pc(0x00)
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_IPCRtoLDataWrite")
	.dwattr $C$DW$214, DW_AT_external
	.dwattr $C$DW$214, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$214, DW_AT_TI_begin_line(0x3e8)
	.dwattr $C$DW$214, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$214, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1001,column 1,is_stmt,address _IPCRtoLDataWrite,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLDataWrite
$C$DW$215	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$215, DW_AT_name("psMessage")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$215, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1000 | IPCRtoLDataWrite(tIpcMessage *psMessage)                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLDataWrite             FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLDataWrite:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$216	.dwtag  DW_TAG_variable
	.dwattr $C$DW$216, DW_AT_name("psMessage")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$216, DW_AT_location[DW_OP_breg20 -2]

$C$DW$217	.dwtag  DW_TAG_variable
	.dwattr $C$DW$217, DW_AT_name("responseFlag")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_responseFlag")
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$217, DW_AT_location[DW_OP_breg20 -4]

$C$DW$218	.dwtag  DW_TAG_variable
	.dwattr $C$DW$218, DW_AT_name("length")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$218, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 1002 | // Data word length = dataw1 (15:0), responseFlag = valid only for IPC
;     | flags                                                                  
; 1003 | // 17-32                                                               
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1001| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1004,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 1004 | uint16_t length = (uint16_t) psMessage->uldataw1;                      
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[4]          ; [CPU_] |1004| 
        MOV       *-SP[5],AL            ; [CPU_] |1004| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1005,column 27,is_stmt,isa 0
;----------------------------------------------------------------------
; 1005 | uint32_t responseFlag = (psMessage->uldataw1) & 0xFFFF0000;            
; 1007 | // Write 16/32-bit word to address                                     
; 1008 | //                                                                     
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |1005| 
        ANDB      AL,#0                 ; [CPU_] |1005| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1005| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1009,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1009 | if (length == IPC_LENGTH_16_BITS)                                      
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |1009| 
        CMPB      AL,#1                 ; [CPU_] |1009| 
        B         $C$L10,NEQ            ; [CPU_] |1009| 
        ; branchcc occurs ; [] |1009| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1011,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1011 | *(uint16_t *)(psMessage->uladdress) = (uint16_t)psMessage->uldataw2;   
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1011| 
        MOVL      XAR7,*+XAR5[2]        ; [CPU_] |1011| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |1011| 
        MOV       *+XAR7[0],AL          ; [CPU_] |1011| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1012,column 5,is_stmt,isa 0
        B         $C$L11,UNC            ; [CPU_] |1012| 
        ; branch occurs ; [] |1012| 
$C$L10:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1013,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1013 | else if (length == IPC_LENGTH_32_BITS)                                 
;----------------------------------------------------------------------
        CMPB      AL,#2                 ; [CPU_] |1013| 
        B         $C$L11,NEQ            ; [CPU_] |1013| 
        ; branchcc occurs ; [] |1013| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1015,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1015 | *(uint32_t *)(psMessage->uladdress) = psMessage->uldataw2;             
; 1018 | // If data write command is in response to a data read command from rem
;     | ote                                                                    
; 1019 | // CPU to local CPU clear ResponseFlag, indicating read data from remot
;     | e                                                                      
; 1020 | // CPU is ready.                                                       
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1015| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1015| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |1015| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |1015| 
$C$L11:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1022,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1022 | IpcRegs.IPCCLR.all |= responseFlag;                                    
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[4]           ; [CPU_] |1022| 
        MOVW      DP,#_IpcRegs+6        ; [CPU_U] 
        OR        @_IpcRegs+6,AL        ; [CPU_] |1022| 
        OR        @_IpcRegs+7,AH        ; [CPU_] |1022| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1023,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$219	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$219, DW_AT_low_pc(0x00)
	.dwattr $C$DW$219, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$214, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$214, DW_AT_TI_end_line(0x3ff)
	.dwattr $C$DW$214, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$214

	.sect	".text:_IPCRtoLDataWrite_Protected"
	.clink
	.global	_IPCRtoLDataWrite_Protected

$C$DW$220	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$220, DW_AT_name("IPCRtoLDataWrite_Protected")
	.dwattr $C$DW$220, DW_AT_low_pc(_IPCRtoLDataWrite_Protected)
	.dwattr $C$DW$220, DW_AT_high_pc(0x00)
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_IPCRtoLDataWrite_Protected")
	.dwattr $C$DW$220, DW_AT_external
	.dwattr $C$DW$220, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$220, DW_AT_TI_begin_line(0x414)
	.dwattr $C$DW$220, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$220, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1045,column 1,is_stmt,address _IPCRtoLDataWrite_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLDataWrite_Protected
$C$DW$221	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$221, DW_AT_name("psMessage")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1044 | IPCRtoLDataWrite_Protected(tIpcMessage *psMessage)                     
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLDataWrite_Protected   FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLDataWrite_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$222	.dwtag  DW_TAG_variable
	.dwattr $C$DW$222, DW_AT_name("psMessage")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$222, DW_AT_location[DW_OP_breg20 -2]

$C$DW$223	.dwtag  DW_TAG_variable
	.dwattr $C$DW$223, DW_AT_name("responseFlag")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_responseFlag")
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$223, DW_AT_location[DW_OP_breg20 -4]

$C$DW$224	.dwtag  DW_TAG_variable
	.dwattr $C$DW$224, DW_AT_name("length")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 1046 | // Data word length = dataw1 (15:0), responseFlag = valid only for IPC
;     | flags                                                                  
; 1047 | // 17-32                                                               
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1045| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1048,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 1048 | uint16_t length = (uint16_t) psMessage->uldataw1;                      
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[4]          ; [CPU_] |1048| 
        MOV       *-SP[5],AL            ; [CPU_] |1048| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1049,column 27,is_stmt,isa 0
;----------------------------------------------------------------------
; 1049 | uint32_t responseFlag = (psMessage->uldataw1) & 0xFFFF0000;            
; 1051 | // Allow access to EALLOW-protected registers.                         
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |1049| 
        ANDB      AL,#0                 ; [CPU_] |1049| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1049| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1052,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1052 | EALLOW;                                                                
; 1054 | // Write 16/32-bit word to EALLOW-protected address                    
; 1055 | //                                                                     
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1056,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1056 | if (length == IPC_LENGTH_16_BITS)                                      
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |1056| 
        CMPB      AL,#1                 ; [CPU_] |1056| 
        B         $C$L12,NEQ            ; [CPU_] |1056| 
        ; branchcc occurs ; [] |1056| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1058,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1058 | *(uint16_t *)(psMessage->uladdress) = (uint16_t)psMessage->uldataw2;   
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1058| 
        MOVL      XAR7,*+XAR5[2]        ; [CPU_] |1058| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |1058| 
        MOV       *+XAR7[0],AL          ; [CPU_] |1058| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1059,column 5,is_stmt,isa 0
        B         $C$L13,UNC            ; [CPU_] |1059| 
        ; branch occurs ; [] |1059| 
$C$L12:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1060,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1060 | else if (length == IPC_LENGTH_32_BITS)                                 
;----------------------------------------------------------------------
        CMPB      AL,#2                 ; [CPU_] |1060| 
        B         $C$L13,NEQ            ; [CPU_] |1060| 
        ; branchcc occurs ; [] |1060| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1062,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1062 | *(uint32_t *)(psMessage->uladdress) = psMessage->uldataw2;             
; 1065 | // Disable access to EALLOW-protected registers.                       
; 1066 | //                                                                     
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1062| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1062| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |1062| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |1062| 
$C$L13:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1067,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1067 | EDIS;                                                                  
; 1069 | // If data write command is in response to a data read command from loc
;     | al                                                                     
; 1070 | // CPU to remote CPU, clear ResponseFlag, indicating read data from    
; 1071 | // secondary CPU is ready                                              
;----------------------------------------------------------------------
 EDIS
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1072,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1072 | IpcRegs.IPCCLR.all |= responseFlag;                                    
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[4]           ; [CPU_] |1072| 
        MOVW      DP,#_IpcRegs+6        ; [CPU_U] 
        OR        @_IpcRegs+6,AL        ; [CPU_] |1072| 
        OR        @_IpcRegs+7,AH        ; [CPU_] |1072| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1073,column 1,is_stmt,isa 0
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$225	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$225, DW_AT_low_pc(0x00)
	.dwattr $C$DW$225, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$220, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$220, DW_AT_TI_end_line(0x431)
	.dwattr $C$DW$220, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$220

	.sect	".text:_IPCRtoLDataRead"
	.clink
	.global	_IPCRtoLDataRead

$C$DW$226	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$226, DW_AT_name("IPCRtoLDataRead")
	.dwattr $C$DW$226, DW_AT_low_pc(_IPCRtoLDataRead)
	.dwattr $C$DW$226, DW_AT_high_pc(0x00)
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_IPCRtoLDataRead")
	.dwattr $C$DW$226, DW_AT_external
	.dwattr $C$DW$226, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$226, DW_AT_TI_begin_line(0x448)
	.dwattr $C$DW$226, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$226, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1098,column 1,is_stmt,address _IPCRtoLDataRead,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLDataRead
$C$DW$227	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$227, DW_AT_name("psController")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$227, DW_AT_location[DW_OP_reg12]

$C$DW$228	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$228, DW_AT_name("psMessage")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$228, DW_AT_location[DW_OP_reg14]

$C$DW$229	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$229, DW_AT_name("bBlock")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$229, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1096 | IPCRtoLDataRead(volatile tIpcController *psController, tIpcMessage *psM
;     | essage,                                                                
; 1097 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLDataRead              FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            6 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLDataRead:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$230	.dwtag  DW_TAG_variable
	.dwattr $C$DW$230, DW_AT_name("psController")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$230, DW_AT_location[DW_OP_breg20 -8]

$C$DW$231	.dwtag  DW_TAG_variable
	.dwattr $C$DW$231, DW_AT_name("psMessage")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$231, DW_AT_location[DW_OP_breg20 -10]

$C$DW$232	.dwtag  DW_TAG_variable
	.dwattr $C$DW$232, DW_AT_name("ulReaddata")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_ulReaddata")
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$232, DW_AT_location[DW_OP_breg20 -12]

$C$DW$233	.dwtag  DW_TAG_variable
	.dwattr $C$DW$233, DW_AT_name("bBlock")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$233, DW_AT_location[DW_OP_breg20 -13]

$C$DW$234	.dwtag  DW_TAG_variable
	.dwattr $C$DW$234, DW_AT_name("usLength")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$234, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 1099 | unsigned long ulReaddata;                                              
; 1100 | uint16_t usLength;                                                     
; 1102 | // If data word length = 16-bits, read the 16-bit value at the given ad
;     | dress                                                                  
; 1103 | // and cast as 32-bit word to send back to remote CPU.                 
; 1104 | // If data word length = 32-bits, read the 32-bit value at the given   
; 1105 | // address.                                                            
;----------------------------------------------------------------------
        MOV       *-SP[13],AL           ; [CPU_] |1098| 
        MOVL      *-SP[10],XAR5         ; [CPU_] |1098| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |1098| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1107,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1107 | usLength = (uint16_t)psMessage->uldataw1;                              
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1107| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |1107| 
        MOV       *-SP[14],AL           ; [CPU_] |1107| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1109,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1109 | if (usLength == IPC_LENGTH_16_BITS)                                    
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |1109| 
        B         $C$L14,NEQ            ; [CPU_] |1109| 
        ; branchcc occurs ; [] |1109| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1111,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1111 | ulReaddata = (unsigned long)(*(volatile uint16_t *)psMessage->uladdress
;     | );                                                                     
;----------------------------------------------------------------------
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1111| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |1111| 
        MOVL      *-SP[12],ACC          ; [CPU_] |1111| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1112,column 5,is_stmt,isa 0
        B         $C$L15,UNC            ; [CPU_] |1112| 
        ; branch occurs ; [] |1112| 
$C$L14:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1113,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1113 | else if (usLength == IPC_LENGTH_32_BITS)                               
;----------------------------------------------------------------------
        CMPB      AL,#2                 ; [CPU_] |1113| 
        B         $C$L15,NEQ            ; [CPU_] |1113| 
        ; branchcc occurs ; [] |1113| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1115,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1115 | ulReaddata = *(unsigned long *)psMessage->uladdress;                   
; 1118 | // Send a Write command to write the requested data to the remote CPU r
;     | ead into                                                               
; 1119 | // address.                                                            
; 1120 | // psMessage->uldataw2 contains remote CPU address where readdata will
;     | be written.                                                            
; 1121 | // psMessage->uldataw1 contains the read response flag in IPC flag 17-3
;     | 2.                                                                     
;----------------------------------------------------------------------
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1115| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1115| 
        MOVL      *-SP[12],ACC          ; [CPU_] |1115| 
$C$L15:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1122,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1122 | IPCLtoRDataWrite(psController, psMessage->uldataw2, ulReaddata, usLengt
;     | h,                                                                     
; 1123 |                  bBlock,(psMessage->uldataw1 & 0xFFFF0000));           
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[12]          ; [CPU_] |1122| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1122| 
        MOV       AL,*-SP[13]           ; [CPU_] |1122| 
        MOV       *-SP[3],AL            ; [CPU_] |1122| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1122| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |1122| 
        ANDB      AL,#0                 ; [CPU_] |1122| 
        MOVL      *-SP[6],ACC           ; [CPU_] |1122| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1122| 
        MOVZ      AR5,*-SP[14]          ; [CPU_] |1122| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1122| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |1122| 
$C$DW$235	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$235, DW_AT_low_pc(0x00)
	.dwattr $C$DW$235, DW_AT_name("_IPCLtoRDataWrite")
	.dwattr $C$DW$235, DW_AT_TI_call

        LCR       #_IPCLtoRDataWrite    ; [CPU_] |1122| 
        ; call occurs [#_IPCLtoRDataWrite] ; [] |1122| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1124,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$236	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$236, DW_AT_low_pc(0x00)
	.dwattr $C$DW$236, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$226, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$226, DW_AT_TI_end_line(0x464)
	.dwattr $C$DW$226, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$226

	.sect	".text:_IPCRtoLDataRead_Protected"
	.clink
	.global	_IPCRtoLDataRead_Protected

$C$DW$237	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$237, DW_AT_name("IPCRtoLDataRead_Protected")
	.dwattr $C$DW$237, DW_AT_low_pc(_IPCRtoLDataRead_Protected)
	.dwattr $C$DW$237, DW_AT_high_pc(0x00)
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_IPCRtoLDataRead_Protected")
	.dwattr $C$DW$237, DW_AT_external
	.dwattr $C$DW$237, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$237, DW_AT_TI_begin_line(0x47c)
	.dwattr $C$DW$237, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$237, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1151,column 1,is_stmt,address _IPCRtoLDataRead_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLDataRead_Protected
$C$DW$238	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$238, DW_AT_name("psController")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_reg12]

$C$DW$239	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$239, DW_AT_name("psMessage")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$239, DW_AT_location[DW_OP_reg14]

$C$DW$240	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$240, DW_AT_name("bBlock")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$240, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1148 | IPCRtoLDataRead_Protected(volatile tIpcController *psController,       
; 1149 | tIpcMessage *psMessage,                                                
; 1150 | uint16_t bBlock)                                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLDataRead_Protected    FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            6 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLDataRead_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$241	.dwtag  DW_TAG_variable
	.dwattr $C$DW$241, DW_AT_name("psController")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_psController")
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$241, DW_AT_location[DW_OP_breg20 -8]

$C$DW$242	.dwtag  DW_TAG_variable
	.dwattr $C$DW$242, DW_AT_name("psMessage")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$242, DW_AT_location[DW_OP_breg20 -10]

$C$DW$243	.dwtag  DW_TAG_variable
	.dwattr $C$DW$243, DW_AT_name("ulReaddata")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_ulReaddata")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_breg20 -12]

$C$DW$244	.dwtag  DW_TAG_variable
	.dwattr $C$DW$244, DW_AT_name("bBlock")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_bBlock")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_breg20 -13]

$C$DW$245	.dwtag  DW_TAG_variable
	.dwattr $C$DW$245, DW_AT_name("usLength")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$245, DW_AT_location[DW_OP_breg20 -14]

;----------------------------------------------------------------------
; 1152 | unsigned long ulReaddata;                                              
; 1153 | uint16_t usLength;                                                     
; 1155 | // If data word length = 16-bits, read the 16-bit value at the given ad
;     | dress                                                                  
; 1156 | // and cast as                                                         
; 1157 | // 32-bit word to send back to remote CPU.                             
; 1158 | // If data word length = 32-bits, read the 32-bit value at the given   
; 1159 | // address.                                                            
; 1160 | //                                                                     
;----------------------------------------------------------------------
        MOV       *-SP[13],AL           ; [CPU_] |1151| 
        MOVL      *-SP[10],XAR5         ; [CPU_] |1151| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |1151| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1161,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1161 | usLength = (uint16_t)psMessage->uldataw1;                              
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1161| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |1161| 
        MOV       *-SP[14],AL           ; [CPU_] |1161| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1163,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1163 | if (usLength == IPC_LENGTH_16_BITS)                                    
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |1163| 
        B         $C$L16,NEQ            ; [CPU_] |1163| 
        ; branchcc occurs ; [] |1163| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1165,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1165 | ulReaddata = (unsigned long)(*(volatile uint16_t *)psMessage->uladdress
;     | );                                                                     
;----------------------------------------------------------------------
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1165| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |1165| 
        MOVL      *-SP[12],ACC          ; [CPU_] |1165| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1166,column 5,is_stmt,isa 0
        B         $C$L17,UNC            ; [CPU_] |1166| 
        ; branch occurs ; [] |1166| 
$C$L16:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1167,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1167 | else if (usLength == IPC_LENGTH_32_BITS)                               
;----------------------------------------------------------------------
        CMPB      AL,#2                 ; [CPU_] |1167| 
        B         $C$L17,NEQ            ; [CPU_] |1167| 
        ; branchcc occurs ; [] |1167| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1169,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1169 | ulReaddata = *(unsigned long *)psMessage->uladdress;                   
; 1172 | // Send a Write command to write the requested data to the remote CPU r
;     | ead into                                                               
; 1173 | // address.                                                            
; 1174 | // psMessage->uldataw2 contains remote CPU address where readdata will
;     | be written.                                                            
; 1175 | // psMessage->uldataw1 contains the read response flag in IPC flag 17-3
;     | 2.                                                                     
;----------------------------------------------------------------------
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1169| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1169| 
        MOVL      *-SP[12],ACC          ; [CPU_] |1169| 
$C$L17:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1176,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1176 | IPCLtoRDataWrite_Protected(psController, psMessage->uldataw2, ulReaddat
;     | a,                                                                     
; 1177 |                            usLength, bBlock,(psMessage->uldataw1 & 0xFF
;     | FF0000));                                                              
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[12]          ; [CPU_] |1176| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1176| 
        MOV       AL,*-SP[13]           ; [CPU_] |1176| 
        MOV       *-SP[3],AL            ; [CPU_] |1176| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1176| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |1176| 
        ANDB      AL,#0                 ; [CPU_] |1176| 
        MOVL      *-SP[6],ACC           ; [CPU_] |1176| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1176| 
        MOVZ      AR5,*-SP[14]          ; [CPU_] |1176| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1176| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |1176| 
$C$DW$246	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$246, DW_AT_low_pc(0x00)
	.dwattr $C$DW$246, DW_AT_name("_IPCLtoRDataWrite_Protected")
	.dwattr $C$DW$246, DW_AT_TI_call

        LCR       #_IPCLtoRDataWrite_Protected ; [CPU_] |1176| 
        ; call occurs [#_IPCLtoRDataWrite_Protected] ; [] |1176| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1178,column 1,is_stmt,isa 0
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$237, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$237, DW_AT_TI_end_line(0x49a)
	.dwattr $C$DW$237, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$237

	.sect	".text:_IPCRtoLSetBits"
	.clink
	.global	_IPCRtoLSetBits

$C$DW$248	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$248, DW_AT_name("IPCRtoLSetBits")
	.dwattr $C$DW$248, DW_AT_low_pc(_IPCRtoLSetBits)
	.dwattr $C$DW$248, DW_AT_high_pc(0x00)
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_IPCRtoLSetBits")
	.dwattr $C$DW$248, DW_AT_external
	.dwattr $C$DW$248, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$248, DW_AT_TI_begin_line(0x4aa)
	.dwattr $C$DW$248, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$248, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1195,column 1,is_stmt,address _IPCRtoLSetBits,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLSetBits
$C$DW$249	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$249, DW_AT_name("psMessage")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$249, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1194 | IPCRtoLSetBits(tIpcMessage *psMessage)                                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLSetBits               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLSetBits:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$250	.dwtag  DW_TAG_variable
	.dwattr $C$DW$250, DW_AT_name("psMessage")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$250, DW_AT_location[DW_OP_breg20 -2]

$C$DW$251	.dwtag  DW_TAG_variable
	.dwattr $C$DW$251, DW_AT_name("usLength")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$251, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 1196 | uint16_t usLength;                                                     
; 1198 | // Determine length of word at psMessage->uladdress and then set bits b
;     | ased                                                                   
; 1199 | // on either the 16-bit or 32-bit bit-mask in psMessage->uldataw2.     
; 1200 | // (16-bit length ignores upper 16-bits of psMessage->uldataw2)        
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1195| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1202,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1202 | usLength = (uint16_t)psMessage->uldataw1;                              
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[4]          ; [CPU_] |1202| 
        MOV       *-SP[3],AL            ; [CPU_] |1202| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1204,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1204 | if (usLength == IPC_LENGTH_16_BITS)                                    
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |1204| 
        B         $C$L18,NEQ            ; [CPU_] |1204| 
        ; branchcc occurs ; [] |1204| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1206,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1206 | *(volatile uint16_t*)psMessage->uladdress |= (uint16_t) psMessage->ulda
;     | taw2;                                                                  
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1206| 
        MOVL      XAR5,*+XAR5[2]        ; [CPU_] |1206| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |1206| 
        OR        *+XAR5[0],AL          ; [CPU_] |1206| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1207,column 5,is_stmt,isa 0
        B         $C$L19,UNC            ; [CPU_] |1207| 
        ; branch occurs ; [] |1207| 
$C$L18:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1208,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1208 | else if (usLength == IPC_LENGTH_32_BITS)                               
;----------------------------------------------------------------------
        CMPB      AL,#2                 ; [CPU_] |1208| 
        B         $C$L19,NEQ            ; [CPU_] |1208| 
        ; branchcc occurs ; [] |1208| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1210,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1210 | *(volatile unsigned long *)psMessage->uladdress |=  psMessage->uldataw2
;     | ;                                                                      
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1210| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1210| 
        MOVL      ACC,*+XAR5[6]         ; [CPU_] |1210| 
        OR        *+XAR4[0],AL          ; [CPU_] |1210| 
        OR        *+XAR4[1],AH          ; [CPU_] |1210| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1212,column 1,is_stmt,isa 0
$C$L19:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$252	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$252, DW_AT_low_pc(0x00)
	.dwattr $C$DW$252, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$248, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$248, DW_AT_TI_end_line(0x4bc)
	.dwattr $C$DW$248, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$248

	.sect	".text:_IPCRtoLSetBits_Protected"
	.clink
	.global	_IPCRtoLSetBits_Protected

$C$DW$253	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$253, DW_AT_name("IPCRtoLSetBits_Protected")
	.dwattr $C$DW$253, DW_AT_low_pc(_IPCRtoLSetBits_Protected)
	.dwattr $C$DW$253, DW_AT_high_pc(0x00)
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_IPCRtoLSetBits_Protected")
	.dwattr $C$DW$253, DW_AT_external
	.dwattr $C$DW$253, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$253, DW_AT_TI_begin_line(0x4cc)
	.dwattr $C$DW$253, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$253, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1229,column 1,is_stmt,address _IPCRtoLSetBits_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLSetBits_Protected
$C$DW$254	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$254, DW_AT_name("psMessage")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$254, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1228 | IPCRtoLSetBits_Protected(tIpcMessage *psMessage)                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLSetBits_Protected     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLSetBits_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$255	.dwtag  DW_TAG_variable
	.dwattr $C$DW$255, DW_AT_name("psMessage")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$255, DW_AT_location[DW_OP_breg20 -2]

$C$DW$256	.dwtag  DW_TAG_variable
	.dwattr $C$DW$256, DW_AT_name("usLength")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$256, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 1230 | uint16_t usLength;                                                     
; 1232 | // Allow access to EALLOW-protected registers.                         
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1229| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1233,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1233 | EALLOW;                                                                
; 1235 | // Determine length of word at psMessage->uladdress and then set bits b
;     | ased                                                                   
; 1236 | // on either the 16-bit or 32-bit bit-mask in psMessage->uldataw2.     
; 1237 | // (16-bit length ignores upper 16-bits of psMessage->uldataw2)        
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1238,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1238 | usLength = (uint16_t)psMessage->uldataw1;                              
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[4]          ; [CPU_] |1238| 
        MOV       *-SP[3],AL            ; [CPU_] |1238| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1240,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1240 | if (usLength == IPC_LENGTH_16_BITS)                                    
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |1240| 
        B         $C$L20,NEQ            ; [CPU_] |1240| 
        ; branchcc occurs ; [] |1240| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1242,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1242 | *(volatile uint16_t*)psMessage->uladdress |= (uint16_t) psMessage->ulda
;     | taw2;                                                                  
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1242| 
        MOVL      XAR5,*+XAR5[2]        ; [CPU_] |1242| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |1242| 
        OR        *+XAR5[0],AL          ; [CPU_] |1242| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1243,column 5,is_stmt,isa 0
        B         $C$L21,UNC            ; [CPU_] |1243| 
        ; branch occurs ; [] |1243| 
$C$L20:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1244,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1244 | else if (usLength == IPC_LENGTH_32_BITS)                               
;----------------------------------------------------------------------
        CMPB      AL,#2                 ; [CPU_] |1244| 
        B         $C$L21,NEQ            ; [CPU_] |1244| 
        ; branchcc occurs ; [] |1244| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1246,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1246 | *(volatile unsigned long *)psMessage->uladdress |= psMessage->uldataw2;
; 1249 | // Disable access to EALLOW-protected registers.                       
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1246| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1246| 
        MOVL      ACC,*+XAR5[6]         ; [CPU_] |1246| 
        OR        *+XAR4[0],AL          ; [CPU_] |1246| 
        OR        *+XAR4[1],AH          ; [CPU_] |1246| 
$C$L21:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1250,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1250 | EDIS;                                                                  
;----------------------------------------------------------------------
 EDIS
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1251,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
        SPM       #0                    ; [CPU_] 
$C$DW$257	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$257, DW_AT_low_pc(0x00)
	.dwattr $C$DW$257, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$253, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$253, DW_AT_TI_end_line(0x4e3)
	.dwattr $C$DW$253, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$253

	.sect	".text:_IPCRtoLClearBits"
	.clink
	.global	_IPCRtoLClearBits

$C$DW$258	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$258, DW_AT_name("IPCRtoLClearBits")
	.dwattr $C$DW$258, DW_AT_low_pc(_IPCRtoLClearBits)
	.dwattr $C$DW$258, DW_AT_high_pc(0x00)
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_IPCRtoLClearBits")
	.dwattr $C$DW$258, DW_AT_external
	.dwattr $C$DW$258, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$258, DW_AT_TI_begin_line(0x4f3)
	.dwattr $C$DW$258, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$258, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1268,column 1,is_stmt,address _IPCRtoLClearBits,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLClearBits
$C$DW$259	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$259, DW_AT_name("psMessage")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$259, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1267 | IPCRtoLClearBits(tIpcMessage *psMessage)                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLClearBits             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLClearBits:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$260	.dwtag  DW_TAG_variable
	.dwattr $C$DW$260, DW_AT_name("psMessage")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$260, DW_AT_location[DW_OP_breg20 -2]

$C$DW$261	.dwtag  DW_TAG_variable
	.dwattr $C$DW$261, DW_AT_name("usLength")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$261, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 1270 | uint16_t usLength;                                                     
; 1272 | // Determine length of word at psMessage->uladdress and then clear bits
; 1273 | // based on                                                            
; 1274 | // either the 16-bit or 32-bit bit-mask in psMessage->uldataw2.        
; 1275 | // (16-bit length ignores upper 16-bits of psMessage->uldataw2)        
; 1276 | //                                                                     
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1268| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1277,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1277 | usLength = (uint16_t)psMessage->uldataw1;                              
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[4]          ; [CPU_] |1277| 
        MOV       *-SP[3],AL            ; [CPU_] |1277| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1279,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1279 | if (usLength == IPC_LENGTH_16_BITS)                                    
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |1279| 
        B         $C$L22,NEQ            ; [CPU_] |1279| 
        ; branchcc occurs ; [] |1279| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1281,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1281 | *(volatile uint16_t*)psMessage->uladdress &=                           
; 1282 |     ~((uint16_t) psMessage->uldataw2);                                 
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1281| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |1281| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |1281| 
        NOT       AL                    ; [CPU_] |1281| 
        AND       *+XAR4[0],AL          ; [CPU_] |1281| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1283,column 5,is_stmt,isa 0
        B         $C$L23,UNC            ; [CPU_] |1283| 
        ; branch occurs ; [] |1283| 
$C$L22:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1284,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1284 | else if (usLength == IPC_LENGTH_32_BITS)                               
;----------------------------------------------------------------------
        CMPB      AL,#2                 ; [CPU_] |1284| 
        B         $C$L23,NEQ            ; [CPU_] |1284| 
        ; branchcc occurs ; [] |1284| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1286,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1286 | *(volatile unsigned long *)psMessage->uladdress &=                     
; 1287 |     ~(psMessage->uldataw2);                                            
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1286| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1286| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |1286| 
        NOT       ACC                   ; [CPU_] |1286| 
        AND       *+XAR4[0],AL          ; [CPU_] |1286| 
        AND       *+XAR4[1],AH          ; [CPU_] |1286| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1289,column 1,is_stmt,isa 0
$C$L23:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$262	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$262, DW_AT_low_pc(0x00)
	.dwattr $C$DW$262, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$258, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$258, DW_AT_TI_end_line(0x509)
	.dwattr $C$DW$258, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$258

	.sect	".text:_IPCRtoLClearBits_Protected"
	.clink
	.global	_IPCRtoLClearBits_Protected

$C$DW$263	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$263, DW_AT_name("IPCRtoLClearBits_Protected")
	.dwattr $C$DW$263, DW_AT_low_pc(_IPCRtoLClearBits_Protected)
	.dwattr $C$DW$263, DW_AT_high_pc(0x00)
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_IPCRtoLClearBits_Protected")
	.dwattr $C$DW$263, DW_AT_external
	.dwattr $C$DW$263, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$263, DW_AT_TI_begin_line(0x519)
	.dwattr $C$DW$263, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$263, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1306,column 1,is_stmt,address _IPCRtoLClearBits_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLClearBits_Protected
$C$DW$264	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$264, DW_AT_name("psMessage")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$264, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1305 | IPCRtoLClearBits_Protected(tIpcMessage *psMessage)                     
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLClearBits_Protected   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLClearBits_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$265	.dwtag  DW_TAG_variable
	.dwattr $C$DW$265, DW_AT_name("psMessage")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$265, DW_AT_location[DW_OP_breg20 -2]

$C$DW$266	.dwtag  DW_TAG_variable
	.dwattr $C$DW$266, DW_AT_name("usLength")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$266, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 1308 | uint16_t usLength;                                                     
; 1310 | // Allow access to EALLOW-protected registers.                         
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1306| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1311,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1311 | EALLOW;                                                                
; 1312 | // Determine length of word at psMessage->uladdress and then clear bits
; 1313 | // based on                                                            
; 1314 | // either the 16-bit or 32-bit bit-mask in psMessage->uldataw2.        
; 1315 | // (16-bit length ignores upper 16-bits of psMessage->uldataw2)        
; 1316 | //                                                                     
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1317,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1317 | usLength = (uint16_t)psMessage->uldataw1;                              
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[4]          ; [CPU_] |1317| 
        MOV       *-SP[3],AL            ; [CPU_] |1317| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1319,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1319 | if (usLength == IPC_LENGTH_16_BITS)                                    
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |1319| 
        B         $C$L24,NEQ            ; [CPU_] |1319| 
        ; branchcc occurs ; [] |1319| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1321,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1321 | *(volatile uint16_t*)psMessage->uladdress &=                           
; 1322 |     ~((uint16_t) psMessage->uldataw2);                                 
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1321| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |1321| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |1321| 
        NOT       AL                    ; [CPU_] |1321| 
        AND       *+XAR4[0],AL          ; [CPU_] |1321| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1323,column 5,is_stmt,isa 0
        B         $C$L25,UNC            ; [CPU_] |1323| 
        ; branch occurs ; [] |1323| 
$C$L24:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1324,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1324 | else if (usLength == IPC_LENGTH_32_BITS)                               
;----------------------------------------------------------------------
        CMPB      AL,#2                 ; [CPU_] |1324| 
        B         $C$L25,NEQ            ; [CPU_] |1324| 
        ; branchcc occurs ; [] |1324| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1326,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1326 | *(volatile unsigned long *)psMessage->uladdress &=                     
; 1327 |     ~(psMessage->uldataw2);                                            
; 1330 | // Disable access to EALLOW-protected registers.                       
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |1326| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1326| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |1326| 
        NOT       ACC                   ; [CPU_] |1326| 
        AND       *+XAR4[0],AL          ; [CPU_] |1326| 
        AND       *+XAR4[1],AH          ; [CPU_] |1326| 
$C$L25:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1331,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1331 | EDIS;                                                                  
;----------------------------------------------------------------------
 EDIS
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1332,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
        SPM       #0                    ; [CPU_] 
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$263, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$263, DW_AT_TI_end_line(0x534)
	.dwattr $C$DW$263, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$263

	.sect	".text:_IPCRtoLBlockRead"
	.clink
	.global	_IPCRtoLBlockRead

$C$DW$268	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$268, DW_AT_name("IPCRtoLBlockRead")
	.dwattr $C$DW$268, DW_AT_low_pc(_IPCRtoLBlockRead)
	.dwattr $C$DW$268, DW_AT_high_pc(0x00)
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_IPCRtoLBlockRead")
	.dwattr $C$DW$268, DW_AT_external
	.dwattr $C$DW$268, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$268, DW_AT_TI_begin_line(0x544)
	.dwattr $C$DW$268, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$268, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1349,column 1,is_stmt,address _IPCRtoLBlockRead,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLBlockRead
$C$DW$269	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$269, DW_AT_name("psMessage")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$269, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1348 | IPCRtoLBlockRead(tIpcMessage *psMessage)                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLBlockRead             FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLBlockRead:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$270	.dwtag  DW_TAG_variable
	.dwattr $C$DW$270, DW_AT_name("psMessage")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$270, DW_AT_location[DW_OP_breg20 -2]

$C$DW$271	.dwtag  DW_TAG_variable
	.dwattr $C$DW$271, DW_AT_name("pusRAddress")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_pusRAddress")
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$271, DW_AT_location[DW_OP_breg20 -4]

$C$DW$272	.dwtag  DW_TAG_variable
	.dwattr $C$DW$272, DW_AT_name("pusWAddress")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_pusWAddress")
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$272, DW_AT_location[DW_OP_breg20 -6]

$C$DW$273	.dwtag  DW_TAG_variable
	.dwattr $C$DW$273, DW_AT_name("usLength")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$273, DW_AT_location[DW_OP_breg20 -7]

$C$DW$274	.dwtag  DW_TAG_variable
	.dwattr $C$DW$274, DW_AT_name("usIndex")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_usIndex")
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$274, DW_AT_location[DW_OP_breg20 -8]

;----------------------------------------------------------------------
; 1351 | uint16_t usLength;                                                     
; 1352 | volatile uint16_t* pusRAddress;                                        
; 1353 | volatile uint16_t* pusWAddress;                                        
; 1354 | uint16_t usIndex;                                                      
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1349| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1356,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1356 | pusRAddress = (volatile uint16_t *)psMessage->uladdress;               
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |1356| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1356| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1357,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1357 | pusWAddress = (volatile uint16_t *)psMessage->uldataw2;                
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1357| 
        MOVL      *-SP[6],ACC           ; [CPU_] |1357| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1358,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1358 | usLength = (uint16_t)psMessage->uldataw1;                              
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[4]          ; [CPU_] |1358| 
        MOV       *-SP[7],AL            ; [CPU_] |1358| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1360,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 1360 | for (usIndex=0; usIndex<usLength; usIndex++)                           
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |1360| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1360,column 21,is_stmt,isa 0
        CMP       AL,*-SP[8]            ; [CPU_] |1360| 
        B         $C$L27,LOS            ; [CPU_] |1360| 
        ; branchcc occurs ; [] |1360| 
$C$L26:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1362,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1362 | *pusWAddress = *pusRAddress;                                           
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1362| 
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1362| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1362| 
        MOV       *+XAR5[0],AL          ; [CPU_] |1362| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1363,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1363 | pusWAddress += 1;                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1363| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |1363| 
        MOVL      *-SP[6],ACC           ; [CPU_] |1363| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1364,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1364 | pusRAddress += 1;                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1364| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |1364| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1364| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1360,column 39,is_stmt,isa 0
        INC       *-SP[8]               ; [CPU_] |1360| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1360,column 21,is_stmt,isa 0
        MOV       AL,*-SP[7]            ; [CPU_] |1360| 
        CMP       AL,*-SP[8]            ; [CPU_] |1360| 
        B         $C$L26,HI             ; [CPU_] |1360| 
        ; branchcc occurs ; [] |1360| 
$C$L27:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1367,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1367 | IpcRegs.IPCACK.all |= (psMessage->uldataw1 & 0xFFFF0000);              
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |1367| 
        MOVW      DP,#_IpcRegs          ; [CPU_U] 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |1367| 
        ANDB      AL,#0                 ; [CPU_] |1367| 
        OR        @_IpcRegs,AL          ; [CPU_] |1367| 
        OR        @_IpcRegs+1,AH        ; [CPU_] |1367| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1368,column 1,is_stmt,isa 0
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$275	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$275, DW_AT_low_pc(0x00)
	.dwattr $C$DW$275, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$268, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$268, DW_AT_TI_end_line(0x558)
	.dwattr $C$DW$268, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$268

	.sect	".text:_IPCRtoLBlockWrite"
	.clink
	.global	_IPCRtoLBlockWrite

$C$DW$276	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$276, DW_AT_name("IPCRtoLBlockWrite")
	.dwattr $C$DW$276, DW_AT_low_pc(_IPCRtoLBlockWrite)
	.dwattr $C$DW$276, DW_AT_high_pc(0x00)
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_IPCRtoLBlockWrite")
	.dwattr $C$DW$276, DW_AT_external
	.dwattr $C$DW$276, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$276, DW_AT_TI_begin_line(0x569)
	.dwattr $C$DW$276, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$276, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1386,column 1,is_stmt,address _IPCRtoLBlockWrite,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLBlockWrite
$C$DW$277	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$277, DW_AT_name("psMessage")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$277, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1385 | IPCRtoLBlockWrite(tIpcMessage *psMessage)                              
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLBlockWrite            FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLBlockWrite:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$278	.dwtag  DW_TAG_variable
	.dwattr $C$DW$278, DW_AT_name("psMessage")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$278, DW_AT_location[DW_OP_breg20 -2]

$C$DW$279	.dwtag  DW_TAG_variable
	.dwattr $C$DW$279, DW_AT_name("usLength")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$279, DW_AT_location[DW_OP_breg20 -3]

$C$DW$280	.dwtag  DW_TAG_variable
	.dwattr $C$DW$280, DW_AT_name("usWLength")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_usWLength")
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$280, DW_AT_location[DW_OP_breg20 -4]

$C$DW$281	.dwtag  DW_TAG_variable
	.dwattr $C$DW$281, DW_AT_name("usIndex")
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_usIndex")
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$281, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 1387 | uint16_t usLength;                                                     
; 1388 | uint16_t usWLength;                                                    
; 1389 | uint16_t usIndex;                                                      
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1386| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1391,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1391 | usLength = (uint16_t)psMessage->uldataw1;                              
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[4]          ; [CPU_] |1391| 
        MOV       *-SP[3],AL            ; [CPU_] |1391| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1392,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1392 | usWLength = (uint16_t)((psMessage->uldataw1)>>16);                     
; 1394 | // Determine data word access size to write to data block.             
; 1395 | //                                                                     
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |1392| 
        MOVU      ACC,AH                ; [CPU_] |1392| 
        MOV       *-SP[4],AL            ; [CPU_] |1392| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1396,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1396 | if (usWLength == IPC_LENGTH_16_BITS)                                   
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |1396| 
        B         $C$L29,NEQ            ; [CPU_] |1396| 
        ; branchcc occurs ; [] |1396| 

$C$DW$282	.dwtag  DW_TAG_lexical_block
	.dwattr $C$DW$282, DW_AT_low_pc(0x00)
	.dwattr $C$DW$282, DW_AT_high_pc(0x00)
$C$DW$283	.dwtag  DW_TAG_variable
	.dwattr $C$DW$283, DW_AT_name("pusWAddress")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_pusWAddress")
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$283, DW_AT_location[DW_OP_breg20 -8]

$C$DW$284	.dwtag  DW_TAG_variable
	.dwattr $C$DW$284, DW_AT_name("pusRAddress")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_pusRAddress")
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$284, DW_AT_location[DW_OP_breg20 -10]

	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1398,column 40,is_stmt,isa 0
;----------------------------------------------------------------------
; 1398 | volatile uint16_t *pusWAddress = (volatile uint16_t *)psMessage->uladdr
;     | ess;                                                                   
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |1398| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1398| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1399,column 40,is_stmt,isa 0
;----------------------------------------------------------------------
; 1399 | volatile uint16_t *pusRAddress = (volatile uint16_t *)psMessage->uldata
;     | w2;                                                                    
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1399| 
        MOVL      *-SP[10],ACC          ; [CPU_] |1399| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1400,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1400 | for (usIndex=0; usIndex<usLength; usIndex++)                           
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |1400| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1400,column 25,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1400| 
        CMP       AL,*-SP[5]            ; [CPU_] |1400| 
        B         $C$L32,LOS            ; [CPU_] |1400| 
        ; branchcc occurs ; [] |1400| 
$C$L28:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1402,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1402 | *pusWAddress = *pusRAddress;                                           
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1402| 
        MOVL      XAR5,*-SP[8]          ; [CPU_] |1402| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1402| 
        MOV       *+XAR5[0],AL          ; [CPU_] |1402| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1403,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1403 | pusWAddress += 1;                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1403| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |1403| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1403| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1404,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1404 | pusRAddress += 1;                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1404| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |1404| 
        MOVL      *-SP[10],ACC          ; [CPU_] |1404| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1400,column 43,is_stmt,isa 0
        INC       *-SP[5]               ; [CPU_] |1400| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1400,column 25,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1400| 
        CMP       AL,*-SP[5]            ; [CPU_] |1400| 
        B         $C$L28,HI             ; [CPU_] |1400| 
        ; branchcc occurs ; [] |1400| 
	.dwendtag $C$DW$282

	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1406,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1406 | } else if (usWLength == IPC_LENGTH_32_BITS)                            
;----------------------------------------------------------------------
        B         $C$L32,UNC            ; [CPU_] |1406| 
        ; branch occurs ; [] |1406| 
$C$L29:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1406,column 12,is_stmt,isa 0
        CMPB      AL,#2                 ; [CPU_] |1406| 
        B         $C$L32,NEQ            ; [CPU_] |1406| 
        ; branchcc occurs ; [] |1406| 

$C$DW$285	.dwtag  DW_TAG_lexical_block
	.dwattr $C$DW$285, DW_AT_low_pc(0x00)
	.dwattr $C$DW$285, DW_AT_high_pc(0x00)
$C$DW$286	.dwtag  DW_TAG_variable
	.dwattr $C$DW$286, DW_AT_name("pulWAddress")
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_pulWAddress")
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$286, DW_AT_location[DW_OP_breg20 -8]

$C$DW$287	.dwtag  DW_TAG_variable
	.dwattr $C$DW$287, DW_AT_name("pulRAddress")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_pulRAddress")
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$287, DW_AT_location[DW_OP_breg20 -10]

	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1408,column 45,is_stmt,isa 0
;----------------------------------------------------------------------
; 1408 | volatile unsigned long *pulWAddress =                                  
; 1409 |     (volatile unsigned long *)psMessage->uladdress;                    
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |1408| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1408| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1410,column 45,is_stmt,isa 0
;----------------------------------------------------------------------
; 1410 | volatile unsigned long *pulRAddress =                                  
; 1411 |     (volatile unsigned long *)psMessage->uldataw2;                     
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1410| 
        MOVL      *-SP[10],ACC          ; [CPU_] |1410| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1413,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1413 | for (usIndex=0; usIndex<usLength; usIndex++)                           
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |1413| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1413,column 25,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1413| 
        CMP       AL,*-SP[5]            ; [CPU_] |1413| 
        B         $C$L31,LOS            ; [CPU_] |1413| 
        ; branchcc occurs ; [] |1413| 
$C$L30:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1415,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1415 | *pulWAddress = *pulRAddress;                                           
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1415| 
        MOVL      XAR5,*-SP[8]          ; [CPU_] |1415| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1415| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |1415| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1416,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1416 | pulWAddress += 1;                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#2                ; [CPU_] |1416| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |1416| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1416| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1417,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1417 | pulRAddress += 1;                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#2                ; [CPU_] |1417| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |1417| 
        MOVL      *-SP[10],ACC          ; [CPU_] |1417| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1413,column 43,is_stmt,isa 0
        INC       *-SP[5]               ; [CPU_] |1413| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1413,column 25,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1413| 
        CMP       AL,*-SP[5]            ; [CPU_] |1413| 
        B         $C$L30,HI             ; [CPU_] |1413| 
        ; branchcc occurs ; [] |1413| 
$C$L31:    
	.dwendtag $C$DW$285

	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1420,column 1,is_stmt,isa 0
$C$L32:    
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$288	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$288, DW_AT_low_pc(0x00)
	.dwattr $C$DW$288, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$276, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$276, DW_AT_TI_end_line(0x58c)
	.dwattr $C$DW$276, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$276

	.sect	".text:_IPCRtoLBlockWrite_Protected"
	.clink
	.global	_IPCRtoLBlockWrite_Protected

$C$DW$289	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$289, DW_AT_name("IPCRtoLBlockWrite_Protected")
	.dwattr $C$DW$289, DW_AT_low_pc(_IPCRtoLBlockWrite_Protected)
	.dwattr $C$DW$289, DW_AT_high_pc(0x00)
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_IPCRtoLBlockWrite_Protected")
	.dwattr $C$DW$289, DW_AT_external
	.dwattr $C$DW$289, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$289, DW_AT_TI_begin_line(0x59e)
	.dwattr $C$DW$289, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$289, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1439,column 1,is_stmt,address _IPCRtoLBlockWrite_Protected,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLBlockWrite_Protected
$C$DW$290	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$290, DW_AT_name("psMessage")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$290, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1438 | IPCRtoLBlockWrite_Protected(tIpcMessage *psMessage)                    
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLBlockWrite_Protected  FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLBlockWrite_Protected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$291	.dwtag  DW_TAG_variable
	.dwattr $C$DW$291, DW_AT_name("psMessage")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$291, DW_AT_location[DW_OP_breg20 -2]

$C$DW$292	.dwtag  DW_TAG_variable
	.dwattr $C$DW$292, DW_AT_name("usLength")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_usLength")
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$292, DW_AT_location[DW_OP_breg20 -3]

$C$DW$293	.dwtag  DW_TAG_variable
	.dwattr $C$DW$293, DW_AT_name("usWLength")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_usWLength")
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$293, DW_AT_location[DW_OP_breg20 -4]

$C$DW$294	.dwtag  DW_TAG_variable
	.dwattr $C$DW$294, DW_AT_name("usIndex")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_usIndex")
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$294, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 1440 | uint16_t usLength;                                                     
; 1441 | uint16_t usWLength;                                                    
; 1442 | uint16_t usIndex;                                                      
; 1444 | // Allow access to EALLOW-protected registers.                         
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1439| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1445,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1445 | EALLOW;                                                                
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1447,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1447 | usLength = (uint16_t)psMessage->uldataw1;                              
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[4]          ; [CPU_] |1447| 
        MOV       *-SP[3],AL            ; [CPU_] |1447| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1448,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1448 | usWLength = (uint16_t)((psMessage->uldataw1)>>16);                     
; 1450 | // Determine data word access size to write to data block.             
; 1451 | // (Writes registers accessible via APB bus must be 32-bits wide)      
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |1448| 
        MOVU      ACC,AH                ; [CPU_] |1448| 
        MOV       *-SP[4],AL            ; [CPU_] |1448| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1452,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1452 | if (usWLength == IPC_LENGTH_16_BITS)                                   
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |1452| 
        B         $C$L34,NEQ            ; [CPU_] |1452| 
        ; branchcc occurs ; [] |1452| 

$C$DW$295	.dwtag  DW_TAG_lexical_block
	.dwattr $C$DW$295, DW_AT_low_pc(0x00)
	.dwattr $C$DW$295, DW_AT_high_pc(0x00)
$C$DW$296	.dwtag  DW_TAG_variable
	.dwattr $C$DW$296, DW_AT_name("pusWAddress")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_pusWAddress")
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$296, DW_AT_location[DW_OP_breg20 -8]

$C$DW$297	.dwtag  DW_TAG_variable
	.dwattr $C$DW$297, DW_AT_name("pusRAddress")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_pusRAddress")
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$297, DW_AT_location[DW_OP_breg20 -10]

	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1454,column 40,is_stmt,isa 0
;----------------------------------------------------------------------
; 1454 | volatile uint16_t *pusWAddress = (volatile uint16_t *)psMessage->uladdr
;     | ess;                                                                   
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |1454| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1454| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1455,column 40,is_stmt,isa 0
;----------------------------------------------------------------------
; 1455 | volatile uint16_t *pusRAddress = (volatile uint16_t *)psMessage->uldata
;     | w2;                                                                    
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1455| 
        MOVL      *-SP[10],ACC          ; [CPU_] |1455| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1456,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1456 | for (usIndex=0; usIndex<usLength; usIndex++)                           
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |1456| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1456,column 25,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1456| 
        CMP       AL,*-SP[5]            ; [CPU_] |1456| 
        B         $C$L37,LOS            ; [CPU_] |1456| 
        ; branchcc occurs ; [] |1456| 
$C$L33:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1458,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1458 | *pusWAddress = *pusRAddress;                                           
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1458| 
        MOVL      XAR5,*-SP[8]          ; [CPU_] |1458| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1458| 
        MOV       *+XAR5[0],AL          ; [CPU_] |1458| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1459,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1459 | pusWAddress += 1;                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1459| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |1459| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1459| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1460,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1460 | pusRAddress += 1;                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1460| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |1460| 
        MOVL      *-SP[10],ACC          ; [CPU_] |1460| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1456,column 43,is_stmt,isa 0
        INC       *-SP[5]               ; [CPU_] |1456| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1456,column 25,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1456| 
        CMP       AL,*-SP[5]            ; [CPU_] |1456| 
        B         $C$L33,HI             ; [CPU_] |1456| 
        ; branchcc occurs ; [] |1456| 
	.dwendtag $C$DW$295

	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1462,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1462 | } else if (usWLength == IPC_LENGTH_32_BITS)                            
;----------------------------------------------------------------------
        B         $C$L37,UNC            ; [CPU_] |1462| 
        ; branch occurs ; [] |1462| 
$C$L34:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1462,column 12,is_stmt,isa 0
        CMPB      AL,#2                 ; [CPU_] |1462| 
        B         $C$L37,NEQ            ; [CPU_] |1462| 
        ; branchcc occurs ; [] |1462| 

$C$DW$298	.dwtag  DW_TAG_lexical_block
	.dwattr $C$DW$298, DW_AT_low_pc(0x00)
	.dwattr $C$DW$298, DW_AT_high_pc(0x00)
$C$DW$299	.dwtag  DW_TAG_variable
	.dwattr $C$DW$299, DW_AT_name("pulWAddress")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_pulWAddress")
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$299, DW_AT_location[DW_OP_breg20 -8]

$C$DW$300	.dwtag  DW_TAG_variable
	.dwattr $C$DW$300, DW_AT_name("pulRAddress")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_pulRAddress")
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$300, DW_AT_location[DW_OP_breg20 -10]

	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1464,column 45,is_stmt,isa 0
;----------------------------------------------------------------------
; 1464 | volatile unsigned long *pulWAddress =                                  
; 1465 |     (volatile unsigned long *)psMessage->uladdress;                    
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |1464| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1464| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1466,column 45,is_stmt,isa 0
;----------------------------------------------------------------------
; 1466 | volatile unsigned long *pulRAddress =                                  
; 1467 |     (volatile unsigned long *)psMessage->uldataw2;                     
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |1466| 
        MOVL      *-SP[10],ACC          ; [CPU_] |1466| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1469,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1469 | for (usIndex=0; usIndex<usLength; usIndex++)                           
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |1469| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1469,column 25,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1469| 
        CMP       AL,*-SP[5]            ; [CPU_] |1469| 
        B         $C$L36,LOS            ; [CPU_] |1469| 
        ; branchcc occurs ; [] |1469| 
$C$L35:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1471,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1471 | *pulWAddress = *pulRAddress;                                           
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[10]         ; [CPU_] |1471| 
        MOVL      XAR5,*-SP[8]          ; [CPU_] |1471| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1471| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |1471| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1472,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1472 | pulWAddress += 1;                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#2                ; [CPU_] |1472| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |1472| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1472| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1473,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1473 | pulRAddress += 1;                                                      
; 1477 | // Disable access to EALLOW-protected registers.                       
;----------------------------------------------------------------------
        MOVB      ACC,#2                ; [CPU_] |1473| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |1473| 
        MOVL      *-SP[10],ACC          ; [CPU_] |1473| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1469,column 43,is_stmt,isa 0
        INC       *-SP[5]               ; [CPU_] |1469| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1469,column 25,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1469| 
        CMP       AL,*-SP[5]            ; [CPU_] |1469| 
        B         $C$L35,HI             ; [CPU_] |1469| 
        ; branchcc occurs ; [] |1469| 
$C$L36:    
	.dwendtag $C$DW$298

$C$L37:    
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1478,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1478 | EDIS;                                                                  
;----------------------------------------------------------------------
 EDIS
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1479,column 1,is_stmt,isa 0
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
        SPM       #0                    ; [CPU_] 
$C$DW$301	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$301, DW_AT_low_pc(0x00)
	.dwattr $C$DW$301, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$289, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$289, DW_AT_TI_end_line(0x5c7)
	.dwattr $C$DW$289, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$289

	.sect	".text:_IPCRtoLFunctionCall"
	.clink
	.global	_IPCRtoLFunctionCall

$C$DW$302	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$302, DW_AT_name("IPCRtoLFunctionCall")
	.dwattr $C$DW$302, DW_AT_low_pc(_IPCRtoLFunctionCall)
	.dwattr $C$DW$302, DW_AT_high_pc(0x00)
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_IPCRtoLFunctionCall")
	.dwattr $C$DW$302, DW_AT_external
	.dwattr $C$DW$302, DW_AT_TI_begin_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$302, DW_AT_TI_begin_line(0x5d6)
	.dwattr $C$DW$302, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$302, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1495,column 1,is_stmt,address _IPCRtoLFunctionCall,isa 0

	.dwfde $C$DW$CIE, _IPCRtoLFunctionCall
$C$DW$303	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$303, DW_AT_name("psMessage")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$303, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1494 | IPCRtoLFunctionCall(tIpcMessage *psMessage)                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCRtoLFunctionCall          FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_IPCRtoLFunctionCall:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$304	.dwtag  DW_TAG_variable
	.dwattr $C$DW$304, DW_AT_name("psMessage")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_psMessage")
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$304, DW_AT_location[DW_OP_breg20 -2]

$C$DW$305	.dwtag  DW_TAG_variable
	.dwattr $C$DW$305, DW_AT_name("func_call")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_func_call")
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$305, DW_AT_location[DW_OP_breg20 -4]

;----------------------------------------------------------------------
; 1496 | // Executes function call with parameter at given address.             
; 1497 | //                                                                     
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1495| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1498,column 29,is_stmt,isa 0
;----------------------------------------------------------------------
; 1498 | tfIpcFuncCall func_call = (tfIpcFuncCall)psMessage->uladdress;         
;----------------------------------------------------------------------
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |1498| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1498| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1499,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1499 | func_call(psMessage->uldataw1);                                        
;----------------------------------------------------------------------
        MOVL      XAR7,*-SP[4]          ; [CPU_] |1499| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |1499| 
$C$DW$306	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$306, DW_AT_low_pc(0x00)
	.dwattr $C$DW$306, DW_AT_TI_call
	.dwattr $C$DW$306, DW_AT_TI_indirect

        LCR       *XAR7                 ; [CPU_] |1499| 
        ; call occurs [XAR7] ; [] |1499| 
	.dwpsn	file "../device/F2837xD_Ipc_Driver.c",line 1500,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$307	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$307, DW_AT_low_pc(0x00)
	.dwattr $C$DW$307, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$302, DW_AT_TI_end_file("../device/F2837xD_Ipc_Driver.c")
	.dwattr $C$DW$302, DW_AT_TI_end_line(0x5dc)
	.dwattr $C$DW$302, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$302

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_IpcRegs

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x08)
$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$308, DW_AT_name("ulcommand")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_ulcommand")
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$309, DW_AT_name("uladdress")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_uladdress")
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$310, DW_AT_name("uldataw1")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_uldataw1")
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$311, DW_AT_name("uldataw2")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_uldataw2")
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$20

$C$DW$T$21	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$21, DW_AT_name("tIpcMessage")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_address_class(0x20)


$C$DW$T$41	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$41, DW_AT_byte_size(0x20)
$C$DW$312	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$312, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$41


$C$DW$T$42	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x80)
$C$DW$313	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$313, DW_AT_upper_bound(0x03)

$C$DW$314	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$314, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$42


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x0e)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$315, DW_AT_name("psPutBuffer")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_psPutBuffer")
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$316, DW_AT_name("ulPutFlag")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_ulPutFlag")
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$317, DW_AT_name("pusPutWriteIndex")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_pusPutWriteIndex")
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$318, DW_AT_name("pusPutReadIndex")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_pusPutReadIndex")
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$319, DW_AT_name("psGetBuffer")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_psGetBuffer")
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$320, DW_AT_name("pusGetWriteIndex")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_pusGetWriteIndex")
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$321, DW_AT_name("pusGetReadIndex")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_pusGetReadIndex")
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$25

$C$DW$T$44	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$44, DW_AT_name("tIpcController")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)

$C$DW$322	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$44)

$C$DW$T$45	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$322)

$C$DW$T$46	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$46, DW_AT_address_class(0x20)


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_name("IPCACK_BITS")
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x02)
$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$323, DW_AT_name("IPC0")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$323, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$323, DW_AT_bit_size(0x01)
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$324, DW_AT_name("IPC1")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$324, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$324, DW_AT_bit_size(0x01)
	.dwattr $C$DW$324, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$325, DW_AT_name("IPC2")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$325, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$325, DW_AT_bit_size(0x01)
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$326, DW_AT_name("IPC3")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$326, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$326, DW_AT_bit_size(0x01)
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$327, DW_AT_name("IPC4")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$327, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$327, DW_AT_bit_size(0x01)
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$328, DW_AT_name("IPC5")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$328, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$328, DW_AT_bit_size(0x01)
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$329, DW_AT_name("IPC6")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$329, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$329, DW_AT_bit_size(0x01)
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$330, DW_AT_name("IPC7")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$330, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$330, DW_AT_bit_size(0x01)
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$331, DW_AT_name("IPC8")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$331, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$331, DW_AT_bit_size(0x01)
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$332, DW_AT_name("IPC9")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$332, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$332, DW_AT_bit_size(0x01)
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$333, DW_AT_name("IPC10")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$333, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$333, DW_AT_bit_size(0x01)
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$334, DW_AT_name("IPC11")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$334, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$334, DW_AT_bit_size(0x01)
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$335, DW_AT_name("IPC12")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$335, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$335, DW_AT_bit_size(0x01)
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$336, DW_AT_name("IPC13")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$336, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$336, DW_AT_bit_size(0x01)
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$337, DW_AT_name("IPC14")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$337, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$337, DW_AT_bit_size(0x01)
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$338, DW_AT_name("IPC15")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$338, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$338, DW_AT_bit_size(0x01)
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$339, DW_AT_name("IPC16")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$339, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$339, DW_AT_bit_size(0x01)
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$340, DW_AT_name("IPC17")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$340, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$340, DW_AT_bit_size(0x01)
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$341, DW_AT_name("IPC18")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$341, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$341, DW_AT_bit_size(0x01)
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$342, DW_AT_name("IPC19")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$342, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$342, DW_AT_bit_size(0x01)
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$343, DW_AT_name("IPC20")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$343, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$343, DW_AT_bit_size(0x01)
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$344, DW_AT_name("IPC21")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$344, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$344, DW_AT_bit_size(0x01)
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$345, DW_AT_name("IPC22")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$345, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$345, DW_AT_bit_size(0x01)
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$346, DW_AT_name("IPC23")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$346, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$346, DW_AT_bit_size(0x01)
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$347, DW_AT_name("IPC24")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$347, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$347, DW_AT_bit_size(0x01)
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$348, DW_AT_name("IPC25")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$348, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$348, DW_AT_bit_size(0x01)
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$349, DW_AT_name("IPC26")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$349, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$349, DW_AT_bit_size(0x01)
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$350, DW_AT_name("IPC27")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$350, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$350, DW_AT_bit_size(0x01)
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$351, DW_AT_name("IPC28")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$351, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$351, DW_AT_bit_size(0x01)
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$352, DW_AT_name("IPC29")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$352, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$352, DW_AT_bit_size(0x01)
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$353, DW_AT_name("IPC30")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$353, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$353, DW_AT_bit_size(0x01)
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$354, DW_AT_name("IPC31")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$354, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$354, DW_AT_bit_size(0x01)
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$27


$C$DW$T$29	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$29, DW_AT_name("IPCACK_REG")
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x02)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$355, DW_AT_name("all")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$356, DW_AT_name("bit")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_name("IPCCLR_BITS")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x02)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$357, DW_AT_name("IPC0")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$357, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$357, DW_AT_bit_size(0x01)
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$358, DW_AT_name("IPC1")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$358, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$358, DW_AT_bit_size(0x01)
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$359, DW_AT_name("IPC2")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$359, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$359, DW_AT_bit_size(0x01)
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$360, DW_AT_name("IPC3")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$360, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$360, DW_AT_bit_size(0x01)
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$361, DW_AT_name("IPC4")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$361, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$361, DW_AT_bit_size(0x01)
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$362, DW_AT_name("IPC5")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$362, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$362, DW_AT_bit_size(0x01)
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$363, DW_AT_name("IPC6")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$363, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$363, DW_AT_bit_size(0x01)
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$364, DW_AT_name("IPC7")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$364, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$364, DW_AT_bit_size(0x01)
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$365, DW_AT_name("IPC8")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$365, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$365, DW_AT_bit_size(0x01)
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$366, DW_AT_name("IPC9")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$366, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$366, DW_AT_bit_size(0x01)
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$367, DW_AT_name("IPC10")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$367, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$367, DW_AT_bit_size(0x01)
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$368, DW_AT_name("IPC11")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$368, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$368, DW_AT_bit_size(0x01)
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$369, DW_AT_name("IPC12")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$369, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$369, DW_AT_bit_size(0x01)
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$370, DW_AT_name("IPC13")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$370, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$370, DW_AT_bit_size(0x01)
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$371, DW_AT_name("IPC14")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$371, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$371, DW_AT_bit_size(0x01)
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$372, DW_AT_name("IPC15")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$372, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$372, DW_AT_bit_size(0x01)
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$373, DW_AT_name("IPC16")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$373, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$373, DW_AT_bit_size(0x01)
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$374, DW_AT_name("IPC17")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$374, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$374, DW_AT_bit_size(0x01)
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$375, DW_AT_name("IPC18")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$375, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$375, DW_AT_bit_size(0x01)
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$376	.dwtag  DW_TAG_member
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$376, DW_AT_name("IPC19")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$376, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$376, DW_AT_bit_size(0x01)
	.dwattr $C$DW$376, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$377, DW_AT_name("IPC20")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$377, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$377, DW_AT_bit_size(0x01)
	.dwattr $C$DW$377, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$378, DW_AT_name("IPC21")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$378, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$378, DW_AT_bit_size(0x01)
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$379, DW_AT_name("IPC22")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$379, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$379, DW_AT_bit_size(0x01)
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$380, DW_AT_name("IPC23")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$380, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$380, DW_AT_bit_size(0x01)
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$381, DW_AT_name("IPC24")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$381, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$381, DW_AT_bit_size(0x01)
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$382, DW_AT_name("IPC25")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$382, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$382, DW_AT_bit_size(0x01)
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$383, DW_AT_name("IPC26")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$383, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$383, DW_AT_bit_size(0x01)
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$384, DW_AT_name("IPC27")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$384, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$384, DW_AT_bit_size(0x01)
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$385	.dwtag  DW_TAG_member
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$385, DW_AT_name("IPC28")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$385, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$385, DW_AT_bit_size(0x01)
	.dwattr $C$DW$385, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$385, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$386	.dwtag  DW_TAG_member
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$386, DW_AT_name("IPC29")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$386, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$386, DW_AT_bit_size(0x01)
	.dwattr $C$DW$386, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$386, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$387	.dwtag  DW_TAG_member
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$387, DW_AT_name("IPC30")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$387, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$387, DW_AT_bit_size(0x01)
	.dwattr $C$DW$387, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$387, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$388, DW_AT_name("IPC31")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$388, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$388, DW_AT_bit_size(0x01)
	.dwattr $C$DW$388, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$31, DW_AT_name("IPCCLR_REG")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x02)
$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$389, DW_AT_name("all")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$390, DW_AT_name("bit")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$32, DW_AT_name("IPCFLG_BITS")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x02)
$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$391, DW_AT_name("IPC0")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$391, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$391, DW_AT_bit_size(0x01)
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$392, DW_AT_name("IPC1")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$392, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$392, DW_AT_bit_size(0x01)
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$393, DW_AT_name("IPC2")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$393, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$393, DW_AT_bit_size(0x01)
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$394, DW_AT_name("IPC3")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$394, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$394, DW_AT_bit_size(0x01)
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$395, DW_AT_name("IPC4")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$395, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$395, DW_AT_bit_size(0x01)
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$396, DW_AT_name("IPC5")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$396, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$396, DW_AT_bit_size(0x01)
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$397, DW_AT_name("IPC6")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$397, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$397, DW_AT_bit_size(0x01)
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$398, DW_AT_name("IPC7")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$398, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$398, DW_AT_bit_size(0x01)
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$399, DW_AT_name("IPC8")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$399, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$399, DW_AT_bit_size(0x01)
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$400, DW_AT_name("IPC9")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$400, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$400, DW_AT_bit_size(0x01)
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$401, DW_AT_name("IPC10")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$401, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$401, DW_AT_bit_size(0x01)
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$402, DW_AT_name("IPC11")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$402, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$402, DW_AT_bit_size(0x01)
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$403, DW_AT_name("IPC12")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$403, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$403, DW_AT_bit_size(0x01)
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$404, DW_AT_name("IPC13")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$404, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$404, DW_AT_bit_size(0x01)
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$405, DW_AT_name("IPC14")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$405, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$405, DW_AT_bit_size(0x01)
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$406, DW_AT_name("IPC15")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$406, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$406, DW_AT_bit_size(0x01)
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$407, DW_AT_name("IPC16")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$407, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$407, DW_AT_bit_size(0x01)
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$408, DW_AT_name("IPC17")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$408, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$408, DW_AT_bit_size(0x01)
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$409	.dwtag  DW_TAG_member
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$409, DW_AT_name("IPC18")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$409, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$409, DW_AT_bit_size(0x01)
	.dwattr $C$DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$409, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$410, DW_AT_name("IPC19")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$410, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$410, DW_AT_bit_size(0x01)
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$411, DW_AT_name("IPC20")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$411, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$411, DW_AT_bit_size(0x01)
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$412, DW_AT_name("IPC21")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$412, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$412, DW_AT_bit_size(0x01)
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$413, DW_AT_name("IPC22")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$413, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$413, DW_AT_bit_size(0x01)
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$414, DW_AT_name("IPC23")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$414, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$414, DW_AT_bit_size(0x01)
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$415, DW_AT_name("IPC24")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$415, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$415, DW_AT_bit_size(0x01)
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$416, DW_AT_name("IPC25")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$416, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$416, DW_AT_bit_size(0x01)
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$417, DW_AT_name("IPC26")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$417, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$417, DW_AT_bit_size(0x01)
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$418, DW_AT_name("IPC27")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$418, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$418, DW_AT_bit_size(0x01)
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$419, DW_AT_name("IPC28")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$419, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$419, DW_AT_bit_size(0x01)
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$420	.dwtag  DW_TAG_member
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$420, DW_AT_name("IPC29")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$420, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$420, DW_AT_bit_size(0x01)
	.dwattr $C$DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$421, DW_AT_name("IPC30")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$421, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$421, DW_AT_bit_size(0x01)
	.dwattr $C$DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$422, DW_AT_name("IPC31")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$422, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$422, DW_AT_bit_size(0x01)
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$33, DW_AT_name("IPCFLG_REG")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x02)
$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$423, DW_AT_name("all")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$423, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$424, DW_AT_name("bit")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$34, DW_AT_name("IPCSET_BITS")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x02)
$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$425, DW_AT_name("IPC0")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$425, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$425, DW_AT_bit_size(0x01)
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$426, DW_AT_name("IPC1")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$426, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$426, DW_AT_bit_size(0x01)
	.dwattr $C$DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$427, DW_AT_name("IPC2")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$427, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$427, DW_AT_bit_size(0x01)
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$428, DW_AT_name("IPC3")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$428, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$428, DW_AT_bit_size(0x01)
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$429, DW_AT_name("IPC4")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$429, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$429, DW_AT_bit_size(0x01)
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$430, DW_AT_name("IPC5")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$430, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$430, DW_AT_bit_size(0x01)
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$431	.dwtag  DW_TAG_member
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$431, DW_AT_name("IPC6")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$431, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$431, DW_AT_bit_size(0x01)
	.dwattr $C$DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$432, DW_AT_name("IPC7")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$432, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$432, DW_AT_bit_size(0x01)
	.dwattr $C$DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$433, DW_AT_name("IPC8")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$433, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$433, DW_AT_bit_size(0x01)
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$434, DW_AT_name("IPC9")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$434, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$434, DW_AT_bit_size(0x01)
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$435	.dwtag  DW_TAG_member
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$435, DW_AT_name("IPC10")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$435, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$435, DW_AT_bit_size(0x01)
	.dwattr $C$DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$436, DW_AT_name("IPC11")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$436, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$436, DW_AT_bit_size(0x01)
	.dwattr $C$DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$437, DW_AT_name("IPC12")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$437, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$437, DW_AT_bit_size(0x01)
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$438, DW_AT_name("IPC13")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$438, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$438, DW_AT_bit_size(0x01)
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$439, DW_AT_name("IPC14")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$439, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$439, DW_AT_bit_size(0x01)
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$440, DW_AT_name("IPC15")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$440, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$440, DW_AT_bit_size(0x01)
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$441, DW_AT_name("IPC16")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$441, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$441, DW_AT_bit_size(0x01)
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$442, DW_AT_name("IPC17")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$442, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$442, DW_AT_bit_size(0x01)
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$443, DW_AT_name("IPC18")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$443, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$443, DW_AT_bit_size(0x01)
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$444, DW_AT_name("IPC19")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$444, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$444, DW_AT_bit_size(0x01)
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$445, DW_AT_name("IPC20")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$445, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$445, DW_AT_bit_size(0x01)
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$446, DW_AT_name("IPC21")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$446, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$446, DW_AT_bit_size(0x01)
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$447, DW_AT_name("IPC22")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$447, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$447, DW_AT_bit_size(0x01)
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$448, DW_AT_name("IPC23")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$448, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$448, DW_AT_bit_size(0x01)
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$449, DW_AT_name("IPC24")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$449, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$449, DW_AT_bit_size(0x01)
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$450, DW_AT_name("IPC25")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$450, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$450, DW_AT_bit_size(0x01)
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$451, DW_AT_name("IPC26")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$451, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$451, DW_AT_bit_size(0x01)
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$452, DW_AT_name("IPC27")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$452, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$452, DW_AT_bit_size(0x01)
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$453, DW_AT_name("IPC28")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$453, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$453, DW_AT_bit_size(0x01)
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$454, DW_AT_name("IPC29")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$454, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$454, DW_AT_bit_size(0x01)
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$455, DW_AT_name("IPC30")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$455, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$455, DW_AT_bit_size(0x01)
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$456, DW_AT_name("IPC31")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$456, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$456, DW_AT_bit_size(0x01)
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$35, DW_AT_name("IPCSET_REG")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x02)
$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$457, DW_AT_name("all")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$458, DW_AT_name("bit")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$35


$C$DW$T$36	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$36, DW_AT_name("IPCSTS_BITS")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$459, DW_AT_name("IPC0")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$459, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$459, DW_AT_bit_size(0x01)
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$460, DW_AT_name("IPC1")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$460, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$460, DW_AT_bit_size(0x01)
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$461, DW_AT_name("IPC2")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$461, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$461, DW_AT_bit_size(0x01)
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$462, DW_AT_name("IPC3")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$462, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$462, DW_AT_bit_size(0x01)
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$463, DW_AT_name("IPC4")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$463, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$463, DW_AT_bit_size(0x01)
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$464, DW_AT_name("IPC5")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$464, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$464, DW_AT_bit_size(0x01)
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$465, DW_AT_name("IPC6")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$465, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$465, DW_AT_bit_size(0x01)
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$466, DW_AT_name("IPC7")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$466, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$466, DW_AT_bit_size(0x01)
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$467, DW_AT_name("IPC8")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$467, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$467, DW_AT_bit_size(0x01)
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$468, DW_AT_name("IPC9")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$468, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$468, DW_AT_bit_size(0x01)
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$469, DW_AT_name("IPC10")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$469, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$469, DW_AT_bit_size(0x01)
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$470, DW_AT_name("IPC11")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$470, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$470, DW_AT_bit_size(0x01)
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$471, DW_AT_name("IPC12")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$471, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$471, DW_AT_bit_size(0x01)
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$472, DW_AT_name("IPC13")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$472, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$472, DW_AT_bit_size(0x01)
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$473, DW_AT_name("IPC14")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$473, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$473, DW_AT_bit_size(0x01)
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$474, DW_AT_name("IPC15")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$474, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$474, DW_AT_bit_size(0x01)
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$475, DW_AT_name("IPC16")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$475, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$475, DW_AT_bit_size(0x01)
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$476, DW_AT_name("IPC17")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$476, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$476, DW_AT_bit_size(0x01)
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$477, DW_AT_name("IPC18")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$477, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$477, DW_AT_bit_size(0x01)
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$478	.dwtag  DW_TAG_member
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$478, DW_AT_name("IPC19")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$478, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$478, DW_AT_bit_size(0x01)
	.dwattr $C$DW$478, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$479, DW_AT_name("IPC20")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$479, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$479, DW_AT_bit_size(0x01)
	.dwattr $C$DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$480, DW_AT_name("IPC21")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$480, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$480, DW_AT_bit_size(0x01)
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$481, DW_AT_name("IPC22")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$481, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$481, DW_AT_bit_size(0x01)
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$482, DW_AT_name("IPC23")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$482, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$482, DW_AT_bit_size(0x01)
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$483, DW_AT_name("IPC24")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$483, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$483, DW_AT_bit_size(0x01)
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$484, DW_AT_name("IPC25")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$484, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$484, DW_AT_bit_size(0x01)
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$485, DW_AT_name("IPC26")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$485, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$485, DW_AT_bit_size(0x01)
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$486, DW_AT_name("IPC27")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$486, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$486, DW_AT_bit_size(0x01)
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$487, DW_AT_name("IPC28")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$487, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$487, DW_AT_bit_size(0x01)
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$488	.dwtag  DW_TAG_member
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$488, DW_AT_name("IPC29")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$488, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$488, DW_AT_bit_size(0x01)
	.dwattr $C$DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$489	.dwtag  DW_TAG_member
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$489, DW_AT_name("IPC30")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$489, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$489, DW_AT_bit_size(0x01)
	.dwattr $C$DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$489, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$490, DW_AT_name("IPC31")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$490, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$490, DW_AT_bit_size(0x01)
	.dwattr $C$DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$36


$C$DW$T$37	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$37, DW_AT_name("IPCSTS_REG")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x02)
$C$DW$491	.dwtag  DW_TAG_member
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$491, DW_AT_name("all")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$491, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$492	.dwtag  DW_TAG_member
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$492, DW_AT_name("bit")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$492, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$37


$C$DW$T$39	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$39, DW_AT_name("IPC_REGS_CPU1")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x24)
$C$DW$493	.dwtag  DW_TAG_member
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$493, DW_AT_name("IPCACK")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_IPCACK")
	.dwattr $C$DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$493, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$494	.dwtag  DW_TAG_member
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$494, DW_AT_name("IPCSTS")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_IPCSTS")
	.dwattr $C$DW$494, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$494, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$495, DW_AT_name("IPCSET")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_IPCSET")
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$496, DW_AT_name("IPCCLR")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_IPCCLR")
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$497, DW_AT_name("IPCFLG")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_IPCFLG")
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$498, DW_AT_name("rsvd1")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$499, DW_AT_name("IPCCOUNTERL")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_IPCCOUNTERL")
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$500, DW_AT_name("IPCCOUNTERH")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_IPCCOUNTERH")
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$501, DW_AT_name("IPCSENDCOM")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_IPCSENDCOM")
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$502, DW_AT_name("IPCSENDADDR")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_IPCSENDADDR")
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$503, DW_AT_name("IPCSENDDATA")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_IPCSENDDATA")
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$504, DW_AT_name("IPCREMOTEREPLY")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_IPCREMOTEREPLY")
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$505, DW_AT_name("IPCRECVCOM")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_IPCRECVCOM")
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$506, DW_AT_name("IPCRECVADDR")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_IPCRECVADDR")
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$507, DW_AT_name("IPCRECVDATA")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_IPCRECVDATA")
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$508, DW_AT_name("IPCLOCALREPLY")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_IPCLOCALREPLY")
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$509, DW_AT_name("IPCBOOTSTS")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_IPCBOOTSTS")
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$510, DW_AT_name("IPCBOOTMODE")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_IPCBOOTMODE")
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$39

$C$DW$511	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$39)

$C$DW$T$50	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$511)

$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_type(*$C$DW$T$2)
	.dwattr $C$DW$T$3, DW_AT_address_class(0x20)

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("Uint16")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)


$C$DW$T$38	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x02)
$C$DW$512	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$512, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$38

$C$DW$T$23	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$23, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)

$C$DW$T$24	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_address_class(0x20)


$C$DW$T$64	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x04)
$C$DW$513	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$513, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$64

$C$DW$514	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$23)

$C$DW$T$65	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$514)

$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x20)

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$28	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$28, DW_AT_name("Uint32")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)

$C$DW$515	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$13)

$C$DW$T$69	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$515)

$C$DW$T$70	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$70, DW_AT_address_class(0x20)

$C$DW$T$19	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$19, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)


$C$DW$T$72	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)
$C$DW$516	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$19)

	.dwendtag $C$DW$T$72

$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x20)

$C$DW$T$74	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$74, DW_AT_name("tfIpcFuncCall")
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$74, DW_AT_language(DW_LANG_C)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$517	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$517, DW_AT_name("AL")
	.dwattr $C$DW$517, DW_AT_location[DW_OP_reg0]

$C$DW$518	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$518, DW_AT_name("AH")
	.dwattr $C$DW$518, DW_AT_location[DW_OP_reg1]

$C$DW$519	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$519, DW_AT_name("PL")
	.dwattr $C$DW$519, DW_AT_location[DW_OP_reg2]

$C$DW$520	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$520, DW_AT_name("PH")
	.dwattr $C$DW$520, DW_AT_location[DW_OP_reg3]

$C$DW$521	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$521, DW_AT_name("SP")
	.dwattr $C$DW$521, DW_AT_location[DW_OP_reg20]

$C$DW$522	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$522, DW_AT_name("XT")
	.dwattr $C$DW$522, DW_AT_location[DW_OP_reg21]

$C$DW$523	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$523, DW_AT_name("T")
	.dwattr $C$DW$523, DW_AT_location[DW_OP_reg22]

$C$DW$524	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$524, DW_AT_name("ST0")
	.dwattr $C$DW$524, DW_AT_location[DW_OP_reg23]

$C$DW$525	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$525, DW_AT_name("ST1")
	.dwattr $C$DW$525, DW_AT_location[DW_OP_reg24]

$C$DW$526	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$526, DW_AT_name("PC")
	.dwattr $C$DW$526, DW_AT_location[DW_OP_reg25]

$C$DW$527	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$527, DW_AT_name("RPC")
	.dwattr $C$DW$527, DW_AT_location[DW_OP_reg26]

$C$DW$528	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$528, DW_AT_name("FP")
	.dwattr $C$DW$528, DW_AT_location[DW_OP_reg28]

$C$DW$529	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$529, DW_AT_name("DP")
	.dwattr $C$DW$529, DW_AT_location[DW_OP_reg29]

$C$DW$530	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$530, DW_AT_name("SXM")
	.dwattr $C$DW$530, DW_AT_location[DW_OP_reg30]

$C$DW$531	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$531, DW_AT_name("PM")
	.dwattr $C$DW$531, DW_AT_location[DW_OP_reg31]

$C$DW$532	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$532, DW_AT_name("OVM")
	.dwattr $C$DW$532, DW_AT_location[DW_OP_regx 0x20]

$C$DW$533	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$533, DW_AT_name("PAGE0")
	.dwattr $C$DW$533, DW_AT_location[DW_OP_regx 0x21]

$C$DW$534	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$534, DW_AT_name("AMODE")
	.dwattr $C$DW$534, DW_AT_location[DW_OP_regx 0x22]

$C$DW$535	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$535, DW_AT_name("INTM")
	.dwattr $C$DW$535, DW_AT_location[DW_OP_regx 0x23]

$C$DW$536	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$536, DW_AT_name("IFR")
	.dwattr $C$DW$536, DW_AT_location[DW_OP_regx 0x24]

$C$DW$537	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$537, DW_AT_name("IER")
	.dwattr $C$DW$537, DW_AT_location[DW_OP_regx 0x25]

$C$DW$538	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$538, DW_AT_name("V")
	.dwattr $C$DW$538, DW_AT_location[DW_OP_regx 0x26]

$C$DW$539	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$539, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$539, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$540	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$540, DW_AT_name("VOL")
	.dwattr $C$DW$540, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$541	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$541, DW_AT_name("AR0")
	.dwattr $C$DW$541, DW_AT_location[DW_OP_reg4]

$C$DW$542	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$542, DW_AT_name("XAR0")
	.dwattr $C$DW$542, DW_AT_location[DW_OP_reg5]

$C$DW$543	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$543, DW_AT_name("AR1")
	.dwattr $C$DW$543, DW_AT_location[DW_OP_reg6]

$C$DW$544	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$544, DW_AT_name("XAR1")
	.dwattr $C$DW$544, DW_AT_location[DW_OP_reg7]

$C$DW$545	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$545, DW_AT_name("AR2")
	.dwattr $C$DW$545, DW_AT_location[DW_OP_reg8]

$C$DW$546	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$546, DW_AT_name("XAR2")
	.dwattr $C$DW$546, DW_AT_location[DW_OP_reg9]

$C$DW$547	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$547, DW_AT_name("AR3")
	.dwattr $C$DW$547, DW_AT_location[DW_OP_reg10]

$C$DW$548	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$548, DW_AT_name("XAR3")
	.dwattr $C$DW$548, DW_AT_location[DW_OP_reg11]

$C$DW$549	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$549, DW_AT_name("AR4")
	.dwattr $C$DW$549, DW_AT_location[DW_OP_reg12]

$C$DW$550	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$550, DW_AT_name("XAR4")
	.dwattr $C$DW$550, DW_AT_location[DW_OP_reg13]

$C$DW$551	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$551, DW_AT_name("AR5")
	.dwattr $C$DW$551, DW_AT_location[DW_OP_reg14]

$C$DW$552	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$552, DW_AT_name("XAR5")
	.dwattr $C$DW$552, DW_AT_location[DW_OP_reg15]

$C$DW$553	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$553, DW_AT_name("AR6")
	.dwattr $C$DW$553, DW_AT_location[DW_OP_reg16]

$C$DW$554	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$554, DW_AT_name("XAR6")
	.dwattr $C$DW$554, DW_AT_location[DW_OP_reg17]

$C$DW$555	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$555, DW_AT_name("AR7")
	.dwattr $C$DW$555, DW_AT_location[DW_OP_reg18]

$C$DW$556	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$556, DW_AT_name("XAR7")
	.dwattr $C$DW$556, DW_AT_location[DW_OP_reg19]

$C$DW$557	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$557, DW_AT_name("R0HL")
	.dwattr $C$DW$557, DW_AT_location[DW_OP_regx 0x29]

$C$DW$558	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$558, DW_AT_name("R0H")
	.dwattr $C$DW$558, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$559	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$559, DW_AT_name("R1HL")
	.dwattr $C$DW$559, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$560	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$560, DW_AT_name("R1H")
	.dwattr $C$DW$560, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$561	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$561, DW_AT_name("R2HL")
	.dwattr $C$DW$561, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$562	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$562, DW_AT_name("R2H")
	.dwattr $C$DW$562, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$563	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$563, DW_AT_name("R3HL")
	.dwattr $C$DW$563, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$564	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$564, DW_AT_name("R3H")
	.dwattr $C$DW$564, DW_AT_location[DW_OP_regx 0x30]

$C$DW$565	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$565, DW_AT_name("R4HL")
	.dwattr $C$DW$565, DW_AT_location[DW_OP_regx 0x31]

$C$DW$566	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$566, DW_AT_name("R4H")
	.dwattr $C$DW$566, DW_AT_location[DW_OP_regx 0x32]

$C$DW$567	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$567, DW_AT_name("R5HL")
	.dwattr $C$DW$567, DW_AT_location[DW_OP_regx 0x33]

$C$DW$568	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$568, DW_AT_name("R5H")
	.dwattr $C$DW$568, DW_AT_location[DW_OP_regx 0x34]

$C$DW$569	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$569, DW_AT_name("R6HL")
	.dwattr $C$DW$569, DW_AT_location[DW_OP_regx 0x35]

$C$DW$570	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$570, DW_AT_name("R6H")
	.dwattr $C$DW$570, DW_AT_location[DW_OP_regx 0x36]

$C$DW$571	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$571, DW_AT_name("R7HL")
	.dwattr $C$DW$571, DW_AT_location[DW_OP_regx 0x37]

$C$DW$572	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$572, DW_AT_name("R7H")
	.dwattr $C$DW$572, DW_AT_location[DW_OP_regx 0x38]

$C$DW$573	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$573, DW_AT_name("RBL")
	.dwattr $C$DW$573, DW_AT_location[DW_OP_regx 0x49]

$C$DW$574	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$574, DW_AT_name("RB")
	.dwattr $C$DW$574, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$575	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$575, DW_AT_name("STFL")
	.dwattr $C$DW$575, DW_AT_location[DW_OP_regx 0x27]

$C$DW$576	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$576, DW_AT_name("STF")
	.dwattr $C$DW$576, DW_AT_location[DW_OP_regx 0x28]

$C$DW$577	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$577, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$577, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


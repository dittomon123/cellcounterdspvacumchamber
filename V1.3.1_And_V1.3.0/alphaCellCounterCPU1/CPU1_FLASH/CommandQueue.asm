;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Tue Feb 06 18:53:00 2018                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../source/CommandQueue.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cellcounter\V_0.0.25\CellCounter_03_Feb_V_0.0.25\alphaCellCounterCPU1\CPU1_FLASH")

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("printf")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_printf")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$26)

$C$DW$3	.dwtag  DW_TAG_unspecified_parameters

	.dwendtag $C$DW$1

;	C:\ti\ccsv7\tools\compiler\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\Pcadmin\\AppData\\Local\\Temp\\1034813 
	.sect	".text:_initQueue"
	.clink
	.global	_initQueue

$C$DW$4	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$4, DW_AT_name("initQueue")
	.dwattr $C$DW$4, DW_AT_low_pc(_initQueue)
	.dwattr $C$DW$4, DW_AT_high_pc(0x00)
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_initQueue")
	.dwattr $C$DW$4, DW_AT_external
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$4, DW_AT_TI_begin_file("../source/CommandQueue.c")
	.dwattr $C$DW$4, DW_AT_TI_begin_line(0x0d)
	.dwattr $C$DW$4, DW_AT_TI_begin_column(0x0f)
	.dwattr $C$DW$4, DW_AT_TI_max_frame_size(-20)
	.dwpsn	file "../source/CommandQueue.c",line 14,column 1,is_stmt,address _initQueue,isa 0

	.dwfde $C$DW$CIE, _initQueue
;----------------------------------------------------------------------
;  13 | struct Queue* initQueue()                                              
;  15 | struct Queue stCommandQueue[COMMAND_QUEUE_SIZE];                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _initQueue                    FR SIZE:  18           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 18 Auto,  0 SOE     *
;***************************************************************

_initQueue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -20
$C$DW$5	.dwtag  DW_TAG_variable
	.dwattr $C$DW$5, DW_AT_name("queue")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_breg20 -2]

$C$DW$6	.dwtag  DW_TAG_variable
	.dwattr $C$DW$6, DW_AT_name("stCommandQueue")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_stCommandQueue")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -18]

	.dwpsn	file "../source/CommandQueue.c",line 16,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
;  16 | struct Queue* queue = stCommandQueue;                                  
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |16| 
        SUBB      XAR4,#18              ; [CPU_U] |16| 
        MOVU      ACC,AR4               ; [CPU_] |16| 
        MOVL      *-SP[2],ACC           ; [CPU_] |16| 
	.dwpsn	file "../source/CommandQueue.c",line 17,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  17 | queue->unCapacity =  COMMAND_QUEUE_SIZE;                               
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |17| 
        MOVB      *+XAR4[3],#4,UNC      ; [CPU_] |17| 
	.dwpsn	file "../source/CommandQueue.c",line 18,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  18 | queue->unFront = queue->unCurrentSize = 0;                             
;  19 | //! This is important, see the enqueue                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |18| 
        MOVB      AL,#0                 ; [CPU_] |18| 
        MOV       *+XAR4[2],AL          ; [CPU_] |18| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |18| 
        MOV       *+XAR4[0],AL          ; [CPU_] |18| 
	.dwpsn	file "../source/CommandQueue.c",line 20,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  20 | queue->unRear = queue->unCapacity  - 1;                                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |20| 
        MOV       AL,*+XAR4[3]          ; [CPU_] |20| 
        ADDB      AL,#-1                ; [CPU_] |20| 
        MOV       *+XAR4[1],AL          ; [CPU_] |20| 
	.dwpsn	file "../source/CommandQueue.c",line 21,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  21 | return queue;                                                          
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |21| 
	.dwpsn	file "../source/CommandQueue.c",line 22,column 1,is_stmt,isa 0
        SUBB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$7	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$7, DW_AT_low_pc(0x00)
	.dwattr $C$DW$7, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$4, DW_AT_TI_end_file("../source/CommandQueue.c")
	.dwattr $C$DW$4, DW_AT_TI_end_line(0x16)
	.dwattr $C$DW$4, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$4

	.sect	".text:_isFull"
	.clink
	.global	_isFull

$C$DW$8	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$8, DW_AT_name("isFull")
	.dwattr $C$DW$8, DW_AT_low_pc(_isFull)
	.dwattr $C$DW$8, DW_AT_high_pc(0x00)
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_isFull")
	.dwattr $C$DW$8, DW_AT_external
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$8, DW_AT_TI_begin_file("../source/CommandQueue.c")
	.dwattr $C$DW$8, DW_AT_TI_begin_line(0x18)
	.dwattr $C$DW$8, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$8, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommandQueue.c",line 25,column 1,is_stmt,address _isFull,isa 0

	.dwfde $C$DW$CIE, _isFull
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_name("queue")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
;  24 | int isFull(struct Queue* queue)                                        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _isFull                       FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_isFull:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$10	.dwtag  DW_TAG_variable
	.dwattr $C$DW$10, DW_AT_name("queue")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_breg20 -2]

        MOVL      *-SP[2],XAR4          ; [CPU_] |25| 
	.dwpsn	file "../source/CommandQueue.c",line 26,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  26 | return (queue->unCurrentSize == queue->unCapacity);                    
;----------------------------------------------------------------------
        MOVL      XAR5,*-SP[2]          ; [CPU_] |26| 
        MOVZ      AR7,*+XAR5[2]         ; [CPU_] |26| 
        MOVU      ACC,*+XAR4[3]         ; [CPU_] |26| 
        MOVB      XAR6,#0               ; [CPU_] |26| 
        CMPL      ACC,XAR7              ; [CPU_] |26| 
        B         $C$L1,NEQ             ; [CPU_] |26| 
        ; branchcc occurs ; [] |26| 
        MOVB      XAR6,#1               ; [CPU_] |26| 
$C$L1:    
        MOV       AL,AR6                ; [CPU_] |26| 
	.dwpsn	file "../source/CommandQueue.c",line 27,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$11	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$11, DW_AT_low_pc(0x00)
	.dwattr $C$DW$11, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$8, DW_AT_TI_end_file("../source/CommandQueue.c")
	.dwattr $C$DW$8, DW_AT_TI_end_line(0x1b)
	.dwattr $C$DW$8, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$8

	.sect	".text:_isEmpty"
	.clink
	.global	_isEmpty

$C$DW$12	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$12, DW_AT_name("isEmpty")
	.dwattr $C$DW$12, DW_AT_low_pc(_isEmpty)
	.dwattr $C$DW$12, DW_AT_high_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_isEmpty")
	.dwattr $C$DW$12, DW_AT_external
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$12, DW_AT_TI_begin_file("../source/CommandQueue.c")
	.dwattr $C$DW$12, DW_AT_TI_begin_line(0x1d)
	.dwattr $C$DW$12, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$12, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommandQueue.c",line 30,column 1,is_stmt,address _isEmpty,isa 0

	.dwfde $C$DW$CIE, _isEmpty
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_name("queue")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
;  29 | int isEmpty(struct Queue* queue)                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _isEmpty                      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_isEmpty:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$14	.dwtag  DW_TAG_variable
	.dwattr $C$DW$14, DW_AT_name("queue")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -2]

        MOVL      *-SP[2],XAR4          ; [CPU_] |30| 
	.dwpsn	file "../source/CommandQueue.c",line 31,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  31 | return (queue->unCurrentSize == 0);                                    
;----------------------------------------------------------------------
        MOVB      AL,#0                 ; [CPU_] |31| 
        MOV       AH,*+XAR4[2]          ; [CPU_] |31| 
        CMPB      AH,#0                 ; [CPU_] |31| 
        B         $C$L2,NEQ             ; [CPU_] |31| 
        ; branchcc occurs ; [] |31| 
        MOVB      AL,#1                 ; [CPU_] |31| 
$C$L2:    
	.dwpsn	file "../source/CommandQueue.c",line 32,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$15	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$15, DW_AT_low_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$12, DW_AT_TI_end_file("../source/CommandQueue.c")
	.dwattr $C$DW$12, DW_AT_TI_end_line(0x20)
	.dwattr $C$DW$12, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$12

	.sect	".text:_enqueue"
	.clink
	.global	_enqueue

$C$DW$16	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$16, DW_AT_name("enqueue")
	.dwattr $C$DW$16, DW_AT_low_pc(_enqueue)
	.dwattr $C$DW$16, DW_AT_high_pc(0x00)
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_enqueue")
	.dwattr $C$DW$16, DW_AT_external
	.dwattr $C$DW$16, DW_AT_TI_begin_file("../source/CommandQueue.c")
	.dwattr $C$DW$16, DW_AT_TI_begin_line(0x22)
	.dwattr $C$DW$16, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$16, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/CommandQueue.c",line 35,column 1,is_stmt,address _enqueue,isa 0

	.dwfde $C$DW$CIE, _enqueue
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_name("queue")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg12]

$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_name("item")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_item")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
;  34 | void enqueue(struct Queue* queue, int item)                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _enqueue                      FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            3 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_enqueue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$19	.dwtag  DW_TAG_variable
	.dwattr $C$DW$19, DW_AT_name("queue")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -6]

$C$DW$20	.dwtag  DW_TAG_variable
	.dwattr $C$DW$20, DW_AT_name("item")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_item")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -7]

        MOV       *-SP[7],AL            ; [CPU_] |35| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |35| 
	.dwpsn	file "../source/CommandQueue.c",line 36,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  36 | if (isFull(queue))                                                     
;----------------------------------------------------------------------
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("_isFull")
	.dwattr $C$DW$21, DW_AT_TI_call

        LCR       #_isFull              ; [CPU_] |36| 
        ; call occurs [#_isFull] ; [] |36| 
        CMPB      AL,#0                 ; [CPU_] |36| 
        B         $C$L3,NEQ             ; [CPU_] |36| 
        ; branchcc occurs ; [] |36| 
	.dwpsn	file "../source/CommandQueue.c",line 37,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
;  37 | return;                                                                
;----------------------------------------------------------------------
	.dwpsn	file "../source/CommandQueue.c",line 38,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  38 | queue->unRear = (queue->unRear + 1)%queue->unCapacity;                 
;  39 | // queue->unData[queue->unRear] = item;                                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[6]          ; [CPU_] |38| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |38| 
        MOV       AH,*+XAR4[3]          ; [CPU_] |38| 
        ADDB      AL,#1                 ; [CPU_] |38| 
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("U$$MOD")
	.dwattr $C$DW$22, DW_AT_TI_call

        FFC       XAR7,#U$$MOD          ; [CPU_] |38| 
        ; call occurs [#U$$MOD] ; [] |38| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |38| 
        MOV       *+XAR4[1],AL          ; [CPU_] |38| 
	.dwpsn	file "../source/CommandQueue.c",line 40,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  40 | queue->unCurrentSize = queue->unCurrentSize + 1;                       
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[6]          ; [CPU_] |40| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |40| 
        ADDB      AL,#1                 ; [CPU_] |40| 
        MOV       *+XAR4[2],AL          ; [CPU_] |40| 
	.dwpsn	file "../source/CommandQueue.c",line 41,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  41 | printf("%d enqueued to queue\n", item);                                
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL1         ; [CPU_U] |41| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |41| 
        MOV       AL,*-SP[7]            ; [CPU_] |41| 
        MOV       *-SP[3],AL            ; [CPU_] |41| 
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("_printf")
	.dwattr $C$DW$23, DW_AT_TI_call

        LCR       #_printf              ; [CPU_] |41| 
        ; call occurs [#_printf] ; [] |41| 
	.dwpsn	file "../source/CommandQueue.c",line 42,column 1,is_stmt,isa 0
$C$L3:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$16, DW_AT_TI_end_file("../source/CommandQueue.c")
	.dwattr $C$DW$16, DW_AT_TI_end_line(0x2a)
	.dwattr $C$DW$16, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$16

	.sect	".text:_dequeue"
	.clink
	.global	_dequeue

$C$DW$25	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$25, DW_AT_name("dequeue")
	.dwattr $C$DW$25, DW_AT_low_pc(_dequeue)
	.dwattr $C$DW$25, DW_AT_high_pc(0x00)
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_dequeue")
	.dwattr $C$DW$25, DW_AT_external
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$25, DW_AT_TI_begin_file("../source/CommandQueue.c")
	.dwattr $C$DW$25, DW_AT_TI_begin_line(0x2c)
	.dwattr $C$DW$25, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$25, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommandQueue.c",line 45,column 1,is_stmt,address _dequeue,isa 0

	.dwfde $C$DW$CIE, _dequeue
$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_name("queue")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
;  44 | int dequeue(struct Queue* queue)                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _dequeue                      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_dequeue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$27	.dwtag  DW_TAG_variable
	.dwattr $C$DW$27, DW_AT_name("queue")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -2]

        MOVL      *-SP[2],XAR4          ; [CPU_] |45| 
	.dwpsn	file "../source/CommandQueue.c",line 46,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  46 | if (isEmpty(queue))                                                    
;----------------------------------------------------------------------
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_isEmpty")
	.dwattr $C$DW$28, DW_AT_TI_call

        LCR       #_isEmpty             ; [CPU_] |46| 
        ; call occurs [#_isEmpty] ; [] |46| 
        CMPB      AL,#0                 ; [CPU_] |46| 
        B         $C$L4,EQ              ; [CPU_] |46| 
        ; branchcc occurs ; [] |46| 
	.dwpsn	file "../source/CommandQueue.c",line 47,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
;  47 | return (-1);                                                           
;  48 | //int item = queue->unData[queue->unFront];                            
;----------------------------------------------------------------------
        MOV       AL,#-1                ; [CPU_] |47| 
        B         $C$L5,UNC             ; [CPU_] |47| 
        ; branch occurs ; [] |47| 
$C$L4:    
	.dwpsn	file "../source/CommandQueue.c",line 49,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  49 | queue->unFront = (queue->unFront + 1)%queue->unCapacity;               
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |49| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |49| 
        MOV       AH,*+XAR4[3]          ; [CPU_] |49| 
        ADDB      AL,#1                 ; [CPU_] |49| 
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("U$$MOD")
	.dwattr $C$DW$29, DW_AT_TI_call

        FFC       XAR7,#U$$MOD          ; [CPU_] |49| 
        ; call occurs [#U$$MOD] ; [] |49| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |49| 
        MOV       *+XAR4[0],AL          ; [CPU_] |49| 
	.dwpsn	file "../source/CommandQueue.c",line 50,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  50 | queue->unCurrentSize = queue->unCurrentSize - 1;                       
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |50| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |50| 
        ADDB      AL,#-1                ; [CPU_] |50| 
        MOV       *+XAR4[2],AL          ; [CPU_] |50| 
	.dwpsn	file "../source/CommandQueue.c",line 51,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  51 | return 1;                                                              
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |51| 
$C$L5:    
	.dwpsn	file "../source/CommandQueue.c",line 52,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$25, DW_AT_TI_end_file("../source/CommandQueue.c")
	.dwattr $C$DW$25, DW_AT_TI_end_line(0x34)
	.dwattr $C$DW$25, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$25

	.sect	".text:_unFront"
	.clink
	.global	_unFront

$C$DW$31	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$31, DW_AT_name("unFront")
	.dwattr $C$DW$31, DW_AT_low_pc(_unFront)
	.dwattr $C$DW$31, DW_AT_high_pc(0x00)
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_unFront")
	.dwattr $C$DW$31, DW_AT_external
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$31, DW_AT_TI_begin_file("../source/CommandQueue.c")
	.dwattr $C$DW$31, DW_AT_TI_begin_line(0x36)
	.dwattr $C$DW$31, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$31, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommandQueue.c",line 55,column 1,is_stmt,address _unFront,isa 0

	.dwfde $C$DW$CIE, _unFront
$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_name("queue")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
;  54 | int unFront(struct Queue* queue)                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _unFront                      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_unFront:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$33	.dwtag  DW_TAG_variable
	.dwattr $C$DW$33, DW_AT_name("queue")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -2]

        MOVL      *-SP[2],XAR4          ; [CPU_] |55| 
	.dwpsn	file "../source/CommandQueue.c",line 56,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  56 | if (isEmpty(queue))                                                    
;----------------------------------------------------------------------
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_isEmpty")
	.dwattr $C$DW$34, DW_AT_TI_call

        LCR       #_isEmpty             ; [CPU_] |56| 
        ; call occurs [#_isEmpty] ; [] |56| 
        CMPB      AL,#0                 ; [CPU_] |56| 
        B         $C$L6,EQ              ; [CPU_] |56| 
        ; branchcc occurs ; [] |56| 
	.dwpsn	file "../source/CommandQueue.c",line 57,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
;  57 | return (-1);                                                           
;  58 | //return queue->unData[queue->unFront];                                
;----------------------------------------------------------------------
        MOV       AL,#-1                ; [CPU_] |57| 
        B         $C$L7,UNC             ; [CPU_] |57| 
        ; branch occurs ; [] |57| 
$C$L6:    
	.dwpsn	file "../source/CommandQueue.c",line 59,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  59 | return 1;                                                              
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |59| 
$C$L7:    
	.dwpsn	file "../source/CommandQueue.c",line 60,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$31, DW_AT_TI_end_file("../source/CommandQueue.c")
	.dwattr $C$DW$31, DW_AT_TI_end_line(0x3c)
	.dwattr $C$DW$31, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$31

	.sect	".text:_unRear"
	.clink
	.global	_unRear

$C$DW$36	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$36, DW_AT_name("unRear")
	.dwattr $C$DW$36, DW_AT_low_pc(_unRear)
	.dwattr $C$DW$36, DW_AT_high_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_unRear")
	.dwattr $C$DW$36, DW_AT_external
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$36, DW_AT_TI_begin_file("../source/CommandQueue.c")
	.dwattr $C$DW$36, DW_AT_TI_begin_line(0x3e)
	.dwattr $C$DW$36, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$36, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommandQueue.c",line 63,column 1,is_stmt,address _unRear,isa 0

	.dwfde $C$DW$CIE, _unRear
$C$DW$37	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$37, DW_AT_name("queue")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
;  62 | int unRear(struct Queue* queue)                                        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _unRear                       FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_unRear:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$38	.dwtag  DW_TAG_variable
	.dwattr $C$DW$38, DW_AT_name("queue")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_queue")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -2]

        MOVL      *-SP[2],XAR4          ; [CPU_] |63| 
	.dwpsn	file "../source/CommandQueue.c",line 64,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  64 | if (isEmpty(queue))                                                    
;----------------------------------------------------------------------
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("_isEmpty")
	.dwattr $C$DW$39, DW_AT_TI_call

        LCR       #_isEmpty             ; [CPU_] |64| 
        ; call occurs [#_isEmpty] ; [] |64| 
        CMPB      AL,#0                 ; [CPU_] |64| 
        B         $C$L8,EQ              ; [CPU_] |64| 
        ; branchcc occurs ; [] |64| 
	.dwpsn	file "../source/CommandQueue.c",line 65,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
;  65 | return (-1);                                                           
;  66 | //return queue->unData[queue->unRear];                                 
;----------------------------------------------------------------------
        MOV       AL,#-1                ; [CPU_] |65| 
        B         $C$L9,UNC             ; [CPU_] |65| 
        ; branch occurs ; [] |65| 
$C$L8:    
	.dwpsn	file "../source/CommandQueue.c",line 67,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  67 | return 1;                                                              
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |67| 
$C$L9:    
	.dwpsn	file "../source/CommandQueue.c",line 68,column 2,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$36, DW_AT_TI_end_file("../source/CommandQueue.c")
	.dwattr $C$DW$36, DW_AT_TI_end_line(0x44)
	.dwattr $C$DW$36, DW_AT_TI_end_column(0x02)
	.dwendentry
	.dwendtag $C$DW$36

;***************************************************************
;* FAR STRINGS                                                 *
;***************************************************************
	.sect	".econst:.string"
	.align	2
$C$FSL1:	.string	"%d enqueued to queue",10,0
;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_printf
	.global	U$$MOD

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$19, DW_AT_name("Queue")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x04)
$C$DW$41	.dwtag  DW_TAG_member
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$41, DW_AT_name("unFront")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_unFront")
	.dwattr $C$DW$41, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$41, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$42	.dwtag  DW_TAG_member
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$42, DW_AT_name("unRear")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_unRear")
	.dwattr $C$DW$42, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$42, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$43	.dwtag  DW_TAG_member
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$43, DW_AT_name("unCurrentSize")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_unCurrentSize")
	.dwattr $C$DW$43, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$44	.dwtag  DW_TAG_member
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$44, DW_AT_name("unCapacity")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_unCapacity")
	.dwattr $C$DW$44, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$44, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_address_class(0x20)


$C$DW$T$22	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)
$C$DW$45	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$45, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$22

$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

$C$DW$46	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$5)

$C$DW$T$25	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$46)

$C$DW$T$26	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_address_class(0x20)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$47	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$47, DW_AT_name("AL")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg0]

$C$DW$48	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$48, DW_AT_name("AH")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg1]

$C$DW$49	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$49, DW_AT_name("PL")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg2]

$C$DW$50	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$50, DW_AT_name("PH")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg3]

$C$DW$51	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$51, DW_AT_name("SP")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg20]

$C$DW$52	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$52, DW_AT_name("XT")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg21]

$C$DW$53	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$53, DW_AT_name("T")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg22]

$C$DW$54	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$54, DW_AT_name("ST0")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg23]

$C$DW$55	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$55, DW_AT_name("ST1")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg24]

$C$DW$56	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$56, DW_AT_name("PC")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg25]

$C$DW$57	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$57, DW_AT_name("RPC")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg26]

$C$DW$58	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$58, DW_AT_name("FP")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg28]

$C$DW$59	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$59, DW_AT_name("DP")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg29]

$C$DW$60	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$60, DW_AT_name("SXM")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg30]

$C$DW$61	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$61, DW_AT_name("PM")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg31]

$C$DW$62	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$62, DW_AT_name("OVM")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_regx 0x20]

$C$DW$63	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$63, DW_AT_name("PAGE0")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_regx 0x21]

$C$DW$64	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$64, DW_AT_name("AMODE")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x22]

$C$DW$65	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$65, DW_AT_name("INTM")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x23]

$C$DW$66	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$66, DW_AT_name("IFR")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x24]

$C$DW$67	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$67, DW_AT_name("IER")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x25]

$C$DW$68	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$68, DW_AT_name("V")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x26]

$C$DW$69	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$69, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$70	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$70, DW_AT_name("VOL")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$71	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$71, DW_AT_name("AR0")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_reg4]

$C$DW$72	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$72, DW_AT_name("XAR0")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg5]

$C$DW$73	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$73, DW_AT_name("AR1")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg6]

$C$DW$74	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$74, DW_AT_name("XAR1")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg7]

$C$DW$75	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$75, DW_AT_name("AR2")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg8]

$C$DW$76	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$76, DW_AT_name("XAR2")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg9]

$C$DW$77	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$77, DW_AT_name("AR3")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg10]

$C$DW$78	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$78, DW_AT_name("XAR3")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg11]

$C$DW$79	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$79, DW_AT_name("AR4")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg12]

$C$DW$80	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$80, DW_AT_name("XAR4")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg13]

$C$DW$81	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$81, DW_AT_name("AR5")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg14]

$C$DW$82	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$82, DW_AT_name("XAR5")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg15]

$C$DW$83	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$83, DW_AT_name("AR6")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg16]

$C$DW$84	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$84, DW_AT_name("XAR6")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg17]

$C$DW$85	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$85, DW_AT_name("AR7")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg18]

$C$DW$86	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$86, DW_AT_name("XAR7")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg19]

$C$DW$87	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$87, DW_AT_name("R0HL")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_regx 0x29]

$C$DW$88	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$88, DW_AT_name("R0H")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$89	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$89, DW_AT_name("R1HL")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$90	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$90, DW_AT_name("R1H")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$91	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$91, DW_AT_name("R2HL")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$92	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$92, DW_AT_name("R2H")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$93	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$93, DW_AT_name("R3HL")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$94	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$94, DW_AT_name("R3H")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x30]

$C$DW$95	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$95, DW_AT_name("R4HL")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x31]

$C$DW$96	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$96, DW_AT_name("R4H")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x32]

$C$DW$97	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$97, DW_AT_name("R5HL")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x33]

$C$DW$98	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$98, DW_AT_name("R5H")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x34]

$C$DW$99	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$99, DW_AT_name("R6HL")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x35]

$C$DW$100	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$100, DW_AT_name("R6H")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x36]

$C$DW$101	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$101, DW_AT_name("R7HL")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x37]

$C$DW$102	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$102, DW_AT_name("R7H")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x38]

$C$DW$103	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$103, DW_AT_name("RBL")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x49]

$C$DW$104	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$104, DW_AT_name("RB")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$105	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$105, DW_AT_name("STFL")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x27]

$C$DW$106	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$106, DW_AT_name("STF")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x28]

$C$DW$107	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$107, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


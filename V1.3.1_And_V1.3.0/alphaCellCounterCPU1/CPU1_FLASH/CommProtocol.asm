;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Fri Jan 29 15:14:26 2021                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../source/CommProtocol.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cell Counter\V1.3.1_And_V1.3.0\alphaCellCounterCPU1\CPU1_FLASH")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_bMutiplePackets+0,32
	.bits	0,16			; _m_bMutiplePackets @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_bTxInterruptStatus+0,32
	.bits	1,16			; _m_bTxInterruptStatus @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_bSPIWriteControlStatus+0,32
	.bits	0,16			; _m_bSPIWriteControlStatus @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_bTxToDMAFlag+0,32
	.bits	0,16			; _m_bTxToDMAFlag @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_bLastMultiplePacket+0,32
	.bits	0,16			; _m_bLastMultiplePacket @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_usnPreviousIndex$3+0,32
	.bits	0,32			; _usnPreviousIndex$3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_susnNoOfPacketsToSitara$2+0,32
	.bits	0,32			; _susnNoOfPacketsToSitara$2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_usnNewIndex$4+0,32
	.bits	0,32			; _usnNewIndex$4 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_uslDataSize$5+0,32
	.bits	0,32			; _uslDataSize$5 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_usnRemainder$1+0,32
	.bits	0,32			; _usnRemainder$1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_m_suslPLTNextIndex+0,32
	.bits	0,32			; _m_suslPLTNextIndex @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_susnNoOfPackets$7+0,32
	.bits	0,32			; _susnNoOfPackets$7 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_m_suslWBCNextIndex+0,32
	.bits	0,32			; _m_suslWBCNextIndex @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_usnPrevIndex$8+0,32
	.bits	0,32			; _usnPrevIndex$8 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_m_suslRBCNextIndex+0,32
	.bits	0,32			; _m_suslRBCNextIndex @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_pstQueue+0,32
	.bits	_stCommandQueue,32		; _pstQueue @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_usnADCRemainder$6+0,32
	.bits	0,32			; _usnADCRemainder$6 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_usnNextIndex$9+0,32
	.bits	0,32			; _usnNextIndex$9 @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("CIFormACKPacket")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_CIFormACKPacket")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$63)

	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$3, DW_AT_name("SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
	.dwendtag $C$DW$3


$C$DW$4	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$4, DW_AT_name("SPISetCommCtrlBusyInt")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
	.dwendtag $C$DW$4


$C$DW$5	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$5, DW_AT_name("SysInitSystemHealthData")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_SysInitSystemHealthData")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$57)

	.dwendtag $C$DW$5


$C$DW$7	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$7, DW_AT_name("abort")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_abort")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
	.dwendtag $C$DW$7


$C$DW$8	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$8, DW_AT_name("perror")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_perror")
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$47)

	.dwendtag $C$DW$8


$C$DW$10	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$10, DW_AT_name("SysInitGetFirmwareVer")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_SysInitGetFirmwareVer")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$57)

	.dwendtag $C$DW$10


$C$DW$12	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$12, DW_AT_name("UartDebugPrint")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_UartDebugPrint")
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$52)

$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$10)

	.dwendtag $C$DW$12


$C$DW$15	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$15, DW_AT_name("CIProcessCommandAttribute")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_CIProcessCommandAttribute")
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$19)

$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$19)

	.dwendtag $C$DW$15

$C$DW$18	.dwtag  DW_TAG_variable
	.dwattr $C$DW$18, DW_AT_name("g_bMonDiluentPriming")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_g_bMonDiluentPriming")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external

$C$DW$19	.dwtag  DW_TAG_variable
	.dwattr $C$DW$19, DW_AT_name("g_bMonRinsePriming")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_g_bMonRinsePriming")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external

$C$DW$20	.dwtag  DW_TAG_variable
	.dwattr $C$DW$20, DW_AT_name("m_eTypeofCell")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_m_eTypeofCell")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external

	.global	_m_bMutiplePackets
_m_bMutiplePackets:	.usect	".ebss",1,1,0
$C$DW$21	.dwtag  DW_TAG_variable
	.dwattr $C$DW$21, DW_AT_name("m_bMutiplePackets")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_m_bMutiplePackets")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_addr _m_bMutiplePackets]
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$21, DW_AT_external

	.global	_m_bTxInterruptStatus
_m_bTxInterruptStatus:	.usect	".ebss",1,1,0
$C$DW$22	.dwtag  DW_TAG_variable
	.dwattr $C$DW$22, DW_AT_name("m_bTxInterruptStatus")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_m_bTxInterruptStatus")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_addr _m_bTxInterruptStatus]
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$22, DW_AT_external

$C$DW$23	.dwtag  DW_TAG_variable
	.dwattr $C$DW$23, DW_AT_name("g_bMotorChkStatus")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_g_bMotorChkStatus")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$23, DW_AT_declaration
	.dwattr $C$DW$23, DW_AT_external

$C$DW$24	.dwtag  DW_TAG_variable
	.dwattr $C$DW$24, DW_AT_name("m_usnPacketCount")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_m_usnPacketCount")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$24, DW_AT_declaration
	.dwattr $C$DW$24, DW_AT_external

$C$DW$25	.dwtag  DW_TAG_variable
	.dwattr $C$DW$25, DW_AT_name("m_ucValveNo")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_m_ucValveNo")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external

	.global	_m_bSPIWriteControlStatus
_m_bSPIWriteControlStatus:	.usect	".ebss",1,1,0
$C$DW$26	.dwtag  DW_TAG_variable
	.dwattr $C$DW$26, DW_AT_name("m_bSPIWriteControlStatus")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_m_bSPIWriteControlStatus")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_addr _m_bSPIWriteControlStatus]
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$26, DW_AT_external

$C$DW$27	.dwtag  DW_TAG_variable
	.dwattr $C$DW$27, DW_AT_name("g_bMonLysePriming")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_g_bMonLysePriming")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external

	.global	_m_bTxToDMAFlag
_m_bTxToDMAFlag:	.usect	".ebss",1,1,0
$C$DW$28	.dwtag  DW_TAG_variable
	.dwattr $C$DW$28, DW_AT_name("m_bTxToDMAFlag")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_m_bTxToDMAFlag")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_addr _m_bTxToDMAFlag]
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$28, DW_AT_external

$C$DW$29	.dwtag  DW_TAG_variable
	.dwattr $C$DW$29, DW_AT_name("g_usnCurrentMode")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_g_usnCurrentMode")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external

$C$DW$30	.dwtag  DW_TAG_variable
	.dwattr $C$DW$30, DW_AT_name("m_bTxPendingFlag")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_m_bTxPendingFlag")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$30, DW_AT_declaration
	.dwattr $C$DW$30, DW_AT_external

$C$DW$31	.dwtag  DW_TAG_variable
	.dwattr $C$DW$31, DW_AT_name("g_bNextSecRawDataReq")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_g_bNextSecRawDataReq")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$31, DW_AT_declaration
	.dwattr $C$DW$31, DW_AT_external

$C$DW$32	.dwtag  DW_TAG_variable
	.dwattr $C$DW$32, DW_AT_name("m_bNAckReceived")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_m_bNAckReceived")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$32, DW_AT_declaration
	.dwattr $C$DW$32, DW_AT_external


$C$DW$33	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$33, DW_AT_name("OIHomeSensor3State")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_OIHomeSensor3State")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external
	.dwendtag $C$DW$33


$C$DW$34	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$34, DW_AT_name("OIHomeSensor2State")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_OIHomeSensor2State")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external
	.dwendtag $C$DW$34


$C$DW$35	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$35, DW_AT_name("OIHomeSensor4State")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_OIHomeSensor4State")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external
	.dwendtag $C$DW$35


$C$DW$36	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$36, DW_AT_name("OIHomeSensor5State")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_OIHomeSensor5State")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
	.dwendtag $C$DW$36


$C$DW$37	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$37, DW_AT_name("SPIDmaWrite")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_SPIDmaWrite")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$37, DW_AT_declaration
	.dwattr $C$DW$37, DW_AT_external
$C$DW$38	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$57)

	.dwendtag $C$DW$37


$C$DW$39	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$39, DW_AT_name("sprintf")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_sprintf")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$39, DW_AT_declaration
	.dwattr $C$DW$39, DW_AT_external
$C$DW$40	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$72)

$C$DW$41	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$47)

$C$DW$42	.dwtag  DW_TAG_unspecified_parameters

	.dwendtag $C$DW$39


$C$DW$43	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$43, DW_AT_name("SysGetAdcCountData")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_SysGetAdcCountData")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$57)

	.dwendtag $C$DW$43


$C$DW$45	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$45, DW_AT_name("OIHomeSensor1State")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_OIHomeSensor1State")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
	.dwendtag $C$DW$45


$C$DW$46	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$46, DW_AT_name("SysInitGetCountingTime")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_SysInitGetCountingTime")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$46, DW_AT_declaration
	.dwattr $C$DW$46, DW_AT_external
$C$DW$47	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$57)

	.dwendtag $C$DW$46

	.global	_m_bLastMultiplePacket
_m_bLastMultiplePacket:	.usect	".ebss",1,1,0
$C$DW$48	.dwtag  DW_TAG_variable
	.dwattr $C$DW$48, DW_AT_name("m_bLastMultiplePacket")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_m_bLastMultiplePacket")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_addr _m_bLastMultiplePacket]
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$48, DW_AT_external

_usnPreviousIndex$3:	.usect	".ebss",2,1,1
_susnNoOfPacketsToSitara$2:	.usect	".ebss",2,1,1
_usnNewIndex$4:	.usect	".ebss",2,1,1
_uslDataSize$5:	.usect	".ebss",2,1,1
$C$DW$49	.dwtag  DW_TAG_variable
	.dwattr $C$DW$49, DW_AT_name("g_unSequenceStateTime")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_g_unSequenceStateTime")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external

$C$DW$50	.dwtag  DW_TAG_variable
	.dwattr $C$DW$50, DW_AT_name("sncalZeroPSI")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_sncalZeroPSI")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external


$C$DW$51	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$51, DW_AT_name("memcpy")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_memcpy")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$51, DW_AT_declaration
	.dwattr $C$DW$51, DW_AT_external
$C$DW$52	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$3)

$C$DW$53	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$40)

$C$DW$54	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$51


$C$DW$55	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$55, DW_AT_name("memset")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_memset")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external
$C$DW$56	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$3)

$C$DW$57	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$10)

$C$DW$58	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$55


$C$DW$59	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$59, DW_AT_name("SysGetADCDataLength")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_SysGetADCDataLength")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$59, DW_AT_declaration
	.dwattr $C$DW$59, DW_AT_external
$C$DW$60	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$19)

	.dwendtag $C$DW$59

_usnRemainder$1:	.usect	".ebss",2,1,1
_m_suslPLTNextIndex:	.usect	".ebss",2,1,1
$C$DW$61	.dwtag  DW_TAG_variable
	.dwattr $C$DW$61, DW_AT_name("m_suslPLTNextIndex")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_m_suslPLTNextIndex")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_addr _m_suslPLTNextIndex]

_susnNoOfPackets$7:	.usect	".ebss",2,1,1
_m_suslWBCNextIndex:	.usect	".ebss",2,1,1
$C$DW$62	.dwtag  DW_TAG_variable
	.dwattr $C$DW$62, DW_AT_name("m_suslWBCNextIndex")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_m_suslWBCNextIndex")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_addr _m_suslWBCNextIndex]

_usnPrevIndex$8:	.usect	".ebss",2,1,1
_m_suslRBCNextIndex:	.usect	".ebss",2,1,1
$C$DW$63	.dwtag  DW_TAG_variable
	.dwattr $C$DW$63, DW_AT_name("m_suslRBCNextIndex")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_m_suslRBCNextIndex")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_addr _m_suslRBCNextIndex]


$C$DW$64	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$64, DW_AT_name("SysGetPulseHeightDataFrmSDRAM")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_SysGetPulseHeightDataFrmSDRAM")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$64, DW_AT_declaration
	.dwattr $C$DW$64, DW_AT_external
$C$DW$65	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$82)

$C$DW$66	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$57)

$C$DW$67	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$29)

$C$DW$68	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$106)

	.dwendtag $C$DW$64

	.global	_pstQueue
_pstQueue:	.usect	".ebss",2,1,1
$C$DW$69	.dwtag  DW_TAG_variable
	.dwattr $C$DW$69, DW_AT_name("pstQueue")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_pstQueue")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_addr _pstQueue]
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$69, DW_AT_external


$C$DW$70	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$70, DW_AT_name("SysGetADCRawDataFrmSDRAM")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_SysGetADCRawDataFrmSDRAM")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$70, DW_AT_declaration
	.dwattr $C$DW$70, DW_AT_external
$C$DW$71	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$82)

$C$DW$72	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$57)

$C$DW$73	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$29)

$C$DW$74	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$106)

	.dwendtag $C$DW$70

_usnADCRemainder$6:	.usect	".ebss",2,1,1
_usnNextIndex$9:	.usect	".ebss",2,1,1
$C$DW$75	.dwtag  DW_TAG_variable
	.dwattr $C$DW$75, DW_AT_name("utVolumetricErrorInfo")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_utVolumetricErrorInfo")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$75, DW_AT_declaration
	.dwattr $C$DW$75, DW_AT_external

$C$DW$76	.dwtag  DW_TAG_variable
	.dwattr $C$DW$76, DW_AT_name("utVolumetricErrorInfoReAcq")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_utVolumetricErrorInfoReAcq")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$76, DW_AT_declaration
	.dwattr $C$DW$76, DW_AT_external

$C$DW$77	.dwtag  DW_TAG_variable
	.dwattr $C$DW$77, DW_AT_name("utErrorInfoCheck")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_utErrorInfoCheck")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$77, DW_AT_declaration
	.dwattr $C$DW$77, DW_AT_external

$C$DW$78	.dwtag  DW_TAG_variable
	.dwattr $C$DW$78, DW_AT_name("DebugPrintBuf")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_DebugPrintBuf")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$78, DW_AT_declaration
	.dwattr $C$DW$78, DW_AT_external

_statDebugPrintBuf:	.usect	".ebss",500,1,0
$C$DW$79	.dwtag  DW_TAG_variable
	.dwattr $C$DW$79, DW_AT_name("statDebugPrintBuf")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_statDebugPrintBuf")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_addr _statDebugPrintBuf]

	.global	_m_stPacketForTxToSitara
_m_stPacketForTxToSitara:	.usect	".ebss",512,1,0
$C$DW$80	.dwtag  DW_TAG_variable
	.dwattr $C$DW$80, DW_AT_name("m_stPacketForTxToSitara")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_m_stPacketForTxToSitara")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_addr _m_stPacketForTxToSitara]
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$80, DW_AT_external

	.global	_m_stReceivedPacketFrame
_m_stReceivedPacketFrame:	.usect	".ebss",512,1,0
$C$DW$81	.dwtag  DW_TAG_variable
	.dwattr $C$DW$81, DW_AT_name("m_stReceivedPacketFrame")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_m_stReceivedPacketFrame")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_addr _m_stReceivedPacketFrame]
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$81, DW_AT_external

	.global	_m_stPrevTxPacket
_m_stPrevTxPacket:	.usect	".ebss",512,1,0
$C$DW$82	.dwtag  DW_TAG_variable
	.dwattr $C$DW$82, DW_AT_name("m_stPrevTxPacket")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_m_stPrevTxPacket")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_addr _m_stPrevTxPacket]
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$82, DW_AT_external

	.global	_m_stPacketForTx
_m_stPacketForTx:	.usect	".ebss",512,1,0
$C$DW$83	.dwtag  DW_TAG_variable
	.dwattr $C$DW$83, DW_AT_name("m_stPacketForTx")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_m_stPacketForTx")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_addr _m_stPacketForTx]
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$83, DW_AT_external

	.global	_stCommandQueue
_stCommandQueue:	.usect	".RAMGS9_UserBuf",3075,1,0
$C$DW$84	.dwtag  DW_TAG_variable
	.dwattr $C$DW$84, DW_AT_name("stCommandQueue")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_stCommandQueue")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_addr _stCommandQueue]
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$84, DW_AT_external

;	D:\Softwares\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\DITTOM~1.DEV\\AppData\\Local\\Temp\\2063212 
	.sect	".text:_CPDecodeReceivedDataPacket"
	.clink
	.global	_CPDecodeReceivedDataPacket

$C$DW$85	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$85, DW_AT_name("CPDecodeReceivedDataPacket")
	.dwattr $C$DW$85, DW_AT_low_pc(_CPDecodeReceivedDataPacket)
	.dwattr $C$DW$85, DW_AT_high_pc(0x00)
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_CPDecodeReceivedDataPacket")
	.dwattr $C$DW$85, DW_AT_external
	.dwattr $C$DW$85, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$85, DW_AT_TI_begin_line(0xc5)
	.dwattr $C$DW$85, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$85, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/CommProtocol.c",line 198,column 1,is_stmt,address _CPDecodeReceivedDataPacket,isa 0

	.dwfde $C$DW$CIE, _CPDecodeReceivedDataPacket
$C$DW$86	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$86, DW_AT_name("rxData")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_rxData")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 197 | void CPDecodeReceivedDataPacket(uint16_t *rxData)                      
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPDecodeReceivedDataPacket   FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_CPDecodeReceivedDataPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$87	.dwtag  DW_TAG_variable
	.dwattr $C$DW$87, DW_AT_name("rxData")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_rxData")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -2]

$C$DW$88	.dwtag  DW_TAG_variable
	.dwattr $C$DW$88, DW_AT_name("usnCount")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_usnCount")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_breg20 -3]

$C$DW$89	.dwtag  DW_TAG_variable
	.dwattr $C$DW$89, DW_AT_name("usnDataDecodingOrder")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_usnDataDecodingOrder")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -4]

$C$DW$90	.dwtag  DW_TAG_variable
	.dwattr $C$DW$90, DW_AT_name("usnDataArrayIndex")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_usnDataArrayIndex")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_breg20 -5]

$C$DW$91	.dwtag  DW_TAG_variable
	.dwattr $C$DW$91, DW_AT_name("m_usnDSPRxSPIData")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_m_usnDSPRxSPIData")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_breg20 -6]

$C$DW$92	.dwtag  DW_TAG_variable
	.dwattr $C$DW$92, DW_AT_name("m_usnCRCCalculated")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_m_usnCRCCalculated")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_breg20 -7]

;----------------------------------------------------------------------
; 199 | //!Set the controller to busy state, so that processor doesn't send the
;     |  next                                                                  
; 200 | //!command when the controller is busy                                 
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |198| 
	.dwpsn	file "../source/CommProtocol.c",line 201,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 201 | SPISetCommCtrlBusyInt();                                               
;----------------------------------------------------------------------
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$93, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |201| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |201| 
	.dwpsn	file "../source/CommProtocol.c",line 204,column 23,is_stmt,isa 0
;----------------------------------------------------------------------
; 204 | uint16_t usnCount = 0;                                                 
; 206 | //!The decoding order is set to startign of the frame                  
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |204| 
	.dwpsn	file "../source/CommProtocol.c",line 207,column 35,is_stmt,isa 0
;----------------------------------------------------------------------
; 207 | uint16_t usnDataDecodingOrder = FRAME_HEADER_HIGH;                     
;----------------------------------------------------------------------
        MOVB      *-SP[4],#1,UNC        ; [CPU_] |207| 
	.dwpsn	file "../source/CommProtocol.c",line 209,column 32,is_stmt,isa 0
;----------------------------------------------------------------------
; 209 | uint16_t usnDataArrayIndex = 0;                                        
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |209| 
	.dwpsn	file "../source/CommProtocol.c",line 211,column 32,is_stmt,isa 0
;----------------------------------------------------------------------
; 211 | uint16_t m_usnDSPRxSPIData = 0;                                        
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |211| 
	.dwpsn	file "../source/CommProtocol.c",line 213,column 33,is_stmt,isa 0
;----------------------------------------------------------------------
; 213 | uint16_t m_usnCRCCalculated =0/*0xFFFF*/;                              
; 242 | //!The loop to decode the data that is received through SPI.           
; 243 | //!the loop runs from starting till the complete packet (512 Word)     
;----------------------------------------------------------------------
        MOV       *-SP[7],#0            ; [CPU_] |213| 
	.dwpsn	file "../source/CommProtocol.c",line 244,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 244 | for(usnCount = 0 ; usnCount < MAX_SPI_PACKET_LENGTH; usnCount++)       
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |244| 
	.dwpsn	file "../source/CommProtocol.c",line 244,column 24,is_stmt,isa 0
;----------------------------------------------------------------------
; 246 | //!Get the data from the array to the variable to process              
;----------------------------------------------------------------------
        CMP       *-SP[3],#512          ; [CPU_] |244| 
        B         $C$L20,HIS            ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
$C$L1:    
	.dwpsn	file "../source/CommProtocol.c",line 247,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 247 | m_usnDSPRxSPIData =  rxData[usnCount];                                 
; 249 | //!Check for the decoding order                                        
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[3]           ; [CPU_] |247| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |247| 
        MOVL      XAR7,ACC              ; [CPU_] |247| 
        MOV       AL,*+XAR7[0]          ; [CPU_] |247| 
        MOV       *-SP[6],AL            ; [CPU_] |247| 
	.dwpsn	file "../source/CommProtocol.c",line 250,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 250 | switch(usnDataDecodingOrder)                                           
; 252 |     //!If the decoding order is \def FRAME_HEADER_HIGH,                
; 253 |     //!update the structure: MSW of the start of packet with the       
; 254 |     //!received data and set the next decoding order to                
; 255 |     //!\def FRAME_HEADER_LOW                                           
; 256 |     case FRAME_HEADER_HIGH:                                            
;----------------------------------------------------------------------
        B         $C$L15,UNC            ; [CPU_] |250| 
        ; branch occurs ; [] |250| 
$C$L2:    
	.dwpsn	file "../source/CommProtocol.c",line 258,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 258 | m_stReceivedPacketFrame.usnStartOfPacket_MSW = \                       
; 259 |         m_usnDSPRxSPIData;                                             
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |258| 
        MOVW      DP,#_m_stReceivedPacketFrame ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame,AL ; [CPU_] |258| 
	.dwpsn	file "../source/CommProtocol.c",line 260,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 260 | usnDataDecodingOrder = FRAME_HEADER_LOW;                               
;----------------------------------------------------------------------
        MOVB      *-SP[4],#2,UNC        ; [CPU_] |260| 
	.dwpsn	file "../source/CommProtocol.c",line 262,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 262 | break;                                                                 
; 263 | //!If the decoding order is \def FRAME_HEADER_LOW,                     
; 264 | //!update the structure: LSW of the start of packet with the           
; 265 | //!received data and set the next decoding order to                    
; 266 | //!\def FRAME_RESERVED_BITS                                            
; 267 | case FRAME_HEADER_LOW:                                                 
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |262| 
        ; branch occurs ; [] |262| 
$C$L3:    
	.dwpsn	file "../source/CommProtocol.c",line 269,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 269 | m_stReceivedPacketFrame.usnStartOfPacket_LSW= \                        
; 270 |         m_usnDSPRxSPIData;                                             
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |269| 
        MOVW      DP,#_m_stReceivedPacketFrame+1 ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame+1,AL ; [CPU_] |269| 
	.dwpsn	file "../source/CommProtocol.c",line 271,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 271 | usnDataDecodingOrder = FRAME_RESERVED_BITS;                            
;----------------------------------------------------------------------
        MOVB      *-SP[4],#3,UNC        ; [CPU_] |271| 
	.dwpsn	file "../source/CommProtocol.c",line 273,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 273 | break;                                                                 
; 275 | //!If the decoding order is \def FRAME_RESERVED_BITS,                  
; 276 | //!update the structure: Reserved Word with the received data          
; 277 | //!and set the next decoding order to \def FRAME_DATA_LENGTH           
; 278 | case FRAME_RESERVED_BITS:                                              
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |273| 
        ; branch occurs ; [] |273| 
$C$L4:    
	.dwpsn	file "../source/CommProtocol.c",line 280,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 280 | m_stReceivedPacketFrame.usnReservedWord = m_usnDSPRxSPIData;           
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |280| 
        MOVW      DP,#_m_stReceivedPacketFrame+2 ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame+2,AL ; [CPU_] |280| 
	.dwpsn	file "../source/CommProtocol.c",line 281,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 281 | usnDataDecodingOrder = FRAME_DATA_LENGTH;                              
;----------------------------------------------------------------------
        MOVB      *-SP[4],#4,UNC        ; [CPU_] |281| 
	.dwpsn	file "../source/CommProtocol.c",line 283,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 283 | break;                                                                 
; 285 | //!If the decoding order is \def FRAME_DATA_LENGTH,                    
; 286 | //!update the structure: Data length with the received data            
; 287 | //!and set the next decoding order to \def FRAME_MAJOR_COMMAND         
; 288 | case FRAME_DATA_LENGTH:                                                
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |283| 
        ; branch occurs ; [] |283| 
$C$L5:    
	.dwpsn	file "../source/CommProtocol.c",line 291,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 291 | m_stReceivedPacketFrame.usnDataLength = m_usnDSPRxSPIData;             
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |291| 
        MOVW      DP,#_m_stReceivedPacketFrame+3 ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame+3,AL ; [CPU_] |291| 
	.dwpsn	file "../source/CommProtocol.c",line 293,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 293 | usnDataDecodingOrder = FRAME_MAJOR_COMMAND;                            
;----------------------------------------------------------------------
        MOVB      *-SP[4],#5,UNC        ; [CPU_] |293| 
	.dwpsn	file "../source/CommProtocol.c",line 295,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 295 | break;                                                                 
; 297 | //!If the decoding order is \def FRAME_MAJOR_COMMAND,                  
; 298 | //!update the structure: Major command with the received data          
; 299 | //!and set the next decoding order to \def FRAME_MINOR_COMMAND         
; 300 | case FRAME_MAJOR_COMMAND:                                              
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |295| 
        ; branch occurs ; [] |295| 
$C$L6:    
	.dwpsn	file "../source/CommProtocol.c",line 302,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 302 | m_stReceivedPacketFrame.usnMajorCommand = m_usnDSPRxSPIData;           
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |302| 
        MOVW      DP,#_m_stReceivedPacketFrame+4 ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame+4,AL ; [CPU_] |302| 
	.dwpsn	file "../source/CommProtocol.c",line 303,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 303 | usnDataDecodingOrder = FRAME_MINOR_COMMAND;                            
;----------------------------------------------------------------------
        MOVB      *-SP[4],#6,UNC        ; [CPU_] |303| 
	.dwpsn	file "../source/CommProtocol.c",line 305,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 305 | break;                                                                 
; 307 | //!If the decoding order is \def FRAME_MINOR_COMMAND,                  
; 308 | //!update the structure: Minor Command with the received data          
; 309 | //!and set the next decoding order to \def FRAME_DATA                  
; 310 | case FRAME_MINOR_COMMAND:                                              
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |305| 
        ; branch occurs ; [] |305| 
$C$L7:    
	.dwpsn	file "../source/CommProtocol.c",line 312,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 312 | m_stReceivedPacketFrame.usnMinorCommand= m_usnDSPRxSPIData;            
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |312| 
        MOVW      DP,#_m_stReceivedPacketFrame+5 ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame+5,AL ; [CPU_] |312| 
	.dwpsn	file "../source/CommProtocol.c",line 313,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 313 | usnDataDecodingOrder = FRAME_DATA;                                     
;----------------------------------------------------------------------
        MOVB      *-SP[4],#7,UNC        ; [CPU_] |313| 
	.dwpsn	file "../source/CommProtocol.c",line 315,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 315 | break;                                                                 
; 317 | //!If the decoding order is \def FRAME_DATA,                           
; 318 | //!update the structure: Data with the received data                   
; 319 | //!and set the next decoding order to \def FRAME_REMAINING_PACKETS_FOR_
;     | TX                                                                     
; 320 | //!after completion of updating the data to the structure              
; 321 | case FRAME_DATA:                                                       
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |315| 
        ; branch occurs ; [] |315| 
$C$L8:    
	.dwpsn	file "../source/CommProtocol.c",line 324,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 324 | if(( usnDataArrayIndex == \                                            
; 325 |         m_stReceivedPacketFrame.usnDataLength ) || \                   
; 326 |         (m_stReceivedPacketFrame.usnDataLength == NULL))               
;----------------------------------------------------------------------
        MOVZ      AR6,*-SP[5]           ; [CPU_] |324| 
        MOVW      DP,#_m_stReceivedPacketFrame+3 ; [CPU_U] 
        MOVU      ACC,@_m_stReceivedPacketFrame+3 ; [CPU_] |324| 
        CMPL      ACC,XAR6              ; [CPU_] |324| 
        B         $C$L9,EQ              ; [CPU_] |324| 
        ; branchcc occurs ; [] |324| 
        MOV       AL,@_m_stReceivedPacketFrame+3 ; [CPU_] |324| 
        B         $C$L10,NEQ            ; [CPU_] |324| 
        ; branchcc occurs ; [] |324| 
$C$L9:    
	.dwpsn	file "../source/CommProtocol.c",line 328,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 328 | usnDataDecodingOrder = FRAME_REMAINING_PACKETS_FOR_TX;                 
;----------------------------------------------------------------------
        MOVB      *-SP[4],#8,UNC        ; [CPU_] |328| 
	.dwpsn	file "../source/CommProtocol.c",line 329,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 329 | usnCount = CRC_LOCATION_IN_PACKET - 3;                                 
;----------------------------------------------------------------------
        MOV       *-SP[3],#507          ; [CPU_] |329| 
	.dwpsn	file "../source/CommProtocol.c",line 330,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 330 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |330| 
        ; branch occurs ; [] |330| 
$C$L10:    
	.dwpsn	file "../source/CommProtocol.c",line 332,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 332 | m_stReceivedPacketFrame.arrusnData[usnDataArrayIndex] = \              
; 333 |                         m_usnDSPRxSPIData;                             
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |332| 
        MOV       AL,*-SP[6]            ; [CPU_] |332| 
        MOVL      XAR4,#_m_stReceivedPacketFrame+6 ; [CPU_U] |332| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |332| 
	.dwpsn	file "../source/CommProtocol.c",line 334,column 33,is_stmt,isa 0
;----------------------------------------------------------------------
; 334 | usnDataArrayIndex++;                                                   
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |334| 
	.dwpsn	file "../source/CommProtocol.c",line 337,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 337 | break;                                                                 
; 339 | //!If the decoding order is \def FRAME_REMAINING_PACKETS_FOR_TX,       
; 340 | //!update the structure: Remaining packet for TX with the received data
; 341 | //!and set the next decoding order to \def FRAME_PACKET_CRC            
; 342 | case FRAME_REMAINING_PACKETS_FOR_TX:                                   
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |337| 
        ; branch occurs ; [] |337| 
$C$L11:    
	.dwpsn	file "../source/CommProtocol.c",line 344,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 344 | m_stReceivedPacketFrame.usnRemainingPacketsforTX = \                   
; 345 |         m_usnDSPRxSPIData;                                             
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |344| 
        MOVW      DP,#_m_stReceivedPacketFrame+508 ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame+508,AL ; [CPU_] |344| 
	.dwpsn	file "../source/CommProtocol.c",line 347,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 347 | usnDataDecodingOrder = FRAME_PACKET_CRC;                               
; 348 | // 508th location increments +1 =509                                   
; 349 | //usnCount = CRC_LOCATION_IN_PACKET - 2;                               
;----------------------------------------------------------------------
        MOVB      *-SP[4],#9,UNC        ; [CPU_] |347| 
	.dwpsn	file "../source/CommProtocol.c",line 351,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 351 | break;                                                                 
; 353 | //!If the decoding order is \def FRAME_PACKET_CRC,                     
; 354 | //!update the structure: Data length with the received data            
; 355 | //!and set the next decoding order to \def FRAME_FOOTER_HIGH           
; 356 | //!Calcualte the CRC for the data received                             
; 357 | case FRAME_PACKET_CRC:                                                 
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |351| 
        ; branch occurs ; [] |351| 
$C$L12:    
	.dwpsn	file "../source/CommProtocol.c",line 359,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 359 | m_stReceivedPacketFrame.usnCRC = m_usnDSPRxSPIData;                    
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |359| 
        MOVW      DP,#_m_stReceivedPacketFrame+509 ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame+509,AL ; [CPU_] |359| 
	.dwpsn	file "../source/CommProtocol.c",line 361,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 361 | m_usnCRCCalculated = \                                                 
; 362 |         CPCalculatePacketCRC(m_stReceivedPacketFrame);                 
;----------------------------------------------------------------------
        MOVL      XAR4,#_m_stReceivedPacketFrame ; [CPU_U] |361| 
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("_CPCalculatePacketCRC")
	.dwattr $C$DW$94, DW_AT_TI_call

        LCR       #_CPCalculatePacketCRC ; [CPU_] |361| 
        ; call occurs [#_CPCalculatePacketCRC] ; [] |361| 
        MOV       *-SP[7],AL            ; [CPU_] |361| 
	.dwpsn	file "../source/CommProtocol.c",line 363,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 363 | usnDataDecodingOrder = FRAME_FOOTER_HIGH;                              
;----------------------------------------------------------------------
        MOVB      *-SP[4],#10,UNC       ; [CPU_] |363| 
	.dwpsn	file "../source/CommProtocol.c",line 365,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 365 | break;                                                                 
; 367 | //!If the decoding order is \def FRAME_FOOTER_HIGH,                    
; 368 | //!update the structure: MSW of End Of Packet with the received data   
; 369 | //!and set the next decoding order to \def FRAME_DATA_LENGTH           
; 370 | case FRAME_FOOTER_HIGH:                                                
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |365| 
        ; branch occurs ; [] |365| 
$C$L13:    
	.dwpsn	file "../source/CommProtocol.c",line 372,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 372 | m_stReceivedPacketFrame.usnEndOfPacket_MSW = \                         
; 373 |         m_usnDSPRxSPIData;                                             
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |372| 
        MOVW      DP,#_m_stReceivedPacketFrame+510 ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame+510,AL ; [CPU_] |372| 
	.dwpsn	file "../source/CommProtocol.c",line 374,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 374 | usnDataDecodingOrder = FRAME_FOOTER_LOW;                               
;----------------------------------------------------------------------
        MOVB      *-SP[4],#11,UNC       ; [CPU_] |374| 
	.dwpsn	file "../source/CommProtocol.c",line 376,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 376 | break;                                                                 
; 378 | //!If the decoding order is \def FRAME_FOOTER_LOW,                     
; 379 | //!update the structure: LSW of End Of Packet with the received data   
; 380 | //!and set the next decoding order to \def FRAME_HEADER_LOW            
; 381 | case FRAME_FOOTER_LOW:                                                 
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |376| 
        ; branch occurs ; [] |376| 
$C$L14:    
	.dwpsn	file "../source/CommProtocol.c",line 383,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 383 | m_stReceivedPacketFrame.usnEndOfPacket_LSW = \                         
; 384 |         m_usnDSPRxSPIData;                                             
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |383| 
        MOVW      DP,#_m_stReceivedPacketFrame+511 ; [CPU_U] 
        MOV       @_m_stReceivedPacketFrame+511,AL ; [CPU_] |383| 
	.dwpsn	file "../source/CommProtocol.c",line 385,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 385 | usnDataDecodingOrder = FRAME_HEADER_LOW;                               
;----------------------------------------------------------------------
        MOVB      *-SP[4],#2,UNC        ; [CPU_] |385| 
	.dwpsn	file "../source/CommProtocol.c",line 387,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 387 | break;                                                                 
; 389 | //!If the decoding order doesn't match to any of the above scenario,   
; 390 | //!do nothing                                                          
; 391 | default:                                                               
; 392 | break;                                                                 
; 394 | }//end of switch case                                                  
; 395 | }//end of for loop                                                     
; 397 | //!Compare the calculated CRC with the received CRC                    
; 398 | //!Packet is valid if calculated checksum is equal to the checksum obta
;     | ined                                                                   
;----------------------------------------------------------------------
        B         $C$L19,UNC            ; [CPU_] |387| 
        ; branch occurs ; [] |387| 
$C$L15:    
	.dwpsn	file "../source/CommProtocol.c",line 250,column 9,is_stmt,isa 0
        MOV       AL,*-SP[4]            ; [CPU_] |250| 
        CMPB      AL,#6                 ; [CPU_] |250| 
        B         $C$L17,GT             ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#6                 ; [CPU_] |250| 
        B         $C$L7,EQ              ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#3                 ; [CPU_] |250| 
        B         $C$L16,GT             ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#3                 ; [CPU_] |250| 
        B         $C$L4,EQ              ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#1                 ; [CPU_] |250| 
        B         $C$L2,EQ              ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#2                 ; [CPU_] |250| 
        B         $C$L3,EQ              ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        B         $C$L19,UNC            ; [CPU_] |250| 
        ; branch occurs ; [] |250| 
$C$L16:    
        CMPB      AL,#4                 ; [CPU_] |250| 
        B         $C$L5,EQ              ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#5                 ; [CPU_] |250| 
        B         $C$L6,EQ              ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        B         $C$L19,UNC            ; [CPU_] |250| 
        ; branch occurs ; [] |250| 
$C$L17:    
        CMPB      AL,#9                 ; [CPU_] |250| 
        B         $C$L18,GT             ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#9                 ; [CPU_] |250| 
        B         $C$L12,EQ             ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#7                 ; [CPU_] |250| 
        B         $C$L8,EQ              ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#8                 ; [CPU_] |250| 
        B         $C$L11,EQ             ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        B         $C$L19,UNC            ; [CPU_] |250| 
        ; branch occurs ; [] |250| 
$C$L18:    
        CMPB      AL,#10                ; [CPU_] |250| 
        B         $C$L13,EQ             ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
        CMPB      AL,#11                ; [CPU_] |250| 
        B         $C$L14,EQ             ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
$C$L19:    
	.dwpsn	file "../source/CommProtocol.c",line 244,column 58,is_stmt,isa 0
        INC       *-SP[3]               ; [CPU_] |244| 
	.dwpsn	file "../source/CommProtocol.c",line 244,column 24,is_stmt,isa 0
        CMP       *-SP[3],#512          ; [CPU_] |244| 
        B         $C$L1,LO              ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
$C$L20:    
	.dwpsn	file "../source/CommProtocol.c",line 399,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 399 | if(m_usnCRCCalculated == m_stReceivedPacketFrame.usnCRC)               
;----------------------------------------------------------------------
        MOVZ      AR6,*-SP[7]           ; [CPU_] |399| 
        MOVW      DP,#_m_stReceivedPacketFrame+509 ; [CPU_U] 
        MOVU      ACC,@_m_stReceivedPacketFrame+509 ; [CPU_] |399| 
        CMPL      ACC,XAR6              ; [CPU_] |399| 
        B         $C$L27,NEQ            ; [CPU_] |399| 
        ; branchcc occurs ; [] |399| 
	.dwpsn	file "../source/CommProtocol.c",line 401,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 401 | CIProcessCommandAttribute(m_stReceivedPacketFrame.usnMajorCommand,\    
; 402 |         m_stReceivedPacketFrame.usnMinorCommand);                      
; 404 | //!Enable reagent monitoring based the current mode selected           
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+4 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+4 ; [CPU_] |401| 
        MOV       AH,@_m_stReceivedPacketFrame+5 ; [CPU_] |401| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("_CIProcessCommandAttribute")
	.dwattr $C$DW$95, DW_AT_TI_call

        LCR       #_CIProcessCommandAttribute ; [CPU_] |401| 
        ; call occurs [#_CIProcessCommandAttribute] ; [] |401| 
	.dwpsn	file "../source/CommProtocol.c",line 405,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 405 | if((FALSE == g_bMonLysePriming) || (FALSE == g_bMonDiluentPriming) \   
; 406 |         || (FALSE == g_bMonRinsePriming))                              
; 408 | switch(g_usnCurrentMode)                                               
; 410 | case WHOLE_BLOOD_MODE:                                                 
; 411 | case PRE_DILUENT_MODE:                                                 
; 412 | case DISPENSE_THE_DILUENT:                                             
; 413 | case START_PRE_DILUENT_COUNTING_CMD:                                   
; 414 | case BODY_FLUID_MODE:                                                  
; 415 | case PARTIAL_BLOOD_COUNT_WBC:                                          
; 416 | case PARTIAL_BLOOD_COUNT_RBC_PLT:                                      
; 417 | case AUTO_CALIBRATION_WB:                                              
; 418 | case COMMERCIAL_CALIBRATION_WBC:                                       
; 419 | case COMMERCIAL_CALIBRATION_RBC:                                       
; 420 | case COMMERCIAL_CALIBRATION_PLT:                                       
; 421 | case QUALITY_CONTROL_WB:                                               
; 422 | case QUALITY_CONTROL_BODY_FLUID:                                       
; 423 | case PRIME_WITH_EZ_CLEANSER_CMD:                                       
; 424 | case BACK_FLUSH_SERVICE_HANDLING:                                      
; 425 | case ZAP_APERTURE_SERVICE_HANDLING:                                    
; 426 | case DRAIN_BATH_SERVICE_HANDLING:                                      
; 427 | case CLEAN_BATH_SERVICE_HANDLING:                                      
; 428 | case SHUTDOWN_SEQUENCE:                                                
; 430 |     //!Enable the Lyse Prime monitoring                                
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOV       AL,@_g_bMonLysePriming ; [CPU_] |405| 
        B         $C$L22,EQ             ; [CPU_] |405| 
        ; branchcc occurs ; [] |405| 
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOV       AL,@_g_bMonDiluentPriming ; [CPU_] |405| 
        B         $C$L22,EQ             ; [CPU_] |405| 
        ; branchcc occurs ; [] |405| 
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOV       AL,@_g_bMonRinsePriming ; [CPU_] |405| 
        B         $C$L28,NEQ            ; [CPU_] |405| 
        ; branchcc occurs ; [] |405| 
        B         $C$L22,UNC            ; [CPU_] |405| 
        ; branch occurs ; [] |405| 
$C$L21:    
	.dwpsn	file "../source/CommProtocol.c",line 431,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 431 | g_bMonLysePriming = TRUE;                                              
; 433 | //!Enable the Diluent Prime monitoring                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOVB      @_g_bMonLysePriming,#1,UNC ; [CPU_] |431| 
	.dwpsn	file "../source/CommProtocol.c",line 434,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 434 | g_bMonDiluentPriming = TRUE;                                           
; 436 | //!Enable the Rinse Prime monitoring                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOVB      @_g_bMonDiluentPriming,#1,UNC ; [CPU_] |434| 
	.dwpsn	file "../source/CommProtocol.c",line 437,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 437 | g_bMonRinsePriming = TRUE;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOVB      @_g_bMonRinsePriming,#1,UNC ; [CPU_] |437| 
	.dwpsn	file "../source/CommProtocol.c",line 438,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 438 | break;                                                                 
; 440 | case PRIME_ALL_SERVICE_HANDLING:                                       
; 441 | case PRIME_WITH_DILUENT_SERVICE_HANDLING:                              
; 442 | case PRIME_WITH_LYSE_SERVICE_HANDLING:                                 
; 443 | case PRIME_WITH_RINSE_SERVICE_HANDLING:                                
; 444 | case DRAIN_ALL_SERVICE_HANDLING:                                       
; 445 | case SITARA_ABORT_MAIN_PROCESS_CMD:                                    
; 446 | //!Do not monitor the reagent priming                                  
; 447 | break;                                                                 
; 449 | default:                                                               
; 450 | break;                                                                 
; 454 | else                                                                   
; 456 | /*!SEND NACK TO SITARA*/ // TO BE IMPLEMENTED                          
;----------------------------------------------------------------------
        B         $C$L28,UNC            ; [CPU_] |438| 
        ; branch occurs ; [] |438| 
$C$L22:    
	.dwpsn	file "../source/CommProtocol.c",line 408,column 13,is_stmt,isa 0
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       AH,@_g_usnCurrentMode ; [CPU_] |408| 
        CMP       AH,#20514             ; [CPU_] |408| 
        B         $C$L24,GT             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        CMP       AH,#20514             ; [CPU_] |408| 
        B         $C$L21,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        CMP       AH,#20511             ; [CPU_] |408| 
        B         $C$L23,GT             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        CMP       AH,#20511             ; [CPU_] |408| 
        B         $C$L21,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        CMP       AH,#4102              ; [CPU_] |408| 
        B         $C$L28,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        MOV       AL,AH                 ; [CPU_] |408| 
        SUB       AL,#20481             ; [CPU_] |408| 
        CMPB      AL,#17                ; [CPU_] |408| 
        B         $C$L26,LOS            ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        B         $C$L28,UNC            ; [CPU_] |408| 
        ; branch occurs ; [] |408| 
$C$L23:    
        CMP       AH,#20512             ; [CPU_] |408| 
        B         $C$L21,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        CMP       AH,#20513             ; [CPU_] |408| 
        B         $C$L21,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        B         $C$L28,UNC            ; [CPU_] |408| 
        ; branch occurs ; [] |408| 
$C$L24:    
        CMP       AH,#20517             ; [CPU_] |408| 
        B         $C$L25,GT             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        CMP       AH,#20517             ; [CPU_] |408| 
        B         $C$L21,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        CMP       AH,#20515             ; [CPU_] |408| 
        B         $C$L21,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        CMP       AH,#20516             ; [CPU_] |408| 
        B         $C$L21,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        B         $C$L28,UNC            ; [CPU_] |408| 
        ; branch occurs ; [] |408| 
$C$L25:    
        CMP       AH,#20518             ; [CPU_] |408| 
        B         $C$L21,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        CMP       AH,#20519             ; [CPU_] |408| 
        B         $C$L21,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        B         $C$L28,UNC            ; [CPU_] |408| 
        ; branch occurs ; [] |408| 
$C$L26:    
        SUB       AH,#20481             ; [CPU_] |408| 
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR7,#$C$SW1          ; [CPU_U] |408| 
        MOV       ACC,AH << #1          ; [CPU_] |408| 
        MOV       ACC,AL                ; [CPU_] |408| 
        ADDL      XAR7,ACC              ; [CPU_] |408| 
        MOVL      XAR7,*XAR7            ; [CPU_] |408| 
        LB        *XAR7                 ; [CPU_] |408| 
        ; branch occurs ; [] |408| 
	.sect	".switch:_CPDecodeReceivedDataPacket"
	.clink
$C$SW1:	.long	$C$L28	; 20481
	.long	$C$L28	; 20482
	.long	$C$L28	; 20483
	.long	$C$L21	; 20484
	.long	$C$L28	; 20485
	.long	$C$L21	; 20486
	.long	$C$L21	; 20487
	.long	$C$L21	; 20488
	.long	$C$L21	; 20489
	.long	$C$L28	; 20490
	.long	$C$L21	; 20491
	.long	$C$L28	; 0
	.long	$C$L28	; 0
	.long	$C$L28	; 0
	.long	$C$L21	; 20495
	.long	$C$L21	; 20496
	.long	$C$L21	; 20497
	.long	$C$L21	; 20498
	.sect	".text:_CPDecodeReceivedDataPacket"
$C$L27:    
	.dwpsn	file "../source/CommProtocol.c",line 457,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 457 | CIFormACKPacket(SET);                                                  
; 459 | //!Since it is only processing command,                                
; 460 | //!and no further sequence is initiated, clear the busy pin            
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |457| 
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_name("_CIFormACKPacket")
	.dwattr $C$DW$96, DW_AT_TI_call

        LCR       #_CIFormACKPacket     ; [CPU_] |457| 
        ; call occurs [#_CIFormACKPacket] ; [] |457| 
	.dwpsn	file "../source/CommProtocol.c",line 461,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 461 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$97, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |461| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |461| 
	.dwpsn	file "../source/CommProtocol.c",line 465,column 1,is_stmt,isa 0
$C$L28:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$98	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$98, DW_AT_low_pc(0x00)
	.dwattr $C$DW$98, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$85, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$85, DW_AT_TI_end_line(0x1d1)
	.dwattr $C$DW$85, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$85

	.sect	".text:_CPSetStartOfPacket"
	.clink
	.global	_CPSetStartOfPacket

$C$DW$99	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$99, DW_AT_name("CPSetStartOfPacket")
	.dwattr $C$DW$99, DW_AT_low_pc(_CPSetStartOfPacket)
	.dwattr $C$DW$99, DW_AT_high_pc(0x00)
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_CPSetStartOfPacket")
	.dwattr $C$DW$99, DW_AT_external
	.dwattr $C$DW$99, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$99, DW_AT_TI_begin_line(0x1da)
	.dwattr $C$DW$99, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$99, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommProtocol.c",line 475,column 1,is_stmt,address _CPSetStartOfPacket,isa 0

	.dwfde $C$DW$CIE, _CPSetStartOfPacket
$C$DW$100	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$100, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 474 | void CPSetStartOfPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket)        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPSetStartOfPacket           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPSetStartOfPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$101	.dwtag  DW_TAG_variable
	.dwattr $C$DW$101, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 476 | //!Update the structure: MSW of Start of Packet with                   
; 477 | //!\def START_OF_PACKET_MSW                                            
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |475| 
	.dwpsn	file "../source/CommProtocol.c",line 478,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 478 | stPtrParsedPacket->usnStartOfPacket_MSW = START_OF_PACKET_MSW;         
; 479 | //!Update the structure: LSW of Start of Packet with                   
; 480 | //!\def START_OF_PACKET_LSW                                            
;----------------------------------------------------------------------
        MOV       *+XAR4[0],#43690      ; [CPU_] |478| 
	.dwpsn	file "../source/CommProtocol.c",line 481,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 481 | stPtrParsedPacket->usnStartOfPacket_LSW = START_OF_PACKET_LSW;         
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |481| 
        MOV       *+XAR4[1],#21845      ; [CPU_] |481| 
	.dwpsn	file "../source/CommProtocol.c",line 482,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$102	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$102, DW_AT_low_pc(0x00)
	.dwattr $C$DW$102, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$99, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$99, DW_AT_TI_end_line(0x1e2)
	.dwattr $C$DW$99, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$99

	.sect	".text:_CPSetEndOfPacket"
	.clink
	.global	_CPSetEndOfPacket

$C$DW$103	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$103, DW_AT_name("CPSetEndOfPacket")
	.dwattr $C$DW$103, DW_AT_low_pc(_CPSetEndOfPacket)
	.dwattr $C$DW$103, DW_AT_high_pc(0x00)
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_CPSetEndOfPacket")
	.dwattr $C$DW$103, DW_AT_external
	.dwattr $C$DW$103, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$103, DW_AT_TI_begin_line(0x1eb)
	.dwattr $C$DW$103, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$103, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommProtocol.c",line 492,column 1,is_stmt,address _CPSetEndOfPacket,isa 0

	.dwfde $C$DW$CIE, _CPSetEndOfPacket
$C$DW$104	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$104, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 491 | void CPSetEndOfPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket)          
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPSetEndOfPacket             FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPSetEndOfPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$105	.dwtag  DW_TAG_variable
	.dwattr $C$DW$105, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 493 | //!Update the structure: MSW of End of Packet with                     
; 494 | //!\def END_OF_PACKET_MSW                                              
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |492| 
	.dwpsn	file "../source/CommProtocol.c",line 495,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 495 | stPtrParsedPacket->usnEndOfPacket_MSW = END_OF_PACKET_MSW;             
; 496 | //!Update the structure: LSW of End of Packet with                     
; 497 | //!\def END_OF_PACKET_LSW                                              
;----------------------------------------------------------------------
        MOVL      XAR4,#510             ; [CPU_U] |495| 
        MOVL      ACC,XAR4              ; [CPU_] |495| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |495| 
        MOVL      XAR4,ACC              ; [CPU_] |495| 
        MOV       *+XAR4[0],#21845      ; [CPU_] |495| 
	.dwpsn	file "../source/CommProtocol.c",line 498,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 498 | stPtrParsedPacket->usnEndOfPacket_LSW = END_OF_PACKET_LSW;             
;----------------------------------------------------------------------
        MOVL      XAR4,#511             ; [CPU_U] |498| 
        MOVL      ACC,XAR4              ; [CPU_] |498| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |498| 
        MOVL      XAR4,ACC              ; [CPU_] |498| 
        MOV       *+XAR4[0],#43690      ; [CPU_] |498| 
	.dwpsn	file "../source/CommProtocol.c",line 499,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$103, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$103, DW_AT_TI_end_line(0x1f3)
	.dwattr $C$DW$103, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$103

	.sect	".text:_CPSetPacketDataLength"
	.clink
	.global	_CPSetPacketDataLength

$C$DW$107	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$107, DW_AT_name("CPSetPacketDataLength")
	.dwattr $C$DW$107, DW_AT_low_pc(_CPSetPacketDataLength)
	.dwattr $C$DW$107, DW_AT_high_pc(0x00)
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_CPSetPacketDataLength")
	.dwattr $C$DW$107, DW_AT_external
	.dwattr $C$DW$107, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$107, DW_AT_TI_begin_line(0x1fc)
	.dwattr $C$DW$107, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$107, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommProtocol.c",line 510,column 1,is_stmt,address _CPSetPacketDataLength,isa 0

	.dwfde $C$DW$CIE, _CPSetPacketDataLength
$C$DW$108	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$108, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg12]

$C$DW$109	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$109, DW_AT_name("dataLength")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_dataLength")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 508 | void CPSetPacketDataLength(ST_SPI_PACKET_FRAME *stPtrParsedPacket, \   
; 509 | Uint16 dataLength)                                                     
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPSetPacketDataLength        FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CPSetPacketDataLength:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$110	.dwtag  DW_TAG_variable
	.dwattr $C$DW$110, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_breg20 -2]

$C$DW$111	.dwtag  DW_TAG_variable
	.dwattr $C$DW$111, DW_AT_name("dataLength")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_dataLength")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 511 | //!Update the structure: Data length with the data length of the packet
;     |  to                                                                    
; 512 | //!be transmitted                                                      
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |510| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |510| 
	.dwpsn	file "../source/CommProtocol.c",line 513,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 513 | stPtrParsedPacket->usnDataLength = dataLength;                         
;----------------------------------------------------------------------
        MOV       *+XAR4[3],AL          ; [CPU_] |513| 
	.dwpsn	file "../source/CommProtocol.c",line 514,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$107, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$107, DW_AT_TI_end_line(0x202)
	.dwattr $C$DW$107, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$107

	.sect	".text:_CPWritePHDataToPacket"
	.clink
	.global	_CPWritePHDataToPacket

$C$DW$113	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$113, DW_AT_name("CPWritePHDataToPacket")
	.dwattr $C$DW$113, DW_AT_low_pc(_CPWritePHDataToPacket)
	.dwattr $C$DW$113, DW_AT_high_pc(0x00)
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_CPWritePHDataToPacket")
	.dwattr $C$DW$113, DW_AT_external
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$113, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$113, DW_AT_TI_begin_line(0x20c)
	.dwattr $C$DW$113, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$113, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommProtocol.c",line 526,column 1,is_stmt,address _CPWritePHDataToPacket,isa 0

	.dwfde $C$DW$CIE, _CPWritePHDataToPacket
$C$DW$114	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$114, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg12]

$C$DW$115	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$115, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 524 | uint16_t CPWritePHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
; 525 | bool_t bNAKReceived)                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWritePHDataToPacket        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CPWritePHDataToPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$116	.dwtag  DW_TAG_variable
	.dwattr $C$DW$116, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$116, DW_AT_location[DW_OP_breg20 -2]

$C$DW$117	.dwtag  DW_TAG_variable
	.dwattr $C$DW$117, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$117, DW_AT_location[DW_OP_breg20 -3]

$C$DW$118	.dwtag  DW_TAG_variable
	.dwattr $C$DW$118, DW_AT_name("usnDataLength")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$118, DW_AT_location[DW_OP_breg20 -4]

$C$DW$119	.dwtag  DW_TAG_variable
	.dwattr $C$DW$119, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$119, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 527 | //!Set data length as zero by default                                  
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |526| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |526| 
	.dwpsn	file "../source/CommProtocol.c",line 528,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 528 | uint16_t usnDataLength = 0;                                            
; 529 | //!Set minor/sub command as zero by default                            
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |528| 
	.dwpsn	file "../source/CommProtocol.c",line 530,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 530 | uint16_t usnSubCmd =0;                                                 
; 532 | //!Get the minor command from the structure to update the data         
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |530| 
	.dwpsn	file "../source/CommProtocol.c",line 533,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 533 | usnSubCmd = stPtrParsedPacket->usnMinorCommand;                        
; 535 | //!Check for the minor/sub command                                     
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[5]          ; [CPU_] |533| 
        MOV       *-SP[5],AL            ; [CPU_] |533| 
	.dwpsn	file "../source/CommProtocol.c",line 536,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 536 | switch(usnSubCmd)                                                      
; 538 |     //!If the minor command is WBC, RBC, PLT, HGB result,              
; 539 |     //!Perform the below operation:                                    
; 540 |             case DELFINO_WBC_RBC_PLT_HGB_COUNT:                        
; 542 |                 //!Frame the Structure: data with the final count resul
;     | ts and get                                                             
; 543 |                 //!the length of the packet                            
;----------------------------------------------------------------------
        B         $C$L37,UNC            ; [CPU_] |536| 
        ; branch occurs ; [] |536| 
$C$L29:    
	.dwpsn	file "../source/CommProtocol.c",line 544,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 544 | usnDataLength = CPGetCountData(stPtrParsedPacket);                     
;----------------------------------------------------------------------
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_name("_CPGetCountData")
	.dwattr $C$DW$120, DW_AT_TI_call

        LCR       #_CPGetCountData      ; [CPU_] |544| 
        ; call occurs [#_CPGetCountData] ; [] |544| 
        MOV       *-SP[4],AL            ; [CPU_] |544| 
	.dwpsn	file "../source/CommProtocol.c",line 546,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 546 | break;                                                                 
; 548 | //!If the minor command is WBC Pulse height, RBC Pulse Height,         
; 549 | //!PLT pulse height, Perform the below operation:                      
; 550 | case DELFINO_WBC_PULSE_HEIGHT_DATA:                                    
; 551 | case DELFINO_RBC_PULSE_HEIGHT_DATA:                                    
; 552 | case DELFINO_PLT_PULSE_HEIGHT_DATA:                                    
; 554 |     //!If NAK is not received for the transmitted data, frame the      
; 555 |     //!pulse height data for the corresponding cell and transmit to the
; 556 |     //!processor, else frame the faulty packets and send it to the     
; 557 |     //!processor                                                       
;----------------------------------------------------------------------
        B         $C$L39,UNC            ; [CPU_] |546| 
        ; branch occurs ; [] |546| 
$C$L30:    
	.dwpsn	file "../source/CommProtocol.c",line 558,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 558 | if(bNAKReceived == FALSE)                                              
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |558| 
        B         $C$L31,NEQ            ; [CPU_] |558| 
        ; branchcc occurs ; [] |558| 
	.dwpsn	file "../source/CommProtocol.c",line 560,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 560 | usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\            
; 561 |         usnSubCmd, FALSE);                                             
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |560| 
        MOVB      AH,#0                 ; [CPU_] |560| 
$C$DW$121	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$121, DW_AT_low_pc(0x00)
	.dwattr $C$DW$121, DW_AT_name("_CPGetRawADCOrHeightData")
	.dwattr $C$DW$121, DW_AT_TI_call

        LCR       #_CPGetRawADCOrHeightData ; [CPU_] |560| 
        ; call occurs [#_CPGetRawADCOrHeightData] ; [] |560| 
        MOV       *-SP[4],AL            ; [CPU_] |560| 
	.dwpsn	file "../source/CommProtocol.c",line 562,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 563 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L39,UNC            ; [CPU_] |562| 
        ; branch occurs ; [] |562| 
$C$L31:    
	.dwpsn	file "../source/CommProtocol.c",line 565,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 565 | usnDataLength = \                                                      
; 566 |         CPGetHeightDataForReTransmission(stPtrParsedPacket,\           
; 567 |                 usnSubCmd);                                            
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |565| 
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_name("_CPGetHeightDataForReTransmission")
	.dwattr $C$DW$122, DW_AT_TI_call

        LCR       #_CPGetHeightDataForReTransmission ; [CPU_] |565| 
        ; call occurs [#_CPGetHeightDataForReTransmission] ; [] |565| 
        MOV       *-SP[4],AL            ; [CPU_] |565| 
	.dwpsn	file "../source/CommProtocol.c",line 570,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 570 | break;                                                                 
; 572 | //!If the minor command is WBC Raw Data, RBC Raw Data, PLT Raw Data    
; 573 | //!Perform the below operation:                                        
; 574 | case DELFINO_WBC_ADC_RAW_DATA:                                         
; 575 | case DELFINO_RBC_ADC_RAW_DATA:                                         
; 576 | case DELFINO_PLT_ADC_RAW_DATA:                                         
; 578 | //!If NAK is not received for the transmitted data, frame the          
; 579 | //!Raw Data for the corresponding cell and transmit to the             
; 580 | //!processor, else frame the faulty packets and send it to the         
; 581 | //!processor                                                           
;----------------------------------------------------------------------
        B         $C$L39,UNC            ; [CPU_] |570| 
        ; branch occurs ; [] |570| 
$C$L32:    
	.dwpsn	file "../source/CommProtocol.c",line 582,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 582 | if(bNAKReceived == FALSE)                                              
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |582| 
        B         $C$L33,NEQ            ; [CPU_] |582| 
        ; branchcc occurs ; [] |582| 
	.dwpsn	file "../source/CommProtocol.c",line 584,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 584 | usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\            
; 585 |         usnSubCmd, TRUE);                                              
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |584| 
        MOVB      AH,#1                 ; [CPU_] |584| 
$C$DW$123	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$123, DW_AT_low_pc(0x00)
	.dwattr $C$DW$123, DW_AT_name("_CPGetRawADCOrHeightData")
	.dwattr $C$DW$123, DW_AT_TI_call

        LCR       #_CPGetRawADCOrHeightData ; [CPU_] |584| 
        ; call occurs [#_CPGetRawADCOrHeightData] ; [] |584| 
        MOV       *-SP[4],AL            ; [CPU_] |584| 
	.dwpsn	file "../source/CommProtocol.c",line 586,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 587 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L39,UNC            ; [CPU_] |586| 
        ; branch occurs ; [] |586| 
$C$L33:    
	.dwpsn	file "../source/CommProtocol.c",line 589,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 589 | usnDataLength = CPReTransmistADCRawData(stPtrParsedPacket,\            
; 590 |         usnSubCmd);                                                    
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |589| 
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_name("_CPReTransmistADCRawData")
	.dwattr $C$DW$124, DW_AT_TI_call

        LCR       #_CPReTransmistADCRawData ; [CPU_] |589| 
        ; call occurs [#_CPReTransmistADCRawData] ; [] |589| 
        MOV       *-SP[4],AL            ; [CPU_] |589| 
	.dwpsn	file "../source/CommProtocol.c",line 593,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 593 | break;                                                                 
; 596 | case DELFINO_ERROR_MINOR_CMD:                                          
;----------------------------------------------------------------------
        B         $C$L39,UNC            ; [CPU_] |593| 
        ; branch occurs ; [] |593| 
$C$L34:    
	.dwpsn	file "../source/CommProtocol.c",line 597,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 597 | usnDataLength = CPGetErrorData(stPtrParsedPacket);                     
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |597| 
$C$DW$125	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$125, DW_AT_low_pc(0x00)
	.dwattr $C$DW$125, DW_AT_name("_CPGetErrorData")
	.dwattr $C$DW$125, DW_AT_TI_call

        LCR       #_CPGetErrorData      ; [CPU_] |597| 
        ; call occurs [#_CPGetErrorData] ; [] |597| 
        MOV       *-SP[4],AL            ; [CPU_] |597| 
	.dwpsn	file "../source/CommProtocol.c",line 598,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 598 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |598| 
	.dwpsn	file "../source/CommProtocol.c",line 599,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 599 | break;                                                                 
; 601 | case DELFINO_SEQUENCE_COMPLETION:                                      
; 602 | //sprintf(DebugPrintBuf, "\r\n m_usnTimeStampLen: %d\n", m_usnTimeStamp
;     | Len);                                                                  
; 603 | //UartDebugPrint((int *)DebugPrintBuf, 40);                            
; 604 | //memcpy_fast_far(&stPtrParsedPacket->arrusnData[0],&m_arrusnTimeStamp[
;     | 0],m_usnTimeStampLen);                                                 
; 605 | //usnDataLength = m_usnTimeStampLen;                                   
; 606 | //m_usnTimeStampLen = 0;                                               
; 607 | //memset(m_arrusnTimeStamp, 0, sizeof(m_arrusnTimeStamp));             
;----------------------------------------------------------------------
        B         $C$L39,UNC            ; [CPU_] |599| 
        ; branch occurs ; [] |599| 
$C$L35:    
	.dwpsn	file "../source/CommProtocol.c",line 608,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 608 | usnDataLength = 0;                                                     
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |608| 
	.dwpsn	file "../source/CommProtocol.c",line 609,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 609 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |609| 
	.dwpsn	file "../source/CommProtocol.c",line 610,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 610 | break;                                                                 
; 612 | //!For any other patient handling commands (Aspiration completed, Ready
; 613 | //!for dispensing, dispensing completed, ready for aspiration,         
; 614 | //!data length is zero and packet count is zero                        
; 615 | case DELFINO_ASPIRATION_COMPLETED:                                     
; 616 | case DELFINO_NEEDLE_READY_FOR_DISPENSING_CMD:                          
; 617 | case DELFINO_DISPENSE_COMPLETED_CMD:                                   
; 618 | case DELFINO_READY_FOR_ASPIRATION:                                     
; 619 | case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:                            
; 621 | default:                                                               
;----------------------------------------------------------------------
        B         $C$L39,UNC            ; [CPU_] |610| 
        ; branch occurs ; [] |610| 
$C$L36:    
	.dwpsn	file "../source/CommProtocol.c",line 623,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 623 | usnDataLength = 0;                                                     
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |623| 
	.dwpsn	file "../source/CommProtocol.c",line 624,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 624 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |624| 
	.dwpsn	file "../source/CommProtocol.c",line 626,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 626 | break;                                                                 
; 627 | }//end of switch case                                                  
; 628 | //!Return the data length which shall be updated in the structure of th
;     | e                                                                      
; 629 | //!data to be transmitted                                              
;----------------------------------------------------------------------
        B         $C$L39,UNC            ; [CPU_] |626| 
        ; branch occurs ; [] |626| 
$C$L37:    
	.dwpsn	file "../source/CommProtocol.c",line 536,column 5,is_stmt,isa 0
        SUB       AL,#8193              ; [CPU_] |536| 
        MOVZ      AR6,*-SP[5]           ; [CPU_] |536| 
        CMPB      AL,#13                ; [CPU_] |536| 
        B         $C$L38,LOS            ; [CPU_] |536| 
        ; branchcc occurs ; [] |536| 
        MOVZ      AR7,AR6               ; [CPU_] |536| 
        MOVL      XAR4,#8238            ; [CPU_U] |536| 
        MOVL      ACC,XAR4              ; [CPU_] |536| 
        CMPL      ACC,XAR7              ; [CPU_] |536| 
        B         $C$L36,EQ             ; [CPU_] |536| 
        ; branchcc occurs ; [] |536| 
        MOVZ      AR6,AR6               ; [CPU_] |536| 
        MOVL      XAR4,#9217            ; [CPU_U] |536| 
        MOVL      ACC,XAR4              ; [CPU_] |536| 
        CMPL      ACC,XAR6              ; [CPU_] |536| 
        B         $C$L34,EQ             ; [CPU_] |536| 
        ; branchcc occurs ; [] |536| 
        B         $C$L36,UNC            ; [CPU_] |536| 
        ; branch occurs ; [] |536| 
$C$L38:    
        SUB       AR6,#8193             ; [CPU_] |536| 
        MOV       ACC,AR6 << #1         ; [CPU_] |536| 
        MOVZ      AR6,AL                ; [CPU_] |536| 
        MOVL      XAR7,#$C$SW3          ; [CPU_U] |536| 
        MOVL      ACC,XAR7              ; [CPU_] |536| 
        ADDU      ACC,AR6               ; [CPU_] |536| 
        MOVL      XAR7,ACC              ; [CPU_] |536| 
        MOVL      XAR7,*XAR7            ; [CPU_] |536| 
        LB        *XAR7                 ; [CPU_] |536| 
        ; branch occurs ; [] |536| 
	.sect	".switch:_CPWritePHDataToPacket"
	.clink
$C$SW3:	.long	$C$L30	; 8193
	.long	$C$L30	; 8194
	.long	$C$L30	; 8195
	.long	$C$L29	; 8196
	.long	$C$L36	; 0
	.long	$C$L32	; 8198
	.long	$C$L32	; 8199
	.long	$C$L32	; 8200
	.long	$C$L36	; 8201
	.long	$C$L36	; 8202
	.long	$C$L36	; 8203
	.long	$C$L36	; 8204
	.long	$C$L36	; 0
	.long	$C$L35	; 8206
	.sect	".text:_CPWritePHDataToPacket"
$C$L39:    
	.dwpsn	file "../source/CommProtocol.c",line 630,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 630 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |630| 
	.dwpsn	file "../source/CommProtocol.c",line 631,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$126	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$126, DW_AT_low_pc(0x00)
	.dwattr $C$DW$126, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$113, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$113, DW_AT_TI_end_line(0x277)
	.dwattr $C$DW$113, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$113

	.sect	".text:_CPSetCommandsForPacket"
	.clink
	.global	_CPSetCommandsForPacket

$C$DW$127	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$127, DW_AT_name("CPSetCommandsForPacket")
	.dwattr $C$DW$127, DW_AT_low_pc(_CPSetCommandsForPacket)
	.dwattr $C$DW$127, DW_AT_high_pc(0x00)
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_CPSetCommandsForPacket")
	.dwattr $C$DW$127, DW_AT_external
	.dwattr $C$DW$127, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$127, DW_AT_TI_begin_line(0x282)
	.dwattr $C$DW$127, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$127, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommProtocol.c",line 644,column 1,is_stmt,address _CPSetCommandsForPacket,isa 0

	.dwfde $C$DW$CIE, _CPSetCommandsForPacket
$C$DW$128	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$128, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_reg12]

$C$DW$129	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$129, DW_AT_name("usnMajorCmd")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_usnMajorCmd")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_reg0]

$C$DW$130	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$130, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$130, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 642 | void CPSetCommandsForPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\   
; 643 | Uint16 usnMajorCmd, Uint16 usnSubCmd)                                  
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPSetCommandsForPacket       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_CPSetCommandsForPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$131	.dwtag  DW_TAG_variable
	.dwattr $C$DW$131, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$131, DW_AT_location[DW_OP_breg20 -2]

$C$DW$132	.dwtag  DW_TAG_variable
	.dwattr $C$DW$132, DW_AT_name("usnMajorCmd")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_usnMajorCmd")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$132, DW_AT_location[DW_OP_breg20 -3]

$C$DW$133	.dwtag  DW_TAG_variable
	.dwattr $C$DW$133, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_breg20 -4]

;----------------------------------------------------------------------
; 645 | //!Update the structure: major command with the major command to be tra
;     | nsmitted                                                               
;----------------------------------------------------------------------
        MOV       *-SP[4],AH            ; [CPU_] |644| 
        MOV       *-SP[3],AL            ; [CPU_] |644| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |644| 
	.dwpsn	file "../source/CommProtocol.c",line 646,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 646 | stPtrParsedPacket ->usnMajorCommand = usnMajorCmd;                     
; 647 | //!Update the structure: minor command with the minor command to be tra
;     | nsmitted                                                               
;----------------------------------------------------------------------
        MOV       *+XAR4[4],AL          ; [CPU_] |646| 
	.dwpsn	file "../source/CommProtocol.c",line 648,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 648 | stPtrParsedPacket ->usnMinorCommand = usnSubCmd;                       
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |648| 
        MOV       AL,*-SP[4]            ; [CPU_] |648| 
        MOV       *+XAR4[5],AL          ; [CPU_] |648| 
	.dwpsn	file "../source/CommProtocol.c",line 649,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$134	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$134, DW_AT_low_pc(0x00)
	.dwattr $C$DW$134, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$127, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$127, DW_AT_TI_end_line(0x289)
	.dwattr $C$DW$127, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$127

	.sect	".text:_CPWriteDataToSPI"
	.clink
	.global	_CPWriteDataToSPI

$C$DW$135	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$135, DW_AT_name("CPWriteDataToSPI")
	.dwattr $C$DW$135, DW_AT_low_pc(_CPWriteDataToSPI)
	.dwattr $C$DW$135, DW_AT_high_pc(0x00)
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_CPWriteDataToSPI")
	.dwattr $C$DW$135, DW_AT_external
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$135, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$135, DW_AT_TI_begin_line(0x292)
	.dwattr $C$DW$135, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$135, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommProtocol.c",line 659,column 1,is_stmt,address _CPWriteDataToSPI,isa 0

	.dwfde $C$DW$CIE, _CPWriteDataToSPI
;----------------------------------------------------------------------
; 658 | bool_t CPWriteDataToSPI()                                              
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWriteDataToSPI             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_CPWriteDataToSPI:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$136	.dwtag  DW_TAG_variable
	.dwattr $C$DW$136, DW_AT_name("bStatus")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_bStatus")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_breg20 -1]

	.dwpsn	file "../source/CommProtocol.c",line 660,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 660 | bool_t bStatus = FALSE;                                                
; 664 | //!Check for the control status, whether any packet to be transmitted t
;     | o processor                                                            
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |660| 
	.dwpsn	file "../source/CommProtocol.c",line 665,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 665 | if(m_bSPIWriteControlStatus == TRUE)                                   
; 667 |     //!Copy the updated packet framed structure the memory to transmit
;     | the                                                                    
; 668 |     //!data to the processor                                           
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSPIWriteControlStatus ; [CPU_U] 
        MOV       AL,@_m_bSPIWriteControlStatus ; [CPU_] |665| 
        CMPB      AL,#1                 ; [CPU_] |665| 
        B         $C$L40,NEQ            ; [CPU_] |665| 
        ; branchcc occurs ; [] |665| 

$C$DW$137	.dwtag  DW_TAG_lexical_block
	.dwattr $C$DW$137, DW_AT_low_pc(0x00)
	.dwattr $C$DW$137, DW_AT_high_pc(0x00)
$C$DW$138	.dwtag  DW_TAG_variable
	.dwattr $C$DW$138, DW_AT_name("pTxData")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_pTxData")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$138, DW_AT_location[DW_OP_breg20 -4]

	.dwpsn	file "../source/CommProtocol.c",line 669,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 669 | uint16_t* pTxData = (uint16_t*)&m_stPacketForTx;                       
; 670 | //!Write the data to DMA                                               
;----------------------------------------------------------------------
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |669| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |669| 
	.dwpsn	file "../source/CommProtocol.c",line 671,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 671 | bStatus = SPIDmaWrite(pTxData);                                        
; 672 | //!If the data is written successfully, the data is transmitted to proc
;     | essor                                                                  
; 673 | //!set the \var m_bSPIWriteControlStatus to false                      
;----------------------------------------------------------------------
$C$DW$139	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$139, DW_AT_low_pc(0x00)
	.dwattr $C$DW$139, DW_AT_name("_SPIDmaWrite")
	.dwattr $C$DW$139, DW_AT_TI_call

        LCR       #_SPIDmaWrite         ; [CPU_] |671| 
        ; call occurs [#_SPIDmaWrite] ; [] |671| 
        MOV       *-SP[1],AL            ; [CPU_] |671| 
	.dwpsn	file "../source/CommProtocol.c",line 674,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 674 | if(bStatus == TRUE)                                                    
; 676 |     //!Successfully command sent                                       
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |674| 
        B         $C$L41,NEQ            ; [CPU_] |674| 
        ; branchcc occurs ; [] |674| 
	.dwpsn	file "../source/CommProtocol.c",line 677,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 677 | m_bSPIWriteControlStatus = FALSE;                                      
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSPIWriteControlStatus ; [CPU_U] 
        MOV       @_m_bSPIWriteControlStatus,#0 ; [CPU_] |677| 
	.dwpsn	file "../source/CommProtocol.c",line 678,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 679 | else                                                                   
; 681 |         //DO Nothing                                                   
;----------------------------------------------------------------------
	.dwpsn	file "../source/CommProtocol.c",line 683,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 683 | return bStatus;                                                        
;----------------------------------------------------------------------
        B         $C$L41,UNC            ; [CPU_] |683| 
        ; branch occurs ; [] |683| 
	.dwendtag $C$DW$137

$C$L40:    
	.dwpsn	file "../source/CommProtocol.c",line 685,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 685 | return FALSE;                                                          
;----------------------------------------------------------------------
        MOVB      AL,#0                 ; [CPU_] |685| 
$C$L41:    
	.dwpsn	file "../source/CommProtocol.c",line 687,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$140	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$140, DW_AT_low_pc(0x00)
	.dwattr $C$DW$140, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$135, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$135, DW_AT_TI_end_line(0x2af)
	.dwattr $C$DW$135, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$135

	.sect	".text:_CPPAbort"
	.clink
	.global	_CPPAbort

$C$DW$141	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$141, DW_AT_name("CPPAbort")
	.dwattr $C$DW$141, DW_AT_low_pc(_CPPAbort)
	.dwattr $C$DW$141, DW_AT_high_pc(0x00)
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_CPPAbort")
	.dwattr $C$DW$141, DW_AT_external
	.dwattr $C$DW$141, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$141, DW_AT_TI_begin_line(0x2b8)
	.dwattr $C$DW$141, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$141, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommProtocol.c",line 697,column 1,is_stmt,address _CPPAbort,isa 0

	.dwfde $C$DW$CIE, _CPPAbort
$C$DW$142	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$142, DW_AT_name("errString")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_errString")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$142, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 696 | void CPPAbort(char *errString)                                         
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPPAbort                     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPPAbort:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$143	.dwtag  DW_TAG_variable
	.dwattr $C$DW$143, DW_AT_name("errString")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_errString")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_breg20 -2]

        MOVL      *-SP[2],XAR4          ; [CPU_] |697| 
	.dwpsn	file "../source/CommProtocol.c",line 698,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 698 | perror(errString);                                                     
;----------------------------------------------------------------------
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_name("_perror")
	.dwattr $C$DW$144, DW_AT_TI_call

        LCR       #_perror              ; [CPU_] |698| 
        ; call occurs [#_perror] ; [] |698| 
	.dwpsn	file "../source/CommProtocol.c",line 699,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 699 | abort();                                                               
;----------------------------------------------------------------------
$C$DW$145	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$145, DW_AT_low_pc(0x00)
	.dwattr $C$DW$145, DW_AT_name("_abort")
	.dwattr $C$DW$145, DW_AT_TI_call

        LCR       #_abort               ; [CPU_] |699| 
        ; call occurs [#_abort] ; [] |699| 
	.dwpsn	file "../source/CommProtocol.c",line 700,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$146	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$146, DW_AT_low_pc(0x00)
	.dwattr $C$DW$146, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$141, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$141, DW_AT_TI_end_line(0x2bc)
	.dwattr $C$DW$141, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$141

	.sect	".text:_CPCalculatePacketCRC"
	.clink
	.global	_CPCalculatePacketCRC

$C$DW$147	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$147, DW_AT_name("CPCalculatePacketCRC")
	.dwattr $C$DW$147, DW_AT_low_pc(_CPCalculatePacketCRC)
	.dwattr $C$DW$147, DW_AT_high_pc(0x00)
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_CPCalculatePacketCRC")
	.dwattr $C$DW$147, DW_AT_external
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$147, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$147, DW_AT_TI_begin_line(0x2c7)
	.dwattr $C$DW$147, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$147, DW_AT_TI_max_frame_size(-520)
	.dwpsn	file "../source/CommProtocol.c",line 712,column 1,is_stmt,address _CPCalculatePacketCRC,isa 0

	.dwfde $C$DW$CIE, _CPCalculatePacketCRC
$C$DW$148	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$148, DW_AT_name("stPacketforCRC")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_stPacketforCRC")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 711 | uint16_t CPCalculatePacketCRC(ST_SPI_PACKET_FRAME stPacketforCRC)      
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPCalculatePacketCRC         FR SIZE: 518           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 515 Auto,  2 SOE     *
;***************************************************************

_CPCalculatePacketCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        MOVL      *SP++,XAR2            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 9, 2
	.dwcfi	cfa_offset, -4
        MOVZ      AR2,SP                ; [CPU_] 
        SUBB      FP,#4                 ; [CPU_U] 
        ADD       SP,#516               ; [CPU_] 
	.dwcfi	cfa_offset, -520
$C$DW$149	.dwtag  DW_TAG_variable
	.dwattr $C$DW$149, DW_AT_name("stPacketforCRC")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_stPacketforCRC")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$149, DW_AT_location[DW_OP_breg20 -2]

$C$DW$150	.dwtag  DW_TAG_variable
	.dwattr $C$DW$150, DW_AT_name("stPacketforCRC")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_stPacketforCRC")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$150, DW_AT_location[DW_OP_breg20 -514]

$C$DW$151	.dwtag  DW_TAG_variable
	.dwattr $C$DW$151, DW_AT_name("usnCalCheckSum")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_usnCalCheckSum")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$151, DW_AT_location[DW_OP_breg20 -515]

;----------------------------------------------------------------------
; 713 | //!* Initialize the "CalCheckSum" for first time to 0XFFFF             
;----------------------------------------------------------------------
        MOVZ      AR5,SP                ; [CPU_] |712| 
        ADD       AR5,#-514             ; [CPU_] |712| 
        MOVL      XAR6,#512             ; [CPU_U] |712| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |712| 
        MOVL      ACC,XAR6              ; [CPU_] |712| 
        MOVZ      AR4,AR5               ; [CPU_] |712| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |712| 
$C$DW$152	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$152, DW_AT_low_pc(0x00)
	.dwattr $C$DW$152, DW_AT_name("_memcpy")
	.dwattr $C$DW$152, DW_AT_TI_call

        LCR       #_memcpy              ; [CPU_] |712| 
        ; call occurs [#_memcpy] ; [] |712| 
	.dwpsn	file "../source/CommProtocol.c",line 714,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 714 | uint16_t usnCalCheckSum=0xFFFF;                                        
; 716 | //!Calculate CRC for reserved Word and update to \var usnCalCheckSum   
;----------------------------------------------------------------------
        MOV       *+FP[5],#65535        ; [CPU_] |714| 
	.dwpsn	file "../source/CommProtocol.c",line 717,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 717 | CPUpdateCRC(&stPacketforCRC.usnReservedWord, 1, &usnCalCheckSum);      
; 719 | //!Calculate The CRC for DataLength and update to \var usnCalCheckSum  
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |717| 
        MOVZ      AR5,SP                ; [CPU_] |717| 
        ADD       AR4,#-512             ; [CPU_] |717| 
        ADD       AR5,#-515             ; [CPU_] |717| 
        MOVB      AL,#1                 ; [CPU_] |717| 
        MOVZ      AR4,AR4               ; [CPU_] |717| 
        MOVZ      AR5,AR5               ; [CPU_] |717| 
$C$DW$153	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$153, DW_AT_low_pc(0x00)
	.dwattr $C$DW$153, DW_AT_name("_CPUpdateCRC")
	.dwattr $C$DW$153, DW_AT_TI_call

        LCR       #_CPUpdateCRC         ; [CPU_] |717| 
        ; call occurs [#_CPUpdateCRC] ; [] |717| 
	.dwpsn	file "../source/CommProtocol.c",line 720,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 720 | CPUpdateCRC(&stPacketforCRC.usnDataLength, 1, &usnCalCheckSum);        
; 722 |     //!Calculate CRC for Major Command and update to \var usnCalCheckSu
;     | m                                                                      
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |720| 
        MOVZ      AR5,SP                ; [CPU_] |720| 
        ADD       AR4,#-511             ; [CPU_] |720| 
        ADD       AR5,#-515             ; [CPU_] |720| 
        MOVB      AL,#1                 ; [CPU_] |720| 
        MOVZ      AR4,AR4               ; [CPU_] |720| 
        MOVZ      AR5,AR5               ; [CPU_] |720| 
$C$DW$154	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$154, DW_AT_low_pc(0x00)
	.dwattr $C$DW$154, DW_AT_name("_CPUpdateCRC")
	.dwattr $C$DW$154, DW_AT_TI_call

        LCR       #_CPUpdateCRC         ; [CPU_] |720| 
        ; call occurs [#_CPUpdateCRC] ; [] |720| 
	.dwpsn	file "../source/CommProtocol.c",line 723,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 723 | CPUpdateCRC(&stPacketforCRC.usnMajorCommand, 1, &usnCalCheckSum);      
; 725 |     //!Calculate CRC for Minor Command and update to \var usnCalCheckSu
;     | m                                                                      
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |723| 
        MOVZ      AR5,SP                ; [CPU_] |723| 
        ADD       AR4,#-510             ; [CPU_] |723| 
        ADD       AR5,#-515             ; [CPU_] |723| 
        MOVB      AL,#1                 ; [CPU_] |723| 
        MOVZ      AR4,AR4               ; [CPU_] |723| 
        MOVZ      AR5,AR5               ; [CPU_] |723| 
$C$DW$155	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$155, DW_AT_low_pc(0x00)
	.dwattr $C$DW$155, DW_AT_name("_CPUpdateCRC")
	.dwattr $C$DW$155, DW_AT_TI_call

        LCR       #_CPUpdateCRC         ; [CPU_] |723| 
        ; call occurs [#_CPUpdateCRC] ; [] |723| 
	.dwpsn	file "../source/CommProtocol.c",line 726,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 726 | CPUpdateCRC(&stPacketforCRC.usnMinorCommand, 1, &usnCalCheckSum);      
; 728 | //!Calculate the CRC for Data and update to \var usnCalCheckSum        
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |726| 
        MOVZ      AR5,SP                ; [CPU_] |726| 
        ADD       AR4,#-509             ; [CPU_] |726| 
        ADD       AR5,#-515             ; [CPU_] |726| 
        MOVB      AL,#1                 ; [CPU_] |726| 
        MOVZ      AR4,AR4               ; [CPU_] |726| 
        MOVZ      AR5,AR5               ; [CPU_] |726| 
$C$DW$156	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$156, DW_AT_low_pc(0x00)
	.dwattr $C$DW$156, DW_AT_name("_CPUpdateCRC")
	.dwattr $C$DW$156, DW_AT_TI_call

        LCR       #_CPUpdateCRC         ; [CPU_] |726| 
        ; call occurs [#_CPUpdateCRC] ; [] |726| 
	.dwpsn	file "../source/CommProtocol.c",line 729,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 729 | CPUpdateCRC(stPacketforCRC.arrusnData, stPacketforCRC.usnDataLength,\  
; 730 |         &usnCalCheckSum);                                              
; 732 | //!Calculate the CRC for remaining packet for transmission and update t
;     | o                                                                      
; 733 | //!\var usnCalCheckSum                                                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |729| 
        MOVZ      AR5,SP                ; [CPU_] |729| 
        ADD       AR4,#-508             ; [CPU_] |729| 
        ADD       AR5,#-515             ; [CPU_] |729| 
        MOVL      XAR0,#9               ; [CPU_] |729| 
        MOV       AL,*+FP[AR0]          ; [CPU_] |729| 
        MOVZ      AR4,AR4               ; [CPU_] |729| 
        MOVZ      AR5,AR5               ; [CPU_] |729| 
$C$DW$157	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$157, DW_AT_low_pc(0x00)
	.dwattr $C$DW$157, DW_AT_name("_CPUpdateCRC")
	.dwattr $C$DW$157, DW_AT_TI_call

        LCR       #_CPUpdateCRC         ; [CPU_] |729| 
        ; call occurs [#_CPUpdateCRC] ; [] |729| 
	.dwpsn	file "../source/CommProtocol.c",line 734,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 734 | CPUpdateCRC(&stPacketforCRC.usnRemainingPacketsforTX,1,&usnCalCheckSum)
;     | ;                                                                      
; 736 | //!Return the calculated CRC to update it to the structure to the packe
;     | t to                                                                   
; 737 | //!be transmitted to processor                                         
;----------------------------------------------------------------------
        MOVZ      AR5,SP                ; [CPU_] |734| 
        MOVZ      AR4,SP                ; [CPU_] |734| 
        ADD       AR5,#-515             ; [CPU_] |734| 
        MOVB      AL,#1                 ; [CPU_] |734| 
        SUBB      XAR4,#6               ; [CPU_U] |734| 
        MOVZ      AR4,AR4               ; [CPU_] |734| 
        MOVZ      AR5,AR5               ; [CPU_] |734| 
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("_CPUpdateCRC")
	.dwattr $C$DW$158, DW_AT_TI_call

        LCR       #_CPUpdateCRC         ; [CPU_] |734| 
        ; call occurs [#_CPUpdateCRC] ; [] |734| 
	.dwpsn	file "../source/CommProtocol.c",line 738,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 738 | return usnCalCheckSum;                                                 
;----------------------------------------------------------------------
        MOV       AL,*+FP[5]            ; [CPU_] |738| 
	.dwpsn	file "../source/CommProtocol.c",line 739,column 1,is_stmt,isa 0
        ADD       SP,#-516              ; [CPU_] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR2,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 9
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$147, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$147, DW_AT_TI_end_line(0x2e3)
	.dwattr $C$DW$147, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$147

	.sect	".text:_CPUpdateCRC"
	.clink
	.global	_CPUpdateCRC

$C$DW$160	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$160, DW_AT_name("CPUpdateCRC")
	.dwattr $C$DW$160, DW_AT_low_pc(_CPUpdateCRC)
	.dwattr $C$DW$160, DW_AT_high_pc(0x00)
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_CPUpdateCRC")
	.dwattr $C$DW$160, DW_AT_external
	.dwattr $C$DW$160, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$160, DW_AT_TI_begin_line(0x2ed)
	.dwattr $C$DW$160, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$160, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/CommProtocol.c",line 750,column 1,is_stmt,address _CPUpdateCRC,isa 0

	.dwfde $C$DW$CIE, _CPUpdateCRC
$C$DW$161	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$161, DW_AT_name("nData")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_nData")
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$161, DW_AT_location[DW_OP_reg12]

$C$DW$162	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$162, DW_AT_name("nByteCnt")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_nByteCnt")
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$162, DW_AT_location[DW_OP_reg0]

$C$DW$163	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$163, DW_AT_name("usnCalculatedCRC")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_usnCalculatedCRC")
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$163, DW_AT_location[DW_OP_reg14]

;----------------------------------------------------------------------
; 749 | void CPUpdateCRC(uint16_t *nData, uint16_t nByteCnt, uint16_t* usnCalcu
;     | latedCRC)                                                              
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPUpdateCRC                  FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_CPUpdateCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$164	.dwtag  DW_TAG_variable
	.dwattr $C$DW$164, DW_AT_name("nData")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_nData")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$164, DW_AT_location[DW_OP_breg20 -2]

$C$DW$165	.dwtag  DW_TAG_variable
	.dwattr $C$DW$165, DW_AT_name("usnCalculatedCRC")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_usnCalculatedCRC")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_breg20 -4]

$C$DW$166	.dwtag  DW_TAG_variable
	.dwattr $C$DW$166, DW_AT_name("nByteCnt")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_nByteCnt")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$166, DW_AT_location[DW_OP_breg20 -5]

$C$DW$167	.dwtag  DW_TAG_variable
	.dwattr $C$DW$167, DW_AT_name("usnIndex")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_usnIndex")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$167, DW_AT_location[DW_OP_breg20 -6]

$C$DW$168	.dwtag  DW_TAG_variable
	.dwattr $C$DW$168, DW_AT_name("usnValRead")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_usnValRead")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$168, DW_AT_location[DW_OP_breg20 -7]

;----------------------------------------------------------------------
; 751 | //!Set the index to Zero by default                                    
;----------------------------------------------------------------------
        MOV       *-SP[5],AL            ; [CPU_] |750| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |750| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |750| 
	.dwpsn	file "../source/CommProtocol.c",line 752,column 23,is_stmt,isa 0
;----------------------------------------------------------------------
; 752 | uint16_t usnIndex = 0;                                                 
; 753 | uint16_t usnValRead;                                                   
; 755 | //!CRC is calculated for the data that is sent                         
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |752| 
	.dwpsn	file "../source/CommProtocol.c",line 756,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 756 | while(usnIndex < nByteCnt)                                             
;----------------------------------------------------------------------
        CMP       AL,*-SP[6]            ; [CPU_] |756| 
        B         $C$L43,LOS            ; [CPU_] |756| 
        ; branchcc occurs ; [] |756| 
$C$L42:    
	.dwpsn	file "../source/CommProtocol.c",line 758,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 758 | usnValRead = *(nData + usnIndex);                                      
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[6]           ; [CPU_] |758| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |758| 
        MOVL      XAR7,ACC              ; [CPU_] |758| 
        MOV       AL,*+XAR7[0]          ; [CPU_] |758| 
        MOV       *-SP[7],AL            ; [CPU_] |758| 
	.dwpsn	file "../source/CommProtocol.c",line 759,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 759 | *usnCalculatedCRC = *usnCalculatedCRC ^ usnValRead;                    
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |759| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |759| 
        XOR       AL,*+XAR4[0]          ; [CPU_] |759| 
        MOV       *+XAR5[0],AL          ; [CPU_] |759| 
	.dwpsn	file "../source/CommProtocol.c",line 760,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 760 | usnIndex++;                                                            
;----------------------------------------------------------------------
        INC       *-SP[6]               ; [CPU_] |760| 
	.dwpsn	file "../source/CommProtocol.c",line 756,column 11,is_stmt,isa 0
        MOV       AL,*-SP[5]            ; [CPU_] |756| 
        CMP       AL,*-SP[6]            ; [CPU_] |756| 
        B         $C$L42,HI             ; [CPU_] |756| 
        ; branchcc occurs ; [] |756| 
	.dwpsn	file "../source/CommProtocol.c",line 762,column 1,is_stmt,isa 0
$C$L43:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$169	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$169, DW_AT_low_pc(0x00)
	.dwattr $C$DW$169, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$160, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$160, DW_AT_TI_end_line(0x2fa)
	.dwattr $C$DW$160, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$160

	.sect	".text:_CPFrameDataPacket"
	.clink
	.global	_CPFrameDataPacket

$C$DW$170	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$170, DW_AT_name("CPFrameDataPacket")
	.dwattr $C$DW$170, DW_AT_low_pc(_CPFrameDataPacket)
	.dwattr $C$DW$170, DW_AT_high_pc(0x00)
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_CPFrameDataPacket")
	.dwattr $C$DW$170, DW_AT_external
	.dwattr $C$DW$170, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$170, DW_AT_TI_begin_line(0x305)
	.dwattr $C$DW$170, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$170, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommProtocol.c",line 775,column 1,is_stmt,address _CPFrameDataPacket,isa 0

	.dwfde $C$DW$CIE, _CPFrameDataPacket
$C$DW$171	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$171, DW_AT_name("usnMajorCmd")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_usnMajorCmd")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg0]

$C$DW$172	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$172, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_reg1]

$C$DW$173	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$173, DW_AT_name("bRemainingPackets")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_bRemainingPackets")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$173, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 773 | void CPFrameDataPacket(Uint16 usnMajorCmd, Uint16 usnSubCmd,\          
; 774 | bool_t bRemainingPackets)                                              
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPFrameDataPacket            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CPFrameDataPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$174	.dwtag  DW_TAG_variable
	.dwattr $C$DW$174, DW_AT_name("usnMajorCmd")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_usnMajorCmd")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$174, DW_AT_location[DW_OP_breg20 -1]

$C$DW$175	.dwtag  DW_TAG_variable
	.dwattr $C$DW$175, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$175, DW_AT_location[DW_OP_breg20 -2]

$C$DW$176	.dwtag  DW_TAG_variable
	.dwattr $C$DW$176, DW_AT_name("bRemainingPackets")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_bRemainingPackets")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_breg20 -3]

$C$DW$177	.dwtag  DW_TAG_variable
	.dwattr $C$DW$177, DW_AT_name("ndataSize")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_ndataSize")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$177, DW_AT_location[DW_OP_breg20 -4]

$C$DW$178	.dwtag  DW_TAG_variable
	.dwattr $C$DW$178, DW_AT_name("usnCalculatedChkSum")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_usnCalculatedChkSum")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$178, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 776 | //!Initialize \var ndataSize by default to Zero                        
;----------------------------------------------------------------------
        MOV       *-SP[3],AR4           ; [CPU_] |775| 
        MOV       *-SP[2],AH            ; [CPU_] |775| 
        MOV       *-SP[1],AL            ; [CPU_] |775| 
	.dwpsn	file "../source/CommProtocol.c",line 777,column 24,is_stmt,isa 0
;----------------------------------------------------------------------
; 777 | uint16_t ndataSize = 0;                                                
; 778 | //!Initialize \var usnCalculatedChkSum by default to Zero              
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |777| 
	.dwpsn	file "../source/CommProtocol.c",line 779,column 31,is_stmt,isa 0
;----------------------------------------------------------------------
; 779 | uint16_t usnCalculatedChkSum = 0xFFFF;                                 
; 781 | //!Check if there is no packet presently to be transmitted to processor
;     | .                                                                      
; 782 | //!If no packet to be transmitted, then the data shall be updated to th
;     | e                                                                      
; 783 | //!structure to be transmitted to the processor                        
;----------------------------------------------------------------------
        MOV       *-SP[5],#65535        ; [CPU_] |779| 
	.dwpsn	file "../source/CommProtocol.c",line 784,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 784 | if((m_bSPIWriteControlStatus == FALSE ) && (FALSE == m_bTxPendingFlag))
; 786 |     //!Set major and minor command to the packet attribute             
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSPIWriteControlStatus ; [CPU_U] 
        MOV       AL,@_m_bSPIWriteControlStatus ; [CPU_] |784| 
        B         $C$L65,NEQ            ; [CPU_] |784| 
        ; branchcc occurs ; [] |784| 
        MOVW      DP,#_m_bTxPendingFlag ; [CPU_U] 
        MOV       AL,@_m_bTxPendingFlag ; [CPU_] |784| 
        B         $C$L65,NEQ            ; [CPU_] |784| 
        ; branchcc occurs ; [] |784| 
	.dwpsn	file "../source/CommProtocol.c",line 787,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 787 | CPSetPacketAttributesForTx(&m_stPacketForTx, usnMajorCmd, usnSubCmd);  
; 789 | //!Check for the major command to write the data to the packet         
;----------------------------------------------------------------------
        MOV       AL,*-SP[1]            ; [CPU_] |787| 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |787| 
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_name("_CPSetPacketAttributesForTx")
	.dwattr $C$DW$179, DW_AT_TI_call

        LCR       #_CPSetPacketAttributesForTx ; [CPU_] |787| 
        ; call occurs [#_CPSetPacketAttributesForTx] ; [] |787| 
	.dwpsn	file "../source/CommProtocol.c",line 790,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 790 | switch(usnMajorCmd)                                                    
; 792 |     //!If the major command if patient handling, write patient handling
; 793 |     //!data packet to the structure                                    
; 794 |         case DELFINO_PATIENT_HANDLING:                                 
;----------------------------------------------------------------------
        B         $C$L56,UNC            ; [CPU_] |790| 
        ; branch occurs ; [] |790| 
$C$L44:    
	.dwpsn	file "../source/CommProtocol.c",line 795,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 795 | ndataSize = CPWritePHDataToPacket(&m_stPacketForTx,\                   
; 796 |         m_bNAckReceived);                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |795| 
        MOV       AL,@_m_bNAckReceived  ; [CPU_] |795| 
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("_CPWritePHDataToPacket")
	.dwattr $C$DW$180, DW_AT_TI_call

        LCR       #_CPWritePHDataToPacket ; [CPU_] |795| 
        ; call occurs [#_CPWritePHDataToPacket] ; [] |795| 
        MOV       *-SP[4],AL            ; [CPU_] |795| 
	.dwpsn	file "../source/CommProtocol.c",line 797,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 797 | break;                                                                 
; 798 | //!If the major command if Firmware Version, write Firmware Version    
; 799 | //!data packet to the structure                                        
; 800 | case DELFINO_FIRMWARE_VERSION_CMD:                                     
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |797| 
        ; branch occurs ; [] |797| 
$C$L45:    
	.dwpsn	file "../source/CommProtocol.c",line 801,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 801 | ndataSize = CPWriteFirmwareVerToPacket(&m_stPacketForTx,\              
; 802 |         m_bNAckReceived);                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |801| 
        MOV       AL,@_m_bNAckReceived  ; [CPU_] |801| 
$C$DW$181	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$181, DW_AT_low_pc(0x00)
	.dwattr $C$DW$181, DW_AT_name("_CPWriteFirmwareVerToPacket")
	.dwattr $C$DW$181, DW_AT_TI_call

        LCR       #_CPWriteFirmwareVerToPacket ; [CPU_] |801| 
        ; call occurs [#_CPWriteFirmwareVerToPacket] ; [] |801| 
        MOV       *-SP[4],AL            ; [CPU_] |801| 
	.dwpsn	file "../source/CommProtocol.c",line 803,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 803 | break;                                                                 
; 804 | //!If the major command if Service Handling, write Service Handling    
; 805 | //!data packet to the structure                                        
; 806 | case DELFINO_SERVICE_HANDLING:                                         
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |803| 
        ; branch occurs ; [] |803| 
$C$L46:    
	.dwpsn	file "../source/CommProtocol.c",line 807,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 807 | ndataSize = CPWriteSHDataToPacket(&m_stPacketForTx,\                   
; 808 |         m_bNAckReceived);                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |807| 
        MOV       AL,@_m_bNAckReceived  ; [CPU_] |807| 
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_name("_CPWriteSHDataToPacket")
	.dwattr $C$DW$182, DW_AT_TI_call

        LCR       #_CPWriteSHDataToPacket ; [CPU_] |807| 
        ; call occurs [#_CPWriteSHDataToPacket] ; [] |807| 
        MOV       *-SP[4],AL            ; [CPU_] |807| 
	.dwpsn	file "../source/CommProtocol.c",line 809,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 809 | break;                                                                 
; 811 | //!If the major command if Calibration handling, write Calibration     
; 812 | //!handling data packet to the structure                               
; 813 | case DELFINO_CALIBRATION_HANDLING:                                     
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |809| 
        ; branch occurs ; [] |809| 
$C$L47:    
	.dwpsn	file "../source/CommProtocol.c",line 814,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 814 | ndataSize = CPWriteCHDataToPacket(&m_stPacketForTx,\                   
; 815 |         m_bNAckReceived);                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |814| 
        MOV       AL,@_m_bNAckReceived  ; [CPU_] |814| 
$C$DW$183	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$183, DW_AT_low_pc(0x00)
	.dwattr $C$DW$183, DW_AT_name("_CPWriteCHDataToPacket")
	.dwattr $C$DW$183, DW_AT_TI_call

        LCR       #_CPWriteCHDataToPacket ; [CPU_] |814| 
        ; call occurs [#_CPWriteCHDataToPacket] ; [] |814| 
        MOV       *-SP[4],AL            ; [CPU_] |814| 
	.dwpsn	file "../source/CommProtocol.c",line 816,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 816 | break;                                                                 
; 818 | //!If the major command if Quality handling, write Quality             
; 819 | //!handling data packet to the structure                               
; 820 | case DELFINO_QUALITY_HANDLING:                                         
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |816| 
        ; branch occurs ; [] |816| 
$C$L48:    
	.dwpsn	file "../source/CommProtocol.c",line 821,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 821 | ndataSize = CPWriteQHDataToPacket(&m_stPacketForTx,\                   
; 822 |         m_bNAckReceived);                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |821| 
        MOV       AL,@_m_bNAckReceived  ; [CPU_] |821| 
$C$DW$184	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$184, DW_AT_low_pc(0x00)
	.dwattr $C$DW$184, DW_AT_name("_CPWriteQHDataToPacket")
	.dwattr $C$DW$184, DW_AT_TI_call

        LCR       #_CPWriteQHDataToPacket ; [CPU_] |821| 
        ; call occurs [#_CPWriteQHDataToPacket] ; [] |821| 
        MOV       *-SP[4],AL            ; [CPU_] |821| 
	.dwpsn	file "../source/CommProtocol.c",line 823,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 823 | break;                                                                 
; 825 | //!If the major command if Monitoirng Sequence, write Monitoring       
; 826 | //!Sequence data packet to the structure                               
; 827 | case DELFINO_MONITOR_SEQUENCE:                                         
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |823| 
        ; branch occurs ; [] |823| 
$C$L49:    
	.dwpsn	file "../source/CommProtocol.c",line 828,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 828 | ndataSize = CPWriteSeqMonitoringDataToPacket(&m_stPacketForTx,\        
; 829 |                         m_bNAckReceived);                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |828| 
        MOV       AL,@_m_bNAckReceived  ; [CPU_] |828| 
$C$DW$185	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$185, DW_AT_low_pc(0x00)
	.dwattr $C$DW$185, DW_AT_name("_CPWriteSeqMonitoringDataToPacket")
	.dwattr $C$DW$185, DW_AT_TI_call

        LCR       #_CPWriteSeqMonitoringDataToPacket ; [CPU_] |828| 
        ; call occurs [#_CPWriteSeqMonitoringDataToPacket] ; [] |828| 
        MOV       *-SP[4],AL            ; [CPU_] |828| 
	.dwpsn	file "../source/CommProtocol.c",line 830,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 830 | break;                                                                 
; 832 | //!If the major command if Shutdown Sequence, set the data length      
; 833 | //!and packet size to Zero                                             
; 834 | case DELFINO_SHUTDOWN_SEQUENCE_CMD:                                    
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |830| 
        ; branch occurs ; [] |830| 
$C$L50:    
	.dwpsn	file "../source/CommProtocol.c",line 835,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 835 | ndataSize =0;                                                          
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |835| 
	.dwpsn	file "../source/CommProtocol.c",line 836,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 836 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |836| 
	.dwpsn	file "../source/CommProtocol.c",line 837,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 837 | break;                                                                 
; 839 | //!If the major command if Abort the current process, write the        
; 840 | //!data packet to the structure                                        
; 841 | case DELFINO_ABORT_PROCESS_MAIN_CMD:                                   
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |837| 
        ; branch occurs ; [] |837| 
$C$L51:    
	.dwpsn	file "../source/CommProtocol.c",line 842,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 842 | ndataSize = CPWriteAbortProcessDataToPacket(&m_stPacketForTx,\         
; 843 |                                         m_bNAckReceived);              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |842| 
        MOV       AL,@_m_bNAckReceived  ; [CPU_] |842| 
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("_CPWriteAbortProcessDataToPacket")
	.dwattr $C$DW$186, DW_AT_TI_call

        LCR       #_CPWriteAbortProcessDataToPacket ; [CPU_] |842| 
        ; call occurs [#_CPWriteAbortProcessDataToPacket] ; [] |842| 
        MOV       *-SP[4],AL            ; [CPU_] |842| 
	.dwpsn	file "../source/CommProtocol.c",line 844,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 844 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |844| 
	.dwpsn	file "../source/CommProtocol.c",line 845,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 845 | break;                                                                 
; 847 | //!If the major command if system setting command, write the           
; 848 | //!data packet to the structure                                        
; 849 | case DELFINO_SYSTEM_SETTING_CMD:                                       
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |845| 
        ; branch occurs ; [] |845| 
$C$L52:    
	.dwpsn	file "../source/CommProtocol.c",line 850,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 850 | ndataSize = CPWriteSystemSettingDataToPacket(&m_stPacketForTx,\        
; 851 |                                         m_bNAckReceived);              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |850| 
        MOV       AL,@_m_bNAckReceived  ; [CPU_] |850| 
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_name("_CPWriteSystemSettingDataToPacket")
	.dwattr $C$DW$187, DW_AT_TI_call

        LCR       #_CPWriteSystemSettingDataToPacket ; [CPU_] |850| 
        ; call occurs [#_CPWriteSystemSettingDataToPacket] ; [] |850| 
        MOV       *-SP[4],AL            ; [CPU_] |850| 
	.dwpsn	file "../source/CommProtocol.c",line 852,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 852 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |852| 
	.dwpsn	file "../source/CommProtocol.c",line 853,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 853 | break;                                                                 
; 855 | //!If the major command if Startup command, write the                  
; 856 | //!data packet to the structure                                        
; 857 | case DELFINO_STARTUP_HANDLING_CMD:                                     
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |853| 
        ; branch occurs ; [] |853| 
$C$L53:    
	.dwpsn	file "../source/CommProtocol.c",line 858,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 858 | ndataSize = CPWriteSTPHDataToPacket(&m_stPacketForTx,\                 
; 859 |                                         m_bNAckReceived);              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |858| 
        MOV       AL,@_m_bNAckReceived  ; [CPU_] |858| 
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_name("_CPWriteSTPHDataToPacket")
	.dwattr $C$DW$188, DW_AT_TI_call

        LCR       #_CPWriteSTPHDataToPacket ; [CPU_] |858| 
        ; call occurs [#_CPWriteSTPHDataToPacket] ; [] |858| 
        MOV       *-SP[4],AL            ; [CPU_] |858| 
	.dwpsn	file "../source/CommProtocol.c",line 860,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 860 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |860| 
	.dwpsn	file "../source/CommProtocol.c",line 861,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 861 | break;                                                                 
; 863 | //!If the major command if error command, write the                    
; 864 | //!data packet to the structure                                        
; 865 | case DELFINO_ERROR_HANDLING_CMD:                                       
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |861| 
        ; branch occurs ; [] |861| 
$C$L54:    
	.dwpsn	file "../source/CommProtocol.c",line 866,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 866 | ndataSize = CPGetErrorData(&m_stPacketForTx);                          
;----------------------------------------------------------------------
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |866| 
$C$DW$189	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$189, DW_AT_low_pc(0x00)
	.dwattr $C$DW$189, DW_AT_name("_CPGetErrorData")
	.dwattr $C$DW$189, DW_AT_TI_call

        LCR       #_CPGetErrorData      ; [CPU_] |866| 
        ; call occurs [#_CPGetErrorData] ; [] |866| 
        MOV       *-SP[4],AL            ; [CPU_] |866| 
	.dwpsn	file "../source/CommProtocol.c",line 867,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 867 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |867| 
	.dwpsn	file "../source/CommProtocol.c",line 868,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 868 | break;                                                                 
; 869 |         //!If the major command does not match to any of the above ment
;     | ioned                                                                  
; 870 |         //!scenario, set the data size and packet count to Zero        
; 871 |         default:                                                       
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |868| 
        ; branch occurs ; [] |868| 
$C$L55:    
	.dwpsn	file "../source/CommProtocol.c",line 872,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 872 | ndataSize =0;                                                          
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |872| 
	.dwpsn	file "../source/CommProtocol.c",line 873,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 873 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |873| 
	.dwpsn	file "../source/CommProtocol.c",line 874,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 874 | break;                                                                 
; 875 | }//end of switch case                                                  
; 877 | //!Fill the Data Length Attribute to the structure                     
;----------------------------------------------------------------------
        B         $C$L60,UNC            ; [CPU_] |874| 
        ; branch occurs ; [] |874| 
$C$L56:    
	.dwpsn	file "../source/CommProtocol.c",line 790,column 3,is_stmt,isa 0
        MOVZ      AR6,*-SP[1]           ; [CPU_] |790| 
        CMP       AR6,#8198             ; [CPU_] |790| 
        B         $C$L58,GT             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        CMP       AR6,#8198             ; [CPU_] |790| 
        B         $C$L49,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        CMP       AR6,#8195             ; [CPU_] |790| 
        B         $C$L57,GT             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        CMP       AR6,#8195             ; [CPU_] |790| 
        B         $C$L45,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        MOVZ      AR7,AR6               ; [CPU_] |790| 
        MOVL      XAR4,#8193            ; [CPU_U] |790| 
        MOVL      ACC,XAR4              ; [CPU_] |790| 
        CMPL      ACC,XAR7              ; [CPU_] |790| 
        B         $C$L44,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        MOVZ      AR6,AR6               ; [CPU_] |790| 
        MOVL      XAR4,#8194            ; [CPU_U] |790| 
        MOVL      ACC,XAR4              ; [CPU_] |790| 
        CMPL      ACC,XAR6              ; [CPU_] |790| 
        B         $C$L47,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        B         $C$L55,UNC            ; [CPU_] |790| 
        ; branch occurs ; [] |790| 
$C$L57:    
        MOVZ      AR7,AR6               ; [CPU_] |790| 
        MOVL      XAR4,#8196            ; [CPU_U] |790| 
        MOVL      ACC,XAR4              ; [CPU_] |790| 
        CMPL      ACC,XAR7              ; [CPU_] |790| 
        B         $C$L46,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        MOVZ      AR6,AR6               ; [CPU_] |790| 
        MOVL      XAR4,#8197            ; [CPU_U] |790| 
        MOVL      ACC,XAR4              ; [CPU_] |790| 
        CMPL      ACC,XAR6              ; [CPU_] |790| 
        B         $C$L48,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        B         $C$L55,UNC            ; [CPU_] |790| 
        ; branch occurs ; [] |790| 
$C$L58:    
        CMP       AR6,#8201             ; [CPU_] |790| 
        B         $C$L59,GT             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        CMP       AR6,#8201             ; [CPU_] |790| 
        B         $C$L53,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        MOVZ      AR7,AR6               ; [CPU_] |790| 
        MOVL      XAR4,#8199            ; [CPU_U] |790| 
        MOVL      ACC,XAR4              ; [CPU_] |790| 
        CMPL      ACC,XAR7              ; [CPU_] |790| 
        B         $C$L50,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        MOVZ      AR6,AR6               ; [CPU_] |790| 
        MOVL      XAR4,#8200            ; [CPU_U] |790| 
        MOVL      ACC,XAR4              ; [CPU_] |790| 
        CMPL      ACC,XAR6              ; [CPU_] |790| 
        B         $C$L52,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        B         $C$L55,UNC            ; [CPU_] |790| 
        ; branch occurs ; [] |790| 
$C$L59:    
        MOVZ      AR7,AR6               ; [CPU_] |790| 
        MOVL      XAR4,#8202            ; [CPU_U] |790| 
        MOVL      ACC,XAR4              ; [CPU_] |790| 
        CMPL      ACC,XAR7              ; [CPU_] |790| 
        B         $C$L51,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        MOVZ      AR6,AR6               ; [CPU_] |790| 
        MOVL      XAR4,#8203            ; [CPU_U] |790| 
        MOVL      ACC,XAR4              ; [CPU_] |790| 
        CMPL      ACC,XAR6              ; [CPU_] |790| 
        B         $C$L54,EQ             ; [CPU_] |790| 
        ; branchcc occurs ; [] |790| 
        B         $C$L55,UNC            ; [CPU_] |790| 
        ; branch occurs ; [] |790| 
$C$L60:    
	.dwpsn	file "../source/CommProtocol.c",line 878,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 878 | CPSetPacketDataLength(&m_stPacketForTx, ndataSize);                    
; 880 | //!Fill the total no of packets to be sent to Sitara                   
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |878| 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |878| 
$C$DW$190	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$190, DW_AT_low_pc(0x00)
	.dwattr $C$DW$190, DW_AT_name("_CPSetPacketDataLength")
	.dwattr $C$DW$190, DW_AT_TI_call

        LCR       #_CPSetPacketDataLength ; [CPU_] |878| 
        ; call occurs [#_CPSetPacketDataLength] ; [] |878| 
	.dwpsn	file "../source/CommProtocol.c",line 881,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 881 | CPSetTotalNoOfPackets(&m_stPacketForTx);                               
; 883 | //!Calculate the CRC to the Data frame                                 
;----------------------------------------------------------------------
$C$DW$191	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$191, DW_AT_low_pc(0x00)
	.dwattr $C$DW$191, DW_AT_name("_CPSetTotalNoOfPackets")
	.dwattr $C$DW$191, DW_AT_TI_call

        LCR       #_CPSetTotalNoOfPackets ; [CPU_] |881| 
        ; call occurs [#_CPSetTotalNoOfPackets] ; [] |881| 
	.dwpsn	file "../source/CommProtocol.c",line 884,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 884 | usnCalculatedChkSum = CPCalculatePacketCRC(m_stPacketForTx);           
; 886 | //!Add the Calculated CRC to the packet                                
;----------------------------------------------------------------------
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |884| 
$C$DW$192	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$192, DW_AT_low_pc(0x00)
	.dwattr $C$DW$192, DW_AT_name("_CPCalculatePacketCRC")
	.dwattr $C$DW$192, DW_AT_TI_call

        LCR       #_CPCalculatePacketCRC ; [CPU_] |884| 
        ; call occurs [#_CPCalculatePacketCRC] ; [] |884| 
        MOV       *-SP[5],AL            ; [CPU_] |884| 
	.dwpsn	file "../source/CommProtocol.c",line 887,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 887 | CPSetPacketCRC(&m_stPacketForTx, usnCalculatedChkSum);                 
; 889 | //!Set the SPI Control status to true indicating data to be transmitted
; 890 | //!to the processor                                                    
;----------------------------------------------------------------------
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |887| 
$C$DW$193	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$193, DW_AT_low_pc(0x00)
	.dwattr $C$DW$193, DW_AT_name("_CPSetPacketCRC")
	.dwattr $C$DW$193, DW_AT_TI_call

        LCR       #_CPSetPacketCRC      ; [CPU_] |887| 
        ; call occurs [#_CPSetPacketCRC] ; [] |887| 
	.dwpsn	file "../source/CommProtocol.c",line 891,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 891 | m_bSPIWriteControlStatus = TRUE;                                       
; 893 | //!Set the transmit DMA flag to true to write data to DMA              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSPIWriteControlStatus ; [CPU_U] 
        MOVB      @_m_bSPIWriteControlStatus,#1,UNC ; [CPU_] |891| 
	.dwpsn	file "../source/CommProtocol.c",line 894,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 894 | m_bTxToDMAFlag = TRUE;                                                 
; 896 | //!If the there are no remaining packets to be transmitted to processor
; 897 | //!in case of multiple packet transmission, set the \var m_bTxInterrupt
;     | Status                                                                 
; 898 | //!to true, else set it to false                                       
;----------------------------------------------------------------------
        MOVB      @_m_bTxToDMAFlag,#1,UNC ; [CPU_] |894| 
	.dwpsn	file "../source/CommProtocol.c",line 899,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 899 | if(bRemainingPackets == FALSE)                                         
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |899| 
        B         $C$L61,NEQ            ; [CPU_] |899| 
        ; branchcc occurs ; [] |899| 
	.dwpsn	file "../source/CommProtocol.c",line 901,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 901 | m_bTxInterruptStatus = TRUE;                                           
;----------------------------------------------------------------------
        MOVB      @_m_bTxInterruptStatus,#1,UNC ; [CPU_] |901| 
	.dwpsn	file "../source/CommProtocol.c",line 902,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 903 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L62,UNC            ; [CPU_] |902| 
        ; branch occurs ; [] |902| 
$C$L61:    
	.dwpsn	file "../source/CommProtocol.c",line 905,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 905 | m_bTxInterruptStatus = FALSE;                                          
;----------------------------------------------------------------------
        MOV       @_m_bTxInterruptStatus,#0 ; [CPU_] |905| 
$C$L62:    
	.dwpsn	file "../source/CommProtocol.c",line 908,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 908 | memcpy(&m_stPrevTxPacket,&m_stPacketForTx,sizeof(m_stPacketForTx));    
; 910 | //!If the packet count is not Zero, then multiple packet to be transmit
;     | ted                                                                    
; 911 | //!to the processor. So set the \var m_bMutiplePackets to true,        
; 912 | //!else set the \var m_bMutiplePackets to false.                       
;----------------------------------------------------------------------
        MOVL      XAR4,#512             ; [CPU_U] |908| 
        MOVL      ACC,XAR4              ; [CPU_] |908| 
        MOVL      XAR5,#_m_stPacketForTx ; [CPU_U] |908| 
        MOVL      XAR4,#_m_stPrevTxPacket ; [CPU_U] |908| 
$C$DW$194	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$194, DW_AT_low_pc(0x00)
	.dwattr $C$DW$194, DW_AT_name("_memcpy")
	.dwattr $C$DW$194, DW_AT_TI_call

        LCR       #_memcpy              ; [CPU_] |908| 
        ; call occurs [#_memcpy] ; [] |908| 
	.dwpsn	file "../source/CommProtocol.c",line 913,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 913 | if(m_usnPacketCount != NULL)                                           
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       AL,@_m_usnPacketCount ; [CPU_] |913| 
        B         $C$L63,EQ             ; [CPU_] |913| 
        ; branchcc occurs ; [] |913| 
	.dwpsn	file "../source/CommProtocol.c",line 915,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 915 | m_bMutiplePackets = TRUE;                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bMutiplePackets ; [CPU_U] 
        MOVB      @_m_bMutiplePackets,#1,UNC ; [CPU_] |915| 
	.dwpsn	file "../source/CommProtocol.c",line 916,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 917 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L64,UNC            ; [CPU_] |916| 
        ; branch occurs ; [] |916| 
$C$L63:    
	.dwpsn	file "../source/CommProtocol.c",line 919,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 919 | m_bMutiplePackets = FALSE;                                             
; 922 | //sprintf(statDebugPrintBuf, "\r\n MJ: %d\n", m_stPacketForTx.usnMajorC
;     | ommand);                                                               
; 923 | //UartDebugPrint((int *)statDebugPrintBuf, 50);                        
; 924 | //sprintf(statDebugPrintBuf, "\r\n Mr: %d\n", m_stPacketForTx.usnMinorC
;     | ommand);                                                               
; 925 | //UartDebugPrint((int *)statDebugPrintBuf, 50);                        
; 927 | //sprintf(statDebugPrintBuf, "\r\n CRC %d\n", usnCalculatedChkSum);    
; 928 | //UartDebugPrint((int *)statDebugPrintBuf, 50);                        
; 930 | //!Write the framed packet to SPI                                      
;----------------------------------------------------------------------
        MOVW      DP,#_m_bMutiplePackets ; [CPU_U] 
        MOV       @_m_bMutiplePackets,#0 ; [CPU_] |919| 
$C$L64:    
	.dwpsn	file "../source/CommProtocol.c",line 931,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 931 | CPWriteDataToSPI();                                                    
; 933 | //CPEnqueue(&m_stPacketForTx);                                         
;----------------------------------------------------------------------
$C$DW$195	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$195, DW_AT_low_pc(0x00)
	.dwattr $C$DW$195, DW_AT_name("_CPWriteDataToSPI")
	.dwattr $C$DW$195, DW_AT_TI_call

        LCR       #_CPWriteDataToSPI    ; [CPU_] |931| 
        ; call occurs [#_CPWriteDataToSPI] ; [] |931| 
	.dwpsn	file "../source/CommProtocol.c",line 936,column 1,is_stmt,isa 0
$C$L65:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$170, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$170, DW_AT_TI_end_line(0x3a8)
	.dwattr $C$DW$170, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$170

	.sect	".text:_CPSetPacketCRC"
	.clink
	.global	_CPSetPacketCRC

$C$DW$197	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$197, DW_AT_name("CPSetPacketCRC")
	.dwattr $C$DW$197, DW_AT_low_pc(_CPSetPacketCRC)
	.dwattr $C$DW$197, DW_AT_high_pc(0x00)
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_CPSetPacketCRC")
	.dwattr $C$DW$197, DW_AT_external
	.dwattr $C$DW$197, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$197, DW_AT_TI_begin_line(0x3b2)
	.dwattr $C$DW$197, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$197, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommProtocol.c",line 948,column 1,is_stmt,address _CPSetPacketCRC,isa 0

	.dwfde $C$DW$CIE, _CPSetPacketCRC
$C$DW$198	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$198, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg12]

$C$DW$199	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$199, DW_AT_name("usnCalculatedCRC")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_usnCalculatedCRC")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 946 | void CPSetPacketCRC(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\           
; 947 | uint16_t usnCalculatedCRC )                                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPSetPacketCRC               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CPSetPacketCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$200	.dwtag  DW_TAG_variable
	.dwattr $C$DW$200, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$200, DW_AT_location[DW_OP_breg20 -2]

$C$DW$201	.dwtag  DW_TAG_variable
	.dwattr $C$DW$201, DW_AT_name("usnCalculatedCRC")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_usnCalculatedCRC")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$201, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 949 | //!Update the calculated CRC to the structure: CRC                     
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |948| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |948| 
	.dwpsn	file "../source/CommProtocol.c",line 950,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 950 | stPtrParsedPacket->usnCRC = usnCalculatedCRC;                          
;----------------------------------------------------------------------
        MOVL      XAR4,#509             ; [CPU_U] |950| 
        MOVL      ACC,XAR4              ; [CPU_] |950| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |950| 
        MOVL      XAR7,ACC              ; [CPU_] |950| 
        MOV       AL,*-SP[3]            ; [CPU_] |950| 
        MOV       *+XAR7[0],AL          ; [CPU_] |950| 
	.dwpsn	file "../source/CommProtocol.c",line 951,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$197, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$197, DW_AT_TI_end_line(0x3b7)
	.dwattr $C$DW$197, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$197

	.sect	".text:_CPSetTotalNoOfPackets"
	.clink
	.global	_CPSetTotalNoOfPackets

$C$DW$203	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$203, DW_AT_name("CPSetTotalNoOfPackets")
	.dwattr $C$DW$203, DW_AT_low_pc(_CPSetTotalNoOfPackets)
	.dwattr $C$DW$203, DW_AT_high_pc(0x00)
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_CPSetTotalNoOfPackets")
	.dwattr $C$DW$203, DW_AT_external
	.dwattr $C$DW$203, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$203, DW_AT_TI_begin_line(0x3c1)
	.dwattr $C$DW$203, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$203, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommProtocol.c",line 962,column 1,is_stmt,address _CPSetTotalNoOfPackets,isa 0

	.dwfde $C$DW$CIE, _CPSetTotalNoOfPackets
$C$DW$204	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$204, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 961 | void CPSetTotalNoOfPackets(ST_SPI_PACKET_FRAME *stPtrParsedPacket)     
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPSetTotalNoOfPackets        FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPSetTotalNoOfPackets:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$205	.dwtag  DW_TAG_variable
	.dwattr $C$DW$205, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$205, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 963 | //!Update the remaining packet count to the structure: Remianing packe
;     | to                                                                     
; 964 | //!to be transmitted                                                   
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |962| 
	.dwpsn	file "../source/CommProtocol.c",line 965,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 965 | stPtrParsedPacket->usnRemainingPacketsforTX = m_usnPacketCount;        
;----------------------------------------------------------------------
        MOVL      XAR4,#508             ; [CPU_U] |965| 
        MOVL      ACC,XAR4              ; [CPU_] |965| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |965| 
        MOVL      XAR7,ACC              ; [CPU_] |965| 
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       AL,@_m_usnPacketCount ; [CPU_] |965| 
        MOV       *+XAR7[0],AL          ; [CPU_] |965| 
	.dwpsn	file "../source/CommProtocol.c",line 966,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$206	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$206, DW_AT_low_pc(0x00)
	.dwattr $C$DW$206, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$203, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$203, DW_AT_TI_end_line(0x3c6)
	.dwattr $C$DW$203, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$203

	.sect	".text:_CPGetRawADCOrHeightData"
	.clink
	.global	_CPGetRawADCOrHeightData

$C$DW$207	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$207, DW_AT_name("CPGetRawADCOrHeightData")
	.dwattr $C$DW$207, DW_AT_low_pc(_CPGetRawADCOrHeightData)
	.dwattr $C$DW$207, DW_AT_high_pc(0x00)
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_CPGetRawADCOrHeightData")
	.dwattr $C$DW$207, DW_AT_external
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$207, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$207, DW_AT_TI_begin_line(0x3d2)
	.dwattr $C$DW$207, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$207, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/CommProtocol.c",line 980,column 1,is_stmt,address _CPGetRawADCOrHeightData,isa 0

	.dwfde $C$DW$CIE, _CPGetRawADCOrHeightData
$C$DW$208	.dwtag  DW_TAG_variable
	.dwattr $C$DW$208, DW_AT_name("usnPreviousIndex")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_usnPreviousIndex$3")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_addr _usnPreviousIndex$3]

$C$DW$209	.dwtag  DW_TAG_variable
	.dwattr $C$DW$209, DW_AT_name("susnNoOfPacketsToSitara")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_susnNoOfPacketsToSitara$2")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_addr _susnNoOfPacketsToSitara$2]

$C$DW$210	.dwtag  DW_TAG_variable
	.dwattr $C$DW$210, DW_AT_name("usnNewIndex")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_usnNewIndex$4")
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$210, DW_AT_location[DW_OP_addr _usnNewIndex$4]

$C$DW$211	.dwtag  DW_TAG_variable
	.dwattr $C$DW$211, DW_AT_name("uslDataSize")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_uslDataSize$5")
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$211, DW_AT_location[DW_OP_addr _uslDataSize$5]

$C$DW$212	.dwtag  DW_TAG_variable
	.dwattr $C$DW$212, DW_AT_name("usnRemainder")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_usnRemainder$1")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_addr _usnRemainder$1]

$C$DW$213	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$213, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_reg12]

$C$DW$214	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$214, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_reg0]

$C$DW$215	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$215, DW_AT_name("bRawADCDataReq")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_bRawADCDataReq")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$215, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 978 | uint16_t CPGetRawADCOrHeightData(ST_SPI_PACKET_FRAME *stPtrParsedPacket
;     | ,\                                                                     
; 979 | uint16_t usnSubCmd, bool_t bRawADCDataReq)                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPGetRawADCOrHeightData      FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_CPGetRawADCOrHeightData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$216	.dwtag  DW_TAG_variable
	.dwattr $C$DW$216, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$216, DW_AT_location[DW_OP_breg20 -4]

$C$DW$217	.dwtag  DW_TAG_variable
	.dwattr $C$DW$217, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$217, DW_AT_location[DW_OP_breg20 -5]

$C$DW$218	.dwtag  DW_TAG_variable
	.dwattr $C$DW$218, DW_AT_name("bRawADCDataReq")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_bRawADCDataReq")
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$218, DW_AT_location[DW_OP_breg20 -6]

$C$DW$219	.dwtag  DW_TAG_variable
	.dwattr $C$DW$219, DW_AT_name("usnDataLength")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$219, DW_AT_location[DW_OP_breg20 -7]

;----------------------------------------------------------------------
; 981 | //!Set the data length to zero by default                              
;----------------------------------------------------------------------
        MOV       *-SP[6],AH            ; [CPU_] |980| 
        MOV       *-SP[5],AL            ; [CPU_] |980| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |980| 
	.dwpsn	file "../source/CommProtocol.c",line 982,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 982 | uint16_t usnDataLength = 0;                                            
; 983 | //!Reminder is used to store the data length of the final packet when  
; 984 | //!multiple packets are transmitted                                    
; 985 | //static uint16_t usnRemainder = 0;                                    
; 986 | static uint32_t usnRemainder = 0;    //QA_C                            
; 988 | //uint16_t usncount = 0;                                               
; 989 | //!Set no. of packets to be sent to Sitara to Zero by default          
; 990 | //static uint16_t susnNoOfPacketsToSitara = 0;                         
; 991 | static uint32_t susnNoOfPacketsToSitara = 0;   //QA_C                  
; 992 | //!Previous index is used to obtain the Raw data address location      
; 993 | //!Set the previous index to zero by default                           
; 994 | static uint32_t usnPreviousIndex = 0;                                  
; 995 | //!Set the new index to Zero by default                                
; 996 | static uint32_t usnNewIndex =0;                                        
; 997 | //!Set the data size to Zero by default                                
; 998 | static uint32_t uslDataSize = 0;                                       
; 1000 | //!Check if packet is pending to be sent to Sitara                     
;----------------------------------------------------------------------
        MOV       *-SP[7],#0            ; [CPU_] |982| 
	.dwpsn	file "../source/CommProtocol.c",line 1001,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1001 | if(susnNoOfPacketsToSitara == NULL)                                    
; 1003 |     //!Check if next second Raw data to be sent to Sitara              
;----------------------------------------------------------------------
        MOVW      DP,#_susnNoOfPacketsToSitara$2 ; [CPU_U] 
        MOVL      ACC,@_susnNoOfPacketsToSitara$2 ; [CPU_] |1001| 
        B         $C$L72,NEQ            ; [CPU_] |1001| 
        ; branchcc occurs ; [] |1001| 
	.dwpsn	file "../source/CommProtocol.c",line 1004,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1004 | if(g_bNextSecRawDataReq == TRUE)                                       
; 1006 |     //!Get the index of the next second data to be transmitted to Proce
;     | ssor                                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_bNextSecRawDataReq ; [CPU_U] 
        MOV       AL,@_g_bNextSecRawDataReq ; [CPU_] |1004| 
        CMPB      AL,#1                 ; [CPU_] |1004| 
        B         $C$L66,NEQ            ; [CPU_] |1004| 
        ; branchcc occurs ; [] |1004| 
	.dwpsn	file "../source/CommProtocol.c",line 1007,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1007 | usnPreviousIndex = CPGetNextIndexForADCData(m_eTypeofCell);            
; 1008 | //!Set next second Raw Data to False.                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOV       AL,@_m_eTypeofCell    ; [CPU_] |1007| 
$C$DW$220	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$220, DW_AT_low_pc(0x00)
	.dwattr $C$DW$220, DW_AT_name("_CPGetNextIndexForADCData")
	.dwattr $C$DW$220, DW_AT_TI_call

        LCR       #_CPGetNextIndexForADCData ; [CPU_] |1007| 
        ; call occurs [#_CPGetNextIndexForADCData] ; [] |1007| 
        MOVW      DP,#_usnPreviousIndex$3 ; [CPU_U] 
        MOVL      @_usnPreviousIndex$3,ACC ; [CPU_] |1007| 
	.dwpsn	file "../source/CommProtocol.c",line 1009,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1009 | g_bNextSecRawDataReq = FALSE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bNextSecRawDataReq ; [CPU_U] 
        MOV       @_g_bNextSecRawDataReq,#0 ; [CPU_] |1009| 
	.dwpsn	file "../source/CommProtocol.c",line 1010,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1011 | else                                                                   
; 1013 |     //!If next second data need not to be sent to Sitara, set the      
; 1014 |     //!previous index to zero                                          
;----------------------------------------------------------------------
        B         $C$L67,UNC            ; [CPU_] |1010| 
        ; branch occurs ; [] |1010| 
$C$L66:    
	.dwpsn	file "../source/CommProtocol.c",line 1015,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1015 | usnPreviousIndex = 0;                                                  
; 1017 | //!Set new index and the reminder to Zero                              
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1015| 
        MOVW      DP,#_usnPreviousIndex$3 ; [CPU_U] 
        MOVL      @_usnPreviousIndex$3,ACC ; [CPU_] |1015| 
$C$L67:    
	.dwpsn	file "../source/CommProtocol.c",line 1018,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1018 | usnNewIndex = 0;                                                       
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1018| 
        MOVW      DP,#_usnNewIndex$4    ; [CPU_U] 
        MOVL      @_usnNewIndex$4,ACC   ; [CPU_] |1018| 
	.dwpsn	file "../source/CommProtocol.c",line 1019,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1019 | usnRemainder =0;                                                       
; 1021 | //!Set the data length of the ADC Raw Data request.                    
; 1022 | //!If it is not the last second  of the RAW data, set the data length  
; 1023 | //!as \def RAW_ADC_DATA_SIZE, else set the data length as per the data 
; 1024 | //!data available in the last second                                   
;----------------------------------------------------------------------
        MOVL      @_usnRemainder$1,ACC  ; [CPU_] |1019| 
	.dwpsn	file "../source/CommProtocol.c",line 1025,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1025 | if(bRawADCDataReq == FALSE)                                            
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1025| 
        B         $C$L68,NEQ            ; [CPU_] |1025| 
        ; branchcc occurs ; [] |1025| 
	.dwpsn	file "../source/CommProtocol.c",line 1027,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1027 | uslDataSize = SysGetADCDataLength(usnSubCmd);                          
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |1027| 
$C$DW$221	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$221, DW_AT_low_pc(0x00)
	.dwattr $C$DW$221, DW_AT_name("_SysGetADCDataLength")
	.dwattr $C$DW$221, DW_AT_TI_call

        LCR       #_SysGetADCDataLength ; [CPU_] |1027| 
        ; call occurs [#_SysGetADCDataLength] ; [] |1027| 
        MOVW      DP,#_uslDataSize$5    ; [CPU_U] 
        MOVL      @_uslDataSize$5,ACC   ; [CPU_] |1027| 
	.dwpsn	file "../source/CommProtocol.c",line 1028,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1029 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L69,UNC            ; [CPU_] |1028| 
        ; branch occurs ; [] |1028| 
$C$L68:    
	.dwpsn	file "../source/CommProtocol.c",line 1031,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1031 | uslDataSize = RAW_ADC_DATA_SIZE ;                                      
; 1033 | //!If it is last packet to be transmitted to processor, no multiple pac
;     | ket                                                                    
; 1034 | //!to be transmitted to sitara, the packet count is set to zero, the   
; 1035 | //!last multiple packet is set to false to ensure no multiple packet is
;     |  transmitted                                                           
;----------------------------------------------------------------------
        MOVL      XAR4,#1004000         ; [CPU_U] |1031| 
        MOVL      @_uslDataSize$5,XAR4  ; [CPU_] |1031| 
$C$L69:    
	.dwpsn	file "../source/CommProtocol.c",line 1036,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1036 | if(uslDataSize<MAXIMUM_DATA_SIZE)                                      
;----------------------------------------------------------------------
        MOVL      XAR4,#502             ; [CPU_U] |1036| 
        MOVL      ACC,XAR4              ; [CPU_] |1036| 
        CMPL      ACC,@_uslDataSize$5   ; [CPU_] |1036| 
        B         $C$L70,LOS            ; [CPU_] |1036| 
        ; branchcc occurs ; [] |1036| 
	.dwpsn	file "../source/CommProtocol.c",line 1038,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1038 | susnNoOfPacketsToSitara = 0;                                           
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1038| 
        MOVL      @_susnNoOfPacketsToSitara$2,ACC ; [CPU_] |1038| 
	.dwpsn	file "../source/CommProtocol.c",line 1039,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1039 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1039| 
	.dwpsn	file "../source/CommProtocol.c",line 1040,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1040 | m_bLastMultiplePacket = FALSE;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_bLastMultiplePacket ; [CPU_U] 
        MOV       @_m_bLastMultiplePacket,#0 ; [CPU_] |1040| 
	.dwpsn	file "../source/CommProtocol.c",line 1041,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1042 | else                                                                   
; 1044 |     //!If it is not last packet, check for the reminder.               
; 1045 |     //!Set the number of packets and the packet count to Sitara to be s
;     | ent                                                                    
; 1046 |         //!Every packet contains 502 WORD (1008 bytes) of data except t
;     | he last                                                                
; 1047 |         //!packet to be sent                                           
;----------------------------------------------------------------------
        B         $C$L73,UNC            ; [CPU_] |1041| 
        ; branch occurs ; [] |1041| 
$C$L70:    
	.dwpsn	file "../source/CommProtocol.c",line 1048,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1048 | usnRemainder = uslDataSize % MAXIMUM_DATA_SIZE;                        
;----------------------------------------------------------------------
        MOVL      P,@_uslDataSize$5     ; [CPU_] |1048| 
        MOVB      ACC,#0                ; [CPU_] |1048| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |1048| 
        MOVL      @_usnRemainder$1,ACC  ; [CPU_] |1048| 
	.dwpsn	file "../source/CommProtocol.c",line 1049,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1049 | if(usnRemainder == 0)                                                  
;----------------------------------------------------------------------
        MOVL      ACC,@_usnRemainder$1  ; [CPU_] |1049| 
        B         $C$L71,NEQ            ; [CPU_] |1049| 
        ; branchcc occurs ; [] |1049| 
	.dwpsn	file "../source/CommProtocol.c",line 1051,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1051 | susnNoOfPacketsToSitara = uslDataSize /MAXIMUM_DATA_SIZE;              
;----------------------------------------------------------------------
        MOVL      P,@_uslDataSize$5     ; [CPU_] |1051| 
        MOVB      ACC,#0                ; [CPU_] |1051| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |1051| 
        MOVL      @_susnNoOfPacketsToSitara$2,P ; [CPU_] |1051| 
	.dwpsn	file "../source/CommProtocol.c",line 1052,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1052 | m_usnPacketCount = susnNoOfPacketsToSitara -1;                         
;----------------------------------------------------------------------
        MOV       AL,@_susnNoOfPacketsToSitara$2 ; [CPU_] |1052| 
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        ADDB      AL,#-1                ; [CPU_] |1052| 
        MOV       @_m_usnPacketCount,AL ; [CPU_] |1052| 
	.dwpsn	file "../source/CommProtocol.c",line 1053,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1054 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L73,UNC            ; [CPU_] |1053| 
        ; branch occurs ; [] |1053| 
$C$L71:    
	.dwpsn	file "../source/CommProtocol.c",line 1056,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1056 | susnNoOfPacketsToSitara = uslDataSize /MAXIMUM_DATA_SIZE;              
;----------------------------------------------------------------------
        MOVL      P,@_uslDataSize$5     ; [CPU_] |1056| 
        MOVB      ACC,#0                ; [CPU_] |1056| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |1056| 
        MOVL      @_susnNoOfPacketsToSitara$2,P ; [CPU_] |1056| 
	.dwpsn	file "../source/CommProtocol.c",line 1057,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1057 | susnNoOfPacketsToSitara =  susnNoOfPacketsToSitara + 1;                
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1057| 
        ADDL      @_susnNoOfPacketsToSitara$2,ACC ; [CPU_] |1057| 
	.dwpsn	file "../source/CommProtocol.c",line 1058,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1058 | m_usnPacketCount = susnNoOfPacketsToSitara - 1;                        
;----------------------------------------------------------------------
        MOV       AL,@_susnNoOfPacketsToSitara$2 ; [CPU_] |1058| 
        ADDB      AL,#-1                ; [CPU_] |1058| 
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,AL ; [CPU_] |1058| 
	.dwpsn	file "../source/CommProtocol.c",line 1061,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1062 | else                                                                   
; 1064 |     //!Reduce the count of no. of packets to Sitara after transmission
;     | of                                                                     
; 1065 |     //!every packet                                                    
;----------------------------------------------------------------------
        B         $C$L73,UNC            ; [CPU_] |1061| 
        ; branch occurs ; [] |1061| 
$C$L72:    
	.dwpsn	file "../source/CommProtocol.c",line 1066,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 1066 | susnNoOfPacketsToSitara--;                                             
; 1067 |     //Update m_usnPacketCount to set the remaining chunks              
; 1068 | //!Update the packet count after sending the packet to Sitara          
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1066| 
        SUBL      @_susnNoOfPacketsToSitara$2,ACC ; [CPU_] |1066| 
	.dwpsn	file "../source/CommProtocol.c",line 1069,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1069 | m_usnPacketCount = susnNoOfPacketsToSitara - 1;                        
; 1072 | //!Update the data length to maximum if it is not the last packet to be
; 1073 | //!transmitted when multiple packets are transmitted.                  
; 1074 | //!else, update the data length with the data left to be transmitted to
;     |  the                                                                   
; 1075 | //!processor                                                           
;----------------------------------------------------------------------
        MOV       AL,@_susnNoOfPacketsToSitara$2 ; [CPU_] |1069| 
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        ADDB      AL,#-1                ; [CPU_] |1069| 
        MOV       @_m_usnPacketCount,AL ; [CPU_] |1069| 
$C$L73:    
	.dwpsn	file "../source/CommProtocol.c",line 1076,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1076 | if(m_usnPacketCount != NULL)                                           
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       AL,@_m_usnPacketCount ; [CPU_] |1076| 
        B         $C$L74,EQ             ; [CPU_] |1076| 
        ; branchcc occurs ; [] |1076| 
	.dwpsn	file "../source/CommProtocol.c",line 1078,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1078 | usnDataLength = MAXIMUM_DATA_SIZE;                                     
;----------------------------------------------------------------------
        MOV       *-SP[7],#502          ; [CPU_] |1078| 
	.dwpsn	file "../source/CommProtocol.c",line 1079,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1080 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L77,UNC            ; [CPU_] |1079| 
        ; branch occurs ; [] |1079| 
$C$L74:    
	.dwpsn	file "../source/CommProtocol.c",line 1082,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1082 | if(uslDataSize<MAXIMUM_DATA_SIZE)                                      
;----------------------------------------------------------------------
        MOVL      XAR4,#502             ; [CPU_U] |1082| 
        MOVW      DP,#_uslDataSize$5    ; [CPU_U] 
        MOVL      ACC,XAR4              ; [CPU_] |1082| 
        CMPL      ACC,@_uslDataSize$5   ; [CPU_] |1082| 
        B         $C$L75,LOS            ; [CPU_] |1082| 
        ; branchcc occurs ; [] |1082| 
	.dwpsn	file "../source/CommProtocol.c",line 1084,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1084 | usnDataLength = (uint16_t)uslDataSize;                                 
;----------------------------------------------------------------------
        MOV       AL,@_uslDataSize$5    ; [CPU_] |1084| 
        MOV       *-SP[7],AL            ; [CPU_] |1084| 
	.dwpsn	file "../source/CommProtocol.c",line 1085,column 3,is_stmt,isa 0
        B         $C$L77,UNC            ; [CPU_] |1085| 
        ; branch occurs ; [] |1085| 
$C$L75:    
	.dwpsn	file "../source/CommProtocol.c",line 1086,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1086 | else if(usnRemainder != NULL)                                          
;----------------------------------------------------------------------
        MOVL      ACC,@_usnRemainder$1  ; [CPU_] |1086| 
        B         $C$L76,EQ             ; [CPU_] |1086| 
        ; branchcc occurs ; [] |1086| 
	.dwpsn	file "../source/CommProtocol.c",line 1088,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1088 | usnDataLength = usnRemainder;                                          
;----------------------------------------------------------------------
        MOV       AL,@_usnRemainder$1   ; [CPU_] |1088| 
        MOV       *-SP[7],AL            ; [CPU_] |1088| 
	.dwpsn	file "../source/CommProtocol.c",line 1089,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1090 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L77,UNC            ; [CPU_] |1089| 
        ; branch occurs ; [] |1089| 
$C$L76:    
	.dwpsn	file "../source/CommProtocol.c",line 1092,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1092 | usnDataLength = MAXIMUM_DATA_SIZE;                                     
; 1096 | //!Update the index to obtain the data from the SDRAM for the raw data
;     | and                                                                    
; 1097 | //!for the pulse height.                                               
;----------------------------------------------------------------------
        MOV       *-SP[7],#502          ; [CPU_] |1092| 
$C$L77:    
	.dwpsn	file "../source/CommProtocol.c",line 1098,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1098 | if(bRawADCDataReq == FALSE)                                            
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1098| 
        B         $C$L78,NEQ            ; [CPU_] |1098| 
        ; branchcc occurs ; [] |1098| 
	.dwpsn	file "../source/CommProtocol.c",line 1100,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1100 | usnNewIndex =  SysGetPulseHeightDataFrmSDRAM(m_eTypeofCell, \          
; 1101 |         stPtrParsedPacket->arrusnData,usnPreviousIndex,usnDataLength); 
;----------------------------------------------------------------------
        MOVZ      AR6,*-SP[7]           ; [CPU_] |1100| 
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      ACC,#6                ; [CPU_] |1100| 
        MOVZ      AR5,@_m_eTypeofCell   ; [CPU_] |1100| 
        MOV       *-SP[1],AR6           ; [CPU_] |1100| 
        MOVW      DP,#_usnPreviousIndex$3 ; [CPU_U] 
        ADDL      ACC,*-SP[4]           ; [CPU_] |1100| 
        MOVL      XAR4,ACC              ; [CPU_] |1100| 
        MOVL      ACC,@_usnPreviousIndex$3 ; [CPU_] |1100| 
$C$DW$222	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$222, DW_AT_low_pc(0x00)
	.dwattr $C$DW$222, DW_AT_name("_SysGetPulseHeightDataFrmSDRAM")
	.dwattr $C$DW$222, DW_AT_TI_call

        LCR       #_SysGetPulseHeightDataFrmSDRAM ; [CPU_] |1100| 
        ; call occurs [#_SysGetPulseHeightDataFrmSDRAM] ; [] |1100| 
        MOVW      DP,#_usnNewIndex$4    ; [CPU_U] 
        MOVL      @_usnNewIndex$4,ACC   ; [CPU_] |1100| 
	.dwpsn	file "../source/CommProtocol.c",line 1102,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1103 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L79,UNC            ; [CPU_] |1102| 
        ; branch occurs ; [] |1102| 
$C$L78:    
	.dwpsn	file "../source/CommProtocol.c",line 1105,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1105 | usnNewIndex =  SysGetADCRawDataFrmSDRAM(m_eTypeofCell, \               
; 1106 |         stPtrParsedPacket->arrusnData,usnPreviousIndex,usnDataLength); 
; 1109 | //!If it is last packet to be sent to Sitara, update the cell type and
;     | new index                                                              
; 1110 | //!and reset all the associated variables                              
;----------------------------------------------------------------------
        MOVZ      AR6,*-SP[7]           ; [CPU_] |1105| 
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      ACC,#6                ; [CPU_] |1105| 
        MOVZ      AR5,@_m_eTypeofCell   ; [CPU_] |1105| 
        MOV       *-SP[1],AR6           ; [CPU_] |1105| 
        MOVW      DP,#_usnPreviousIndex$3 ; [CPU_U] 
        ADDL      ACC,*-SP[4]           ; [CPU_] |1105| 
        MOVL      XAR4,ACC              ; [CPU_] |1105| 
        MOVL      ACC,@_usnPreviousIndex$3 ; [CPU_] |1105| 
$C$DW$223	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$223, DW_AT_low_pc(0x00)
	.dwattr $C$DW$223, DW_AT_name("_SysGetADCRawDataFrmSDRAM")
	.dwattr $C$DW$223, DW_AT_TI_call

        LCR       #_SysGetADCRawDataFrmSDRAM ; [CPU_] |1105| 
        ; call occurs [#_SysGetADCRawDataFrmSDRAM] ; [] |1105| 
        MOVW      DP,#_usnNewIndex$4    ; [CPU_U] 
        MOVL      @_usnNewIndex$4,ACC   ; [CPU_] |1105| 
$C$L79:    
	.dwpsn	file "../source/CommProtocol.c",line 1111,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1111 | if(susnNoOfPacketsToSitara == 1)                                       
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1111| 
        CMPL      ACC,@_susnNoOfPacketsToSitara$2 ; [CPU_] |1111| 
        B         $C$L81,NEQ            ; [CPU_] |1111| 
        ; branchcc occurs ; [] |1111| 
	.dwpsn	file "../source/CommProtocol.c",line 1113,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1113 | if(bRawADCDataReq == TRUE)                                             
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1113| 
        CMPB      AL,#1                 ; [CPU_] |1113| 
        B         $C$L80,NEQ            ; [CPU_] |1113| 
        ; branchcc occurs ; [] |1113| 
	.dwpsn	file "../source/CommProtocol.c",line 1115,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1115 | CPSetNextIndexForADCData(m_eTypeofCell, usnNewIndex);                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVZ      AR4,@_m_eTypeofCell   ; [CPU_] |1115| 
        MOVW      DP,#_usnNewIndex$4    ; [CPU_U] 
        MOVL      ACC,@_usnNewIndex$4   ; [CPU_] |1115| 
$C$DW$224	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$224, DW_AT_low_pc(0x00)
	.dwattr $C$DW$224, DW_AT_name("_CPSetNextIndexForADCData")
	.dwattr $C$DW$224, DW_AT_TI_call

        LCR       #_CPSetNextIndexForADCData ; [CPU_] |1115| 
        ; call occurs [#_CPSetNextIndexForADCData] ; [] |1115| 
$C$L80:    
	.dwpsn	file "../source/CommProtocol.c",line 1117,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1117 | usnPreviousIndex = 0;                                                  
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1117| 
        MOVW      DP,#_usnPreviousIndex$3 ; [CPU_U] 
        MOVL      @_usnPreviousIndex$3,ACC ; [CPU_] |1117| 
	.dwpsn	file "../source/CommProtocol.c",line 1118,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1118 | usnNewIndex = 0;                                                       
;----------------------------------------------------------------------
        MOVL      @_usnNewIndex$4,ACC   ; [CPU_] |1118| 
	.dwpsn	file "../source/CommProtocol.c",line 1119,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1119 | usnRemainder = 0;                                                      
;----------------------------------------------------------------------
        MOVL      @_usnRemainder$1,ACC  ; [CPU_] |1119| 
	.dwpsn	file "../source/CommProtocol.c",line 1120,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1120 | susnNoOfPacketsToSitara =0;                                            
;----------------------------------------------------------------------
        MOVL      @_susnNoOfPacketsToSitara$2,ACC ; [CPU_] |1120| 
	.dwpsn	file "../source/CommProtocol.c",line 1121,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1121 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1121| 
	.dwpsn	file "../source/CommProtocol.c",line 1122,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1122 | m_bLastMultiplePacket = TRUE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_m_bLastMultiplePacket ; [CPU_U] 
        MOVB      @_m_bLastMultiplePacket,#1,UNC ; [CPU_] |1122| 
	.dwpsn	file "../source/CommProtocol.c",line 1123,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1123 | uslDataSize = 0;                                                       
;----------------------------------------------------------------------
        MOVL      @_uslDataSize$5,ACC   ; [CPU_] |1123| 
	.dwpsn	file "../source/CommProtocol.c",line 1124,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1125 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L82,UNC            ; [CPU_] |1124| 
        ; branch occurs ; [] |1124| 
$C$L81:    
	.dwpsn	file "../source/CommProtocol.c",line 1127,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1127 | usnPreviousIndex = usnNewIndex;                                        
;----------------------------------------------------------------------
        MOVL      ACC,@_usnNewIndex$4   ; [CPU_] |1127| 
        MOVL      @_usnPreviousIndex$3,ACC ; [CPU_] |1127| 
	.dwpsn	file "../source/CommProtocol.c",line 1128,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1128 | m_bLastMultiplePacket = FALSE;                                         
; 1131 | //!Return the data length to be sent to Sitara                         
;----------------------------------------------------------------------
        MOV       @_m_bLastMultiplePacket,#0 ; [CPU_] |1128| 
$C$L82:    
	.dwpsn	file "../source/CommProtocol.c",line 1132,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1132 | return usnDataLength;//return here                                     
;----------------------------------------------------------------------
        MOV       AL,*-SP[7]            ; [CPU_] |1132| 
	.dwpsn	file "../source/CommProtocol.c",line 1133,column 1,is_stmt,isa 0
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$225	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$225, DW_AT_low_pc(0x00)
	.dwattr $C$DW$225, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$207, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$207, DW_AT_TI_end_line(0x46d)
	.dwattr $C$DW$207, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$207

	.sect	".text:_CPGetHeightDataForReTransmission"
	.clink
	.global	_CPGetHeightDataForReTransmission

$C$DW$226	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$226, DW_AT_name("CPGetHeightDataForReTransmission")
	.dwattr $C$DW$226, DW_AT_low_pc(_CPGetHeightDataForReTransmission)
	.dwattr $C$DW$226, DW_AT_high_pc(0x00)
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_CPGetHeightDataForReTransmission")
	.dwattr $C$DW$226, DW_AT_external
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$226, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$226, DW_AT_TI_begin_line(0x478)
	.dwattr $C$DW$226, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$226, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "../source/CommProtocol.c",line 1146,column 1,is_stmt,address _CPGetHeightDataForReTransmission,isa 0

	.dwfde $C$DW$CIE, _CPGetHeightDataForReTransmission
$C$DW$227	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$227, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$227, DW_AT_location[DW_OP_reg12]

$C$DW$228	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$228, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$228, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1144 | uint16_t CPGetHeightDataForReTransmission(ST_SPI_PACKET_FRAME *stPtrPar
;     | sedPacket,\                                                            
; 1145 | uint16_t usnSubCmd)                                                    
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPGetHeightDataForReTransmission FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_CPGetHeightDataForReTransmission:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$229	.dwtag  DW_TAG_variable
	.dwattr $C$DW$229, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$229, DW_AT_location[DW_OP_breg20 -4]

$C$DW$230	.dwtag  DW_TAG_variable
	.dwattr $C$DW$230, DW_AT_name("iRemainder")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_iRemainder")
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$230, DW_AT_location[DW_OP_breg20 -6]

$C$DW$231	.dwtag  DW_TAG_variable
	.dwattr $C$DW$231, DW_AT_name("iTotalNoOfPackets")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_iTotalNoOfPackets")
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$231, DW_AT_location[DW_OP_breg20 -8]

$C$DW$232	.dwtag  DW_TAG_variable
	.dwattr $C$DW$232, DW_AT_name("usnPreviousIndex")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_usnPreviousIndex")
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$232, DW_AT_location[DW_OP_breg20 -10]

$C$DW$233	.dwtag  DW_TAG_variable
	.dwattr $C$DW$233, DW_AT_name("uslDataSize")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_uslDataSize")
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$233, DW_AT_location[DW_OP_breg20 -12]

$C$DW$234	.dwtag  DW_TAG_variable
	.dwattr $C$DW$234, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$234, DW_AT_location[DW_OP_breg20 -13]

$C$DW$235	.dwtag  DW_TAG_variable
	.dwattr $C$DW$235, DW_AT_name("usnDataLength")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$235, DW_AT_location[DW_OP_breg20 -14]

$C$DW$236	.dwtag  DW_TAG_variable
	.dwattr $C$DW$236, DW_AT_name("usncount")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_usncount")
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$236, DW_AT_location[DW_OP_breg20 -15]

;----------------------------------------------------------------------
; 1147 | //!Set the data length to zero by default                              
;----------------------------------------------------------------------
        MOV       *-SP[13],AL           ; [CPU_] |1146| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |1146| 
	.dwpsn	file "../source/CommProtocol.c",line 1148,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 1148 | uint16_t usnDataLength = 0;                                            
; 1149 | //!Reminder is used to store the data length of the final packet when  
; 1150 | //!multiple packets are transmitted                                    
; 1151 | //uint16_t iRemainder = 0;                                             
;----------------------------------------------------------------------
        MOV       *-SP[14],#0           ; [CPU_] |1148| 
	.dwpsn	file "../source/CommProtocol.c",line 1152,column 22,is_stmt,isa 0
;----------------------------------------------------------------------
; 1152 | uint32_t iRemainder = 0;     //QA_C                                    
; 1153 | //!total no. of packets to be re-transmitted to Sitara                 
; 1154 | //uint16_t iTotalNoOfPackets = 0;                                      
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1152| 
        MOVL      *-SP[6],ACC           ; [CPU_] |1152| 
	.dwpsn	file "../source/CommProtocol.c",line 1155,column 29,is_stmt,isa 0
;----------------------------------------------------------------------
; 1155 | uint32_t iTotalNoOfPackets = 0;      //QA_C                            
; 1156 | //!Previous index is used to obtain the Raw data address location      
; 1157 | //!Set the previous index to zero by default                           
;----------------------------------------------------------------------
        MOVL      *-SP[8],ACC           ; [CPU_] |1155| 
	.dwpsn	file "../source/CommProtocol.c",line 1158,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 1158 | uint32_t usnPreviousIndex =0;                                          
; 1159 | //!Set the data size to Zero by default                                
;----------------------------------------------------------------------
        MOVL      *-SP[10],ACC          ; [CPU_] |1158| 
	.dwpsn	file "../source/CommProtocol.c",line 1160,column 23,is_stmt,isa 0
;----------------------------------------------------------------------
; 1160 | uint32_t uslDataSize = 0;                                              
; 1161 | //!Count to run the loop to update the structure with zero where data i
;     | s not available                                                        
;----------------------------------------------------------------------
        MOVL      *-SP[12],ACC          ; [CPU_] |1160| 
	.dwpsn	file "../source/CommProtocol.c",line 1162,column 20,is_stmt,isa 0
;----------------------------------------------------------------------
; 1162 | uint16_t usncount = 0;                                                 
; 1164 | //!Udpate the previous index with the data array                       
;----------------------------------------------------------------------
        MOV       *-SP[15],#0           ; [CPU_] |1162| 
	.dwpsn	file "../source/CommProtocol.c",line 1165,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1165 | usnPreviousIndex = m_stReceivedPacketFrame.arrusnData[0] * MAXIMUM_DATA
;     | _SIZE;                                                                 
; 1166 | //!Update the data size with for the data to be sent to Processor      
;----------------------------------------------------------------------
        MOV       T,#502                ; [CPU_] |1165| 
        MOVW      DP,#_m_stReceivedPacketFrame+6 ; [CPU_U] 
        MPY       ACC,T,@_m_stReceivedPacketFrame+6 ; [CPU_] |1165| 
        MOVU      ACC,AL                ; [CPU_] |1165| 
        MOVL      *-SP[10],ACC          ; [CPU_] |1165| 
	.dwpsn	file "../source/CommProtocol.c",line 1167,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1167 | uslDataSize = SysGetADCDataLength(stPtrParsedPacket->usnMinorCommand); 
; 1169 | //!If it is not last packet, check for the reminder.                   
; 1170 | //!Set the number of packets and the packet count to Sitara to be sent 
; 1171 | //!Every packet contains 502 WORD (1008 bytes) of data except the last 
; 1172 | //!packet to be sent                                                   
; 1173 | //every packet can contain 502 WORD of data.                           
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[5]          ; [CPU_] |1167| 
$C$DW$237	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$237, DW_AT_low_pc(0x00)
	.dwattr $C$DW$237, DW_AT_name("_SysGetADCDataLength")
	.dwattr $C$DW$237, DW_AT_TI_call

        LCR       #_SysGetADCDataLength ; [CPU_] |1167| 
        ; call occurs [#_SysGetADCDataLength] ; [] |1167| 
        MOVL      *-SP[12],ACC          ; [CPU_] |1167| 
	.dwpsn	file "../source/CommProtocol.c",line 1174,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1174 | iRemainder = uslDataSize % MAXIMUM_DATA_SIZE;                          
; 1175 | //!Update the total number of packets depending on the reminder.       
; 1176 | //!If the reminder is Zero, total no of packet to be transmitted to pro
;     | cessor                                                                 
; 1177 | //!is calculated as (Data Size/Maximum Data Packet)                    
; 1178 | //!if the reminder is non Zero, then the total no. of packet to be tran
;     | smitted                                                                
; 1179 | //!is  (Data Size/Maximum Data Packet) + 1                             
;----------------------------------------------------------------------
        MOVL      XAR4,#502             ; [CPU_U] |1174| 
        MOVB      ACC,#0                ; [CPU_] |1174| 
        MOVL      P,*-SP[12]            ; [CPU_] |1174| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |1174| 
        MOVL      *-SP[6],ACC           ; [CPU_] |1174| 
	.dwpsn	file "../source/CommProtocol.c",line 1180,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1180 | if(iRemainder == 0)                                                    
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[6]           ; [CPU_] |1180| 
        B         $C$L83,NEQ            ; [CPU_] |1180| 
        ; branchcc occurs ; [] |1180| 
	.dwpsn	file "../source/CommProtocol.c",line 1182,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1182 | iTotalNoOfPackets = uslDataSize / MAXIMUM_DATA_SIZE;                   
;----------------------------------------------------------------------
        MOVL      P,*-SP[12]            ; [CPU_] |1182| 
        MOVB      ACC,#0                ; [CPU_] |1182| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |1182| 
        MOVL      *-SP[8],P             ; [CPU_] |1182| 
	.dwpsn	file "../source/CommProtocol.c",line 1183,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1184 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L84,UNC            ; [CPU_] |1183| 
        ; branch occurs ; [] |1183| 
$C$L83:    
	.dwpsn	file "../source/CommProtocol.c",line 1186,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1186 | iTotalNoOfPackets = uslDataSize / MAXIMUM_DATA_SIZE;                   
;----------------------------------------------------------------------
        MOVL      P,*-SP[12]            ; [CPU_] |1186| 
        MOVB      ACC,#0                ; [CPU_] |1186| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |1186| 
        MOVL      *-SP[8],P             ; [CPU_] |1186| 
	.dwpsn	file "../source/CommProtocol.c",line 1187,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1187 | iTotalNoOfPackets =  iTotalNoOfPackets + 1;                            
; 1190 | //!Update the data length depending upon the data size to be transmitte
;     | d.                                                                     
; 1191 | //!If it is last packet and the data to be transmitted is less than the
; 1192 | //!Maximum data size, then update it with the pending data.            
; 1193 | //!If it is no tthe last packet then update it with the maximum data pa
;     | cket                                                                   
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1187| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |1187| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1187| 
$C$L84:    
	.dwpsn	file "../source/CommProtocol.c",line 1194,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1194 | if(uslDataSize<MAXIMUM_DATA_SIZE)                                      
;----------------------------------------------------------------------
        MOVL      ACC,XAR4              ; [CPU_] |1194| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |1194| 
        B         $C$L85,LOS            ; [CPU_] |1194| 
        ; branchcc occurs ; [] |1194| 
	.dwpsn	file "../source/CommProtocol.c",line 1196,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1196 | usnDataLength = (uint16_t)uslDataSize;                                 
;----------------------------------------------------------------------
        MOV       AL,*-SP[12]           ; [CPU_] |1196| 
        MOV       *-SP[14],AL           ; [CPU_] |1196| 
	.dwpsn	file "../source/CommProtocol.c",line 1197,column 2,is_stmt,isa 0
        B         $C$L87,UNC            ; [CPU_] |1197| 
        ; branch occurs ; [] |1197| 
$C$L85:    
	.dwpsn	file "../source/CommProtocol.c",line 1198,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1198 | else if(m_stReceivedPacketFrame.arrusnData[0] == (iTotalNoOfPackets - 1
;     | ))                                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+6 ; [CPU_U] 
        MOVZ      AR6,@_m_stReceivedPacketFrame+6 ; [CPU_] |1198| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |1198| 
        SUBB      ACC,#1                ; [CPU_] |1198| 
        CMPL      ACC,XAR6              ; [CPU_] |1198| 
        B         $C$L86,NEQ            ; [CPU_] |1198| 
        ; branchcc occurs ; [] |1198| 
	.dwpsn	file "../source/CommProtocol.c",line 1200,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1200 | usnDataLength = iRemainder;                                            
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1200| 
        MOV       *-SP[14],AL           ; [CPU_] |1200| 
	.dwpsn	file "../source/CommProtocol.c",line 1201,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1202 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L87,UNC            ; [CPU_] |1201| 
        ; branch occurs ; [] |1201| 
$C$L86:    
	.dwpsn	file "../source/CommProtocol.c",line 1204,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1204 | usnDataLength = MAXIMUM_DATA_SIZE;                                     
; 1207 | //!Get the pulse height data of the cell type that needs to be transmit
;     | ted                                                                    
;----------------------------------------------------------------------
        MOV       *-SP[14],#502         ; [CPU_] |1204| 
$C$L87:    
	.dwpsn	file "../source/CommProtocol.c",line 1208,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1208 | SysGetPulseHeightDataFrmSDRAM(m_eTypeofCell, stPtrParsedPacket->arrusnD
;     | ata,\                                                                  
; 1209 |         usnPreviousIndex,usnDataLength);                               
; 1211 | //!Update the structure: Data with the zero for the pending data       
;----------------------------------------------------------------------
        MOVZ      AR6,*-SP[14]          ; [CPU_] |1208| 
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      ACC,#6                ; [CPU_] |1208| 
        MOV       *-SP[1],AR6           ; [CPU_] |1208| 
        MOVZ      AR5,@_m_eTypeofCell   ; [CPU_] |1208| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |1208| 
        MOVL      XAR4,ACC              ; [CPU_] |1208| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |1208| 
$C$DW$238	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$238, DW_AT_low_pc(0x00)
	.dwattr $C$DW$238, DW_AT_name("_SysGetPulseHeightDataFrmSDRAM")
	.dwattr $C$DW$238, DW_AT_TI_call

        LCR       #_SysGetPulseHeightDataFrmSDRAM ; [CPU_] |1208| 
        ; call occurs [#_SysGetPulseHeightDataFrmSDRAM] ; [] |1208| 
	.dwpsn	file "../source/CommProtocol.c",line 1212,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1212 | if(usnDataLength < MAXIMUM_DATA_SIZE)                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[14]           ; [CPU_] |1212| 
        CMP       AL,#502               ; [CPU_] |1212| 
        B         $C$L89,HIS            ; [CPU_] |1212| 
        ; branchcc occurs ; [] |1212| 
	.dwpsn	file "../source/CommProtocol.c",line 1214,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1214 | for(usncount = usnDataLength; usncount < MAXIMUM_DATA_SIZE; usncount++)
;----------------------------------------------------------------------
        MOV       *-SP[15],AL           ; [CPU_] |1214| 
	.dwpsn	file "../source/CommProtocol.c",line 1214,column 34,is_stmt,isa 0
        CMP       AL,#502               ; [CPU_] |1214| 
        B         $C$L89,HIS            ; [CPU_] |1214| 
        ; branchcc occurs ; [] |1214| 
$C$L88:    
	.dwpsn	file "../source/CommProtocol.c",line 1216,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1216 | stPtrParsedPacket->arrusnData[usncount] = 0;                           
; 1220 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[15]          ; [CPU_] |1216| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |1216| 
        MOVL      XAR4,ACC              ; [CPU_] |1216| 
        MOV       *+XAR4[6],#0          ; [CPU_] |1216| 
	.dwpsn	file "../source/CommProtocol.c",line 1214,column 64,is_stmt,isa 0
        INC       *-SP[15]              ; [CPU_] |1214| 
	.dwpsn	file "../source/CommProtocol.c",line 1214,column 34,is_stmt,isa 0
        CMP       *-SP[15],#502         ; [CPU_] |1214| 
        B         $C$L88,LO             ; [CPU_] |1214| 
        ; branchcc occurs ; [] |1214| 
$C$L89:    
	.dwpsn	file "../source/CommProtocol.c",line 1221,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1221 | m_usnPacketCount = 0;                                                  
; 1222 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1221| 
	.dwpsn	file "../source/CommProtocol.c",line 1223,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1223 | m_bNAckReceived = FALSE;                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOV       @_m_bNAckReceived,#0  ; [CPU_] |1223| 
	.dwpsn	file "../source/CommProtocol.c",line 1224,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1224 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[14]           ; [CPU_] |1224| 
	.dwpsn	file "../source/CommProtocol.c",line 1225,column 1,is_stmt,isa 0
        SUBB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$239	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$239, DW_AT_low_pc(0x00)
	.dwattr $C$DW$239, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$226, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$226, DW_AT_TI_end_line(0x4c9)
	.dwattr $C$DW$226, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$226

	.sect	".text:_CPSetPacketAttributesForTx"
	.clink
	.global	_CPSetPacketAttributesForTx

$C$DW$240	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$240, DW_AT_name("CPSetPacketAttributesForTx")
	.dwattr $C$DW$240, DW_AT_low_pc(_CPSetPacketAttributesForTx)
	.dwattr $C$DW$240, DW_AT_high_pc(0x00)
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_CPSetPacketAttributesForTx")
	.dwattr $C$DW$240, DW_AT_external
	.dwattr $C$DW$240, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$240, DW_AT_TI_begin_line(0x4d5)
	.dwattr $C$DW$240, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$240, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommProtocol.c",line 1239,column 1,is_stmt,address _CPSetPacketAttributesForTx,isa 0

	.dwfde $C$DW$CIE, _CPSetPacketAttributesForTx
$C$DW$241	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$241, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$241, DW_AT_location[DW_OP_reg12]

$C$DW$242	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$242, DW_AT_name("usnMajorCmd")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_usnMajorCmd")
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$242, DW_AT_location[DW_OP_reg0]

$C$DW$243	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$243, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 1237 | void CPSetPacketAttributesForTx(ST_SPI_PACKET_FRAME *stPtrParsedPacket,
;     | \                                                                      
; 1238 | uint16_t usnMajorCmd,uint16_t usnSubCmd)                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPSetPacketAttributesForTx   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_CPSetPacketAttributesForTx:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$244	.dwtag  DW_TAG_variable
	.dwattr $C$DW$244, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_breg20 -2]

$C$DW$245	.dwtag  DW_TAG_variable
	.dwattr $C$DW$245, DW_AT_name("usnMajorCmd")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_usnMajorCmd")
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$245, DW_AT_location[DW_OP_breg20 -3]

$C$DW$246	.dwtag  DW_TAG_variable
	.dwattr $C$DW$246, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_breg20 -4]

;----------------------------------------------------------------------
; 1240 | //!Adds the Start of packet to the Packet Frame                        
;----------------------------------------------------------------------
        MOV       *-SP[4],AH            ; [CPU_] |1239| 
        MOV       *-SP[3],AL            ; [CPU_] |1239| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1239| 
	.dwpsn	file "../source/CommProtocol.c",line 1241,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1241 | CPSetStartOfPacket(&m_stPacketForTx);                                  
; 1243 | //!Fill the Command Attribute                                          
;----------------------------------------------------------------------
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |1241| 
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_name("_CPSetStartOfPacket")
	.dwattr $C$DW$247, DW_AT_TI_call

        LCR       #_CPSetStartOfPacket  ; [CPU_] |1241| 
        ; call occurs [#_CPSetStartOfPacket] ; [] |1241| 
	.dwpsn	file "../source/CommProtocol.c",line 1244,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1244 | CPSetCommandsForPacket(&m_stPacketForTx, usnMajorCmd, usnSubCmd);      
; 1246 | //!Adds the Footer to the Data Packet                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |1244| 
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |1244| 
        MOV       AH,*-SP[4]            ; [CPU_] |1244| 
$C$DW$248	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$248, DW_AT_low_pc(0x00)
	.dwattr $C$DW$248, DW_AT_name("_CPSetCommandsForPacket")
	.dwattr $C$DW$248, DW_AT_TI_call

        LCR       #_CPSetCommandsForPacket ; [CPU_] |1244| 
        ; call occurs [#_CPSetCommandsForPacket] ; [] |1244| 
	.dwpsn	file "../source/CommProtocol.c",line 1247,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1247 | CPSetEndOfPacket(&m_stPacketForTx);                                    
;----------------------------------------------------------------------
        MOVL      XAR4,#_m_stPacketForTx ; [CPU_U] |1247| 
$C$DW$249	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$249, DW_AT_low_pc(0x00)
	.dwattr $C$DW$249, DW_AT_name("_CPSetEndOfPacket")
	.dwattr $C$DW$249, DW_AT_TI_call

        LCR       #_CPSetEndOfPacket    ; [CPU_] |1247| 
        ; call occurs [#_CPSetEndOfPacket] ; [] |1247| 
	.dwpsn	file "../source/CommProtocol.c",line 1248,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$250	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$250, DW_AT_low_pc(0x00)
	.dwattr $C$DW$250, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$240, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$240, DW_AT_TI_end_line(0x4e0)
	.dwattr $C$DW$240, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$240

	.sect	".text:_CPGetCountData"
	.clink
	.global	_CPGetCountData

$C$DW$251	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$251, DW_AT_name("CPGetCountData")
	.dwattr $C$DW$251, DW_AT_low_pc(_CPGetCountData)
	.dwattr $C$DW$251, DW_AT_high_pc(0x00)
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_CPGetCountData")
	.dwattr $C$DW$251, DW_AT_external
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$251, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$251, DW_AT_TI_begin_line(0x4e9)
	.dwattr $C$DW$251, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$251, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommProtocol.c",line 1258,column 1,is_stmt,address _CPGetCountData,isa 0

	.dwfde $C$DW$CIE, _CPGetCountData
$C$DW$252	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$252, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 1257 | uint16_t CPGetCountData(ST_SPI_PACKET_FRAME *stPtrParsedPacket)        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPGetCountData               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_CPGetCountData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$253	.dwtag  DW_TAG_variable
	.dwattr $C$DW$253, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$253, DW_AT_location[DW_OP_breg20 -2]

$C$DW$254	.dwtag  DW_TAG_variable
	.dwattr $C$DW$254, DW_AT_name("usnDataLength")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$254, DW_AT_location[DW_OP_breg20 -3]

$C$DW$255	.dwtag  DW_TAG_variable
	.dwattr $C$DW$255, DW_AT_name("usncount")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_usncount")
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$255, DW_AT_location[DW_OP_breg20 -4]

;----------------------------------------------------------------------
; 1259 | //!By default set the data length to Zero                              
;----------------------------------------------------------------------
        MOVL      *-SP[2],XAR4          ; [CPU_] |1258| 
	.dwpsn	file "../source/CommProtocol.c",line 1260,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 1260 | uint16_t usnDataLength = 0;                                            
; 1261 | //!By default set the count to Zero                                    
; 1262 | //!Count to run the loop to update the structure with zero where data i
;     | s not available                                                        
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |1260| 
	.dwpsn	file "../source/CommProtocol.c",line 1263,column 20,is_stmt,isa 0
;----------------------------------------------------------------------
; 1263 | uint16_t usncount = 0;                                                 
; 1265 | //!If the minor command is WBC, RBC, PLT, HGB Count                    
; 1266 | //!Update the WBC, RBC, PLT, HGB data to the structure: data and       
; 1267 | //!update the rest of the data section to zero                         
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |1263| 
	.dwpsn	file "../source/CommProtocol.c",line 1268,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1268 | if(stPtrParsedPacket->usnMinorCommand == DELFINO_WBC_RBC_PLT_HGB_COUNT)
;----------------------------------------------------------------------
        MOV       AL,#8196              ; [CPU_] |1268| 
        CMP       AL,*+XAR4[5]          ; [CPU_] |1268| 
        B         $C$L91,NEQ            ; [CPU_] |1268| 
        ; branchcc occurs ; [] |1268| 
	.dwpsn	file "../source/CommProtocol.c",line 1270,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1270 | usnDataLength = SysGetAdcCountData(stPtrParsedPacket->arrusnData);     
;----------------------------------------------------------------------
        MOVB      ACC,#6                ; [CPU_] |1270| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1270| 
        MOVL      XAR4,ACC              ; [CPU_] |1270| 
$C$DW$256	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$256, DW_AT_low_pc(0x00)
	.dwattr $C$DW$256, DW_AT_name("_SysGetAdcCountData")
	.dwattr $C$DW$256, DW_AT_TI_call

        LCR       #_SysGetAdcCountData  ; [CPU_] |1270| 
        ; call occurs [#_SysGetAdcCountData] ; [] |1270| 
        MOV       *-SP[3],AL            ; [CPU_] |1270| 
	.dwpsn	file "../source/CommProtocol.c",line 1272,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1272 | for(usncount = usnDataLength; usncount < MAXIMUM_DATA_SIZE ; usncount++
;     | )                                                                      
;----------------------------------------------------------------------
        MOV       *-SP[4],AL            ; [CPU_] |1272| 
	.dwpsn	file "../source/CommProtocol.c",line 1272,column 33,is_stmt,isa 0
        CMP       AL,#502               ; [CPU_] |1272| 
        B         $C$L91,HIS            ; [CPU_] |1272| 
        ; branchcc occurs ; [] |1272| 
$C$L90:    
	.dwpsn	file "../source/CommProtocol.c",line 1274,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1274 | stPtrParsedPacket->arrusnData[usncount] = 0;                           
; 1278 | //!The data count for Set the packet count to zero, multiple packet to
;     | false                                                                  
; 1279 | //!and last multiple packet to false                                   
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[4]           ; [CPU_] |1274| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1274| 
        MOVL      XAR4,ACC              ; [CPU_] |1274| 
        MOV       *+XAR4[6],#0          ; [CPU_] |1274| 
	.dwpsn	file "../source/CommProtocol.c",line 1272,column 64,is_stmt,isa 0
        INC       *-SP[4]               ; [CPU_] |1272| 
	.dwpsn	file "../source/CommProtocol.c",line 1272,column 33,is_stmt,isa 0
        CMP       *-SP[4],#502          ; [CPU_] |1272| 
        B         $C$L90,LO             ; [CPU_] |1272| 
        ; branchcc occurs ; [] |1272| 
$C$L91:    
	.dwpsn	file "../source/CommProtocol.c",line 1280,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1280 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1280| 
	.dwpsn	file "../source/CommProtocol.c",line 1281,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1281 | m_bMutiplePackets = FALSE;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_m_bMutiplePackets ; [CPU_U] 
        MOV       @_m_bMutiplePackets,#0 ; [CPU_] |1281| 
	.dwpsn	file "../source/CommProtocol.c",line 1282,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1282 | m_bLastMultiplePacket = FALSE;                                         
; 1283 | //!Return the data length                                              
;----------------------------------------------------------------------
        MOV       @_m_bLastMultiplePacket,#0 ; [CPU_] |1282| 
	.dwpsn	file "../source/CommProtocol.c",line 1284,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1284 | return usnDataLength;//return here                                     
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |1284| 
	.dwpsn	file "../source/CommProtocol.c",line 1285,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$257	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$257, DW_AT_low_pc(0x00)
	.dwattr $C$DW$257, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$251, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$251, DW_AT_TI_end_line(0x505)
	.dwattr $C$DW$251, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$251

	.sect	".text:_CPWriteSHDataToPacket"
	.clink
	.global	_CPWriteSHDataToPacket

$C$DW$258	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$258, DW_AT_name("CPWriteSHDataToPacket")
	.dwattr $C$DW$258, DW_AT_low_pc(_CPWriteSHDataToPacket)
	.dwattr $C$DW$258, DW_AT_high_pc(0x00)
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_CPWriteSHDataToPacket")
	.dwattr $C$DW$258, DW_AT_external
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$258, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$258, DW_AT_TI_begin_line(0x50f)
	.dwattr $C$DW$258, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$258, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/CommProtocol.c",line 1297,column 1,is_stmt,address _CPWriteSHDataToPacket,isa 0

	.dwfde $C$DW$CIE, _CPWriteSHDataToPacket
$C$DW$259	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$259, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$259, DW_AT_location[DW_OP_reg12]

$C$DW$260	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$260, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$260, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1295 | uint16_t CPWriteSHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
; 1296 | bool_t bNAKReceived)                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWriteSHDataToPacket        FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CPWriteSHDataToPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$261	.dwtag  DW_TAG_variable
	.dwattr $C$DW$261, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$261, DW_AT_location[DW_OP_breg20 -4]

$C$DW$262	.dwtag  DW_TAG_variable
	.dwattr $C$DW$262, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$262, DW_AT_location[DW_OP_breg20 -5]

$C$DW$263	.dwtag  DW_TAG_variable
	.dwattr $C$DW$263, DW_AT_name("usnDataLength")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$263, DW_AT_location[DW_OP_breg20 -6]

$C$DW$264	.dwtag  DW_TAG_variable
	.dwattr $C$DW$264, DW_AT_name("utMotorHomePositionCheck")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_utMotorHomePositionCheck")
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$264, DW_AT_location[DW_OP_breg20 -7]

;----------------------------------------------------------------------
; 1298 | //!Set the data length to zero by default                              
;----------------------------------------------------------------------
        MOV       *-SP[5],AL            ; [CPU_] |1297| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |1297| 
	.dwpsn	file "../source/CommProtocol.c",line 1299,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 1299 | uint16_t usnDataLength = 0;                                            
; 1302 | union UT_MOTOR_HOME_POSITION utMotorHomePositionCheck;                 
; 1303 | //!Check for the minor command and set the data, data length and packet
; 1304 | //! count based on the minor command                                   
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1299| 
	.dwpsn	file "../source/CommProtocol.c",line 1305,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1305 | switch(stPtrParsedPacket->usnMinorCommand)                             
; 1307 |         case DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD:                   
; 1308 |         case DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD:                    
; 1309 |         case DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD:                 
; 1310 |         case DELFINO_PRIME_ALL_COMPLETED_CMD:                          
; 1311 |         case DELFINO_BACK_FLUSH_COMPLETED_CMD:                         
; 1312 |         case DELFINO_PRIME_WITH_EZ_CLEANSER_CMD:                       
; 1313 |         case DELFINO_ZAP_APERTURE_COMPLETED_CMD:                       
; 1314 |         case DELFINO_DRAIN_BATH_COMPLETED_CMD:                         
; 1315 |         case DELFINO_DRAIN_ALL_COMPLETED_CMD:                          
; 1316 |         case DELFINO_CLEAN_BATH_COMPLETED_CMD:                         
; 1317 |         case DELFINO_START_UP_SEQUENCE_COMPLETED_CMD:                  
; 1318 |         case DELFINO_HEAD_RINSING_COMPLETED_CMD:                       
; 1319 |         case DELFINO_PROBE_CLEANING_COMPLETED_CMD:                     
; 1320 |         case DELFINO_SPI_ERROR_CHECK:                                  
; 1321 |         case DELFINO_SLEEP_SEQUENCE_COMPLETED:                         
; 1322 |         case DELFINO_ABORT_SEQUENCE_COMPLETED_CMD:  //HN_added         
; 1323 |         case DELFINO_BATH_FILL_SEQUENCE_COMPLETED_CMD:  //HN_added     
; 1324 |         case DELFINO_ZAP_INITIATE_COMPLETED_CMD:                       
; 1325 |         case DELFINO_SET_WASTE_BIN_FULL:                               
; 1326 |         case DELFINO_CLEAR_WASTE_BIN_FULL:                             
; 1328 |             //!Set data length and packet count to zero for the above s
;     | equence                                                                
;----------------------------------------------------------------------
        B         $C$L137,UNC           ; [CPU_] |1305| 
        ; branch occurs ; [] |1305| 
$C$L92:    
	.dwpsn	file "../source/CommProtocol.c",line 1329,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1329 | usnDataLength = 0;                                                     
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1329| 
	.dwpsn	file "../source/CommProtocol.c",line 1330,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1330 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1330| 
	.dwpsn	file "../source/CommProtocol.c",line 1332,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1332 | break;                                                                 
; 1335 | case DELFINO_VALVE_TEST_COMPLETED_CMD:                                 
; 1337 |     //!Set the valve number for which the valve test was performed     
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1332| 
        ; branch occurs ; [] |1332| 
$C$L93:    
	.dwpsn	file "../source/CommProtocol.c",line 1338,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1338 | stPtrParsedPacket->arrusnData[0] =  m_ucValveNo;                       
; 1339 | //!Reset the valve number                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_ucValveNo      ; [CPU_U] 
        MOV       AL,@_m_ucValveNo      ; [CPU_] |1338| 
        MOV       *+XAR4[6],AL          ; [CPU_] |1338| 
	.dwpsn	file "../source/CommProtocol.c",line 1340,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1340 | m_ucValveNo = 0;                                                       
; 1341 | //!Set the data length to one                                          
;----------------------------------------------------------------------
        MOV       @_m_ucValveNo,#0      ; [CPU_] |1340| 
	.dwpsn	file "../source/CommProtocol.c",line 1342,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1342 | usnDataLength = 1;                                                     
; 1343 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[6],#1,UNC        ; [CPU_] |1342| 
	.dwpsn	file "../source/CommProtocol.c",line 1344,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1344 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1344| 
	.dwpsn	file "../source/CommProtocol.c",line 1347,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1347 | break;                                                                 
; 1349 | case DELFINO_SYSTEM_STATUS_DATA:                                       
; 1351 |     //!Set the data for System health status packet                    
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1347| 
        ; branch occurs ; [] |1347| 
$C$L94:    
	.dwpsn	file "../source/CommProtocol.c",line 1352,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1352 | SysInitSystemHealthData(stPtrParsedPacket->arrusnData);                
; 1353 | //!Set the data length according to system health packet               
;----------------------------------------------------------------------
        MOVB      ACC,#6                ; [CPU_] |1352| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |1352| 
        MOVL      XAR4,ACC              ; [CPU_] |1352| 
$C$DW$265	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$265, DW_AT_low_pc(0x00)
	.dwattr $C$DW$265, DW_AT_name("_SysInitSystemHealthData")
	.dwattr $C$DW$265, DW_AT_TI_call

        LCR       #_SysInitSystemHealthData ; [CPU_] |1352| 
        ; call occurs [#_SysInitSystemHealthData] ; [] |1352| 
	.dwpsn	file "../source/CommProtocol.c",line 1354,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1354 | usnDataLength = CP_SYSTEM_STATUS_LENGTH;                               
; 1355 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[6],#6,UNC        ; [CPU_] |1354| 
	.dwpsn	file "../source/CommProtocol.c",line 1356,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1356 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1356| 
	.dwpsn	file "../source/CommProtocol.c",line 1358,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1358 | break;                                                                 
; 1360 | case DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD:                           
; 1362 |     //!If the motor check status is success,                           
; 1363 |     //! then set the success info in the data field, else set motor    
; 1364 |     //! failure status in data filed                                   
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1358| 
        ; branch occurs ; [] |1358| 
$C$L95:    
	.dwpsn	file "../source/CommProtocol.c",line 1365,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1365 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |1365| 
        CMPB      AL,#1                 ; [CPU_] |1365| 
        B         $C$L96,NEQ            ; [CPU_] |1365| 
        ; branchcc occurs ; [] |1365| 
	.dwpsn	file "../source/CommProtocol.c",line 1367,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1367 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
; 1368 | //!Reset the motor check status flag                                   
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |1367| 
	.dwpsn	file "../source/CommProtocol.c",line 1369,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1369 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |1369| 
	.dwpsn	file "../source/CommProtocol.c",line 1370,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1371 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L97,UNC            ; [CPU_] |1370| 
        ; branch occurs ; [] |1370| 
$C$L96:    
	.dwpsn	file "../source/CommProtocol.c",line 1373,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1373 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 1376 | //!Get the home position check for the motors and update the data      
; 1377 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |1373| 
$C$L97:    
	.dwpsn	file "../source/CommProtocol.c",line 1378,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1378 | if(TRUE == OIHomeSensor4State())                                       
;----------------------------------------------------------------------
$C$DW$266	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$266, DW_AT_low_pc(0x00)
	.dwattr $C$DW$266, DW_AT_name("_OIHomeSensor4State")
	.dwattr $C$DW$266, DW_AT_TI_call

        LCR       #_OIHomeSensor4State  ; [CPU_] |1378| 
        ; call occurs [#_OIHomeSensor4State] ; [] |1378| 
        CMPB      AL,#1                 ; [CPU_] |1378| 
        B         $C$L98,NEQ            ; [CPU_] |1378| 
        ; branchcc occurs ; [] |1378| 
	.dwpsn	file "../source/CommProtocol.c",line 1380,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1380 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1380| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |1380| 
	.dwpsn	file "../source/CommProtocol.c",line 1381,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1382 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L99,UNC            ; [CPU_] |1381| 
        ; branch occurs ; [] |1381| 
$C$L98:    
	.dwpsn	file "../source/CommProtocol.c",line 1384,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1384 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 1386 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1384| 
        MOV       *+XAR4[7],#0          ; [CPU_] |1384| 
$C$L99:    
	.dwpsn	file "../source/CommProtocol.c",line 1387,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1387 | usnDataLength = 2;                                                     
; 1388 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[6],#2,UNC        ; [CPU_] |1387| 
	.dwpsn	file "../source/CommProtocol.c",line 1389,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1389 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1389| 
	.dwpsn	file "../source/CommProtocol.c",line 1391,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1391 | break;                                                                 
; 1393 | case DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD:                           
; 1395 | //!If the motor check status is success,                               
; 1396 | //! then set the success info in the data field, else set motor        
; 1397 | //! failure status in data filed                                       
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1391| 
        ; branch occurs ; [] |1391| 
$C$L100:    
	.dwpsn	file "../source/CommProtocol.c",line 1398,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1398 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |1398| 
        CMPB      AL,#1                 ; [CPU_] |1398| 
        B         $C$L101,NEQ           ; [CPU_] |1398| 
        ; branchcc occurs ; [] |1398| 
	.dwpsn	file "../source/CommProtocol.c",line 1400,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1400 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |1400| 
	.dwpsn	file "../source/CommProtocol.c",line 1401,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1401 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |1401| 
	.dwpsn	file "../source/CommProtocol.c",line 1402,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1403 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L102,UNC           ; [CPU_] |1402| 
        ; branch occurs ; [] |1402| 
$C$L101:    
	.dwpsn	file "../source/CommProtocol.c",line 1405,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1405 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 1407 | //!Get the home position check for the motors and update the data      
; 1408 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |1405| 
$C$L102:    
	.dwpsn	file "../source/CommProtocol.c",line 1409,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1409 | if(TRUE == OIHomeSensor5State())                                       
;----------------------------------------------------------------------
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_name("_OIHomeSensor5State")
	.dwattr $C$DW$267, DW_AT_TI_call

        LCR       #_OIHomeSensor5State  ; [CPU_] |1409| 
        ; call occurs [#_OIHomeSensor5State] ; [] |1409| 
        CMPB      AL,#1                 ; [CPU_] |1409| 
        B         $C$L103,NEQ           ; [CPU_] |1409| 
        ; branchcc occurs ; [] |1409| 
	.dwpsn	file "../source/CommProtocol.c",line 1411,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1411 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1411| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |1411| 
	.dwpsn	file "../source/CommProtocol.c",line 1412,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1413 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L104,UNC           ; [CPU_] |1412| 
        ; branch occurs ; [] |1412| 
$C$L103:    
	.dwpsn	file "../source/CommProtocol.c",line 1415,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1415 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 1417 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1415| 
        MOV       *+XAR4[7],#0          ; [CPU_] |1415| 
$C$L104:    
	.dwpsn	file "../source/CommProtocol.c",line 1418,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1418 | usnDataLength = 2;                                                     
; 1419 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[6],#2,UNC        ; [CPU_] |1418| 
	.dwpsn	file "../source/CommProtocol.c",line 1420,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1420 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1420| 
	.dwpsn	file "../source/CommProtocol.c",line 1422,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1422 | break;                                                                 
; 1424 | case DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD:                  
; 1426 |     //!If the motor check status is success,                           
; 1427 |     //! then set the success info in the data field, else set motor    
; 1428 |     //! failure status in data filed                                   
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1422| 
        ; branch occurs ; [] |1422| 
$C$L105:    
	.dwpsn	file "../source/CommProtocol.c",line 1429,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1429 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |1429| 
        CMPB      AL,#1                 ; [CPU_] |1429| 
        B         $C$L106,NEQ           ; [CPU_] |1429| 
        ; branchcc occurs ; [] |1429| 
	.dwpsn	file "../source/CommProtocol.c",line 1431,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1431 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |1431| 
	.dwpsn	file "../source/CommProtocol.c",line 1432,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1432 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |1432| 
	.dwpsn	file "../source/CommProtocol.c",line 1433,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1434 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L107,UNC           ; [CPU_] |1433| 
        ; branch occurs ; [] |1433| 
$C$L106:    
	.dwpsn	file "../source/CommProtocol.c",line 1436,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1436 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 1438 | //!Get the home position check for the motors and update the data      
; 1439 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |1436| 
$C$L107:    
	.dwpsn	file "../source/CommProtocol.c",line 1440,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1440 | if(TRUE == OIHomeSensor2State())                                       
;----------------------------------------------------------------------
$C$DW$268	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$268, DW_AT_low_pc(0x00)
	.dwattr $C$DW$268, DW_AT_name("_OIHomeSensor2State")
	.dwattr $C$DW$268, DW_AT_TI_call

        LCR       #_OIHomeSensor2State  ; [CPU_] |1440| 
        ; call occurs [#_OIHomeSensor2State] ; [] |1440| 
        CMPB      AL,#1                 ; [CPU_] |1440| 
        B         $C$L108,NEQ           ; [CPU_] |1440| 
        ; branchcc occurs ; [] |1440| 
	.dwpsn	file "../source/CommProtocol.c",line 1442,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1442 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1442| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |1442| 
	.dwpsn	file "../source/CommProtocol.c",line 1443,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1444 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L109,UNC           ; [CPU_] |1443| 
        ; branch occurs ; [] |1443| 
$C$L108:    
	.dwpsn	file "../source/CommProtocol.c",line 1446,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1446 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 1448 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1446| 
        MOV       *+XAR4[7],#0          ; [CPU_] |1446| 
$C$L109:    
	.dwpsn	file "../source/CommProtocol.c",line 1449,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1449 | usnDataLength = 2;                                                     
; 1450 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[6],#2,UNC        ; [CPU_] |1449| 
	.dwpsn	file "../source/CommProtocol.c",line 1451,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1451 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1451| 
	.dwpsn	file "../source/CommProtocol.c",line 1453,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1453 | break;                                                                 
; 1455 | case DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD:                    
; 1457 |     //!If the motor check status is success,                           
; 1458 |     //! then set the success info in the data field, else set motor    
; 1459 |     //! failure status in data filed                                   
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1453| 
        ; branch occurs ; [] |1453| 
$C$L110:    
	.dwpsn	file "../source/CommProtocol.c",line 1460,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1460 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |1460| 
        CMPB      AL,#1                 ; [CPU_] |1460| 
        B         $C$L111,NEQ           ; [CPU_] |1460| 
        ; branchcc occurs ; [] |1460| 
	.dwpsn	file "../source/CommProtocol.c",line 1462,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1462 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |1462| 
	.dwpsn	file "../source/CommProtocol.c",line 1463,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1463 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |1463| 
	.dwpsn	file "../source/CommProtocol.c",line 1464,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1465 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L112,UNC           ; [CPU_] |1464| 
        ; branch occurs ; [] |1464| 
$C$L111:    
	.dwpsn	file "../source/CommProtocol.c",line 1467,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1467 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 1469 | //!Get the home position check for the motors and update the data      
; 1470 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |1467| 
$C$L112:    
	.dwpsn	file "../source/CommProtocol.c",line 1471,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1471 | if(TRUE == OIHomeSensor1State())                                       
;----------------------------------------------------------------------
$C$DW$269	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$269, DW_AT_low_pc(0x00)
	.dwattr $C$DW$269, DW_AT_name("_OIHomeSensor1State")
	.dwattr $C$DW$269, DW_AT_TI_call

        LCR       #_OIHomeSensor1State  ; [CPU_] |1471| 
        ; call occurs [#_OIHomeSensor1State] ; [] |1471| 
        CMPB      AL,#1                 ; [CPU_] |1471| 
        B         $C$L113,NEQ           ; [CPU_] |1471| 
        ; branchcc occurs ; [] |1471| 
	.dwpsn	file "../source/CommProtocol.c",line 1473,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1473 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1473| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |1473| 
	.dwpsn	file "../source/CommProtocol.c",line 1474,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1475 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L114,UNC           ; [CPU_] |1474| 
        ; branch occurs ; [] |1474| 
$C$L113:    
	.dwpsn	file "../source/CommProtocol.c",line 1477,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1477 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 1479 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1477| 
        MOV       *+XAR4[7],#0          ; [CPU_] |1477| 
$C$L114:    
	.dwpsn	file "../source/CommProtocol.c",line 1480,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1480 | usnDataLength = 2;                                                     
; 1481 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[6],#2,UNC        ; [CPU_] |1480| 
	.dwpsn	file "../source/CommProtocol.c",line 1482,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1482 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1482| 
	.dwpsn	file "../source/CommProtocol.c",line 1484,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1484 | break;                                                                 
; 1486 | case DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD:              
; 1488 |     //!If the motor check status is success,                           
; 1489 |     //! then set the success info in the data field, else set motor    
; 1490 |     //! failure status in data filed                                   
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1484| 
        ; branch occurs ; [] |1484| 
$C$L115:    
	.dwpsn	file "../source/CommProtocol.c",line 1491,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1491 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |1491| 
        CMPB      AL,#1                 ; [CPU_] |1491| 
        B         $C$L116,NEQ           ; [CPU_] |1491| 
        ; branchcc occurs ; [] |1491| 
	.dwpsn	file "../source/CommProtocol.c",line 1493,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1493 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |1493| 
	.dwpsn	file "../source/CommProtocol.c",line 1494,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1494 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |1494| 
	.dwpsn	file "../source/CommProtocol.c",line 1495,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1496 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L117,UNC           ; [CPU_] |1495| 
        ; branch occurs ; [] |1495| 
$C$L116:    
	.dwpsn	file "../source/CommProtocol.c",line 1498,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1498 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 1500 | //!Get the home position check for the motors and update the data      
; 1501 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |1498| 
$C$L117:    
	.dwpsn	file "../source/CommProtocol.c",line 1502,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1502 | if(TRUE == OIHomeSensor3State())                                       
;----------------------------------------------------------------------
$C$DW$270	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$270, DW_AT_low_pc(0x00)
	.dwattr $C$DW$270, DW_AT_name("_OIHomeSensor3State")
	.dwattr $C$DW$270, DW_AT_TI_call

        LCR       #_OIHomeSensor3State  ; [CPU_] |1502| 
        ; call occurs [#_OIHomeSensor3State] ; [] |1502| 
        CMPB      AL,#1                 ; [CPU_] |1502| 
        B         $C$L118,NEQ           ; [CPU_] |1502| 
        ; branchcc occurs ; [] |1502| 
	.dwpsn	file "../source/CommProtocol.c",line 1504,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1504 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1504| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |1504| 
	.dwpsn	file "../source/CommProtocol.c",line 1505,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1506 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L119,UNC           ; [CPU_] |1505| 
        ; branch occurs ; [] |1505| 
$C$L118:    
	.dwpsn	file "../source/CommProtocol.c",line 1508,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1508 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 1510 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1508| 
        MOV       *+XAR4[7],#0          ; [CPU_] |1508| 
$C$L119:    
	.dwpsn	file "../source/CommProtocol.c",line 1511,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1511 | usnDataLength = 2;                                                     
; 1512 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[6],#2,UNC        ; [CPU_] |1511| 
	.dwpsn	file "../source/CommProtocol.c",line 1513,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1513 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1513| 
	.dwpsn	file "../source/CommProtocol.c",line 1515,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1515 | break;                                                                 
; 1517 | //!If the sequence is counting time, get the data and update to the dat
;     | a                                                                      
; 1518 | //! field                                                              
; 1519 |         case DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD:             
; 1521 |             //!Get the counting time for WBC and RBC and update the dat
;     | a length                                                               
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1515| 
        ; branch occurs ; [] |1515| 
$C$L120:    
	.dwpsn	file "../source/CommProtocol.c",line 1522,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1522 | usnDataLength = SysInitGetCountingTime(stPtrParsedPacket->arrusnData); 
; 1523 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      ACC,#6                ; [CPU_] |1522| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |1522| 
        MOVL      XAR4,ACC              ; [CPU_] |1522| 
$C$DW$271	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$271, DW_AT_low_pc(0x00)
	.dwattr $C$DW$271, DW_AT_name("_SysInitGetCountingTime")
	.dwattr $C$DW$271, DW_AT_TI_call

        LCR       #_SysInitGetCountingTime ; [CPU_] |1522| 
        ; call occurs [#_SysInitGetCountingTime] ; [] |1522| 
        MOV       *-SP[6],AL            ; [CPU_] |1522| 
	.dwpsn	file "../source/CommProtocol.c",line 1524,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1524 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1524| 
	.dwpsn	file "../source/CommProtocol.c",line 1526,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1526 | break;                                                                 
; 1528 | case DELFINO_PRIME_WITH_RINSE_STARTED_CMD:                             
; 1529 | case DELFINO_PRIME_WITH_LYSE_STARTED_CMD:                              
; 1530 | case DELFINO_PRIME_WITH_DILUENT_STARTED_CMD:                           
; 1531 | case DELFINO_PRIME_ALL_STARTED_CMD:                                    
; 1532 | case DELFINO_BACK_FLUSH_STARTED_CMD:                                   
; 1533 | case DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD:                       
; 1534 | case DELFINO_ZAP_APERTURE_STARTED_CMD:                                 
; 1535 | case DELFINO_DRAIN_BATH_STARTED_CMD:                                   
; 1536 | case DELFINO_DRAIN_ALL_STARTED_CMD:                                    
; 1537 | case DELFINO_CLEAN_BATH_STARTED_CMD:                                   
; 1538 | case DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD://SKM_COUNTSTART_CHANGE
; 1539 | case DELFINO_SLEEP_SEQUENCE_STARTED:                                   
; 1540 | case DELFINO_ABORT_SEQUENCE_STARTED_CMD: //HN_added                    
; 1541 | case DELFINO_BATH_FILL_SEQUENCE_STARTED_CMD: //HN_added                
; 1542 | case DELFINO_ZAP_INITIATE_STARTED_CMD:                                 
; 1544 | //!Set the data length                                                 
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1526| 
        ; branch occurs ; [] |1526| 
$C$L121:    
	.dwpsn	file "../source/CommProtocol.c",line 1545,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1545 | usnDataLength = 0;                                                     
; 1546 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1545| 
	.dwpsn	file "../source/CommProtocol.c",line 1547,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1547 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1547| 
	.dwpsn	file "../source/CommProtocol.c",line 1549,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1549 | break;                                                                 
; 1551 | case DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD:                             
; 1552 | case DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD:                             
; 1553 | case DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD:                    
; 1554 | case DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD:                      
; 1555 | case DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD:                
; 1557 |     //!Set the data length                                             
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1549| 
        ; branch occurs ; [] |1549| 
$C$L122:    
	.dwpsn	file "../source/CommProtocol.c",line 1558,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1558 | usnDataLength = 0;                                                     
; 1559 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1558| 
	.dwpsn	file "../source/CommProtocol.c",line 1560,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1560 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1560| 
	.dwpsn	file "../source/CommProtocol.c",line 1562,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1562 | break;                                                                 
; 1564 | case DELFINO_HOME_POSITION_SENSOR_CHECK:                               
; 1566 |     //!If the motor is at home position, set motor is at home position,
; 1567 |     //! else set motor is not at home position                         
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1562| 
        ; branch occurs ; [] |1562| 
$C$L123:    
	.dwpsn	file "../source/CommProtocol.c",line 1568,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1568 | if(TRUE == OIHomeSensor4State())                                       
;----------------------------------------------------------------------
$C$DW$272	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$272, DW_AT_low_pc(0x00)
	.dwattr $C$DW$272, DW_AT_name("_OIHomeSensor4State")
	.dwattr $C$DW$272, DW_AT_TI_call

        LCR       #_OIHomeSensor4State  ; [CPU_] |1568| 
        ; call occurs [#_OIHomeSensor4State] ; [] |1568| 
        CMPB      AL,#1                 ; [CPU_] |1568| 
        B         $C$L124,NEQ           ; [CPU_] |1568| 
        ; branchcc occurs ; [] |1568| 
	.dwpsn	file "../source/CommProtocol.c",line 1570,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1570 | utMotorHomePositionCheck.stMotorHomePosition.usnXHomePosition \        
; 1571 |                                  = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        OR        *-SP[7],#0x0001       ; [CPU_] |1570| 
	.dwpsn	file "../source/CommProtocol.c",line 1572,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1573 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L125,UNC           ; [CPU_] |1572| 
        ; branch occurs ; [] |1572| 
$C$L124:    
	.dwpsn	file "../source/CommProtocol.c",line 1575,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1575 | utMotorHomePositionCheck.stMotorHomePosition.usnXHomePosition \        
; 1576 |                                  = MOTOR_NOT_HOME_POSITION;            
; 1578 | //!If the motor is at home position, set motor is at home position,    
; 1579 | //! else set motor is not at home position                             
;----------------------------------------------------------------------
        AND       *-SP[7],#0xfffe       ; [CPU_] |1575| 
$C$L125:    
	.dwpsn	file "../source/CommProtocol.c",line 1580,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1580 | if(TRUE == OIHomeSensor5State())                                       
;----------------------------------------------------------------------
$C$DW$273	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$273, DW_AT_low_pc(0x00)
	.dwattr $C$DW$273, DW_AT_name("_OIHomeSensor5State")
	.dwattr $C$DW$273, DW_AT_TI_call

        LCR       #_OIHomeSensor5State  ; [CPU_] |1580| 
        ; call occurs [#_OIHomeSensor5State] ; [] |1580| 
        CMPB      AL,#1                 ; [CPU_] |1580| 
        B         $C$L126,NEQ           ; [CPU_] |1580| 
        ; branchcc occurs ; [] |1580| 
	.dwpsn	file "../source/CommProtocol.c",line 1582,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1582 | utMotorHomePositionCheck.stMotorHomePosition.usnYHomePosition \        
; 1583 |                                  = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        OR        *-SP[7],#0x0002       ; [CPU_] |1582| 
	.dwpsn	file "../source/CommProtocol.c",line 1584,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1585 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L127,UNC           ; [CPU_] |1584| 
        ; branch occurs ; [] |1584| 
$C$L126:    
	.dwpsn	file "../source/CommProtocol.c",line 1587,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1587 | utMotorHomePositionCheck.stMotorHomePosition.usnYHomePosition \        
; 1588 |                                  = MOTOR_NOT_HOME_POSITION;            
; 1590 | //!If the motor is at home position, set motor is at home position,    
; 1591 | //! else set motor is not at home position                             
;----------------------------------------------------------------------
        AND       *-SP[7],#0xfffd       ; [CPU_] |1587| 
$C$L127:    
	.dwpsn	file "../source/CommProtocol.c",line 1592,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1592 | if(TRUE == OIHomeSensor2State())                                       
;----------------------------------------------------------------------
$C$DW$274	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$274, DW_AT_low_pc(0x00)
	.dwattr $C$DW$274, DW_AT_name("_OIHomeSensor2State")
	.dwattr $C$DW$274, DW_AT_TI_call

        LCR       #_OIHomeSensor2State  ; [CPU_] |1592| 
        ; call occurs [#_OIHomeSensor2State] ; [] |1592| 
        CMPB      AL,#1                 ; [CPU_] |1592| 
        B         $C$L128,NEQ           ; [CPU_] |1592| 
        ; branchcc occurs ; [] |1592| 
	.dwpsn	file "../source/CommProtocol.c",line 1594,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1594 | utMotorHomePositionCheck.stMotorHomePosition.usnDiluentHomePosition \  
; 1595 |                                  = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        OR        *-SP[7],#0x0004       ; [CPU_] |1594| 
	.dwpsn	file "../source/CommProtocol.c",line 1596,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1597 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L129,UNC           ; [CPU_] |1596| 
        ; branch occurs ; [] |1596| 
$C$L128:    
	.dwpsn	file "../source/CommProtocol.c",line 1599,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1599 | utMotorHomePositionCheck.stMotorHomePosition.usnDiluentHomePosition \  
; 1600 |                                  = MOTOR_NOT_HOME_POSITION;            
; 1602 | //!If the motor is at home position, set motor is at home position,    
; 1603 | //! else set motor is not at home position                             
;----------------------------------------------------------------------
        AND       *-SP[7],#0xfffb       ; [CPU_] |1599| 
$C$L129:    
	.dwpsn	file "../source/CommProtocol.c",line 1604,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1604 | if(TRUE == OIHomeSensor1State())                                       
;----------------------------------------------------------------------
$C$DW$275	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$275, DW_AT_low_pc(0x00)
	.dwattr $C$DW$275, DW_AT_name("_OIHomeSensor1State")
	.dwattr $C$DW$275, DW_AT_TI_call

        LCR       #_OIHomeSensor1State  ; [CPU_] |1604| 
        ; call occurs [#_OIHomeSensor1State] ; [] |1604| 
        CMPB      AL,#1                 ; [CPU_] |1604| 
        B         $C$L130,NEQ           ; [CPU_] |1604| 
        ; branchcc occurs ; [] |1604| 
	.dwpsn	file "../source/CommProtocol.c",line 1606,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1606 | utMotorHomePositionCheck.stMotorHomePosition.usnWasteHomePosition \    
; 1607 |                                  = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        OR        *-SP[7],#0x0008       ; [CPU_] |1606| 
	.dwpsn	file "../source/CommProtocol.c",line 1608,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1609 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L131,UNC           ; [CPU_] |1608| 
        ; branch occurs ; [] |1608| 
$C$L130:    
	.dwpsn	file "../source/CommProtocol.c",line 1611,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1611 | utMotorHomePositionCheck.stMotorHomePosition.usnWasteHomePosition \    
; 1612 |                                  = MOTOR_NOT_HOME_POSITION;            
; 1614 | //!If the motor is at home position, set motor is at home position,    
; 1615 | //! else set motor is not at home position                             
;----------------------------------------------------------------------
        AND       *-SP[7],#0xfff7       ; [CPU_] |1611| 
$C$L131:    
	.dwpsn	file "../source/CommProtocol.c",line 1616,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1616 | if(TRUE == OIHomeSensor3State())                                       
;----------------------------------------------------------------------
$C$DW$276	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$276, DW_AT_low_pc(0x00)
	.dwattr $C$DW$276, DW_AT_name("_OIHomeSensor3State")
	.dwattr $C$DW$276, DW_AT_TI_call

        LCR       #_OIHomeSensor3State  ; [CPU_] |1616| 
        ; call occurs [#_OIHomeSensor3State] ; [] |1616| 
        CMPB      AL,#1                 ; [CPU_] |1616| 
        B         $C$L132,NEQ           ; [CPU_] |1616| 
        ; branchcc occurs ; [] |1616| 
	.dwpsn	file "../source/CommProtocol.c",line 1618,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1618 | utMotorHomePositionCheck.stMotorHomePosition.usnSampleHomePosition \   
; 1619 |                                  = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        OR        *-SP[7],#0x0010       ; [CPU_] |1618| 
	.dwpsn	file "../source/CommProtocol.c",line 1620,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1621 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L133,UNC           ; [CPU_] |1620| 
        ; branch occurs ; [] |1620| 
$C$L132:    
	.dwpsn	file "../source/CommProtocol.c",line 1623,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1623 | utMotorHomePositionCheck.stMotorHomePosition.usnSampleHomePosition \   
; 1624 |                                  = MOTOR_NOT_HOME_POSITION;            
; 1626 | //!set Motor home position information to data                         
;----------------------------------------------------------------------
        AND       *-SP[7],#0xffef       ; [CPU_] |1623| 
$C$L133:    
	.dwpsn	file "../source/CommProtocol.c",line 1627,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1627 | stPtrParsedPacket->arrusnData[0] = utMotorHomePositionCheck.usnMotorHom
;     | ePosition;                                                             
; 1628 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1627| 
        MOV       AL,*-SP[7]            ; [CPU_] |1627| 
        MOV       *+XAR4[6],AL          ; [CPU_] |1627| 
	.dwpsn	file "../source/CommProtocol.c",line 1629,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1629 | usnDataLength = 1;                                                     
; 1630 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[6],#1,UNC        ; [CPU_] |1629| 
	.dwpsn	file "../source/CommProtocol.c",line 1631,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1631 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1631| 
	.dwpsn	file "../source/CommProtocol.c",line 1633,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1633 | break;                                                                 
; 1635 | case DELFINO_READY_FOR_ASPIRATION:                                     
; 1636 | case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:                            
; 1638 |     //!Set the data length                                             
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1633| 
        ; branch occurs ; [] |1633| 
$C$L134:    
	.dwpsn	file "../source/CommProtocol.c",line 1639,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1639 | usnDataLength = 0;                                                     
; 1640 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1639| 
	.dwpsn	file "../source/CommProtocol.c",line 1641,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1641 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1641| 
	.dwpsn	file "../source/CommProtocol.c",line 1643,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1643 | break;                                                                 
; 1645 | case DELFINO_ERROR_MINOR_CMD:                                          
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1643| 
        ; branch occurs ; [] |1643| 
$C$L135:    
	.dwpsn	file "../source/CommProtocol.c",line 1646,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1646 | sprintf(DebugPrintBuf, "\r\nDELFINO_ERROR_MINOR_CMD\n");               
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL1         ; [CPU_U] |1646| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1646| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1646| 
$C$DW$277	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$277, DW_AT_low_pc(0x00)
	.dwattr $C$DW$277, DW_AT_name("_sprintf")
	.dwattr $C$DW$277, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1646| 
        ; call occurs [#_sprintf] ; [] |1646| 
	.dwpsn	file "../source/CommProtocol.c",line 1647,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1647 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1647| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1647| 
$C$DW$278	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$278, DW_AT_low_pc(0x00)
	.dwattr $C$DW$278, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$278, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1647| 
        ; call occurs [#_UartDebugPrint] ; [] |1647| 
	.dwpsn	file "../source/CommProtocol.c",line 1648,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1648 | usnDataLength = CPGetErrorData(stPtrParsedPacket);                     
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1648| 
$C$DW$279	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$279, DW_AT_low_pc(0x00)
	.dwattr $C$DW$279, DW_AT_name("_CPGetErrorData")
	.dwattr $C$DW$279, DW_AT_TI_call

        LCR       #_CPGetErrorData      ; [CPU_] |1648| 
        ; call occurs [#_CPGetErrorData] ; [] |1648| 
        MOV       *-SP[6],AL            ; [CPU_] |1648| 
	.dwpsn	file "../source/CommProtocol.c",line 1649,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1649 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1649| 
	.dwpsn	file "../source/CommProtocol.c",line 1650,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1650 | break;                                                                 
; 1652 |     default:                                                           
; 1654 |         //!Set the data length                                         
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1650| 
        ; branch occurs ; [] |1650| 
$C$L136:    
	.dwpsn	file "../source/CommProtocol.c",line 1655,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1655 | usnDataLength = 0;                                                     
; 1656 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |1655| 
	.dwpsn	file "../source/CommProtocol.c",line 1657,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1657 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1657| 
	.dwpsn	file "../source/CommProtocol.c",line 1659,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1659 | break;                                                                 
; 1660 | }//end of switch case                                                  
;----------------------------------------------------------------------
        B         $C$L140,UNC           ; [CPU_] |1659| 
        ; branch occurs ; [] |1659| 
$C$L137:    
	.dwpsn	file "../source/CommProtocol.c",line 1305,column 2,is_stmt,isa 0
        MOVZ      AR6,*+XAR4[5]         ; [CPU_] |1305| 
        CMP       AR6,#8961             ; [CPU_] |1305| 
        B         $C$L138,GT            ; [CPU_] |1305| 
        ; branchcc occurs ; [] |1305| 
        CMP       AR6,#8961             ; [CPU_] |1305| 
        B         $C$L121,EQ            ; [CPU_] |1305| 
        ; branchcc occurs ; [] |1305| 
        MOV       AL,AR6                ; [CPU_] |1305| 
        SUB       AL,#8193              ; [CPU_] |1305| 
        CMPB      AL,#67                ; [CPU_] |1305| 
        B         $C$L139,LOS           ; [CPU_] |1305| 
        ; branchcc occurs ; [] |1305| 
        MOVZ      AR6,AR6               ; [CPU_] |1305| 
        MOVL      XAR4,#8738            ; [CPU_U] |1305| 
        MOVL      ACC,XAR4              ; [CPU_] |1305| 
        CMPL      ACC,XAR6              ; [CPU_] |1305| 
        B         $C$L92,EQ             ; [CPU_] |1305| 
        ; branchcc occurs ; [] |1305| 
        B         $C$L136,UNC           ; [CPU_] |1305| 
        ; branch occurs ; [] |1305| 
$C$L138:    
        MOVZ      AR7,AR6               ; [CPU_] |1305| 
        MOVL      XAR4,#8962            ; [CPU_U] |1305| 
        MOVL      ACC,XAR4              ; [CPU_] |1305| 
        CMPL      ACC,XAR7              ; [CPU_] |1305| 
        B         $C$L92,EQ             ; [CPU_] |1305| 
        ; branchcc occurs ; [] |1305| 
        MOVZ      AR6,AR6               ; [CPU_] |1305| 
        MOVL      XAR4,#9217            ; [CPU_U] |1305| 
        MOVL      ACC,XAR4              ; [CPU_] |1305| 
        CMPL      ACC,XAR6              ; [CPU_] |1305| 
        B         $C$L135,EQ            ; [CPU_] |1305| 
        ; branchcc occurs ; [] |1305| 
        B         $C$L136,UNC           ; [CPU_] |1305| 
        ; branch occurs ; [] |1305| 
$C$L139:    
        SUB       AR6,#8193             ; [CPU_] |1305| 
        MOV       ACC,AR6 << #1         ; [CPU_] |1305| 
        MOVZ      AR6,AL                ; [CPU_] |1305| 
        MOVL      XAR7,#$C$SW5          ; [CPU_U] |1305| 
        MOVL      ACC,XAR7              ; [CPU_] |1305| 
        ADDU      ACC,AR6               ; [CPU_] |1305| 
        MOVL      XAR7,ACC              ; [CPU_] |1305| 
        MOVL      XAR7,*XAR7            ; [CPU_] |1305| 
        LB        *XAR7                 ; [CPU_] |1305| 
        ; branch occurs ; [] |1305| 
	.sect	".switch:_CPWriteSHDataToPacket"
	.clink
$C$SW5:	.long	$C$L92	; 8193
	.long	$C$L92	; 8194
	.long	$C$L92	; 8195
	.long	$C$L92	; 8196
	.long	$C$L92	; 8197
	.long	$C$L92	; 8198
	.long	$C$L92	; 8199
	.long	$C$L92	; 8200
	.long	$C$L92	; 8201
	.long	$C$L92	; 8202
	.long	$C$L93	; 8203
	.long	$C$L134	; 8204
	.long	$C$L136	; 0
	.long	$C$L92	; 8206
	.long	$C$L92	; 8207
	.long	$C$L92	; 8208
	.long	$C$L95	; 8209
	.long	$C$L100	; 8210
	.long	$C$L105	; 8211
	.long	$C$L110	; 8212
	.long	$C$L115	; 8213
	.long	$C$L120	; 8214
	.long	$C$L136	; 0
	.long	$C$L121	; 8216
	.long	$C$L121	; 8217
	.long	$C$L121	; 8218
	.long	$C$L121	; 8219
	.long	$C$L121	; 8220
	.long	$C$L121	; 8221
	.long	$C$L121	; 8222
	.long	$C$L121	; 8223
	.long	$C$L121	; 8224
	.long	$C$L121	; 8225
	.long	$C$L136	; 0
	.long	$C$L122	; 8227
	.long	$C$L122	; 8228
	.long	$C$L122	; 8229
	.long	$C$L122	; 8230
	.long	$C$L122	; 8231
	.long	$C$L123	; 8232
	.long	$C$L94	; 8233
	.long	$C$L121	; 8234
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L134	; 8238
	.long	$C$L136	; 0
	.long	$C$L121	; 8240
	.long	$C$L92	; 8241
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L136	; 0
	.long	$C$L121	; 8255
	.long	$C$L92	; 8256
	.long	$C$L121	; 8257
	.long	$C$L92	; 8258
	.long	$C$L92	; 8259
	.long	$C$L92	; 8260
	.sect	".text:_CPWriteSHDataToPacket"
$C$L140:    
	.dwpsn	file "../source/CommProtocol.c",line 1661,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1661 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1661| 
	.dwpsn	file "../source/CommProtocol.c",line 1662,column 1,is_stmt,isa 0
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$280	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$280, DW_AT_low_pc(0x00)
	.dwattr $C$DW$280, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$258, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$258, DW_AT_TI_end_line(0x67e)
	.dwattr $C$DW$258, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$258

	.sect	".text:_CPWriteFirmwareVerToPacket"
	.clink
	.global	_CPWriteFirmwareVerToPacket

$C$DW$281	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$281, DW_AT_name("CPWriteFirmwareVerToPacket")
	.dwattr $C$DW$281, DW_AT_low_pc(_CPWriteFirmwareVerToPacket)
	.dwattr $C$DW$281, DW_AT_high_pc(0x00)
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_CPWriteFirmwareVerToPacket")
	.dwattr $C$DW$281, DW_AT_external
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$281, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$281, DW_AT_TI_begin_line(0x688)
	.dwattr $C$DW$281, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$281, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommProtocol.c",line 1674,column 1,is_stmt,address _CPWriteFirmwareVerToPacket,isa 0

	.dwfde $C$DW$CIE, _CPWriteFirmwareVerToPacket
$C$DW$282	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$282, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$282, DW_AT_location[DW_OP_reg12]

$C$DW$283	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$283, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$283, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1672 | uint16_t CPWriteFirmwareVerToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPac
;     | ket,\                                                                  
; 1673 | bool_t bNAKReceived)                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWriteFirmwareVerToPacket   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CPWriteFirmwareVerToPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$284	.dwtag  DW_TAG_variable
	.dwattr $C$DW$284, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$284, DW_AT_location[DW_OP_breg20 -2]

$C$DW$285	.dwtag  DW_TAG_variable
	.dwattr $C$DW$285, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$285, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 1675 | //!Get the firmware version and update to the data packet              
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |1674| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1674| 
	.dwpsn	file "../source/CommProtocol.c",line 1676,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1676 | SysInitGetFirmwareVer(stPtrParsedPacket->arrusnData);                  
; 1677 | //!Return the firmware version data length                             
;----------------------------------------------------------------------
        MOVB      ACC,#6                ; [CPU_] |1676| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1676| 
        MOVL      XAR4,ACC              ; [CPU_] |1676| 
$C$DW$286	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$286, DW_AT_low_pc(0x00)
	.dwattr $C$DW$286, DW_AT_name("_SysInitGetFirmwareVer")
	.dwattr $C$DW$286, DW_AT_TI_call

        LCR       #_SysInitGetFirmwareVer ; [CPU_] |1676| 
        ; call occurs [#_SysInitGetFirmwareVer] ; [] |1676| 
	.dwpsn	file "../source/CommProtocol.c",line 1678,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1678 | return CP_FIRMWARE_VERSION_LENGTH;                                     
;----------------------------------------------------------------------
        MOVB      AL,#6                 ; [CPU_] |1678| 
	.dwpsn	file "../source/CommProtocol.c",line 1679,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$287	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$287, DW_AT_low_pc(0x00)
	.dwattr $C$DW$287, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$281, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$281, DW_AT_TI_end_line(0x68f)
	.dwattr $C$DW$281, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$281

	.sect	".text:_CPSetNextIndexForADCData"
	.clink
	.global	_CPSetNextIndexForADCData

$C$DW$288	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$288, DW_AT_name("CPSetNextIndexForADCData")
	.dwattr $C$DW$288, DW_AT_low_pc(_CPSetNextIndexForADCData)
	.dwattr $C$DW$288, DW_AT_high_pc(0x00)
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_CPSetNextIndexForADCData")
	.dwattr $C$DW$288, DW_AT_external
	.dwattr $C$DW$288, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$288, DW_AT_TI_begin_line(0x699)
	.dwattr $C$DW$288, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$288, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommProtocol.c",line 1690,column 1,is_stmt,address _CPSetNextIndexForADCData,isa 0

	.dwfde $C$DW$CIE, _CPSetNextIndexForADCData
$C$DW$289	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$289, DW_AT_name("eTypeofCell")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_eTypeofCell")
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$289, DW_AT_location[DW_OP_reg12]

$C$DW$290	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$290, DW_AT_name("uslNextIndex")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_uslNextIndex")
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$290, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1689 | void CPSetNextIndexForADCData(E_CELL_TYPE eTypeofCell, uint32_t uslNext
;     | Index)                                                                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPSetNextIndexForADCData     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CPSetNextIndexForADCData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$291	.dwtag  DW_TAG_variable
	.dwattr $C$DW$291, DW_AT_name("uslNextIndex")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_uslNextIndex")
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$291, DW_AT_location[DW_OP_breg20 -2]

$C$DW$292	.dwtag  DW_TAG_variable
	.dwattr $C$DW$292, DW_AT_name("eTypeofCell")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_eTypeofCell")
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$292, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 1691 | //!Check for the cell type                                             
;----------------------------------------------------------------------
        MOV       *-SP[3],AR4           ; [CPU_] |1690| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1690| 
	.dwpsn	file "../source/CommProtocol.c",line 1692,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1692 | switch(eTypeofCell)                                                    
; 1694 | //!If the cell type is RBC, update the next index for getting the data
;     | from SDRAM                                                             
; 1695 | case eCellRBC:                                                         
;----------------------------------------------------------------------
        B         $C$L144,UNC           ; [CPU_] |1692| 
        ; branch occurs ; [] |1692| 
$C$L141:    
	.dwpsn	file "../source/CommProtocol.c",line 1696,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1696 | m_suslRBCNextIndex = uslNextIndex;                                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[2]           ; [CPU_] |1696| 
        MOVW      DP,#_m_suslRBCNextIndex ; [CPU_U] 
        MOVL      @_m_suslRBCNextIndex,ACC ; [CPU_] |1696| 
	.dwpsn	file "../source/CommProtocol.c",line 1697,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1697 | break;                                                                 
; 1699 | //!If the cell type is PLT, update the next index for getting the data
;     | from SDRAM                                                             
; 1700 | case eCellPLT:                                                         
;----------------------------------------------------------------------
        B         $C$L145,UNC           ; [CPU_] |1697| 
        ; branch occurs ; [] |1697| 
$C$L142:    
	.dwpsn	file "../source/CommProtocol.c",line 1701,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1701 | m_suslPLTNextIndex = uslNextIndex;                                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[2]           ; [CPU_] |1701| 
        MOVW      DP,#_m_suslPLTNextIndex ; [CPU_U] 
        MOVL      @_m_suslPLTNextIndex,ACC ; [CPU_] |1701| 
	.dwpsn	file "../source/CommProtocol.c",line 1702,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1702 | break;                                                                 
; 1704 | //!If the cell type is WBC, update the next index for getting the data
;     | from SDRAM                                                             
; 1705 | case eCellWBC:                                                         
;----------------------------------------------------------------------
        B         $C$L145,UNC           ; [CPU_] |1702| 
        ; branch occurs ; [] |1702| 
$C$L143:    
	.dwpsn	file "../source/CommProtocol.c",line 1706,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1706 | m_suslWBCNextIndex = uslNextIndex;                                     
;----------------------------------------------------------------------
        MOVL      ACC,*-SP[2]           ; [CPU_] |1706| 
        MOVW      DP,#_m_suslWBCNextIndex ; [CPU_U] 
        MOVL      @_m_suslWBCNextIndex,ACC ; [CPU_] |1706| 
	.dwpsn	file "../source/CommProtocol.c",line 1707,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1707 | break;                                                                 
; 1709 | default:                                                               
; 1710 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L145,UNC           ; [CPU_] |1707| 
        ; branch occurs ; [] |1707| 
$C$L144:    
	.dwpsn	file "../source/CommProtocol.c",line 1692,column 2,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1692| 
        CMPB      AL,#1                 ; [CPU_] |1692| 
        B         $C$L141,EQ            ; [CPU_] |1692| 
        ; branchcc occurs ; [] |1692| 
        CMPB      AL,#2                 ; [CPU_] |1692| 
        B         $C$L142,EQ            ; [CPU_] |1692| 
        ; branchcc occurs ; [] |1692| 
        CMPB      AL,#3                 ; [CPU_] |1692| 
        B         $C$L143,EQ            ; [CPU_] |1692| 
        ; branchcc occurs ; [] |1692| 
        B         $C$L145,UNC           ; [CPU_] |1692| 
        ; branch occurs ; [] |1692| 
$C$L145:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$293	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$293, DW_AT_low_pc(0x00)
	.dwattr $C$DW$293, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$288, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$288, DW_AT_TI_end_line(0x6b1)
	.dwattr $C$DW$288, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$288

	.sect	".text:_CPGetNextIndexForADCData"
	.clink
	.global	_CPGetNextIndexForADCData

$C$DW$294	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$294, DW_AT_name("CPGetNextIndexForADCData")
	.dwattr $C$DW$294, DW_AT_low_pc(_CPGetNextIndexForADCData)
	.dwattr $C$DW$294, DW_AT_high_pc(0x00)
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_CPGetNextIndexForADCData")
	.dwattr $C$DW$294, DW_AT_external
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$294, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$294, DW_AT_TI_begin_line(0x6ba)
	.dwattr $C$DW$294, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$294, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommProtocol.c",line 1723,column 1,is_stmt,address _CPGetNextIndexForADCData,isa 0

	.dwfde $C$DW$CIE, _CPGetNextIndexForADCData
$C$DW$295	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$295, DW_AT_name("eTypeofCell")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_eTypeofCell")
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$295, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1722 | uint32_t CPGetNextIndexForADCData(E_CELL_TYPE eTypeofCell)             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPGetNextIndexForADCData     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CPGetNextIndexForADCData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$296	.dwtag  DW_TAG_variable
	.dwattr $C$DW$296, DW_AT_name("uslIndex")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_uslIndex")
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$296, DW_AT_location[DW_OP_breg20 -2]

$C$DW$297	.dwtag  DW_TAG_variable
	.dwattr $C$DW$297, DW_AT_name("eTypeofCell")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_eTypeofCell")
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$297, DW_AT_location[DW_OP_breg20 -3]

        MOV       *-SP[3],AL            ; [CPU_] |1723| 
	.dwpsn	file "../source/CommProtocol.c",line 1724,column 20,is_stmt,isa 0
;----------------------------------------------------------------------
; 1724 | uint32_t uslIndex = 0;                                                 
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1724| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1724| 
	.dwpsn	file "../source/CommProtocol.c",line 1725,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1725 | switch (eTypeofCell)                                                   
; 1727 |     //!If the cell type is RBC, get the next index for getting the data
;     |  from SDRAM                                                            
; 1728 |         case eCellRBC:                                                 
;----------------------------------------------------------------------
        B         $C$L150,UNC           ; [CPU_] |1725| 
        ; branch occurs ; [] |1725| 
$C$L146:    
	.dwpsn	file "../source/CommProtocol.c",line 1729,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1729 | uslIndex = m_suslRBCNextIndex;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_suslRBCNextIndex ; [CPU_U] 
        MOVL      ACC,@_m_suslRBCNextIndex ; [CPU_] |1729| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1729| 
	.dwpsn	file "../source/CommProtocol.c",line 1730,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1730 | break;                                                                 
; 1732 | //!If the cell type is PLT, get the next index for getting the data fro
;     | m SDRAM                                                                
; 1733 | case eCellPLT:                                                         
;----------------------------------------------------------------------
        B         $C$L151,UNC           ; [CPU_] |1730| 
        ; branch occurs ; [] |1730| 
$C$L147:    
	.dwpsn	file "../source/CommProtocol.c",line 1734,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1734 | uslIndex = m_suslPLTNextIndex;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_suslPLTNextIndex ; [CPU_U] 
        MOVL      ACC,@_m_suslPLTNextIndex ; [CPU_] |1734| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1734| 
	.dwpsn	file "../source/CommProtocol.c",line 1735,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1735 | break;                                                                 
; 1737 | //!If the cell type is WBC, get the next index for getting the data fro
;     | m SDRAM                                                                
; 1738 | case eCellWBC:                                                         
;----------------------------------------------------------------------
        B         $C$L151,UNC           ; [CPU_] |1735| 
        ; branch occurs ; [] |1735| 
$C$L148:    
	.dwpsn	file "../source/CommProtocol.c",line 1739,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1739 | uslIndex = m_suslWBCNextIndex;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_suslWBCNextIndex ; [CPU_U] 
        MOVL      ACC,@_m_suslWBCNextIndex ; [CPU_] |1739| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1739| 
	.dwpsn	file "../source/CommProtocol.c",line 1740,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1740 | break;                                                                 
; 1742 | default:                                                               
;----------------------------------------------------------------------
        B         $C$L151,UNC           ; [CPU_] |1740| 
        ; branch occurs ; [] |1740| 
$C$L149:    
	.dwpsn	file "../source/CommProtocol.c",line 1743,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1743 | uslIndex = NULL;                                                       
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1743| 
	.dwpsn	file "../source/CommProtocol.c",line 1744,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1744 | break;                                                                 
; 1746 | //!Return the index                                                    
;----------------------------------------------------------------------
        B         $C$L151,UNC           ; [CPU_] |1744| 
        ; branch occurs ; [] |1744| 
$C$L150:    
	.dwpsn	file "../source/CommProtocol.c",line 1725,column 2,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |1725| 
        CMPB      AL,#1                 ; [CPU_] |1725| 
        B         $C$L146,EQ            ; [CPU_] |1725| 
        ; branchcc occurs ; [] |1725| 
        CMPB      AL,#2                 ; [CPU_] |1725| 
        B         $C$L147,EQ            ; [CPU_] |1725| 
        ; branchcc occurs ; [] |1725| 
        CMPB      AL,#3                 ; [CPU_] |1725| 
        B         $C$L148,EQ            ; [CPU_] |1725| 
        ; branchcc occurs ; [] |1725| 
        B         $C$L149,UNC           ; [CPU_] |1725| 
        ; branch occurs ; [] |1725| 
$C$L151:    
	.dwpsn	file "../source/CommProtocol.c",line 1747,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1747 | return uslIndex;                                                       
;----------------------------------------------------------------------
	.dwpsn	file "../source/CommProtocol.c",line 1748,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$298	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$298, DW_AT_low_pc(0x00)
	.dwattr $C$DW$298, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$294, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$294, DW_AT_TI_end_line(0x6d4)
	.dwattr $C$DW$294, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$294

	.sect	".text:_CPReInitialiseOnNewMeasurement"
	.clink
	.global	_CPReInitialiseOnNewMeasurement

$C$DW$299	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$299, DW_AT_name("CPReInitialiseOnNewMeasurement")
	.dwattr $C$DW$299, DW_AT_low_pc(_CPReInitialiseOnNewMeasurement)
	.dwattr $C$DW$299, DW_AT_high_pc(0x00)
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_CPReInitialiseOnNewMeasurement")
	.dwattr $C$DW$299, DW_AT_external
	.dwattr $C$DW$299, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$299, DW_AT_TI_begin_line(0x6dd)
	.dwattr $C$DW$299, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$299, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/CommProtocol.c",line 1758,column 1,is_stmt,address _CPReInitialiseOnNewMeasurement,isa 0

	.dwfde $C$DW$CIE, _CPReInitialiseOnNewMeasurement
;----------------------------------------------------------------------
; 1757 | void CPReInitialiseOnNewMeasurement()                                  
; 1759 | //!Initialize the RBC next index                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPReInitialiseOnNewMeasurement FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_CPReInitialiseOnNewMeasurement:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/CommProtocol.c",line 1760,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1760 | m_suslRBCNextIndex = 0;                                                
; 1761 | //!Initialize the WBC next index                                       
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1760| 
        MOVW      DP,#_m_suslRBCNextIndex ; [CPU_U] 
        MOVL      @_m_suslRBCNextIndex,ACC ; [CPU_] |1760| 
	.dwpsn	file "../source/CommProtocol.c",line 1762,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1762 | m_suslWBCNextIndex = 0;                                                
; 1763 | //!Initialize the PLT next index                                       
;----------------------------------------------------------------------
        MOVL      @_m_suslWBCNextIndex,ACC ; [CPU_] |1762| 
	.dwpsn	file "../source/CommProtocol.c",line 1764,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1764 | m_suslPLTNextIndex = 0;                                                
; 1766 | //!Reset the multiple packet to false                                  
;----------------------------------------------------------------------
        MOVL      @_m_suslPLTNextIndex,ACC ; [CPU_] |1764| 
	.dwpsn	file "../source/CommProtocol.c",line 1767,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1767 | m_bMutiplePackets = FALSE;                                             
; 1768 | //!Reset the last multiple packet to false                             
;----------------------------------------------------------------------
        MOV       @_m_bMutiplePackets,#0 ; [CPU_] |1767| 
	.dwpsn	file "../source/CommProtocol.c",line 1769,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1769 | m_bLastMultiplePacket = FALSE;                                         
; 1771 | //!Reset the volumetric error                                          
;----------------------------------------------------------------------
        MOV       @_m_bLastMultiplePacket,#0 ; [CPU_] |1769| 
	.dwpsn	file "../source/CommProtocol.c",line 1772,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1772 | utVolumetricErrorInfo.unVolumetricError = ZERO;                        
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfo ; [CPU_U] 
        MOVL      @_utVolumetricErrorInfo,ACC ; [CPU_] |1772| 
	.dwpsn	file "../source/CommProtocol.c",line 1773,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1773 | utVolumetricErrorInfoReAcq.unVolumetricError = ZERO;                   
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        MOVL      @_utVolumetricErrorInfoReAcq,ACC ; [CPU_] |1773| 
	.dwpsn	file "../source/CommProtocol.c",line 1775,column 1,is_stmt,isa 0
$C$DW$300	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$300, DW_AT_low_pc(0x00)
	.dwattr $C$DW$300, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$299, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$299, DW_AT_TI_end_line(0x6ef)
	.dwattr $C$DW$299, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$299

	.sect	".text:_CPReTransmistADCRawData"
	.clink
	.global	_CPReTransmistADCRawData

$C$DW$301	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$301, DW_AT_name("CPReTransmistADCRawData")
	.dwattr $C$DW$301, DW_AT_low_pc(_CPReTransmistADCRawData)
	.dwattr $C$DW$301, DW_AT_high_pc(0x00)
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_CPReTransmistADCRawData")
	.dwattr $C$DW$301, DW_AT_external
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$301, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$301, DW_AT_TI_begin_line(0x6f8)
	.dwattr $C$DW$301, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$301, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/CommProtocol.c",line 1786,column 1,is_stmt,address _CPReTransmistADCRawData,isa 0

	.dwfde $C$DW$CIE, _CPReTransmistADCRawData
$C$DW$302	.dwtag  DW_TAG_variable
	.dwattr $C$DW$302, DW_AT_name("susnNoOfPackets")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_susnNoOfPackets$7")
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$302, DW_AT_location[DW_OP_addr _susnNoOfPackets$7]

$C$DW$303	.dwtag  DW_TAG_variable
	.dwattr $C$DW$303, DW_AT_name("usnPrevIndex")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_usnPrevIndex$8")
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$303, DW_AT_location[DW_OP_addr _usnPrevIndex$8]

$C$DW$304	.dwtag  DW_TAG_variable
	.dwattr $C$DW$304, DW_AT_name("usnADCRemainder")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_usnADCRemainder$6")
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$304, DW_AT_location[DW_OP_addr _usnADCRemainder$6]

$C$DW$305	.dwtag  DW_TAG_variable
	.dwattr $C$DW$305, DW_AT_name("usnNextIndex")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_usnNextIndex$9")
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$305, DW_AT_location[DW_OP_addr _usnNextIndex$9]

$C$DW$306	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$306, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$306, DW_AT_location[DW_OP_reg12]

$C$DW$307	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$307, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$307, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1784 | uint16_t CPReTransmistADCRawData(ST_SPI_PACKET_FRAME *stPtrParsedPacket
;     | ,\                                                                     
; 1785 | uint16_t usnSubCmd)                                                    
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPReTransmistADCRawData      FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_CPReTransmistADCRawData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$308	.dwtag  DW_TAG_variable
	.dwattr $C$DW$308, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$308, DW_AT_location[DW_OP_breg20 -4]

$C$DW$309	.dwtag  DW_TAG_variable
	.dwattr $C$DW$309, DW_AT_name("uslDataSize")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_uslDataSize")
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$309, DW_AT_location[DW_OP_breg20 -6]

$C$DW$310	.dwtag  DW_TAG_variable
	.dwattr $C$DW$310, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$310, DW_AT_location[DW_OP_breg20 -7]

$C$DW$311	.dwtag  DW_TAG_variable
	.dwattr $C$DW$311, DW_AT_name("usnDataLength")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$311, DW_AT_location[DW_OP_breg20 -8]

;----------------------------------------------------------------------
; 1787 | //!Initialize the data length, ADC reminder, No. of packets,           
; 1788 | //! Previous index and present index                                   
;----------------------------------------------------------------------
        MOV       *-SP[7],AL            ; [CPU_] |1786| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |1786| 
	.dwpsn	file "../source/CommProtocol.c",line 1789,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 1789 | uint16_t usnDataLength = 0;                                            
; 1790 | //static uint16_t usnADCRemainder = 0;                                 
; 1791 | static uint32_t usnADCRemainder = 0;    //QA_C                         
; 1792 | //static uint16_t susnNoOfPackets = 0;                                 
; 1793 | //msg(3:4461) A non-constant expression of 'essentially unsigned' type
;     | (unsigned long)                                                        
; 1794 | //is being converted to narrower unsigned type, 'unsigned int' on assig
;     | nment.                                                                 
; 1795 | static uint32_t susnNoOfPackets = 0;     //QA_C                        
; 1796 | static uint32_t usnPrevIndex = 0;                                      
; 1797 | static uint32_t usnNextIndex =0;                                       
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |1789| 
	.dwpsn	file "../source/CommProtocol.c",line 1798,column 23,is_stmt,isa 0
;----------------------------------------------------------------------
; 1798 | uint32_t uslDataSize = 0;                                              
; 1800 | //!If the no. of packets to be transmitted is null                     
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1798| 
        MOVL      *-SP[6],ACC           ; [CPU_] |1798| 
	.dwpsn	file "../source/CommProtocol.c",line 1801,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1801 | if(susnNoOfPackets == NULL)                                            
; 1803 |     //!Get the next index for RAW ADC Data                             
;----------------------------------------------------------------------
        MOVW      DP,#_susnNoOfPackets$7 ; [CPU_U] 
        MOVL      ACC,@_susnNoOfPackets$7 ; [CPU_] |1801| 
        B         $C$L154,NEQ           ; [CPU_] |1801| 
        ; branchcc occurs ; [] |1801| 
	.dwpsn	file "../source/CommProtocol.c",line 1804,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1804 | usnPrevIndex = CPGetNextIndexForADCData(m_eTypeofCell);                
; 1805 | //!Set next index to Zero                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOV       AL,@_m_eTypeofCell    ; [CPU_] |1804| 
$C$DW$312	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$312, DW_AT_low_pc(0x00)
	.dwattr $C$DW$312, DW_AT_name("_CPGetNextIndexForADCData")
	.dwattr $C$DW$312, DW_AT_TI_call

        LCR       #_CPGetNextIndexForADCData ; [CPU_] |1804| 
        ; call occurs [#_CPGetNextIndexForADCData] ; [] |1804| 
        MOVW      DP,#_usnPrevIndex$8   ; [CPU_U] 
        MOVL      @_usnPrevIndex$8,ACC  ; [CPU_] |1804| 
	.dwpsn	file "../source/CommProtocol.c",line 1806,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1806 | usnNextIndex = 0;                                                      
; 1807 | //!ADC Reminder to Zero                                                
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1806| 
        MOVL      @_usnNextIndex$9,ACC  ; [CPU_] |1806| 
	.dwpsn	file "../source/CommProtocol.c",line 1808,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1808 | usnADCRemainder =0;                                                    
; 1809 | //!ADC Data size as maximum packet data size                           
;----------------------------------------------------------------------
        MOVL      @_usnADCRemainder$6,ACC ; [CPU_] |1808| 
	.dwpsn	file "../source/CommProtocol.c",line 1810,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1810 | uslDataSize = RAW_ADC_DATA_SIZE ;                                      
; 1812 | //!If the Data size is less than the Maximum data size, set the        
; 1813 | //! packet count to and no.of packet to Zero and set                   
; 1814 | //! last multiple packet to Zero                                       
;----------------------------------------------------------------------
        MOVL      XAR4,#1004000         ; [CPU_U] |1810| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |1810| 
	.dwpsn	file "../source/CommProtocol.c",line 1815,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1815 | if(uslDataSize<MAXIMUM_DATA_SIZE)                                      
;----------------------------------------------------------------------
        MOVL      XAR4,#502             ; [CPU_U] |1815| 
        MOVL      ACC,XAR4              ; [CPU_] |1815| 
        CMPL      ACC,*-SP[6]           ; [CPU_] |1815| 
        B         $C$L152,LOS           ; [CPU_] |1815| 
        ; branchcc occurs ; [] |1815| 
	.dwpsn	file "../source/CommProtocol.c",line 1817,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1817 | susnNoOfPackets = 0;                                                   
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |1817| 
        MOVL      @_susnNoOfPackets$7,ACC ; [CPU_] |1817| 
	.dwpsn	file "../source/CommProtocol.c",line 1818,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1818 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1818| 
	.dwpsn	file "../source/CommProtocol.c",line 1819,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1819 | m_bLastMultiplePacket = FALSE;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_bLastMultiplePacket ; [CPU_U] 
        MOV       @_m_bLastMultiplePacket,#0 ; [CPU_] |1819| 
	.dwpsn	file "../source/CommProtocol.c",line 1820,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1821 | else                                                                   
; 1823 |         //!every packet can contain 502 WORD (1008 bytes) of data.     
; 1824 |         //! and calculate the ADC reminder                             
;----------------------------------------------------------------------
        B         $C$L155,UNC           ; [CPU_] |1820| 
        ; branch occurs ; [] |1820| 
$C$L152:    
	.dwpsn	file "../source/CommProtocol.c",line 1825,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1825 | usnADCRemainder = uslDataSize % MAXIMUM_DATA_SIZE;                     
; 1826 | //!Set no. of packet and packet count, based on the reminder           
;----------------------------------------------------------------------
        MOVL      P,*-SP[6]             ; [CPU_] |1825| 
        MOVB      ACC,#0                ; [CPU_] |1825| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |1825| 
        MOVL      @_usnADCRemainder$6,ACC ; [CPU_] |1825| 
	.dwpsn	file "../source/CommProtocol.c",line 1827,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1827 | if(usnADCRemainder == 0)                                               
;----------------------------------------------------------------------
        MOVL      ACC,@_usnADCRemainder$6 ; [CPU_] |1827| 
        B         $C$L153,NEQ           ; [CPU_] |1827| 
        ; branchcc occurs ; [] |1827| 
	.dwpsn	file "../source/CommProtocol.c",line 1830,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1830 | susnNoOfPackets = uslDataSize /MAXIMUM_DATA_SIZE;                      
;----------------------------------------------------------------------
        MOVL      P,*-SP[6]             ; [CPU_] |1830| 
        MOVB      ACC,#0                ; [CPU_] |1830| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |1830| 
        MOVL      @_susnNoOfPackets$7,P ; [CPU_] |1830| 
	.dwpsn	file "../source/CommProtocol.c",line 1831,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1831 | m_usnPacketCount = susnNoOfPackets -1;                                 
;----------------------------------------------------------------------
        MOV       AL,@_susnNoOfPackets$7 ; [CPU_] |1831| 
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        ADDB      AL,#-1                ; [CPU_] |1831| 
        MOV       @_m_usnPacketCount,AL ; [CPU_] |1831| 
	.dwpsn	file "../source/CommProtocol.c",line 1832,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1833 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L155,UNC           ; [CPU_] |1832| 
        ; branch occurs ; [] |1832| 
$C$L153:    
	.dwpsn	file "../source/CommProtocol.c",line 1836,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1836 | susnNoOfPackets = uslDataSize /MAXIMUM_DATA_SIZE;                      
;----------------------------------------------------------------------
        MOVL      P,*-SP[6]             ; [CPU_] |1836| 
        MOVB      ACC,#0                ; [CPU_] |1836| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |1836| 
        MOVL      @_susnNoOfPackets$7,P ; [CPU_] |1836| 
	.dwpsn	file "../source/CommProtocol.c",line 1837,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1837 | susnNoOfPackets =  susnNoOfPackets + 1;                                
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1837| 
        ADDL      @_susnNoOfPackets$7,ACC ; [CPU_] |1837| 
	.dwpsn	file "../source/CommProtocol.c",line 1838,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1838 | m_usnPacketCount = susnNoOfPackets - 1;                                
;----------------------------------------------------------------------
        MOV       AL,@_susnNoOfPackets$7 ; [CPU_] |1838| 
        ADDB      AL,#-1                ; [CPU_] |1838| 
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,AL ; [CPU_] |1838| 
	.dwpsn	file "../source/CommProtocol.c",line 1841,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1842 | else                                                                   
; 1844 |     //!Decrement the packet count and no. of packet                    
;----------------------------------------------------------------------
        B         $C$L155,UNC           ; [CPU_] |1841| 
        ; branch occurs ; [] |1841| 
$C$L154:    
	.dwpsn	file "../source/CommProtocol.c",line 1845,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1845 | susnNoOfPackets--;                                                     
; 1846 | //Update m_usnPacketCount to set the remaining chunks                  
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1845| 
        SUBL      @_susnNoOfPackets$7,ACC ; [CPU_] |1845| 
	.dwpsn	file "../source/CommProtocol.c",line 1847,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1847 | m_usnPacketCount = susnNoOfPackets - 1;                                
; 1850 | //!Set the data length based on the packet count                       
;----------------------------------------------------------------------
        MOV       AL,@_susnNoOfPackets$7 ; [CPU_] |1847| 
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        ADDB      AL,#-1                ; [CPU_] |1847| 
        MOV       @_m_usnPacketCount,AL ; [CPU_] |1847| 
$C$L155:    
	.dwpsn	file "../source/CommProtocol.c",line 1851,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1851 | if(m_usnPacketCount != NULL)                                           
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       AL,@_m_usnPacketCount ; [CPU_] |1851| 
        B         $C$L156,EQ            ; [CPU_] |1851| 
        ; branchcc occurs ; [] |1851| 
	.dwpsn	file "../source/CommProtocol.c",line 1853,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1853 | usnDataLength = MAXIMUM_DATA_SIZE;                                     
;----------------------------------------------------------------------
        MOV       *-SP[8],#502          ; [CPU_] |1853| 
	.dwpsn	file "../source/CommProtocol.c",line 1854,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1855 | else                                                                   
; 1857 |     //!Set the data length of the packet based on the Data size        
;----------------------------------------------------------------------
        B         $C$L159,UNC           ; [CPU_] |1854| 
        ; branch occurs ; [] |1854| 
$C$L156:    
	.dwpsn	file "../source/CommProtocol.c",line 1858,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1858 | if(uslDataSize<MAXIMUM_DATA_SIZE)                                      
;----------------------------------------------------------------------
        MOVL      XAR4,#502             ; [CPU_U] |1858| 
        MOVL      ACC,XAR4              ; [CPU_] |1858| 
        CMPL      ACC,*-SP[6]           ; [CPU_] |1858| 
        B         $C$L157,LOS           ; [CPU_] |1858| 
        ; branchcc occurs ; [] |1858| 
	.dwpsn	file "../source/CommProtocol.c",line 1860,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1860 | usnDataLength = (uint16_t)uslDataSize;                                 
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1860| 
        MOV       *-SP[8],AL            ; [CPU_] |1860| 
	.dwpsn	file "../source/CommProtocol.c",line 1861,column 3,is_stmt,isa 0
        B         $C$L159,UNC           ; [CPU_] |1861| 
        ; branch occurs ; [] |1861| 
$C$L157:    
	.dwpsn	file "../source/CommProtocol.c",line 1862,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1862 | else if(usnADCRemainder != NULL)                                       
;----------------------------------------------------------------------
        MOVW      DP,#_usnADCRemainder$6 ; [CPU_U] 
        MOVL      ACC,@_usnADCRemainder$6 ; [CPU_] |1862| 
        B         $C$L158,EQ            ; [CPU_] |1862| 
        ; branchcc occurs ; [] |1862| 
	.dwpsn	file "../source/CommProtocol.c",line 1864,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1864 | usnDataLength = usnADCRemainder;                                       
;----------------------------------------------------------------------
        MOV       AL,@_usnADCRemainder$6 ; [CPU_] |1864| 
        MOV       *-SP[8],AL            ; [CPU_] |1864| 
	.dwpsn	file "../source/CommProtocol.c",line 1865,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1866 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L159,UNC           ; [CPU_] |1865| 
        ; branch occurs ; [] |1865| 
$C$L158:    
	.dwpsn	file "../source/CommProtocol.c",line 1868,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1868 | usnDataLength = MAXIMUM_DATA_SIZE;                                     
; 1872 | //!Update the ADC raw data to the packet to send to Sitara and update t
;     | he                                                                     
; 1873 | //! next index to send data to Sitara                                  
;----------------------------------------------------------------------
        MOV       *-SP[8],#502          ; [CPU_] |1868| 
$C$L159:    
	.dwpsn	file "../source/CommProtocol.c",line 1874,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1874 | usnNextIndex =  SysGetADCRawDataFrmSDRAM(m_eTypeofCell, \              
; 1875 |         stPtrParsedPacket->arrusnData,usnPrevIndex,usnDataLength);     
; 1877 | //!Reset the variables based on the data transmitted to sitara         
;----------------------------------------------------------------------
        MOVZ      AR6,*-SP[8]           ; [CPU_] |1874| 
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      ACC,#6                ; [CPU_] |1874| 
        MOVZ      AR5,@_m_eTypeofCell   ; [CPU_] |1874| 
        MOV       *-SP[1],AR6           ; [CPU_] |1874| 
        MOVW      DP,#_usnPrevIndex$8   ; [CPU_U] 
        ADDL      ACC,*-SP[4]           ; [CPU_] |1874| 
        MOVL      XAR4,ACC              ; [CPU_] |1874| 
        MOVL      ACC,@_usnPrevIndex$8  ; [CPU_] |1874| 
$C$DW$313	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$313, DW_AT_low_pc(0x00)
	.dwattr $C$DW$313, DW_AT_name("_SysGetADCRawDataFrmSDRAM")
	.dwattr $C$DW$313, DW_AT_TI_call

        LCR       #_SysGetADCRawDataFrmSDRAM ; [CPU_] |1874| 
        ; call occurs [#_SysGetADCRawDataFrmSDRAM] ; [] |1874| 
        MOVW      DP,#_usnNextIndex$9   ; [CPU_U] 
        MOVL      @_usnNextIndex$9,ACC  ; [CPU_] |1874| 
	.dwpsn	file "../source/CommProtocol.c",line 1878,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1878 | if(susnNoOfPackets == 1)                                               
; 1881 |     //!Update the index to copy data from SDRAM to for paket to Sitara 
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |1878| 
        CMPL      ACC,@_susnNoOfPackets$7 ; [CPU_] |1878| 
        B         $C$L160,NEQ           ; [CPU_] |1878| 
        ; branchcc occurs ; [] |1878| 
	.dwpsn	file "../source/CommProtocol.c",line 1882,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1882 | CPSetNextIndexForADCData(m_eTypeofCell,usnNextIndex);                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVZ      AR4,@_m_eTypeofCell   ; [CPU_] |1882| 
        MOVW      DP,#_usnNextIndex$9   ; [CPU_U] 
        MOVL      ACC,@_usnNextIndex$9  ; [CPU_] |1882| 
$C$DW$314	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$314, DW_AT_low_pc(0x00)
	.dwattr $C$DW$314, DW_AT_name("_CPSetNextIndexForADCData")
	.dwattr $C$DW$314, DW_AT_TI_call

        LCR       #_CPSetNextIndexForADCData ; [CPU_] |1882| 
        ; call occurs [#_CPSetNextIndexForADCData] ; [] |1882| 
	.dwpsn	file "../source/CommProtocol.c",line 1884,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1884 | usnPrevIndex = 0;                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_usnPrevIndex$8   ; [CPU_U] 
        MOVB      ACC,#0                ; [CPU_] |1884| 
        MOVL      @_usnPrevIndex$8,ACC  ; [CPU_] |1884| 
	.dwpsn	file "../source/CommProtocol.c",line 1885,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1885 | usnNextIndex = 0;                                                      
;----------------------------------------------------------------------
        MOVL      @_usnNextIndex$9,ACC  ; [CPU_] |1885| 
	.dwpsn	file "../source/CommProtocol.c",line 1886,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1886 | usnADCRemainder = 0;                                                   
;----------------------------------------------------------------------
        MOVL      @_usnADCRemainder$6,ACC ; [CPU_] |1886| 
	.dwpsn	file "../source/CommProtocol.c",line 1887,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1887 | susnNoOfPackets =0;                                                    
;----------------------------------------------------------------------
        MOVL      @_susnNoOfPackets$7,ACC ; [CPU_] |1887| 
	.dwpsn	file "../source/CommProtocol.c",line 1888,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1888 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1888| 
	.dwpsn	file "../source/CommProtocol.c",line 1889,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1889 | m_bLastMultiplePacket = TRUE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_m_bLastMultiplePacket ; [CPU_U] 
        MOVB      @_m_bLastMultiplePacket,#1,UNC ; [CPU_] |1889| 
	.dwpsn	file "../source/CommProtocol.c",line 1890,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1890 | m_bNAckReceived = FALSE;                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOV       @_m_bNAckReceived,#0  ; [CPU_] |1890| 
	.dwpsn	file "../source/CommProtocol.c",line 1891,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1892 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L161,UNC           ; [CPU_] |1891| 
        ; branch occurs ; [] |1891| 
$C$L160:    
	.dwpsn	file "../source/CommProtocol.c",line 1894,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1894 | usnPrevIndex = usnNextIndex;                                           
;----------------------------------------------------------------------
        MOVL      ACC,@_usnNextIndex$9  ; [CPU_] |1894| 
        MOVL      @_usnPrevIndex$8,ACC  ; [CPU_] |1894| 
	.dwpsn	file "../source/CommProtocol.c",line 1895,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1895 | m_bLastMultiplePacket = FALSE;                                         
; 1898 | //!Return the data length                                              
;----------------------------------------------------------------------
        MOV       @_m_bLastMultiplePacket,#0 ; [CPU_] |1895| 
$C$L161:    
	.dwpsn	file "../source/CommProtocol.c",line 1899,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1899 | return usnDataLength;//return here                                     
;----------------------------------------------------------------------
        MOV       AL,*-SP[8]            ; [CPU_] |1899| 
	.dwpsn	file "../source/CommProtocol.c",line 1900,column 1,is_stmt,isa 0
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$315	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$315, DW_AT_low_pc(0x00)
	.dwattr $C$DW$315, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$301, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$301, DW_AT_TI_end_line(0x76c)
	.dwattr $C$DW$301, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$301

	.sect	".text:_CPWriteCHDataToPacket"
	.clink
	.global	_CPWriteCHDataToPacket

$C$DW$316	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$316, DW_AT_name("CPWriteCHDataToPacket")
	.dwattr $C$DW$316, DW_AT_low_pc(_CPWriteCHDataToPacket)
	.dwattr $C$DW$316, DW_AT_high_pc(0x00)
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_CPWriteCHDataToPacket")
	.dwattr $C$DW$316, DW_AT_external
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$316, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$316, DW_AT_TI_begin_line(0x776)
	.dwattr $C$DW$316, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$316, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommProtocol.c",line 1912,column 1,is_stmt,address _CPWriteCHDataToPacket,isa 0

	.dwfde $C$DW$CIE, _CPWriteCHDataToPacket
$C$DW$317	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$317, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$317, DW_AT_location[DW_OP_reg12]

$C$DW$318	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$318, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$318, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 1910 | uint16_t CPWriteCHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
; 1911 | bool_t bNAKReceived)                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWriteCHDataToPacket        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CPWriteCHDataToPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$319	.dwtag  DW_TAG_variable
	.dwattr $C$DW$319, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$319, DW_AT_location[DW_OP_breg20 -2]

$C$DW$320	.dwtag  DW_TAG_variable
	.dwattr $C$DW$320, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$320, DW_AT_location[DW_OP_breg20 -3]

$C$DW$321	.dwtag  DW_TAG_variable
	.dwattr $C$DW$321, DW_AT_name("usnDataLength")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$321, DW_AT_location[DW_OP_breg20 -4]

$C$DW$322	.dwtag  DW_TAG_variable
	.dwattr $C$DW$322, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$322, DW_AT_location[DW_OP_breg20 -5]

        MOV       *-SP[3],AL            ; [CPU_] |1912| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1912| 
	.dwpsn	file "../source/CommProtocol.c",line 1913,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 1913 | uint16_t usnDataLength = 0;                                            
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |1913| 
	.dwpsn	file "../source/CommProtocol.c",line 1914,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 1914 | uint16_t usnSubCmd =0;                                                 
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |1914| 
	.dwpsn	file "../source/CommProtocol.c",line 1916,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1916 | usnSubCmd = stPtrParsedPacket->usnMinorCommand;                        
; 1918 | //!Depending upon the minor command frame the data packet              
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[5]          ; [CPU_] |1916| 
        MOV       *-SP[5],AL            ; [CPU_] |1916| 
	.dwpsn	file "../source/CommProtocol.c",line 1919,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1919 | switch(usnSubCmd)                                                      
; 1921 |     //!If the minor command is WBC, RBC, PLT count, then packetize the
;     | results                                                                
; 1922 |             case DELFINO_WBC_RBC_PLT_HGB_COUNT:                        
;----------------------------------------------------------------------
        B         $C$L169,UNC           ; [CPU_] |1919| 
        ; branch occurs ; [] |1919| 
$C$L162:    
	.dwpsn	file "../source/CommProtocol.c",line 1924,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1924 | usnDataLength = CPGetCountData(stPtrParsedPacket);                     
;----------------------------------------------------------------------
$C$DW$323	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$323, DW_AT_low_pc(0x00)
	.dwattr $C$DW$323, DW_AT_name("_CPGetCountData")
	.dwattr $C$DW$323, DW_AT_TI_call

        LCR       #_CPGetCountData      ; [CPU_] |1924| 
        ; call occurs [#_CPGetCountData] ; [] |1924| 
        MOV       *-SP[4],AL            ; [CPU_] |1924| 
	.dwpsn	file "../source/CommProtocol.c",line 1926,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1926 | break;                                                                 
; 1928 | //!If the minor command is pulse height, then packetize the pulse heigh
;     | t data                                                                 
; 1929 | case DELFINO_WBC_PULSE_HEIGHT_DATA:                                    
; 1930 | case DELFINO_RBC_PULSE_HEIGHT_DATA:                                    
; 1931 | case DELFINO_PLT_PULSE_HEIGHT_DATA:                                    
;----------------------------------------------------------------------
        B         $C$L171,UNC           ; [CPU_] |1926| 
        ; branch occurs ; [] |1926| 
$C$L163:    
	.dwpsn	file "../source/CommProtocol.c",line 1933,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1933 | if(bNAKReceived == FALSE)                                              
; 1935 |     //!fn. to get Pulse height or ADC RAW data                         
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |1933| 
        B         $C$L164,NEQ           ; [CPU_] |1933| 
        ; branchcc occurs ; [] |1933| 
	.dwpsn	file "../source/CommProtocol.c",line 1936,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1936 | usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\            
; 1937 |         usnSubCmd, FALSE);                                             
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |1936| 
        MOVB      AH,#0                 ; [CPU_] |1936| 
$C$DW$324	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$324, DW_AT_low_pc(0x00)
	.dwattr $C$DW$324, DW_AT_name("_CPGetRawADCOrHeightData")
	.dwattr $C$DW$324, DW_AT_TI_call

        LCR       #_CPGetRawADCOrHeightData ; [CPU_] |1936| 
        ; call occurs [#_CPGetRawADCOrHeightData] ; [] |1936| 
        MOV       *-SP[4],AL            ; [CPU_] |1936| 
	.dwpsn	file "../source/CommProtocol.c",line 1938,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1939 | else                                                                   
; 1941 |     //!fn. to get Pulse height or ADC RAW data                         
;----------------------------------------------------------------------
        B         $C$L171,UNC           ; [CPU_] |1938| 
        ; branch occurs ; [] |1938| 
$C$L164:    
	.dwpsn	file "../source/CommProtocol.c",line 1942,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1942 | usnDataLength =\                                                       
; 1943 |         CPGetHeightDataForReTransmission(stPtrParsedPacket,\           
; 1944 |                 usnSubCmd);                                            
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |1942| 
$C$DW$325	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$325, DW_AT_low_pc(0x00)
	.dwattr $C$DW$325, DW_AT_name("_CPGetHeightDataForReTransmission")
	.dwattr $C$DW$325, DW_AT_TI_call

        LCR       #_CPGetHeightDataForReTransmission ; [CPU_] |1942| 
        ; call occurs [#_CPGetHeightDataForReTransmission] ; [] |1942| 
        MOV       *-SP[4],AL            ; [CPU_] |1942| 
	.dwpsn	file "../source/CommProtocol.c",line 1947,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1947 | break;                                                                 
; 1949 | //!If the minor command is ADC RAW data, then packetize the ADC RAW dat
;     | a                                                                      
; 1950 | case DELFINO_WBC_ADC_RAW_DATA:                                         
; 1951 | case DELFINO_RBC_ADC_RAW_DATA:                                         
; 1952 | case DELFINO_PLT_ADC_RAW_DATA:                                         
;----------------------------------------------------------------------
        B         $C$L171,UNC           ; [CPU_] |1947| 
        ; branch occurs ; [] |1947| 
$C$L165:    
	.dwpsn	file "../source/CommProtocol.c",line 1954,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1954 | if(bNAKReceived == FALSE)                                              
; 1956 |     //!fn. to get Pulse height or ADC RAW data                         
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |1954| 
        B         $C$L166,NEQ           ; [CPU_] |1954| 
        ; branchcc occurs ; [] |1954| 
	.dwpsn	file "../source/CommProtocol.c",line 1957,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1957 | usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\            
; 1958 |         usnSubCmd, TRUE);                                              
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |1957| 
        MOVB      AH,#1                 ; [CPU_] |1957| 
$C$DW$326	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$326, DW_AT_low_pc(0x00)
	.dwattr $C$DW$326, DW_AT_name("_CPGetRawADCOrHeightData")
	.dwattr $C$DW$326, DW_AT_TI_call

        LCR       #_CPGetRawADCOrHeightData ; [CPU_] |1957| 
        ; call occurs [#_CPGetRawADCOrHeightData] ; [] |1957| 
        MOV       *-SP[4],AL            ; [CPU_] |1957| 
	.dwpsn	file "../source/CommProtocol.c",line 1959,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1960 | else                                                                   
; 1962 |     //!fn. to get Pulse height or ADC RAW data                         
;----------------------------------------------------------------------
        B         $C$L171,UNC           ; [CPU_] |1959| 
        ; branch occurs ; [] |1959| 
$C$L166:    
	.dwpsn	file "../source/CommProtocol.c",line 1963,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1963 | usnDataLength = CPReTransmistADCRawData(stPtrParsedPacket,\            
; 1964 |         usnSubCmd);                                                    
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |1963| 
$C$DW$327	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$327, DW_AT_low_pc(0x00)
	.dwattr $C$DW$327, DW_AT_name("_CPReTransmistADCRawData")
	.dwattr $C$DW$327, DW_AT_TI_call

        LCR       #_CPReTransmistADCRawData ; [CPU_] |1963| 
        ; call occurs [#_CPReTransmistADCRawData] ; [] |1963| 
        MOV       *-SP[4],AL            ; [CPU_] |1963| 
	.dwpsn	file "../source/CommProtocol.c",line 1967,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1967 | break;                                                                 
; 1969 | case DELFINO_ERROR_MINOR_CMD:                                          
;----------------------------------------------------------------------
        B         $C$L171,UNC           ; [CPU_] |1967| 
        ; branch occurs ; [] |1967| 
$C$L167:    
	.dwpsn	file "../source/CommProtocol.c",line 1970,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1970 | usnDataLength = CPGetErrorData(stPtrParsedPacket);                     
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |1970| 
$C$DW$328	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$328, DW_AT_low_pc(0x00)
	.dwattr $C$DW$328, DW_AT_name("_CPGetErrorData")
	.dwattr $C$DW$328, DW_AT_TI_call

        LCR       #_CPGetErrorData      ; [CPU_] |1970| 
        ; call occurs [#_CPGetErrorData] ; [] |1970| 
        MOV       *-SP[4],AL            ; [CPU_] |1970| 
	.dwpsn	file "../source/CommProtocol.c",line 1971,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1971 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1971| 
	.dwpsn	file "../source/CommProtocol.c",line 1972,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1972 | break;                                                                 
; 1974 |     case DELFINO_ASPIRATION_COMPLETED:                                 
; 1975 |     case DELFINO_NEEDLE_READY_FOR_DISPENSING_CMD:                      
; 1976 |     case DELFINO_DISPENSE_COMPLETED_CMD:                               
; 1977 |     case DELFINO_READY_FOR_ASPIRATION://SAM_ASP_READY                  
; 1978 |     case DELFINO_SEQUENCE_COMPLETION:                                  
; 1979 |     case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:                        
; 1980 |     default:                                                           
;----------------------------------------------------------------------
        B         $C$L171,UNC           ; [CPU_] |1972| 
        ; branch occurs ; [] |1972| 
$C$L168:    
	.dwpsn	file "../source/CommProtocol.c",line 1982,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1982 | usnDataLength = 0;                                                     
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |1982| 
	.dwpsn	file "../source/CommProtocol.c",line 1983,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1983 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |1983| 
	.dwpsn	file "../source/CommProtocol.c",line 1985,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1985 | break;                                                                 
; 1986 | }//end of switch case                                                  
; 1988 | //!Return the data length                                              
;----------------------------------------------------------------------
        B         $C$L171,UNC           ; [CPU_] |1985| 
        ; branch occurs ; [] |1985| 
$C$L169:    
	.dwpsn	file "../source/CommProtocol.c",line 1919,column 5,is_stmt,isa 0
        SUB       AL,#8193              ; [CPU_] |1919| 
        MOVZ      AR6,*-SP[5]           ; [CPU_] |1919| 
        CMPB      AL,#13                ; [CPU_] |1919| 
        B         $C$L170,LOS           ; [CPU_] |1919| 
        ; branchcc occurs ; [] |1919| 
        MOVZ      AR7,AR6               ; [CPU_] |1919| 
        MOVL      XAR4,#8238            ; [CPU_U] |1919| 
        MOVL      ACC,XAR4              ; [CPU_] |1919| 
        CMPL      ACC,XAR7              ; [CPU_] |1919| 
        B         $C$L168,EQ            ; [CPU_] |1919| 
        ; branchcc occurs ; [] |1919| 
        MOVZ      AR6,AR6               ; [CPU_] |1919| 
        MOVL      XAR4,#9217            ; [CPU_U] |1919| 
        MOVL      ACC,XAR4              ; [CPU_] |1919| 
        CMPL      ACC,XAR6              ; [CPU_] |1919| 
        B         $C$L167,EQ            ; [CPU_] |1919| 
        ; branchcc occurs ; [] |1919| 
        B         $C$L168,UNC           ; [CPU_] |1919| 
        ; branch occurs ; [] |1919| 
$C$L170:    
        SUB       AR6,#8193             ; [CPU_] |1919| 
        MOV       ACC,AR6 << #1         ; [CPU_] |1919| 
        MOVZ      AR6,AL                ; [CPU_] |1919| 
        MOVL      XAR7,#$C$SW7          ; [CPU_U] |1919| 
        MOVL      ACC,XAR7              ; [CPU_] |1919| 
        ADDU      ACC,AR6               ; [CPU_] |1919| 
        MOVL      XAR7,ACC              ; [CPU_] |1919| 
        MOVL      XAR7,*XAR7            ; [CPU_] |1919| 
        LB        *XAR7                 ; [CPU_] |1919| 
        ; branch occurs ; [] |1919| 
	.sect	".switch:_CPWriteCHDataToPacket"
	.clink
$C$SW7:	.long	$C$L163	; 8193
	.long	$C$L163	; 8194
	.long	$C$L163	; 8195
	.long	$C$L162	; 8196
	.long	$C$L168	; 0
	.long	$C$L165	; 8198
	.long	$C$L165	; 8199
	.long	$C$L165	; 8200
	.long	$C$L168	; 8201
	.long	$C$L168	; 8202
	.long	$C$L168	; 8203
	.long	$C$L168	; 8204
	.long	$C$L168	; 0
	.long	$C$L168	; 8206
	.sect	".text:_CPWriteCHDataToPacket"
$C$L171:    
	.dwpsn	file "../source/CommProtocol.c",line 1989,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1989 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |1989| 
	.dwpsn	file "../source/CommProtocol.c",line 1990,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$329	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$329, DW_AT_low_pc(0x00)
	.dwattr $C$DW$329, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$316, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$316, DW_AT_TI_end_line(0x7c6)
	.dwattr $C$DW$316, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$316

	.sect	".text:_CPWriteQHDataToPacket"
	.clink
	.global	_CPWriteQHDataToPacket

$C$DW$330	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$330, DW_AT_name("CPWriteQHDataToPacket")
	.dwattr $C$DW$330, DW_AT_low_pc(_CPWriteQHDataToPacket)
	.dwattr $C$DW$330, DW_AT_high_pc(0x00)
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_CPWriteQHDataToPacket")
	.dwattr $C$DW$330, DW_AT_external
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$330, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$330, DW_AT_TI_begin_line(0x7d3)
	.dwattr $C$DW$330, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$330, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommProtocol.c",line 2005,column 1,is_stmt,address _CPWriteQHDataToPacket,isa 0

	.dwfde $C$DW$CIE, _CPWriteQHDataToPacket
$C$DW$331	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$331, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$331, DW_AT_location[DW_OP_reg12]

$C$DW$332	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$332, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$332, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2003 | uint16_t CPWriteQHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket,\
; 2004 | bool_t bNAKReceived)                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWriteQHDataToPacket        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CPWriteQHDataToPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$333	.dwtag  DW_TAG_variable
	.dwattr $C$DW$333, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$333, DW_AT_location[DW_OP_breg20 -2]

$C$DW$334	.dwtag  DW_TAG_variable
	.dwattr $C$DW$334, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$334, DW_AT_location[DW_OP_breg20 -3]

$C$DW$335	.dwtag  DW_TAG_variable
	.dwattr $C$DW$335, DW_AT_name("usnDataLength")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$335, DW_AT_location[DW_OP_breg20 -4]

$C$DW$336	.dwtag  DW_TAG_variable
	.dwattr $C$DW$336, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$336, DW_AT_location[DW_OP_breg20 -5]

        MOV       *-SP[3],AL            ; [CPU_] |2005| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2005| 
	.dwpsn	file "../source/CommProtocol.c",line 2006,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 2006 | uint16_t usnDataLength = 0;                                            
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |2006| 
	.dwpsn	file "../source/CommProtocol.c",line 2007,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 2007 | uint16_t usnSubCmd =0;                                                 
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |2007| 
	.dwpsn	file "../source/CommProtocol.c",line 2009,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2009 | usnSubCmd = stPtrParsedPacket->usnMinorCommand;                        
; 2011 | //!Depending upon the minor command frame the data packet              
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[5]          ; [CPU_] |2009| 
        MOV       *-SP[5],AL            ; [CPU_] |2009| 
	.dwpsn	file "../source/CommProtocol.c",line 2012,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2012 | switch(usnSubCmd)                                                      
; 2014 |     //!If the minor command is WBC, RBC, PLT count, then packetize the
;     | results                                                                
; 2015 |             case DELFINO_WBC_RBC_PLT_HGB_COUNT:                        
;----------------------------------------------------------------------
        B         $C$L179,UNC           ; [CPU_] |2012| 
        ; branch occurs ; [] |2012| 
$C$L172:    
	.dwpsn	file "../source/CommProtocol.c",line 2017,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2017 | usnDataLength = CPGetCountData(stPtrParsedPacket);                     
;----------------------------------------------------------------------
$C$DW$337	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$337, DW_AT_low_pc(0x00)
	.dwattr $C$DW$337, DW_AT_name("_CPGetCountData")
	.dwattr $C$DW$337, DW_AT_TI_call

        LCR       #_CPGetCountData      ; [CPU_] |2017| 
        ; call occurs [#_CPGetCountData] ; [] |2017| 
        MOV       *-SP[4],AL            ; [CPU_] |2017| 
	.dwpsn	file "../source/CommProtocol.c",line 2019,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2019 | break;                                                                 
; 2021 | //!If the minor command is pulse height, then packetize the pulse heigh
;     | t data                                                                 
; 2022 | case DELFINO_WBC_PULSE_HEIGHT_DATA:                                    
; 2023 | case DELFINO_RBC_PULSE_HEIGHT_DATA:                                    
; 2024 | case DELFINO_PLT_PULSE_HEIGHT_DATA:                                    
;----------------------------------------------------------------------
        B         $C$L181,UNC           ; [CPU_] |2019| 
        ; branch occurs ; [] |2019| 
$C$L173:    
	.dwpsn	file "../source/CommProtocol.c",line 2026,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2026 | if(bNAKReceived == FALSE)                                              
; 2028 |     //!fn. to get Pulse height or ADC RAW data                         
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |2026| 
        B         $C$L174,NEQ           ; [CPU_] |2026| 
        ; branchcc occurs ; [] |2026| 
	.dwpsn	file "../source/CommProtocol.c",line 2029,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2029 | usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\            
; 2030 |         usnSubCmd, FALSE);                                             
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |2029| 
        MOVB      AH,#0                 ; [CPU_] |2029| 
$C$DW$338	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$338, DW_AT_low_pc(0x00)
	.dwattr $C$DW$338, DW_AT_name("_CPGetRawADCOrHeightData")
	.dwattr $C$DW$338, DW_AT_TI_call

        LCR       #_CPGetRawADCOrHeightData ; [CPU_] |2029| 
        ; call occurs [#_CPGetRawADCOrHeightData] ; [] |2029| 
        MOV       *-SP[4],AL            ; [CPU_] |2029| 
	.dwpsn	file "../source/CommProtocol.c",line 2031,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2032 | else                                                                   
; 2034 |     //!fn. to get Pulse height or ADC RAW data                         
;----------------------------------------------------------------------
        B         $C$L181,UNC           ; [CPU_] |2031| 
        ; branch occurs ; [] |2031| 
$C$L174:    
	.dwpsn	file "../source/CommProtocol.c",line 2035,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2035 | usnDataLength = \                                                      
; 2036 |         CPGetHeightDataForReTransmission(stPtrParsedPacket,\           
; 2037 |                 usnSubCmd);                                            
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |2035| 
$C$DW$339	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$339, DW_AT_low_pc(0x00)
	.dwattr $C$DW$339, DW_AT_name("_CPGetHeightDataForReTransmission")
	.dwattr $C$DW$339, DW_AT_TI_call

        LCR       #_CPGetHeightDataForReTransmission ; [CPU_] |2035| 
        ; call occurs [#_CPGetHeightDataForReTransmission] ; [] |2035| 
        MOV       *-SP[4],AL            ; [CPU_] |2035| 
	.dwpsn	file "../source/CommProtocol.c",line 2040,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2040 | break;                                                                 
; 2042 | //!If the minor command is ADC RAW data, then packetize the ADC RAW dat
;     | a                                                                      
; 2043 | case DELFINO_WBC_ADC_RAW_DATA:                                         
; 2044 | case DELFINO_RBC_ADC_RAW_DATA:                                         
; 2045 | case DELFINO_PLT_ADC_RAW_DATA:                                         
;----------------------------------------------------------------------
        B         $C$L181,UNC           ; [CPU_] |2040| 
        ; branch occurs ; [] |2040| 
$C$L175:    
	.dwpsn	file "../source/CommProtocol.c",line 2047,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2047 | if(bNAKReceived == FALSE)                                              
; 2049 |     //!fn. to get Pulse height or ADC RAW data                         
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |2047| 
        B         $C$L176,NEQ           ; [CPU_] |2047| 
        ; branchcc occurs ; [] |2047| 
	.dwpsn	file "../source/CommProtocol.c",line 2050,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2050 | usnDataLength = CPGetRawADCOrHeightData(stPtrParsedPacket,\            
; 2051 |         usnSubCmd, TRUE);                                              
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |2050| 
        MOVB      AH,#1                 ; [CPU_] |2050| 
$C$DW$340	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$340, DW_AT_low_pc(0x00)
	.dwattr $C$DW$340, DW_AT_name("_CPGetRawADCOrHeightData")
	.dwattr $C$DW$340, DW_AT_TI_call

        LCR       #_CPGetRawADCOrHeightData ; [CPU_] |2050| 
        ; call occurs [#_CPGetRawADCOrHeightData] ; [] |2050| 
        MOV       *-SP[4],AL            ; [CPU_] |2050| 
	.dwpsn	file "../source/CommProtocol.c",line 2052,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2053 | else                                                                   
; 2055 |     //!fn. to get Pulse height or ADC RAW data                         
;----------------------------------------------------------------------
        B         $C$L181,UNC           ; [CPU_] |2052| 
        ; branch occurs ; [] |2052| 
$C$L176:    
	.dwpsn	file "../source/CommProtocol.c",line 2056,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2056 | usnDataLength = CPReTransmistADCRawData(stPtrParsedPacket,\            
; 2057 |         usnSubCmd);                                                    
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |2056| 
$C$DW$341	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$341, DW_AT_low_pc(0x00)
	.dwattr $C$DW$341, DW_AT_name("_CPReTransmistADCRawData")
	.dwattr $C$DW$341, DW_AT_TI_call

        LCR       #_CPReTransmistADCRawData ; [CPU_] |2056| 
        ; call occurs [#_CPReTransmistADCRawData] ; [] |2056| 
        MOV       *-SP[4],AL            ; [CPU_] |2056| 
	.dwpsn	file "../source/CommProtocol.c",line 2060,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2060 | break;                                                                 
; 2062 | case DELFINO_ERROR_MINOR_CMD:                                          
;----------------------------------------------------------------------
        B         $C$L181,UNC           ; [CPU_] |2060| 
        ; branch occurs ; [] |2060| 
$C$L177:    
	.dwpsn	file "../source/CommProtocol.c",line 2063,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2063 | usnDataLength = CPGetErrorData(stPtrParsedPacket);                     
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |2063| 
$C$DW$342	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$342, DW_AT_low_pc(0x00)
	.dwattr $C$DW$342, DW_AT_name("_CPGetErrorData")
	.dwattr $C$DW$342, DW_AT_TI_call

        LCR       #_CPGetErrorData      ; [CPU_] |2063| 
        ; call occurs [#_CPGetErrorData] ; [] |2063| 
        MOV       *-SP[4],AL            ; [CPU_] |2063| 
	.dwpsn	file "../source/CommProtocol.c",line 2064,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2064 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |2064| 
	.dwpsn	file "../source/CommProtocol.c",line 2065,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2065 | break;                                                                 
; 2067 |     case DELFINO_ASPIRATION_COMPLETED:                                 
; 2068 |     case DELFINO_NEEDLE_READY_FOR_DISPENSING_CMD:                      
; 2069 |     case DELFINO_DISPENSE_COMPLETED_CMD:                               
; 2070 |     case DELFINO_READY_FOR_ASPIRATION:                                 
; 2071 |     case DELFINO_SEQUENCE_COMPLETION:                                  
; 2072 |     case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:                        
; 2073 |     default:                                                           
;----------------------------------------------------------------------
        B         $C$L181,UNC           ; [CPU_] |2065| 
        ; branch occurs ; [] |2065| 
$C$L178:    
	.dwpsn	file "../source/CommProtocol.c",line 2075,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2075 | usnDataLength = 0;                                                     
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |2075| 
	.dwpsn	file "../source/CommProtocol.c",line 2076,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2076 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |2076| 
	.dwpsn	file "../source/CommProtocol.c",line 2078,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2078 | break;                                                                 
; 2079 | }//end of switch case                                                  
; 2081 | //!Return the data length                                              
;----------------------------------------------------------------------
        B         $C$L181,UNC           ; [CPU_] |2078| 
        ; branch occurs ; [] |2078| 
$C$L179:    
	.dwpsn	file "../source/CommProtocol.c",line 2012,column 5,is_stmt,isa 0
        SUB       AL,#8193              ; [CPU_] |2012| 
        MOVZ      AR6,*-SP[5]           ; [CPU_] |2012| 
        CMPB      AL,#13                ; [CPU_] |2012| 
        B         $C$L180,LOS           ; [CPU_] |2012| 
        ; branchcc occurs ; [] |2012| 
        MOVZ      AR7,AR6               ; [CPU_] |2012| 
        MOVL      XAR4,#8238            ; [CPU_U] |2012| 
        MOVL      ACC,XAR4              ; [CPU_] |2012| 
        CMPL      ACC,XAR7              ; [CPU_] |2012| 
        B         $C$L178,EQ            ; [CPU_] |2012| 
        ; branchcc occurs ; [] |2012| 
        MOVZ      AR6,AR6               ; [CPU_] |2012| 
        MOVL      XAR4,#9217            ; [CPU_U] |2012| 
        MOVL      ACC,XAR4              ; [CPU_] |2012| 
        CMPL      ACC,XAR6              ; [CPU_] |2012| 
        B         $C$L177,EQ            ; [CPU_] |2012| 
        ; branchcc occurs ; [] |2012| 
        B         $C$L178,UNC           ; [CPU_] |2012| 
        ; branch occurs ; [] |2012| 
$C$L180:    
        SUB       AR6,#8193             ; [CPU_] |2012| 
        MOV       ACC,AR6 << #1         ; [CPU_] |2012| 
        MOVZ      AR6,AL                ; [CPU_] |2012| 
        MOVL      XAR7,#$C$SW9          ; [CPU_U] |2012| 
        MOVL      ACC,XAR7              ; [CPU_] |2012| 
        ADDU      ACC,AR6               ; [CPU_] |2012| 
        MOVL      XAR7,ACC              ; [CPU_] |2012| 
        MOVL      XAR7,*XAR7            ; [CPU_] |2012| 
        LB        *XAR7                 ; [CPU_] |2012| 
        ; branch occurs ; [] |2012| 
	.sect	".switch:_CPWriteQHDataToPacket"
	.clink
$C$SW9:	.long	$C$L173	; 8193
	.long	$C$L173	; 8194
	.long	$C$L173	; 8195
	.long	$C$L172	; 8196
	.long	$C$L178	; 0
	.long	$C$L175	; 8198
	.long	$C$L175	; 8199
	.long	$C$L175	; 8200
	.long	$C$L178	; 8201
	.long	$C$L178	; 8202
	.long	$C$L178	; 8203
	.long	$C$L178	; 8204
	.long	$C$L178	; 0
	.long	$C$L178	; 8206
	.sect	".text:_CPWriteQHDataToPacket"
$C$L181:    
	.dwpsn	file "../source/CommProtocol.c",line 2082,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2082 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |2082| 
	.dwpsn	file "../source/CommProtocol.c",line 2083,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$343	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$343, DW_AT_low_pc(0x00)
	.dwattr $C$DW$343, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$330, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$330, DW_AT_TI_end_line(0x823)
	.dwattr $C$DW$330, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$330

	.sect	".text:_CPWriteSeqMonitoringDataToPacket"
	.clink
	.global	_CPWriteSeqMonitoringDataToPacket

$C$DW$344	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$344, DW_AT_name("CPWriteSeqMonitoringDataToPacket")
	.dwattr $C$DW$344, DW_AT_low_pc(_CPWriteSeqMonitoringDataToPacket)
	.dwattr $C$DW$344, DW_AT_high_pc(0x00)
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_CPWriteSeqMonitoringDataToPacket")
	.dwattr $C$DW$344, DW_AT_external
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$344, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$344, DW_AT_TI_begin_line(0x82d)
	.dwattr $C$DW$344, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$344, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommProtocol.c",line 2095,column 1,is_stmt,address _CPWriteSeqMonitoringDataToPacket,isa 0

	.dwfde $C$DW$CIE, _CPWriteSeqMonitoringDataToPacket
$C$DW$345	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$345, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$345, DW_AT_location[DW_OP_reg12]

$C$DW$346	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$346, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$346, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2093 | uint16_t CPWriteSeqMonitoringDataToPacket(ST_SPI_PACKET_FRAME *stPtrPar
;     | sedPacket,\                                                            
; 2094 | bool_t bNAKReceived)                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWriteSeqMonitoringDataToPacket FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CPWriteSeqMonitoringDataToPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$347	.dwtag  DW_TAG_variable
	.dwattr $C$DW$347, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$347, DW_AT_location[DW_OP_breg20 -2]

$C$DW$348	.dwtag  DW_TAG_variable
	.dwattr $C$DW$348, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$348, DW_AT_location[DW_OP_breg20 -3]

$C$DW$349	.dwtag  DW_TAG_variable
	.dwattr $C$DW$349, DW_AT_name("usnDataLength")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$349, DW_AT_location[DW_OP_breg20 -4]

$C$DW$350	.dwtag  DW_TAG_variable
	.dwattr $C$DW$350, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$350, DW_AT_location[DW_OP_breg20 -5]

        MOV       *-SP[3],AL            ; [CPU_] |2095| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2095| 
	.dwpsn	file "../source/CommProtocol.c",line 2096,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 2096 | uint16_t usnDataLength = 0;                                            
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |2096| 
	.dwpsn	file "../source/CommProtocol.c",line 2097,column 24,is_stmt,isa 0
;----------------------------------------------------------------------
; 2097 | uint16_t usnSubCmd =0;                                                 
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |2097| 
	.dwpsn	file "../source/CommProtocol.c",line 2099,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2099 | usnSubCmd = stPtrParsedPacket->usnMinorCommand;                        
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[5]          ; [CPU_] |2099| 
        MOV       *-SP[5],AL            ; [CPU_] |2099| 
	.dwpsn	file "../source/CommProtocol.c",line 2100,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2100 | switch(usnSubCmd)                                                      
; 2102 |     case DELFINO_STATE_SEQUENCE_STARTED:                               
; 2103 |     case DELFINO_STATE_READY_FOR_DISPENSING:                           
; 2104 |     case DELFINO_STATE_DISPENSE_COMPLETED:                             
; 2105 |     case DELFINO_STATE_READY_FOR_ASPIRATION:                           
; 2106 |     case DELFINO_STATE_ASPIRATION_COMPLETED:                           
; 2107 |     case DELFINO_STATE_WBC_DISPENSATION_COMPLETED:                     
; 2108 |     case DELFINO_STATE_RBC_DISPENSATION_COMPLETED:                     
; 2109 |     case DELFINO_STATE_WBC_RBC_BUBBLING_COMPLETED:                     
; 2110 |     case DELFINO_STATE_COUNTING_STARTED:                               
; 2111 |     case DELFINO_STATE_COUNTING_COMPLETED:                             
; 2112 |     case DELFINO_STATE_BACKFLUSH_COMPLETED:                            
; 2113 |     case DELFINO_STATE_BATH_FILLING_COMPLETED:                         
; 2114 |         //!Length of the packet is 1 for sequence monitoring.          
;----------------------------------------------------------------------
        B         $C$L183,UNC           ; [CPU_] |2100| 
        ; branch occurs ; [] |2100| 
$C$L182:    
	.dwpsn	file "../source/CommProtocol.c",line 2115,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2115 | usnDataLength =1;                                                      
; 2116 | //!Packet data is sequence state time                                  
;----------------------------------------------------------------------
        MOVB      *-SP[4],#1,UNC        ; [CPU_] |2115| 
	.dwpsn	file "../source/CommProtocol.c",line 2117,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2117 | m_stPacketForTx.arrusnData[0]= g_unSequenceStateTime;                  
; 2118 | //!Reset the sequence state to Zero so that the next state period is   
; 2119 | //! counted from the previous state                                    
;----------------------------------------------------------------------
        MOVW      DP,#_g_unSequenceStateTime ; [CPU_U] 
        MOV       AL,@_g_unSequenceStateTime ; [CPU_] |2117| 
        MOVW      DP,#_m_stPacketForTx+6 ; [CPU_U] 
        MOV       @_m_stPacketForTx+6,AL ; [CPU_] |2117| 
	.dwpsn	file "../source/CommProtocol.c",line 2120,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2120 | g_unSequenceStateTime = 0;                                             
; 2121 | //!Since it is single packet, packet count is zero                     
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |2120| 
        MOVW      DP,#_g_unSequenceStateTime ; [CPU_U] 
        MOVL      @_g_unSequenceStateTime,ACC ; [CPU_] |2120| 
	.dwpsn	file "../source/CommProtocol.c",line 2122,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2122 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |2122| 
	.dwpsn	file "../source/CommProtocol.c",line 2123,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2123 | break;                                                                 
; 2125 | default:                                                               
; 2126 | break;                                                                 
; 2128 | }//end of switch case                                                  
; 2129 | //!Return the data length                                              
;----------------------------------------------------------------------
        B         $C$L184,UNC           ; [CPU_] |2123| 
        ; branch occurs ; [] |2123| 
$C$L183:    
	.dwpsn	file "../source/CommProtocol.c",line 2100,column 5,is_stmt,isa 0
        MOV       AH,*-SP[5]            ; [CPU_] |2100| 
        SUB       AL,#8193              ; [CPU_] |2100| 
        CMPB      AL,#11                ; [CPU_] |2100| 
        B         $C$L184,HI            ; [CPU_] |2100| 
        ; branchcc occurs ; [] |2100| 
        MOV       AL,AH                 ; [CPU_] |2100| 
        SUB       AL,#8193              ; [CPU_] |2100| 
        MOV       ACC,AL << #1          ; [CPU_] |2100| 
        MOVZ      AR6,AL                ; [CPU_] |2100| 
        MOVL      XAR7,#$C$SW11         ; [CPU_U] |2100| 
        MOVL      ACC,XAR7              ; [CPU_] |2100| 
        ADDU      ACC,AR6               ; [CPU_] |2100| 
        MOVL      XAR7,ACC              ; [CPU_] |2100| 
        MOVL      XAR7,*XAR7            ; [CPU_] |2100| 
        LB        *XAR7                 ; [CPU_] |2100| 
        ; branch occurs ; [] |2100| 
	.sect	".switch:_CPWriteSeqMonitoringDataToPacket"
	.clink
$C$SW11:	.long	$C$L182	; 8193
	.long	$C$L182	; 8194
	.long	$C$L182	; 8195
	.long	$C$L182	; 8196
	.long	$C$L182	; 8197
	.long	$C$L182	; 8198
	.long	$C$L182	; 8199
	.long	$C$L182	; 8200
	.long	$C$L182	; 8201
	.long	$C$L182	; 8202
	.long	$C$L182	; 8203
	.long	$C$L182	; 8204
	.sect	".text:_CPWriteSeqMonitoringDataToPacket"
$C$L184:    
	.dwpsn	file "../source/CommProtocol.c",line 2130,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2130 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |2130| 
	.dwpsn	file "../source/CommProtocol.c",line 2131,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$351	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$351, DW_AT_low_pc(0x00)
	.dwattr $C$DW$351, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$344, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$344, DW_AT_TI_end_line(0x853)
	.dwattr $C$DW$344, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$344

	.sect	".text:_CPWriteAbortProcessDataToPacket"
	.clink
	.global	_CPWriteAbortProcessDataToPacket

$C$DW$352	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$352, DW_AT_name("CPWriteAbortProcessDataToPacket")
	.dwattr $C$DW$352, DW_AT_low_pc(_CPWriteAbortProcessDataToPacket)
	.dwattr $C$DW$352, DW_AT_high_pc(0x00)
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_CPWriteAbortProcessDataToPacket")
	.dwattr $C$DW$352, DW_AT_external
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$352, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$352, DW_AT_TI_begin_line(0x85d)
	.dwattr $C$DW$352, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$352, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommProtocol.c",line 2143,column 1,is_stmt,address _CPWriteAbortProcessDataToPacket,isa 0

	.dwfde $C$DW$CIE, _CPWriteAbortProcessDataToPacket
$C$DW$353	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$353, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$353, DW_AT_location[DW_OP_reg12]

$C$DW$354	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$354, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$354, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2141 | uint16_t CPWriteAbortProcessDataToPacket(ST_SPI_PACKET_FRAME *stPtrPars
;     | edPacket,\                                                             
; 2142 | bool_t bNAKReceived)                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWriteAbortProcessDataToPacket FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CPWriteAbortProcessDataToPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$355	.dwtag  DW_TAG_variable
	.dwattr $C$DW$355, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$355, DW_AT_location[DW_OP_breg20 -2]

$C$DW$356	.dwtag  DW_TAG_variable
	.dwattr $C$DW$356, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$356, DW_AT_location[DW_OP_breg20 -3]

$C$DW$357	.dwtag  DW_TAG_variable
	.dwattr $C$DW$357, DW_AT_name("usnDataLength")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$357, DW_AT_location[DW_OP_breg20 -4]

$C$DW$358	.dwtag  DW_TAG_variable
	.dwattr $C$DW$358, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$358, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 2144 | //!Initialize data length of the packet to zero                        
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |2143| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2143| 
	.dwpsn	file "../source/CommProtocol.c",line 2145,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 2145 | uint16_t usnDataLength = ZERO;                                         
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |2145| 
	.dwpsn	file "../source/CommProtocol.c",line 2146,column 24,is_stmt,isa 0
;----------------------------------------------------------------------
; 2146 | uint16_t usnSubCmd = ZERO;                                             
; 2148 | //!Get the minor command from the structure                            
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |2146| 
	.dwpsn	file "../source/CommProtocol.c",line 2149,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2149 | usnSubCmd = stPtrParsedPacket->usnMinorCommand;                        
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[5]          ; [CPU_] |2149| 
        MOV       *-SP[5],AL            ; [CPU_] |2149| 
	.dwpsn	file "../source/CommProtocol.c",line 2172,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2172 | if(usnSubCmd == DELFINO_ABORT_PROCESS_SUB_CMD)    //QA_C               
; 2175 |     //!If the minor command is to abort the process then perform the be
;     | low operations                                                         
; 2177 |         //!Set the data length to 8 and update the errors              
;----------------------------------------------------------------------
        MOV       AL,#11195             ; [CPU_] |2172| 
        CMP       AL,*-SP[5]            ; [CPU_] |2172| 
        B         $C$L185,NEQ           ; [CPU_] |2172| 
        ; branchcc occurs ; [] |2172| 
	.dwpsn	file "../source/CommProtocol.c",line 2178,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2178 | usnDataLength =8;                                                      
;----------------------------------------------------------------------
        MOVB      *-SP[4],#8,UNC        ; [CPU_] |2178| 
	.dwpsn	file "../source/CommProtocol.c",line 2179,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2179 | m_stPacketForTx.arrusnData[0]= 0;                                      
;----------------------------------------------------------------------
        MOVW      DP,#_m_stPacketForTx+6 ; [CPU_U] 
        MOV       @_m_stPacketForTx+6,#0 ; [CPU_] |2179| 
	.dwpsn	file "../source/CommProtocol.c",line 2180,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2180 | m_stPacketForTx.arrusnData[1]= 0;                                      
;----------------------------------------------------------------------
        MOV       @_m_stPacketForTx+7,#0 ; [CPU_] |2180| 
	.dwpsn	file "../source/CommProtocol.c",line 2181,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2181 | m_stPacketForTx.arrusnData[2]= 0;                                      
;----------------------------------------------------------------------
        MOV       @_m_stPacketForTx+8,#0 ; [CPU_] |2181| 
	.dwpsn	file "../source/CommProtocol.c",line 2182,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2182 | m_stPacketForTx.arrusnData[3]= 0;                                      
;----------------------------------------------------------------------
        MOV       @_m_stPacketForTx+9,#0 ; [CPU_] |2182| 
	.dwpsn	file "../source/CommProtocol.c",line 2183,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2183 | m_stPacketForTx.arrusnData[4]= 0;                                      
;----------------------------------------------------------------------
        MOV       @_m_stPacketForTx+10,#0 ; [CPU_] |2183| 
	.dwpsn	file "../source/CommProtocol.c",line 2184,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2184 | m_stPacketForTx.arrusnData[5]= utErrorInfoCheck.stErrorInfo.ulnDiluentE
;     | mpty;                                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       AL,@_utErrorInfoCheck+2,#0x0020 ; [CPU_] |2184| 
        MOVW      DP,#_m_stPacketForTx+11 ; [CPU_U] 
        LSR       AL,5                  ; [CPU_] |2184| 
        MOV       @_m_stPacketForTx+11,AL ; [CPU_] |2184| 
	.dwpsn	file "../source/CommProtocol.c",line 2185,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2185 | m_stPacketForTx.arrusnData[6]= utErrorInfoCheck.stErrorInfo.ulnLyseEmpt
;     | y;                                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       AL,@_utErrorInfoCheck+2,#0x0040 ; [CPU_] |2185| 
        MOVW      DP,#_m_stPacketForTx+12 ; [CPU_U] 
        LSR       AL,6                  ; [CPU_] |2185| 
        MOV       @_m_stPacketForTx+12,AL ; [CPU_] |2185| 
	.dwpsn	file "../source/CommProtocol.c",line 2186,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2186 | m_stPacketForTx.arrusnData[7]= utErrorInfoCheck.stErrorInfo.ulnRinseEmp
;     | ty;                                                                    
; 2187 | //!Since it is single packet, packet count is zero                     
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       AL,@_utErrorInfoCheck+2,#0x0080 ; [CPU_] |2186| 
        MOVW      DP,#_m_stPacketForTx+13 ; [CPU_U] 
        LSR       AL,7                  ; [CPU_] |2186| 
        MOV       @_m_stPacketForTx+13,AL ; [CPU_] |2186| 
	.dwpsn	file "../source/CommProtocol.c",line 2188,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2188 | m_usnPacketCount = ZERO;                                               
; 2191 | //!Return the data length                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |2188| 
$C$L185:    
	.dwpsn	file "../source/CommProtocol.c",line 2192,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2192 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |2192| 
	.dwpsn	file "../source/CommProtocol.c",line 2193,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$359	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$359, DW_AT_low_pc(0x00)
	.dwattr $C$DW$359, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$352, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$352, DW_AT_TI_end_line(0x891)
	.dwattr $C$DW$352, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$352

	.sect	".text:_CPGetErrorData"
	.clink
	.global	_CPGetErrorData

$C$DW$360	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$360, DW_AT_name("CPGetErrorData")
	.dwattr $C$DW$360, DW_AT_low_pc(_CPGetErrorData)
	.dwattr $C$DW$360, DW_AT_high_pc(0x00)
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_CPGetErrorData")
	.dwattr $C$DW$360, DW_AT_external
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$360, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$360, DW_AT_TI_begin_line(0x89b)
	.dwattr $C$DW$360, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$360, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommProtocol.c",line 2204,column 1,is_stmt,address _CPGetErrorData,isa 0

	.dwfde $C$DW$CIE, _CPGetErrorData
$C$DW$361	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$361, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$361, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 2203 | uint16_t CPGetErrorData(ST_SPI_PACKET_FRAME *stPtrParsedPacket)        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPGetErrorData               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CPGetErrorData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$362	.dwtag  DW_TAG_variable
	.dwattr $C$DW$362, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$362, DW_AT_location[DW_OP_breg20 -4]

$C$DW$363	.dwtag  DW_TAG_variable
	.dwattr $C$DW$363, DW_AT_name("usnDataLength")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$363, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 2205 | //!Atleast one data shall be sent in a packet , so set data length to 1
;     | ,                                                                      
;----------------------------------------------------------------------
        MOVL      *-SP[4],XAR4          ; [CPU_] |2204| 
	.dwpsn	file "../source/CommProtocol.c",line 2206,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 2206 | uint16_t usnDataLength = 0;                                            
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |2206| 
	.dwpsn	file "../source/CommProtocol.c",line 2207,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2207 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnSequenceAborted)            
; 2209 |     //!Set the Zeroth location as sequence aborted                     
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck ; [CPU_U] 
        MOV       AL,@_utErrorInfoCheck ; [CPU_] |2207| 
        ANDB      AL,#0x01              ; [CPU_] |2207| 
        CMPB      AL,#1                 ; [CPU_] |2207| 
        B         $C$L186,NEQ           ; [CPU_] |2207| 
        ; branchcc occurs ; [] |2207| 
	.dwpsn	file "../source/CommProtocol.c",line 2210,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2210 | m_stPacketForTx.arrusnData[usnDataLength] = eSEQUENCE_ABORTED;         
; 2211 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2210| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2210| 
        MOVB      *+XAR4[AR0],#2,UNC    ; [CPU_] |2210| 
	.dwpsn	file "../source/CommProtocol.c",line 2212,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2212 | usnDataLength++;                                                       
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2212| 
	.dwpsn	file "../source/CommProtocol.c",line 2213,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2213 | utErrorInfoCheck.stErrorInfo.ulnSequenceAborted = FALSE;               
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck,#0xfffe ; [CPU_] |2213| 
$C$L186:    
	.dwpsn	file "../source/CommProtocol.c",line 2217,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2217 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnSequenceArrested)           
; 2219 |     //!Set the Zeroth location as sequence aborted                     
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+3,#0x0008 ; [CPU_] |2217| 
        LSR       AL,3                  ; [CPU_] |2217| 
        CMPB      AL,#1                 ; [CPU_] |2217| 
        B         $C$L187,NEQ           ; [CPU_] |2217| 
        ; branchcc occurs ; [] |2217| 
	.dwpsn	file "../source/CommProtocol.c",line 2220,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2220 | m_stPacketForTx.arrusnData[usnDataLength] = eSEQUENCE_ARRESTED;        
; 2221 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2220| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2220| 
        MOVB      *+XAR4[AR0],#73,UNC   ; [CPU_] |2220| 
	.dwpsn	file "../source/CommProtocol.c",line 2222,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2222 | usnDataLength++;                                                       
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2222| 
	.dwpsn	file "../source/CommProtocol.c",line 2223,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2223 | utErrorInfoCheck.stErrorInfo.ulnSequenceArrested = FALSE;              
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+3,#0xfff7 ; [CPU_] |2223| 
$C$L187:    
	.dwpsn	file "../source/CommProtocol.c",line 2226,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2226 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnX_Motor)                    
; 2228 |     //!Set X-Motor Error                                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0002 ; [CPU_] |2226| 
        LSR       AL,1                  ; [CPU_] |2226| 
        CMPB      AL,#1                 ; [CPU_] |2226| 
        B         $C$L188,NEQ           ; [CPU_] |2226| 
        ; branchcc occurs ; [] |2226| 
	.dwpsn	file "../source/CommProtocol.c",line 2229,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2229 | m_stPacketForTx.arrusnData[usnDataLength]= eX_MOTOR;                   
; 2230 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2229| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2229| 
        MOVB      *+XAR4[AR0],#3,UNC    ; [CPU_] |2229| 
	.dwpsn	file "../source/CommProtocol.c",line 2231,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2231 | usnDataLength++;                                                       
; 2233 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2231| 
	.dwpsn	file "../source/CommProtocol.c",line 2234,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2234 | utErrorInfoCheck.stErrorInfo.ulnX_Motor = FALSE;                       
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck,#0xfffd ; [CPU_] |2234| 
$C$L188:    
	.dwpsn	file "../source/CommProtocol.c",line 2236,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2236 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnDiluentSyringe)             
; 2238 |     //!Set Diluent Syringe Error                                       
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0004 ; [CPU_] |2236| 
        LSR       AL,2                  ; [CPU_] |2236| 
        CMPB      AL,#1                 ; [CPU_] |2236| 
        B         $C$L189,NEQ           ; [CPU_] |2236| 
        ; branchcc occurs ; [] |2236| 
	.dwpsn	file "../source/CommProtocol.c",line 2239,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2239 | m_stPacketForTx.arrusnData[usnDataLength]= eDILUENT_SYRINGE;           
; 2240 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2239| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2239| 
        MOVB      *+XAR4[AR0],#4,UNC    ; [CPU_] |2239| 
	.dwpsn	file "../source/CommProtocol.c",line 2241,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2241 | usnDataLength++;                                                       
; 2243 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2241| 
$C$L189:    
	.dwpsn	file "../source/CommProtocol.c",line 2245,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2245 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnSampleSyringe)              
; 2247 |     //!Set Sample Syringe Error                                        
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0008 ; [CPU_] |2245| 
        LSR       AL,3                  ; [CPU_] |2245| 
        CMPB      AL,#1                 ; [CPU_] |2245| 
        B         $C$L190,NEQ           ; [CPU_] |2245| 
        ; branchcc occurs ; [] |2245| 
	.dwpsn	file "../source/CommProtocol.c",line 2248,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2248 | m_stPacketForTx.arrusnData[usnDataLength]= eSAMPLE_SYRINGE;            
; 2249 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2248| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2248| 
        MOVB      *+XAR4[AR0],#5,UNC    ; [CPU_] |2248| 
	.dwpsn	file "../source/CommProtocol.c",line 2250,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2250 | usnDataLength++;                                                       
; 2252 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2250| 
$C$L190:    
	.dwpsn	file "../source/CommProtocol.c",line 2254,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2254 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWasteSyringe)               
; 2256 |     //!Set Waste Syringe Error                                         
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0010 ; [CPU_] |2254| 
        LSR       AL,4                  ; [CPU_] |2254| 
        CMPB      AL,#1                 ; [CPU_] |2254| 
        B         $C$L191,NEQ           ; [CPU_] |2254| 
        ; branchcc occurs ; [] |2254| 
	.dwpsn	file "../source/CommProtocol.c",line 2257,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2257 | m_stPacketForTx.arrusnData[usnDataLength]= eWASTE_SYRINGE;             
; 2258 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2257| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2257| 
        MOVB      *+XAR4[AR0],#6,UNC    ; [CPU_] |2257| 
	.dwpsn	file "../source/CommProtocol.c",line 2259,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2259 | usnDataLength++;                                                       
; 2261 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2259| 
$C$L191:    
	.dwpsn	file "../source/CommProtocol.c",line 2263,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2263 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnY_Motor)                    
; 2265 |     //!Set Y-Motor Error                                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0020 ; [CPU_] |2263| 
        LSR       AL,5                  ; [CPU_] |2263| 
        CMPB      AL,#1                 ; [CPU_] |2263| 
        B         $C$L192,NEQ           ; [CPU_] |2263| 
        ; branchcc occurs ; [] |2263| 
	.dwpsn	file "../source/CommProtocol.c",line 2266,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2266 | m_stPacketForTx.arrusnData[usnDataLength]= eY_MOTOR;                   
; 2267 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2266| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2266| 
        MOVB      *+XAR4[AR0],#7,UNC    ; [CPU_] |2266| 
	.dwpsn	file "../source/CommProtocol.c",line 2268,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2268 | usnDataLength++;                                                       
; 2270 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2268| 
$C$L192:    
	.dwpsn	file "../source/CommProtocol.c",line 2272,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2272 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacWBCDraining)             
; 2274 |     //!Set error for WBC draining vacuum                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0040 ; [CPU_] |2272| 
        LSR       AL,6                  ; [CPU_] |2272| 
        CMPB      AL,#1                 ; [CPU_] |2272| 
        B         $C$L193,NEQ           ; [CPU_] |2272| 
        ; branchcc occurs ; [] |2272| 
	.dwpsn	file "../source/CommProtocol.c",line 2275,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2275 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_WBC_DRAINING
;     | ;                                                                      
; 2276 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2275| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2275| 
        MOVB      *+XAR4[AR0],#8,UNC    ; [CPU_] |2275| 
	.dwpsn	file "../source/CommProtocol.c",line 2277,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2277 | usnDataLength++;                                                       
; 2279 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2277| 
$C$L193:    
	.dwpsn	file "../source/CommProtocol.c",line 2281,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2281 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDraining)             
; 2283 |     //!Set error for RBC draining vacuum                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0080 ; [CPU_] |2281| 
        LSR       AL,7                  ; [CPU_] |2281| 
        CMPB      AL,#1                 ; [CPU_] |2281| 
        B         $C$L194,NEQ           ; [CPU_] |2281| 
        ; branchcc occurs ; [] |2281| 
	.dwpsn	file "../source/CommProtocol.c",line 2284,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2284 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING
;     | ;                                                                      
; 2285 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2284| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2284| 
        MOVB      *+XAR4[AR0],#9,UNC    ; [CPU_] |2284| 
	.dwpsn	file "../source/CommProtocol.c",line 2286,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2286 | usnDataLength++;                                                       
; 2288 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2286| 
$C$L194:    
	.dwpsn	file "../source/CommProtocol.c",line 2290,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2290 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingEnd)          
; 2292 |     //!Set error for RBC draining vacuum                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0100 ; [CPU_] |2290| 
        LSR       AL,8                  ; [CPU_] |2290| 
        CMPB      AL,#1                 ; [CPU_] |2290| 
        B         $C$L195,NEQ           ; [CPU_] |2290| 
        ; branchcc occurs ; [] |2290| 
	.dwpsn	file "../source/CommProtocol.c",line 2293,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2293 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING
;     | _COMPLETED;                                                            
; 2294 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2293| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2293| 
        MOVB      *+XAR4[AR0],#10,UNC   ; [CPU_] |2293| 
	.dwpsn	file "../source/CommProtocol.c",line 2295,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2295 | usnDataLength++;                                                       
; 2297 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2295| 
$C$L195:    
	.dwpsn	file "../source/CommProtocol.c",line 2299,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2299 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresWBCBubblingStart)       
; 2301 |     //!Set error for WBC bubbling pressure                             
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0200 ; [CPU_] |2299| 
        LSR       AL,9                  ; [CPU_] |2299| 
        CMPB      AL,#1                 ; [CPU_] |2299| 
        B         $C$L196,NEQ           ; [CPU_] |2299| 
        ; branchcc occurs ; [] |2299| 
	.dwpsn	file "../source/CommProtocol.c",line 2302,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2302 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_WBC_BUBBLI
;     | NG_STARTED;                                                            
; 2303 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2302| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2302| 
        MOVB      *+XAR4[AR0],#11,UNC   ; [CPU_] |2302| 
	.dwpsn	file "../source/CommProtocol.c",line 2304,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2304 | usnDataLength++;                                                       
; 2306 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2304| 
$C$L196:    
	.dwpsn	file "../source/CommProtocol.c",line 2308,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2308 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresWBCBubblingEnd)         
; 2310 |     //!Set error for WBC bubbling pressure                             
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0400 ; [CPU_] |2308| 
        LSR       AL,10                 ; [CPU_] |2308| 
        CMPB      AL,#1                 ; [CPU_] |2308| 
        B         $C$L197,NEQ           ; [CPU_] |2308| 
        ; branchcc occurs ; [] |2308| 
	.dwpsn	file "../source/CommProtocol.c",line 2311,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2311 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_WBC_BUBBLI
;     | NG_COMPLETED;                                                          
; 2312 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2311| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2311| 
        MOVB      *+XAR4[AR0],#12,UNC   ; [CPU_] |2311| 
	.dwpsn	file "../source/CommProtocol.c",line 2313,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2313 | usnDataLength++;                                                       
; 2315 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2313| 
$C$L197:    
	.dwpsn	file "../source/CommProtocol.c",line 2317,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2317 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresWBCBubblingStartLyse)   
; 2319 |     //!Set error for WBC bubbling after adding lyse pressure           
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x0800 ; [CPU_] |2317| 
        LSR       AL,11                 ; [CPU_] |2317| 
        CMPB      AL,#1                 ; [CPU_] |2317| 
        B         $C$L198,NEQ           ; [CPU_] |2317| 
        ; branchcc occurs ; [] |2317| 
	.dwpsn	file "../source/CommProtocol.c",line 2320,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2320 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_WBC_BUBBLI
;     | NG_STARTED_AFTER_LYSE;                                                 
; 2321 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2320| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2320| 
        MOVB      *+XAR4[AR0],#13,UNC   ; [CPU_] |2320| 
	.dwpsn	file "../source/CommProtocol.c",line 2322,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2322 | usnDataLength++;                                                       
; 2324 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2322| 
$C$L198:    
	.dwpsn	file "../source/CommProtocol.c",line 2326,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2326 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresRBCBubblingStartLyse)   
; 2328 |     //!Set error for RBC bubbling pressure                             
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x1000 ; [CPU_] |2326| 
        LSR       AL,12                 ; [CPU_] |2326| 
        CMPB      AL,#1                 ; [CPU_] |2326| 
        B         $C$L199,NEQ           ; [CPU_] |2326| 
        ; branchcc occurs ; [] |2326| 
	.dwpsn	file "../source/CommProtocol.c",line 2329,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2329 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_RBC_BUBBLI
;     | NG_STARTED_AFTER_LYSE;                                                 
; 2330 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2329| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2329| 
        MOVB      *+XAR4[AR0],#14,UNC   ; [CPU_] |2329| 
	.dwpsn	file "../source/CommProtocol.c",line 2331,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2331 | usnDataLength++;                                                       
; 2333 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2331| 
$C$L199:    
	.dwpsn	file "../source/CommProtocol.c",line 2335,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2335 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresRBCBubblingStartLyseEnd)
; 2337 |     //!Set error for RBC bubbling pressure                             
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x2000 ; [CPU_] |2335| 
        LSR       AL,13                 ; [CPU_] |2335| 
        CMPB      AL,#1                 ; [CPU_] |2335| 
        B         $C$L200,NEQ           ; [CPU_] |2335| 
        ; branchcc occurs ; [] |2335| 
	.dwpsn	file "../source/CommProtocol.c",line 2338,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2338 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_RBC_BUBBLI
;     | NG_COMPLETED_AFTER_LYSE;                                               
; 2339 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2338| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2338| 
        MOVB      *+XAR4[AR0],#15,UNC   ; [CPU_] |2338| 
	.dwpsn	file "../source/CommProtocol.c",line 2340,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2340 | usnDataLength++;                                                       
; 2342 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2340| 
$C$L200:    
	.dwpsn	file "../source/CommProtocol.c",line 2344,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2344 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacCountingStart)           
; 2346 |     //!Set error for counting start vacuum                             
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x4000 ; [CPU_] |2344| 
        LSR       AL,14                 ; [CPU_] |2344| 
        CMPB      AL,#1                 ; [CPU_] |2344| 
        B         $C$L201,NEQ           ; [CPU_] |2344| 
        ; branchcc occurs ; [] |2344| 
	.dwpsn	file "../source/CommProtocol.c",line 2347,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2347 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_COUNTING_STA
;     | RTED;                                                                  
; 2348 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2347| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2347| 
        MOVB      *+XAR4[AR0],#16,UNC   ; [CPU_] |2347| 
	.dwpsn	file "../source/CommProtocol.c",line 2349,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2349 | usnDataLength++;                                                       
; 2351 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2349| 
$C$L201:    
	.dwpsn	file "../source/CommProtocol.c",line 2353,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2353 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacCountingEnd)             
; 2355 |     //!Set error for counting completed vacuum                         
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck,#0x8000 ; [CPU_] |2353| 
        LSR       AL,15                 ; [CPU_] |2353| 
        CMPB      AL,#1                 ; [CPU_] |2353| 
        B         $C$L202,NEQ           ; [CPU_] |2353| 
        ; branchcc occurs ; [] |2353| 
	.dwpsn	file "../source/CommProtocol.c",line 2356,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2356 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_COUNTING_COM
;     | PLETED;                                                                
; 2357 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2356| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2356| 
        MOVB      *+XAR4[AR0],#17,UNC   ; [CPU_] |2356| 
	.dwpsn	file "../source/CommProtocol.c",line 2358,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2358 | usnDataLength++;                                                       
; 2360 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2358| 
$C$L202:    
	.dwpsn	file "../source/CommProtocol.c",line 2362,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2362 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacWBCDrainingAC)           
; 2364 |     //!Set error for WBC draining vacuum                               
;----------------------------------------------------------------------
        MOV       AL,@_utErrorInfoCheck+1 ; [CPU_] |2362| 
        ANDB      AL,#0x01              ; [CPU_] |2362| 
        CMPB      AL,#1                 ; [CPU_] |2362| 
        B         $C$L203,NEQ           ; [CPU_] |2362| 
        ; branchcc occurs ; [] |2362| 
	.dwpsn	file "../source/CommProtocol.c",line 2365,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2365 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_WBC_DRAINING
;     | _AC;                                                                   
; 2366 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2365| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2365| 
        MOVB      *+XAR4[AR0],#18,UNC   ; [CPU_] |2365| 
	.dwpsn	file "../source/CommProtocol.c",line 2367,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2367 | usnDataLength++;                                                       
; 2369 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2367| 
$C$L203:    
	.dwpsn	file "../source/CommProtocol.c",line 2371,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2371 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingAC)           
; 2373 |     //!Set error for RBC draining vacuum                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0002 ; [CPU_] |2371| 
        LSR       AL,1                  ; [CPU_] |2371| 
        CMPB      AL,#1                 ; [CPU_] |2371| 
        B         $C$L204,NEQ           ; [CPU_] |2371| 
        ; branchcc occurs ; [] |2371| 
	.dwpsn	file "../source/CommProtocol.c",line 2374,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2374 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING
;     | _AC;                                                                   
; 2375 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2374| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2374| 
        MOVB      *+XAR4[AR0],#19,UNC   ; [CPU_] |2374| 
	.dwpsn	file "../source/CommProtocol.c",line 2376,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2376 | usnDataLength++;                                                       
; 2378 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2376| 
$C$L204:    
	.dwpsn	file "../source/CommProtocol.c",line 2380,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2380 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingACEnd)        
; 2382 |     //!Set error for RBC draining vacuum                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0004 ; [CPU_] |2380| 
        LSR       AL,2                  ; [CPU_] |2380| 
        CMPB      AL,#1                 ; [CPU_] |2380| 
        B         $C$L205,NEQ           ; [CPU_] |2380| 
        ; branchcc occurs ; [] |2380| 
	.dwpsn	file "../source/CommProtocol.c",line 2383,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2383 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING
;     | _COMPLETED_AC;                                                         
; 2384 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2383| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2383| 
        MOVB      *+XAR4[AR0],#20,UNC   ; [CPU_] |2383| 
	.dwpsn	file "../source/CommProtocol.c",line 2385,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2385 | usnDataLength++;                                                       
; 2387 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2385| 
$C$L205:    
	.dwpsn	file "../source/CommProtocol.c",line 2389,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2389 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresBackflushStart)         
; 2391 |     //!Set error for backflush pressure                                
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0008 ; [CPU_] |2389| 
        LSR       AL,3                  ; [CPU_] |2389| 
        CMPB      AL,#1                 ; [CPU_] |2389| 
        B         $C$L206,NEQ           ; [CPU_] |2389| 
        ; branchcc occurs ; [] |2389| 
	.dwpsn	file "../source/CommProtocol.c",line 2392,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2392 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_BACKFLUSH_
;     | STARTED;                                                               
; 2393 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2392| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2392| 
        MOVB      *+XAR4[AR0],#21,UNC   ; [CPU_] |2392| 
	.dwpsn	file "../source/CommProtocol.c",line 2394,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2394 | usnDataLength++;                                                       
; 2396 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2394| 
$C$L206:    
	.dwpsn	file "../source/CommProtocol.c",line 2398,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2398 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPresBackflushEnd)           
; 2400 |     //!Set error for backflush pressure                                
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0010 ; [CPU_] |2398| 
        LSR       AL,4                  ; [CPU_] |2398| 
        CMPB      AL,#1                 ; [CPU_] |2398| 
        B         $C$L207,NEQ           ; [CPU_] |2398| 
        ; branchcc occurs ; [] |2398| 
	.dwpsn	file "../source/CommProtocol.c",line 2401,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2401 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_PRESSURE_BACKFLUSH_
;     | COMPLETED;                                                             
; 2402 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2401| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2401| 
        MOVB      *+XAR4[AR0],#22,UNC   ; [CPU_] |2401| 
	.dwpsn	file "../source/CommProtocol.c",line 2403,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2403 | usnDataLength++;                                                       
; 2405 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2403| 
$C$L207:    
	.dwpsn	file "../source/CommProtocol.c",line 2407,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2407 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacWBCDrainingFD)           
; 2409 |     //!Set error for WBC draining vacuum                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0020 ; [CPU_] |2407| 
        LSR       AL,5                  ; [CPU_] |2407| 
        CMPB      AL,#1                 ; [CPU_] |2407| 
        B         $C$L208,NEQ           ; [CPU_] |2407| 
        ; branchcc occurs ; [] |2407| 
	.dwpsn	file "../source/CommProtocol.c",line 2410,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2410 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_WBC_DRAINING
;     | _FD;                                                                   
; 2411 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2410| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2410| 
        MOVB      *+XAR4[AR0],#23,UNC   ; [CPU_] |2410| 
	.dwpsn	file "../source/CommProtocol.c",line 2412,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2412 | usnDataLength++;                                                       
; 2414 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2412| 
$C$L208:    
	.dwpsn	file "../source/CommProtocol.c",line 2416,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2416 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingFD)           
; 2418 |     //!Set error for RBC draining vacuum                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0040 ; [CPU_] |2416| 
        LSR       AL,6                  ; [CPU_] |2416| 
        CMPB      AL,#1                 ; [CPU_] |2416| 
        B         $C$L209,NEQ           ; [CPU_] |2416| 
        ; branchcc occurs ; [] |2416| 
	.dwpsn	file "../source/CommProtocol.c",line 2419,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2419 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING
;     | _FD;                                                                   
; 2420 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2419| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2419| 
        MOVB      *+XAR4[AR0],#24,UNC   ; [CPU_] |2419| 
	.dwpsn	file "../source/CommProtocol.c",line 2421,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2421 | usnDataLength++;                                                       
; 2423 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2421| 
$C$L209:    
	.dwpsn	file "../source/CommProtocol.c",line 2425,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2425 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnVacRBCDrainingFDEnd)        
; 2427 |     //!Set error for RBC draining vacuum                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0080 ; [CPU_] |2425| 
        LSR       AL,7                  ; [CPU_] |2425| 
        CMPB      AL,#1                 ; [CPU_] |2425| 
        B         $C$L210,NEQ           ; [CPU_] |2425| 
        ; branchcc occurs ; [] |2425| 
	.dwpsn	file "../source/CommProtocol.c",line 2428,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2428 | m_stPacketForTx.arrusnData[usnDataLength]= eMINIMUM_VACUUM_RBC_DRAINING
;     | _COMPLETED_FD;                                                         
; 2429 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2428| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2428| 
        MOVB      *+XAR4[AR0],#25,UNC   ; [CPU_] |2428| 
	.dwpsn	file "../source/CommProtocol.c",line 2430,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2430 | usnDataLength++;                                                       
; 2432 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2430| 
$C$L210:    
	.dwpsn	file "../source/CommProtocol.c",line 2434,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2434 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnAspirationNotCompleted)     
; 2436 |     //!Set error for RBC draining vacuum                               
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0100 ; [CPU_] |2434| 
        LSR       AL,8                  ; [CPU_] |2434| 
        CMPB      AL,#1                 ; [CPU_] |2434| 
        B         $C$L211,NEQ           ; [CPU_] |2434| 
        ; branchcc occurs ; [] |2434| 
	.dwpsn	file "../source/CommProtocol.c",line 2437,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2437 | m_stPacketForTx.arrusnData[usnDataLength]= eASPIRATION_NOT_COMPLETED;  
; 2438 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2437| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2437| 
        MOVB      *+XAR4[AR0],#26,UNC   ; [CPU_] |2437| 
	.dwpsn	file "../source/CommProtocol.c",line 2439,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2439 | usnDataLength++;                                                       
; 2441 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2439| 
$C$L211:    
	.dwpsn	file "../source/CommProtocol.c",line 2443,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2443 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnAspirationCompletedImproper)
; 2445 |     //!Set error for improper aspiration time                          
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0200 ; [CPU_] |2443| 
        LSR       AL,9                  ; [CPU_] |2443| 
        CMPB      AL,#1                 ; [CPU_] |2443| 
        B         $C$L212,NEQ           ; [CPU_] |2443| 
        ; branchcc occurs ; [] |2443| 
	.dwpsn	file "../source/CommProtocol.c",line 2446,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2446 | m_stPacketForTx.arrusnData[usnDataLength]= eASPIRATION_IMPROPER_COMPLET
;     | ION;                                                                   
; 2447 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2446| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2446| 
        MOVB      *+XAR4[AR0],#27,UNC   ; [CPU_] |2446| 
	.dwpsn	file "../source/CommProtocol.c",line 2448,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2448 | usnDataLength++;                                                       
; 2450 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2448| 
$C$L212:    
	.dwpsn	file "../source/CommProtocol.c",line 2452,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2452 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCDispensingNotCompleted)  
; 2454 |     //!Set error for WBC dispensing not completed                      
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0400 ; [CPU_] |2452| 
        LSR       AL,10                 ; [CPU_] |2452| 
        CMPB      AL,#1                 ; [CPU_] |2452| 
        B         $C$L213,NEQ           ; [CPU_] |2452| 
        ; branchcc occurs ; [] |2452| 
	.dwpsn	file "../source/CommProtocol.c",line 2455,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2455 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_DISPENSING_NOT_COMPLETE
;     | D;                                                                     
; 2456 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2455| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2455| 
        MOVB      *+XAR4[AR0],#28,UNC   ; [CPU_] |2455| 
	.dwpsn	file "../source/CommProtocol.c",line 2457,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2457 | usnDataLength++;                                                       
; 2459 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2457| 
$C$L213:    
	.dwpsn	file "../source/CommProtocol.c",line 2461,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2461 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCDispensingCompletedImprop
;     | er)                                                                    
; 2463 |     //!Set error for WBC dispensing completion improper                
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x0800 ; [CPU_] |2461| 
        LSR       AL,11                 ; [CPU_] |2461| 
        CMPB      AL,#1                 ; [CPU_] |2461| 
        B         $C$L214,NEQ           ; [CPU_] |2461| 
        ; branchcc occurs ; [] |2461| 
	.dwpsn	file "../source/CommProtocol.c",line 2464,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2464 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_DISPENSING_IMPROPER_COM
;     | PLETION;                                                               
; 2465 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2464| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2464| 
        MOVB      *+XAR4[AR0],#29,UNC   ; [CPU_] |2464| 
	.dwpsn	file "../source/CommProtocol.c",line 2466,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2466 | usnDataLength++;                                                       
; 2468 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2466| 
$C$L214:    
	.dwpsn	file "../source/CommProtocol.c",line 2470,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2470 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnRBCDispensingNotCompleted)  
; 2472 |     //!Set error for RBC dispensing not completed                      
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x1000 ; [CPU_] |2470| 
        LSR       AL,12                 ; [CPU_] |2470| 
        CMPB      AL,#1                 ; [CPU_] |2470| 
        B         $C$L215,NEQ           ; [CPU_] |2470| 
        ; branchcc occurs ; [] |2470| 
	.dwpsn	file "../source/CommProtocol.c",line 2473,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2473 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_DISPENSING_NOT_COMPLETE
;     | D;                                                                     
; 2474 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2473| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2473| 
        MOVB      *+XAR4[AR0],#30,UNC   ; [CPU_] |2473| 
	.dwpsn	file "../source/CommProtocol.c",line 2475,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2475 | usnDataLength++;                                                       
; 2477 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2475| 
$C$L215:    
	.dwpsn	file "../source/CommProtocol.c",line 2479,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2479 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnRBCDispensingCompletedImprop
;     | er)                                                                    
; 2481 |     //!Set error for WBC dispensing completion improper                
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x2000 ; [CPU_] |2479| 
        LSR       AL,13                 ; [CPU_] |2479| 
        CMPB      AL,#1                 ; [CPU_] |2479| 
        B         $C$L216,NEQ           ; [CPU_] |2479| 
        ; branchcc occurs ; [] |2479| 
	.dwpsn	file "../source/CommProtocol.c",line 2482,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2482 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_DISPENSING_IMPROPER_COM
;     | PLETION;                                                               
; 2483 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2482| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2482| 
        MOVB      *+XAR4[AR0],#31,UNC   ; [CPU_] |2482| 
	.dwpsn	file "../source/CommProtocol.c",line 2484,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2484 | usnDataLength++;                                                       
; 2486 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2484| 
$C$L216:    
	.dwpsn	file "../source/CommProtocol.c",line 2488,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2488 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCRBCBubblingNotCompleted) 
; 2490 |     //!Set error for WBC RBC bubbling not completed                    
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x4000 ; [CPU_] |2488| 
        LSR       AL,14                 ; [CPU_] |2488| 
        CMPB      AL,#1                 ; [CPU_] |2488| 
        B         $C$L217,NEQ           ; [CPU_] |2488| 
        ; branchcc occurs ; [] |2488| 
	.dwpsn	file "../source/CommProtocol.c",line 2491,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2491 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_RBC_BUBBLING_NOT_COMPLE
;     | TED;                                                                   
; 2492 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2491| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2491| 
        MOVB      *+XAR4[AR0],#32,UNC   ; [CPU_] |2491| 
	.dwpsn	file "../source/CommProtocol.c",line 2493,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2493 | usnDataLength++;                                                       
; 2495 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2493| 
$C$L217:    
	.dwpsn	file "../source/CommProtocol.c",line 2497,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2497 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWBCRBCBubblingCompletedImpro
;     | per)                                                                   
; 2499 |     //!Set error for WBC RBC bubbling completion improper              
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+1,#0x8000 ; [CPU_] |2497| 
        LSR       AL,15                 ; [CPU_] |2497| 
        CMPB      AL,#1                 ; [CPU_] |2497| 
        B         $C$L218,NEQ           ; [CPU_] |2497| 
        ; branchcc occurs ; [] |2497| 
	.dwpsn	file "../source/CommProtocol.c",line 2500,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2500 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_RBC_BUBBLING_IMPROPER_C
;     | OMPLETION;                                                             
; 2501 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2500| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2500| 
        MOVB      *+XAR4[AR0],#33,UNC   ; [CPU_] |2500| 
	.dwpsn	file "../source/CommProtocol.c",line 2502,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2502 | usnDataLength++;                                                       
; 2504 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2502| 
$C$L218:    
	.dwpsn	file "../source/CommProtocol.c",line 2506,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2506 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStar
;     | ted)                                                                   
; 2508 |     //!Set error for WBC counting not started                          
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfo ; [CPU_U] 
        MOV       AL,@_utVolumetricErrorInfo ; [CPU_] |2506| 
        ANDB      AL,#0x01              ; [CPU_] |2506| 
        CMPB      AL,#1                 ; [CPU_] |2506| 
        B         $C$L219,NEQ           ; [CPU_] |2506| 
        ; branchcc occurs ; [] |2506| 
	.dwpsn	file "../source/CommProtocol.c",line 2509,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2509 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_NOT_STARTED;  
; 2510 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2509| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2509| 
        MOVB      *+XAR4[AR0],#34,UNC   ; [CPU_] |2509| 
	.dwpsn	file "../source/CommProtocol.c",line 2511,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2511 | usnDataLength++;                                                       
; 2513 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2511| 
	.dwpsn	file "../source/CommProtocol.c",line 2514,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2514 | utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotStarted = FALSE
;     | ;                                                                      
;----------------------------------------------------------------------
        AND       @_utVolumetricErrorInfo,#0xfffe ; [CPU_] |2514| 
$C$L219:    
	.dwpsn	file "../source/CommProtocol.c",line 2516,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2516 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingStarted
;     | Improper)                                                              
; 2518 |     //!Set error for WBC counting started improper                     
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0002 ; [CPU_] |2516| 
        LSR       AL,1                  ; [CPU_] |2516| 
        CMPB      AL,#1                 ; [CPU_] |2516| 
        B         $C$L220,NEQ           ; [CPU_] |2516| 
        ; branchcc occurs ; [] |2516| 
	.dwpsn	file "../source/CommProtocol.c",line 2519,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2519 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_IMPROPER_START
;     | ;                                                                      
; 2520 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2519| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2519| 
        MOVB      *+XAR4[AR0],#35,UNC   ; [CPU_] |2519| 
	.dwpsn	file "../source/CommProtocol.c",line 2521,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2521 | usnDataLength++;                                                       
; 2523 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2521| 
	.dwpsn	file "../source/CommProtocol.c",line 2524,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2524 | utVolumetricErrorInfo.stVolumetricError.unWBCCountingStartedImproper =
;     | FALSE;                                                                 
;----------------------------------------------------------------------
        AND       @_utVolumetricErrorInfo,#0xfffd ; [CPU_] |2524| 
$C$L220:    
	.dwpsn	file "../source/CommProtocol.c",line 2526,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2526 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStar
;     | ted)                                                                   
; 2528 |     //!Set error for RBC counting not started                          
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0004 ; [CPU_] |2526| 
        LSR       AL,2                  ; [CPU_] |2526| 
        CMPB      AL,#1                 ; [CPU_] |2526| 
        B         $C$L221,NEQ           ; [CPU_] |2526| 
        ; branchcc occurs ; [] |2526| 
	.dwpsn	file "../source/CommProtocol.c",line 2529,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2529 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_NOT_STARTED;  
; 2530 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2529| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2529| 
        MOVB      *+XAR4[AR0],#36,UNC   ; [CPU_] |2529| 
	.dwpsn	file "../source/CommProtocol.c",line 2531,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2531 | usnDataLength++;                                                       
; 2533 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2531| 
	.dwpsn	file "../source/CommProtocol.c",line 2534,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2534 | utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotStarted = FALSE
;     | ;                                                                      
;----------------------------------------------------------------------
        AND       @_utVolumetricErrorInfo,#0xfffb ; [CPU_] |2534| 
$C$L221:    
	.dwpsn	file "../source/CommProtocol.c",line 2536,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2536 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingStarted
;     | Improper)                                                              
; 2538 |     //!Set error for RBC counting started improper                     
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0008 ; [CPU_] |2536| 
        LSR       AL,3                  ; [CPU_] |2536| 
        CMPB      AL,#1                 ; [CPU_] |2536| 
        B         $C$L222,NEQ           ; [CPU_] |2536| 
        ; branchcc occurs ; [] |2536| 
	.dwpsn	file "../source/CommProtocol.c",line 2539,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2539 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_IMPROPER_START
;     | ;                                                                      
; 2540 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2539| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2539| 
        MOVB      *+XAR4[AR0],#37,UNC   ; [CPU_] |2539| 
	.dwpsn	file "../source/CommProtocol.c",line 2541,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2541 | usnDataLength++;                                                       
; 2543 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2541| 
	.dwpsn	file "../source/CommProtocol.c",line 2544,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2544 | utVolumetricErrorInfo.stVolumetricError.unRBCCountingStartedImproper =
;     | FALSE;                                                                 
;----------------------------------------------------------------------
        AND       @_utVolumetricErrorInfo,#0xfff7 ; [CPU_] |2544| 
$C$L222:    
	.dwpsn	file "../source/CommProtocol.c",line 2546,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2546 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotComp
;     | leted)                                                                 
; 2548 |     //!Set error for RBC counting not completed                        
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0010 ; [CPU_] |2546| 
        LSR       AL,4                  ; [CPU_] |2546| 
        CMPB      AL,#1                 ; [CPU_] |2546| 
        B         $C$L223,NEQ           ; [CPU_] |2546| 
        ; branchcc occurs ; [] |2546| 
	.dwpsn	file "../source/CommProtocol.c",line 2549,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2549 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_NOT_COMPLETED;
; 2550 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2549| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2549| 
        MOVB      *+XAR4[AR0],#38,UNC   ; [CPU_] |2549| 
	.dwpsn	file "../source/CommProtocol.c",line 2551,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2551 | usnDataLength++;                                                       
; 2553 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2551| 
	.dwpsn	file "../source/CommProtocol.c",line 2554,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2554 | utVolumetricErrorInfo.stVolumetricError.unWBCCountingNotCompleted = FAL
;     | SE;                                                                    
;----------------------------------------------------------------------
        AND       @_utVolumetricErrorInfo,#0xffef ; [CPU_] |2554| 
$C$L223:    
	.dwpsn	file "../source/CommProtocol.c",line 2556,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2556 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCCountingComplet
;     | edImproper)                                                            
; 2558 |     //!Set error for WBC counting completed improper                   
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0020 ; [CPU_] |2556| 
        LSR       AL,5                  ; [CPU_] |2556| 
        CMPB      AL,#1                 ; [CPU_] |2556| 
        B         $C$L224,NEQ           ; [CPU_] |2556| 
        ; branchcc occurs ; [] |2556| 
	.dwpsn	file "../source/CommProtocol.c",line 2559,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2559 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_IMPROPER_COMPL
;     | ETION;                                                                 
; 2560 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2559| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2559| 
        MOVB      *+XAR4[AR0],#39,UNC   ; [CPU_] |2559| 
	.dwpsn	file "../source/CommProtocol.c",line 2561,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2561 | usnDataLength++;                                                       
; 2563 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2561| 
	.dwpsn	file "../source/CommProtocol.c",line 2564,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2564 | utVolumetricErrorInfo.stVolumetricError.unWBCCountingCompletedImproper
;     | = FALSE;                                                               
;----------------------------------------------------------------------
        AND       @_utVolumetricErrorInfo,#0xffdf ; [CPU_] |2564| 
$C$L224:    
	.dwpsn	file "../source/CommProtocol.c",line 2567,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2567 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotComp
;     | leted)                                                                 
; 2569 |     //!Set error for RBC counting not completed                        
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0040 ; [CPU_] |2567| 
        LSR       AL,6                  ; [CPU_] |2567| 
        CMPB      AL,#1                 ; [CPU_] |2567| 
        B         $C$L225,NEQ           ; [CPU_] |2567| 
        ; branchcc occurs ; [] |2567| 
	.dwpsn	file "../source/CommProtocol.c",line 2570,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2570 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_NOT_COMPLETED;
; 2571 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2570| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2570| 
        MOVB      *+XAR4[AR0],#40,UNC   ; [CPU_] |2570| 
	.dwpsn	file "../source/CommProtocol.c",line 2572,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2572 | usnDataLength++;                                                       
; 2574 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2572| 
	.dwpsn	file "../source/CommProtocol.c",line 2575,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2575 | utVolumetricErrorInfo.stVolumetricError.unRBCCountingNotCompleted = FAL
;     | SE;                                                                    
;----------------------------------------------------------------------
        AND       @_utVolumetricErrorInfo,#0xffbf ; [CPU_] |2575| 
$C$L225:    
	.dwpsn	file "../source/CommProtocol.c",line 2577,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2577 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCCountingComplet
;     | edImproper)                                                            
; 2579 |     //!Set error for RBC counting completed improper                   
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0080 ; [CPU_] |2577| 
        LSR       AL,7                  ; [CPU_] |2577| 
        CMPB      AL,#1                 ; [CPU_] |2577| 
        B         $C$L226,NEQ           ; [CPU_] |2577| 
        ; branchcc occurs ; [] |2577| 
	.dwpsn	file "../source/CommProtocol.c",line 2580,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2580 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_IMPROPER_COMPL
;     | ETION;                                                                 
; 2581 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2580| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2580| 
        MOVB      *+XAR4[AR0],#41,UNC   ; [CPU_] |2580| 
	.dwpsn	file "../source/CommProtocol.c",line 2582,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2582 | usnDataLength++;                                                       
; 2584 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2582| 
	.dwpsn	file "../source/CommProtocol.c",line 2585,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2585 | utVolumetricErrorInfo.stVolumetricError.unRBCCountingCompletedImproper
;     | = FALSE;                                                               
;----------------------------------------------------------------------
        AND       @_utVolumetricErrorInfo,#0xff7f ; [CPU_] |2585| 
$C$L226:    
	.dwpsn	file "../source/CommProtocol.c",line 2587,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2587 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnBackFlushNotCompleted)      
; 2589 |     //!Set error for Backflush not completed                           
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        MOV       AL,@_utErrorInfoCheck+2 ; [CPU_] |2587| 
        ANDB      AL,#0x01              ; [CPU_] |2587| 
        CMPB      AL,#1                 ; [CPU_] |2587| 
        B         $C$L227,NEQ           ; [CPU_] |2587| 
        ; branchcc occurs ; [] |2587| 
	.dwpsn	file "../source/CommProtocol.c",line 2590,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2590 | m_stPacketForTx.arrusnData[usnDataLength]= eBACKFLUSH_NOT_COMPLETED;   
; 2591 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2590| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2590| 
        MOVB      *+XAR4[AR0],#42,UNC   ; [CPU_] |2590| 
	.dwpsn	file "../source/CommProtocol.c",line 2592,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2592 | usnDataLength++;                                                       
; 2594 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2592| 
$C$L227:    
	.dwpsn	file "../source/CommProtocol.c",line 2596,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2596 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnBackFlushCompletedImproper) 
; 2598 |     //!Set error for Backflush completion improper                     
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0002 ; [CPU_] |2596| 
        LSR       AL,1                  ; [CPU_] |2596| 
        CMPB      AL,#1                 ; [CPU_] |2596| 
        B         $C$L228,NEQ           ; [CPU_] |2596| 
        ; branchcc occurs ; [] |2596| 
	.dwpsn	file "../source/CommProtocol.c",line 2599,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2599 | m_stPacketForTx.arrusnData[usnDataLength]= eBACKFLUSH_IMPROPER_COMPLETI
;     | ON;                                                                    
; 2600 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2599| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2599| 
        MOVB      *+XAR4[AR0],#43,UNC   ; [CPU_] |2599| 
	.dwpsn	file "../source/CommProtocol.c",line 2601,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2601 | usnDataLength++;                                                       
; 2603 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2601| 
$C$L228:    
	.dwpsn	file "../source/CommProtocol.c",line 2605,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2605 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnBathFillingNotCompleted)    
; 2607 |     //!Set error for Bath filling not completed                        
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0004 ; [CPU_] |2605| 
        LSR       AL,2                  ; [CPU_] |2605| 
        CMPB      AL,#1                 ; [CPU_] |2605| 
        B         $C$L229,NEQ           ; [CPU_] |2605| 
        ; branchcc occurs ; [] |2605| 
	.dwpsn	file "../source/CommProtocol.c",line 2608,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2608 | m_stPacketForTx.arrusnData[usnDataLength]= eBATHFILLING_NOT_COMPLETED; 
; 2609 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2608| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2608| 
        MOVB      *+XAR4[AR0],#44,UNC   ; [CPU_] |2608| 
	.dwpsn	file "../source/CommProtocol.c",line 2610,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2610 | usnDataLength++;                                                       
; 2612 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2610| 
$C$L229:    
	.dwpsn	file "../source/CommProtocol.c",line 2614,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2614 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnBathFillingCompletedImproper
;     | )                                                                      
; 2616 |     //!Set error for Backflush completion improper                     
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0008 ; [CPU_] |2614| 
        LSR       AL,3                  ; [CPU_] |2614| 
        CMPB      AL,#1                 ; [CPU_] |2614| 
        B         $C$L230,NEQ           ; [CPU_] |2614| 
        ; branchcc occurs ; [] |2614| 
	.dwpsn	file "../source/CommProtocol.c",line 2617,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2617 | m_stPacketForTx.arrusnData[usnDataLength]= eBATHFILLING_IMPROPER_COMPLE
;     | TION;                                                                  
; 2618 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2617| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2617| 
        MOVB      *+XAR4[AR0],#45,UNC   ; [CPU_] |2617| 
	.dwpsn	file "../source/CommProtocol.c",line 2619,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2619 | usnDataLength++;                                                       
; 2621 | //!Reset the error after sending the error to Sitara                   
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2619| 
$C$L230:    
	.dwpsn	file "../source/CommProtocol.c",line 2623,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2623 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWasteBinFull)               
; 2625 |     //!Set error for Waste bin full                                    
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0010 ; [CPU_] |2623| 
        LSR       AL,4                  ; [CPU_] |2623| 
        CMPB      AL,#1                 ; [CPU_] |2623| 
        B         $C$L231,NEQ           ; [CPU_] |2623| 
        ; branchcc occurs ; [] |2623| 
	.dwpsn	file "../source/CommProtocol.c",line 2626,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2626 | m_stPacketForTx.arrusnData[usnDataLength]= eWASTE_BIN_FULL;            
; 2627 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2626| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2626| 
        MOVB      *+XAR4[AR0],#46,UNC   ; [CPU_] |2626| 
	.dwpsn	file "../source/CommProtocol.c",line 2628,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2628 | usnDataLength++;                                                       
; 2630 | //!Do not reset the error after sending the error to Sitara            
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2628| 
$C$L231:    
	.dwpsn	file "../source/CommProtocol.c",line 2632,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2632 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty)               
; 2634 |     //!Set error for Diluent empty                                     
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0020 ; [CPU_] |2632| 
        LSR       AL,5                  ; [CPU_] |2632| 
        CMPB      AL,#1                 ; [CPU_] |2632| 
        B         $C$L232,NEQ           ; [CPU_] |2632| 
        ; branchcc occurs ; [] |2632| 
	.dwpsn	file "../source/CommProtocol.c",line 2635,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2635 | m_stPacketForTx.arrusnData[usnDataLength]= eDILUENT_EMPTY;             
; 2637 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2635| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2635| 
        MOVB      *+XAR4[AR0],#47,UNC   ; [CPU_] |2635| 
	.dwpsn	file "../source/CommProtocol.c",line 2638,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2638 | usnDataLength++;                                                       
; 2640 | //!Reset the error after sending the error to Sitara                   
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2638| 
	.dwpsn	file "../source/CommProtocol.c",line 2641,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2641 | utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;                  
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xffdf ; [CPU_] |2641| 
$C$L232:    
	.dwpsn	file "../source/CommProtocol.c",line 2643,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2643 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnLyseEmpty)                  
; 2645 |     //!Set error for Lyse empty                                        
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0040 ; [CPU_] |2643| 
        LSR       AL,6                  ; [CPU_] |2643| 
        CMPB      AL,#1                 ; [CPU_] |2643| 
        B         $C$L233,NEQ           ; [CPU_] |2643| 
        ; branchcc occurs ; [] |2643| 
	.dwpsn	file "../source/CommProtocol.c",line 2646,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2646 | m_stPacketForTx.arrusnData[usnDataLength]= eLYSE_EMPTY;                
; 2648 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2646| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2646| 
        MOVB      *+XAR4[AR0],#48,UNC   ; [CPU_] |2646| 
	.dwpsn	file "../source/CommProtocol.c",line 2649,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2649 | usnDataLength++;                                                       
; 2651 | //!Reset the error after sending the error to Sitara                   
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2649| 
	.dwpsn	file "../source/CommProtocol.c",line 2652,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2652 | utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;                     
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xffbf ; [CPU_] |2652| 
$C$L233:    
	.dwpsn	file "../source/CommProtocol.c",line 2654,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2654 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnRinseEmpty)                 
; 2656 |     //!Set error for Rinse empty                                       
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0080 ; [CPU_] |2654| 
        LSR       AL,7                  ; [CPU_] |2654| 
        CMPB      AL,#1                 ; [CPU_] |2654| 
        B         $C$L234,NEQ           ; [CPU_] |2654| 
        ; branchcc occurs ; [] |2654| 
	.dwpsn	file "../source/CommProtocol.c",line 2657,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2657 | m_stPacketForTx.arrusnData[usnDataLength]= eRINSE_EMPTY;               
; 2659 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2657| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2657| 
        MOVB      *+XAR4[AR0],#49,UNC   ; [CPU_] |2657| 
	.dwpsn	file "../source/CommProtocol.c",line 2660,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2660 | usnDataLength++;                                                       
; 2662 | //!Reset the error after sending the error to Sitara                   
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2660| 
	.dwpsn	file "../source/CommProtocol.c",line 2663,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2663 | utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;                    
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xff7f ; [CPU_] |2663| 
$C$L234:    
	.dwpsn	file "../source/CommProtocol.c",line 2665,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2665 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnTemperature)                
; 2667 |     //!Set error for Temperature not with in the range                 
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0100 ; [CPU_] |2665| 
        LSR       AL,8                  ; [CPU_] |2665| 
        CMPB      AL,#1                 ; [CPU_] |2665| 
        B         $C$L235,NEQ           ; [CPU_] |2665| 
        ; branchcc occurs ; [] |2665| 
	.dwpsn	file "../source/CommProtocol.c",line 2668,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2668 | m_stPacketForTx.arrusnData[usnDataLength]= eTEMPERATURE_ERROR;         
; 2669 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2668| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2668| 
        MOVB      *+XAR4[AR0],#50,UNC   ; [CPU_] |2668| 
	.dwpsn	file "../source/CommProtocol.c",line 2670,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2670 | usnDataLength++;                                                       
; 2672 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2670| 
	.dwpsn	file "../source/CommProtocol.c",line 2673,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2673 | utErrorInfoCheck.stErrorInfo.ulnTemperature = FALSE;                   
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xfeff ; [CPU_] |2673| 
$C$L235:    
	.dwpsn	file "../source/CommProtocol.c",line 2675,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2675 | if(TRUE == utErrorInfoCheck.stErrorInfo.uln24V_Check)                  
; 2677 |     //!Set error for 24V not with in the range                         
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0200 ; [CPU_] |2675| 
        LSR       AL,9                  ; [CPU_] |2675| 
        CMPB      AL,#1                 ; [CPU_] |2675| 
        B         $C$L236,NEQ           ; [CPU_] |2675| 
        ; branchcc occurs ; [] |2675| 
	.dwpsn	file "../source/CommProtocol.c",line 2678,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2678 | m_stPacketForTx.arrusnData[usnDataLength]= e24V_ERROR;                 
; 2680 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2678| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2678| 
        MOVB      *+XAR4[AR0],#51,UNC   ; [CPU_] |2678| 
	.dwpsn	file "../source/CommProtocol.c",line 2681,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2681 | usnDataLength++;                                                       
; 2683 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2681| 
	.dwpsn	file "../source/CommProtocol.c",line 2684,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2684 | utErrorInfoCheck.stErrorInfo.uln24V_Check = FALSE;                     
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xfdff ; [CPU_] |2684| 
$C$L236:    
	.dwpsn	file "../source/CommProtocol.c",line 2686,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2686 | if(TRUE == utErrorInfoCheck.stErrorInfo.uln5V_Check)                   
; 2688 |     //!Set error for 5V not with in the range                          
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0400 ; [CPU_] |2686| 
        LSR       AL,10                 ; [CPU_] |2686| 
        CMPB      AL,#1                 ; [CPU_] |2686| 
        B         $C$L237,NEQ           ; [CPU_] |2686| 
        ; branchcc occurs ; [] |2686| 
	.dwpsn	file "../source/CommProtocol.c",line 2689,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2689 | m_stPacketForTx.arrusnData[usnDataLength]= e5V_ERROR;                  
; 2690 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2689| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2689| 
        MOVB      *+XAR4[AR0],#52,UNC   ; [CPU_] |2689| 
	.dwpsn	file "../source/CommProtocol.c",line 2691,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2691 | usnDataLength++;                                                       
; 2693 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2691| 
	.dwpsn	file "../source/CommProtocol.c",line 2694,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2694 | utErrorInfoCheck.stErrorInfo.uln5V_Check = FALSE;                      
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xfbff ; [CPU_] |2694| 
$C$L237:    
	.dwpsn	file "../source/CommProtocol.c",line 2696,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2696 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnReAcq)                      
; 2698 |     //!Set error for 5V not with in the range                          
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x0800 ; [CPU_] |2696| 
        LSR       AL,11                 ; [CPU_] |2696| 
        CMPB      AL,#1                 ; [CPU_] |2696| 
        B         $C$L238,NEQ           ; [CPU_] |2696| 
        ; branchcc occurs ; [] |2696| 
	.dwpsn	file "../source/CommProtocol.c",line 2699,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2699 | m_stPacketForTx.arrusnData[usnDataLength]= eRE_ACQUISITION;            
; 2700 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2699| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2699| 
        MOVB      *+XAR4[AR0],#53,UNC   ; [CPU_] |2699| 
	.dwpsn	file "../source/CommProtocol.c",line 2701,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2701 | usnDataLength++;                                                       
; 2703 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2701| 
	.dwpsn	file "../source/CommProtocol.c",line 2704,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2704 | utErrorInfoCheck.stErrorInfo.ulnReAcq = FALSE;                         
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xf7ff ; [CPU_] |2704| 
$C$L238:    
	.dwpsn	file "../source/CommProtocol.c",line 2707,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2707 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCEarlyCompletion
;     | )                                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfo ; [CPU_U] 
        AND       AL,@_utVolumetricErrorInfo,#0x0100 ; [CPU_] |2707| 
        LSR       AL,8                  ; [CPU_] |2707| 
        CMPB      AL,#1                 ; [CPU_] |2707| 
        B         $C$L239,NEQ           ; [CPU_] |2707| 
        ; branchcc occurs ; [] |2707| 
	.dwpsn	file "../source/CommProtocol.c",line 2709,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2709 | sprintf(DebugPrintBuf, "\r\nunWBCEarlyCompletion");                    
; 2710 | //!Set error for 5V not with in the range                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL2         ; [CPU_U] |2709| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2709| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2709| 
$C$DW$364	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$364, DW_AT_low_pc(0x00)
	.dwattr $C$DW$364, DW_AT_name("_sprintf")
	.dwattr $C$DW$364, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2709| 
        ; call occurs [#_sprintf] ; [] |2709| 
	.dwpsn	file "../source/CommProtocol.c",line 2711,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2711 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_EARLY_COMPLETI
;     | ON;                                                                    
; 2712 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2711| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2711| 
        MOVB      *+XAR4[AR0],#54,UNC   ; [CPU_] |2711| 
	.dwpsn	file "../source/CommProtocol.c",line 2713,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2713 | usnDataLength++;                                                       
; 2715 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2713| 
	.dwpsn	file "../source/CommProtocol.c",line 2716,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2716 | utVolumetricErrorInfo.stVolumetricError.unWBCEarlyCompletion = FALSE;  
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfo ; [CPU_U] 
        AND       @_utVolumetricErrorInfo,#0xfeff ; [CPU_] |2716| 
$C$L239:    
	.dwpsn	file "../source/CommProtocol.c",line 2719,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2719 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unWBCLateCompletion)
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0200 ; [CPU_] |2719| 
        LSR       AL,9                  ; [CPU_] |2719| 
        CMPB      AL,#1                 ; [CPU_] |2719| 
        B         $C$L240,NEQ           ; [CPU_] |2719| 
        ; branchcc occurs ; [] |2719| 
	.dwpsn	file "../source/CommProtocol.c",line 2721,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2721 | sprintf(DebugPrintBuf, "\r\nunWBCLateCompletion");                     
; 2722 | //!Set error for 5V not with in the range                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL3         ; [CPU_U] |2721| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2721| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2721| 
$C$DW$365	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$365, DW_AT_low_pc(0x00)
	.dwattr $C$DW$365, DW_AT_name("_sprintf")
	.dwattr $C$DW$365, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2721| 
        ; call occurs [#_sprintf] ; [] |2721| 
	.dwpsn	file "../source/CommProtocol.c",line 2723,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2723 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_LATE_COMPLETIO
;     | N;                                                                     
; 2724 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2723| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2723| 
        MOVB      *+XAR4[AR0],#55,UNC   ; [CPU_] |2723| 
	.dwpsn	file "../source/CommProtocol.c",line 2725,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2725 | usnDataLength++;                                                       
; 2727 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2725| 
	.dwpsn	file "../source/CommProtocol.c",line 2728,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2728 | utVolumetricErrorInfo.stVolumetricError.unWBCLateCompletion = FALSE;   
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfo ; [CPU_U] 
        AND       @_utVolumetricErrorInfo,#0xfdff ; [CPU_] |2728| 
$C$L240:    
	.dwpsn	file "../source/CommProtocol.c",line 2731,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2731 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCEarlyCompletion
;     | )                                                                      
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0400 ; [CPU_] |2731| 
        LSR       AL,10                 ; [CPU_] |2731| 
        CMPB      AL,#1                 ; [CPU_] |2731| 
        B         $C$L241,NEQ           ; [CPU_] |2731| 
        ; branchcc occurs ; [] |2731| 
	.dwpsn	file "../source/CommProtocol.c",line 2733,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2733 | sprintf(DebugPrintBuf, "\r\nunRBCEarlyCompletion");                    
; 2734 | //!Set error for 5V not with in the range                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL4         ; [CPU_U] |2733| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2733| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2733| 
$C$DW$366	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$366, DW_AT_low_pc(0x00)
	.dwattr $C$DW$366, DW_AT_name("_sprintf")
	.dwattr $C$DW$366, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2733| 
        ; call occurs [#_sprintf] ; [] |2733| 
	.dwpsn	file "../source/CommProtocol.c",line 2735,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2735 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_EARLY_COMPLETI
;     | ON;                                                                    
; 2736 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2735| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2735| 
        MOVB      *+XAR4[AR0],#56,UNC   ; [CPU_] |2735| 
	.dwpsn	file "../source/CommProtocol.c",line 2737,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2737 | usnDataLength++;                                                       
; 2739 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2737| 
	.dwpsn	file "../source/CommProtocol.c",line 2740,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2740 | utVolumetricErrorInfo.stVolumetricError.unRBCEarlyCompletion = FALSE;  
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfo ; [CPU_U] 
        AND       @_utVolumetricErrorInfo,#0xfbff ; [CPU_] |2740| 
$C$L241:    
	.dwpsn	file "../source/CommProtocol.c",line 2743,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2743 | if(TRUE == utVolumetricErrorInfo.stVolumetricError.unRBCLateCompletion)
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfo,#0x0800 ; [CPU_] |2743| 
        LSR       AL,11                 ; [CPU_] |2743| 
        CMPB      AL,#1                 ; [CPU_] |2743| 
        B         $C$L242,NEQ           ; [CPU_] |2743| 
        ; branchcc occurs ; [] |2743| 
	.dwpsn	file "../source/CommProtocol.c",line 2745,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2745 | sprintf(DebugPrintBuf, "\r\nunRBCLateCompletion");                     
; 2746 | //!Set error for 5V not with in the range                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL5         ; [CPU_U] |2745| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2745| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2745| 
$C$DW$367	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$367, DW_AT_low_pc(0x00)
	.dwattr $C$DW$367, DW_AT_name("_sprintf")
	.dwattr $C$DW$367, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2745| 
        ; call occurs [#_sprintf] ; [] |2745| 
	.dwpsn	file "../source/CommProtocol.c",line 2747,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2747 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_LATE_COMPLETIO
;     | N;                                                                     
; 2748 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2747| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2747| 
        MOVB      *+XAR4[AR0],#57,UNC   ; [CPU_] |2747| 
	.dwpsn	file "../source/CommProtocol.c",line 2749,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2749 | usnDataLength++;                                                       
; 2751 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2749| 
	.dwpsn	file "../source/CommProtocol.c",line 2752,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2752 | utVolumetricErrorInfo.stVolumetricError.unRBCLateCompletion = FALSE;   
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfo ; [CPU_U] 
        AND       @_utVolumetricErrorInfo,#0xf7ff ; [CPU_] |2752| 
$C$L242:    
	.dwpsn	file "../source/CommProtocol.c",line 2755,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2755 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCount
;     | ingNotStarted)                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        MOV       AL,@_utVolumetricErrorInfoReAcq ; [CPU_] |2755| 
        ANDB      AL,#0x01              ; [CPU_] |2755| 
        CMPB      AL,#1                 ; [CPU_] |2755| 
        B         $C$L243,NEQ           ; [CPU_] |2755| 
        ; branchcc occurs ; [] |2755| 
	.dwpsn	file "../source/CommProtocol.c",line 2757,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2757 | sprintf(DebugPrintBuf, "\r\nunWBCCountingNotStarted");                 
; 2758 | //!Set error for WBC counting not started                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL6         ; [CPU_U] |2757| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2757| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2757| 
$C$DW$368	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$368, DW_AT_low_pc(0x00)
	.dwattr $C$DW$368, DW_AT_name("_sprintf")
	.dwattr $C$DW$368, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2757| 
        ; call occurs [#_sprintf] ; [] |2757| 
	.dwpsn	file "../source/CommProtocol.c",line 2759,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2759 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_NOT_STARTED_RE
;     | _ACQ;                                                                  
; 2760 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2759| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2759| 
        MOVB      *+XAR4[AR0],#58,UNC   ; [CPU_] |2759| 
	.dwpsn	file "../source/CommProtocol.c",line 2761,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2761 | usnDataLength++;                                                       
; 2763 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2761| 
	.dwpsn	file "../source/CommProtocol.c",line 2764,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2764 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingNotStart
;     | ed = FALSE;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xfffe ; [CPU_] |2764| 
$C$L243:    
	.dwpsn	file "../source/CommProtocol.c",line 2766,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2766 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCount
;     | ingStartedImproper)                                                    
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0002 ; [CPU_] |2766| 
        LSR       AL,1                  ; [CPU_] |2766| 
        CMPB      AL,#1                 ; [CPU_] |2766| 
        B         $C$L244,NEQ           ; [CPU_] |2766| 
        ; branchcc occurs ; [] |2766| 
	.dwpsn	file "../source/CommProtocol.c",line 2768,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2768 | sprintf(DebugPrintBuf, "\r\nunWBCCountingStartedImproper");            
; 2769 | //!Set error for WBC counting started improper                         
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL7         ; [CPU_U] |2768| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2768| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2768| 
$C$DW$369	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$369, DW_AT_low_pc(0x00)
	.dwattr $C$DW$369, DW_AT_name("_sprintf")
	.dwattr $C$DW$369, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2768| 
        ; call occurs [#_sprintf] ; [] |2768| 
	.dwpsn	file "../source/CommProtocol.c",line 2770,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2770 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_IMPROPER_START
;     | _RE_ACQ;                                                               
; 2771 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2770| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2770| 
        MOVB      *+XAR4[AR0],#59,UNC   ; [CPU_] |2770| 
	.dwpsn	file "../source/CommProtocol.c",line 2772,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2772 | usnDataLength++;                                                       
; 2774 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2772| 
	.dwpsn	file "../source/CommProtocol.c",line 2775,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2775 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingStartedI
;     | mproper = FALSE;                                                       
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xfffd ; [CPU_] |2775| 
$C$L244:    
	.dwpsn	file "../source/CommProtocol.c",line 2777,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2777 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCount
;     | ingNotStarted)                                                         
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0004 ; [CPU_] |2777| 
        LSR       AL,2                  ; [CPU_] |2777| 
        CMPB      AL,#1                 ; [CPU_] |2777| 
        B         $C$L245,NEQ           ; [CPU_] |2777| 
        ; branchcc occurs ; [] |2777| 
	.dwpsn	file "../source/CommProtocol.c",line 2779,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2779 | sprintf(DebugPrintBuf, "\r\nunRBCCountingNotStarted");                 
; 2780 | //!Set error for RBC counting not started                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL8         ; [CPU_U] |2779| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2779| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2779| 
$C$DW$370	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$370, DW_AT_low_pc(0x00)
	.dwattr $C$DW$370, DW_AT_name("_sprintf")
	.dwattr $C$DW$370, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2779| 
        ; call occurs [#_sprintf] ; [] |2779| 
	.dwpsn	file "../source/CommProtocol.c",line 2781,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2781 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_NOT_STARTED_RE
;     | _ACQ;                                                                  
; 2782 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2781| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2781| 
        MOVB      *+XAR4[AR0],#60,UNC   ; [CPU_] |2781| 
	.dwpsn	file "../source/CommProtocol.c",line 2783,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2783 | usnDataLength++;                                                       
; 2785 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2783| 
	.dwpsn	file "../source/CommProtocol.c",line 2786,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2786 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingNotStart
;     | ed = FALSE;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xfffb ; [CPU_] |2786| 
$C$L245:    
	.dwpsn	file "../source/CommProtocol.c",line 2788,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2788 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCount
;     | ingStartedImproper)                                                    
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0008 ; [CPU_] |2788| 
        LSR       AL,3                  ; [CPU_] |2788| 
        CMPB      AL,#1                 ; [CPU_] |2788| 
        B         $C$L246,NEQ           ; [CPU_] |2788| 
        ; branchcc occurs ; [] |2788| 
	.dwpsn	file "../source/CommProtocol.c",line 2790,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2790 | sprintf(DebugPrintBuf, "\r\nunRBCCountingStartedImproper");            
; 2791 | //!Set error for RBC counting started improper                         
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL9         ; [CPU_U] |2790| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2790| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2790| 
$C$DW$371	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$371, DW_AT_low_pc(0x00)
	.dwattr $C$DW$371, DW_AT_name("_sprintf")
	.dwattr $C$DW$371, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2790| 
        ; call occurs [#_sprintf] ; [] |2790| 
	.dwpsn	file "../source/CommProtocol.c",line 2792,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2792 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_IMPROPER_START
;     | _RE_ACQ;                                                               
; 2793 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2792| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2792| 
        MOVB      *+XAR4[AR0],#61,UNC   ; [CPU_] |2792| 
	.dwpsn	file "../source/CommProtocol.c",line 2794,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2794 | usnDataLength++;                                                       
; 2796 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2794| 
	.dwpsn	file "../source/CommProtocol.c",line 2797,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2797 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingStartedI
;     | mproper = FALSE;                                                       
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xfff7 ; [CPU_] |2797| 
$C$L246:    
	.dwpsn	file "../source/CommProtocol.c",line 2799,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2799 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCount
;     | ingNotCompleted)                                                       
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0010 ; [CPU_] |2799| 
        LSR       AL,4                  ; [CPU_] |2799| 
        CMPB      AL,#1                 ; [CPU_] |2799| 
        B         $C$L247,NEQ           ; [CPU_] |2799| 
        ; branchcc occurs ; [] |2799| 
	.dwpsn	file "../source/CommProtocol.c",line 2801,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2801 | sprintf(DebugPrintBuf, "\r\nunWBCCountingNotCompleted");               
; 2802 | //!Set error for RBC counting not completed                            
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL10        ; [CPU_U] |2801| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2801| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2801| 
$C$DW$372	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$372, DW_AT_low_pc(0x00)
	.dwattr $C$DW$372, DW_AT_name("_sprintf")
	.dwattr $C$DW$372, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2801| 
        ; call occurs [#_sprintf] ; [] |2801| 
	.dwpsn	file "../source/CommProtocol.c",line 2803,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2803 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_NOT_COMPLETED_
;     | RE_ACQ;                                                                
; 2804 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2803| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2803| 
        MOVB      *+XAR4[AR0],#62,UNC   ; [CPU_] |2803| 
	.dwpsn	file "../source/CommProtocol.c",line 2805,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2805 | usnDataLength++;                                                       
; 2807 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2805| 
	.dwpsn	file "../source/CommProtocol.c",line 2808,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2808 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingNotCompl
;     | eted = FALSE;                                                          
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xffef ; [CPU_] |2808| 
$C$L247:    
	.dwpsn	file "../source/CommProtocol.c",line 2810,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2810 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCount
;     | ingCompletedImproper)                                                  
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0020 ; [CPU_] |2810| 
        LSR       AL,5                  ; [CPU_] |2810| 
        CMPB      AL,#1                 ; [CPU_] |2810| 
        B         $C$L248,NEQ           ; [CPU_] |2810| 
        ; branchcc occurs ; [] |2810| 
	.dwpsn	file "../source/CommProtocol.c",line 2812,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2812 | sprintf(DebugPrintBuf, "\r\nunWBCCountingCompletedImproper");          
; 2813 | //!Set error for WBC counting completed improper                       
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL11        ; [CPU_U] |2812| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2812| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2812| 
$C$DW$373	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$373, DW_AT_low_pc(0x00)
	.dwattr $C$DW$373, DW_AT_name("_sprintf")
	.dwattr $C$DW$373, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2812| 
        ; call occurs [#_sprintf] ; [] |2812| 
	.dwpsn	file "../source/CommProtocol.c",line 2814,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2814 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_IMPROPER_COMPL
;     | ETION_RE_ACQ;                                                          
; 2815 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2814| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2814| 
        MOVB      *+XAR4[AR0],#63,UNC   ; [CPU_] |2814| 
	.dwpsn	file "../source/CommProtocol.c",line 2816,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2816 | usnDataLength++;                                                       
; 2818 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2816| 
	.dwpsn	file "../source/CommProtocol.c",line 2819,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2819 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCCountingComplete
;     | dImproper = FALSE;                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xffdf ; [CPU_] |2819| 
$C$L248:    
	.dwpsn	file "../source/CommProtocol.c",line 2822,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2822 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCount
;     | ingNotCompleted)                                                       
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0040 ; [CPU_] |2822| 
        LSR       AL,6                  ; [CPU_] |2822| 
        CMPB      AL,#1                 ; [CPU_] |2822| 
        B         $C$L249,NEQ           ; [CPU_] |2822| 
        ; branchcc occurs ; [] |2822| 
	.dwpsn	file "../source/CommProtocol.c",line 2824,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2824 | sprintf(DebugPrintBuf, "\r\nunRBCCountingNotCompleted");               
; 2825 | //!Set error for RBC counting not completed                            
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL12        ; [CPU_U] |2824| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2824| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2824| 
$C$DW$374	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$374, DW_AT_low_pc(0x00)
	.dwattr $C$DW$374, DW_AT_name("_sprintf")
	.dwattr $C$DW$374, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2824| 
        ; call occurs [#_sprintf] ; [] |2824| 
	.dwpsn	file "../source/CommProtocol.c",line 2826,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2826 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_NOT_COMPLETED_
;     | RE_ACQ;                                                                
; 2827 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2826| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2826| 
        MOVB      *+XAR4[AR0],#64,UNC   ; [CPU_] |2826| 
	.dwpsn	file "../source/CommProtocol.c",line 2828,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2828 | usnDataLength++;                                                       
; 2830 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2828| 
	.dwpsn	file "../source/CommProtocol.c",line 2831,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2831 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingNotCompl
;     | eted = FALSE;                                                          
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xffbf ; [CPU_] |2831| 
$C$L249:    
	.dwpsn	file "../source/CommProtocol.c",line 2833,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2833 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCount
;     | ingCompletedImproper)                                                  
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0080 ; [CPU_] |2833| 
        LSR       AL,7                  ; [CPU_] |2833| 
        CMPB      AL,#1                 ; [CPU_] |2833| 
        B         $C$L250,NEQ           ; [CPU_] |2833| 
        ; branchcc occurs ; [] |2833| 
	.dwpsn	file "../source/CommProtocol.c",line 2835,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2835 | sprintf(DebugPrintBuf, "\r\nunRBCCountingCompletedImproper");          
; 2836 | //!Set error for RBC counting completed improper                       
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL13        ; [CPU_U] |2835| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2835| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2835| 
$C$DW$375	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$375, DW_AT_low_pc(0x00)
	.dwattr $C$DW$375, DW_AT_name("_sprintf")
	.dwattr $C$DW$375, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2835| 
        ; call occurs [#_sprintf] ; [] |2835| 
	.dwpsn	file "../source/CommProtocol.c",line 2837,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2837 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_IMPROPER_COMPL
;     | ETION_RE_ACQ;                                                          
; 2838 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2837| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2837| 
        MOVB      *+XAR4[AR0],#65,UNC   ; [CPU_] |2837| 
	.dwpsn	file "../source/CommProtocol.c",line 2839,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2839 | usnDataLength++;                                                       
; 2841 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2839| 
	.dwpsn	file "../source/CommProtocol.c",line 2842,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2842 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCCountingComplete
;     | dImproper = FALSE;                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xff7f ; [CPU_] |2842| 
$C$L250:    
	.dwpsn	file "../source/CommProtocol.c",line 2845,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2845 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCEarly
;     | Completion)                                                            
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0100 ; [CPU_] |2845| 
        LSR       AL,8                  ; [CPU_] |2845| 
        CMPB      AL,#1                 ; [CPU_] |2845| 
        B         $C$L251,NEQ           ; [CPU_] |2845| 
        ; branchcc occurs ; [] |2845| 
	.dwpsn	file "../source/CommProtocol.c",line 2847,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2847 | sprintf(DebugPrintBuf, "\r\nunWBCEarlyCompletion");                    
; 2848 | //!Set error for 5V not with in the range                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL2         ; [CPU_U] |2847| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2847| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2847| 
$C$DW$376	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$376, DW_AT_low_pc(0x00)
	.dwattr $C$DW$376, DW_AT_name("_sprintf")
	.dwattr $C$DW$376, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2847| 
        ; call occurs [#_sprintf] ; [] |2847| 
	.dwpsn	file "../source/CommProtocol.c",line 2849,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2849 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_EARLY_COMPLETI
;     | ON_RE_ACQ;                                                             
; 2850 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2849| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2849| 
        MOVB      *+XAR4[AR0],#66,UNC   ; [CPU_] |2849| 
	.dwpsn	file "../source/CommProtocol.c",line 2851,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2851 | usnDataLength++;                                                       
; 2853 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2851| 
	.dwpsn	file "../source/CommProtocol.c",line 2854,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2854 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCEarlyCompletion
;     | = FALSE;                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xfeff ; [CPU_] |2854| 
$C$L251:    
	.dwpsn	file "../source/CommProtocol.c",line 2857,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2857 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCLateC
;     | ompletion)                                                             
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0200 ; [CPU_] |2857| 
        LSR       AL,9                  ; [CPU_] |2857| 
        CMPB      AL,#1                 ; [CPU_] |2857| 
        B         $C$L252,NEQ           ; [CPU_] |2857| 
        ; branchcc occurs ; [] |2857| 
	.dwpsn	file "../source/CommProtocol.c",line 2859,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2859 | sprintf(DebugPrintBuf, "\r\nunWBCLateCompletion");                     
; 2860 | //!Set error for 5V not with in the range                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL3         ; [CPU_U] |2859| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2859| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2859| 
$C$DW$377	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$377, DW_AT_low_pc(0x00)
	.dwattr $C$DW$377, DW_AT_name("_sprintf")
	.dwattr $C$DW$377, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2859| 
        ; call occurs [#_sprintf] ; [] |2859| 
	.dwpsn	file "../source/CommProtocol.c",line 2861,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2861 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_COUNTING_LATE_COMPLETIO
;     | N_RE_ACQ;                                                              
; 2862 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2861| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2861| 
        MOVB      *+XAR4[AR0],#67,UNC   ; [CPU_] |2861| 
	.dwpsn	file "../source/CommProtocol.c",line 2863,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2863 | usnDataLength++;                                                       
; 2865 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2863| 
	.dwpsn	file "../source/CommProtocol.c",line 2866,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2866 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCLateCompletion =
;     |  FALSE;                                                                
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xfdff ; [CPU_] |2866| 
$C$L252:    
	.dwpsn	file "../source/CommProtocol.c",line 2869,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2869 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCEarly
;     | Completion)                                                            
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0400 ; [CPU_] |2869| 
        LSR       AL,10                 ; [CPU_] |2869| 
        CMPB      AL,#1                 ; [CPU_] |2869| 
        B         $C$L253,NEQ           ; [CPU_] |2869| 
        ; branchcc occurs ; [] |2869| 
	.dwpsn	file "../source/CommProtocol.c",line 2871,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2871 | sprintf(DebugPrintBuf, "\r\nunRBCEarlyCompletion");                    
; 2872 | //!Set error for 5V not with in the range                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL4         ; [CPU_U] |2871| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2871| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2871| 
$C$DW$378	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$378, DW_AT_low_pc(0x00)
	.dwattr $C$DW$378, DW_AT_name("_sprintf")
	.dwattr $C$DW$378, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2871| 
        ; call occurs [#_sprintf] ; [] |2871| 
	.dwpsn	file "../source/CommProtocol.c",line 2873,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2873 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_EARLY_COMPLETI
;     | ON_RE_ACQ;                                                             
; 2874 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2873| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2873| 
        MOVB      *+XAR4[AR0],#68,UNC   ; [CPU_] |2873| 
	.dwpsn	file "../source/CommProtocol.c",line 2875,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2875 | usnDataLength++;                                                       
; 2877 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2875| 
	.dwpsn	file "../source/CommProtocol.c",line 2878,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2878 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCEarlyCompletion
;     | = FALSE;                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xfbff ; [CPU_] |2878| 
$C$L253:    
	.dwpsn	file "../source/CommProtocol.c",line 2881,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2881 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCLateC
;     | ompletion)                                                             
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x0800 ; [CPU_] |2881| 
        LSR       AL,11                 ; [CPU_] |2881| 
        CMPB      AL,#1                 ; [CPU_] |2881| 
        B         $C$L254,NEQ           ; [CPU_] |2881| 
        ; branchcc occurs ; [] |2881| 
	.dwpsn	file "../source/CommProtocol.c",line 2883,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2883 | sprintf(DebugPrintBuf, "\r\nunRBCLateCompletion");                     
; 2884 | //!Set error for 5V not with in the range                              
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL5         ; [CPU_U] |2883| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2883| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2883| 
$C$DW$379	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$379, DW_AT_low_pc(0x00)
	.dwattr $C$DW$379, DW_AT_name("_sprintf")
	.dwattr $C$DW$379, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2883| 
        ; call occurs [#_sprintf] ; [] |2883| 
	.dwpsn	file "../source/CommProtocol.c",line 2885,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2885 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_COUNTING_LATE_COMPLETIO
;     | N_RE_ACQ;                                                              
; 2886 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2885| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2885| 
        MOVB      *+XAR4[AR0],#69,UNC   ; [CPU_] |2885| 
	.dwpsn	file "../source/CommProtocol.c",line 2887,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2887 | usnDataLength++;                                                       
; 2889 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2887| 
	.dwpsn	file "../source/CommProtocol.c",line 2890,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2890 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCLateCompletion =
;     |  FALSE;                                                                
; 2892 | //DS_TESTING                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xf7ff ; [CPU_] |2890| 
$C$L254:    
	.dwpsn	file "../source/CommProtocol.c",line 2893,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2893 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCImpro
;     | perAbortion)                                                           
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x1000 ; [CPU_] |2893| 
        LSR       AL,12                 ; [CPU_] |2893| 
        CMPB      AL,#1                 ; [CPU_] |2893| 
        B         $C$L255,NEQ           ; [CPU_] |2893| 
        ; branchcc occurs ; [] |2893| 
	.dwpsn	file "../source/CommProtocol.c",line 2895,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2895 | sprintf(DebugPrintBuf, "\r\nunWBCImproperAbortion");                   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL14        ; [CPU_U] |2895| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2895| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2895| 
$C$DW$380	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$380, DW_AT_low_pc(0x00)
	.dwattr $C$DW$380, DW_AT_name("_sprintf")
	.dwattr $C$DW$380, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2895| 
        ; call occurs [#_sprintf] ; [] |2895| 
	.dwpsn	file "../source/CommProtocol.c",line 2896,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2896 | UartDebugPrint((int *)DebugPrintBuf, 40);                              
;----------------------------------------------------------------------
        MOVB      AL,#40                ; [CPU_] |2896| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2896| 
$C$DW$381	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$381, DW_AT_low_pc(0x00)
	.dwattr $C$DW$381, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$381, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2896| 
        ; call occurs [#_UartDebugPrint] ; [] |2896| 
	.dwpsn	file "../source/CommProtocol.c",line 2897,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2897 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_IMPROPER_ABORTION;     
; 2898 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2897| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2897| 
        MOVB      *+XAR4[AR0],#79,UNC   ; [CPU_] |2897| 
	.dwpsn	file "../source/CommProtocol.c",line 2899,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2899 | usnDataLength++;                                                       
; 2901 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2899| 
	.dwpsn	file "../source/CommProtocol.c",line 2902,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2902 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCImproperAbortion
;     |  = FALSE;                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xefff ; [CPU_] |2902| 
$C$L255:    
	.dwpsn	file "../source/CommProtocol.c",line 2904,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2904 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCImpro
;     | perAbortion)                                                           
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x2000 ; [CPU_] |2904| 
        LSR       AL,13                 ; [CPU_] |2904| 
        CMPB      AL,#1                 ; [CPU_] |2904| 
        B         $C$L256,NEQ           ; [CPU_] |2904| 
        ; branchcc occurs ; [] |2904| 
	.dwpsn	file "../source/CommProtocol.c",line 2906,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2906 | sprintf(DebugPrintBuf, "\r\nunRBCImproperAbortion");                   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL15        ; [CPU_U] |2906| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2906| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2906| 
$C$DW$382	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$382, DW_AT_low_pc(0x00)
	.dwattr $C$DW$382, DW_AT_name("_sprintf")
	.dwattr $C$DW$382, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2906| 
        ; call occurs [#_sprintf] ; [] |2906| 
	.dwpsn	file "../source/CommProtocol.c",line 2907,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2907 | UartDebugPrint((int *)DebugPrintBuf, 40);                              
;----------------------------------------------------------------------
        MOVB      AL,#40                ; [CPU_] |2907| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2907| 
$C$DW$383	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$383, DW_AT_low_pc(0x00)
	.dwattr $C$DW$383, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$383, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2907| 
        ; call occurs [#_UartDebugPrint] ; [] |2907| 
	.dwpsn	file "../source/CommProtocol.c",line 2908,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2908 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_IMPROPER_ABORTION;     
; 2909 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2908| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2908| 
        MOVB      *+XAR4[AR0],#80,UNC   ; [CPU_] |2908| 
	.dwpsn	file "../source/CommProtocol.c",line 2910,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2910 | usnDataLength++;                                                       
; 2912 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2910| 
	.dwpsn	file "../source/CommProtocol.c",line 2913,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2913 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCImproperAbortion
;     |  = FALSE;                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xdfff ; [CPU_] |2913| 
$C$L256:    
	.dwpsn	file "../source/CommProtocol.c",line 2915,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2915 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCFlowC
;     | alibAbortion)                                                          
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x4000 ; [CPU_] |2915| 
        LSR       AL,14                 ; [CPU_] |2915| 
        CMPB      AL,#1                 ; [CPU_] |2915| 
        B         $C$L257,NEQ           ; [CPU_] |2915| 
        ; branchcc occurs ; [] |2915| 
	.dwpsn	file "../source/CommProtocol.c",line 2917,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2917 | sprintf(DebugPrintBuf, "\r\nunWBCFlowCalibAbortion");                  
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL16        ; [CPU_U] |2917| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2917| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2917| 
$C$DW$384	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$384, DW_AT_low_pc(0x00)
	.dwattr $C$DW$384, DW_AT_name("_sprintf")
	.dwattr $C$DW$384, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2917| 
        ; call occurs [#_sprintf] ; [] |2917| 
	.dwpsn	file "../source/CommProtocol.c",line 2918,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2918 | UartDebugPrint((int *)DebugPrintBuf, 40);                              
;----------------------------------------------------------------------
        MOVB      AL,#40                ; [CPU_] |2918| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2918| 
$C$DW$385	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$385, DW_AT_low_pc(0x00)
	.dwattr $C$DW$385, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$385, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2918| 
        ; call occurs [#_UartDebugPrint] ; [] |2918| 
	.dwpsn	file "../source/CommProtocol.c",line 2919,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2919 | m_stPacketForTx.arrusnData[usnDataLength]= eWBC_FLOWCALIB_ABORTION;    
; 2920 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2919| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2919| 
        MOVB      *+XAR4[AR0],#82,UNC   ; [CPU_] |2919| 
	.dwpsn	file "../source/CommProtocol.c",line 2921,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2921 | usnDataLength++;                                                       
; 2923 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2921| 
	.dwpsn	file "../source/CommProtocol.c",line 2924,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2924 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unWBCFlowCalibAbortio
;     | n = FALSE;                                                             
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0xbfff ; [CPU_] |2924| 
$C$L257:    
	.dwpsn	file "../source/CommProtocol.c",line 2926,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2926 | if(TRUE == utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCFlowC
;     | alibAbortion)                                                          
;----------------------------------------------------------------------
        AND       AL,@_utVolumetricErrorInfoReAcq,#0x8000 ; [CPU_] |2926| 
        LSR       AL,15                 ; [CPU_] |2926| 
        CMPB      AL,#1                 ; [CPU_] |2926| 
        B         $C$L258,NEQ           ; [CPU_] |2926| 
        ; branchcc occurs ; [] |2926| 
	.dwpsn	file "../source/CommProtocol.c",line 2928,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2928 | sprintf(DebugPrintBuf, "\r\nunRBCFlowCalibAbortion");                  
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL17        ; [CPU_U] |2928| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2928| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2928| 
$C$DW$386	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$386, DW_AT_low_pc(0x00)
	.dwattr $C$DW$386, DW_AT_name("_sprintf")
	.dwattr $C$DW$386, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2928| 
        ; call occurs [#_sprintf] ; [] |2928| 
	.dwpsn	file "../source/CommProtocol.c",line 2929,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2929 | UartDebugPrint((int *)DebugPrintBuf, 40);                              
;----------------------------------------------------------------------
        MOVB      AL,#40                ; [CPU_] |2929| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2929| 
$C$DW$387	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$387, DW_AT_low_pc(0x00)
	.dwattr $C$DW$387, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$387, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2929| 
        ; call occurs [#_UartDebugPrint] ; [] |2929| 
	.dwpsn	file "../source/CommProtocol.c",line 2930,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2930 | m_stPacketForTx.arrusnData[usnDataLength]= eRBC_FLOWCALIB_ABORTION;    
; 2931 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2930| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2930| 
        MOVB      *+XAR4[AR0],#83,UNC   ; [CPU_] |2930| 
	.dwpsn	file "../source/CommProtocol.c",line 2932,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2932 | usnDataLength++;                                                       
; 2934 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2932| 
	.dwpsn	file "../source/CommProtocol.c",line 2935,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2935 | utVolumetricErrorInfoReAcq.stVolumetricErrorReAcq.unRBCFlowCalibAbortio
;     | n = FALSE;                                                             
; 2937 | //DS_TESTING                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_utVolumetricErrorInfoReAcq ; [CPU_U] 
        AND       @_utVolumetricErrorInfoReAcq,#0x7fff ; [CPU_] |2935| 
$C$L258:    
	.dwpsn	file "../source/CommProtocol.c",line 2938,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2938 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail)         
; 2940 |     //!Set error for pressure sensor error                             
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       AL,@_utErrorInfoCheck+2,#0x1000 ; [CPU_] |2938| 
        LSR       AL,12                 ; [CPU_] |2938| 
        CMPB      AL,#1                 ; [CPU_] |2938| 
        B         $C$L259,NEQ           ; [CPU_] |2938| 
        ; branchcc occurs ; [] |2938| 
	.dwpsn	file "../source/CommProtocol.c",line 2941,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2941 | m_stPacketForTx.arrusnData[usnDataLength]= ePRESSURE_SENSOR_FAILURE;   
; 2942 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2941| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2941| 
        MOVB      *+XAR4[AR0],#70,UNC   ; [CPU_] |2941| 
	.dwpsn	file "../source/CommProtocol.c",line 2943,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2943 | usnDataLength++;                                                       
; 2945 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2943| 
	.dwpsn	file "../source/CommProtocol.c",line 2946,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2946 | utErrorInfoCheck.stErrorInfo.ulnPressureSensorFail = FALSE;            
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xefff ; [CPU_] |2946| 
$C$L259:    
	.dwpsn	file "../source/CommProtocol.c",line 2949,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2949 | if(TRUE == utErrorInfoCheck.stErrorInfo.PressureCalCmpltd)             
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+3,#0x0200 ; [CPU_] |2949| 
        LSR       AL,9                  ; [CPU_] |2949| 
        CMPB      AL,#1                 ; [CPU_] |2949| 
        B         $C$L260,NEQ           ; [CPU_] |2949| 
        ; branchcc occurs ; [] |2949| 
	.dwpsn	file "../source/CommProtocol.c",line 2951,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2951 | sprintf(statDebugPrintBuf, "\r\n PressureCalCmpltd sent to sitara  \n")
;     | ;                                                                      
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL18        ; [CPU_U] |2951| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2951| 
        MOVL      XAR4,#_statDebugPrintBuf ; [CPU_U] |2951| 
$C$DW$388	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$388, DW_AT_low_pc(0x00)
	.dwattr $C$DW$388, DW_AT_name("_sprintf")
	.dwattr $C$DW$388, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2951| 
        ; call occurs [#_sprintf] ; [] |2951| 
	.dwpsn	file "../source/CommProtocol.c",line 2952,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2952 | UartDebugPrint((int *)statDebugPrintBuf, 50);                          
; 2954 | //!Set Pressure calibration completed                                  
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2952| 
        MOVL      XAR4,#_statDebugPrintBuf ; [CPU_U] |2952| 
$C$DW$389	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$389, DW_AT_low_pc(0x00)
	.dwattr $C$DW$389, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$389, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2952| 
        ; call occurs [#_UartDebugPrint] ; [] |2952| 
	.dwpsn	file "../source/CommProtocol.c",line 2955,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2955 | m_stPacketForTx.arrusnData[usnDataLength]= ePRESSURE_CAL_CMPLTD;       
; 2956 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2955| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2955| 
        MOVB      *+XAR4[AR0],#81,UNC   ; [CPU_] |2955| 
	.dwpsn	file "../source/CommProtocol.c",line 2957,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2957 | usnDataLength++;                                                       
; 2959 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2957| 
	.dwpsn	file "../source/CommProtocol.c",line 2960,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2960 | utErrorInfoCheck.stErrorInfo.PressureCalCmpltd = FALSE;                
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+3 ; [CPU_U] 
        AND       @_utErrorInfoCheck+3,#0xfdff ; [CPU_] |2960| 
$C$L260:    
	.dwpsn	file "../source/CommProtocol.c",line 2962,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2962 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnLowVacuum)                  
; 2964 |     //!Set error for 5V not with in the range                          
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x2000 ; [CPU_] |2962| 
        LSR       AL,13                 ; [CPU_] |2962| 
        CMPB      AL,#1                 ; [CPU_] |2962| 
        B         $C$L261,NEQ           ; [CPU_] |2962| 
        ; branchcc occurs ; [] |2962| 
	.dwpsn	file "../source/CommProtocol.c",line 2965,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2965 | m_stPacketForTx.arrusnData[usnDataLength]= eHIGH_VACUUM;               
; 2966 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2965| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2965| 
        MOVB      *+XAR4[AR0],#71,UNC   ; [CPU_] |2965| 
	.dwpsn	file "../source/CommProtocol.c",line 2967,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2967 | usnDataLength++;                                                       
; 2969 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2967| 
	.dwpsn	file "../source/CommProtocol.c",line 2970,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2970 | utErrorInfoCheck.stErrorInfo.ulnLowVacuum = FALSE;                     
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xdfff ; [CPU_] |2970| 
$C$L261:    
	.dwpsn	file "../source/CommProtocol.c",line 2973,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2973 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnHighPressure)               
; 2975 |     //!Set error for 5V not with in the range                          
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+2,#0x4000 ; [CPU_] |2973| 
        LSR       AL,14                 ; [CPU_] |2973| 
        CMPB      AL,#1                 ; [CPU_] |2973| 
        B         $C$L262,NEQ           ; [CPU_] |2973| 
        ; branchcc occurs ; [] |2973| 
	.dwpsn	file "../source/CommProtocol.c",line 2976,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2976 | m_stPacketForTx.arrusnData[usnDataLength]= eHIGH_PRESSURE;             
; 2977 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2976| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2976| 
        MOVB      *+XAR4[AR0],#72,UNC   ; [CPU_] |2976| 
	.dwpsn	file "../source/CommProtocol.c",line 2978,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2978 | usnDataLength++;                                                       
; 2980 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2978| 
	.dwpsn	file "../source/CommProtocol.c",line 2981,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2981 | utErrorInfoCheck.stErrorInfo.ulnHighPressure = FALSE;                  
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+2,#0xbfff ; [CPU_] |2981| 
$C$L262:    
	.dwpsn	file "../source/CommProtocol.c",line 2984,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2984 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnY_MotorFault)               
; 2986 |     //!Set error for Y_MotorFault                                      
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+3,#0x0010 ; [CPU_] |2984| 
        LSR       AL,4                  ; [CPU_] |2984| 
        CMPB      AL,#1                 ; [CPU_] |2984| 
        B         $C$L263,NEQ           ; [CPU_] |2984| 
        ; branchcc occurs ; [] |2984| 
	.dwpsn	file "../source/CommProtocol.c",line 2987,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2987 | m_stPacketForTx.arrusnData[usnDataLength]= eY_MOTOR_FAULT;             
; 2988 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2987| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2987| 
        MOVB      *+XAR4[AR0],#74,UNC   ; [CPU_] |2987| 
	.dwpsn	file "../source/CommProtocol.c",line 2989,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2989 | usnDataLength++;                                                       
; 2991 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2989| 
	.dwpsn	file "../source/CommProtocol.c",line 2992,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2992 | utErrorInfoCheck.stErrorInfo.ulnY_MotorFault = FALSE;                  
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+3,#0xffef ; [CPU_] |2992| 
$C$L263:    
	.dwpsn	file "../source/CommProtocol.c",line 2994,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2994 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnX_MotorFault)               
; 2996 |     //!Set error for X_MotorFault                                      
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+3,#0x0020 ; [CPU_] |2994| 
        LSR       AL,5                  ; [CPU_] |2994| 
        CMPB      AL,#1                 ; [CPU_] |2994| 
        B         $C$L264,NEQ           ; [CPU_] |2994| 
        ; branchcc occurs ; [] |2994| 
	.dwpsn	file "../source/CommProtocol.c",line 2997,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2997 | m_stPacketForTx.arrusnData[usnDataLength]= eX_MOTOR_FAULT;             
; 2998 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |2997| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |2997| 
        MOVB      *+XAR4[AR0],#75,UNC   ; [CPU_] |2997| 
	.dwpsn	file "../source/CommProtocol.c",line 2999,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2999 | usnDataLength++;                                                       
; 3001 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |2999| 
	.dwpsn	file "../source/CommProtocol.c",line 3002,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3002 | utErrorInfoCheck.stErrorInfo.ulnX_MotorFault = FALSE;                  
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+3,#0xffdf ; [CPU_] |3002| 
$C$L264:    
	.dwpsn	file "../source/CommProtocol.c",line 3004,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3004 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnDiluent_MotorFault)         
; 3006 |     //!Set error for Diluent_MotorFault                                
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+3,#0x0040 ; [CPU_] |3004| 
        LSR       AL,6                  ; [CPU_] |3004| 
        CMPB      AL,#1                 ; [CPU_] |3004| 
        B         $C$L265,NEQ           ; [CPU_] |3004| 
        ; branchcc occurs ; [] |3004| 
	.dwpsn	file "../source/CommProtocol.c",line 3007,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3007 | m_stPacketForTx.arrusnData[usnDataLength]= eDILUENT_MOTOR_FAULT;       
; 3008 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |3007| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |3007| 
        MOVB      *+XAR4[AR0],#76,UNC   ; [CPU_] |3007| 
	.dwpsn	file "../source/CommProtocol.c",line 3009,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3009 | usnDataLength++;                                                       
; 3011 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |3009| 
	.dwpsn	file "../source/CommProtocol.c",line 3012,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3012 | utErrorInfoCheck.stErrorInfo.ulnDiluent_MotorFault = FALSE;            
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+3,#0xffbf ; [CPU_] |3012| 
$C$L265:    
	.dwpsn	file "../source/CommProtocol.c",line 3014,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3014 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnSample_MotorFault)          
; 3016 |     //!Set error for Sample_MotorFault                                 
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+3,#0x0080 ; [CPU_] |3014| 
        LSR       AL,7                  ; [CPU_] |3014| 
        CMPB      AL,#1                 ; [CPU_] |3014| 
        B         $C$L266,NEQ           ; [CPU_] |3014| 
        ; branchcc occurs ; [] |3014| 
	.dwpsn	file "../source/CommProtocol.c",line 3017,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3017 | m_stPacketForTx.arrusnData[usnDataLength]= eSAMPLE_MOTOR_FAULT;        
; 3018 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |3017| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |3017| 
        MOVB      *+XAR4[AR0],#77,UNC   ; [CPU_] |3017| 
	.dwpsn	file "../source/CommProtocol.c",line 3019,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3019 | usnDataLength++;                                                       
; 3021 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |3019| 
	.dwpsn	file "../source/CommProtocol.c",line 3022,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3022 | utErrorInfoCheck.stErrorInfo.ulnSample_MotorFault = FALSE;             
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+3,#0xff7f ; [CPU_] |3022| 
$C$L266:    
	.dwpsn	file "../source/CommProtocol.c",line 3024,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3024 | if(TRUE == utErrorInfoCheck.stErrorInfo.ulnWaste_MotorFault)           
; 3026 |     //!Set error for Waste_MotorFault                                  
;----------------------------------------------------------------------
        AND       AL,@_utErrorInfoCheck+3,#0x0100 ; [CPU_] |3024| 
        LSR       AL,8                  ; [CPU_] |3024| 
        CMPB      AL,#1                 ; [CPU_] |3024| 
        B         $C$L267,NEQ           ; [CPU_] |3024| 
        ; branchcc occurs ; [] |3024| 
	.dwpsn	file "../source/CommProtocol.c",line 3027,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3027 | m_stPacketForTx.arrusnData[usnDataLength]= eWASTE_MOTOR_FAULT;         
; 3028 | //!Increment the data length                                           
;----------------------------------------------------------------------
        MOVZ      AR0,*-SP[5]           ; [CPU_] |3027| 
        MOVL      XAR4,#_m_stPacketForTx+6 ; [CPU_U] |3027| 
        MOVB      *+XAR4[AR0],#78,UNC   ; [CPU_] |3027| 
	.dwpsn	file "../source/CommProtocol.c",line 3029,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3029 | usnDataLength++;                                                       
; 3031 | //!Rest the error after sending the error to Sitara                    
;----------------------------------------------------------------------
        INC       *-SP[5]               ; [CPU_] |3029| 
	.dwpsn	file "../source/CommProtocol.c",line 3032,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3032 | utErrorInfoCheck.stErrorInfo.ulnWaste_MotorFault = FALSE;              
; 3035 | //utErrorInfoCheck.ulnErrorInfo = 0;                                   
; 3036 | //!Since it is single packet, packet count is zero                     
;----------------------------------------------------------------------
        AND       @_utErrorInfoCheck+3,#0xfeff ; [CPU_] |3032| 
$C$L267:    
	.dwpsn	file "../source/CommProtocol.c",line 3037,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3037 | m_usnPacketCount = 0;                                                  
; 3038 | //!Return the data length                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3037| 
	.dwpsn	file "../source/CommProtocol.c",line 3039,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3039 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |3039| 
	.dwpsn	file "../source/CommProtocol.c",line 3040,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$390	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$390, DW_AT_low_pc(0x00)
	.dwattr $C$DW$390, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$360, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$360, DW_AT_TI_end_line(0xbe0)
	.dwattr $C$DW$360, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$360

	.sect	".text:_CPWriteSystemSettingDataToPacket"
	.clink
	.global	_CPWriteSystemSettingDataToPacket

$C$DW$391	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$391, DW_AT_name("CPWriteSystemSettingDataToPacket")
	.dwattr $C$DW$391, DW_AT_low_pc(_CPWriteSystemSettingDataToPacket)
	.dwattr $C$DW$391, DW_AT_high_pc(0x00)
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_CPWriteSystemSettingDataToPacket")
	.dwattr $C$DW$391, DW_AT_external
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$391, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$391, DW_AT_TI_begin_line(0xbeb)
	.dwattr $C$DW$391, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$391, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommProtocol.c",line 3053,column 1,is_stmt,address _CPWriteSystemSettingDataToPacket,isa 0

	.dwfde $C$DW$CIE, _CPWriteSystemSettingDataToPacket
$C$DW$392	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$392, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$392, DW_AT_location[DW_OP_reg12]

$C$DW$393	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$393, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$393, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 3051 | uint16_t CPWriteSystemSettingDataToPacket(ST_SPI_PACKET_FRAME *stPtrPar
;     | sedPacket,\                                                            
; 3052 | bool_t bNAKReceived)                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWriteSystemSettingDataToPacket FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CPWriteSystemSettingDataToPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$394	.dwtag  DW_TAG_variable
	.dwattr $C$DW$394, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$394, DW_AT_location[DW_OP_breg20 -2]

$C$DW$395	.dwtag  DW_TAG_variable
	.dwattr $C$DW$395, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$395, DW_AT_location[DW_OP_breg20 -3]

$C$DW$396	.dwtag  DW_TAG_variable
	.dwattr $C$DW$396, DW_AT_name("usnDataLength")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$396, DW_AT_location[DW_OP_breg20 -4]

$C$DW$397	.dwtag  DW_TAG_variable
	.dwattr $C$DW$397, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$397, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 3054 | //!Initialize data length of the packet to zero                        
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |3053| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3053| 
	.dwpsn	file "../source/CommProtocol.c",line 3055,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 3055 | uint16_t usnDataLength = ZERO;                                         
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |3055| 
	.dwpsn	file "../source/CommProtocol.c",line 3056,column 24,is_stmt,isa 0
;----------------------------------------------------------------------
; 3056 | uint16_t usnSubCmd = ZERO;                                             
; 3058 | //!Get the minor command from the structure                            
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |3056| 
	.dwpsn	file "../source/CommProtocol.c",line 3059,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3059 | usnSubCmd = stPtrParsedPacket->usnMinorCommand;                        
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[5]          ; [CPU_] |3059| 
        MOV       *-SP[5],AL            ; [CPU_] |3059| 
	.dwpsn	file "../source/CommProtocol.c",line 3060,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3060 | switch(usnSubCmd)                                                      
; 3062 |     //!If the minor command is to abort the process then perform the be
;     | low operations                                                         
; 3063 |     case DELFINO_FLOW_CALIBRATION_CMD:                                 
; 3064 |         //!Get the counting time for WBC and RBC and update the data le
;     | ngth                                                                   
;----------------------------------------------------------------------
        B         $C$L274,UNC           ; [CPU_] |3060| 
        ; branch occurs ; [] |3060| 
$C$L268:    
	.dwpsn	file "../source/CommProtocol.c",line 3065,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3065 | usnDataLength = SysInitGetCountingTime(stPtrParsedPacket->arrusnData); 
; 3066 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      ACC,#6                ; [CPU_] |3065| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |3065| 
        MOVL      XAR4,ACC              ; [CPU_] |3065| 
$C$DW$398	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$398, DW_AT_low_pc(0x00)
	.dwattr $C$DW$398, DW_AT_name("_SysInitGetCountingTime")
	.dwattr $C$DW$398, DW_AT_TI_call

        LCR       #_SysInitGetCountingTime ; [CPU_] |3065| 
        ; call occurs [#_SysInitGetCountingTime] ; [] |3065| 
        MOV       *-SP[4],AL            ; [CPU_] |3065| 
	.dwpsn	file "../source/CommProtocol.c",line 3067,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3067 | m_usnPacketCount = ZERO;                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3067| 
	.dwpsn	file "../source/CommProtocol.c",line 3068,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3068 | break;                                                                 
; 3070 | case DELFINO_FLOW_CALIBRATION_STARTED:                                 
;----------------------------------------------------------------------
        B         $C$L275,UNC           ; [CPU_] |3068| 
        ; branch occurs ; [] |3068| 
$C$L269:    
	.dwpsn	file "../source/CommProtocol.c",line 3071,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3071 | usnDataLength = ZERO;                                                  
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |3071| 
	.dwpsn	file "../source/CommProtocol.c",line 3072,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3072 | m_usnPacketCount = ZERO;                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3072| 
	.dwpsn	file "../source/CommProtocol.c",line 3073,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3073 | break;                                                                 
; 3075 | case DELFINO_RINSE_PRIME_COMPLETED:                                    
;----------------------------------------------------------------------
        B         $C$L275,UNC           ; [CPU_] |3073| 
        ; branch occurs ; [] |3073| 
$C$L270:    
	.dwpsn	file "../source/CommProtocol.c",line 3076,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3076 | usnDataLength = ZERO;                                                  
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |3076| 
	.dwpsn	file "../source/CommProtocol.c",line 3077,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3077 | m_usnPacketCount = ZERO;                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3077| 
	.dwpsn	file "../source/CommProtocol.c",line 3078,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3078 | break;                                                                 
; 3079 | case DELFINO_PRESSURE_CALIBRATED_DATA:                                 
; 3080 | //!Set the pressure calibrated value to the data packet                
;----------------------------------------------------------------------
        B         $C$L275,UNC           ; [CPU_] |3078| 
        ; branch occurs ; [] |3078| 
$C$L271:    
	.dwpsn	file "../source/CommProtocol.c",line 3081,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3081 | if(sncalZeroPSI > ZERO)                                                
; 3083 |     //!SPI configuration is only done for positive numbers txRx        
; 3084 |     //! so we should send only positive numbers because of that here   
; 3085 |     //! we are sending absolute value and Sign value                   
;----------------------------------------------------------------------
        MOVW      DP,#_sncalZeroPSI     ; [CPU_U] 
        MOVL      ACC,@_sncalZeroPSI    ; [CPU_] |3081| 
        B         $C$L272,EQ            ; [CPU_] |3081| 
        ; branchcc occurs ; [] |3081| 
	.dwpsn	file "../source/CommProtocol.c",line 3086,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3086 | stPtrParsedPacket->arrusnData[0] = sncalZeroPSI;   //Hn_added          
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3086| 
        MOV       AL,@_sncalZeroPSI     ; [CPU_] |3086| 
        MOV       *+XAR4[6],AL          ; [CPU_] |3086| 
	.dwpsn	file "../source/CommProtocol.c",line 3087,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3087 | stPtrParsedPacket->arrusnData[1] = 0;   //Positive          //Hn_added 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3087| 
        MOV       *+XAR4[7],#0          ; [CPU_] |3087| 
	.dwpsn	file "../source/CommProtocol.c",line 3088,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3089 | else                                                                   
; 3091 |     //! SPI configuration is only done for positive numbers txRx       
; 3092 |     //! so we should send only positive numbers because of that here   
; 3093 |     //! we are sending absolute value and Sign value                   
;----------------------------------------------------------------------
        B         $C$L273,UNC           ; [CPU_] |3088| 
        ; branch occurs ; [] |3088| 
$C$L272:    
	.dwpsn	file "../source/CommProtocol.c",line 3094,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3094 | stPtrParsedPacket->arrusnData[0] = (sncalZeroPSI * -1);  //Hn_added    
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3094| 
        MOV       AL,@_sncalZeroPSI     ; [CPU_] |3094| 
        NEG       AL                    ; [CPU_] |3094| 
        MOV       *+XAR4[6],AL          ; [CPU_] |3094| 
	.dwpsn	file "../source/CommProtocol.c",line 3095,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3095 | stPtrParsedPacket->arrusnData[1] = 1;  //negative          //Hn_added  
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3095| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |3095| 
$C$L273:    
	.dwpsn	file "../source/CommProtocol.c",line 3099,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3099 | usnDataLength = 2;                                                     
;----------------------------------------------------------------------
        MOVB      *-SP[4],#2,UNC        ; [CPU_] |3099| 
	.dwpsn	file "../source/CommProtocol.c",line 3100,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3100 | m_usnPacketCount = ZERO;                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3100| 
	.dwpsn	file "../source/CommProtocol.c",line 3101,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3101 | break;                                                                 
; 3102 | default:                                                               
; 3103 | break;                                                                 
; 3105 | }//end of switch case                                                  
; 3107 | //!Return the data length                                              
;----------------------------------------------------------------------
        B         $C$L275,UNC           ; [CPU_] |3101| 
        ; branch occurs ; [] |3101| 
$C$L274:    
	.dwpsn	file "../source/CommProtocol.c",line 3060,column 5,is_stmt,isa 0
        MOVZ      AR6,*-SP[5]           ; [CPU_] |3060| 
        MOVZ      AR7,AR6               ; [CPU_] |3060| 
        MOVL      XAR4,#8193            ; [CPU_U] |3060| 
        MOVL      ACC,XAR4              ; [CPU_] |3060| 
        CMPL      ACC,XAR7              ; [CPU_] |3060| 
        B         $C$L270,EQ            ; [CPU_] |3060| 
        ; branchcc occurs ; [] |3060| 
        MOVZ      AR7,AR6               ; [CPU_] |3060| 
        MOVL      XAR4,#8194            ; [CPU_U] |3060| 
        MOVL      ACC,XAR4              ; [CPU_] |3060| 
        CMPL      ACC,XAR7              ; [CPU_] |3060| 
        B         $C$L269,EQ            ; [CPU_] |3060| 
        ; branchcc occurs ; [] |3060| 
        MOVZ      AR7,AR6               ; [CPU_] |3060| 
        MOVL      XAR4,#8195            ; [CPU_U] |3060| 
        MOVL      ACC,XAR4              ; [CPU_] |3060| 
        CMPL      ACC,XAR7              ; [CPU_] |3060| 
        B         $C$L268,EQ            ; [CPU_] |3060| 
        ; branchcc occurs ; [] |3060| 
        MOVZ      AR6,AR6               ; [CPU_] |3060| 
        MOVL      XAR4,#8196            ; [CPU_U] |3060| 
        MOVL      ACC,XAR4              ; [CPU_] |3060| 
        CMPL      ACC,XAR6              ; [CPU_] |3060| 
        B         $C$L271,EQ            ; [CPU_] |3060| 
        ; branchcc occurs ; [] |3060| 
$C$L275:    
	.dwpsn	file "../source/CommProtocol.c",line 3108,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3108 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |3108| 
	.dwpsn	file "../source/CommProtocol.c",line 3109,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$399	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$399, DW_AT_low_pc(0x00)
	.dwattr $C$DW$399, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$391, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$391, DW_AT_TI_end_line(0xc25)
	.dwattr $C$DW$391, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$391

	.sect	".text:_CPWriteSTPHDataToPacket"
	.clink
	.global	_CPWriteSTPHDataToPacket

$C$DW$400	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$400, DW_AT_name("CPWriteSTPHDataToPacket")
	.dwattr $C$DW$400, DW_AT_low_pc(_CPWriteSTPHDataToPacket)
	.dwattr $C$DW$400, DW_AT_high_pc(0x00)
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_CPWriteSTPHDataToPacket")
	.dwattr $C$DW$400, DW_AT_external
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$400, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$400, DW_AT_TI_begin_line(0xc30)
	.dwattr $C$DW$400, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$400, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommProtocol.c",line 3122,column 1,is_stmt,address _CPWriteSTPHDataToPacket,isa 0

	.dwfde $C$DW$CIE, _CPWriteSTPHDataToPacket
$C$DW$401	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$401, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$401, DW_AT_location[DW_OP_reg12]

$C$DW$402	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$402, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$402, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 3120 | uint16_t CPWriteSTPHDataToPacket(ST_SPI_PACKET_FRAME *stPtrParsedPacket
;     | ,\                                                                     
; 3121 | bool_t bNAKReceived)                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPWriteSTPHDataToPacket      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CPWriteSTPHDataToPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$403	.dwtag  DW_TAG_variable
	.dwattr $C$DW$403, DW_AT_name("stPtrParsedPacket")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_stPtrParsedPacket")
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$403, DW_AT_location[DW_OP_breg20 -2]

$C$DW$404	.dwtag  DW_TAG_variable
	.dwattr $C$DW$404, DW_AT_name("bNAKReceived")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_bNAKReceived")
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$404, DW_AT_location[DW_OP_breg20 -3]

$C$DW$405	.dwtag  DW_TAG_variable
	.dwattr $C$DW$405, DW_AT_name("usnDataLength")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$405, DW_AT_location[DW_OP_breg20 -4]

$C$DW$406	.dwtag  DW_TAG_variable
	.dwattr $C$DW$406, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$406, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 3124 | //!Set data length as zero by default                                  
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |3122| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3122| 
	.dwpsn	file "../source/CommProtocol.c",line 3125,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 3125 | uint16_t usnDataLength = 0;                                            
; 3126 | //!Set minor/sub command as zero by default                            
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |3125| 
	.dwpsn	file "../source/CommProtocol.c",line 3127,column 24,is_stmt,isa 0
;----------------------------------------------------------------------
; 3127 | uint16_t usnSubCmd =0;                                                 
; 3129 | //!Get the minor command from the structure to update the data         
;----------------------------------------------------------------------
        MOV       *-SP[5],#0            ; [CPU_] |3127| 
	.dwpsn	file "../source/CommProtocol.c",line 3130,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3130 | usnSubCmd = stPtrParsedPacket->usnMinorCommand;                        
; 3132 | //union UT_MOTOR_HOME_POSITION utMotorHomePositionCheck;               
; 3134 | //!Check for the minor/sub command                                     
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[5]          ; [CPU_] |3130| 
        MOV       *-SP[5],AL            ; [CPU_] |3130| 
	.dwpsn	file "../source/CommProtocol.c",line 3135,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3135 | switch(usnSubCmd)                                                      
; 3137 |     case DELFINO_ERROR_MINOR_CMD:                                      
;----------------------------------------------------------------------
        B         $C$L307,UNC           ; [CPU_] |3135| 
        ; branch occurs ; [] |3135| 
$C$L276:    
	.dwpsn	file "../source/CommProtocol.c",line 3138,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3138 | usnDataLength = CPGetErrorData(stPtrParsedPacket);                     
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3138| 
$C$DW$407	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$407, DW_AT_low_pc(0x00)
	.dwattr $C$DW$407, DW_AT_name("_CPGetErrorData")
	.dwattr $C$DW$407, DW_AT_TI_call

        LCR       #_CPGetErrorData      ; [CPU_] |3138| 
        ; call occurs [#_CPGetErrorData] ; [] |3138| 
        MOV       *-SP[4],AL            ; [CPU_] |3138| 
	.dwpsn	file "../source/CommProtocol.c",line 3139,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3139 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3139| 
	.dwpsn	file "../source/CommProtocol.c",line 3140,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3140 | break;                                                                 
; 3142 | //case DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD:                         
; 3143 | //case DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD:                          
; 3144 | //case DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD:                       
; 3145 | case DELFINO_PRIME_ALL_COMPLETED_CMD:                                  
; 3146 | case DELFINO_BACK_FLUSH_COMPLETED_CMD:                                 
; 3147 | //case DELFINO_PRIME_WITH_EZ_CLEANSER_CMD:                             
; 3148 | //case DELFINO_ZAP_APERTURE_COMPLETED_CMD:                             
; 3149 | //case DELFINO_DRAIN_BATH_COMPLETED_CMD:                               
; 3150 | //case DELFINO_DRAIN_ALL_COMPLETED_CMD:                                
; 3151 | //case DELFINO_CLEAN_BATH_COMPLETED_CMD:                               
; 3152 | //case DELFINO_START_UP_SEQUENCE_COMPLETED_CMD:                        
; 3153 | //case DELFINO_HEAD_RINSING_COMPLETED_CMD:                             
; 3154 | //case DELFINO_PROBE_CLEANING_COMPLETED_CMD:                           
; 3156 | //!Set data length and packet count to zero for the above sequence     
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3140| 
        ; branch occurs ; [] |3140| 
$C$L277:    
	.dwpsn	file "../source/CommProtocol.c",line 3157,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3157 | usnDataLength = 0;                                                     
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |3157| 
	.dwpsn	file "../source/CommProtocol.c",line 3158,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3158 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3158| 
	.dwpsn	file "../source/CommProtocol.c",line 3160,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3160 | break;                                                                 
; 3163 | case DELFINO_SYSTEM_STATUS_DATA:                                       
; 3165 |     //!Set the data for System health status packet                    
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3160| 
        ; branch occurs ; [] |3160| 
$C$L278:    
	.dwpsn	file "../source/CommProtocol.c",line 3166,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3166 | SysInitSystemHealthData(stPtrParsedPacket->arrusnData);                
; 3167 | //!Set the data length according to system health packet               
;----------------------------------------------------------------------
        MOVB      ACC,#6                ; [CPU_] |3166| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |3166| 
        MOVL      XAR4,ACC              ; [CPU_] |3166| 
$C$DW$408	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$408, DW_AT_low_pc(0x00)
	.dwattr $C$DW$408, DW_AT_name("_SysInitSystemHealthData")
	.dwattr $C$DW$408, DW_AT_TI_call

        LCR       #_SysInitSystemHealthData ; [CPU_] |3166| 
        ; call occurs [#_SysInitSystemHealthData] ; [] |3166| 
	.dwpsn	file "../source/CommProtocol.c",line 3168,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3168 | usnDataLength = CP_SYSTEM_STATUS_LENGTH;                               
; 3169 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[4],#6,UNC        ; [CPU_] |3168| 
	.dwpsn	file "../source/CommProtocol.c",line 3170,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3170 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3170| 
	.dwpsn	file "../source/CommProtocol.c",line 3172,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3172 | break;                                                                 
; 3174 | case DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD:                           
; 3176 |     //!If the motor check status is success,                           
; 3177 |     //! then set the success info in the data field, else set motor    
; 3178 |     //! failure status in data filed                                   
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3172| 
        ; branch occurs ; [] |3172| 
$C$L279:    
	.dwpsn	file "../source/CommProtocol.c",line 3179,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3179 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |3179| 
        CMPB      AL,#1                 ; [CPU_] |3179| 
        B         $C$L280,NEQ           ; [CPU_] |3179| 
        ; branchcc occurs ; [] |3179| 
	.dwpsn	file "../source/CommProtocol.c",line 3181,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3181 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
; 3182 | //!Reset the motor check status flag                                   
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3181| 
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |3181| 
	.dwpsn	file "../source/CommProtocol.c",line 3183,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3183 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |3183| 
	.dwpsn	file "../source/CommProtocol.c",line 3184,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3185 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L281,UNC           ; [CPU_] |3184| 
        ; branch occurs ; [] |3184| 
$C$L280:    
	.dwpsn	file "../source/CommProtocol.c",line 3187,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3187 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 3190 | //!Get the home position check for the motors and update the data      
; 3191 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3187| 
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |3187| 
$C$L281:    
	.dwpsn	file "../source/CommProtocol.c",line 3192,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3192 | if(TRUE == OIHomeSensor4State())                                       
;----------------------------------------------------------------------
$C$DW$409	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$409, DW_AT_low_pc(0x00)
	.dwattr $C$DW$409, DW_AT_name("_OIHomeSensor4State")
	.dwattr $C$DW$409, DW_AT_TI_call

        LCR       #_OIHomeSensor4State  ; [CPU_] |3192| 
        ; call occurs [#_OIHomeSensor4State] ; [] |3192| 
        CMPB      AL,#1                 ; [CPU_] |3192| 
        B         $C$L282,NEQ           ; [CPU_] |3192| 
        ; branchcc occurs ; [] |3192| 
	.dwpsn	file "../source/CommProtocol.c",line 3194,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3194 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3194| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |3194| 
	.dwpsn	file "../source/CommProtocol.c",line 3195,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3196 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L283,UNC           ; [CPU_] |3195| 
        ; branch occurs ; [] |3195| 
$C$L282:    
	.dwpsn	file "../source/CommProtocol.c",line 3198,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3198 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 3200 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3198| 
        MOV       *+XAR4[7],#0          ; [CPU_] |3198| 
$C$L283:    
	.dwpsn	file "../source/CommProtocol.c",line 3201,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3201 | usnDataLength = 2;                                                     
; 3202 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[4],#2,UNC        ; [CPU_] |3201| 
	.dwpsn	file "../source/CommProtocol.c",line 3203,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3203 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3203| 
	.dwpsn	file "../source/CommProtocol.c",line 3205,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3205 | break;                                                                 
; 3207 | case DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD:                           
; 3209 |     //!If the motor check status is success,                           
; 3210 |     //! then set the success info in the data field, else set motor    
; 3211 |     //! failure status in data filed                                   
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3205| 
        ; branch occurs ; [] |3205| 
$C$L284:    
	.dwpsn	file "../source/CommProtocol.c",line 3212,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3212 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |3212| 
        CMPB      AL,#1                 ; [CPU_] |3212| 
        B         $C$L285,NEQ           ; [CPU_] |3212| 
        ; branchcc occurs ; [] |3212| 
	.dwpsn	file "../source/CommProtocol.c",line 3214,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3214 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3214| 
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |3214| 
	.dwpsn	file "../source/CommProtocol.c",line 3215,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3215 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |3215| 
	.dwpsn	file "../source/CommProtocol.c",line 3216,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3217 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L286,UNC           ; [CPU_] |3216| 
        ; branch occurs ; [] |3216| 
$C$L285:    
	.dwpsn	file "../source/CommProtocol.c",line 3219,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3219 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 3221 | //!Get the home position check for the motors and update the data      
; 3222 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3219| 
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |3219| 
$C$L286:    
	.dwpsn	file "../source/CommProtocol.c",line 3223,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3223 | if(TRUE == OIHomeSensor5State())                                       
;----------------------------------------------------------------------
$C$DW$410	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$410, DW_AT_low_pc(0x00)
	.dwattr $C$DW$410, DW_AT_name("_OIHomeSensor5State")
	.dwattr $C$DW$410, DW_AT_TI_call

        LCR       #_OIHomeSensor5State  ; [CPU_] |3223| 
        ; call occurs [#_OIHomeSensor5State] ; [] |3223| 
        CMPB      AL,#1                 ; [CPU_] |3223| 
        B         $C$L287,NEQ           ; [CPU_] |3223| 
        ; branchcc occurs ; [] |3223| 
	.dwpsn	file "../source/CommProtocol.c",line 3225,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3225 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3225| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |3225| 
	.dwpsn	file "../source/CommProtocol.c",line 3226,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3227 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L288,UNC           ; [CPU_] |3226| 
        ; branch occurs ; [] |3226| 
$C$L287:    
	.dwpsn	file "../source/CommProtocol.c",line 3229,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3229 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 3231 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3229| 
        MOV       *+XAR4[7],#0          ; [CPU_] |3229| 
$C$L288:    
	.dwpsn	file "../source/CommProtocol.c",line 3232,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3232 | usnDataLength = 2;                                                     
; 3233 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[4],#2,UNC        ; [CPU_] |3232| 
	.dwpsn	file "../source/CommProtocol.c",line 3234,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3234 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3234| 
	.dwpsn	file "../source/CommProtocol.c",line 3236,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3236 | break;                                                                 
; 3238 | case DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD:                  
; 3240 |     //!If the motor check status is success,                           
; 3241 |     //! then set the success info in the data field, else set motor    
; 3242 |     //! failure status in data filed                                   
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3236| 
        ; branch occurs ; [] |3236| 
$C$L289:    
	.dwpsn	file "../source/CommProtocol.c",line 3243,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3243 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |3243| 
        CMPB      AL,#1                 ; [CPU_] |3243| 
        B         $C$L290,NEQ           ; [CPU_] |3243| 
        ; branchcc occurs ; [] |3243| 
	.dwpsn	file "../source/CommProtocol.c",line 3245,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3245 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |3245| 
	.dwpsn	file "../source/CommProtocol.c",line 3246,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3246 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |3246| 
	.dwpsn	file "../source/CommProtocol.c",line 3247,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3248 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L291,UNC           ; [CPU_] |3247| 
        ; branch occurs ; [] |3247| 
$C$L290:    
	.dwpsn	file "../source/CommProtocol.c",line 3250,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3250 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 3252 | //!Get the home position check for the motors and update the data      
; 3253 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |3250| 
$C$L291:    
	.dwpsn	file "../source/CommProtocol.c",line 3254,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3254 | if(TRUE == OIHomeSensor2State())                                       
;----------------------------------------------------------------------
$C$DW$411	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$411, DW_AT_low_pc(0x00)
	.dwattr $C$DW$411, DW_AT_name("_OIHomeSensor2State")
	.dwattr $C$DW$411, DW_AT_TI_call

        LCR       #_OIHomeSensor2State  ; [CPU_] |3254| 
        ; call occurs [#_OIHomeSensor2State] ; [] |3254| 
        CMPB      AL,#1                 ; [CPU_] |3254| 
        B         $C$L292,NEQ           ; [CPU_] |3254| 
        ; branchcc occurs ; [] |3254| 
	.dwpsn	file "../source/CommProtocol.c",line 3256,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3256 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3256| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |3256| 
	.dwpsn	file "../source/CommProtocol.c",line 3257,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3258 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3257| 
        ; branch occurs ; [] |3257| 
$C$L292:    
	.dwpsn	file "../source/CommProtocol.c",line 3260,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3260 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 3262 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3260| 
        MOV       *+XAR4[7],#0          ; [CPU_] |3260| 
$C$L293:    
	.dwpsn	file "../source/CommProtocol.c",line 3263,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3263 | usnDataLength = 2;                                                     
; 3264 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[4],#2,UNC        ; [CPU_] |3263| 
	.dwpsn	file "../source/CommProtocol.c",line 3265,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3265 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3265| 
	.dwpsn	file "../source/CommProtocol.c",line 3267,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3267 | break;                                                                 
; 3269 | case DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD:                    
; 3271 |     //!If the motor check status is success,                           
; 3272 |     //! then set the success info in the data field, else set motor    
; 3273 |     //! failure status in data filed                                   
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3267| 
        ; branch occurs ; [] |3267| 
$C$L294:    
	.dwpsn	file "../source/CommProtocol.c",line 3274,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3274 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |3274| 
        CMPB      AL,#1                 ; [CPU_] |3274| 
        B         $C$L295,NEQ           ; [CPU_] |3274| 
        ; branchcc occurs ; [] |3274| 
	.dwpsn	file "../source/CommProtocol.c",line 3276,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3276 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3276| 
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |3276| 
	.dwpsn	file "../source/CommProtocol.c",line 3277,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3277 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |3277| 
	.dwpsn	file "../source/CommProtocol.c",line 3278,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3279 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L296,UNC           ; [CPU_] |3278| 
        ; branch occurs ; [] |3278| 
$C$L295:    
	.dwpsn	file "../source/CommProtocol.c",line 3281,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3281 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 3283 | //!Get the home position check for the motors and update the data      
; 3284 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3281| 
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |3281| 
$C$L296:    
	.dwpsn	file "../source/CommProtocol.c",line 3285,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3285 | if(TRUE == OIHomeSensor1State())                                       
;----------------------------------------------------------------------
$C$DW$412	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$412, DW_AT_low_pc(0x00)
	.dwattr $C$DW$412, DW_AT_name("_OIHomeSensor1State")
	.dwattr $C$DW$412, DW_AT_TI_call

        LCR       #_OIHomeSensor1State  ; [CPU_] |3285| 
        ; call occurs [#_OIHomeSensor1State] ; [] |3285| 
        CMPB      AL,#1                 ; [CPU_] |3285| 
        B         $C$L297,NEQ           ; [CPU_] |3285| 
        ; branchcc occurs ; [] |3285| 
	.dwpsn	file "../source/CommProtocol.c",line 3287,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3287 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3287| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |3287| 
	.dwpsn	file "../source/CommProtocol.c",line 3288,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3289 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L298,UNC           ; [CPU_] |3288| 
        ; branch occurs ; [] |3288| 
$C$L297:    
	.dwpsn	file "../source/CommProtocol.c",line 3291,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3291 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 3293 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3291| 
        MOV       *+XAR4[7],#0          ; [CPU_] |3291| 
$C$L298:    
	.dwpsn	file "../source/CommProtocol.c",line 3294,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3294 | usnDataLength = 2;                                                     
; 3295 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[4],#2,UNC        ; [CPU_] |3294| 
	.dwpsn	file "../source/CommProtocol.c",line 3296,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3296 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3296| 
	.dwpsn	file "../source/CommProtocol.c",line 3298,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3298 | break;                                                                 
; 3300 | case DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD:              
; 3302 |     //!If the motor check status is success,                           
; 3303 |     //! then set the success info in the data field, else set motor    
; 3304 |     //! failure status in data filed                                   
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3298| 
        ; branch occurs ; [] |3298| 
$C$L299:    
	.dwpsn	file "../source/CommProtocol.c",line 3305,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3305 | if(g_bMotorChkStatus == TRUE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMotorChkStatus ; [CPU_U] 
        MOV       AL,@_g_bMotorChkStatus ; [CPU_] |3305| 
        CMPB      AL,#1                 ; [CPU_] |3305| 
        B         $C$L300,NEQ           ; [CPU_] |3305| 
        ; branchcc occurs ; [] |3305| 
	.dwpsn	file "../source/CommProtocol.c",line 3307,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3307 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_SUCCESS;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3307| 
        MOVB      *+XAR4[6],#1,UNC      ; [CPU_] |3307| 
	.dwpsn	file "../source/CommProtocol.c",line 3308,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3308 | g_bMotorChkStatus = FALSE;                                             
;----------------------------------------------------------------------
        MOV       @_g_bMotorChkStatus,#0 ; [CPU_] |3308| 
	.dwpsn	file "../source/CommProtocol.c",line 3309,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3310 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L301,UNC           ; [CPU_] |3309| 
        ; branch occurs ; [] |3309| 
$C$L300:    
	.dwpsn	file "../source/CommProtocol.c",line 3312,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3312 | stPtrParsedPacket->arrusnData[0] = MOTOR_CHECK_FAILURE;                
; 3314 | //!Get the home position check for the motors and update the data      
; 3315 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3312| 
        MOVB      *+XAR4[6],#2,UNC      ; [CPU_] |3312| 
$C$L301:    
	.dwpsn	file "../source/CommProtocol.c",line 3316,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3316 | if(TRUE == OIHomeSensor3State())                                       
;----------------------------------------------------------------------
$C$DW$413	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$413, DW_AT_low_pc(0x00)
	.dwattr $C$DW$413, DW_AT_name("_OIHomeSensor3State")
	.dwattr $C$DW$413, DW_AT_TI_call

        LCR       #_OIHomeSensor3State  ; [CPU_] |3316| 
        ; call occurs [#_OIHomeSensor3State] ; [] |3316| 
        CMPB      AL,#1                 ; [CPU_] |3316| 
        B         $C$L302,NEQ           ; [CPU_] |3316| 
        ; branchcc occurs ; [] |3316| 
	.dwpsn	file "../source/CommProtocol.c",line 3318,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3318 | stPtrParsedPacket->arrusnData[1] = MOTOR_HOME_POSITION;                
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3318| 
        MOVB      *+XAR4[7],#1,UNC      ; [CPU_] |3318| 
	.dwpsn	file "../source/CommProtocol.c",line 3319,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3320 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L303,UNC           ; [CPU_] |3319| 
        ; branch occurs ; [] |3319| 
$C$L302:    
	.dwpsn	file "../source/CommProtocol.c",line 3322,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3322 | stPtrParsedPacket->arrusnData[1] = MOTOR_NOT_HOME_POSITION;            
; 3324 | //!Set the data length                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[2]          ; [CPU_] |3322| 
        MOV       *+XAR4[7],#0          ; [CPU_] |3322| 
$C$L303:    
	.dwpsn	file "../source/CommProtocol.c",line 3325,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3325 | usnDataLength = 2;                                                     
; 3326 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOVB      *-SP[4],#2,UNC        ; [CPU_] |3325| 
	.dwpsn	file "../source/CommProtocol.c",line 3327,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3327 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3327| 
	.dwpsn	file "../source/CommProtocol.c",line 3329,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3329 | break;                                                                 
; 3331 | //case DELFINO_PRIME_WITH_RINSE_STARTED_CMD:                           
; 3332 | //case DELFINO_PRIME_WITH_LYSE_STARTED_CMD:                            
; 3333 | //case DELFINO_PRIME_WITH_DILUENT_STARTED_CMD:                         
; 3334 | case DELFINO_PRIME_ALL_STARTED_CMD:                                    
; 3335 | //case DELFINO_BACK_FLUSH_STARTED_CMD:                                 
; 3336 | //case DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD:                     
; 3337 | //case DELFINO_ZAP_APERTURE_STARTED_CMD:                               
; 3338 | //case DELFINO_DRAIN_BATH_STARTED_CMD:                                 
; 3339 | //case DELFINO_DRAIN_ALL_STARTED_CMD:                                  
; 3340 | //case DELFINO_CLEAN_BATH_STARTED_CMD:                                 
; 3341 | //case DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD://SKM_COUNTSTART_CHAN
;     | GE                                                                     
; 3342 | case DELFINO_ASPIRATION_KEY_PRESS_RECIEVED:                            
; 3344 |     //!Set the data length                                             
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3329| 
        ; branch occurs ; [] |3329| 
$C$L304:    
	.dwpsn	file "../source/CommProtocol.c",line 3345,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3345 | usnDataLength = 0;                                                     
; 3346 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |3345| 
	.dwpsn	file "../source/CommProtocol.c",line 3347,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3347 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3347| 
	.dwpsn	file "../source/CommProtocol.c",line 3349,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3349 | break;                                                                 
; 3351 | case DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD:                             
; 3352 | case DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD:                             
; 3353 | case DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD:                    
; 3354 | case DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD:                      
; 3355 | case DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD:                
; 3357 |     //!Set the data length                                             
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3349| 
        ; branch occurs ; [] |3349| 
$C$L305:    
	.dwpsn	file "../source/CommProtocol.c",line 3358,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3358 | usnDataLength = 0;                                                     
; 3359 | //!Set the packet count to Zero                                        
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |3358| 
	.dwpsn	file "../source/CommProtocol.c",line 3360,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3360 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3360| 
	.dwpsn	file "../source/CommProtocol.c",line 3362,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3362 | break;                                                                 
; 3364 | #if 0                                                                  
; 3365 | //!If the sequence is counting time, get the data and update to the dat
;     | a                                                                      
; 3366 | //! field                                                              
; 3367 | case DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD:                     
; 3369 |     //!Get the counting time for WBC and RBC and update the data length
; 3370 |     usnDataLength = SysInitGetCountingTime(stPtrParsedPacket->arrusnDat
;     | a);                                                                    
; 3371 |     //!Set the packet count to Zero                                    
; 3372 |     m_usnPacketCount = 0;                                              
; 3374 | break;                                                                 
; 3376 | case DELFINO_VALVE_TEST_COMPLETED_CMD:                                 
; 3378 |     //!Set the valve number for which the valve test was performed     
; 3379 |     stPtrParsedPacket->arrusnData[0] =  m_ucValveNo;                   
; 3380 |     //!Reset the valve number                                          
; 3381 |     m_ucValveNo = 0;                                                   
; 3382 |     //!Set the data length to one                                      
; 3383 |     usnDataLength = 1;                                                 
; 3384 |     //!Set the packet count to Zero                                    
; 3385 |     m_usnPacketCount = 0;                                              
; 3388 | break;                                                                 
; 3390 | case DELFINO_HOME_POSITION_SENSOR_CHECK:                               
; 3392 |     sprintf(statDebugPrintBuf, " DELFINO_HOME_POSITION_SENSOR_CHECK \n"
;     | );                                                                     
; 3393 |     UartDebugPrint((int *)statDebugPrintBuf, 50);                      
; 3394 |     //!If the motor is at home position, set motor is at home position,
; 3395 |     //! else set motor is not at home position                         
; 3396 |     if(TRUE == OIHomeSensor4State())                                   
; 3398 |         utMotorHomePositionCheck.stMotorHomePosition.usnXHomePosition \
; 3399 |                                          = MOTOR_HOME_POSITION;        
; 3401 |     else                                                               
; 3403 |         utMotorHomePositionCheck.stMotorHomePosition.usnXHomePosition \
; 3404 |                                          = MOTOR_NOT_HOME_POSITION;    
; 3406 |     //!If the motor is at home position, set motor is at home position,
; 3407 |     //! else set motor is not at home position                         
; 3408 |     if(TRUE == OIHomeSensor5State())                                   
; 3410 |         utMotorHomePositionCheck.stMotorHomePosition.usnYHomePosition \
; 3411 |                                          = MOTOR_HOME_POSITION;        
; 3413 |     else                                                               
; 3415 |         utMotorHomePositionCheck.stMotorHomePosition.usnYHomePosition \
; 3416 |                                          = MOTOR_NOT_HOME_POSITION;    
; 3418 |     //!If the motor is at home position, set motor is at home position,
; 3419 |     //! else set motor is not at home position                         
; 3420 |     if(TRUE == OIHomeSensor2State())                                   
; 3422 |         utMotorHomePositionCheck.stMotorHomePosition.usnDiluentHomePosi
;     | tion \                                                                 
; 3423 |                                          = MOTOR_HOME_POSITION;        
; 3425 |     else                                                               
; 3427 |         utMotorHomePositionCheck.stMotorHomePosition.usnDiluentHomePosi
;     | tion \                                                                 
; 3428 |                                          = MOTOR_NOT_HOME_POSITION;    
; 3430 |     //!If the motor is at home position, set motor is at home position,
; 3431 |     //! else set motor is not at home position                         
; 3432 |     if(TRUE == OIHomeSensor1State())                                   
; 3434 |         utMotorHomePositionCheck.stMotorHomePosition.usnWasteHomePositi
;     | on \                                                                   
; 3435 |                                          = MOTOR_HOME_POSITION;        
; 3437 |     else                                                               
; 3439 |         utMotorHomePositionCheck.stMotorHomePosition.usnWasteHomePositi
;     | on \                                                                   
; 3440 |                                          = MOTOR_NOT_HOME_POSITION;    
; 3442 |     //!If the motor is at home position, set motor is at home position,
; 3443 |     //! else set motor is not at home position                         
; 3444 |     if(TRUE == OIHomeSensor3State())                                   
; 3446 |         utMotorHomePositionCheck.stMotorHomePosition.usnSampleHomePosit
;     | ion \                                                                  
; 3447 |                                          = MOTOR_HOME_POSITION;        
; 3449 |     else                                                               
; 3451 |         utMotorHomePositionCheck.stMotorHomePosition.usnSampleHomePosit
;     | ion \                                                                  
; 3452 |                                          = MOTOR_NOT_HOME_POSITION;    
; 3454 |     //!set Motor home position information to data                     
; 3455 |     stPtrParsedPacket->arrusnData[0] = utMotorHomePositionCheck.usnMoto
;     | rHomePosition;                                                         
; 3456 |     //!Set the data length                                             
; 3457 |     usnDataLength = 1;                                                 
; 3458 |     //!Set the packet count to Zero                                    
; 3459 |     m_usnPacketCount = 0;                                              
; 3461 | break;                                                                 
; 3463 | #endif                                                                 
; 3464 | default:                                                               
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3362| 
        ; branch occurs ; [] |3362| 
$C$L306:    
	.dwpsn	file "../source/CommProtocol.c",line 3466,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3466 | usnDataLength = 0;                                                     
;----------------------------------------------------------------------
        MOV       *-SP[4],#0            ; [CPU_] |3466| 
	.dwpsn	file "../source/CommProtocol.c",line 3467,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3467 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |3467| 
	.dwpsn	file "../source/CommProtocol.c",line 3469,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3469 | break;                                                                 
; 3470 | }//end of switch case                                                  
; 3473 | //!Return the data length                                              
;----------------------------------------------------------------------
        B         $C$L311,UNC           ; [CPU_] |3469| 
        ; branch occurs ; [] |3469| 
$C$L307:    
	.dwpsn	file "../source/CommProtocol.c",line 3135,column 5,is_stmt,isa 0
        MOVZ      AR6,*-SP[5]           ; [CPU_] |3135| 
        CMP       AR6,#8227             ; [CPU_] |3135| 
        B         $C$L309,GT            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        CMP       AR6,#8227             ; [CPU_] |3135| 
        B         $C$L305,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        CMP       AR6,#8211             ; [CPU_] |3135| 
        B         $C$L308,GT            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        CMP       AR6,#8211             ; [CPU_] |3135| 
        B         $C$L289,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR7,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8196            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR7              ; [CPU_] |3135| 
        B         $C$L277,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR7,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8197            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR7              ; [CPU_] |3135| 
        B         $C$L277,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR7,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8209            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR7              ; [CPU_] |3135| 
        B         $C$L279,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR6,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8210            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR6              ; [CPU_] |3135| 
        B         $C$L284,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        B         $C$L306,UNC           ; [CPU_] |3135| 
        ; branch occurs ; [] |3135| 
$C$L308:    
        MOVZ      AR7,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8212            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR7              ; [CPU_] |3135| 
        B         $C$L294,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR7,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8213            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR7              ; [CPU_] |3135| 
        B         $C$L299,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR6,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8219            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR6              ; [CPU_] |3135| 
        B         $C$L304,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        B         $C$L306,UNC           ; [CPU_] |3135| 
        ; branch occurs ; [] |3135| 
$C$L309:    
        CMP       AR6,#8231             ; [CPU_] |3135| 
        B         $C$L310,GT            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        CMP       AR6,#8231             ; [CPU_] |3135| 
        B         $C$L305,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR7,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8228            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR7              ; [CPU_] |3135| 
        B         $C$L305,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR7,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8229            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR7              ; [CPU_] |3135| 
        B         $C$L305,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR6,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8230            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR6              ; [CPU_] |3135| 
        B         $C$L305,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        B         $C$L306,UNC           ; [CPU_] |3135| 
        ; branch occurs ; [] |3135| 
$C$L310:    
        MOVZ      AR7,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8233            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR7              ; [CPU_] |3135| 
        B         $C$L278,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR7,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#8238            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR7              ; [CPU_] |3135| 
        B         $C$L304,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        MOVZ      AR6,AR6               ; [CPU_] |3135| 
        MOVL      XAR4,#9217            ; [CPU_U] |3135| 
        MOVL      ACC,XAR4              ; [CPU_] |3135| 
        CMPL      ACC,XAR6              ; [CPU_] |3135| 
        B         $C$L276,EQ            ; [CPU_] |3135| 
        ; branchcc occurs ; [] |3135| 
        B         $C$L306,UNC           ; [CPU_] |3135| 
        ; branch occurs ; [] |3135| 
$C$L311:    
	.dwpsn	file "../source/CommProtocol.c",line 3474,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3474 | return usnDataLength;                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |3474| 
	.dwpsn	file "../source/CommProtocol.c",line 3475,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$414	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$414, DW_AT_low_pc(0x00)
	.dwattr $C$DW$414, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$400, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$400, DW_AT_TI_end_line(0xd93)
	.dwattr $C$DW$400, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$400

	.sect	".text:_CPInitQueue"
	.clink
	.global	_CPInitQueue

$C$DW$415	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$415, DW_AT_name("CPInitQueue")
	.dwattr $C$DW$415, DW_AT_low_pc(_CPInitQueue)
	.dwattr $C$DW$415, DW_AT_high_pc(0x00)
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_CPInitQueue")
	.dwattr $C$DW$415, DW_AT_external
	.dwattr $C$DW$415, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$415, DW_AT_TI_begin_line(0xd9d)
	.dwattr $C$DW$415, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$415, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/CommProtocol.c",line 3486,column 1,is_stmt,address _CPInitQueue,isa 0

	.dwfde $C$DW$CIE, _CPInitQueue
;----------------------------------------------------------------------
; 3485 | void CPInitQueue(void)                                                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPInitQueue                  FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_CPInitQueue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/CommProtocol.c",line 3487,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3487 | pstQueue->nFront = -1;                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_pstQueue         ; [CPU_U] 
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3487| 
        MOV       *+XAR4[0],#-1         ; [CPU_] |3487| 
	.dwpsn	file "../source/CommProtocol.c",line 3488,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3488 | pstQueue->nRear = - 1;                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3488| 
        MOV       *+XAR4[1],#-1         ; [CPU_] |3488| 
	.dwpsn	file "../source/CommProtocol.c",line 3489,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3489 | memset(pstQueue->SSPIPacketFrame, 0, sizeof(pstQueue->SSPIPacketFrame))
;     | ;                                                                      
;----------------------------------------------------------------------
        MOVB      ACC,#3                ; [CPU_] |3489| 
        MOVL      XAR5,#3072            ; [CPU_U] |3489| 
        ADDL      ACC,@_pstQueue        ; [CPU_] |3489| 
        MOVL      XAR4,ACC              ; [CPU_] |3489| 
        MOVL      ACC,XAR5              ; [CPU_] |3489| 
        MOVB      XAR5,#0               ; [CPU_] |3489| 
$C$DW$416	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$416, DW_AT_low_pc(0x00)
	.dwattr $C$DW$416, DW_AT_name("_memset")
	.dwattr $C$DW$416, DW_AT_TI_call

        LCR       #_memset              ; [CPU_] |3489| 
        ; call occurs [#_memset] ; [] |3489| 
	.dwpsn	file "../source/CommProtocol.c",line 3490,column 1,is_stmt,isa 0
$C$DW$417	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$417, DW_AT_low_pc(0x00)
	.dwattr $C$DW$417, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$415, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$415, DW_AT_TI_end_line(0xda2)
	.dwattr $C$DW$415, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$415

	.sect	".text:_CPIsFull"
	.clink
	.global	_CPIsFull

$C$DW$418	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$418, DW_AT_name("CPIsFull")
	.dwattr $C$DW$418, DW_AT_low_pc(_CPIsFull)
	.dwattr $C$DW$418, DW_AT_high_pc(0x00)
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_CPIsFull")
	.dwattr $C$DW$418, DW_AT_external
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$418, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$418, DW_AT_TI_begin_line(0xdac)
	.dwattr $C$DW$418, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$418, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/CommProtocol.c",line 3501,column 1,is_stmt,address _CPIsFull,isa 0

	.dwfde $C$DW$CIE, _CPIsFull
;----------------------------------------------------------------------
; 3500 | int CPIsFull(void)                                                     
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPIsFull                     FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_CPIsFull:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/CommProtocol.c",line 3502,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3502 | return ((pstQueue->nCurrentSize == MAX_COMMAND_QUEUE_LENGTH));         
;----------------------------------------------------------------------
        MOVW      DP,#_pstQueue         ; [CPU_U] 
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3502| 
        MOVB      AL,#0                 ; [CPU_] |3502| 
        MOV       AH,*+XAR4[2]          ; [CPU_] |3502| 
        CMPB      AH,#6                 ; [CPU_] |3502| 
        B         $C$L312,NEQ           ; [CPU_] |3502| 
        ; branchcc occurs ; [] |3502| 
        MOVB      AL,#1                 ; [CPU_] |3502| 
$C$L312:    
	.dwpsn	file "../source/CommProtocol.c",line 3503,column 1,is_stmt,isa 0
$C$DW$419	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$419, DW_AT_low_pc(0x00)
	.dwattr $C$DW$419, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$418, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$418, DW_AT_TI_end_line(0xdaf)
	.dwattr $C$DW$418, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$418

	.sect	".text:_CPIsEmpty"
	.clink
	.global	_CPIsEmpty

$C$DW$420	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$420, DW_AT_name("CPIsEmpty")
	.dwattr $C$DW$420, DW_AT_low_pc(_CPIsEmpty)
	.dwattr $C$DW$420, DW_AT_high_pc(0x00)
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_CPIsEmpty")
	.dwattr $C$DW$420, DW_AT_external
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$420, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$420, DW_AT_TI_begin_line(0xdb9)
	.dwattr $C$DW$420, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$420, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/CommProtocol.c",line 3514,column 1,is_stmt,address _CPIsEmpty,isa 0

	.dwfde $C$DW$CIE, _CPIsEmpty
;----------------------------------------------------------------------
; 3513 | int CPIsEmpty(void)                                                    
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPIsEmpty                    FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_CPIsEmpty:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/CommProtocol.c",line 3515,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3515 | return (pstQueue->nCurrentSize);                                       
;----------------------------------------------------------------------
        MOVW      DP,#_pstQueue         ; [CPU_U] 
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3515| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3515| 
	.dwpsn	file "../source/CommProtocol.c",line 3516,column 1,is_stmt,isa 0
$C$DW$421	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$421, DW_AT_low_pc(0x00)
	.dwattr $C$DW$421, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$420, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$420, DW_AT_TI_end_line(0xdbc)
	.dwattr $C$DW$420, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$420

	.sect	".text:_CPEnqueue"
	.clink
	.global	_CPEnqueue

$C$DW$422	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$422, DW_AT_name("CPEnqueue")
	.dwattr $C$DW$422, DW_AT_low_pc(_CPEnqueue)
	.dwattr $C$DW$422, DW_AT_high_pc(0x00)
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_CPEnqueue")
	.dwattr $C$DW$422, DW_AT_external
	.dwattr $C$DW$422, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$422, DW_AT_TI_begin_line(0xdc6)
	.dwattr $C$DW$422, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$422, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommProtocol.c",line 3527,column 1,is_stmt,address _CPEnqueue,isa 0

	.dwfde $C$DW$CIE, _CPEnqueue
$C$DW$423	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$423, DW_AT_name("stPacketFrame")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_stPacketFrame")
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$423, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 3526 | void CPEnqueue(ST_SPI_PACKET_FRAME *stPacketFrame)                     
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPEnqueue                    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPEnqueue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$424	.dwtag  DW_TAG_variable
	.dwattr $C$DW$424, DW_AT_name("stPacketFrame")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_stPacketFrame")
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$424, DW_AT_location[DW_OP_breg20 -2]

        MOVL      *-SP[2],XAR4          ; [CPU_] |3527| 
	.dwpsn	file "../source/CommProtocol.c",line 3528,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3528 | if (CPIsFull())                                                        
;----------------------------------------------------------------------
$C$DW$425	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$425, DW_AT_low_pc(0x00)
	.dwattr $C$DW$425, DW_AT_name("_CPIsFull")
	.dwattr $C$DW$425, DW_AT_TI_call

        LCR       #_CPIsFull            ; [CPU_] |3528| 
        ; call occurs [#_CPIsFull] ; [] |3528| 
        CMPB      AL,#0                 ; [CPU_] |3528| 
        B         $C$L317,NEQ           ; [CPU_] |3528| 
        ; branchcc occurs ; [] |3528| 
	.dwpsn	file "../source/CommProtocol.c",line 3529,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3529 | return;                                                                
;----------------------------------------------------------------------
	.dwpsn	file "../source/CommProtocol.c",line 3531,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3531 | if(0 == pstQueue->nCurrentSize)                                        
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3531| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3531| 
        B         $C$L313,NEQ           ; [CPU_] |3531| 
        ; branchcc occurs ; [] |3531| 
	.dwpsn	file "../source/CommProtocol.c",line 3533,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3533 | CPInitQueue();                                                         
;----------------------------------------------------------------------
$C$DW$426	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$426, DW_AT_low_pc(0x00)
	.dwattr $C$DW$426, DW_AT_name("_CPInitQueue")
	.dwattr $C$DW$426, DW_AT_TI_call

        LCR       #_CPInitQueue         ; [CPU_] |3533| 
        ; call occurs [#_CPInitQueue] ; [] |3533| 
$C$L313:    
	.dwpsn	file "../source/CommProtocol.c",line 3535,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3535 | if(-1 == pstQueue->nFront)                                             
;----------------------------------------------------------------------
        MOVW      DP,#_pstQueue         ; [CPU_U] 
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3535| 
        CMP       *+XAR4[0],#-1         ; [CPU_] |3535| 
        B         $C$L314,NEQ           ; [CPU_] |3535| 
        ; branchcc occurs ; [] |3535| 
	.dwpsn	file "../source/CommProtocol.c",line 3537,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3537 | pstQueue->nFront = 0;                                                  
;----------------------------------------------------------------------
        MOV       *+XAR4[0],#0          ; [CPU_] |3537| 
$C$L314:    
	.dwpsn	file "../source/CommProtocol.c",line 3540,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3540 | pstQueue->nRear = pstQueue->nRear+1;                                   
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3540| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |3540| 
        ADDB      AL,#1                 ; [CPU_] |3540| 
        MOV       *+XAR4[1],AL          ; [CPU_] |3540| 
	.dwpsn	file "../source/CommProtocol.c",line 3541,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3541 | pstQueue->nCurrentSize = pstQueue->nCurrentSize + 1;                   
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3541| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3541| 
        ADDB      AL,#1                 ; [CPU_] |3541| 
        MOV       *+XAR4[2],AL          ; [CPU_] |3541| 
	.dwpsn	file "../source/CommProtocol.c",line 3542,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3542 | if(pstQueue->nCurrentSize > MAX_COMMAND_QUEUE_LENGTH)                  
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3542| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3542| 
        CMPB      AL,#6                 ; [CPU_] |3542| 
        B         $C$L315,LEQ           ; [CPU_] |3542| 
        ; branchcc occurs ; [] |3542| 
	.dwpsn	file "../source/CommProtocol.c",line 3544,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3544 | pstQueue->nCurrentSize = MAX_COMMAND_QUEUE_LENGTH;                     
;----------------------------------------------------------------------
        MOVB      *+XAR4[2],#6,UNC      ; [CPU_] |3544| 
$C$L315:    
	.dwpsn	file "../source/CommProtocol.c",line 3546,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3546 | if(pstQueue->nRear > (pstQueue->nCurrentSize - 1))                     
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3546| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3546| 
        ADDB      AL,#-1                ; [CPU_] |3546| 
        CMP       AL,*+XAR4[1]          ; [CPU_] |3546| 
        B         $C$L316,GEQ           ; [CPU_] |3546| 
        ; branchcc occurs ; [] |3546| 
	.dwpsn	file "../source/CommProtocol.c",line 3548,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3548 | pstQueue->nRear = pstQueue->nCurrentSize - 1;                          
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[2]          ; [CPU_] |3548| 
        ADDB      AL,#-1                ; [CPU_] |3548| 
        MOV       *+XAR4[1],AL          ; [CPU_] |3548| 
$C$L316:    
	.dwpsn	file "../source/CommProtocol.c",line 3551,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3551 | memcpy(&pstQueue->SSPIPacketFrame[pstQueue->nRear], stPacketFrame, size
;     | of(stPacketFrame));                                                    
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3551| 
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR7,*-SP[2]          ; [CPU_] |3551| 
        MOV       ACC,*+XAR4[1] << 9    ; [CPU_] |3551| 
        ADDL      ACC,@_pstQueue        ; [CPU_] |3551| 
        ADDB      ACC,#3                ; [CPU_] |3551| 
        MOVL      XAR4,ACC              ; [CPU_] |3551| 
        RPT       #1
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |3551| 
	.dwpsn	file "../source/CommProtocol.c",line 3552,column 1,is_stmt,isa 0
$C$L317:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$427	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$427, DW_AT_low_pc(0x00)
	.dwattr $C$DW$427, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$422, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$422, DW_AT_TI_end_line(0xde0)
	.dwattr $C$DW$422, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$422

	.sect	".text:_CPDequeue"
	.clink
	.global	_CPDequeue

$C$DW$428	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$428, DW_AT_name("CPDequeue")
	.dwattr $C$DW$428, DW_AT_low_pc(_CPDequeue)
	.dwattr $C$DW$428, DW_AT_high_pc(0x00)
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_CPDequeue")
	.dwattr $C$DW$428, DW_AT_external
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$428, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$428, DW_AT_TI_begin_line(0xdea)
	.dwattr $C$DW$428, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$428, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/CommProtocol.c",line 3563,column 1,is_stmt,address _CPDequeue,isa 0

	.dwfde $C$DW$CIE, _CPDequeue
;----------------------------------------------------------------------
; 3562 | int CPDequeue()                                                        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPDequeue                    FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_CPDequeue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/CommProtocol.c",line 3564,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3564 | if (CPIsEmpty()||(pstQueue->nFront <= -1))                             
;----------------------------------------------------------------------
$C$DW$429	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$429, DW_AT_low_pc(0x00)
	.dwattr $C$DW$429, DW_AT_name("_CPIsEmpty")
	.dwattr $C$DW$429, DW_AT_TI_call

        LCR       #_CPIsEmpty           ; [CPU_] |3564| 
        ; call occurs [#_CPIsEmpty] ; [] |3564| 
        CMPB      AL,#0                 ; [CPU_] |3564| 
        B         $C$L318,NEQ           ; [CPU_] |3564| 
        ; branchcc occurs ; [] |3564| 
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3564| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |3564| 
        B         $C$L319,GEQ           ; [CPU_] |3564| 
        ; branchcc occurs ; [] |3564| 
$C$L318:    
	.dwpsn	file "../source/CommProtocol.c",line 3565,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3565 | return (-1);                                                           
;----------------------------------------------------------------------
        MOV       AL,#-1                ; [CPU_] |3565| 
        B         $C$L322,UNC           ; [CPU_] |3565| 
        ; branch occurs ; [] |3565| 
$C$L319:    
	.dwpsn	file "../source/CommProtocol.c",line 3567,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3567 | pstQueue->nFront = (pstQueue->nFront + 1);                             
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[0]          ; [CPU_] |3567| 
        ADDB      AL,#1                 ; [CPU_] |3567| 
        MOV       *+XAR4[0],AL          ; [CPU_] |3567| 
	.dwpsn	file "../source/CommProtocol.c",line 3568,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3568 | pstQueue->nCurrentSize = pstQueue->nCurrentSize - 1;                   
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3568| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3568| 
        ADDB      AL,#-1                ; [CPU_] |3568| 
        MOV       *+XAR4[2],AL          ; [CPU_] |3568| 
	.dwpsn	file "../source/CommProtocol.c",line 3570,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3570 | if(pstQueue->nCurrentSize < 0 )                                        
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3570| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3570| 
        B         $C$L320,GEQ           ; [CPU_] |3570| 
        ; branchcc occurs ; [] |3570| 
	.dwpsn	file "../source/CommProtocol.c",line 3572,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3572 | pstQueue->nCurrentSize = 0;                                            
;----------------------------------------------------------------------
        MOV       *+XAR4[2],#0          ; [CPU_] |3572| 
$C$L320:    
	.dwpsn	file "../source/CommProtocol.c",line 3574,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3574 | if(pstQueue->nFront > (pstQueue->nCurrentSize - 1))                    
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3574| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3574| 
        ADDB      AL,#-1                ; [CPU_] |3574| 
        CMP       AL,*+XAR4[0]          ; [CPU_] |3574| 
        B         $C$L321,GEQ           ; [CPU_] |3574| 
        ; branchcc occurs ; [] |3574| 
	.dwpsn	file "../source/CommProtocol.c",line 3576,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3576 | pstQueue->nFront = pstQueue->nCurrentSize - 1;                         
;----------------------------------------------------------------------
        MOV       AL,*+XAR4[2]          ; [CPU_] |3576| 
        ADDB      AL,#-1                ; [CPU_] |3576| 
        MOV       *+XAR4[0],AL          ; [CPU_] |3576| 
$C$L321:    
	.dwpsn	file "../source/CommProtocol.c",line 3578,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3578 | memcpy(&m_stPacketForTxToSitara, &pstQueue->SSPIPacketFrame[pstQueue->n
;     | Front], \                                                              
; 3579 |         sizeof(m_stPacketForTxToSitara));                              
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3578| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*+XAR4[0] << 9    ; [CPU_] |3578| 
        ADDL      ACC,@_pstQueue        ; [CPU_] |3578| 
        MOVL      XAR4,#512             ; [CPU_U] |3578| 
        ADDB      ACC,#3                ; [CPU_] |3578| 
        MOVL      XAR5,ACC              ; [CPU_] |3578| 
        MOVL      ACC,XAR4              ; [CPU_] |3578| 
        MOVL      XAR4,#_m_stPacketForTxToSitara ; [CPU_U] |3578| 
$C$DW$430	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$430, DW_AT_low_pc(0x00)
	.dwattr $C$DW$430, DW_AT_name("_memcpy")
	.dwattr $C$DW$430, DW_AT_TI_call

        LCR       #_memcpy              ; [CPU_] |3578| 
        ; call occurs [#_memcpy] ; [] |3578| 
	.dwpsn	file "../source/CommProtocol.c",line 3581,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3581 | return 1;                                                              
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |3581| 
$C$L322:    
	.dwpsn	file "../source/CommProtocol.c",line 3582,column 1,is_stmt,isa 0
$C$DW$431	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$431, DW_AT_low_pc(0x00)
	.dwattr $C$DW$431, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$428, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$428, DW_AT_TI_end_line(0xdfe)
	.dwattr $C$DW$428, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$428

	.sect	".text:_CPSendPacketToSitara"
	.clink
	.global	_CPSendPacketToSitara

$C$DW$432	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$432, DW_AT_name("CPSendPacketToSitara")
	.dwattr $C$DW$432, DW_AT_low_pc(_CPSendPacketToSitara)
	.dwattr $C$DW$432, DW_AT_high_pc(0x00)
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_CPSendPacketToSitara")
	.dwattr $C$DW$432, DW_AT_external
	.dwattr $C$DW$432, DW_AT_TI_begin_file("../source/CommProtocol.c")
	.dwattr $C$DW$432, DW_AT_TI_begin_line(0xe08)
	.dwattr $C$DW$432, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$432, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommProtocol.c",line 3593,column 1,is_stmt,address _CPSendPacketToSitara,isa 0

	.dwfde $C$DW$CIE, _CPSendPacketToSitara
;----------------------------------------------------------------------
; 3592 | void CPSendPacketToSitara(void)                                        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPSendPacketToSitara         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CPSendPacketToSitara:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
	.dwpsn	file "../source/CommProtocol.c",line 3594,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3594 | if(!CPIsEmpty())                                                       
;----------------------------------------------------------------------
$C$DW$433	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$433, DW_AT_low_pc(0x00)
	.dwattr $C$DW$433, DW_AT_name("_CPIsEmpty")
	.dwattr $C$DW$433, DW_AT_TI_call

        LCR       #_CPIsEmpty           ; [CPU_] |3594| 
        ; call occurs [#_CPIsEmpty] ; [] |3594| 
        CMPB      AL,#0                 ; [CPU_] |3594| 
        B         $C$L324,NEQ           ; [CPU_] |3594| 
        ; branchcc occurs ; [] |3594| 

$C$DW$434	.dwtag  DW_TAG_lexical_block
	.dwattr $C$DW$434, DW_AT_low_pc(0x00)
	.dwattr $C$DW$434, DW_AT_high_pc(0x00)
$C$DW$435	.dwtag  DW_TAG_variable
	.dwattr $C$DW$435, DW_AT_name("bTxStatus")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_bTxStatus")
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$435, DW_AT_location[DW_OP_breg20 -1]

	.dwpsn	file "../source/CommProtocol.c",line 3596,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3596 | CPDequeue();                                                           
;----------------------------------------------------------------------
$C$DW$436	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$436, DW_AT_low_pc(0x00)
	.dwattr $C$DW$436, DW_AT_name("_CPDequeue")
	.dwattr $C$DW$436, DW_AT_TI_call

        LCR       #_CPDequeue           ; [CPU_] |3596| 
        ; call occurs [#_CPDequeue] ; [] |3596| 
	.dwpsn	file "../source/CommProtocol.c",line 3597,column 26,is_stmt,isa 0
;----------------------------------------------------------------------
; 3597 | bool_t bTxStatus = CPWriteDataToSPI();                                 
;----------------------------------------------------------------------
$C$DW$437	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$437, DW_AT_low_pc(0x00)
	.dwattr $C$DW$437, DW_AT_name("_CPWriteDataToSPI")
	.dwattr $C$DW$437, DW_AT_TI_call

        LCR       #_CPWriteDataToSPI    ; [CPU_] |3597| 
        ; call occurs [#_CPWriteDataToSPI] ; [] |3597| 
        MOV       *-SP[1],AL            ; [CPU_] |3597| 
	.dwpsn	file "../source/CommProtocol.c",line 3598,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3598 | if(FALSE == bTxStatus)                                                 
;----------------------------------------------------------------------
        CMPB      AL,#0                 ; [CPU_] |3598| 
        B         $C$L323,NEQ           ; [CPU_] |3598| 
        ; branchcc occurs ; [] |3598| 
	.dwpsn	file "../source/CommProtocol.c",line 3600,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3600 | pstQueue->nFront = (pstQueue->nFront - 1);                             
;----------------------------------------------------------------------
        MOVW      DP,#_pstQueue         ; [CPU_U] 
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3600| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |3600| 
        ADDB      AL,#-1                ; [CPU_] |3600| 
        MOV       *+XAR4[0],AL          ; [CPU_] |3600| 
	.dwpsn	file "../source/CommProtocol.c",line 3601,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3601 | pstQueue->nCurrentSize = pstQueue->nCurrentSize + 1;                   
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3601| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3601| 
        ADDB      AL,#1                 ; [CPU_] |3601| 
        MOV       *+XAR4[2],AL          ; [CPU_] |3601| 
	.dwpsn	file "../source/CommProtocol.c",line 3603,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3603 | if(pstQueue->nCurrentSize > MAX_COMMAND_QUEUE_LENGTH)                  
;----------------------------------------------------------------------
        MOVL      XAR4,@_pstQueue       ; [CPU_] |3603| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |3603| 
        CMPB      AL,#6                 ; [CPU_] |3603| 
        B         $C$L323,LEQ           ; [CPU_] |3603| 
        ; branchcc occurs ; [] |3603| 
	.dwpsn	file "../source/CommProtocol.c",line 3605,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3605 | pstQueue->nCurrentSize = MAX_COMMAND_QUEUE_LENGTH;                     
;----------------------------------------------------------------------
        MOVB      *+XAR4[2],#6,UNC      ; [CPU_] |3605| 
$C$L323:    
	.dwendtag $C$DW$434

	.dwpsn	file "../source/CommProtocol.c",line 3609,column 1,is_stmt,isa 0
$C$L324:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$438	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$438, DW_AT_low_pc(0x00)
	.dwattr $C$DW$438, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$432, DW_AT_TI_end_file("../source/CommProtocol.c")
	.dwattr $C$DW$432, DW_AT_TI_end_line(0xe19)
	.dwattr $C$DW$432, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$432

;***************************************************************
;* FAR STRINGS                                                 *
;***************************************************************
	.sect	".econst:.string"
	.align	2
$C$FSL1:	.string	13,10,"DELFINO_ERROR_MINOR_CMD",10,0
	.align	2
$C$FSL2:	.string	13,10,"unWBCEarlyCompletion",0
	.align	2
$C$FSL3:	.string	13,10,"unWBCLateCompletion",0
	.align	2
$C$FSL4:	.string	13,10,"unRBCEarlyCompletion",0
	.align	2
$C$FSL5:	.string	13,10,"unRBCLateCompletion",0
	.align	2
$C$FSL6:	.string	13,10,"unWBCCountingNotStarted",0
	.align	2
$C$FSL7:	.string	13,10,"unWBCCountingStartedImproper",0
	.align	2
$C$FSL8:	.string	13,10,"unRBCCountingNotStarted",0
	.align	2
$C$FSL9:	.string	13,10,"unRBCCountingStartedImproper",0
	.align	2
$C$FSL10:	.string	13,10,"unWBCCountingNotCompleted",0
	.align	2
$C$FSL11:	.string	13,10,"unWBCCountingCompletedImproper",0
	.align	2
$C$FSL12:	.string	13,10,"unRBCCountingNotCompleted",0
	.align	2
$C$FSL13:	.string	13,10,"unRBCCountingCompletedImproper",0
	.align	2
$C$FSL14:	.string	13,10,"unWBCImproperAbortion",0
	.align	2
$C$FSL15:	.string	13,10,"unRBCImproperAbortion",0
	.align	2
$C$FSL16:	.string	13,10,"unWBCFlowCalibAbortion",0
	.align	2
$C$FSL17:	.string	13,10,"unRBCFlowCalibAbortion",0
	.align	2
$C$FSL18:	.string	13,10," PressureCalCmpltd sent to sitara  ",10,0
;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_CIFormACKPacket
	.global	_SPIClearCommCtrlBusyInt
	.global	_SPISetCommCtrlBusyInt
	.global	_SysInitSystemHealthData
	.global	_abort
	.global	_perror
	.global	_SysInitGetFirmwareVer
	.global	_UartDebugPrint
	.global	_CIProcessCommandAttribute
	.global	_g_bMonDiluentPriming
	.global	_g_bMonRinsePriming
	.global	_m_eTypeofCell
	.global	_g_bMotorChkStatus
	.global	_m_usnPacketCount
	.global	_m_ucValveNo
	.global	_g_bMonLysePriming
	.global	_g_usnCurrentMode
	.global	_m_bTxPendingFlag
	.global	_g_bNextSecRawDataReq
	.global	_m_bNAckReceived
	.global	_OIHomeSensor3State
	.global	_OIHomeSensor2State
	.global	_OIHomeSensor4State
	.global	_OIHomeSensor5State
	.global	_SPIDmaWrite
	.global	_sprintf
	.global	_SysGetAdcCountData
	.global	_OIHomeSensor1State
	.global	_SysInitGetCountingTime
	.global	_g_unSequenceStateTime
	.global	_sncalZeroPSI
	.global	_memset
	.global	_SysGetADCDataLength
	.global	_SysGetPulseHeightDataFrmSDRAM
	.global	_SysGetADCRawDataFrmSDRAM
	.global	_utVolumetricErrorInfo
	.global	_utVolumetricErrorInfoReAcq
	.global	_utErrorInfoCheck
	.global	_DebugPrintBuf
	.global	_memcpy

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_name("SSPIPacketFrame")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x200)
$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$439, DW_AT_name("usnStartOfPacket_MSW")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_usnStartOfPacket_MSW")
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$440, DW_AT_name("usnStartOfPacket_LSW")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_usnStartOfPacket_LSW")
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$441, DW_AT_name("usnReservedWord")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_usnReservedWord")
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$442, DW_AT_name("usnDataLength")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$443, DW_AT_name("usnMajorCommand")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_usnMajorCommand")
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$444, DW_AT_name("usnMinorCommand")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_usnMinorCommand")
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$445, DW_AT_name("arrusnData")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_arrusnData")
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$446, DW_AT_name("usnRemainingPacketsforTX")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_usnRemainingPacketsforTX")
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x1fc]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$447, DW_AT_name("usnCRC")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_usnCRC")
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x1fd]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$448, DW_AT_name("usnEndOfPacket_MSW")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_usnEndOfPacket_MSW")
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x1fe]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$449, DW_AT_name("usnEndOfPacket_LSW")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_usnEndOfPacket_LSW")
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x1ff]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$21

$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("ST_SPI_PACKET_FRAME")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)


$C$DW$T$27	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$27, DW_AT_byte_size(0xc00)
$C$DW$450	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$450, DW_AT_upper_bound(0x05)

	.dwendtag $C$DW$T$27

$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x20)


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_name("ST_ERROR_INFO")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x06)
$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$451, DW_AT_name("ulnSequenceAborted")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_ulnSequenceAborted")
	.dwattr $C$DW$451, DW_AT_bit_offset(0x3f)
	.dwattr $C$DW$451, DW_AT_bit_size(0x01)
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$452, DW_AT_name("ulnX_Motor")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_ulnX_Motor")
	.dwattr $C$DW$452, DW_AT_bit_offset(0x3e)
	.dwattr $C$DW$452, DW_AT_bit_size(0x01)
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$453, DW_AT_name("ulnDiluentSyringe")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_ulnDiluentSyringe")
	.dwattr $C$DW$453, DW_AT_bit_offset(0x3d)
	.dwattr $C$DW$453, DW_AT_bit_size(0x01)
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$454, DW_AT_name("ulnSampleSyringe")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_ulnSampleSyringe")
	.dwattr $C$DW$454, DW_AT_bit_offset(0x3c)
	.dwattr $C$DW$454, DW_AT_bit_size(0x01)
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$455, DW_AT_name("ulnWasteSyringe")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_ulnWasteSyringe")
	.dwattr $C$DW$455, DW_AT_bit_offset(0x3b)
	.dwattr $C$DW$455, DW_AT_bit_size(0x01)
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$456, DW_AT_name("ulnY_Motor")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_ulnY_Motor")
	.dwattr $C$DW$456, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$456, DW_AT_bit_size(0x01)
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$457, DW_AT_name("ulnVacWBCDraining")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_ulnVacWBCDraining")
	.dwattr $C$DW$457, DW_AT_bit_offset(0x39)
	.dwattr $C$DW$457, DW_AT_bit_size(0x01)
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$458, DW_AT_name("ulnVacRBCDraining")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_ulnVacRBCDraining")
	.dwattr $C$DW$458, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$458, DW_AT_bit_size(0x01)
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$459, DW_AT_name("ulnVacRBCDrainingEnd")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_ulnVacRBCDrainingEnd")
	.dwattr $C$DW$459, DW_AT_bit_offset(0x37)
	.dwattr $C$DW$459, DW_AT_bit_size(0x01)
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$460, DW_AT_name("ulnPresWBCBubblingStart")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_ulnPresWBCBubblingStart")
	.dwattr $C$DW$460, DW_AT_bit_offset(0x36)
	.dwattr $C$DW$460, DW_AT_bit_size(0x01)
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$461, DW_AT_name("ulnPresWBCBubblingEnd")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_ulnPresWBCBubblingEnd")
	.dwattr $C$DW$461, DW_AT_bit_offset(0x35)
	.dwattr $C$DW$461, DW_AT_bit_size(0x01)
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$462, DW_AT_name("ulnPresWBCBubblingStartLyse")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_ulnPresWBCBubblingStartLyse")
	.dwattr $C$DW$462, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$462, DW_AT_bit_size(0x01)
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$463, DW_AT_name("ulnPresRBCBubblingStartLyse")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_ulnPresRBCBubblingStartLyse")
	.dwattr $C$DW$463, DW_AT_bit_offset(0x33)
	.dwattr $C$DW$463, DW_AT_bit_size(0x01)
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$464, DW_AT_name("ulnPresRBCBubblingStartLyseEnd")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_ulnPresRBCBubblingStartLyseEnd")
	.dwattr $C$DW$464, DW_AT_bit_offset(0x32)
	.dwattr $C$DW$464, DW_AT_bit_size(0x01)
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$465, DW_AT_name("ulnVacCountingStart")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_ulnVacCountingStart")
	.dwattr $C$DW$465, DW_AT_bit_offset(0x31)
	.dwattr $C$DW$465, DW_AT_bit_size(0x01)
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$466, DW_AT_name("ulnVacCountingEnd")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_ulnVacCountingEnd")
	.dwattr $C$DW$466, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$466, DW_AT_bit_size(0x01)
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$467, DW_AT_name("ulnVacWBCDrainingAC")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_ulnVacWBCDrainingAC")
	.dwattr $C$DW$467, DW_AT_bit_offset(0x2f)
	.dwattr $C$DW$467, DW_AT_bit_size(0x01)
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$468, DW_AT_name("ulnVacRBCDrainingAC")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_ulnVacRBCDrainingAC")
	.dwattr $C$DW$468, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$468, DW_AT_bit_size(0x01)
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$469, DW_AT_name("ulnVacRBCDrainingACEnd")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_ulnVacRBCDrainingACEnd")
	.dwattr $C$DW$469, DW_AT_bit_offset(0x2d)
	.dwattr $C$DW$469, DW_AT_bit_size(0x01)
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$470, DW_AT_name("ulnPresBackflushStart")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_ulnPresBackflushStart")
	.dwattr $C$DW$470, DW_AT_bit_offset(0x2c)
	.dwattr $C$DW$470, DW_AT_bit_size(0x01)
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$471, DW_AT_name("ulnPresBackflushEnd")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_ulnPresBackflushEnd")
	.dwattr $C$DW$471, DW_AT_bit_offset(0x2b)
	.dwattr $C$DW$471, DW_AT_bit_size(0x01)
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$472, DW_AT_name("ulnVacWBCDrainingFD")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_ulnVacWBCDrainingFD")
	.dwattr $C$DW$472, DW_AT_bit_offset(0x2a)
	.dwattr $C$DW$472, DW_AT_bit_size(0x01)
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$473, DW_AT_name("ulnVacRBCDrainingFD")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_ulnVacRBCDrainingFD")
	.dwattr $C$DW$473, DW_AT_bit_offset(0x29)
	.dwattr $C$DW$473, DW_AT_bit_size(0x01)
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$474, DW_AT_name("ulnVacRBCDrainingFDEnd")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_ulnVacRBCDrainingFDEnd")
	.dwattr $C$DW$474, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$474, DW_AT_bit_size(0x01)
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$475, DW_AT_name("ulnAspirationNotCompleted")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_ulnAspirationNotCompleted")
	.dwattr $C$DW$475, DW_AT_bit_offset(0x27)
	.dwattr $C$DW$475, DW_AT_bit_size(0x01)
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$476, DW_AT_name("ulnAspirationCompletedImproper")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_ulnAspirationCompletedImproper")
	.dwattr $C$DW$476, DW_AT_bit_offset(0x26)
	.dwattr $C$DW$476, DW_AT_bit_size(0x01)
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$477, DW_AT_name("ulnWBCDispensingNotCompleted")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_ulnWBCDispensingNotCompleted")
	.dwattr $C$DW$477, DW_AT_bit_offset(0x25)
	.dwattr $C$DW$477, DW_AT_bit_size(0x01)
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$478	.dwtag  DW_TAG_member
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$478, DW_AT_name("ulnWBCDispensingCompletedImproper")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_ulnWBCDispensingCompletedImproper")
	.dwattr $C$DW$478, DW_AT_bit_offset(0x24)
	.dwattr $C$DW$478, DW_AT_bit_size(0x01)
	.dwattr $C$DW$478, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$479, DW_AT_name("ulnRBCDispensingNotCompleted")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_ulnRBCDispensingNotCompleted")
	.dwattr $C$DW$479, DW_AT_bit_offset(0x23)
	.dwattr $C$DW$479, DW_AT_bit_size(0x01)
	.dwattr $C$DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$480, DW_AT_name("ulnRBCDispensingCompletedImproper")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_ulnRBCDispensingCompletedImproper")
	.dwattr $C$DW$480, DW_AT_bit_offset(0x22)
	.dwattr $C$DW$480, DW_AT_bit_size(0x01)
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$481, DW_AT_name("ulnWBCRBCBubblingNotCompleted")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_ulnWBCRBCBubblingNotCompleted")
	.dwattr $C$DW$481, DW_AT_bit_offset(0x21)
	.dwattr $C$DW$481, DW_AT_bit_size(0x01)
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$482, DW_AT_name("ulnWBCRBCBubblingCompletedImproper")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_ulnWBCRBCBubblingCompletedImproper")
	.dwattr $C$DW$482, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$482, DW_AT_bit_size(0x01)
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$483, DW_AT_name("ulnBackFlushNotCompleted")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_ulnBackFlushNotCompleted")
	.dwattr $C$DW$483, DW_AT_bit_offset(0x1f)
	.dwattr $C$DW$483, DW_AT_bit_size(0x01)
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$484, DW_AT_name("ulnBackFlushCompletedImproper")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_ulnBackFlushCompletedImproper")
	.dwattr $C$DW$484, DW_AT_bit_offset(0x1e)
	.dwattr $C$DW$484, DW_AT_bit_size(0x01)
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$485, DW_AT_name("ulnBathFillingNotCompleted")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_ulnBathFillingNotCompleted")
	.dwattr $C$DW$485, DW_AT_bit_offset(0x1d)
	.dwattr $C$DW$485, DW_AT_bit_size(0x01)
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$486, DW_AT_name("ulnBathFillingCompletedImproper")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_ulnBathFillingCompletedImproper")
	.dwattr $C$DW$486, DW_AT_bit_offset(0x1c)
	.dwattr $C$DW$486, DW_AT_bit_size(0x01)
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$487, DW_AT_name("ulnWasteBinFull")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_ulnWasteBinFull")
	.dwattr $C$DW$487, DW_AT_bit_offset(0x1b)
	.dwattr $C$DW$487, DW_AT_bit_size(0x01)
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$488	.dwtag  DW_TAG_member
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$488, DW_AT_name("ulnDiluentEmpty")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_ulnDiluentEmpty")
	.dwattr $C$DW$488, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$488, DW_AT_bit_size(0x01)
	.dwattr $C$DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$489	.dwtag  DW_TAG_member
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$489, DW_AT_name("ulnLyseEmpty")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_ulnLyseEmpty")
	.dwattr $C$DW$489, DW_AT_bit_offset(0x19)
	.dwattr $C$DW$489, DW_AT_bit_size(0x01)
	.dwattr $C$DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$489, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$490, DW_AT_name("ulnRinseEmpty")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_ulnRinseEmpty")
	.dwattr $C$DW$490, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$490, DW_AT_bit_size(0x01)
	.dwattr $C$DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$491	.dwtag  DW_TAG_member
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$491, DW_AT_name("ulnTemperature")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_ulnTemperature")
	.dwattr $C$DW$491, DW_AT_bit_offset(0x17)
	.dwattr $C$DW$491, DW_AT_bit_size(0x01)
	.dwattr $C$DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$491, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$492	.dwtag  DW_TAG_member
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$492, DW_AT_name("uln24V_Check")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_uln24V_Check")
	.dwattr $C$DW$492, DW_AT_bit_offset(0x16)
	.dwattr $C$DW$492, DW_AT_bit_size(0x01)
	.dwattr $C$DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$492, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$493	.dwtag  DW_TAG_member
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$493, DW_AT_name("uln5V_Check")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_uln5V_Check")
	.dwattr $C$DW$493, DW_AT_bit_offset(0x15)
	.dwattr $C$DW$493, DW_AT_bit_size(0x01)
	.dwattr $C$DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$493, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$494	.dwtag  DW_TAG_member
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$494, DW_AT_name("ulnReAcq")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_ulnReAcq")
	.dwattr $C$DW$494, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$494, DW_AT_bit_size(0x01)
	.dwattr $C$DW$494, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$494, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$495, DW_AT_name("ulnPressureSensorFail")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_ulnPressureSensorFail")
	.dwattr $C$DW$495, DW_AT_bit_offset(0x13)
	.dwattr $C$DW$495, DW_AT_bit_size(0x01)
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$496, DW_AT_name("ulnLowVacuum")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_ulnLowVacuum")
	.dwattr $C$DW$496, DW_AT_bit_offset(0x12)
	.dwattr $C$DW$496, DW_AT_bit_size(0x01)
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$497, DW_AT_name("ulnHighPressure")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_ulnHighPressure")
	.dwattr $C$DW$497, DW_AT_bit_offset(0x11)
	.dwattr $C$DW$497, DW_AT_bit_size(0x01)
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$498, DW_AT_name("ulnRBCStartSensor")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_ulnRBCStartSensor")
	.dwattr $C$DW$498, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$498, DW_AT_bit_size(0x01)
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$499, DW_AT_name("ulnRBCStopSensor")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_ulnRBCStopSensor")
	.dwattr $C$DW$499, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$499, DW_AT_bit_size(0x01)
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$500, DW_AT_name("ulnWBCStartSensor")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_ulnWBCStartSensor")
	.dwattr $C$DW$500, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$500, DW_AT_bit_size(0x01)
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$501, DW_AT_name("ulnWBCStopSensor")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_ulnWBCStopSensor")
	.dwattr $C$DW$501, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$501, DW_AT_bit_size(0x01)
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$502, DW_AT_name("ulnSequenceArrested")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_ulnSequenceArrested")
	.dwattr $C$DW$502, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$502, DW_AT_bit_size(0x01)
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$503, DW_AT_name("ulnY_MotorFault")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_ulnY_MotorFault")
	.dwattr $C$DW$503, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$503, DW_AT_bit_size(0x01)
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$504, DW_AT_name("ulnX_MotorFault")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_ulnX_MotorFault")
	.dwattr $C$DW$504, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$504, DW_AT_bit_size(0x01)
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$505, DW_AT_name("ulnDiluent_MotorFault")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_ulnDiluent_MotorFault")
	.dwattr $C$DW$505, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$505, DW_AT_bit_size(0x01)
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$506, DW_AT_name("ulnSample_MotorFault")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_ulnSample_MotorFault")
	.dwattr $C$DW$506, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$506, DW_AT_bit_size(0x01)
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$507, DW_AT_name("ulnWaste_MotorFault")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_ulnWaste_MotorFault")
	.dwattr $C$DW$507, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$507, DW_AT_bit_size(0x01)
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$508, DW_AT_name("PressureCalCmpltd")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_PressureCalCmpltd")
	.dwattr $C$DW$508, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$508, DW_AT_bit_size(0x01)
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$509, DW_AT_name("ulnReserved")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_ulnReserved")
	.dwattr $C$DW$509, DW_AT_bit_offset(-6)
	.dwattr $C$DW$509, DW_AT_bit_size(0x0c)
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$23


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_name("ST_MOTOR_HOME_POSITION")
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$510, DW_AT_name("usnXHomePosition")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_usnXHomePosition")
	.dwattr $C$DW$510, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$510, DW_AT_bit_size(0x01)
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$511, DW_AT_name("usnYHomePosition")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_usnYHomePosition")
	.dwattr $C$DW$511, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$511, DW_AT_bit_size(0x01)
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$512, DW_AT_name("usnDiluentHomePosition")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_usnDiluentHomePosition")
	.dwattr $C$DW$512, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$512, DW_AT_bit_size(0x01)
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$513, DW_AT_name("usnWasteHomePosition")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_usnWasteHomePosition")
	.dwattr $C$DW$513, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$513, DW_AT_bit_size(0x01)
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$514, DW_AT_name("usnSampleHomePosition")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_usnSampleHomePosition")
	.dwattr $C$DW$514, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$514, DW_AT_bit_size(0x01)
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$515, DW_AT_name("usnReserved")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_usnReserved")
	.dwattr $C$DW$515, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$515, DW_AT_bit_size(0x0b)
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$25


$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_name("ST_QUEUE")
	.dwattr $C$DW$T$28, DW_AT_byte_size(0xc03)
$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$516, DW_AT_name("nFront")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_nFront")
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$517, DW_AT_name("nRear")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_nRear")
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$518, DW_AT_name("nCurrentSize")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_nCurrentSize")
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$519, DW_AT_name("SSPIPacketFrame")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_SSPIPacketFrame")
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$28

$C$DW$T$37	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$37, DW_AT_name("stQueue")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)

$C$DW$T$38	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$38, DW_AT_address_class(0x20)


$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_name("ST_VOLUMETRIC_ERROR")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x04)
$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$520, DW_AT_name("unWBCCountingNotStarted")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_unWBCCountingNotStarted")
	.dwattr $C$DW$520, DW_AT_bit_offset(0x1f)
	.dwattr $C$DW$520, DW_AT_bit_size(0x01)
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$521, DW_AT_name("unWBCCountingStartedImproper")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_unWBCCountingStartedImproper")
	.dwattr $C$DW$521, DW_AT_bit_offset(0x1e)
	.dwattr $C$DW$521, DW_AT_bit_size(0x01)
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$522, DW_AT_name("unRBCCountingNotStarted")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_unRBCCountingNotStarted")
	.dwattr $C$DW$522, DW_AT_bit_offset(0x1d)
	.dwattr $C$DW$522, DW_AT_bit_size(0x01)
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$523, DW_AT_name("unRBCCountingStartedImproper")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_unRBCCountingStartedImproper")
	.dwattr $C$DW$523, DW_AT_bit_offset(0x1c)
	.dwattr $C$DW$523, DW_AT_bit_size(0x01)
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$524, DW_AT_name("unWBCCountingNotCompleted")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_unWBCCountingNotCompleted")
	.dwattr $C$DW$524, DW_AT_bit_offset(0x1b)
	.dwattr $C$DW$524, DW_AT_bit_size(0x01)
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$525, DW_AT_name("unWBCCountingCompletedImproper")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_unWBCCountingCompletedImproper")
	.dwattr $C$DW$525, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$525, DW_AT_bit_size(0x01)
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$526, DW_AT_name("unRBCCountingNotCompleted")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_unRBCCountingNotCompleted")
	.dwattr $C$DW$526, DW_AT_bit_offset(0x19)
	.dwattr $C$DW$526, DW_AT_bit_size(0x01)
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$527, DW_AT_name("unRBCCountingCompletedImproper")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_unRBCCountingCompletedImproper")
	.dwattr $C$DW$527, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$527, DW_AT_bit_size(0x01)
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$528, DW_AT_name("unWBCEarlyCompletion")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_unWBCEarlyCompletion")
	.dwattr $C$DW$528, DW_AT_bit_offset(0x17)
	.dwattr $C$DW$528, DW_AT_bit_size(0x01)
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$529, DW_AT_name("unWBCLateCompletion")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_unWBCLateCompletion")
	.dwattr $C$DW$529, DW_AT_bit_offset(0x16)
	.dwattr $C$DW$529, DW_AT_bit_size(0x01)
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$530, DW_AT_name("unRBCEarlyCompletion")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_unRBCEarlyCompletion")
	.dwattr $C$DW$530, DW_AT_bit_offset(0x15)
	.dwattr $C$DW$530, DW_AT_bit_size(0x01)
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$531, DW_AT_name("unRBCLateCompletion")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_unRBCLateCompletion")
	.dwattr $C$DW$531, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$531, DW_AT_bit_size(0x01)
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$532, DW_AT_name("unWBCImproperAbortion")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_unWBCImproperAbortion")
	.dwattr $C$DW$532, DW_AT_bit_offset(0x13)
	.dwattr $C$DW$532, DW_AT_bit_size(0x01)
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$533, DW_AT_name("unRBCImproperAbortion")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_unRBCImproperAbortion")
	.dwattr $C$DW$533, DW_AT_bit_offset(0x12)
	.dwattr $C$DW$533, DW_AT_bit_size(0x01)
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$534, DW_AT_name("unWBCFlowCalibAbortion")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_unWBCFlowCalibAbortion")
	.dwattr $C$DW$534, DW_AT_bit_offset(0x11)
	.dwattr $C$DW$534, DW_AT_bit_size(0x01)
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$535, DW_AT_name("unRBCFlowCalibAbortion")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_unRBCFlowCalibAbortion")
	.dwattr $C$DW$535, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$535, DW_AT_bit_size(0x01)
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$536, DW_AT_name("unReserved")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_unReserved")
	.dwattr $C$DW$536, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$536, DW_AT_bit_size(0x14)
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("ST_VOLUMETRIC_ERROR_RE_ACQ")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x04)
$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$537, DW_AT_name("unWBCCountingNotStarted")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_unWBCCountingNotStarted")
	.dwattr $C$DW$537, DW_AT_bit_offset(0x1f)
	.dwattr $C$DW$537, DW_AT_bit_size(0x01)
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$538, DW_AT_name("unWBCCountingStartedImproper")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_unWBCCountingStartedImproper")
	.dwattr $C$DW$538, DW_AT_bit_offset(0x1e)
	.dwattr $C$DW$538, DW_AT_bit_size(0x01)
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$539, DW_AT_name("unRBCCountingNotStarted")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_unRBCCountingNotStarted")
	.dwattr $C$DW$539, DW_AT_bit_offset(0x1d)
	.dwattr $C$DW$539, DW_AT_bit_size(0x01)
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$540, DW_AT_name("unRBCCountingStartedImproper")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_unRBCCountingStartedImproper")
	.dwattr $C$DW$540, DW_AT_bit_offset(0x1c)
	.dwattr $C$DW$540, DW_AT_bit_size(0x01)
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$541, DW_AT_name("unWBCCountingNotCompleted")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_unWBCCountingNotCompleted")
	.dwattr $C$DW$541, DW_AT_bit_offset(0x1b)
	.dwattr $C$DW$541, DW_AT_bit_size(0x01)
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$542, DW_AT_name("unWBCCountingCompletedImproper")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_unWBCCountingCompletedImproper")
	.dwattr $C$DW$542, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$542, DW_AT_bit_size(0x01)
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$543, DW_AT_name("unRBCCountingNotCompleted")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_unRBCCountingNotCompleted")
	.dwattr $C$DW$543, DW_AT_bit_offset(0x19)
	.dwattr $C$DW$543, DW_AT_bit_size(0x01)
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$544, DW_AT_name("unRBCCountingCompletedImproper")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_unRBCCountingCompletedImproper")
	.dwattr $C$DW$544, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$544, DW_AT_bit_size(0x01)
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$545, DW_AT_name("unWBCEarlyCompletion")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_unWBCEarlyCompletion")
	.dwattr $C$DW$545, DW_AT_bit_offset(0x17)
	.dwattr $C$DW$545, DW_AT_bit_size(0x01)
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$546, DW_AT_name("unWBCLateCompletion")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_unWBCLateCompletion")
	.dwattr $C$DW$546, DW_AT_bit_offset(0x16)
	.dwattr $C$DW$546, DW_AT_bit_size(0x01)
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$547, DW_AT_name("unRBCEarlyCompletion")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_unRBCEarlyCompletion")
	.dwattr $C$DW$547, DW_AT_bit_offset(0x15)
	.dwattr $C$DW$547, DW_AT_bit_size(0x01)
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$548, DW_AT_name("unRBCLateCompletion")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_unRBCLateCompletion")
	.dwattr $C$DW$548, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$548, DW_AT_bit_size(0x01)
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$549, DW_AT_name("unWBCImproperAbortion")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_unWBCImproperAbortion")
	.dwattr $C$DW$549, DW_AT_bit_offset(0x13)
	.dwattr $C$DW$549, DW_AT_bit_size(0x01)
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$550, DW_AT_name("unRBCImproperAbortion")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_unRBCImproperAbortion")
	.dwattr $C$DW$550, DW_AT_bit_offset(0x12)
	.dwattr $C$DW$550, DW_AT_bit_size(0x01)
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$551, DW_AT_name("unWBCFlowCalibAbortion")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_unWBCFlowCalibAbortion")
	.dwattr $C$DW$551, DW_AT_bit_offset(0x11)
	.dwattr $C$DW$551, DW_AT_bit_size(0x01)
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$552, DW_AT_name("unRBCFlowCalibAbortion")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_unRBCFlowCalibAbortion")
	.dwattr $C$DW$552, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$552, DW_AT_bit_size(0x01)
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$553, DW_AT_name("usnReserved")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_usnReserved")
	.dwattr $C$DW$553, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$553, DW_AT_bit_size(0x14)
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$32, DW_AT_name("UT_ERROR_INFO")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x06)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$554, DW_AT_name("stErrorInfo")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_stErrorInfo")
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$555, DW_AT_name("ulnErrorInfo")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_ulnErrorInfo")
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$33, DW_AT_name("UT_MOTOR_HOME_POSITION")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x01)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$556, DW_AT_name("stMotorHomePosition")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_stMotorHomePosition")
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$557, DW_AT_name("usnMotorHomePosition")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_usnMotorHomePosition")
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$34, DW_AT_name("UT_VOLUMETRIC_ERROR")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x04)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$558, DW_AT_name("stVolumetricError")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_stVolumetricError")
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$559, DW_AT_name("unVolumetricError")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_unVolumetricError")
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$35, DW_AT_name("UT_VOLUMETRIC_ERROR_RE_ACQ")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x04)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$560, DW_AT_name("stVolumetricErrorReAcq")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_stVolumetricErrorReAcq")
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$561, DW_AT_name("unVolumetricError")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_unVolumetricError")
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$35

$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_type(*$C$DW$T$2)
	.dwattr $C$DW$T$3, DW_AT_address_class(0x20)

$C$DW$562	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$2)

$C$DW$T$39	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$562)

$C$DW$T$40	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$40, DW_AT_address_class(0x20)

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$563	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$6)

$C$DW$T$85	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$563)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x20)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$24	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$24, DW_AT_name("Uint16")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$19, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)


$C$DW$T$20	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x1f6)
$C$DW$564	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$564, DW_AT_upper_bound(0x1f5)

	.dwendtag $C$DW$T$20

$C$DW$T$57	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$57, DW_AT_address_class(0x20)

$C$DW$565	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$19)

$C$DW$T$106	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$565)

$C$DW$566	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$19)

$C$DW$T$107	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$566)

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$108	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$108, DW_AT_name("Uint32")
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)

$C$DW$T$41	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$41, DW_AT_name("size_t")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)

$C$DW$T$29	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$29, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$22	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$22, DW_AT_name("Uint64")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

$C$DW$567	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$5)

$C$DW$T$46	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$567)

$C$DW$T$47	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x20)

$C$DW$T$72	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$72, DW_AT_address_class(0x20)


$C$DW$T$115	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$115, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x1f4)
$C$DW$568	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$568, DW_AT_upper_bound(0x1f3)

	.dwendtag $C$DW$T$115


$C$DW$T$116	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$116, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$116, DW_AT_byte_size(0x32)
$C$DW$569	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$569, DW_AT_upper_bound(0x31)

	.dwendtag $C$DW$T$116

$C$DW$T$62	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$62, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$62, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)

$C$DW$T$63	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$63, DW_AT_name("bool_t")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)

$C$DW$570	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$63)

$C$DW$T$141	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$570)


$C$DW$T$81	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$81, DW_AT_name("eCellType")
	.dwattr $C$DW$T$81, DW_AT_byte_size(0x01)
$C$DW$571	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$571, DW_AT_name("ecellInvalid")
	.dwattr $C$DW$571, DW_AT_const_value(0x00)

$C$DW$572	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$572, DW_AT_name("eCellRBC")
	.dwattr $C$DW$572, DW_AT_const_value(0x01)

$C$DW$573	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$573, DW_AT_name("eCellPLT")
	.dwattr $C$DW$573, DW_AT_const_value(0x02)

$C$DW$574	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$574, DW_AT_name("eCellWBC")
	.dwattr $C$DW$574, DW_AT_const_value(0x03)

	.dwendtag $C$DW$T$81

$C$DW$T$82	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$82, DW_AT_name("E_CELL_TYPE")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)

$C$DW$575	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$82)

$C$DW$T$142	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$575)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$576	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$576, DW_AT_name("AL")
	.dwattr $C$DW$576, DW_AT_location[DW_OP_reg0]

$C$DW$577	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$577, DW_AT_name("AH")
	.dwattr $C$DW$577, DW_AT_location[DW_OP_reg1]

$C$DW$578	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$578, DW_AT_name("PL")
	.dwattr $C$DW$578, DW_AT_location[DW_OP_reg2]

$C$DW$579	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$579, DW_AT_name("PH")
	.dwattr $C$DW$579, DW_AT_location[DW_OP_reg3]

$C$DW$580	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$580, DW_AT_name("SP")
	.dwattr $C$DW$580, DW_AT_location[DW_OP_reg20]

$C$DW$581	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$581, DW_AT_name("XT")
	.dwattr $C$DW$581, DW_AT_location[DW_OP_reg21]

$C$DW$582	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$582, DW_AT_name("T")
	.dwattr $C$DW$582, DW_AT_location[DW_OP_reg22]

$C$DW$583	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$583, DW_AT_name("ST0")
	.dwattr $C$DW$583, DW_AT_location[DW_OP_reg23]

$C$DW$584	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$584, DW_AT_name("ST1")
	.dwattr $C$DW$584, DW_AT_location[DW_OP_reg24]

$C$DW$585	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$585, DW_AT_name("PC")
	.dwattr $C$DW$585, DW_AT_location[DW_OP_reg25]

$C$DW$586	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$586, DW_AT_name("RPC")
	.dwattr $C$DW$586, DW_AT_location[DW_OP_reg26]

$C$DW$587	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$587, DW_AT_name("FP")
	.dwattr $C$DW$587, DW_AT_location[DW_OP_reg28]

$C$DW$588	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$588, DW_AT_name("DP")
	.dwattr $C$DW$588, DW_AT_location[DW_OP_reg29]

$C$DW$589	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$589, DW_AT_name("SXM")
	.dwattr $C$DW$589, DW_AT_location[DW_OP_reg30]

$C$DW$590	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$590, DW_AT_name("PM")
	.dwattr $C$DW$590, DW_AT_location[DW_OP_reg31]

$C$DW$591	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$591, DW_AT_name("OVM")
	.dwattr $C$DW$591, DW_AT_location[DW_OP_regx 0x20]

$C$DW$592	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$592, DW_AT_name("PAGE0")
	.dwattr $C$DW$592, DW_AT_location[DW_OP_regx 0x21]

$C$DW$593	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$593, DW_AT_name("AMODE")
	.dwattr $C$DW$593, DW_AT_location[DW_OP_regx 0x22]

$C$DW$594	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$594, DW_AT_name("INTM")
	.dwattr $C$DW$594, DW_AT_location[DW_OP_regx 0x23]

$C$DW$595	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$595, DW_AT_name("IFR")
	.dwattr $C$DW$595, DW_AT_location[DW_OP_regx 0x24]

$C$DW$596	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$596, DW_AT_name("IER")
	.dwattr $C$DW$596, DW_AT_location[DW_OP_regx 0x25]

$C$DW$597	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$597, DW_AT_name("V")
	.dwattr $C$DW$597, DW_AT_location[DW_OP_regx 0x26]

$C$DW$598	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$598, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$598, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$599	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$599, DW_AT_name("VOL")
	.dwattr $C$DW$599, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$600	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$600, DW_AT_name("AR0")
	.dwattr $C$DW$600, DW_AT_location[DW_OP_reg4]

$C$DW$601	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$601, DW_AT_name("XAR0")
	.dwattr $C$DW$601, DW_AT_location[DW_OP_reg5]

$C$DW$602	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$602, DW_AT_name("AR1")
	.dwattr $C$DW$602, DW_AT_location[DW_OP_reg6]

$C$DW$603	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$603, DW_AT_name("XAR1")
	.dwattr $C$DW$603, DW_AT_location[DW_OP_reg7]

$C$DW$604	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$604, DW_AT_name("AR2")
	.dwattr $C$DW$604, DW_AT_location[DW_OP_reg8]

$C$DW$605	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$605, DW_AT_name("XAR2")
	.dwattr $C$DW$605, DW_AT_location[DW_OP_reg9]

$C$DW$606	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$606, DW_AT_name("AR3")
	.dwattr $C$DW$606, DW_AT_location[DW_OP_reg10]

$C$DW$607	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$607, DW_AT_name("XAR3")
	.dwattr $C$DW$607, DW_AT_location[DW_OP_reg11]

$C$DW$608	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$608, DW_AT_name("AR4")
	.dwattr $C$DW$608, DW_AT_location[DW_OP_reg12]

$C$DW$609	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$609, DW_AT_name("XAR4")
	.dwattr $C$DW$609, DW_AT_location[DW_OP_reg13]

$C$DW$610	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$610, DW_AT_name("AR5")
	.dwattr $C$DW$610, DW_AT_location[DW_OP_reg14]

$C$DW$611	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$611, DW_AT_name("XAR5")
	.dwattr $C$DW$611, DW_AT_location[DW_OP_reg15]

$C$DW$612	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$612, DW_AT_name("AR6")
	.dwattr $C$DW$612, DW_AT_location[DW_OP_reg16]

$C$DW$613	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$613, DW_AT_name("XAR6")
	.dwattr $C$DW$613, DW_AT_location[DW_OP_reg17]

$C$DW$614	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$614, DW_AT_name("AR7")
	.dwattr $C$DW$614, DW_AT_location[DW_OP_reg18]

$C$DW$615	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$615, DW_AT_name("XAR7")
	.dwattr $C$DW$615, DW_AT_location[DW_OP_reg19]

$C$DW$616	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$616, DW_AT_name("R0HL")
	.dwattr $C$DW$616, DW_AT_location[DW_OP_regx 0x29]

$C$DW$617	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$617, DW_AT_name("R0H")
	.dwattr $C$DW$617, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$618	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$618, DW_AT_name("R1HL")
	.dwattr $C$DW$618, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$619	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$619, DW_AT_name("R1H")
	.dwattr $C$DW$619, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$620	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$620, DW_AT_name("R2HL")
	.dwattr $C$DW$620, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$621	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$621, DW_AT_name("R2H")
	.dwattr $C$DW$621, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$622	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$622, DW_AT_name("R3HL")
	.dwattr $C$DW$622, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$623	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$623, DW_AT_name("R3H")
	.dwattr $C$DW$623, DW_AT_location[DW_OP_regx 0x30]

$C$DW$624	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$624, DW_AT_name("R4HL")
	.dwattr $C$DW$624, DW_AT_location[DW_OP_regx 0x31]

$C$DW$625	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$625, DW_AT_name("R4H")
	.dwattr $C$DW$625, DW_AT_location[DW_OP_regx 0x32]

$C$DW$626	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$626, DW_AT_name("R5HL")
	.dwattr $C$DW$626, DW_AT_location[DW_OP_regx 0x33]

$C$DW$627	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$627, DW_AT_name("R5H")
	.dwattr $C$DW$627, DW_AT_location[DW_OP_regx 0x34]

$C$DW$628	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$628, DW_AT_name("R6HL")
	.dwattr $C$DW$628, DW_AT_location[DW_OP_regx 0x35]

$C$DW$629	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$629, DW_AT_name("R6H")
	.dwattr $C$DW$629, DW_AT_location[DW_OP_regx 0x36]

$C$DW$630	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$630, DW_AT_name("R7HL")
	.dwattr $C$DW$630, DW_AT_location[DW_OP_regx 0x37]

$C$DW$631	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$631, DW_AT_name("R7H")
	.dwattr $C$DW$631, DW_AT_location[DW_OP_regx 0x38]

$C$DW$632	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$632, DW_AT_name("RBL")
	.dwattr $C$DW$632, DW_AT_location[DW_OP_regx 0x49]

$C$DW$633	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$633, DW_AT_name("RB")
	.dwattr $C$DW$633, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$634	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$634, DW_AT_name("STFL")
	.dwattr $C$DW$634, DW_AT_location[DW_OP_regx 0x27]

$C$DW$635	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$635, DW_AT_name("STF")
	.dwattr $C$DW$635, DW_AT_location[DW_OP_regx 0x28]

$C$DW$636	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$636, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$636, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


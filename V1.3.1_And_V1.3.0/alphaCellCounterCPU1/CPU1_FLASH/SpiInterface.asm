;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Fri Jan 29 15:14:32 2021                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../source/SpiInterface.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cell Counter\V1.3.1_And_V1.3.0\alphaCellCounterCPU1\CPU1_FLASH")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_sdata1+0,32
	.bits	0,16			; _sdata1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_sdata2+0,32
	.bits	0,16			; _sdata2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_sdata3+0,32
	.bits	0,16			; _sdata3 @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("F28x_usDelay")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_F28x_usDelay")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$12)

	.dwendtag $C$DW$1

	.global	_sdata1
_sdata1:	.usect	".ebss",1,1,0
$C$DW$3	.dwtag  DW_TAG_variable
	.dwattr $C$DW$3, DW_AT_name("sdata1")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_sdata1")
	.dwattr $C$DW$3, DW_AT_location[DW_OP_addr _sdata1]
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$3, DW_AT_external

	.global	_sdata2
_sdata2:	.usect	".ebss",1,1,0
$C$DW$4	.dwtag  DW_TAG_variable
	.dwattr $C$DW$4, DW_AT_name("sdata2")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_sdata2")
	.dwattr $C$DW$4, DW_AT_location[DW_OP_addr _sdata2]
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$4, DW_AT_external

	.global	_sdata3
_sdata3:	.usect	".ebss",1,1,0
$C$DW$5	.dwtag  DW_TAG_variable
	.dwattr $C$DW$5, DW_AT_name("sdata3")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_sdata3")
	.dwattr $C$DW$5, DW_AT_location[DW_OP_addr _sdata3]
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$5, DW_AT_external

$C$DW$6	.dwtag  DW_TAG_variable
	.dwattr $C$DW$6, DW_AT_name("SpicRegs")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_SpicRegs")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external

;	D:\Softwares\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\DITTOM~1.DEV\\AppData\\Local\\Temp\\0458012 
	.sect	".text:_SIInitialize_SpiValve"
	.clink
	.global	_SIInitialize_SpiValve

$C$DW$7	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$7, DW_AT_name("SIInitialize_SpiValve")
	.dwattr $C$DW$7, DW_AT_low_pc(_SIInitialize_SpiValve)
	.dwattr $C$DW$7, DW_AT_high_pc(0x00)
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_SIInitialize_SpiValve")
	.dwattr $C$DW$7, DW_AT_external
	.dwattr $C$DW$7, DW_AT_TI_begin_file("../source/SpiInterface.c")
	.dwattr $C$DW$7, DW_AT_TI_begin_line(0x42)
	.dwattr $C$DW$7, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$7, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/SpiInterface.c",line 67,column 1,is_stmt,address _SIInitialize_SpiValve,isa 0

	.dwfde $C$DW$CIE, _SIInitialize_SpiValve
;----------------------------------------------------------------------
;  66 | void SIInitialize_SpiValve(void)                                       
;  68 | #if SPI_INTERFACE_VALVE_OPERATION                                      
;  70 | //!Reset on, falling edge with delay, 8-bit char bits                  
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SIInitialize_SpiValve        FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SIInitialize_SpiValve:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/SpiInterface.c",line 71,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  71 | SpicRegs.SPICCR.all =0x0007;                                           
;  72 | //!Enable master mode, normal phase,                                   
;  73 | //!Enable talk, and SPI int disabled.                                  
;----------------------------------------------------------------------
        MOVW      DP,#_SpicRegs         ; [CPU_U] 
        MOVB      @_SpicRegs,#7,UNC     ; [CPU_] |71| 
	.dwpsn	file "../source/SpiInterface.c",line 74,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  74 | SpicRegs.SPICTL.all =0x0006;                                           
;----------------------------------------------------------------------
        MOVB      @_SpicRegs+1,#6,UNC   ; [CPU_] |74| 
	.dwpsn	file "../source/SpiInterface.c",line 76,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  76 | SpicRegs.SPISTS.all=0x0000;                                            
;  77 | // LSPCLK/50                                                           
;----------------------------------------------------------------------
        MOV       @_SpicRegs+2,#0       ; [CPU_] |76| 
	.dwpsn	file "../source/SpiInterface.c",line 78,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  78 | SpicRegs.SPIBRR.all = 0x0031;                                          
;  79 | //!LoopBack Disabled                                                   
;----------------------------------------------------------------------
        MOVB      @_SpicRegs+4,#49,UNC  ; [CPU_] |78| 
	.dwpsn	file "../source/SpiInterface.c",line 80,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  80 | SpicRegs.SPICCR.all =0x0087;                                           
;  81 | //!Set so breakpoints don't disturb xmission                           
;----------------------------------------------------------------------
        MOVB      @_SpicRegs,#135,UNC   ; [CPU_] |80| 
	.dwpsn	file "../source/SpiInterface.c",line 82,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  82 | SpicRegs.SPIPRI.bit.FREE = 1;                                          
;  84 | //!Initialize the Spi FIFO                                             
;----------------------------------------------------------------------
        OR        @_SpicRegs+15,#0x0010 ; [CPU_] |82| 
	.dwpsn	file "../source/SpiInterface.c",line 85,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  85 | SISPI_Fifo_Initialize();                                               
;  86 | #endif                                                                 
;----------------------------------------------------------------------
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_name("_SISPI_Fifo_Initialize")
	.dwattr $C$DW$8, DW_AT_TI_call

        LCR       #_SISPI_Fifo_Initialize ; [CPU_] |85| 
        ; call occurs [#_SISPI_Fifo_Initialize] ; [] |85| 
	.dwpsn	file "../source/SpiInterface.c",line 87,column 1,is_stmt,isa 0
$C$DW$9	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$9, DW_AT_low_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$7, DW_AT_TI_end_file("../source/SpiInterface.c")
	.dwattr $C$DW$7, DW_AT_TI_end_line(0x57)
	.dwattr $C$DW$7, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$7

	.sect	".text:_SISPI_Fifo_Initialize"
	.clink
	.global	_SISPI_Fifo_Initialize

$C$DW$10	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$10, DW_AT_name("SISPI_Fifo_Initialize")
	.dwattr $C$DW$10, DW_AT_low_pc(_SISPI_Fifo_Initialize)
	.dwattr $C$DW$10, DW_AT_high_pc(0x00)
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_SISPI_Fifo_Initialize")
	.dwattr $C$DW$10, DW_AT_external
	.dwattr $C$DW$10, DW_AT_TI_begin_file("../source/SpiInterface.c")
	.dwattr $C$DW$10, DW_AT_TI_begin_line(0x62)
	.dwattr $C$DW$10, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$10, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/SpiInterface.c",line 99,column 1,is_stmt,address _SISPI_Fifo_Initialize,isa 0

	.dwfde $C$DW$CIE, _SISPI_Fifo_Initialize
;----------------------------------------------------------------------
;  98 | void SISPI_Fifo_Initialize(void)                                       
; 100 | //!Initialize SPI FIFO registers                                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SISPI_Fifo_Initialize        FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SISPI_Fifo_Initialize:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/SpiInterface.c",line 101,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 101 | SpicRegs.SPIFFTX.all=0xE040;                                           
; 102 | //!Data is transferred to SPITXBUF immediately upon completion         
; 103 | //!of transmission of the previous data                                
;----------------------------------------------------------------------
        MOVW      DP,#_SpicRegs+10      ; [CPU_U] 
        MOV       @_SpicRegs+10,#57408  ; [CPU_] |101| 
	.dwpsn	file "../source/SpiInterface.c",line 104,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 104 | SpicRegs.SPIFFCT.all=0x0000;                                           
; 106 | //SpicRegs.SPIFFRX.all=0x2044;                                         
;----------------------------------------------------------------------
        MOV       @_SpicRegs+12,#0      ; [CPU_] |104| 
	.dwpsn	file "../source/SpiInterface.c",line 107,column 1,is_stmt,isa 0
$C$DW$11	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$11, DW_AT_low_pc(0x00)
	.dwattr $C$DW$11, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$10, DW_AT_TI_end_file("../source/SpiInterface.c")
	.dwattr $C$DW$10, DW_AT_TI_end_line(0x6b)
	.dwattr $C$DW$10, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$10

	.sect	".text:_SISPI_WriteData"
	.clink
	.global	_SISPI_WriteData

$C$DW$12	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$12, DW_AT_name("SISPI_WriteData")
	.dwattr $C$DW$12, DW_AT_low_pc(_SISPI_WriteData)
	.dwattr $C$DW$12, DW_AT_high_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_SISPI_WriteData")
	.dwattr $C$DW$12, DW_AT_external
	.dwattr $C$DW$12, DW_AT_TI_begin_file("../source/SpiInterface.c")
	.dwattr $C$DW$12, DW_AT_TI_begin_line(0x78)
	.dwattr $C$DW$12, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$12, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/SpiInterface.c",line 121,column 1,is_stmt,address _SISPI_WriteData,isa 0

	.dwfde $C$DW$CIE, _SISPI_WriteData
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_name("x")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 120 | void SISPI_WriteData(Uint16 x)                                         
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SISPI_WriteData              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_SISPI_WriteData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$14	.dwtag  DW_TAG_variable
	.dwattr $C$DW$14, DW_AT_name("x")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -1]

        MOV       *-SP[1],AL            ; [CPU_] |121| 
	.dwpsn	file "../source/SpiInterface.c",line 122,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 122 | SpicRegs.SPITXBUF=x<<8;                                                
;----------------------------------------------------------------------
        MOV       ACC,*-SP[1] << #8     ; [CPU_] |122| 
        MOVW      DP,#_SpicRegs+8       ; [CPU_U] 
        MOV       @_SpicRegs+8,AL       ; [CPU_] |122| 
	.dwpsn	file "../source/SpiInterface.c",line 123,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$15	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$15, DW_AT_low_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$12, DW_AT_TI_end_file("../source/SpiInterface.c")
	.dwattr $C$DW$12, DW_AT_TI_end_line(0x7b)
	.dwattr $C$DW$12, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$12

	.sect	".text:_SISPI_ReadData"
	.clink
	.global	_SISPI_ReadData

$C$DW$16	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$16, DW_AT_name("SISPI_ReadData")
	.dwattr $C$DW$16, DW_AT_low_pc(_SISPI_ReadData)
	.dwattr $C$DW$16, DW_AT_high_pc(0x00)
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_SISPI_ReadData")
	.dwattr $C$DW$16, DW_AT_external
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$16, DW_AT_TI_begin_file("../source/SpiInterface.c")
	.dwattr $C$DW$16, DW_AT_TI_begin_line(0x86)
	.dwattr $C$DW$16, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$16, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/SpiInterface.c",line 135,column 1,is_stmt,address _SISPI_ReadData,isa 0

	.dwfde $C$DW$CIE, _SISPI_ReadData
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_name("rdata1")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_rdata1")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 134 | Uint16 SISPI_ReadData(Uint16 rdata1)                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SISPI_ReadData               FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_SISPI_ReadData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$18	.dwtag  DW_TAG_variable
	.dwattr $C$DW$18, DW_AT_name("rdata1")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_rdata1")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -1]

        MOV       *-SP[1],AL            ; [CPU_] |135| 
	.dwpsn	file "../source/SpiInterface.c",line 136,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 136 | rdata1=SpicRegs.SPIRXBUF;                                              
;----------------------------------------------------------------------
        MOVW      DP,#_SpicRegs+7       ; [CPU_U] 
        MOV       AL,@_SpicRegs+7       ; [CPU_] |136| 
        MOV       *-SP[1],AL            ; [CPU_] |136| 
	.dwpsn	file "../source/SpiInterface.c",line 137,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 137 | return rdata1;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../source/SpiInterface.c",line 138,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$16, DW_AT_TI_end_file("../source/SpiInterface.c")
	.dwattr $C$DW$16, DW_AT_TI_end_line(0x8a)
	.dwattr $C$DW$16, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$16

	.sect	".text:_SIDelay_Loop"
	.clink
	.global	_SIDelay_Loop

$C$DW$20	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$20, DW_AT_name("SIDelay_Loop")
	.dwattr $C$DW$20, DW_AT_low_pc(_SIDelay_Loop)
	.dwattr $C$DW$20, DW_AT_high_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_SIDelay_Loop")
	.dwattr $C$DW$20, DW_AT_external
	.dwattr $C$DW$20, DW_AT_TI_begin_file("../source/SpiInterface.c")
	.dwattr $C$DW$20, DW_AT_TI_begin_line(0x95)
	.dwattr $C$DW$20, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$20, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/SpiInterface.c",line 150,column 1,is_stmt,address _SIDelay_Loop,isa 0

	.dwfde $C$DW$CIE, _SIDelay_Loop
;----------------------------------------------------------------------
; 149 | void SIDelay_Loop(void)                                                
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SIDelay_Loop                 FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SIDelay_Loop:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/SpiInterface.c",line 151,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 151 | DELAY_US(800);                                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#31998           ; [CPU_U] |151| 
        MOVL      ACC,XAR4              ; [CPU_] |151| 
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("_F28x_usDelay")
	.dwattr $C$DW$21, DW_AT_TI_call

        LCR       #_F28x_usDelay        ; [CPU_] |151| 
        ; call occurs [#_F28x_usDelay] ; [] |151| 
	.dwpsn	file "../source/SpiInterface.c",line 152,column 1,is_stmt,isa 0
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$20, DW_AT_TI_end_file("../source/SpiInterface.c")
	.dwattr $C$DW$20, DW_AT_TI_end_line(0x98)
	.dwattr $C$DW$20, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$20

	.sect	".text:_SIUpdateValveStatus"
	.clink
	.global	_SIUpdateValveStatus

$C$DW$23	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$23, DW_AT_name("SIUpdateValveStatus")
	.dwattr $C$DW$23, DW_AT_low_pc(_SIUpdateValveStatus)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_SIUpdateValveStatus")
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_TI_begin_file("../source/SpiInterface.c")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0xa6)
	.dwattr $C$DW$23, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/SpiInterface.c",line 167,column 1,is_stmt,address _SIUpdateValveStatus,isa 0

	.dwfde $C$DW$CIE, _SIUpdateValveStatus
$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_name("ValveStatus_1")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_ValveStatus_1")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg0]

$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_name("ValveStatus_2")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_ValveStatus_2")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 166 | void SIUpdateValveStatus(Uint16 ValveStatus_1, Uint16 ValveStatus_2)   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SIUpdateValveStatus          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SIUpdateValveStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$26	.dwtag  DW_TAG_variable
	.dwattr $C$DW$26, DW_AT_name("ValveStatus_1")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_ValveStatus_1")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -1]

$C$DW$27	.dwtag  DW_TAG_variable
	.dwattr $C$DW$27, DW_AT_name("ValveStatus_2")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_ValveStatus_2")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 169 | #if SPI_INTERFACE_VALVE_OPERATION                                      
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |167| 
        MOV       *-SP[1],AL            ; [CPU_] |167| 
	.dwpsn	file "../source/SpiInterface.c",line 170,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 170 | if(sdata2 != ValveStatus_2 || sdata3 != ValveStatus_1)                 
; 172 |         //!Read data from model                                        
;----------------------------------------------------------------------
        MOVW      DP,#_sdata2           ; [CPU_U] 
        MOVZ      AR6,@_sdata2          ; [CPU_] |170| 
        MOVU      ACC,*-SP[2]           ; [CPU_] |170| 
        CMPL      ACC,XAR6              ; [CPU_] |170| 
        B         $C$L1,NEQ             ; [CPU_] |170| 
        ; branchcc occurs ; [] |170| 
        MOVZ      AR6,@_sdata3          ; [CPU_] |170| 
        MOVU      ACC,*-SP[1]           ; [CPU_] |170| 
        CMPL      ACC,XAR6              ; [CPU_] |170| 
        B         $C$L2,EQ              ; [CPU_] |170| 
        ; branchcc occurs ; [] |170| 
$C$L1:    
	.dwpsn	file "../source/SpiInterface.c",line 173,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 173 | sdata3 = ValveStatus_1;                                                
;----------------------------------------------------------------------
        MOV       AL,*-SP[1]            ; [CPU_] |173| 
        MOV       @_sdata3,AL           ; [CPU_] |173| 
	.dwpsn	file "../source/SpiInterface.c",line 174,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 174 | sdata2 = ValveStatus_2;                                                
; 176 | //!Shift data by 8 bits                                                
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |174| 
        MOV       @_sdata2,AL           ; [CPU_] |174| 
	.dwpsn	file "../source/SpiInterface.c",line 177,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 177 | sdata1 = 0x0000;                                                       
; 179 | //!Write sdata1 to SPI                                                 
;----------------------------------------------------------------------
        MOV       @_sdata1,#0           ; [CPU_] |177| 
	.dwpsn	file "../source/SpiInterface.c",line 180,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 180 | SISPI_WriteData(sdata1);                                               
; 181 | //!Write sdata2 to SPI                                                 
;----------------------------------------------------------------------
        MOV       AL,@_sdata1           ; [CPU_] |180| 
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_SISPI_WriteData")
	.dwattr $C$DW$28, DW_AT_TI_call

        LCR       #_SISPI_WriteData     ; [CPU_] |180| 
        ; call occurs [#_SISPI_WriteData] ; [] |180| 
	.dwpsn	file "../source/SpiInterface.c",line 182,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 182 | SISPI_WriteData(sdata2);                                               
; 183 | //!Write sdata3 to SPI                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_sdata2           ; [CPU_U] 
        MOV       AL,@_sdata2           ; [CPU_] |182| 
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("_SISPI_WriteData")
	.dwattr $C$DW$29, DW_AT_TI_call

        LCR       #_SISPI_WriteData     ; [CPU_] |182| 
        ; call occurs [#_SISPI_WriteData] ; [] |182| 
	.dwpsn	file "../source/SpiInterface.c",line 184,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 184 | SISPI_WriteData(sdata3);                                               
; 193 | //      DELAY_US(100);                                                 
; 194 | #endif                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_sdata3           ; [CPU_U] 
        MOV       AL,@_sdata3           ; [CPU_] |184| 
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("_SISPI_WriteData")
	.dwattr $C$DW$30, DW_AT_TI_call

        LCR       #_SISPI_WriteData     ; [CPU_] |184| 
        ; call occurs [#_SISPI_WriteData] ; [] |184| 
	.dwpsn	file "../source/SpiInterface.c",line 195,column 1,is_stmt,isa 0
$C$L2:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$23, DW_AT_TI_end_file("../source/SpiInterface.c")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0xc3)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$23

	.sect	".text:_SIErrorSpiValve"
	.clink
	.global	_SIErrorSpiValve

$C$DW$32	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$32, DW_AT_name("SIErrorSpiValve")
	.dwattr $C$DW$32, DW_AT_low_pc(_SIErrorSpiValve)
	.dwattr $C$DW$32, DW_AT_high_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_SIErrorSpiValve")
	.dwattr $C$DW$32, DW_AT_external
	.dwattr $C$DW$32, DW_AT_TI_begin_file("../source/SpiInterface.c")
	.dwattr $C$DW$32, DW_AT_TI_begin_line(0xce)
	.dwattr $C$DW$32, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$32, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/SpiInterface.c",line 207,column 1,is_stmt,address _SIErrorSpiValve,isa 0

	.dwfde $C$DW$CIE, _SIErrorSpiValve
;----------------------------------------------------------------------
; 206 | void SIErrorSpiValve(void)                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SIErrorSpiValve              FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SIErrorSpiValve:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/SpiInterface.c",line 210,column 1,is_stmt,isa 0
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$32, DW_AT_TI_end_file("../source/SpiInterface.c")
	.dwattr $C$DW$32, DW_AT_TI_end_line(0xd2)
	.dwattr $C$DW$32, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$32

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_F28x_usDelay
	.global	_SpicRegs

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_name("SPIBRR_BITS")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x01)
$C$DW$34	.dwtag  DW_TAG_member
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$34, DW_AT_name("SPI_BIT_RATE")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_SPI_BIT_RATE")
	.dwattr $C$DW$34, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$34, DW_AT_bit_size(0x07)
	.dwattr $C$DW$34, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$34, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$35	.dwtag  DW_TAG_member
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$35, DW_AT_name("rsvd1")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$35, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$35, DW_AT_bit_size(0x09)
	.dwattr $C$DW$35, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$35, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$20


$C$DW$T$21	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$21, DW_AT_name("SPIBRR_REG")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$36	.dwtag  DW_TAG_member
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$36, DW_AT_name("all")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$36, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$36, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$37	.dwtag  DW_TAG_member
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$37, DW_AT_name("bit")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$37, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$37, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$21


$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_name("SPICCR_BITS")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x01)
$C$DW$38	.dwtag  DW_TAG_member
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$38, DW_AT_name("SPICHAR")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_SPICHAR")
	.dwattr $C$DW$38, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$38, DW_AT_bit_size(0x04)
	.dwattr $C$DW$38, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$38, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$39	.dwtag  DW_TAG_member
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$39, DW_AT_name("SPILBK")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_SPILBK")
	.dwattr $C$DW$39, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$39, DW_AT_bit_size(0x01)
	.dwattr $C$DW$39, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$39, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$40	.dwtag  DW_TAG_member
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$40, DW_AT_name("HS_MODE")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_HS_MODE")
	.dwattr $C$DW$40, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$40, DW_AT_bit_size(0x01)
	.dwattr $C$DW$40, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$40, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$41	.dwtag  DW_TAG_member
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$41, DW_AT_name("CLKPOLARITY")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_CLKPOLARITY")
	.dwattr $C$DW$41, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$41, DW_AT_bit_size(0x01)
	.dwattr $C$DW$41, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$41, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$42	.dwtag  DW_TAG_member
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$42, DW_AT_name("SPISWRESET")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_SPISWRESET")
	.dwattr $C$DW$42, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$42, DW_AT_bit_size(0x01)
	.dwattr $C$DW$42, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$42, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$43	.dwtag  DW_TAG_member
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$43, DW_AT_name("rsvd1")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$43, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$43, DW_AT_bit_size(0x08)
	.dwattr $C$DW$43, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$22


$C$DW$T$23	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$23, DW_AT_name("SPICCR_REG")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x01)
$C$DW$44	.dwtag  DW_TAG_member
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$44, DW_AT_name("all")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$44, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$44, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$45	.dwtag  DW_TAG_member
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$45, DW_AT_name("bit")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$45, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$45, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$23


$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_name("SPICTL_BITS")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$46	.dwtag  DW_TAG_member
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$46, DW_AT_name("SPIINTENA")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_SPIINTENA")
	.dwattr $C$DW$46, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$46, DW_AT_bit_size(0x01)
	.dwattr $C$DW$46, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$46, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$47	.dwtag  DW_TAG_member
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$47, DW_AT_name("TALK")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_TALK")
	.dwattr $C$DW$47, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$47, DW_AT_bit_size(0x01)
	.dwattr $C$DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$47, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$48	.dwtag  DW_TAG_member
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$48, DW_AT_name("MASTER_SLAVE")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_MASTER_SLAVE")
	.dwattr $C$DW$48, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$48, DW_AT_bit_size(0x01)
	.dwattr $C$DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$48, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$49	.dwtag  DW_TAG_member
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$49, DW_AT_name("CLK_PHASE")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_CLK_PHASE")
	.dwattr $C$DW$49, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$49, DW_AT_bit_size(0x01)
	.dwattr $C$DW$49, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$49, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$50	.dwtag  DW_TAG_member
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$50, DW_AT_name("OVERRUNINTENA")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_OVERRUNINTENA")
	.dwattr $C$DW$50, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$50, DW_AT_bit_size(0x01)
	.dwattr $C$DW$50, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$50, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$51	.dwtag  DW_TAG_member
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$51, DW_AT_name("rsvd1")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$51, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$51, DW_AT_bit_size(0x0b)
	.dwattr $C$DW$51, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$51, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$25, DW_AT_name("SPICTL_REG")
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$52	.dwtag  DW_TAG_member
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$52, DW_AT_name("all")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$52, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$52, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$53	.dwtag  DW_TAG_member
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$53, DW_AT_name("bit")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_name("SPIFFCT_BITS")
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x01)
$C$DW$54	.dwtag  DW_TAG_member
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$54, DW_AT_name("TXDLY")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_TXDLY")
	.dwattr $C$DW$54, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$54, DW_AT_bit_size(0x08)
	.dwattr $C$DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$54, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$55	.dwtag  DW_TAG_member
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$55, DW_AT_name("rsvd1")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$55, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$55, DW_AT_bit_size(0x08)
	.dwattr $C$DW$55, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$55, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$27, DW_AT_name("SPIFFCT_REG")
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$56	.dwtag  DW_TAG_member
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$56, DW_AT_name("all")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$56, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$56, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$57	.dwtag  DW_TAG_member
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$57, DW_AT_name("bit")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$57, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$57, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$27


$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_name("SPIFFRX_BITS")
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x01)
$C$DW$58	.dwtag  DW_TAG_member
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$58, DW_AT_name("RXFFIL")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_RXFFIL")
	.dwattr $C$DW$58, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$58, DW_AT_bit_size(0x05)
	.dwattr $C$DW$58, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$58, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$59	.dwtag  DW_TAG_member
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$59, DW_AT_name("RXFFIENA")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_RXFFIENA")
	.dwattr $C$DW$59, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$59, DW_AT_bit_size(0x01)
	.dwattr $C$DW$59, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$59, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$60	.dwtag  DW_TAG_member
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$60, DW_AT_name("RXFFINTCLR")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_RXFFINTCLR")
	.dwattr $C$DW$60, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$60, DW_AT_bit_size(0x01)
	.dwattr $C$DW$60, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$60, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$61	.dwtag  DW_TAG_member
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$61, DW_AT_name("RXFFINT")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_RXFFINT")
	.dwattr $C$DW$61, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$61, DW_AT_bit_size(0x01)
	.dwattr $C$DW$61, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$61, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$62	.dwtag  DW_TAG_member
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$62, DW_AT_name("RXFFST")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_RXFFST")
	.dwattr $C$DW$62, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$62, DW_AT_bit_size(0x05)
	.dwattr $C$DW$62, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$62, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$63	.dwtag  DW_TAG_member
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$63, DW_AT_name("RXFIFORESET")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_RXFIFORESET")
	.dwattr $C$DW$63, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$63, DW_AT_bit_size(0x01)
	.dwattr $C$DW$63, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$63, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$64	.dwtag  DW_TAG_member
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$64, DW_AT_name("RXFFOVFCLR")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_RXFFOVFCLR")
	.dwattr $C$DW$64, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$64, DW_AT_bit_size(0x01)
	.dwattr $C$DW$64, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$64, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$65	.dwtag  DW_TAG_member
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$65, DW_AT_name("RXFFOVF")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_RXFFOVF")
	.dwattr $C$DW$65, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$65, DW_AT_bit_size(0x01)
	.dwattr $C$DW$65, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$65, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$28


$C$DW$T$29	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$29, DW_AT_name("SPIFFRX_REG")
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x01)
$C$DW$66	.dwtag  DW_TAG_member
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$66, DW_AT_name("all")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$66, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$66, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$67	.dwtag  DW_TAG_member
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$67, DW_AT_name("bit")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$67, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$67, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_name("SPIFFTX_BITS")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x01)
$C$DW$68	.dwtag  DW_TAG_member
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$68, DW_AT_name("TXFFIL")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_TXFFIL")
	.dwattr $C$DW$68, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$68, DW_AT_bit_size(0x05)
	.dwattr $C$DW$68, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$68, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$69	.dwtag  DW_TAG_member
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$69, DW_AT_name("TXFFIENA")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_TXFFIENA")
	.dwattr $C$DW$69, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$69, DW_AT_bit_size(0x01)
	.dwattr $C$DW$69, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$69, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$70	.dwtag  DW_TAG_member
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$70, DW_AT_name("TXFFINTCLR")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_TXFFINTCLR")
	.dwattr $C$DW$70, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$70, DW_AT_bit_size(0x01)
	.dwattr $C$DW$70, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$70, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$71	.dwtag  DW_TAG_member
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$71, DW_AT_name("TXFFINT")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_TXFFINT")
	.dwattr $C$DW$71, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$71, DW_AT_bit_size(0x01)
	.dwattr $C$DW$71, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$71, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$72	.dwtag  DW_TAG_member
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$72, DW_AT_name("TXFFST")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_TXFFST")
	.dwattr $C$DW$72, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$72, DW_AT_bit_size(0x05)
	.dwattr $C$DW$72, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$72, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$73	.dwtag  DW_TAG_member
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$73, DW_AT_name("TXFIFO")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_TXFIFO")
	.dwattr $C$DW$73, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$73, DW_AT_bit_size(0x01)
	.dwattr $C$DW$73, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$73, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$74	.dwtag  DW_TAG_member
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$74, DW_AT_name("SPIFFENA")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_SPIFFENA")
	.dwattr $C$DW$74, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$74, DW_AT_bit_size(0x01)
	.dwattr $C$DW$74, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$74, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$75	.dwtag  DW_TAG_member
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$75, DW_AT_name("SPIRST")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_SPIRST")
	.dwattr $C$DW$75, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$75, DW_AT_bit_size(0x01)
	.dwattr $C$DW$75, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$75, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$31, DW_AT_name("SPIFFTX_REG")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$76	.dwtag  DW_TAG_member
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$76, DW_AT_name("all")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$76, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$76, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$77	.dwtag  DW_TAG_member
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$77, DW_AT_name("bit")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$77, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$77, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$32, DW_AT_name("SPIPRI_BITS")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x01)
$C$DW$78	.dwtag  DW_TAG_member
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$78, DW_AT_name("TRIWIRE")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_TRIWIRE")
	.dwattr $C$DW$78, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$78, DW_AT_bit_size(0x01)
	.dwattr $C$DW$78, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$78, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$79	.dwtag  DW_TAG_member
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$79, DW_AT_name("STEINV")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_STEINV")
	.dwattr $C$DW$79, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$79, DW_AT_bit_size(0x01)
	.dwattr $C$DW$79, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$79, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$80	.dwtag  DW_TAG_member
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$80, DW_AT_name("rsvd1")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$80, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$80, DW_AT_bit_size(0x02)
	.dwattr $C$DW$80, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$80, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$81	.dwtag  DW_TAG_member
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$81, DW_AT_name("FREE")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_FREE")
	.dwattr $C$DW$81, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$81, DW_AT_bit_size(0x01)
	.dwattr $C$DW$81, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$81, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$82	.dwtag  DW_TAG_member
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$82, DW_AT_name("SOFT")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_SOFT")
	.dwattr $C$DW$82, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$82, DW_AT_bit_size(0x01)
	.dwattr $C$DW$82, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$82, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$83	.dwtag  DW_TAG_member
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$83, DW_AT_name("rsvd2")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$83, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$83, DW_AT_bit_size(0x01)
	.dwattr $C$DW$83, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$83, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$84	.dwtag  DW_TAG_member
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$84, DW_AT_name("rsvd3")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$84, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$84, DW_AT_bit_size(0x09)
	.dwattr $C$DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$84, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$33, DW_AT_name("SPIPRI_REG")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x01)
$C$DW$85	.dwtag  DW_TAG_member
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$85, DW_AT_name("all")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$85, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$86	.dwtag  DW_TAG_member
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$86, DW_AT_name("bit")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$86, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$86, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$34, DW_AT_name("SPISTS_BITS")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x01)
$C$DW$87	.dwtag  DW_TAG_member
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$87, DW_AT_name("rsvd1")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$87, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$87, DW_AT_bit_size(0x05)
	.dwattr $C$DW$87, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$87, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$88	.dwtag  DW_TAG_member
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$88, DW_AT_name("BUFFULL_FLAG")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_BUFFULL_FLAG")
	.dwattr $C$DW$88, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$88, DW_AT_bit_size(0x01)
	.dwattr $C$DW$88, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$88, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$89	.dwtag  DW_TAG_member
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$89, DW_AT_name("INT_FLAG")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_INT_FLAG")
	.dwattr $C$DW$89, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$89, DW_AT_bit_size(0x01)
	.dwattr $C$DW$89, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$89, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$90	.dwtag  DW_TAG_member
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$90, DW_AT_name("OVERRUN_FLAG")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_OVERRUN_FLAG")
	.dwattr $C$DW$90, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$90, DW_AT_bit_size(0x01)
	.dwattr $C$DW$90, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$90, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$91	.dwtag  DW_TAG_member
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$91, DW_AT_name("rsvd2")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$91, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$91, DW_AT_bit_size(0x08)
	.dwattr $C$DW$91, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$91, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$35, DW_AT_name("SPISTS_REG")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x01)
$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$92, DW_AT_name("all")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$92, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$93	.dwtag  DW_TAG_member
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$93, DW_AT_name("bit")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$93, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$35


$C$DW$T$37	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$37, DW_AT_name("SPI_REGS")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x10)
$C$DW$94	.dwtag  DW_TAG_member
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$94, DW_AT_name("SPICCR")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_SPICCR")
	.dwattr $C$DW$94, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$94, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$95	.dwtag  DW_TAG_member
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$95, DW_AT_name("SPICTL")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_SPICTL")
	.dwattr $C$DW$95, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$96	.dwtag  DW_TAG_member
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$96, DW_AT_name("SPISTS")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_SPISTS")
	.dwattr $C$DW$96, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$96, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$97	.dwtag  DW_TAG_member
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$97, DW_AT_name("rsvd1")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$97, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$97, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$98	.dwtag  DW_TAG_member
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$98, DW_AT_name("SPIBRR")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_SPIBRR")
	.dwattr $C$DW$98, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$98, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$99, DW_AT_name("rsvd2")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$99, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$100, DW_AT_name("SPIRXEMU")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_SPIRXEMU")
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$101, DW_AT_name("SPIRXBUF")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_SPIRXBUF")
	.dwattr $C$DW$101, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$102, DW_AT_name("SPITXBUF")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_SPITXBUF")
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$103	.dwtag  DW_TAG_member
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$103, DW_AT_name("SPIDAT")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_SPIDAT")
	.dwattr $C$DW$103, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$104	.dwtag  DW_TAG_member
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$104, DW_AT_name("SPIFFTX")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_SPIFFTX")
	.dwattr $C$DW$104, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$104, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$105	.dwtag  DW_TAG_member
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$105, DW_AT_name("SPIFFRX")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_SPIFFRX")
	.dwattr $C$DW$105, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$106	.dwtag  DW_TAG_member
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$106, DW_AT_name("SPIFFCT")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_SPIFFCT")
	.dwattr $C$DW$106, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$106, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$107	.dwtag  DW_TAG_member
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$107, DW_AT_name("rsvd3")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$107, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$107, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$108	.dwtag  DW_TAG_member
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$108, DW_AT_name("SPIPRI")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_SPIPRI")
	.dwattr $C$DW$108, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$108, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$37

$C$DW$109	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$37)

$C$DW$T$46	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$109)

$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$19	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$19, DW_AT_name("Uint16")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)


$C$DW$T$36	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$110	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$110, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$36

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$111	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$111, DW_AT_name("AL")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_reg0]

$C$DW$112	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$112, DW_AT_name("AH")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg1]

$C$DW$113	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$113, DW_AT_name("PL")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg2]

$C$DW$114	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$114, DW_AT_name("PH")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg3]

$C$DW$115	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$115, DW_AT_name("SP")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg20]

$C$DW$116	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$116, DW_AT_name("XT")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_reg21]

$C$DW$117	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$117, DW_AT_name("T")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_reg22]

$C$DW$118	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$118, DW_AT_name("ST0")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg23]

$C$DW$119	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$119, DW_AT_name("ST1")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg24]

$C$DW$120	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$120, DW_AT_name("PC")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg25]

$C$DW$121	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$121, DW_AT_name("RPC")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_reg26]

$C$DW$122	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$122, DW_AT_name("FP")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg28]

$C$DW$123	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$123, DW_AT_name("DP")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_reg29]

$C$DW$124	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$124, DW_AT_name("SXM")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_reg30]

$C$DW$125	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$125, DW_AT_name("PM")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_reg31]

$C$DW$126	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$126, DW_AT_name("OVM")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x20]

$C$DW$127	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$127, DW_AT_name("PAGE0")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x21]

$C$DW$128	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$128, DW_AT_name("AMODE")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x22]

$C$DW$129	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$129, DW_AT_name("INTM")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x23]

$C$DW$130	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$130, DW_AT_name("IFR")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x24]

$C$DW$131	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$131, DW_AT_name("IER")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x25]

$C$DW$132	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$132, DW_AT_name("V")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x26]

$C$DW$133	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$133, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$134	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$134, DW_AT_name("VOL")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$135	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$135, DW_AT_name("AR0")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_reg4]

$C$DW$136	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$136, DW_AT_name("XAR0")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_reg5]

$C$DW$137	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$137, DW_AT_name("AR1")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_reg6]

$C$DW$138	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$138, DW_AT_name("XAR1")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_reg7]

$C$DW$139	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$139, DW_AT_name("AR2")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_reg8]

$C$DW$140	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$140, DW_AT_name("XAR2")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_reg9]

$C$DW$141	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$141, DW_AT_name("AR3")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg10]

$C$DW$142	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$142, DW_AT_name("XAR3")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_reg11]

$C$DW$143	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$143, DW_AT_name("AR4")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg12]

$C$DW$144	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$144, DW_AT_name("XAR4")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_reg13]

$C$DW$145	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$145, DW_AT_name("AR5")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_reg14]

$C$DW$146	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$146, DW_AT_name("XAR5")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_reg15]

$C$DW$147	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$147, DW_AT_name("AR6")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg16]

$C$DW$148	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$148, DW_AT_name("XAR6")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg17]

$C$DW$149	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$149, DW_AT_name("AR7")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_reg18]

$C$DW$150	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$150, DW_AT_name("XAR7")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_reg19]

$C$DW$151	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$151, DW_AT_name("R0HL")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x29]

$C$DW$152	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$152, DW_AT_name("R0H")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$153	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$153, DW_AT_name("R1HL")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$154	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$154, DW_AT_name("R1H")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$155	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$155, DW_AT_name("R2HL")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$156	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$156, DW_AT_name("R2H")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$157	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$157, DW_AT_name("R3HL")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$158	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$158, DW_AT_name("R3H")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x30]

$C$DW$159	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$159, DW_AT_name("R4HL")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x31]

$C$DW$160	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$160, DW_AT_name("R4H")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x32]

$C$DW$161	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$161, DW_AT_name("R5HL")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x33]

$C$DW$162	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$162, DW_AT_name("R5H")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0x34]

$C$DW$163	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$163, DW_AT_name("R6HL")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x35]

$C$DW$164	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$164, DW_AT_name("R6H")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_regx 0x36]

$C$DW$165	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$165, DW_AT_name("R7HL")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_regx 0x37]

$C$DW$166	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$166, DW_AT_name("R7H")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_regx 0x38]

$C$DW$167	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$167, DW_AT_name("RBL")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x49]

$C$DW$168	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$168, DW_AT_name("RB")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$169	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$169, DW_AT_name("STFL")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x27]

$C$DW$170	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$170, DW_AT_name("STF")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_regx 0x28]

$C$DW$171	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$171, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


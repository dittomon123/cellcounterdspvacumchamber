;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Fri Jan 29 15:14:29 2021                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../source/PressureSense.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cell Counter\V1.3.1_And_V1.3.0\alphaCellCounterCPU1\CPU1_FLASH")

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("ADAcquirePressureData")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_ADAcquirePressureData")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$26)

	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$3, DW_AT_name("ADGetAdcBufAddr")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_ADGetAdcBufAddr")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
	.dwendtag $C$DW$3

	.global	_PressureSense
_PressureSense:	.usect	".ebss",20,1,1
$C$DW$4	.dwtag  DW_TAG_variable
	.dwattr $C$DW$4, DW_AT_name("PressureSense")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_PressureSense")
	.dwattr $C$DW$4, DW_AT_location[DW_OP_addr _PressureSense]
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$4, DW_AT_external

;	D:\Softwares\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\DITTOM~1.DEV\\AppData\\Local\\Temp\\2236412 
	.sect	".text:_PSGetPressureSense"
	.clink
	.global	_PSGetPressureSense

$C$DW$5	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$5, DW_AT_name("PSGetPressureSense")
	.dwattr $C$DW$5, DW_AT_low_pc(_PSGetPressureSense)
	.dwattr $C$DW$5, DW_AT_high_pc(0x00)
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_PSGetPressureSense")
	.dwattr $C$DW$5, DW_AT_external
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$5, DW_AT_TI_begin_file("../source/PressureSense.c")
	.dwattr $C$DW$5, DW_AT_TI_begin_line(0x59)
	.dwattr $C$DW$5, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$5, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/PressureSense.c",line 90,column 1,is_stmt,address _PSGetPressureSense,isa 0

	.dwfde $C$DW$CIE, _PSGetPressureSense
;----------------------------------------------------------------------
;  89 | PressureParameter* PSGetPressureSense(void)                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _PSGetPressureSense           FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_PSGetPressureSense:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/PressureSense.c",line 91,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  91 | return &PressureSense;                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,#_PressureSense  ; [CPU_U] |91| 
	.dwpsn	file "../source/PressureSense.c",line 92,column 1,is_stmt,isa 0
$C$DW$6	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$6, DW_AT_low_pc(0x00)
	.dwattr $C$DW$6, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$5, DW_AT_TI_end_file("../source/PressureSense.c")
	.dwattr $C$DW$5, DW_AT_TI_end_line(0x5c)
	.dwattr $C$DW$5, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$5

	.sect	".text:_PSInitPressureModule"
	.clink
	.global	_PSInitPressureModule

$C$DW$7	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$7, DW_AT_name("PSInitPressureModule")
	.dwattr $C$DW$7, DW_AT_low_pc(_PSInitPressureModule)
	.dwattr $C$DW$7, DW_AT_high_pc(0x00)
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_PSInitPressureModule")
	.dwattr $C$DW$7, DW_AT_external
	.dwattr $C$DW$7, DW_AT_TI_begin_file("../source/PressureSense.c")
	.dwattr $C$DW$7, DW_AT_TI_begin_line(0x68)
	.dwattr $C$DW$7, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$7, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/PressureSense.c",line 105,column 1,is_stmt,address _PSInitPressureModule,isa 0

	.dwfde $C$DW$CIE, _PSInitPressureModule
;----------------------------------------------------------------------
; 104 | void PSInitPressureModule(void)                                        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _PSInitPressureModule         FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_PSInitPressureModule:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/PressureSense.c",line 106,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 106 | PressureSense.ADCMin = (ADC_INPUT_VOLTS_FOR_MIN_PRESSURE*(ADC_COUNTS_SP
;     | AN/ADC_REFERENCE));  //      450.56                                    
;----------------------------------------------------------------------
        MOVIZ     R0H,#17377            ; [CPU_] |106| 
        MOVW      DP,#_PressureSense+14 ; [CPU_U] 
        MOVXI     R0H,#14746            ; [CPU_] |106| 
        MOV32     @_PressureSense+14,R0H ; [CPU_] |106| 
	.dwpsn	file "../source/PressureSense.c",line 107,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 107 | PressureSense.ADCMax = ADC_INPUT_VOLTS_FOR_MAX_PRESSURE*(ADC_COUNTS_SPA
;     | N/ADC_REFERENCE);        //      3604.48                               
;----------------------------------------------------------------------
        MOVIZ     R0H,#17761            ; [CPU_] |107| 
        MOVXI     R0H,#14746            ; [CPU_] |107| 
        MOV32     @_PressureSense+16,R0H ; [CPU_] |107| 
	.dwpsn	file "../source/PressureSense.c",line 108,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 108 | PressureSense.ADCSpan = PressureSense.ADCMax - PressureSense.ADCMin;   
;----------------------------------------------------------------------
        MOV32     R1H,@_PressureSense+16 ; [CPU_] |108| 
        MOV32     R0H,@_PressureSense+14 ; [CPU_] |108| 
        SUBF32    R0H,R1H,R0H           ; [CPU_] |108| 
        NOP       ; [CPU_] 
        MOV32     @_PressureSense+4,R0H ; [CPU_] |108| 
	.dwpsn	file "../source/PressureSense.c",line 109,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 109 | PressureSense.PressureValueMin = PRESSURE_MIN;  //psi                  
;----------------------------------------------------------------------
        MOVIZ     R0H,#49648            ; [CPU_] |109| 
        MOV32     @_PressureSense+10,R0H ; [CPU_] |109| 
	.dwpsn	file "../source/PressureSense.c",line 110,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 110 | PressureSense.PressureValueMax = PRESSURE_MAX;  //psi                  
;----------------------------------------------------------------------
        MOVIZ     R0H,#16880            ; [CPU_] |110| 
        MOV32     @_PressureSense+12,R0H ; [CPU_] |110| 
	.dwpsn	file "../source/PressureSense.c",line 111,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 111 | PressureSense.PressureSpan = PressureSense.PressureValueMax - \        
; 112 |         PressureSense.PressureValueMin;                                
;----------------------------------------------------------------------
        MOV32     R0H,@_PressureSense+10 ; [CPU_] |111| 
        MOV32     R1H,@_PressureSense+12 ; [CPU_] |111| 
        SUBF32    R0H,R1H,R0H           ; [CPU_] |111| 
        NOP       ; [CPU_] 
        MOV32     @_PressureSense+8,R0H ; [CPU_] |111| 
	.dwpsn	file "../source/PressureSense.c",line 113,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 113 | PressureSense.PRESSURE_Err.PressureErrStatus = 0x00;                   
;----------------------------------------------------------------------
        MOV       @_PressureSense+18,#0 ; [CPU_] |113| 
	.dwpsn	file "../source/PressureSense.c",line 114,column 1,is_stmt,isa 0
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$7, DW_AT_TI_end_file("../source/PressureSense.c")
	.dwattr $C$DW$7, DW_AT_TI_end_line(0x72)
	.dwattr $C$DW$7, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$7

	.sect	".text:_PSGetPressureData"
	.clink
	.global	_PSGetPressureData

$C$DW$9	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$9, DW_AT_name("PSGetPressureData")
	.dwattr $C$DW$9, DW_AT_low_pc(_PSGetPressureData)
	.dwattr $C$DW$9, DW_AT_high_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_PSGetPressureData")
	.dwattr $C$DW$9, DW_AT_external
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$9, DW_AT_TI_begin_file("../source/PressureSense.c")
	.dwattr $C$DW$9, DW_AT_TI_begin_line(0x82)
	.dwattr $C$DW$9, DW_AT_TI_begin_column(0x0e)
	.dwattr $C$DW$9, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/PressureSense.c",line 131,column 1,is_stmt,address _PSGetPressureData,isa 0

	.dwfde $C$DW$CIE, _PSGetPressureData
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_name("Data")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_Data")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg12]

$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_name("DataBufSize")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_DataBufSize")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 130 | unsigned int PSGetPressureData(unsigned int *Data, unsigned int DataBuf
;     | Size)                                                                  
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _PSGetPressureData            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_PSGetPressureData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$12	.dwtag  DW_TAG_variable
	.dwattr $C$DW$12, DW_AT_name("Data")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_Data")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_breg20 -2]

$C$DW$13	.dwtag  DW_TAG_variable
	.dwattr $C$DW$13, DW_AT_name("AverageData")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_AverageData")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_breg20 -4]

$C$DW$14	.dwtag  DW_TAG_variable
	.dwattr $C$DW$14, DW_AT_name("DataBufSize")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_DataBufSize")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -5]

$C$DW$15	.dwtag  DW_TAG_variable
	.dwattr $C$DW$15, DW_AT_name("i")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -6]

        MOV       *-SP[5],AL            ; [CPU_] |131| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |131| 
	.dwpsn	file "../source/PressureSense.c",line 132,column 27,is_stmt,isa 0
;----------------------------------------------------------------------
; 132 | unsigned long AverageData=0;                                           
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |132| 
        MOVL      *-SP[4],ACC           ; [CPU_] |132| 
	.dwpsn	file "../source/PressureSense.c",line 133,column 16,is_stmt,isa 0
;----------------------------------------------------------------------
; 133 | unsigned int i=0;                                                      
; 134 | //      static unsigned long AvgData[]                                 
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |133| 
	.dwpsn	file "../source/PressureSense.c",line 135,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 135 | if(DataBufSize > 0)                                                    
;----------------------------------------------------------------------
        MOV       AL,*-SP[5]            ; [CPU_] |135| 
        B         $C$L3,EQ              ; [CPU_] |135| 
        ; branchcc occurs ; [] |135| 
	.dwpsn	file "../source/PressureSense.c",line 137,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 137 | for(i=0; i<DataBufSize; i++)                                           
;----------------------------------------------------------------------
        MOV       *-SP[6],#0            ; [CPU_] |137| 
	.dwpsn	file "../source/PressureSense.c",line 137,column 12,is_stmt,isa 0
        CMP       AL,*-SP[6]            ; [CPU_] |137| 
        B         $C$L2,LOS             ; [CPU_] |137| 
        ; branchcc occurs ; [] |137| 
$C$L1:    
	.dwpsn	file "../source/PressureSense.c",line 139,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 139 | AverageData = AverageData + Data[(i*7)+1];                             
;----------------------------------------------------------------------
        MOV       T,*-SP[6]             ; [CPU_] |139| 
        MPYB      ACC,T,#7              ; [CPU_] |139| 
        ADDB      AL,#1                 ; [CPU_] |139| 
        MOVZ      AR0,AL                ; [CPU_] |139| 
        MOVU      ACC,*+XAR4[AR0]       ; [CPU_] |139| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |139| 
        MOVL      *-SP[4],ACC           ; [CPU_] |139| 
	.dwpsn	file "../source/PressureSense.c",line 137,column 27,is_stmt,isa 0
        INC       *-SP[6]               ; [CPU_] |137| 
	.dwpsn	file "../source/PressureSense.c",line 137,column 12,is_stmt,isa 0
        MOV       AL,*-SP[5]            ; [CPU_] |137| 
        CMP       AL,*-SP[6]            ; [CPU_] |137| 
        B         $C$L1,HI              ; [CPU_] |137| 
        ; branchcc occurs ; [] |137| 
$C$L2:    
	.dwpsn	file "../source/PressureSense.c",line 142,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 142 | return (unsigned int)(AverageData/DataBufSize);                        
; 144 | else                                                                   
;----------------------------------------------------------------------
        MOVZ      AR6,*-SP[5]           ; [CPU_] |142| 
        MOVL      P,*-SP[4]             ; [CPU_] |142| 
        MOVB      ACC,#0                ; [CPU_] |142| 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |142| 
        MOV       AL,PL                 ; [CPU_] |142| 
        B         $C$L4,UNC             ; [CPU_] |142| 
        ; branch occurs ; [] |142| 
$C$L3:    
	.dwpsn	file "../source/PressureSense.c",line 146,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 146 | return 0;                                                              
;----------------------------------------------------------------------
        MOVB      AL,#0                 ; [CPU_] |146| 
$C$L4:    
	.dwpsn	file "../source/PressureSense.c",line 149,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$16	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$16, DW_AT_low_pc(0x00)
	.dwattr $C$DW$16, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$9, DW_AT_TI_end_file("../source/PressureSense.c")
	.dwattr $C$DW$9, DW_AT_TI_end_line(0x95)
	.dwattr $C$DW$9, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$9

	.sect	".text:_PSADCtoPressureMap"
	.clink
	.global	_PSADCtoPressureMap

$C$DW$17	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$17, DW_AT_name("PSADCtoPressureMap")
	.dwattr $C$DW$17, DW_AT_low_pc(_PSADCtoPressureMap)
	.dwattr $C$DW$17, DW_AT_high_pc(0x00)
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_PSADCtoPressureMap")
	.dwattr $C$DW$17, DW_AT_external
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$17, DW_AT_TI_begin_file("../source/PressureSense.c")
	.dwattr $C$DW$17, DW_AT_TI_begin_line(0xa4)
	.dwattr $C$DW$17, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$17, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/PressureSense.c",line 165,column 1,is_stmt,address _PSADCtoPressureMap,isa 0

	.dwfde $C$DW$CIE, _PSADCtoPressureMap
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_name("ADCRawData")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_ADCRawData")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 164 | float PSADCtoPressureMap(unsigned int ADCRawData)                      
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _PSADCtoPressureMap           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_PSADCtoPressureMap:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$19	.dwtag  DW_TAG_variable
	.dwattr $C$DW$19, DW_AT_name("PressureData")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_PressureData")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -4]

$C$DW$20	.dwtag  DW_TAG_variable
	.dwattr $C$DW$20, DW_AT_name("ADCRawData")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_ADCRawData")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -5]

        MOV       *-SP[5],AL            ; [CPU_] |165| 
	.dwpsn	file "../source/PressureSense.c",line 166,column 20,is_stmt,isa 0
;----------------------------------------------------------------------
; 166 | float PressureData=0.0;                                                
; 167 | //      unsigned int                                                   
; 168 | //      char DebugPrintBuf[50];                                        
;----------------------------------------------------------------------
        ZERO      R0H                   ; [CPU_] |166| 
        MOV32     *-SP[4],R0H           ; [CPU_] |166| 
	.dwpsn	file "../source/PressureSense.c",line 169,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 169 | if(ADCRawData >= PressureSense.ADCMin && ADCRawData <= \               
; 170 |         PressureSense.ADCMax && (PressureSense.ADCSpan > 0 ))          
;----------------------------------------------------------------------
        MOVW      DP,#_PressureSense+14 ; [CPU_U] 
        UI16TOF32 R1H,*-SP[5]           ; [CPU_] |169| 
        MOV32     R0H,@_PressureSense+14 ; [CPU_] |169| 
        CMPF32    R1H,R0H               ; [CPU_] |169| 
        MOVST0    ZF, NF                ; [CPU_] |169| 
        B         $C$L5,LT              ; [CPU_] |169| 
        ; branchcc occurs ; [] |169| 
        UI16TOF32 R1H,*-SP[5]           ; [CPU_] |169| 
        MOV32     R0H,@_PressureSense+16 ; [CPU_] |169| 
        CMPF32    R1H,R0H               ; [CPU_] |169| 
        MOVST0    ZF, NF                ; [CPU_] |169| 
        B         $C$L5,GT              ; [CPU_] |169| 
        ; branchcc occurs ; [] |169| 
        MOV32     R0H,@_PressureSense+4 ; [CPU_] |169| 
        CMPF32    R0H,#0                ; [CPU_] |169| 
        MOVST0    ZF, NF                ; [CPU_] |169| 
        B         $C$L5,LEQ             ; [CPU_] |169| 
        ; branchcc occurs ; [] |169| 
	.dwpsn	file "../source/PressureSense.c",line 172,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 172 | PressureData =(float)(((float)ADCRawData - PressureSense.ADCMin)  *(Pre
;     | ssureSense.PressureSpan)/PressureSense.ADCSpan);                       
;----------------------------------------------------------------------
        UI16TOF32 R1H,*-SP[5]           ; [CPU_] |172| 
        MOV32     R0H,@_PressureSense+14 ; [CPU_] |172| 
        SUBF32    R0H,R1H,R0H           ; [CPU_] |172| 
        MOV32     R3H,@_PressureSense+8 ; [CPU_] |172| 

        MPYF32    R0H,R3H,R0H           ; [CPU_] |172| 
||      MOV32     R1H,@_PressureSense+4 ; [CPU_] |172| 

$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$21, DW_AT_TI_call

        LCR       #FS$$DIV              ; [CPU_] |172| 
        ; call occurs [#FS$$DIV] ; [] |172| 
        MOV32     *-SP[4],R0H           ; [CPU_] |172| 
	.dwpsn	file "../source/PressureSense.c",line 173,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 173 | PressureData = PressureData + PressureSense.PressureValueMin;          
;----------------------------------------------------------------------
        MOVW      DP,#_PressureSense+10 ; [CPU_U] 
        MOV32     R1H,@_PressureSense+10 ; [CPU_] |173| 
        ADDF32    R0H,R0H,R1H           ; [CPU_] |173| 
        NOP       ; [CPU_] 
        MOV32     *-SP[4],R0H           ; [CPU_] |173| 
	.dwpsn	file "../source/PressureSense.c",line 174,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 175 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L7,UNC             ; [CPU_] |174| 
        ; branch occurs ; [] |174| 
$C$L5:    
	.dwpsn	file "../source/PressureSense.c",line 177,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 177 | if(ADCRawData < PressureSense.ADCMin)                                  
;----------------------------------------------------------------------
        UI16TOF32 R1H,*-SP[5]           ; [CPU_] |177| 
        MOV32     R0H,@_PressureSense+14 ; [CPU_] |177| 
        CMPF32    R1H,R0H               ; [CPU_] |177| 
        MOVST0    ZF, NF                ; [CPU_] |177| 
        B         $C$L6,GEQ             ; [CPU_] |177| 
        ; branchcc occurs ; [] |177| 
	.dwpsn	file "../source/PressureSense.c",line 179,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 179 | return PRESSURE_MIN;                                                   
; 181 | else                                                                   
;----------------------------------------------------------------------
        MOVIZ     R0H,#49648            ; [CPU_] |179| 
        B         $C$L8,UNC             ; [CPU_] |179| 
        ; branch occurs ; [] |179| 
$C$L6:    
	.dwpsn	file "../source/PressureSense.c",line 183,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 183 | return PRESSURE_MAX;                                                   
;----------------------------------------------------------------------
        MOVIZ     R0H,#16880            ; [CPU_] |183| 
        B         $C$L8,UNC             ; [CPU_] |183| 
        ; branch occurs ; [] |183| 
$C$L7:    
	.dwpsn	file "../source/PressureSense.c",line 187,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 187 | return PressureData;                                                   
;----------------------------------------------------------------------
$C$L8:    
	.dwpsn	file "../source/PressureSense.c",line 188,column 1,is_stmt,isa 0
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$17, DW_AT_TI_end_file("../source/PressureSense.c")
	.dwattr $C$DW$17, DW_AT_TI_end_line(0xbc)
	.dwattr $C$DW$17, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$17

	.sect	".text:_PSProcessPressureData"
	.clink
	.global	_PSProcessPressureData

$C$DW$23	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$23, DW_AT_name("PSProcessPressureData")
	.dwattr $C$DW$23, DW_AT_low_pc(_PSProcessPressureData)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_PSProcessPressureData")
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$23, DW_AT_TI_begin_file("../source/PressureSense.c")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0xc8)
	.dwattr $C$DW$23, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/PressureSense.c",line 201,column 1,is_stmt,address _PSProcessPressureData,isa 0

	.dwfde $C$DW$CIE, _PSProcessPressureData
;----------------------------------------------------------------------
; 200 | float PSProcessPressureData(void)                                      
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _PSProcessPressureData        FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_PSProcessPressureData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$24	.dwtag  DW_TAG_variable
	.dwattr $C$DW$24, DW_AT_name("PressureDataBuf")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_PressureDataBuf")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -2]

$C$DW$25	.dwtag  DW_TAG_variable
	.dwattr $C$DW$25, DW_AT_name("PressureRawData")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_PressureRawData")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -3]

	.dwpsn	file "../source/PressureSense.c",line 202,column 30,is_stmt,isa 0
;----------------------------------------------------------------------
; 202 | unsigned int PressureRawData=0;                                        
; 203 | #ifdef ADC_INPUT_SIMULATION_PRESSURE                                   
; 204 | static unsigned int Counter=0;                                         
; 205 | #endif                                                                 
; 206 | unsigned int *PressureDataBuf;                                         
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |202| 
	.dwpsn	file "../source/PressureSense.c",line 207,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 207 | PressureDataBuf = ADGetAdcBufAddr();                                   
;----------------------------------------------------------------------
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_ADGetAdcBufAddr")
	.dwattr $C$DW$26, DW_AT_TI_call

        LCR       #_ADGetAdcBufAddr     ; [CPU_] |207| 
        ; call occurs [#_ADGetAdcBufAddr] ; [] |207| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |207| 
	.dwpsn	file "../source/PressureSense.c",line 209,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 209 | if(PressureSense.AcquireStatus == ACQUIRE_COMPLETE)                    
;----------------------------------------------------------------------
        MOVW      DP,#_PressureSense+1  ; [CPU_U] 
        MOV       AL,@_PressureSense+1  ; [CPU_] |209| 
        CMPB      AL,#2                 ; [CPU_] |209| 
        B         $C$L9,NEQ             ; [CPU_] |209| 
        ; branchcc occurs ; [] |209| 
	.dwpsn	file "../source/PressureSense.c",line 211,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 211 | PressureRawData = PSGetPressureData(PressureDataBuf, PRESSURE_DATA_BUF_
;     | SIZE);                                                                 
; 212 | #ifdef ADC_INPUT_SIMULATION_PRESSURE                                   
; 213 | if(Counter < 4096)                                                     
; 215 |         Counter++;                                                     
; 217 | else                                                                   
; 219 |         Counter = 0;                                                   
; 221 | PressureRawData = Counter;                                             
; 222 | #endif                                                                 
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |211| 
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("_PSGetPressureData")
	.dwattr $C$DW$27, DW_AT_TI_call

        LCR       #_PSGetPressureData   ; [CPU_] |211| 
        ; call occurs [#_PSGetPressureData] ; [] |211| 
        MOV       *-SP[3],AL            ; [CPU_] |211| 
	.dwpsn	file "../source/PressureSense.c",line 223,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 223 | PressureSense.PressureData = PSADCtoPressureMap(PressureRawData);      
;----------------------------------------------------------------------
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_PSADCtoPressureMap")
	.dwattr $C$DW$28, DW_AT_TI_call

        LCR       #_PSADCtoPressureMap  ; [CPU_] |223| 
        ; call occurs [#_PSADCtoPressureMap] ; [] |223| 
        MOV32     @_PressureSense+6,R0H ; [CPU_] |223| 
	.dwpsn	file "../source/PressureSense.c",line 224,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 224 | ADAcquirePressureData(PRESSURE_DATA_BUF_SIZE);                         
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |224| 
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("_ADAcquirePressureData")
	.dwattr $C$DW$29, DW_AT_TI_call

        LCR       #_ADAcquirePressureData ; [CPU_] |224| 
        ; call occurs [#_ADAcquirePressureData] ; [] |224| 
$C$L9:    
	.dwpsn	file "../source/PressureSense.c",line 226,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 226 | return PressureSense.PressureData;                                     
;----------------------------------------------------------------------
        MOVW      DP,#_PressureSense+6  ; [CPU_U] 
        MOV32     R0H,@_PressureSense+6 ; [CPU_] |226| 
	.dwpsn	file "../source/PressureSense.c",line 227,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$23, DW_AT_TI_end_file("../source/PressureSense.c")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0xe3)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$23

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_ADAcquirePressureData
	.global	_ADGetAdcBufAddr
	.global	FS$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_name("ADC_PRAMETER_PRESSURE")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x14)
$C$DW$31	.dwtag  DW_TAG_member
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$31, DW_AT_name("AdcCounter")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_AdcCounter")
	.dwattr $C$DW$31, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$31, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$32	.dwtag  DW_TAG_member
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$32, DW_AT_name("AcquireStatus")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_AcquireStatus")
	.dwattr $C$DW$32, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$32, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$33	.dwtag  DW_TAG_member
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$33, DW_AT_name("DataSize")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_DataSize")
	.dwattr $C$DW$33, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$33, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$34	.dwtag  DW_TAG_member
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$34, DW_AT_name("ADCSpan")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ADCSpan")
	.dwattr $C$DW$34, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$34, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$35	.dwtag  DW_TAG_member
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$35, DW_AT_name("PressureData")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_PressureData")
	.dwattr $C$DW$35, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$35, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$36	.dwtag  DW_TAG_member
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$36, DW_AT_name("PressureSpan")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_PressureSpan")
	.dwattr $C$DW$36, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$36, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$37	.dwtag  DW_TAG_member
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$37, DW_AT_name("PressureValueMin")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_PressureValueMin")
	.dwattr $C$DW$37, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$37, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$38	.dwtag  DW_TAG_member
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$38, DW_AT_name("PressureValueMax")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_PressureValueMax")
	.dwattr $C$DW$38, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$38, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$39	.dwtag  DW_TAG_member
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$39, DW_AT_name("ADCMin")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_ADCMin")
	.dwattr $C$DW$39, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$39, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$40	.dwtag  DW_TAG_member
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$40, DW_AT_name("ADCMax")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_ADCMax")
	.dwattr $C$DW$40, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$40, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$41	.dwtag  DW_TAG_member
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$41, DW_AT_name("PRESSURE_Err")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_PRESSURE_Err")
	.dwattr $C$DW$41, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$41, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$20

$C$DW$T$23	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$23, DW_AT_name("PressureParameter")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)

$C$DW$T$24	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_address_class(0x20)


$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_name("PRESSURE_ERROR_BITS")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$42	.dwtag  DW_TAG_member
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$42, DW_AT_name("LOW_THRESHOLD")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_LOW_THRESHOLD")
	.dwattr $C$DW$42, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$42, DW_AT_bit_size(0x01)
	.dwattr $C$DW$42, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$42, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$43	.dwtag  DW_TAG_member
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$43, DW_AT_name("HIGH_THRESHOLD")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_HIGH_THRESHOLD")
	.dwattr $C$DW$43, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$43, DW_AT_bit_size(0x01)
	.dwattr $C$DW$43, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$44	.dwtag  DW_TAG_member
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$44, DW_AT_name("FAULT_STATE")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_FAULT_STATE")
	.dwattr $C$DW$44, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$44, DW_AT_bit_size(0x01)
	.dwattr $C$DW$44, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$44, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$45	.dwtag  DW_TAG_member
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$45, DW_AT_name("AC_STATUS")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_AC_STATUS")
	.dwattr $C$DW$45, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$45, DW_AT_bit_size(0x02)
	.dwattr $C$DW$45, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$45, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$46	.dwtag  DW_TAG_member
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$46, DW_AT_name("VALUE")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_VALUE")
	.dwattr $C$DW$46, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$46, DW_AT_bit_size(0x0b)
	.dwattr $C$DW$46, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$46, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$21


$C$DW$T$22	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$22, DW_AT_name("PRESSURE_ERROR_STATUS")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x01)
$C$DW$47	.dwtag  DW_TAG_member
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$47, DW_AT_name("PressureErrStatus")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_PressureErrStatus")
	.dwattr $C$DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$47, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$48	.dwtag  DW_TAG_member
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$48, DW_AT_name("bit")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$48, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$22

$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("Uint16")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)

$C$DW$T$30	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$30, DW_AT_address_class(0x20)

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$49	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$16)

$C$DW$T$19	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$49)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$50	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$50, DW_AT_name("AL")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg0]

$C$DW$51	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$51, DW_AT_name("AH")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg1]

$C$DW$52	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$52, DW_AT_name("PL")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg2]

$C$DW$53	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$53, DW_AT_name("PH")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg3]

$C$DW$54	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$54, DW_AT_name("SP")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg20]

$C$DW$55	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$55, DW_AT_name("XT")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg21]

$C$DW$56	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$56, DW_AT_name("T")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg22]

$C$DW$57	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$57, DW_AT_name("ST0")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg23]

$C$DW$58	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$58, DW_AT_name("ST1")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg24]

$C$DW$59	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$59, DW_AT_name("PC")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg25]

$C$DW$60	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$60, DW_AT_name("RPC")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg26]

$C$DW$61	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$61, DW_AT_name("FP")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg28]

$C$DW$62	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$62, DW_AT_name("DP")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg29]

$C$DW$63	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$63, DW_AT_name("SXM")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg30]

$C$DW$64	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$64, DW_AT_name("PM")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg31]

$C$DW$65	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$65, DW_AT_name("OVM")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x20]

$C$DW$66	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$66, DW_AT_name("PAGE0")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x21]

$C$DW$67	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$67, DW_AT_name("AMODE")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x22]

$C$DW$68	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$68, DW_AT_name("INTM")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x23]

$C$DW$69	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$69, DW_AT_name("IFR")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x24]

$C$DW$70	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$70, DW_AT_name("IER")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x25]

$C$DW$71	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$71, DW_AT_name("V")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x26]

$C$DW$72	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$72, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$73	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$73, DW_AT_name("VOL")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$74	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$74, DW_AT_name("AR0")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg4]

$C$DW$75	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$75, DW_AT_name("XAR0")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg5]

$C$DW$76	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$76, DW_AT_name("AR1")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg6]

$C$DW$77	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$77, DW_AT_name("XAR1")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg7]

$C$DW$78	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$78, DW_AT_name("AR2")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg8]

$C$DW$79	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$79, DW_AT_name("XAR2")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg9]

$C$DW$80	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$80, DW_AT_name("AR3")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg10]

$C$DW$81	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$81, DW_AT_name("XAR3")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg11]

$C$DW$82	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$82, DW_AT_name("AR4")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg12]

$C$DW$83	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$83, DW_AT_name("XAR4")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg13]

$C$DW$84	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$84, DW_AT_name("AR5")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg14]

$C$DW$85	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$85, DW_AT_name("XAR5")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg15]

$C$DW$86	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$86, DW_AT_name("AR6")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg16]

$C$DW$87	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$87, DW_AT_name("XAR6")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg17]

$C$DW$88	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$88, DW_AT_name("AR7")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg18]

$C$DW$89	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$89, DW_AT_name("XAR7")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg19]

$C$DW$90	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$90, DW_AT_name("R0HL")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x29]

$C$DW$91	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$91, DW_AT_name("R0H")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$92	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$92, DW_AT_name("R1HL")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$93	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$93, DW_AT_name("R1H")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$94	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$94, DW_AT_name("R2HL")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$95	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$95, DW_AT_name("R2H")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$96	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$96, DW_AT_name("R3HL")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$97	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$97, DW_AT_name("R3H")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x30]

$C$DW$98	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$98, DW_AT_name("R4HL")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x31]

$C$DW$99	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$99, DW_AT_name("R4H")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x32]

$C$DW$100	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$100, DW_AT_name("R5HL")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x33]

$C$DW$101	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$101, DW_AT_name("R5H")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x34]

$C$DW$102	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$102, DW_AT_name("R6HL")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x35]

$C$DW$103	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$103, DW_AT_name("R6H")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x36]

$C$DW$104	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$104, DW_AT_name("R7HL")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x37]

$C$DW$105	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$105, DW_AT_name("R7H")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x38]

$C$DW$106	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$106, DW_AT_name("RBL")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x49]

$C$DW$107	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$107, DW_AT_name("RB")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$108	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$108, DW_AT_name("STFL")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x27]

$C$DW$109	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$109, DW_AT_name("STF")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x28]

$C$DW$110	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$110, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Fri Jan 29 15:14:32 2021                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../source/StepperMotorDriver.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cell Counter\V1.3.1_And_V1.3.0\alphaCellCounterCPU1\CPU1_FLASH")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_bLowVolumeDisp+0,32
	.bits	0,16			; _g_bLowVolumeDisp @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("IGGpioConfig_MotorReset")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_IGGpioConfig_MotorReset")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
	.dwendtag $C$DW$1


$C$DW$2	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$2, DW_AT_name("IGGpioConfig_Motor5")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_IGGpioConfig_Motor5")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
	.dwendtag $C$DW$2


$C$DW$3	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$3, DW_AT_name("UartDebugPrint")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_UartDebugPrint")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$35)

$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$10)

	.dwendtag $C$DW$3


$C$DW$6	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$6, DW_AT_name("PWMConfigMotor4Param")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_PWMConfigMotor4Param")
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$16)

$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$30)

$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$6


$C$DW$10	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$10, DW_AT_name("PWMConfigMotor5Param")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_PWMConfigMotor5Param")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$16)

$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$30)

$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$10


$C$DW$14	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$14, DW_AT_name("PWMConfigMotor3Param")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_PWMConfigMotor3Param")
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$16)

$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$30)

$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$14


$C$DW$18	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$18, DW_AT_name("PWMConfigMotor1Param")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_PWMConfigMotor1Param")
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$16)

$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$30)

$C$DW$21	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$18


$C$DW$22	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$22, DW_AT_name("PWMConfigMotor2Param")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_PWMConfigMotor2Param")
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$16)

$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$30)

$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$22


$C$DW$26	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$26, DW_AT_name("F28x_usDelay")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_F28x_usDelay")
	.dwattr $C$DW$26, DW_AT_declaration
	.dwattr $C$DW$26, DW_AT_external
$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$12)

	.dwendtag $C$DW$26


$C$DW$28	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$28, DW_AT_name("IGGpioConfig_Motor2")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_IGGpioConfig_Motor2")
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
	.dwendtag $C$DW$28


$C$DW$29	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$29, DW_AT_name("IGGpioConfig_Motor1")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_IGGpioConfig_Motor1")
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external
	.dwendtag $C$DW$29


$C$DW$30	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$30, DW_AT_name("IGGpioConfig_Motor4")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_IGGpioConfig_Motor4")
	.dwattr $C$DW$30, DW_AT_declaration
	.dwattr $C$DW$30, DW_AT_external
	.dwendtag $C$DW$30


$C$DW$31	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$31, DW_AT_name("IGGpioConfig_Motor3")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_IGGpioConfig_Motor3")
	.dwattr $C$DW$31, DW_AT_declaration
	.dwattr $C$DW$31, DW_AT_external
	.dwendtag $C$DW$31

$C$DW$32	.dwtag  DW_TAG_variable
	.dwattr $C$DW$32, DW_AT_name("m_bXAxisMonEnable")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_m_bXAxisMonEnable")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$32, DW_AT_declaration
	.dwattr $C$DW$32, DW_AT_external

$C$DW$33	.dwtag  DW_TAG_variable
	.dwattr $C$DW$33, DW_AT_name("m_bDiluentSyringeMonEnable")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_m_bDiluentSyringeMonEnable")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external

	.global	_g_bLowVolumeDisp
_g_bLowVolumeDisp:	.usect	".ebss",1,1,0
$C$DW$34	.dwtag  DW_TAG_variable
	.dwattr $C$DW$34, DW_AT_name("g_bLowVolumeDisp")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_g_bLowVolumeDisp")
	.dwattr $C$DW$34, DW_AT_location[DW_OP_addr _g_bLowVolumeDisp]
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$34, DW_AT_external

$C$DW$35	.dwtag  DW_TAG_variable
	.dwattr $C$DW$35, DW_AT_name("g_usnCurrentMode")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_g_usnCurrentMode")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external

$C$DW$36	.dwtag  DW_TAG_variable
	.dwattr $C$DW$36, DW_AT_name("m_bYAxisMonEnable")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_m_bYAxisMonEnable")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external

$C$DW$37	.dwtag  DW_TAG_variable
	.dwattr $C$DW$37, DW_AT_name("m_bBubblingMovEnFlag")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_m_bBubblingMovEnFlag")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$37, DW_AT_declaration
	.dwattr $C$DW$37, DW_AT_external

$C$DW$38	.dwtag  DW_TAG_variable
	.dwattr $C$DW$38, DW_AT_name("m_bSampleSyringeMonEnable")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_m_bSampleSyringeMonEnable")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external

$C$DW$39	.dwtag  DW_TAG_variable
	.dwattr $C$DW$39, DW_AT_name("m_bWasteSyringeMonEnable")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_m_bWasteSyringeMonEnable")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$39, DW_AT_declaration
	.dwattr $C$DW$39, DW_AT_external


$C$DW$40	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$40, DW_AT_name("PWMGetMotor3Status")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_PWMGetMotor3Status")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external
	.dwendtag $C$DW$40


$C$DW$41	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$41, DW_AT_name("IGReadMotorGpio")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_IGReadMotorGpio")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
$C$DW$42	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$11)

	.dwendtag $C$DW$41


$C$DW$43	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$43, DW_AT_name("PWMGetMotor2Status")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_PWMGetMotor2Status")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
	.dwendtag $C$DW$43


$C$DW$44	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$44, DW_AT_name("PWMGetMotor5Status")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_PWMGetMotor5Status")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$44, DW_AT_declaration
	.dwattr $C$DW$44, DW_AT_external
	.dwendtag $C$DW$44


$C$DW$45	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$45, DW_AT_name("sprintf")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_sprintf")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$45)

$C$DW$47	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$47)

$C$DW$48	.dwtag  DW_TAG_unspecified_parameters

	.dwendtag $C$DW$45


$C$DW$49	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$49, DW_AT_name("IGWriteMotorGpio")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_IGWriteMotorGpio")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external
$C$DW$50	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$11)

$C$DW$51	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$11)

	.dwendtag $C$DW$49


$C$DW$52	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$52, DW_AT_name("PWMGetMotor4Status")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_PWMGetMotor4Status")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external
	.dwendtag $C$DW$52


$C$DW$53	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$53, DW_AT_name("PWMGetMotor1Status")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_PWMGetMotor1Status")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
	.dwendtag $C$DW$53

$C$DW$54	.dwtag  DW_TAG_variable
	.dwattr $C$DW$54, DW_AT_name("BloodCellCounter_U")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_BloodCellCounter_U")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$54, DW_AT_declaration
	.dwattr $C$DW$54, DW_AT_external

$C$DW$55	.dwtag  DW_TAG_variable
	.dwattr $C$DW$55, DW_AT_name("DebugPrintBuf")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_DebugPrintBuf")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external

;	D:\Softwares\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\DITTOM~1.DEV\\AppData\\Local\\Temp\\1159612 
	.sect	".text:_SMDInitialize_MotorGpio"
	.clink
	.global	_SMDInitialize_MotorGpio

$C$DW$56	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$56, DW_AT_name("SMDInitialize_MotorGpio")
	.dwattr $C$DW$56, DW_AT_low_pc(_SMDInitialize_MotorGpio)
	.dwattr $C$DW$56, DW_AT_high_pc(0x00)
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_SMDInitialize_MotorGpio")
	.dwattr $C$DW$56, DW_AT_external
	.dwattr $C$DW$56, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$56, DW_AT_TI_begin_line(0x40)
	.dwattr $C$DW$56, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$56, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/StepperMotorDriver.c",line 65,column 1,is_stmt,address _SMDInitialize_MotorGpio,isa 0

	.dwfde $C$DW$CIE, _SMDInitialize_MotorGpio
;----------------------------------------------------------------------
;  64 | void SMDInitialize_MotorGpio(void)                                     
;  66 | //!Configure the motor reset GPIO                                      
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDInitialize_MotorGpio      FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SMDInitialize_MotorGpio:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/StepperMotorDriver.c",line 67,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  67 | IGGpioConfig_MotorReset();                                             
;  69 | //!Configure the motor1 control GPIOs                                  
;----------------------------------------------------------------------
$C$DW$57	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$57, DW_AT_low_pc(0x00)
	.dwattr $C$DW$57, DW_AT_name("_IGGpioConfig_MotorReset")
	.dwattr $C$DW$57, DW_AT_TI_call

        LCR       #_IGGpioConfig_MotorReset ; [CPU_] |67| 
        ; call occurs [#_IGGpioConfig_MotorReset] ; [] |67| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 70,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  70 | IGGpioConfig_Motor1();                                                 
;  72 | //!Configure the motor2 control GPIOs                                  
;----------------------------------------------------------------------
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_name("_IGGpioConfig_Motor1")
	.dwattr $C$DW$58, DW_AT_TI_call

        LCR       #_IGGpioConfig_Motor1 ; [CPU_] |70| 
        ; call occurs [#_IGGpioConfig_Motor1] ; [] |70| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 73,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  73 | IGGpioConfig_Motor2();                                                 
;  75 | //!Configure the motor3 control GPIOs                                  
;----------------------------------------------------------------------
$C$DW$59	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$59, DW_AT_low_pc(0x00)
	.dwattr $C$DW$59, DW_AT_name("_IGGpioConfig_Motor2")
	.dwattr $C$DW$59, DW_AT_TI_call

        LCR       #_IGGpioConfig_Motor2 ; [CPU_] |73| 
        ; call occurs [#_IGGpioConfig_Motor2] ; [] |73| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 76,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  76 | IGGpioConfig_Motor3();                                                 
;  78 | //!Configure the motor4 control GPIOs                                  
;----------------------------------------------------------------------
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_name("_IGGpioConfig_Motor3")
	.dwattr $C$DW$60, DW_AT_TI_call

        LCR       #_IGGpioConfig_Motor3 ; [CPU_] |76| 
        ; call occurs [#_IGGpioConfig_Motor3] ; [] |76| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 79,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  79 | IGGpioConfig_Motor4();                                                 
;  81 | //!Configure the motor5 control GPIOs                                  
;----------------------------------------------------------------------
$C$DW$61	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$61, DW_AT_low_pc(0x00)
	.dwattr $C$DW$61, DW_AT_name("_IGGpioConfig_Motor4")
	.dwattr $C$DW$61, DW_AT_TI_call

        LCR       #_IGGpioConfig_Motor4 ; [CPU_] |79| 
        ; call occurs [#_IGGpioConfig_Motor4] ; [] |79| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 82,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  82 | IGGpioConfig_Motor5();                                                 
;  84 | //!Configure the default state of the motor control GPIOs              
;----------------------------------------------------------------------
$C$DW$62	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$62, DW_AT_low_pc(0x00)
	.dwattr $C$DW$62, DW_AT_name("_IGGpioConfig_Motor5")
	.dwattr $C$DW$62, DW_AT_TI_call

        LCR       #_IGGpioConfig_Motor5 ; [CPU_] |82| 
        ; call occurs [#_IGGpioConfig_Motor5] ; [] |82| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 85,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  85 | SMDGpioConfig_MotorDefaultState();                                     
;----------------------------------------------------------------------
$C$DW$63	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$63, DW_AT_low_pc(0x00)
	.dwattr $C$DW$63, DW_AT_name("_SMDGpioConfig_MotorDefaultState")
	.dwattr $C$DW$63, DW_AT_TI_call

        LCR       #_SMDGpioConfig_MotorDefaultState ; [CPU_] |85| 
        ; call occurs [#_SMDGpioConfig_MotorDefaultState] ; [] |85| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 86,column 1,is_stmt,isa 0
$C$DW$64	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$64, DW_AT_low_pc(0x00)
	.dwattr $C$DW$64, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$56, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$56, DW_AT_TI_end_line(0x56)
	.dwattr $C$DW$56, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$56

	.sect	".text:_SMDGpioConfig_MotorDefaultState"
	.clink
	.global	_SMDGpioConfig_MotorDefaultState

$C$DW$65	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$65, DW_AT_name("SMDGpioConfig_MotorDefaultState")
	.dwattr $C$DW$65, DW_AT_low_pc(_SMDGpioConfig_MotorDefaultState)
	.dwattr $C$DW$65, DW_AT_high_pc(0x00)
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_SMDGpioConfig_MotorDefaultState")
	.dwattr $C$DW$65, DW_AT_external
	.dwattr $C$DW$65, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$65, DW_AT_TI_begin_line(0x63)
	.dwattr $C$DW$65, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$65, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/StepperMotorDriver.c",line 100,column 1,is_stmt,address _SMDGpioConfig_MotorDefaultState,isa 0

	.dwfde $C$DW$CIE, _SMDGpioConfig_MotorDefaultState
;----------------------------------------------------------------------
;  99 | void SMDGpioConfig_MotorDefaultState(void)                             
; 101 | //!Reset the Motor 1                                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDGpioConfig_MotorDefaultState FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SMDGpioConfig_MotorDefaultState:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/StepperMotorDriver.c",line 102,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 102 | SMDMotorReset();                                                       
; 103 | //!Enable Motor 1                                                      
;----------------------------------------------------------------------
$C$DW$66	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$66, DW_AT_low_pc(0x00)
	.dwattr $C$DW$66, DW_AT_name("_SMDMotorReset")
	.dwattr $C$DW$66, DW_AT_TI_call

        LCR       #_SMDMotorReset       ; [CPU_] |102| 
        ; call occurs [#_SMDMotorReset] ; [] |102| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 104,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 104 | SMDSetMotorEnable(MOTOR1_SELECT);                                      
; 105 | //!Set the default step size of Motor 1                                
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |104| 
$C$DW$67	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$67, DW_AT_low_pc(0x00)
	.dwattr $C$DW$67, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$67, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |104| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |104| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 106,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 106 | SMDSetMotorStepSize(MOTOR1_SELECT, FULL_STEP);                         
; 107 | //!Set the motor 1 default direction                                   
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |106| 
        MOVB      AH,#1                 ; [CPU_] |106| 
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$68, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |106| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |106| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 108,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 108 | SMDSetMotorDirection(MOTOR1_SELECT, M1_DIR_DEFAULT);                   
; 109 | //!Set motor 1 decay                                                   
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |108| 
        MOVB      AH,#1                 ; [CPU_] |108| 
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$69, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |108| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |108| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 110,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 110 | SMDSetMotorDecay(MOTOR1_SELECT, M1_DECAY_DEFAULT);                     
; 111 | //!Disable the motor 1 by default                                      
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |110| 
        MOVB      AH,#1                 ; [CPU_] |110| 
$C$DW$70	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$70, DW_AT_low_pc(0x00)
	.dwattr $C$DW$70, DW_AT_name("_SMDSetMotorDecay")
	.dwattr $C$DW$70, DW_AT_TI_call

        LCR       #_SMDSetMotorDecay    ; [CPU_] |110| 
        ; call occurs [#_SMDSetMotorDecay] ; [] |110| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 112,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 112 | SMDSetMotorDisable(MOTOR1_SELECT);                                     
; 113 | //!Configure the PWM of the motor 1                                    
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |112| 
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_name("_SMDSetMotorDisable")
	.dwattr $C$DW$71, DW_AT_TI_call

        LCR       #_SMDSetMotorDisable  ; [CPU_] |112| 
        ; call occurs [#_SMDSetMotorDisable] ; [] |112| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 114,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 114 | PWMConfigMotor1Param(1, M1_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);          
; 116 | //!Reset the Motor                                                     
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |114| 
        MOV       AH,#1000              ; [CPU_] |114| 
        MOVIZ     R0H,#16256            ; [CPU_] |114| 
$C$DW$72	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$72, DW_AT_low_pc(0x00)
	.dwattr $C$DW$72, DW_AT_name("_PWMConfigMotor1Param")
	.dwattr $C$DW$72, DW_AT_TI_call

        LCR       #_PWMConfigMotor1Param ; [CPU_] |114| 
        ; call occurs [#_PWMConfigMotor1Param] ; [] |114| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 117,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 117 | SMDMotorReset();                                                       
; 118 | //!Enable Motor 2                                                      
;----------------------------------------------------------------------
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_name("_SMDMotorReset")
	.dwattr $C$DW$73, DW_AT_TI_call

        LCR       #_SMDMotorReset       ; [CPU_] |117| 
        ; call occurs [#_SMDMotorReset] ; [] |117| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 119,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 119 | SMDSetMotorEnable(MOTOR2_SELECT);                                      
; 120 | //!Set the default step size of Motor 2                                
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |119| 
$C$DW$74	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$74, DW_AT_low_pc(0x00)
	.dwattr $C$DW$74, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$74, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |119| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |119| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 121,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 121 | SMDSetMotorStepSize(MOTOR2_SELECT, FULL_STEP);                         
; 122 | //!Set the motor 2 default direction                                   
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |121| 
        MOVB      AH,#1                 ; [CPU_] |121| 
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$75, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |121| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |121| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 123,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 123 | SMDSetMotorDirection(MOTOR2_SELECT, M2_DIR_DEFAULT);                   
; 124 | //!Set motor 2 decay                                                   
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |123| 
        MOVB      AH,#1                 ; [CPU_] |123| 
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$76, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |123| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |123| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 125,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 125 | SMDSetMotorDecay(MOTOR2_SELECT, M2_DECAY_DEFAULT);                     
; 126 | //!Disable the motor 2 by default                                      
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |125| 
        MOVB      AH,#1                 ; [CPU_] |125| 
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("_SMDSetMotorDecay")
	.dwattr $C$DW$77, DW_AT_TI_call

        LCR       #_SMDSetMotorDecay    ; [CPU_] |125| 
        ; call occurs [#_SMDSetMotorDecay] ; [] |125| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 127,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 127 | SMDSetMotorDisable(MOTOR2_SELECT);                                     
; 128 | //!Configure the PWM of the motor 2                                    
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |127| 
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_name("_SMDSetMotorDisable")
	.dwattr $C$DW$78, DW_AT_TI_call

        LCR       #_SMDSetMotorDisable  ; [CPU_] |127| 
        ; call occurs [#_SMDSetMotorDisable] ; [] |127| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 129,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 129 | PWMConfigMotor2Param(1, M2_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);          
; 131 | //!Reset the Motor                                                     
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |129| 
        MOV       AH,#1000              ; [CPU_] |129| 
        MOVIZ     R0H,#16256            ; [CPU_] |129| 
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_name("_PWMConfigMotor2Param")
	.dwattr $C$DW$79, DW_AT_TI_call

        LCR       #_PWMConfigMotor2Param ; [CPU_] |129| 
        ; call occurs [#_PWMConfigMotor2Param] ; [] |129| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 132,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 132 | SMDMotorReset();                                                       
; 133 | //!Enable Motor 3                                                      
;----------------------------------------------------------------------
$C$DW$80	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$80, DW_AT_low_pc(0x00)
	.dwattr $C$DW$80, DW_AT_name("_SMDMotorReset")
	.dwattr $C$DW$80, DW_AT_TI_call

        LCR       #_SMDMotorReset       ; [CPU_] |132| 
        ; call occurs [#_SMDMotorReset] ; [] |132| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 134,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 134 | SMDSetMotorEnable(MOTOR3_SELECT);                                      
; 135 | //!Set the default step size of Motor 3                                
;----------------------------------------------------------------------
        MOVB      AL,#3                 ; [CPU_] |134| 
$C$DW$81	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$81, DW_AT_low_pc(0x00)
	.dwattr $C$DW$81, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$81, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |134| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |134| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 136,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 136 | SMDSetMotorStepSize(MOTOR3_SELECT, FULL_STEP);                         
; 137 | //!Set the motor 3 default direction                                   
;----------------------------------------------------------------------
        MOVB      AL,#3                 ; [CPU_] |136| 
        MOVB      AH,#1                 ; [CPU_] |136| 
$C$DW$82	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$82, DW_AT_low_pc(0x00)
	.dwattr $C$DW$82, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$82, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |136| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |136| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 138,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 138 | SMDSetMotorDirection(MOTOR3_SELECT, M3_DIR_DEFAULT);                   
; 139 | //!Set motor 3 decay                                                   
;----------------------------------------------------------------------
        MOVB      AL,#3                 ; [CPU_] |138| 
        MOVB      AH,#1                 ; [CPU_] |138| 
$C$DW$83	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$83, DW_AT_low_pc(0x00)
	.dwattr $C$DW$83, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$83, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |138| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |138| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 140,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 140 | SMDSetMotorDecay(MOTOR3_SELECT, M3_DECAY_DEFAULT);                     
; 141 | //!Disable the motor 3 by default                                      
;----------------------------------------------------------------------
        MOVB      AL,#3                 ; [CPU_] |140| 
        MOVB      AH,#1                 ; [CPU_] |140| 
$C$DW$84	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$84, DW_AT_low_pc(0x00)
	.dwattr $C$DW$84, DW_AT_name("_SMDSetMotorDecay")
	.dwattr $C$DW$84, DW_AT_TI_call

        LCR       #_SMDSetMotorDecay    ; [CPU_] |140| 
        ; call occurs [#_SMDSetMotorDecay] ; [] |140| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 142,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 142 | SMDSetMotorDisable(MOTOR3_SELECT);                                     
; 143 | //!Configure the PWM of the motor 3                                    
;----------------------------------------------------------------------
        MOVB      AL,#3                 ; [CPU_] |142| 
$C$DW$85	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$85, DW_AT_low_pc(0x00)
	.dwattr $C$DW$85, DW_AT_name("_SMDSetMotorDisable")
	.dwattr $C$DW$85, DW_AT_TI_call

        LCR       #_SMDSetMotorDisable  ; [CPU_] |142| 
        ; call occurs [#_SMDSetMotorDisable] ; [] |142| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 144,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 144 | PWMConfigMotor3Param(1, M3_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);          
; 146 | //!Reset the Motor                                                     
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |144| 
        MOV       AH,#1000              ; [CPU_] |144| 
        MOVIZ     R0H,#16256            ; [CPU_] |144| 
$C$DW$86	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$86, DW_AT_low_pc(0x00)
	.dwattr $C$DW$86, DW_AT_name("_PWMConfigMotor3Param")
	.dwattr $C$DW$86, DW_AT_TI_call

        LCR       #_PWMConfigMotor3Param ; [CPU_] |144| 
        ; call occurs [#_PWMConfigMotor3Param] ; [] |144| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 147,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 147 | SMDMotorReset();                                                       
; 148 | //!Enable Motor 4                                                      
;----------------------------------------------------------------------
$C$DW$87	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$87, DW_AT_low_pc(0x00)
	.dwattr $C$DW$87, DW_AT_name("_SMDMotorReset")
	.dwattr $C$DW$87, DW_AT_TI_call

        LCR       #_SMDMotorReset       ; [CPU_] |147| 
        ; call occurs [#_SMDMotorReset] ; [] |147| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 149,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 149 | SMDSetMotorEnable(MOTOR4_SELECT);                                      
; 150 | //!Set the default step size of Motor 4                                
;----------------------------------------------------------------------
        MOVB      AL,#4                 ; [CPU_] |149| 
$C$DW$88	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$88, DW_AT_low_pc(0x00)
	.dwattr $C$DW$88, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$88, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |149| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |149| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 151,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 151 | SMDSetMotorStepSize(MOTOR4_SELECT, FULL_STEP);                         
; 152 | //!Set the motor 4 default direction                                   
;----------------------------------------------------------------------
        MOVB      AL,#4                 ; [CPU_] |151| 
        MOVB      AH,#1                 ; [CPU_] |151| 
$C$DW$89	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$89, DW_AT_low_pc(0x00)
	.dwattr $C$DW$89, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$89, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |151| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |151| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 153,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 153 | SMDSetMotorDirection(MOTOR4_SELECT, M4_DIR_DEFAULT);                   
; 154 | //!Set motor 4 decay                                                   
;----------------------------------------------------------------------
        MOVB      AL,#4                 ; [CPU_] |153| 
        MOVB      AH,#1                 ; [CPU_] |153| 
$C$DW$90	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$90, DW_AT_low_pc(0x00)
	.dwattr $C$DW$90, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$90, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |153| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |153| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 155,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 155 | SMDSetMotorDecay(MOTOR4_SELECT, M4_DECAY_DEFAULT);                     
; 156 | //!Disable the motor 4 by default                                      
;----------------------------------------------------------------------
        MOVB      AL,#4                 ; [CPU_] |155| 
        MOVB      AH,#1                 ; [CPU_] |155| 
$C$DW$91	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$91, DW_AT_low_pc(0x00)
	.dwattr $C$DW$91, DW_AT_name("_SMDSetMotorDecay")
	.dwattr $C$DW$91, DW_AT_TI_call

        LCR       #_SMDSetMotorDecay    ; [CPU_] |155| 
        ; call occurs [#_SMDSetMotorDecay] ; [] |155| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 157,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 157 | SMDSetMotorDisable(MOTOR4_SELECT);                                     
; 158 | //!Configure the PWM of the motor 4                                    
;----------------------------------------------------------------------
        MOVB      AL,#4                 ; [CPU_] |157| 
$C$DW$92	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$92, DW_AT_low_pc(0x00)
	.dwattr $C$DW$92, DW_AT_name("_SMDSetMotorDisable")
	.dwattr $C$DW$92, DW_AT_TI_call

        LCR       #_SMDSetMotorDisable  ; [CPU_] |157| 
        ; call occurs [#_SMDSetMotorDisable] ; [] |157| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 159,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 159 | PWMConfigMotor4Param(1, M4_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);          
; 161 | //!Reset the Motor                                                     
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |159| 
        MOV       AH,#1000              ; [CPU_] |159| 
        MOVIZ     R0H,#16256            ; [CPU_] |159| 
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("_PWMConfigMotor4Param")
	.dwattr $C$DW$93, DW_AT_TI_call

        LCR       #_PWMConfigMotor4Param ; [CPU_] |159| 
        ; call occurs [#_PWMConfigMotor4Param] ; [] |159| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 162,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 162 | SMDMotorReset();                                                       
; 163 | //!Enable Motor 5                                                      
;----------------------------------------------------------------------
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("_SMDMotorReset")
	.dwattr $C$DW$94, DW_AT_TI_call

        LCR       #_SMDMotorReset       ; [CPU_] |162| 
        ; call occurs [#_SMDMotorReset] ; [] |162| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 164,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 164 | SMDSetMotorEnable(MOTOR5_SELECT);                                      
; 165 | //!Set the default step size of Motor 5                                
;----------------------------------------------------------------------
        MOVB      AL,#5                 ; [CPU_] |164| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$95, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |164| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |164| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 166,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 166 | SMDSetMotorStepSize(MOTOR5_SELECT, FULL_STEP);                         
; 167 | //!Set the motor 5 default direction                                   
;----------------------------------------------------------------------
        MOVB      AL,#5                 ; [CPU_] |166| 
        MOVB      AH,#1                 ; [CPU_] |166| 
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$96, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |166| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |166| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 168,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 168 | SMDSetMotorDirection(MOTOR5_SELECT, M5_DIR_DEFAULT);                   
; 169 | //!Set motor 5 decay                                                   
;----------------------------------------------------------------------
        MOVB      AL,#5                 ; [CPU_] |168| 
        MOVB      AH,#1                 ; [CPU_] |168| 
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$97, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |168| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |168| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 170,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 170 | SMDSetMotorDecay(MOTOR5_SELECT, M5_DECAY_DEFAULT);                     
; 171 | //!Disable the motor 5 by default                                      
;----------------------------------------------------------------------
        MOVB      AL,#5                 ; [CPU_] |170| 
        MOVB      AH,#1                 ; [CPU_] |170| 
$C$DW$98	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$98, DW_AT_low_pc(0x00)
	.dwattr $C$DW$98, DW_AT_name("_SMDSetMotorDecay")
	.dwattr $C$DW$98, DW_AT_TI_call

        LCR       #_SMDSetMotorDecay    ; [CPU_] |170| 
        ; call occurs [#_SMDSetMotorDecay] ; [] |170| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 172,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 172 | SMDSetMotorDisable(MOTOR5_SELECT);                                     
; 173 | //!Configure the PWM of the motor 5                                    
;----------------------------------------------------------------------
        MOVB      AL,#5                 ; [CPU_] |172| 
$C$DW$99	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$99, DW_AT_low_pc(0x00)
	.dwattr $C$DW$99, DW_AT_name("_SMDSetMotorDisable")
	.dwattr $C$DW$99, DW_AT_TI_call

        LCR       #_SMDSetMotorDisable  ; [CPU_] |172| 
        ; call occurs [#_SMDSetMotorDisable] ; [] |172| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 174,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 174 | PWMConfigMotor5Param(1, M5_DIR_DEFAULT, MOTOR_PWM_FREQUENCY);          
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |174| 
        MOV       AH,#1000              ; [CPU_] |174| 
        MOVIZ     R0H,#16256            ; [CPU_] |174| 
$C$DW$100	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$100, DW_AT_low_pc(0x00)
	.dwattr $C$DW$100, DW_AT_name("_PWMConfigMotor5Param")
	.dwattr $C$DW$100, DW_AT_TI_call

        LCR       #_PWMConfigMotor5Param ; [CPU_] |174| 
        ; call occurs [#_PWMConfigMotor5Param] ; [] |174| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 176,column 1,is_stmt,isa 0
$C$DW$101	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$101, DW_AT_low_pc(0x00)
	.dwattr $C$DW$101, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$65, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$65, DW_AT_TI_end_line(0xb0)
	.dwattr $C$DW$65, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$65

	.sect	".text:_SMDSetMotorStepSize"
	.clink
	.global	_SMDSetMotorStepSize

$C$DW$102	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$102, DW_AT_name("SMDSetMotorStepSize")
	.dwattr $C$DW$102, DW_AT_low_pc(_SMDSetMotorStepSize)
	.dwattr $C$DW$102, DW_AT_high_pc(0x00)
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$102, DW_AT_external
	.dwattr $C$DW$102, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$102, DW_AT_TI_begin_line(0xbd)
	.dwattr $C$DW$102, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$102, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/StepperMotorDriver.c",line 190,column 1,is_stmt,address _SMDSetMotorStepSize,isa 0

	.dwfde $C$DW$CIE, _SMDSetMotorStepSize
$C$DW$103	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$103, DW_AT_name("Motor")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_reg0]

$C$DW$104	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$104, DW_AT_name("Step")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_Step")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 189 | void SMDSetMotorStepSize(Uint16 Motor, Uint16 Step)                    
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDSetMotorStepSize          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_SMDSetMotorStepSize:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$105	.dwtag  DW_TAG_variable
	.dwattr $C$DW$105, DW_AT_name("StepSize")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_StepSize")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -3]

$C$DW$106	.dwtag  DW_TAG_variable
	.dwattr $C$DW$106, DW_AT_name("Motor")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_breg20 -4]

$C$DW$107	.dwtag  DW_TAG_variable
	.dwattr $C$DW$107, DW_AT_name("Step")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_Step")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$107, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 191 | Uint16 StepSize[3];                                                    
; 192 | //!Configure the step size to operate the motor based on the step size
;     | selected                                                               
;----------------------------------------------------------------------
        MOV       *-SP[5],AH            ; [CPU_] |190| 
        MOV       *-SP[4],AL            ; [CPU_] |190| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 193,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 193 | switch(Step)                                                           
; 195 | case FULL_STEP:                                                        
;----------------------------------------------------------------------
        B         $C$L8,UNC             ; [CPU_] |193| 
        ; branch occurs ; [] |193| 
$C$L1:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 196,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 196 | StepSize[0] = FULLL_STEP_MODE0;                                        
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |196| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 197,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 197 | StepSize[1] = FULLL_STEP_MODE1;                                        
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |197| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 198,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 198 | StepSize[2] = FULLL_STEP_MODE2;                                        
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |198| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 199,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 199 | break;                                                                 
; 201 | case TWO_MICRO_STEP:                                                   
;----------------------------------------------------------------------
        B         $C$L15,UNC            ; [CPU_] |199| 
        ; branch occurs ; [] |199| 
$C$L2:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 202,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 202 | StepSize[0] = TWO_MICRO_STEP_MODE0;                                    
;----------------------------------------------------------------------
        MOVB      *-SP[3],#1,UNC        ; [CPU_] |202| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 203,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 203 | StepSize[1] = TWO_MICRO_STEP_MODE1;                                    
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |203| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 204,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 204 | StepSize[2] = TWO_MICRO_STEP_MODE2;                                    
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |204| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 205,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 205 | break;                                                                 
; 207 | case FOUR_MICRO_STEP:                                                  
;----------------------------------------------------------------------
        B         $C$L15,UNC            ; [CPU_] |205| 
        ; branch occurs ; [] |205| 
$C$L3:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 208,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 208 | StepSize[0] = FOUR_MICRO_STEP_MODE0;                                   
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |208| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 209,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 209 | StepSize[1] = FOUR_MICRO_STEP_MODE1;                                   
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |209| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 210,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 210 | StepSize[2] = FOUR_MICRO_STEP_MODE2;                                   
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |210| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 211,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 211 | break;                                                                 
; 213 | case EIGHT_MICRO_STEP:                                                 
;----------------------------------------------------------------------
        B         $C$L15,UNC            ; [CPU_] |211| 
        ; branch occurs ; [] |211| 
$C$L4:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 214,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 214 | StepSize[0] = EIGHT_MICRO_STEP_MODE0;                                  
;----------------------------------------------------------------------
        MOVB      *-SP[3],#1,UNC        ; [CPU_] |214| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 215,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 215 | StepSize[1] = EIGHT_MICRO_STEP_MODE1;                                  
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |215| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 216,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 216 | StepSize[2] = EIGHT_MICRO_STEP_MODE2;                                  
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |216| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 217,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 217 | break;                                                                 
; 220 | case SIXTEEN_MICRO_STEP:                                               
;----------------------------------------------------------------------
        B         $C$L15,UNC            ; [CPU_] |217| 
        ; branch occurs ; [] |217| 
$C$L5:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 221,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 221 | StepSize[0] = SIXTEEN_MICRO_STEP_MODE0;                                
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |221| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 222,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 222 | StepSize[1] = SIXTEEN_MICRO_STEP_MODE1;                                
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |222| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 223,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 223 | StepSize[2] = SIXTEEN_MICRO_STEP_MODE2;                                
;----------------------------------------------------------------------
        MOVB      *-SP[1],#1,UNC        ; [CPU_] |223| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 224,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 224 | break;                                                                 
; 226 | case THIRTYTWO_MICRO_STEP:                                             
;----------------------------------------------------------------------
        B         $C$L15,UNC            ; [CPU_] |224| 
        ; branch occurs ; [] |224| 
$C$L6:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 227,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 227 | StepSize[0] = THIRTYTWO_MICRO_STEP_MODE0;                              
;----------------------------------------------------------------------
        MOVB      *-SP[3],#1,UNC        ; [CPU_] |227| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 228,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 228 | StepSize[1] = THIRTYTWO_MICRO_STEP_MODE1;                              
;----------------------------------------------------------------------
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |228| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 229,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 229 | StepSize[2] = THIRTYTWO_MICRO_STEP_MODE2;                              
;----------------------------------------------------------------------
        MOVB      *-SP[1],#1,UNC        ; [CPU_] |229| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 230,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 230 | break;                                                                 
; 232 | default:                                                               
;----------------------------------------------------------------------
        B         $C$L15,UNC            ; [CPU_] |230| 
        ; branch occurs ; [] |230| 
$C$L7:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 233,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 233 | StepSize[0] = FULLL_STEP_MODE0;                                        
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |233| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 234,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 234 | StepSize[1] = FULLL_STEP_MODE1;                                        
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |234| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 235,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 235 | StepSize[2] = FULLL_STEP_MODE2;                                        
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |235| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 236,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 236 | break;                                                                 
; 240 | //!Set the GPIO and for specific modes for the motor selected          
; 241 | switch(Motor)                                                          
; 243 | case MOTOR1_SELECT:                                                    
;----------------------------------------------------------------------
        B         $C$L15,UNC            ; [CPU_] |236| 
        ; branch occurs ; [] |236| 
$C$L8:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 193,column 2,is_stmt,isa 0
        MOV       AL,*-SP[5]            ; [CPU_] |193| 
        CMPB      AL,#8                 ; [CPU_] |193| 
        B         $C$L9,GT              ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
        CMPB      AL,#8                 ; [CPU_] |193| 
        B         $C$L4,EQ              ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
        CMPB      AL,#1                 ; [CPU_] |193| 
        B         $C$L1,EQ              ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
        CMPB      AL,#2                 ; [CPU_] |193| 
        B         $C$L2,EQ              ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
        CMPB      AL,#4                 ; [CPU_] |193| 
        B         $C$L3,EQ              ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
        B         $C$L7,UNC             ; [CPU_] |193| 
        ; branch occurs ; [] |193| 
$C$L9:    
        CMPB      AL,#16                ; [CPU_] |193| 
        B         $C$L5,EQ              ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
        CMPB      AL,#32                ; [CPU_] |193| 
        B         $C$L6,EQ              ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
        B         $C$L7,UNC             ; [CPU_] |193| 
        ; branch occurs ; [] |193| 
$C$L10:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 244,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 244 | IGWriteMotorGpio(M1_MODE0, StepSize[0]);                               
;----------------------------------------------------------------------
        MOV       AH,*-SP[3]            ; [CPU_] |244| 
        MOVB      AL,#4                 ; [CPU_] |244| 
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$108, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |244| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |244| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 245,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 245 | IGWriteMotorGpio(M1_MODE1, StepSize[1]);                               
;----------------------------------------------------------------------
        MOVB      AL,#3                 ; [CPU_] |245| 
        MOV       AH,*-SP[2]            ; [CPU_] |245| 
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$109, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |245| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |245| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 246,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 246 | IGWriteMotorGpio(M1_MODE2, StepSize[2]);                               
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |246| 
        MOV       AH,*-SP[1]            ; [CPU_] |246| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$110, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |246| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |246| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 247,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 247 | break;                                                                 
; 249 | case MOTOR2_SELECT:                                                    
;----------------------------------------------------------------------
        B         $C$L17,UNC            ; [CPU_] |247| 
        ; branch occurs ; [] |247| 
$C$L11:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 250,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 250 | IGWriteMotorGpio(M2_MODE0, StepSize[0]);                               
;----------------------------------------------------------------------
        MOV       AH,*-SP[3]            ; [CPU_] |250| 
        MOVB      AL,#13                ; [CPU_] |250| 
$C$DW$111	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$111, DW_AT_low_pc(0x00)
	.dwattr $C$DW$111, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$111, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |250| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |250| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 251,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 251 | IGWriteMotorGpio(M2_MODE1, StepSize[1]);                               
;----------------------------------------------------------------------
        MOVB      AL,#12                ; [CPU_] |251| 
        MOV       AH,*-SP[2]            ; [CPU_] |251| 
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$112, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |251| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |251| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 252,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 252 | IGWriteMotorGpio(M2_MODE2, StepSize[2]);                               
;----------------------------------------------------------------------
        MOVB      AL,#11                ; [CPU_] |252| 
        MOV       AH,*-SP[1]            ; [CPU_] |252| 
$C$DW$113	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$113, DW_AT_low_pc(0x00)
	.dwattr $C$DW$113, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$113, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |252| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |252| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 253,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 253 | break;                                                                 
; 255 | case MOTOR3_SELECT:                                                    
;----------------------------------------------------------------------
        B         $C$L17,UNC            ; [CPU_] |253| 
        ; branch occurs ; [] |253| 
$C$L12:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 256,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 256 | IGWriteMotorGpio(M3_MODE0, StepSize[0]);                               
;----------------------------------------------------------------------
        MOV       AH,*-SP[3]            ; [CPU_] |256| 
        MOVB      AL,#25                ; [CPU_] |256| 
$C$DW$114	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$114, DW_AT_low_pc(0x00)
	.dwattr $C$DW$114, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$114, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |256| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |256| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 257,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 257 | IGWriteMotorGpio(M3_MODE1, StepSize[1]);                               
;----------------------------------------------------------------------
        MOVB      AL,#24                ; [CPU_] |257| 
        MOV       AH,*-SP[2]            ; [CPU_] |257| 
$C$DW$115	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$115, DW_AT_low_pc(0x00)
	.dwattr $C$DW$115, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$115, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |257| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |257| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 258,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 258 | IGWriteMotorGpio(M3_MODE2, StepSize[2]);                               
;----------------------------------------------------------------------
        MOVB      AL,#23                ; [CPU_] |258| 
        MOV       AH,*-SP[1]            ; [CPU_] |258| 
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$116, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |258| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |258| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 259,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 259 | break;                                                                 
; 261 | case MOTOR4_SELECT:                                                    
;----------------------------------------------------------------------
        B         $C$L17,UNC            ; [CPU_] |259| 
        ; branch occurs ; [] |259| 
$C$L13:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 262,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 262 | IGWriteMotorGpio(M4_MODE0, StepSize[0]);                               
;----------------------------------------------------------------------
        MOV       AH,*-SP[3]            ; [CPU_] |262| 
        MOVB      AL,#37                ; [CPU_] |262| 
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$117, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |262| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |262| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 263,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 263 | IGWriteMotorGpio(M4_MODE1, StepSize[1]);                               
;----------------------------------------------------------------------
        MOVB      AL,#35                ; [CPU_] |263| 
        MOV       AH,*-SP[2]            ; [CPU_] |263| 
$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$118, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |263| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |263| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 264,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 264 | IGWriteMotorGpio(M4_MODE2, StepSize[2]);                               
;----------------------------------------------------------------------
        MOVB      AL,#34                ; [CPU_] |264| 
        MOV       AH,*-SP[1]            ; [CPU_] |264| 
$C$DW$119	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$119, DW_AT_low_pc(0x00)
	.dwattr $C$DW$119, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$119, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |264| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |264| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 265,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 265 | break;                                                                 
; 267 | case MOTOR5_SELECT:                                                    
;----------------------------------------------------------------------
        B         $C$L17,UNC            ; [CPU_] |265| 
        ; branch occurs ; [] |265| 
$C$L14:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 268,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 268 | IGWriteMotorGpio(M5_MODE0, StepSize[0]);                               
;----------------------------------------------------------------------
        MOV       AH,*-SP[3]            ; [CPU_] |268| 
        MOVB      AL,#99                ; [CPU_] |268| 
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$120, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |268| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |268| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 269,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 269 | IGWriteMotorGpio(M5_MODE1, StepSize[1]);                               
;----------------------------------------------------------------------
        MOVB      AL,#98                ; [CPU_] |269| 
        MOV       AH,*-SP[2]            ; [CPU_] |269| 
$C$DW$121	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$121, DW_AT_low_pc(0x00)
	.dwattr $C$DW$121, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$121, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |269| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |269| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 270,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 270 | IGWriteMotorGpio(M5_MODE2, StepSize[2]);                               
;----------------------------------------------------------------------
        MOVB      AL,#97                ; [CPU_] |270| 
        MOV       AH,*-SP[1]            ; [CPU_] |270| 
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$122, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |270| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |270| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 271,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 271 | break;                                                                 
; 273 | default:                                                               
; 274 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L17,UNC            ; [CPU_] |271| 
        ; branch occurs ; [] |271| 
$C$L15:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 241,column 2,is_stmt,isa 0
        MOV       AL,*-SP[4]            ; [CPU_] |241| 
        CMPB      AL,#3                 ; [CPU_] |241| 
        B         $C$L16,GT             ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
        CMPB      AL,#3                 ; [CPU_] |241| 
        B         $C$L12,EQ             ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
        CMPB      AL,#1                 ; [CPU_] |241| 
        B         $C$L10,EQ             ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
        CMPB      AL,#2                 ; [CPU_] |241| 
        B         $C$L11,EQ             ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
        B         $C$L17,UNC            ; [CPU_] |241| 
        ; branch occurs ; [] |241| 
$C$L16:    
        CMPB      AL,#4                 ; [CPU_] |241| 
        B         $C$L13,EQ             ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
        CMPB      AL,#5                 ; [CPU_] |241| 
        B         $C$L14,EQ             ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
        B         $C$L17,UNC            ; [CPU_] |241| 
        ; branch occurs ; [] |241| 
$C$L17:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$123	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$123, DW_AT_low_pc(0x00)
	.dwattr $C$DW$123, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$102, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$102, DW_AT_TI_end_line(0x114)
	.dwattr $C$DW$102, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$102

	.sect	".text:_SMDSetMotorEnable"
	.clink
	.global	_SMDSetMotorEnable

$C$DW$124	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$124, DW_AT_name("SMDSetMotorEnable")
	.dwattr $C$DW$124, DW_AT_low_pc(_SMDSetMotorEnable)
	.dwattr $C$DW$124, DW_AT_high_pc(0x00)
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_SMDSetMotorEnable")
	.dwattr $C$DW$124, DW_AT_external
	.dwattr $C$DW$124, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$124, DW_AT_TI_begin_line(0x121)
	.dwattr $C$DW$124, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$124, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/StepperMotorDriver.c",line 290,column 1,is_stmt,address _SMDSetMotorEnable,isa 0

	.dwfde $C$DW$CIE, _SMDSetMotorEnable
$C$DW$125	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$125, DW_AT_name("Motor")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$125, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 289 | void SMDSetMotorEnable(Uint16 Motor)                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDSetMotorEnable            FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_SMDSetMotorEnable:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$126	.dwtag  DW_TAG_variable
	.dwattr $C$DW$126, DW_AT_name("Motor")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 291 | //!Enbale the motor based on the motor selected                        
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |290| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 292,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 292 | switch(Motor)                                                          
; 294 | case 1:                                                                
; 295 |     //!Enable motor 1                                                  
;----------------------------------------------------------------------
        B         $C$L23,UNC            ; [CPU_] |292| 
        ; branch occurs ; [] |292| 
$C$L18:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 296,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 296 | IGWriteMotorGpio(M1_ENBL, MOTOR_ENABLE);                               
;----------------------------------------------------------------------
        MOVB      AL,#6                 ; [CPU_] |296| 
        MOVB      AH,#0                 ; [CPU_] |296| 
$C$DW$127	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$127, DW_AT_low_pc(0x00)
	.dwattr $C$DW$127, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$127, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |296| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |296| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 297,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 297 | break;                                                                 
; 299 | case 2:                                                                
; 300 | //!Enable motor 2                                                      
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |297| 
        ; branch occurs ; [] |297| 
$C$L19:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 301,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 301 | IGWriteMotorGpio(M2_ENBL, MOTOR_ENABLE);                               
;----------------------------------------------------------------------
        MOVB      AL,#15                ; [CPU_] |301| 
        MOVB      AH,#0                 ; [CPU_] |301| 
$C$DW$128	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$128, DW_AT_low_pc(0x00)
	.dwattr $C$DW$128, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$128, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |301| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |301| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 302,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 302 | break;                                                                 
; 304 | case 3:                                                                
; 305 | //!Enable motor 3                                                      
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |302| 
        ; branch occurs ; [] |302| 
$C$L20:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 306,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 306 | IGWriteMotorGpio(M3_ENBL, MOTOR_ENABLE);                               
;----------------------------------------------------------------------
        MOVB      AL,#27                ; [CPU_] |306| 
        MOVB      AH,#0                 ; [CPU_] |306| 
$C$DW$129	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$129, DW_AT_low_pc(0x00)
	.dwattr $C$DW$129, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$129, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |306| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |306| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 307,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 307 | break;                                                                 
; 309 | case 4:                                                                
; 310 | //!Enable motor 4                                                      
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |307| 
        ; branch occurs ; [] |307| 
$C$L21:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 311,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 311 | IGWriteMotorGpio(M4_ENBL, MOTOR_ENABLE);                               
;----------------------------------------------------------------------
        MOVB      AL,#91                ; [CPU_] |311| 
        MOVB      AH,#0                 ; [CPU_] |311| 
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$130, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |311| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |311| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 312,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 312 | break;                                                                 
; 314 | case 5:                                                                
; 315 | //!Enable motor 5                                                      
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |312| 
        ; branch occurs ; [] |312| 
$C$L22:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 316,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 316 | IGWriteMotorGpio(M5_ENBL, MOTOR_ENABLE);                               
; 318 | default:                                                               
;----------------------------------------------------------------------
        MOVB      AL,#101               ; [CPU_] |316| 
        MOVB      AH,#0                 ; [CPU_] |316| 
$C$DW$131	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$131, DW_AT_low_pc(0x00)
	.dwattr $C$DW$131, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$131, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |316| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |316| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 319,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 319 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |319| 
        ; branch occurs ; [] |319| 
$C$L23:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 292,column 2,is_stmt,isa 0
        CMPB      AL,#3                 ; [CPU_] |292| 
        B         $C$L24,GT             ; [CPU_] |292| 
        ; branchcc occurs ; [] |292| 
        CMPB      AL,#3                 ; [CPU_] |292| 
        B         $C$L20,EQ             ; [CPU_] |292| 
        ; branchcc occurs ; [] |292| 
        CMPB      AL,#1                 ; [CPU_] |292| 
        B         $C$L18,EQ             ; [CPU_] |292| 
        ; branchcc occurs ; [] |292| 
        CMPB      AL,#2                 ; [CPU_] |292| 
        B         $C$L19,EQ             ; [CPU_] |292| 
        ; branchcc occurs ; [] |292| 
        B         $C$L25,UNC            ; [CPU_] |292| 
        ; branch occurs ; [] |292| 
$C$L24:    
        CMPB      AL,#4                 ; [CPU_] |292| 
        B         $C$L21,EQ             ; [CPU_] |292| 
        ; branchcc occurs ; [] |292| 
        CMPB      AL,#5                 ; [CPU_] |292| 
        B         $C$L22,EQ             ; [CPU_] |292| 
        ; branchcc occurs ; [] |292| 
        B         $C$L25,UNC            ; [CPU_] |292| 
        ; branch occurs ; [] |292| 
$C$L25:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$132	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$132, DW_AT_low_pc(0x00)
	.dwattr $C$DW$132, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$124, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$124, DW_AT_TI_end_line(0x141)
	.dwattr $C$DW$124, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$124

	.sect	".text:_SMDSetMotorDisable"
	.clink
	.global	_SMDSetMotorDisable

$C$DW$133	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$133, DW_AT_name("SMDSetMotorDisable")
	.dwattr $C$DW$133, DW_AT_low_pc(_SMDSetMotorDisable)
	.dwattr $C$DW$133, DW_AT_high_pc(0x00)
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_SMDSetMotorDisable")
	.dwattr $C$DW$133, DW_AT_external
	.dwattr $C$DW$133, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$133, DW_AT_TI_begin_line(0x14e)
	.dwattr $C$DW$133, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$133, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/StepperMotorDriver.c",line 335,column 1,is_stmt,address _SMDSetMotorDisable,isa 0

	.dwfde $C$DW$CIE, _SMDSetMotorDisable
$C$DW$134	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$134, DW_AT_name("Motor")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 334 | void SMDSetMotorDisable(Uint16 Motor)                                  
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDSetMotorDisable           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_SMDSetMotorDisable:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$135	.dwtag  DW_TAG_variable
	.dwattr $C$DW$135, DW_AT_name("Motor")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 336 | //!Disable the motor based on the motor selected                       
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |335| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 337,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 337 | switch(Motor)                                                          
; 339 | case MOTOR1_SELECT:                                                    
; 340 |     //!Disable motor 1                                                 
;----------------------------------------------------------------------
        B         $C$L31,UNC            ; [CPU_] |337| 
        ; branch occurs ; [] |337| 
$C$L26:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 341,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 341 | IGWriteMotorGpio(M1_ENBL, MOTOR_DISABLE);                              
;----------------------------------------------------------------------
        MOVB      AL,#6                 ; [CPU_] |341| 
        MOVB      AH,#1                 ; [CPU_] |341| 
$C$DW$136	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$136, DW_AT_low_pc(0x00)
	.dwattr $C$DW$136, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$136, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |341| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |341| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 342,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 342 | break;                                                                 
; 344 | case MOTOR2_SELECT:                                                    
; 345 | //!Disable motor 2                                                     
;----------------------------------------------------------------------
        B         $C$L33,UNC            ; [CPU_] |342| 
        ; branch occurs ; [] |342| 
$C$L27:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 346,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 346 | IGWriteMotorGpio(M2_ENBL, MOTOR_DISABLE);                              
;----------------------------------------------------------------------
        MOVB      AL,#15                ; [CPU_] |346| 
        MOVB      AH,#1                 ; [CPU_] |346| 
$C$DW$137	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$137, DW_AT_low_pc(0x00)
	.dwattr $C$DW$137, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$137, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |346| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |346| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 347,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 347 | break;                                                                 
; 349 | case MOTOR3_SELECT:                                                    
; 350 | //!Disable motor 3                                                     
;----------------------------------------------------------------------
        B         $C$L33,UNC            ; [CPU_] |347| 
        ; branch occurs ; [] |347| 
$C$L28:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 351,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 351 | IGWriteMotorGpio(M3_ENBL, MOTOR_DISABLE);                              
;----------------------------------------------------------------------
        MOVB      AL,#27                ; [CPU_] |351| 
        MOVB      AH,#1                 ; [CPU_] |351| 
$C$DW$138	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$138, DW_AT_low_pc(0x00)
	.dwattr $C$DW$138, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$138, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |351| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |351| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 352,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 352 | break;                                                                 
; 354 | case MOTOR4_SELECT:                                                    
; 355 | //!Disable motor 4                                                     
;----------------------------------------------------------------------
        B         $C$L33,UNC            ; [CPU_] |352| 
        ; branch occurs ; [] |352| 
$C$L29:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 356,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 356 | IGWriteMotorGpio(M4_ENBL, MOTOR_DISABLE);                              
;----------------------------------------------------------------------
        MOVB      AL,#91                ; [CPU_] |356| 
        MOVB      AH,#1                 ; [CPU_] |356| 
$C$DW$139	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$139, DW_AT_low_pc(0x00)
	.dwattr $C$DW$139, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$139, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |356| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |356| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 357,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 357 | break;                                                                 
; 359 | case MOTOR5_SELECT:                                                    
; 360 | //!Disable motor 5                                                     
;----------------------------------------------------------------------
        B         $C$L33,UNC            ; [CPU_] |357| 
        ; branch occurs ; [] |357| 
$C$L30:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 361,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 361 | IGWriteMotorGpio(M5_ENBL, MOTOR_DISABLE);                              
; 363 | default:                                                               
;----------------------------------------------------------------------
        MOVB      AL,#101               ; [CPU_] |361| 
        MOVB      AH,#1                 ; [CPU_] |361| 
$C$DW$140	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$140, DW_AT_low_pc(0x00)
	.dwattr $C$DW$140, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$140, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |361| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |361| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 364,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 364 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L33,UNC            ; [CPU_] |364| 
        ; branch occurs ; [] |364| 
$C$L31:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 337,column 2,is_stmt,isa 0
        CMPB      AL,#3                 ; [CPU_] |337| 
        B         $C$L32,GT             ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
        CMPB      AL,#3                 ; [CPU_] |337| 
        B         $C$L28,EQ             ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
        CMPB      AL,#1                 ; [CPU_] |337| 
        B         $C$L26,EQ             ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
        CMPB      AL,#2                 ; [CPU_] |337| 
        B         $C$L27,EQ             ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
        B         $C$L33,UNC            ; [CPU_] |337| 
        ; branch occurs ; [] |337| 
$C$L32:    
        CMPB      AL,#4                 ; [CPU_] |337| 
        B         $C$L29,EQ             ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
        CMPB      AL,#5                 ; [CPU_] |337| 
        B         $C$L30,EQ             ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
        B         $C$L33,UNC            ; [CPU_] |337| 
        ; branch occurs ; [] |337| 
$C$L33:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$141	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$141, DW_AT_low_pc(0x00)
	.dwattr $C$DW$141, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$133, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$133, DW_AT_TI_end_line(0x16e)
	.dwattr $C$DW$133, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$133

	.sect	".text:_SMDSetMotorDirection"
	.clink
	.global	_SMDSetMotorDirection

$C$DW$142	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$142, DW_AT_name("SMDSetMotorDirection")
	.dwattr $C$DW$142, DW_AT_low_pc(_SMDSetMotorDirection)
	.dwattr $C$DW$142, DW_AT_high_pc(0x00)
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_SMDSetMotorDirection")
	.dwattr $C$DW$142, DW_AT_external
	.dwattr $C$DW$142, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$142, DW_AT_TI_begin_line(0x17b)
	.dwattr $C$DW$142, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$142, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/StepperMotorDriver.c",line 380,column 1,is_stmt,address _SMDSetMotorDirection,isa 0

	.dwfde $C$DW$CIE, _SMDSetMotorDirection
$C$DW$143	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$143, DW_AT_name("Motor")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg0]

$C$DW$144	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$144, DW_AT_name("Direction")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_Direction")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$144, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 379 | void SMDSetMotorDirection(Uint16 Motor, Uint16 Direction)              
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDSetMotorDirection         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SMDSetMotorDirection:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$145	.dwtag  DW_TAG_variable
	.dwattr $C$DW$145, DW_AT_name("Motor")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$145, DW_AT_location[DW_OP_breg20 -1]

$C$DW$146	.dwtag  DW_TAG_variable
	.dwattr $C$DW$146, DW_AT_name("Direction")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_Direction")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$146, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 381 | //!Set the motor direction based on the motor selected                 
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |380| 
        MOV       *-SP[1],AL            ; [CPU_] |380| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 382,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 382 | switch(Motor)                                                          
; 384 | case MOTOR1_SELECT:                                                    
; 385 |     //!Set the direction of motor 1                                    
;----------------------------------------------------------------------
        B         $C$L41,UNC            ; [CPU_] |382| 
        ; branch occurs ; [] |382| 
$C$L34:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 386,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 386 | IGWriteMotorGpio(M1_DIR, Direction);                                   
;----------------------------------------------------------------------
        MOVB      AL,#7                 ; [CPU_] |386| 
$C$DW$147	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$147, DW_AT_low_pc(0x00)
	.dwattr $C$DW$147, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$147, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |386| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |386| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 387,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 387 | break;                                                                 
; 389 | case MOTOR2_SELECT:                                                    
; 390 | //!Set the direction of motor 2                                        
;----------------------------------------------------------------------
        B         $C$L43,UNC            ; [CPU_] |387| 
        ; branch occurs ; [] |387| 
$C$L35:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 391,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 391 | IGWriteMotorGpio(M2_DIR, Direction);                                   
;----------------------------------------------------------------------
        MOVB      AL,#20                ; [CPU_] |391| 
$C$DW$148	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$148, DW_AT_low_pc(0x00)
	.dwattr $C$DW$148, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$148, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |391| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |391| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 392,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 392 | break;                                                                 
; 394 | case MOTOR3_SELECT:                                                    
; 395 | //!Set the direction of motor 3                                        
;----------------------------------------------------------------------
        B         $C$L43,UNC            ; [CPU_] |392| 
        ; branch occurs ; [] |392| 
$C$L36:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 396,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 396 | IGWriteMotorGpio(M3_DIR, Direction);                                   
;----------------------------------------------------------------------
        MOVB      AL,#28                ; [CPU_] |396| 
$C$DW$149	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$149, DW_AT_low_pc(0x00)
	.dwattr $C$DW$149, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$149, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |396| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |396| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 397,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 397 | break;                                                                 
; 399 | case MOTOR4_SELECT:                                                    
; 400 | //!Set the direction of motor 4                                        
;----------------------------------------------------------------------
        B         $C$L43,UNC            ; [CPU_] |397| 
        ; branch occurs ; [] |397| 
$C$L37:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 401,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 401 | IGWriteMotorGpio(M4_DIR, (int)(!Direction));  //QA_C                   
;----------------------------------------------------------------------
        MOVB      AL,#0                 ; [CPU_] |401| 
        CMPB      AH,#0                 ; [CPU_] |401| 
        MOVB      XAR6,#0               ; [CPU_] |401| 
        B         $C$L38,EQ             ; [CPU_] |401| 
        ; branchcc occurs ; [] |401| 
        MOVB      AL,#1                 ; [CPU_] |401| 
$C$L38:    
        CMPB      AL,#0                 ; [CPU_] |401| 
        B         $C$L39,NEQ            ; [CPU_] |401| 
        ; branchcc occurs ; [] |401| 
        MOVB      XAR6,#1               ; [CPU_] |401| 
$C$L39:    
        MOV       AH,AR6                ; [CPU_] |401| 
        MOVB      AL,#150               ; [CPU_] |401| 
$C$DW$150	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$150, DW_AT_low_pc(0x00)
	.dwattr $C$DW$150, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$150, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |401| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |401| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 402,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 402 | break;                                                                 
; 404 | case MOTOR5_SELECT:                                                    
; 405 | //!Set the direction of motor 5                                        
;----------------------------------------------------------------------
        B         $C$L43,UNC            ; [CPU_] |402| 
        ; branch occurs ; [] |402| 
$C$L40:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 406,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 406 | IGWriteMotorGpio(M5_DIR, Direction);                                   
; 408 | default:                                                               
;----------------------------------------------------------------------
        MOVB      AL,#102               ; [CPU_] |406| 
$C$DW$151	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$151, DW_AT_low_pc(0x00)
	.dwattr $C$DW$151, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$151, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |406| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |406| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 409,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 409 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L43,UNC            ; [CPU_] |409| 
        ; branch occurs ; [] |409| 
$C$L41:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 382,column 2,is_stmt,isa 0
        CMPB      AL,#3                 ; [CPU_] |382| 
        B         $C$L42,GT             ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
        CMPB      AL,#3                 ; [CPU_] |382| 
        B         $C$L36,EQ             ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
        CMPB      AL,#1                 ; [CPU_] |382| 
        B         $C$L34,EQ             ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
        CMPB      AL,#2                 ; [CPU_] |382| 
        B         $C$L35,EQ             ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
        B         $C$L43,UNC            ; [CPU_] |382| 
        ; branch occurs ; [] |382| 
$C$L42:    
        CMPB      AL,#4                 ; [CPU_] |382| 
        B         $C$L37,EQ             ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
        CMPB      AL,#5                 ; [CPU_] |382| 
        B         $C$L40,EQ             ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
        B         $C$L43,UNC            ; [CPU_] |382| 
        ; branch occurs ; [] |382| 
$C$L43:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$152	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$152, DW_AT_low_pc(0x00)
	.dwattr $C$DW$152, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$142, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$142, DW_AT_TI_end_line(0x19b)
	.dwattr $C$DW$142, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$142

	.sect	".text:_SMDSetMotorDecay"
	.clink
	.global	_SMDSetMotorDecay

$C$DW$153	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$153, DW_AT_name("SMDSetMotorDecay")
	.dwattr $C$DW$153, DW_AT_low_pc(_SMDSetMotorDecay)
	.dwattr $C$DW$153, DW_AT_high_pc(0x00)
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_SMDSetMotorDecay")
	.dwattr $C$DW$153, DW_AT_external
	.dwattr $C$DW$153, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$153, DW_AT_TI_begin_line(0x1a8)
	.dwattr $C$DW$153, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$153, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/StepperMotorDriver.c",line 425,column 1,is_stmt,address _SMDSetMotorDecay,isa 0

	.dwfde $C$DW$CIE, _SMDSetMotorDecay
$C$DW$154	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$154, DW_AT_name("Motor")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$154, DW_AT_location[DW_OP_reg0]

$C$DW$155	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$155, DW_AT_name("Decay")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_Decay")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$155, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 424 | void SMDSetMotorDecay(Uint16 Motor, Uint16 Decay)                      
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDSetMotorDecay             FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SMDSetMotorDecay:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$156	.dwtag  DW_TAG_variable
	.dwattr $C$DW$156, DW_AT_name("Motor")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$156, DW_AT_location[DW_OP_breg20 -1]

$C$DW$157	.dwtag  DW_TAG_variable
	.dwattr $C$DW$157, DW_AT_name("Decay")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_Decay")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$157, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 426 | //!Set the decay state of the motor selected                           
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |425| 
        MOV       *-SP[1],AL            ; [CPU_] |425| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 427,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 427 | switch(Motor)                                                          
; 429 | case MOTOR1_SELECT:                                                    
; 430 |     //!Set the decay state of motor 1                                  
;----------------------------------------------------------------------
        B         $C$L49,UNC            ; [CPU_] |427| 
        ; branch occurs ; [] |427| 
$C$L44:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 431,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 431 | IGWriteMotorGpio(M1_DECAY, Decay);                                     
;----------------------------------------------------------------------
        MOVB      AL,#16                ; [CPU_] |431| 
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$158, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |431| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |431| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 432,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 432 | break;                                                                 
; 434 | case MOTOR2_SELECT:                                                    
; 435 | //!Set the decay state of motor 2                                      
;----------------------------------------------------------------------
        B         $C$L51,UNC            ; [CPU_] |432| 
        ; branch occurs ; [] |432| 
$C$L45:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 436,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 436 | IGWriteMotorGpio(M2_DECAY, Decay);                                     
;----------------------------------------------------------------------
        MOVB      AL,#21                ; [CPU_] |436| 
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$159, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |436| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |436| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 437,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 437 | break;                                                                 
; 439 | case MOTOR3_SELECT:                                                    
; 440 | //!Set the decay state of motor 3                                      
;----------------------------------------------------------------------
        B         $C$L51,UNC            ; [CPU_] |437| 
        ; branch occurs ; [] |437| 
$C$L46:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 441,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 441 | IGWriteMotorGpio(M3_DECAY, Decay);                                     
;----------------------------------------------------------------------
        MOVB      AL,#33                ; [CPU_] |441| 
$C$DW$160	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$160, DW_AT_low_pc(0x00)
	.dwattr $C$DW$160, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$160, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |441| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |441| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 442,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 442 | break;                                                                 
; 444 | case MOTOR4_SELECT:                                                    
; 445 | //!Set the decay state of motor 4                                      
;----------------------------------------------------------------------
        B         $C$L51,UNC            ; [CPU_] |442| 
        ; branch occurs ; [] |442| 
$C$L47:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 446,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 446 | IGWriteMotorGpio(M4_DECAY, Decay);                                     
;----------------------------------------------------------------------
        MOVB      AL,#95                ; [CPU_] |446| 
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$161, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |446| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |446| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 447,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 447 | break;                                                                 
; 449 | case MOTOR5_SELECT:                                                    
; 450 | //!Set the decay state of motor 5                                      
;----------------------------------------------------------------------
        B         $C$L51,UNC            ; [CPU_] |447| 
        ; branch occurs ; [] |447| 
$C$L48:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 451,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 451 | IGWriteMotorGpio(M5_DECAY, Decay);                                     
; 453 | default:                                                               
;----------------------------------------------------------------------
        MOVB      AL,#103               ; [CPU_] |451| 
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$162, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |451| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |451| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 454,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 454 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L51,UNC            ; [CPU_] |454| 
        ; branch occurs ; [] |454| 
$C$L49:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 427,column 2,is_stmt,isa 0
        CMPB      AL,#3                 ; [CPU_] |427| 
        B         $C$L50,GT             ; [CPU_] |427| 
        ; branchcc occurs ; [] |427| 
        CMPB      AL,#3                 ; [CPU_] |427| 
        B         $C$L46,EQ             ; [CPU_] |427| 
        ; branchcc occurs ; [] |427| 
        CMPB      AL,#1                 ; [CPU_] |427| 
        B         $C$L44,EQ             ; [CPU_] |427| 
        ; branchcc occurs ; [] |427| 
        CMPB      AL,#2                 ; [CPU_] |427| 
        B         $C$L45,EQ             ; [CPU_] |427| 
        ; branchcc occurs ; [] |427| 
        B         $C$L51,UNC            ; [CPU_] |427| 
        ; branch occurs ; [] |427| 
$C$L50:    
        CMPB      AL,#4                 ; [CPU_] |427| 
        B         $C$L47,EQ             ; [CPU_] |427| 
        ; branchcc occurs ; [] |427| 
        CMPB      AL,#5                 ; [CPU_] |427| 
        B         $C$L48,EQ             ; [CPU_] |427| 
        ; branchcc occurs ; [] |427| 
        B         $C$L51,UNC            ; [CPU_] |427| 
        ; branch occurs ; [] |427| 
$C$L51:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$153, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$153, DW_AT_TI_end_line(0x1c8)
	.dwattr $C$DW$153, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$153

	.sect	".text:_SMDGetMotorFault"
	.clink
	.global	_SMDGetMotorFault

$C$DW$164	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$164, DW_AT_name("SMDGetMotorFault")
	.dwattr $C$DW$164, DW_AT_low_pc(_SMDGetMotorFault)
	.dwattr $C$DW$164, DW_AT_high_pc(0x00)
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_SMDGetMotorFault")
	.dwattr $C$DW$164, DW_AT_external
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$164, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$164, DW_AT_TI_begin_line(0x1d5)
	.dwattr $C$DW$164, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$164, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/StepperMotorDriver.c",line 470,column 1,is_stmt,address _SMDGetMotorFault,isa 0

	.dwfde $C$DW$CIE, _SMDGetMotorFault
$C$DW$165	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$165, DW_AT_name("Motor")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 469 | Uint16 SMDGetMotorFault(Uint16 Motor)                                  
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDGetMotorFault             FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SMDGetMotorFault:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$166	.dwtag  DW_TAG_variable
	.dwattr $C$DW$166, DW_AT_name("Motor")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_Motor")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$166, DW_AT_location[DW_OP_breg20 -1]

$C$DW$167	.dwtag  DW_TAG_variable
	.dwattr $C$DW$167, DW_AT_name("FaultStatus")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_FaultStatus")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$167, DW_AT_location[DW_OP_breg20 -2]

        MOV       *-SP[1],AL            ; [CPU_] |470| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 471,column 20,is_stmt,isa 0
;----------------------------------------------------------------------
; 471 | Uint16 FaultStatus=0x00;                                               
; 472 | //!Get the motor fault status based on the motor selected              
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |471| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 473,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 473 | switch(Motor)                                                          
; 475 | case MOTOR1_SELECT:                                                    
; 476 |     //!Get the fault status of motor 1                                 
;----------------------------------------------------------------------
        B         $C$L57,UNC            ; [CPU_] |473| 
        ; branch occurs ; [] |473| 
$C$L52:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 477,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 477 | FaultStatus = IGReadMotorGpio(M1_FAULT);                               
;----------------------------------------------------------------------
        MOVB      AL,#17                ; [CPU_] |477| 
$C$DW$168	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$168, DW_AT_low_pc(0x00)
	.dwattr $C$DW$168, DW_AT_name("_IGReadMotorGpio")
	.dwattr $C$DW$168, DW_AT_TI_call

        LCR       #_IGReadMotorGpio     ; [CPU_] |477| 
        ; call occurs [#_IGReadMotorGpio] ; [] |477| 
        MOV       *-SP[2],AL            ; [CPU_] |477| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 478,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 478 | break;                                                                 
; 480 | case MOTOR2_SELECT:                                                    
; 481 | //!Get the fault status of motor 2                                     
;----------------------------------------------------------------------
        B         $C$L59,UNC            ; [CPU_] |478| 
        ; branch occurs ; [] |478| 
$C$L53:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 482,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 482 | FaultStatus = IGReadMotorGpio(M2_FAULT);                               
;----------------------------------------------------------------------
        MOVB      AL,#22                ; [CPU_] |482| 
$C$DW$169	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$169, DW_AT_low_pc(0x00)
	.dwattr $C$DW$169, DW_AT_name("_IGReadMotorGpio")
	.dwattr $C$DW$169, DW_AT_TI_call

        LCR       #_IGReadMotorGpio     ; [CPU_] |482| 
        ; call occurs [#_IGReadMotorGpio] ; [] |482| 
        MOV       *-SP[2],AL            ; [CPU_] |482| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 483,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 483 | break;                                                                 
; 485 | case MOTOR3_SELECT:                                                    
; 486 | //!Get the fault status of motor 3                                     
;----------------------------------------------------------------------
        B         $C$L59,UNC            ; [CPU_] |483| 
        ; branch occurs ; [] |483| 
$C$L54:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 487,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 487 | FaultStatus = IGReadMotorGpio(M3_FAULT);                               
;----------------------------------------------------------------------
        MOVB      AL,#19                ; [CPU_] |487| 
$C$DW$170	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$170, DW_AT_low_pc(0x00)
	.dwattr $C$DW$170, DW_AT_name("_IGReadMotorGpio")
	.dwattr $C$DW$170, DW_AT_TI_call

        LCR       #_IGReadMotorGpio     ; [CPU_] |487| 
        ; call occurs [#_IGReadMotorGpio] ; [] |487| 
        MOV       *-SP[2],AL            ; [CPU_] |487| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 488,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 488 | break;                                                                 
; 490 | case MOTOR4_SELECT:                                                    
; 491 | //!Get the fault status of motor 4                                     
;----------------------------------------------------------------------
        B         $C$L59,UNC            ; [CPU_] |488| 
        ; branch occurs ; [] |488| 
$C$L55:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 492,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 492 | FaultStatus = IGReadMotorGpio(M4_FAULT);                               
;----------------------------------------------------------------------
        MOVB      AL,#96                ; [CPU_] |492| 
$C$DW$171	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$171, DW_AT_low_pc(0x00)
	.dwattr $C$DW$171, DW_AT_name("_IGReadMotorGpio")
	.dwattr $C$DW$171, DW_AT_TI_call

        LCR       #_IGReadMotorGpio     ; [CPU_] |492| 
        ; call occurs [#_IGReadMotorGpio] ; [] |492| 
        MOV       *-SP[2],AL            ; [CPU_] |492| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 493,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 493 | break;                                                                 
; 495 | case MOTOR5_SELECT:                                                    
; 496 | //!Get the fault status of motor 5                                     
;----------------------------------------------------------------------
        B         $C$L59,UNC            ; [CPU_] |493| 
        ; branch occurs ; [] |493| 
$C$L56:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 497,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 497 | FaultStatus = IGReadMotorGpio(M5_FAULT);                               
; 499 | default:                                                               
;----------------------------------------------------------------------
        MOVB      AL,#94                ; [CPU_] |497| 
$C$DW$172	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$172, DW_AT_low_pc(0x00)
	.dwattr $C$DW$172, DW_AT_name("_IGReadMotorGpio")
	.dwattr $C$DW$172, DW_AT_TI_call

        LCR       #_IGReadMotorGpio     ; [CPU_] |497| 
        ; call occurs [#_IGReadMotorGpio] ; [] |497| 
        MOV       *-SP[2],AL            ; [CPU_] |497| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 500,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 500 | break;                                                                 
; 502 | //!Return the fault status                                             
;----------------------------------------------------------------------
        B         $C$L59,UNC            ; [CPU_] |500| 
        ; branch occurs ; [] |500| 
$C$L57:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 473,column 2,is_stmt,isa 0
        CMPB      AL,#3                 ; [CPU_] |473| 
        B         $C$L58,GT             ; [CPU_] |473| 
        ; branchcc occurs ; [] |473| 
        CMPB      AL,#3                 ; [CPU_] |473| 
        B         $C$L54,EQ             ; [CPU_] |473| 
        ; branchcc occurs ; [] |473| 
        CMPB      AL,#1                 ; [CPU_] |473| 
        B         $C$L52,EQ             ; [CPU_] |473| 
        ; branchcc occurs ; [] |473| 
        CMPB      AL,#2                 ; [CPU_] |473| 
        B         $C$L53,EQ             ; [CPU_] |473| 
        ; branchcc occurs ; [] |473| 
        B         $C$L59,UNC            ; [CPU_] |473| 
        ; branch occurs ; [] |473| 
$C$L58:    
        CMPB      AL,#4                 ; [CPU_] |473| 
        B         $C$L55,EQ             ; [CPU_] |473| 
        ; branchcc occurs ; [] |473| 
        CMPB      AL,#5                 ; [CPU_] |473| 
        B         $C$L56,EQ             ; [CPU_] |473| 
        ; branchcc occurs ; [] |473| 
$C$L59:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 503,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 503 | return FaultStatus;                                                    
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |503| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 504,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$173	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$173, DW_AT_low_pc(0x00)
	.dwattr $C$DW$173, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$164, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$164, DW_AT_TI_end_line(0x1f8)
	.dwattr $C$DW$164, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$164

	.sect	".text:_SMDMotorReset"
	.clink
	.global	_SMDMotorReset

$C$DW$174	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$174, DW_AT_name("SMDMotorReset")
	.dwattr $C$DW$174, DW_AT_low_pc(_SMDMotorReset)
	.dwattr $C$DW$174, DW_AT_high_pc(0x00)
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_SMDMotorReset")
	.dwattr $C$DW$174, DW_AT_external
	.dwattr $C$DW$174, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$174, DW_AT_TI_begin_line(0x203)
	.dwattr $C$DW$174, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$174, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/StepperMotorDriver.c",line 516,column 1,is_stmt,address _SMDMotorReset,isa 0

	.dwfde $C$DW$CIE, _SMDMotorReset
;----------------------------------------------------------------------
; 515 | void SMDMotorReset(void)                                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDMotorReset                FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SMDMotorReset:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/StepperMotorDriver.c",line 517,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 517 | IGWriteMotorGpio(M_RESET, !M_RESET_DEFAULT);                           
;----------------------------------------------------------------------
        MOVB      AL,#10                ; [CPU_] |517| 
        MOVB      AH,#0                 ; [CPU_] |517| 
$C$DW$175	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$175, DW_AT_low_pc(0x00)
	.dwattr $C$DW$175, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$175, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |517| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |517| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 518,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 518 | DELAY_US(10);                                                          
;----------------------------------------------------------------------
        MOVL      XAR4,#398             ; [CPU_U] |518| 
        MOVL      ACC,XAR4              ; [CPU_] |518| 
$C$DW$176	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$176, DW_AT_low_pc(0x00)
	.dwattr $C$DW$176, DW_AT_name("_F28x_usDelay")
	.dwattr $C$DW$176, DW_AT_TI_call

        LCR       #_F28x_usDelay        ; [CPU_] |518| 
        ; call occurs [#_F28x_usDelay] ; [] |518| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 519,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 519 | IGWriteMotorGpio(M_RESET, M_RESET_DEFAULT);                            
;----------------------------------------------------------------------
        MOVB      AL,#10                ; [CPU_] |519| 
        MOVB      AH,#1                 ; [CPU_] |519| 
$C$DW$177	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$177, DW_AT_low_pc(0x00)
	.dwattr $C$DW$177, DW_AT_name("_IGWriteMotorGpio")
	.dwattr $C$DW$177, DW_AT_TI_call

        LCR       #_IGWriteMotorGpio    ; [CPU_] |519| 
        ; call occurs [#_IGWriteMotorGpio] ; [] |519| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 520,column 1,is_stmt,isa 0
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$174, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$174, DW_AT_TI_end_line(0x208)
	.dwattr $C$DW$174, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$174

	.sect	".text:_SMDStart_Motor1"
	.clink
	.global	_SMDStart_Motor1

$C$DW$179	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$179, DW_AT_name("SMDStart_Motor1")
	.dwattr $C$DW$179, DW_AT_low_pc(_SMDStart_Motor1)
	.dwattr $C$DW$179, DW_AT_high_pc(0x00)
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_SMDStart_Motor1")
	.dwattr $C$DW$179, DW_AT_external
	.dwattr $C$DW$179, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$179, DW_AT_TI_begin_line(0x21f)
	.dwattr $C$DW$179, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$179, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/StepperMotorDriver.c",line 545,column 1,is_stmt,address _SMDStart_Motor1,isa 0

	.dwfde $C$DW$CIE, _SMDStart_Motor1
$C$DW$180	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$180, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$180, DW_AT_location[DW_OP_reg0]

$C$DW$181	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$181, DW_AT_name("usnDirection")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$181, DW_AT_location[DW_OP_reg1]

$C$DW$182	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$182, DW_AT_name("StepFrequency")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$182, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 543 | void SMDStart_Motor1(unsigned int  usnDisplacement, unsigned int usnDir
;     | ection, \                                                              
; 544 | unsigned int StepFrequency)                                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDStart_Motor1              FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_SMDStart_Motor1:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$183	.dwtag  DW_TAG_variable
	.dwattr $C$DW$183, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$183, DW_AT_location[DW_OP_breg20 -1]

$C$DW$184	.dwtag  DW_TAG_variable
	.dwattr $C$DW$184, DW_AT_name("usnDirection")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$184, DW_AT_location[DW_OP_breg20 -2]

$C$DW$185	.dwtag  DW_TAG_variable
	.dwattr $C$DW$185, DW_AT_name("StepFrequency")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$185, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 546 | //char DebugPrintBuf[50];                                              
;----------------------------------------------------------------------
        MOV       *-SP[3],AR4           ; [CPU_] |545| 
        MOV       *-SP[2],AH            ; [CPU_] |545| 
        MOV       *-SP[1],AL            ; [CPU_] |545| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 547,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 547 | if(PWMGetMotor1Status() == IDLE)                                       
; 550 | #if MOTOR_DEBUG_PRINT_ENABLE                                           
; 551 |         printf(" M1 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\ 
; 552 |                 usnDirection,StepFrequency);                           
; 553 | #endif                                                                 
; 555 | //!Enable to monitor the Waste syringe motor movement                  
;----------------------------------------------------------------------
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("_PWMGetMotor1Status")
	.dwattr $C$DW$186, DW_AT_TI_call

        LCR       #_PWMGetMotor1Status  ; [CPU_] |547| 
        ; call occurs [#_PWMGetMotor1Status] ; [] |547| 
        CMPB      AL,#3                 ; [CPU_] |547| 
        B         $C$L62,NEQ            ; [CPU_] |547| 
        ; branchcc occurs ; [] |547| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 556,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 556 | m_bWasteSyringeMonEnable = TRUE;                                       
;----------------------------------------------------------------------
        MOVW      DP,#_m_bWasteSyringeMonEnable ; [CPU_U] 
        MOVB      @_m_bWasteSyringeMonEnable,#1,UNC ; [CPU_] |556| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 558,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 558 | if(StepFrequency > 100 && StepFrequency < 300)                         
;----------------------------------------------------------------------
        MOV       AL,*-SP[3]            ; [CPU_] |558| 
        CMPB      AL,#100               ; [CPU_] |558| 
        B         $C$L60,LOS            ; [CPU_] |558| 
        ; branchcc occurs ; [] |558| 
        CMP       AL,#300               ; [CPU_] |558| 
        B         $C$L60,HIS            ; [CPU_] |558| 
        ; branchcc occurs ; [] |558| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 560,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 560 | m_bBubblingMovEnFlag = true;                                           
;----------------------------------------------------------------------
        MOVW      DP,#_m_bBubblingMovEnFlag ; [CPU_U] 
        MOVB      @_m_bBubblingMovEnFlag,#1,UNC ; [CPU_] |560| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 561,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 562 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L61,UNC            ; [CPU_] |561| 
        ; branch occurs ; [] |561| 
$C$L60:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 564,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 564 | m_bBubblingMovEnFlag = false;                                          
; 577 |     //!Set the motor step size                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_bBubblingMovEnFlag ; [CPU_U] 
        MOV       @_m_bBubblingMovEnFlag,#0 ; [CPU_] |564| 
$C$L61:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 578,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 578 | SMDSetMotorStepSize(MOTOR1_SELECT, MOTOR1_STEP_CONFIGURATION);         
; 579 | //!Enable motor                                                        
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |578| 
        MOVB      AH,#4                 ; [CPU_] |578| 
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$187, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |578| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |578| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 580,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 580 | SMDSetMotorEnable(MOTOR1_SELECT);                                      
; 581 | //!Set the motor direction                                             
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |580| 
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$188, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |580| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |580| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 582,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 582 | SMDSetMotorDirection(MOTOR1_SELECT, usnDirection);                     
; 583 | //                      BloodCellCounter_U.HP_YAxis = IDLE;            
; 585 | //!Configure the pwm to start the motor                                
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |582| 
        MOV       AH,*-SP[2]            ; [CPU_] |582| 
$C$DW$189	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$189, DW_AT_low_pc(0x00)
	.dwattr $C$DW$189, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$189, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |582| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |582| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 586,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 586 | PWMConfigMotor1Param(usnDisplacement, usnDirection, StepFrequency);    
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |586| 
        MOV       AH,*-SP[3]            ; [CPU_] |586| 
        UI16TOF32 R0H,*-SP[1]           ; [CPU_] |586| 
$C$DW$190	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$190, DW_AT_low_pc(0x00)
	.dwattr $C$DW$190, DW_AT_name("_PWMConfigMotor1Param")
	.dwattr $C$DW$190, DW_AT_TI_call

        LCR       #_PWMConfigMotor1Param ; [CPU_] |586| 
        ; call occurs [#_PWMConfigMotor1Param] ; [] |586| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 588,column 1,is_stmt,isa 0
$C$L62:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$191	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$191, DW_AT_low_pc(0x00)
	.dwattr $C$DW$191, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$179, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$179, DW_AT_TI_end_line(0x24c)
	.dwattr $C$DW$179, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$179

	.sect	".text:_SMDStart_Motor2"
	.clink
	.global	_SMDStart_Motor2

$C$DW$192	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$192, DW_AT_name("SMDStart_Motor2")
	.dwattr $C$DW$192, DW_AT_low_pc(_SMDStart_Motor2)
	.dwattr $C$DW$192, DW_AT_high_pc(0x00)
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_SMDStart_Motor2")
	.dwattr $C$DW$192, DW_AT_external
	.dwattr $C$DW$192, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$192, DW_AT_TI_begin_line(0x261)
	.dwattr $C$DW$192, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$192, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/StepperMotorDriver.c",line 611,column 1,is_stmt,address _SMDStart_Motor2,isa 0

	.dwfde $C$DW$CIE, _SMDStart_Motor2
$C$DW$193	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$193, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$193, DW_AT_location[DW_OP_reg0]

$C$DW$194	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$194, DW_AT_name("usnDirection")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg1]

$C$DW$195	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$195, DW_AT_name("StepFrequency")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 609 | void SMDStart_Motor2(unsigned int  usnDisplacement, unsigned int usnDir
;     | ection, \                                                              
; 610 | unsigned int StepFrequency)                                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDStart_Motor2              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_SMDStart_Motor2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$196	.dwtag  DW_TAG_variable
	.dwattr $C$DW$196, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$196, DW_AT_location[DW_OP_breg20 -3]

$C$DW$197	.dwtag  DW_TAG_variable
	.dwattr $C$DW$197, DW_AT_name("usnDirection")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$197, DW_AT_location[DW_OP_breg20 -4]

$C$DW$198	.dwtag  DW_TAG_variable
	.dwattr $C$DW$198, DW_AT_name("StepFrequency")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_breg20 -5]

$C$DW$199	.dwtag  DW_TAG_variable
	.dwattr $C$DW$199, DW_AT_name("maxstepFreq")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_maxstepFreq")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_breg20 -6]

;----------------------------------------------------------------------
; 612 | //! 650Hz - There is pressure build up when the displacement is small  
; 613 | //! The syringe body moves due to high pressure which will impact the r
;     | eliability of the syringe and motor                                    
;----------------------------------------------------------------------
        MOV       *-SP[5],AR4           ; [CPU_] |611| 
        MOV       *-SP[4],AH            ; [CPU_] |611| 
        MOV       *-SP[3],AL            ; [CPU_] |611| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 614,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 614 | int maxstepFreq = 550;                                                 
; 615 | //char DebugPrintBuf[50];                                              
;----------------------------------------------------------------------
        MOV       *-SP[6],#550          ; [CPU_] |614| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 616,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 616 | if(PWMGetMotor2Status() == IDLE)                                       
; 618 | #if MOTOR_DEBUG_PRINT_ENABLE                                           
; 619 |         printf(" M2 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\ 
; 620 |                 usnDirection,StepFrequency);                           
; 621 | #endif                                                                 
; 622 |         //!If the motor is moving towards home, then check the frq of t
;     | he motor                                                               
;----------------------------------------------------------------------
$C$DW$200	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$200, DW_AT_low_pc(0x00)
	.dwattr $C$DW$200, DW_AT_name("_PWMGetMotor2Status")
	.dwattr $C$DW$200, DW_AT_TI_call

        LCR       #_PWMGetMotor2Status  ; [CPU_] |616| 
        ; call occurs [#_PWMGetMotor2Status] ; [] |616| 
        CMPB      AL,#3                 ; [CPU_] |616| 
        B         $C$L67,NEQ            ; [CPU_] |616| 
        ; branchcc occurs ; [] |616| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 623,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 623 | if(usnDirection == MOTOR2_HOME_DIRECTION)                              
; 625 |     //!If the frq is more then 550Hz, set it to 550Hz.                 
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |623| 
        B         $C$L64,NEQ            ; [CPU_] |623| 
        ; branchcc occurs ; [] |623| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 626,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 626 | if(StepFrequency > maxstepFreq)                                        
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |626| 
        CMP       AL,*-SP[5]            ; [CPU_] |626| 
        B         $C$L63,HIS            ; [CPU_] |626| 
        ; branchcc occurs ; [] |626| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 628,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 628 | StepFrequency = maxstepFreq;                                           
;----------------------------------------------------------------------
        MOV       *-SP[5],AL            ; [CPU_] |628| 
$C$L63:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 631,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 631 | if(usnDisplacement <= 340)                                             
; 632 | //if(usnDisplacement < 300)                                            
;----------------------------------------------------------------------
        CMP       *-SP[3],#340          ; [CPU_] |631| 
        B         $C$L66,HI             ; [CPU_] |631| 
        ; branchcc occurs ; [] |631| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 634,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 634 | StepFrequency = 170;                                                   
; 635 | //! If the sequence is pre-diluent then make g_bLowVolumeDisp variable
;     | to true                                                                
; 636 | //! so that we will not apply sCurve for that movement.                
;----------------------------------------------------------------------
        MOVB      *-SP[5],#170,UNC      ; [CPU_] |634| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 637,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 637 | if(g_bLowVolumeDisp)                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_bLowVolumeDisp ; [CPU_U] 
        MOV       AL,@_g_bLowVolumeDisp ; [CPU_] |637| 
        B         $C$L66,EQ             ; [CPU_] |637| 
        ; branchcc occurs ; [] |637| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 639,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 639 | StepFrequency = 50;                                                    
;----------------------------------------------------------------------
        MOVB      *-SP[5],#50,UNC       ; [CPU_] |639| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 640,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 640 | sprintf(DebugPrintBuf, "\n g_bLowVolumeDisp \n");                      
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL1         ; [CPU_U] |640| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |640| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |640| 
$C$DW$201	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$201, DW_AT_low_pc(0x00)
	.dwattr $C$DW$201, DW_AT_name("_sprintf")
	.dwattr $C$DW$201, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |640| 
        ; call occurs [#_sprintf] ; [] |640| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 641,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 641 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |641| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |641| 
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$202, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |641| 
        ; call occurs [#_UartDebugPrint] ; [] |641| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 645,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 646 | else                                                                   
; 648 |     //!If the frequency is > 100 Hz, motor is not moving smoothly      
; 649 |     //! so this condition got introduced to move motor smoothly.       
;----------------------------------------------------------------------
        B         $C$L66,UNC            ; [CPU_] |645| 
        ; branch occurs ; [] |645| 
$C$L64:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 650,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 650 | if(StepFrequency > 900)                                                
;----------------------------------------------------------------------
        CMP       *-SP[5],#900          ; [CPU_] |650| 
        B         $C$L65,LOS            ; [CPU_] |650| 
        ; branchcc occurs ; [] |650| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 652,column 15,is_stmt,isa 0
;----------------------------------------------------------------------
; 652 | StepFrequency = 900;                                                   
; 654 |       //! This is for sucking the Drop let at the tip of the needle by 
; 655 |       //! using diluent syringe moving down 0.25mm with 50hz frq and wi
;     | thout sCurve                                                           
;----------------------------------------------------------------------
        MOV       *-SP[5],#900          ; [CPU_] |652| 
$C$L65:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 656,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 656 | if(g_bLowVolumeDisp)                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_bLowVolumeDisp ; [CPU_U] 
        MOV       AL,@_g_bLowVolumeDisp ; [CPU_] |656| 
        B         $C$L66,EQ             ; [CPU_] |656| 
        ; branchcc occurs ; [] |656| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 658,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 658 | StepFrequency = 50;                                                    
; 663 | //!Enable to monitor the Diluent syringe motor movement                
;----------------------------------------------------------------------
        MOVB      *-SP[5],#50,UNC       ; [CPU_] |658| 
$C$L66:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 664,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 664 | m_bDiluentSyringeMonEnable = TRUE;                                     
; 666 | //!Set the motor step size                                             
;----------------------------------------------------------------------
        MOVW      DP,#_m_bDiluentSyringeMonEnable ; [CPU_U] 
        MOVB      @_m_bDiluentSyringeMonEnable,#1,UNC ; [CPU_] |664| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 667,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 667 | SMDSetMotorStepSize(MOTOR2_SELECT, MOTOR2_STEP_CONFIGURATION);         
; 668 | //!Enable motor                                                        
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |667| 
        MOVB      AH,#4                 ; [CPU_] |667| 
$C$DW$203	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$203, DW_AT_low_pc(0x00)
	.dwattr $C$DW$203, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$203, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |667| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |667| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 669,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 669 | SMDSetMotorEnable(MOTOR2_SELECT);                                      
; 670 | //!Set the motor direction                                             
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |669| 
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$204, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |669| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |669| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 671,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 671 | SMDSetMotorDirection(MOTOR2_SELECT, usnDirection);                     
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |671| 
        MOV       AH,*-SP[4]            ; [CPU_] |671| 
$C$DW$205	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$205, DW_AT_low_pc(0x00)
	.dwattr $C$DW$205, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$205, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |671| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |671| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 672,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 672 | BloodCellCounter_U.HomeStatus[0] = IDLE;                               
; 673 | //BloodCellCounter_U.HP_YAxis = IDLE;                                  
; 674 | //!Configure the pwm to start the motor                                
;----------------------------------------------------------------------
        MOVW      DP,#_BloodCellCounter_U+16 ; [CPU_U] 
        MOVB      @_BloodCellCounter_U+16,#3,UNC ; [CPU_] |672| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 675,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 675 | PWMConfigMotor2Param(usnDisplacement, usnDirection, StepFrequency);    
;----------------------------------------------------------------------
        MOV       AL,*-SP[4]            ; [CPU_] |675| 
        MOV       AH,*-SP[5]            ; [CPU_] |675| 
        UI16TOF32 R0H,*-SP[3]           ; [CPU_] |675| 
$C$DW$206	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$206, DW_AT_low_pc(0x00)
	.dwattr $C$DW$206, DW_AT_name("_PWMConfigMotor2Param")
	.dwattr $C$DW$206, DW_AT_TI_call

        LCR       #_PWMConfigMotor2Param ; [CPU_] |675| 
        ; call occurs [#_PWMConfigMotor2Param] ; [] |675| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 677,column 1,is_stmt,isa 0
$C$L67:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$207	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$207, DW_AT_low_pc(0x00)
	.dwattr $C$DW$207, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$192, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$192, DW_AT_TI_end_line(0x2a5)
	.dwattr $C$DW$192, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$192

	.sect	".text:_SMDStart_Motor3"
	.clink
	.global	_SMDStart_Motor3

$C$DW$208	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$208, DW_AT_name("SMDStart_Motor3")
	.dwattr $C$DW$208, DW_AT_low_pc(_SMDStart_Motor3)
	.dwattr $C$DW$208, DW_AT_high_pc(0x00)
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_SMDStart_Motor3")
	.dwattr $C$DW$208, DW_AT_external
	.dwattr $C$DW$208, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$208, DW_AT_TI_begin_line(0x2bb)
	.dwattr $C$DW$208, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$208, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/StepperMotorDriver.c",line 701,column 1,is_stmt,address _SMDStart_Motor3,isa 0

	.dwfde $C$DW$CIE, _SMDStart_Motor3
$C$DW$209	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$209, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_reg0]

$C$DW$210	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$210, DW_AT_name("usnDirection")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$210, DW_AT_location[DW_OP_reg1]

$C$DW$211	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$211, DW_AT_name("StepFrequency")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$211, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 699 | void SMDStart_Motor3(unsigned int  usnDisplacement, unsigned int usnDir
;     | ection, \                                                              
; 700 | unsigned int StepFrequency)                                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDStart_Motor3              FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_SMDStart_Motor3:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$212	.dwtag  DW_TAG_variable
	.dwattr $C$DW$212, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_breg20 -1]

$C$DW$213	.dwtag  DW_TAG_variable
	.dwattr $C$DW$213, DW_AT_name("usnDirection")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_breg20 -2]

$C$DW$214	.dwtag  DW_TAG_variable
	.dwattr $C$DW$214, DW_AT_name("StepFrequency")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 702 | //char DebugPrintBuf[50];                                              
;----------------------------------------------------------------------
        MOV       *-SP[3],AR4           ; [CPU_] |701| 
        MOV       *-SP[2],AH            ; [CPU_] |701| 
        MOV       *-SP[1],AL            ; [CPU_] |701| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 703,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 703 | if(PWMGetMotor3Status() == IDLE)                                       
; 705 | #if MOTOR_DEBUG_PRINT_ENABLE                                           
; 706 |         printf(" M3 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\ 
; 707 |                 usnDirection,StepFrequency);                           
; 708 | #endif                                                                 
; 710 |         //!Enable to monitor the Sample syringe motor movement         
;----------------------------------------------------------------------
$C$DW$215	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$215, DW_AT_low_pc(0x00)
	.dwattr $C$DW$215, DW_AT_name("_PWMGetMotor3Status")
	.dwattr $C$DW$215, DW_AT_TI_call

        LCR       #_PWMGetMotor3Status  ; [CPU_] |703| 
        ; call occurs [#_PWMGetMotor3Status] ; [] |703| 
        CMPB      AL,#3                 ; [CPU_] |703| 
        B         $C$L68,NEQ            ; [CPU_] |703| 
        ; branchcc occurs ; [] |703| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 711,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 711 | m_bSampleSyringeMonEnable = TRUE;                                      
; 714 | //!Set the motor step size                                             
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSampleSyringeMonEnable ; [CPU_U] 
        MOVB      @_m_bSampleSyringeMonEnable,#1,UNC ; [CPU_] |711| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 715,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 715 | SMDSetMotorStepSize(MOTOR3_SELECT, MOTOR3_STEP_CONFIGURATION);         
; 716 | //!Enable motor                                                        
;----------------------------------------------------------------------
        MOVB      AL,#3                 ; [CPU_] |715| 
        MOVB      AH,#4                 ; [CPU_] |715| 
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$216, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |715| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |715| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 717,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 717 | SMDSetMotorEnable(MOTOR3_SELECT);                                      
; 718 | //!Set the motor direction                                             
;----------------------------------------------------------------------
        MOVB      AL,#3                 ; [CPU_] |717| 
$C$DW$217	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$217, DW_AT_low_pc(0x00)
	.dwattr $C$DW$217, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$217, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |717| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |717| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 719,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 719 | SMDSetMotorDirection(MOTOR3_SELECT, usnDirection);                     
; 720 | //!Configure the pwm to start the motor                                
;----------------------------------------------------------------------
        MOVB      AL,#3                 ; [CPU_] |719| 
        MOV       AH,*-SP[2]            ; [CPU_] |719| 
$C$DW$218	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$218, DW_AT_low_pc(0x00)
	.dwattr $C$DW$218, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$218, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |719| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |719| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 721,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 721 | PWMConfigMotor3Param(usnDisplacement, usnDirection, StepFrequency);    
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |721| 
        MOV       AH,*-SP[3]            ; [CPU_] |721| 
        UI16TOF32 R0H,*-SP[1]           ; [CPU_] |721| 
$C$DW$219	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$219, DW_AT_low_pc(0x00)
	.dwattr $C$DW$219, DW_AT_name("_PWMConfigMotor3Param")
	.dwattr $C$DW$219, DW_AT_TI_call

        LCR       #_PWMConfigMotor3Param ; [CPU_] |721| 
        ; call occurs [#_PWMConfigMotor3Param] ; [] |721| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 723,column 1,is_stmt,isa 0
$C$L68:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$220	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$220, DW_AT_low_pc(0x00)
	.dwattr $C$DW$220, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$208, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$208, DW_AT_TI_end_line(0x2d3)
	.dwattr $C$DW$208, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$208

	.sect	".text:_SMDStart_Motor4"
	.clink
	.global	_SMDStart_Motor4

$C$DW$221	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$221, DW_AT_name("SMDStart_Motor4")
	.dwattr $C$DW$221, DW_AT_low_pc(_SMDStart_Motor4)
	.dwattr $C$DW$221, DW_AT_high_pc(0x00)
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_SMDStart_Motor4")
	.dwattr $C$DW$221, DW_AT_external
	.dwattr $C$DW$221, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$221, DW_AT_TI_begin_line(0x2e8)
	.dwattr $C$DW$221, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$221, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/StepperMotorDriver.c",line 746,column 1,is_stmt,address _SMDStart_Motor4,isa 0

	.dwfde $C$DW$CIE, _SMDStart_Motor4
$C$DW$222	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$222, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg0]

$C$DW$223	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$223, DW_AT_name("usnDirection")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$223, DW_AT_location[DW_OP_reg1]

$C$DW$224	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$224, DW_AT_name("StepFrequency")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 744 | void SMDStart_Motor4(unsigned int  usnDisplacement, unsigned int usnDir
;     | ection, \                                                              
; 745 | unsigned int StepFrequency)                                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDStart_Motor4              FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_SMDStart_Motor4:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$225	.dwtag  DW_TAG_variable
	.dwattr $C$DW$225, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$225, DW_AT_location[DW_OP_breg20 -1]

$C$DW$226	.dwtag  DW_TAG_variable
	.dwattr $C$DW$226, DW_AT_name("usnDirection")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$226, DW_AT_location[DW_OP_breg20 -2]

$C$DW$227	.dwtag  DW_TAG_variable
	.dwattr $C$DW$227, DW_AT_name("StepFrequency")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$227, DW_AT_location[DW_OP_breg20 -3]

        MOV       *-SP[3],AR4           ; [CPU_] |746| 
        MOV       *-SP[2],AH            ; [CPU_] |746| 
        MOV       *-SP[1],AL            ; [CPU_] |746| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 747,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 747 | if(PWMGetMotor4Status() == IDLE)                                       
; 749 | #if MOTOR_DEBUG_PRINT_ENABLE                                           
; 750 |         printf(" M4 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\ 
; 751 |                 usnDirection,StepFrequency);                           
; 752 | #endif                                                                 
; 754 | //!Enable to monitor the X-Axis motor movement                         
;----------------------------------------------------------------------
$C$DW$228	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$228, DW_AT_low_pc(0x00)
	.dwattr $C$DW$228, DW_AT_name("_PWMGetMotor4Status")
	.dwattr $C$DW$228, DW_AT_TI_call

        LCR       #_PWMGetMotor4Status  ; [CPU_] |747| 
        ; call occurs [#_PWMGetMotor4Status] ; [] |747| 
        CMPB      AL,#3                 ; [CPU_] |747| 
        B         $C$L72,NEQ            ; [CPU_] |747| 
        ; branchcc occurs ; [] |747| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 755,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 755 | m_bXAxisMonEnable = TRUE;                                              
; 757 |         //!Set the motor frequency based on the sequences              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bXAxisMonEnable ; [CPU_U] 
        MOVB      @_m_bXAxisMonEnable,#1,UNC ; [CPU_] |755| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 758,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 758 | switch(g_usnCurrentMode)                                               
; 760 |     case WHOLE_BLOOD_MODE:                                             
; 761 |     //!Do not check for START_PRE_DILUENT_COUNTING_CMD                 
; 762 |     //!Motor movement is mainly for the main mode (pre-dilute mode) and
; 763 |     //!not the intermediate mode of starting the pre-dilute sequence af
;     | ter                                                                    
; 764 |     //!diluting the blood externally in pre-dilute mode                
; 765 |     //case START_PRE_DILUENT_COUNTING_CMD:                             
; 766 |     case PRE_DILUENT_MODE:                                             
; 767 |     case BODY_FLUID_MODE:                                              
; 768 |     case START_UP_SEQUENCE_MODE:                                       
; 769 |     case AUTO_CALIBRATION_WB:                                          
; 770 |     case COMMERCIAL_CALIBRATION_WBC:                                   
; 771 |     case COMMERCIAL_CALIBRATION_RBC:                                   
; 772 |     case COMMERCIAL_CALIBRATION_PLT:                                   
; 773 |     case QUALITY_CONTROL_WB:                                           
; 774 |     case QUALITY_CONTROL_BODY_FLUID:                                   
; 775 |     case PARTIAL_BLOOD_COUNT_WBC:                                      
; 776 |     case PARTIAL_BLOOD_COUNT_RBC_PLT:                                  
; 777 |     case FLOW_CALIBRATION:                                             
; 778 |         break;                                                         
; 780 |     case MBD_WAKEUP_START:                                             
; 781 |     case PRIME_WITH_RINSE_SERVICE_HANDLING:                            
; 782 |     case PRIME_WITH_LYSE_SERVICE_HANDLING:                             
; 783 |     case PRIME_WITH_DILUENT_SERVICE_HANDLING:                          
; 784 |     case PRIME_ALL_SERVICE_HANDLING:                                   
; 785 |     case BACK_FLUSH_SERVICE_HANDLING:                                  
; 786 |     case ZAP_APERTURE_SERVICE_HANDLING:                                
; 787 |     case DRAIN_BATH_SERVICE_HANDLING:                                  
; 788 |     case DRAIN_ALL_SERVICE_HANDLING:                                   
; 789 |     case CLEAN_BATH_SERVICE_HANDLING:                                  
; 790 |     case HEAD_RINSING_SERVICE_HANDLING:                                
; 791 |     case PROBE_CLEANING_SERVICE_HANDLING:                              
; 792 |     case MBD_TO_SHIP_SEQUENCE_START:                                   
; 793 |     case SHUTDOWN_SEQUENCE:                                            
; 794 |     case COUNTING_TIME_SEQUENCE:                                       
; 795 |     //case FLOW_CALIBRATION:                                           
; 796 |     case PRIME_WITH_EZ_CLEANSER_CMD:                                   
; 797 |     case MBD_USER_ABORT_PROCESS:                                       
; 798 |     case RINSE_PRIME_FLOW_CALIBRATION:                                 
; 799 |     case MBD_USER_ABORT_Y_AXIS_PROCESS:                                
; 800 |     case MBD_VOLUMETRIC_BOARD_CHECK:                                   
; 801 |     case MBD_SLEEP_PROCESS:                                            
;----------------------------------------------------------------------
        B         $C$L70,UNC            ; [CPU_] |758| 
        ; branch occurs ; [] |758| 
$C$L69:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 802,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 802 | if(StepFrequency > 700)                                                
;----------------------------------------------------------------------
        CMP       *-SP[3],#700          ; [CPU_] |802| 
        B         $C$L71,LOS            ; [CPU_] |802| 
        ; branchcc occurs ; [] |802| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 804,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 804 | StepFrequency = 700;                                                   
;----------------------------------------------------------------------
        MOV       *-SP[3],#700          ; [CPU_] |804| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 806,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 806 | break;                                                                 
; 808 | default:                                                               
; 809 | break;                                                                 
; 811 | //!Set the motor step size                                             
;----------------------------------------------------------------------
        B         $C$L71,UNC            ; [CPU_] |806| 
        ; branch occurs ; [] |806| 
$C$L70:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 758,column 6,is_stmt,isa 0
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       AH,@_g_usnCurrentMode ; [CPU_] |758| 
        MOV       AL,AH                 ; [CPU_] |758| 
        SUB       AL,#20481             ; [CPU_] |758| 
        CMPB      AL,#46                ; [CPU_] |758| 
        B         $C$L71,HI             ; [CPU_] |758| 
        ; branchcc occurs ; [] |758| 
        MOV       AL,AH                 ; [CPU_] |758| 
        SUB       AL,#20481             ; [CPU_] |758| 
        MOV       ACC,AL << #1          ; [CPU_] |758| 
        MOVZ      AR6,AL                ; [CPU_] |758| 
        MOVL      XAR7,#$C$SW1          ; [CPU_U] |758| 
        MOVL      ACC,XAR7              ; [CPU_] |758| 
        ADDU      ACC,AR6               ; [CPU_] |758| 
        MOVL      XAR7,ACC              ; [CPU_] |758| 
        MOVL      XAR7,*XAR7            ; [CPU_] |758| 
        LB        *XAR7                 ; [CPU_] |758| 
        ; branch occurs ; [] |758| 
	.sect	".switch:_SMDStart_Motor4"
	.clink
$C$SW1:	.long	$C$L69	; 20481
	.long	$C$L69	; 20482
	.long	$C$L69	; 20483
	.long	$C$L69	; 20484
	.long	$C$L69	; 20485
	.long	$C$L69	; 20486
	.long	$C$L71	; 20487
	.long	$C$L69	; 20488
	.long	$C$L69	; 20489
	.long	$C$L69	; 20490
	.long	$C$L69	; 20491
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L71	; 20495
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L71	; 20498
	.long	$C$L69	; 20499
	.long	$C$L71	; 20500
	.long	$C$L69	; 20501
	.long	$C$L69	; 20502
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L71	; 0
	.long	$C$L69	; 20511
	.long	$C$L71	; 20512
	.long	$C$L71	; 20513
	.long	$C$L71	; 20514
	.long	$C$L71	; 20515
	.long	$C$L71	; 20516
	.long	$C$L71	; 20517
	.long	$C$L71	; 20518
	.long	$C$L71	; 20519
	.long	$C$L69	; 20520
	.long	$C$L69	; 20521
	.long	$C$L69	; 20522
	.long	$C$L71	; 20523
	.long	$C$L69	; 20524
	.long	$C$L69	; 20525
	.long	$C$L69	; 20526
	.long	$C$L69	; 20527
	.sect	".text:_SMDStart_Motor4"
$C$L71:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 812,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 812 | SMDSetMotorStepSize(MOTOR4_SELECT, MOTOR4_STEP_CONFIGURATION);         
; 813 | //!Enable motor                                                        
;----------------------------------------------------------------------
        MOVB      AL,#4                 ; [CPU_] |812| 
        MOVB      AH,#16                ; [CPU_] |812| 
$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$229, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |812| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |812| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 814,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 814 | SMDSetMotorEnable(MOTOR4_SELECT);                                      
; 815 | //!Set the motor direction                                             
;----------------------------------------------------------------------
        MOVB      AL,#4                 ; [CPU_] |814| 
$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$230, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |814| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |814| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 816,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 816 | SMDSetMotorDirection(MOTOR4_SELECT, usnDirection);                     
; 817 | //!Configure the pwm to start the motor                                
;----------------------------------------------------------------------
        MOVB      AL,#4                 ; [CPU_] |816| 
        MOV       AH,*-SP[2]            ; [CPU_] |816| 
$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$231, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |816| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |816| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 818,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 818 | PWMConfigMotor4Param(usnDisplacement, usnDirection, StepFrequency);    
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |818| 
        MOV       AH,*-SP[3]            ; [CPU_] |818| 
        UI16TOF32 R0H,*-SP[1]           ; [CPU_] |818| 
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_name("_PWMConfigMotor4Param")
	.dwattr $C$DW$232, DW_AT_TI_call

        LCR       #_PWMConfigMotor4Param ; [CPU_] |818| 
        ; call occurs [#_PWMConfigMotor4Param] ; [] |818| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 820,column 1,is_stmt,isa 0
$C$L72:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$221, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$221, DW_AT_TI_end_line(0x334)
	.dwattr $C$DW$221, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$221

	.sect	".text:_SMDStart_Motor5"
	.clink
	.global	_SMDStart_Motor5

$C$DW$234	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$234, DW_AT_name("SMDStart_Motor5")
	.dwattr $C$DW$234, DW_AT_low_pc(_SMDStart_Motor5)
	.dwattr $C$DW$234, DW_AT_high_pc(0x00)
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_SMDStart_Motor5")
	.dwattr $C$DW$234, DW_AT_external
	.dwattr $C$DW$234, DW_AT_TI_begin_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$234, DW_AT_TI_begin_line(0x349)
	.dwattr $C$DW$234, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$234, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/StepperMotorDriver.c",line 843,column 1,is_stmt,address _SMDStart_Motor5,isa 0

	.dwfde $C$DW$CIE, _SMDStart_Motor5
$C$DW$235	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$235, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$235, DW_AT_location[DW_OP_reg0]

$C$DW$236	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$236, DW_AT_name("usnDirection")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$236, DW_AT_location[DW_OP_reg1]

$C$DW$237	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$237, DW_AT_name("StepFrequency")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 841 | void SMDStart_Motor5(unsigned int  usnDisplacement, unsigned int usnDir
;     | ection, \                                                              
; 842 | unsigned int StepFrequency)                                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _SMDStart_Motor5              FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_SMDStart_Motor5:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$238	.dwtag  DW_TAG_variable
	.dwattr $C$DW$238, DW_AT_name("usnDisplacement")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_usnDisplacement")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_breg20 -1]

$C$DW$239	.dwtag  DW_TAG_variable
	.dwattr $C$DW$239, DW_AT_name("usnDirection")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_usnDirection")
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$239, DW_AT_location[DW_OP_breg20 -2]

$C$DW$240	.dwtag  DW_TAG_variable
	.dwattr $C$DW$240, DW_AT_name("StepFrequency")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_StepFrequency")
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$240, DW_AT_location[DW_OP_breg20 -3]

        MOV       *-SP[3],AR4           ; [CPU_] |843| 
        MOV       *-SP[2],AH            ; [CPU_] |843| 
        MOV       *-SP[1],AL            ; [CPU_] |843| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 845,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 845 | if(PWMGetMotor5Status() == IDLE)                                       
; 847 | #if MOTOR_DEBUG_PRINT_ENABLE                                           
; 848 |         printf(" M5 :Disp:%d\t Dir:%d\t Freq:%d  \n",usnDisplacement,\ 
; 849 |                 usnDirection,StepFrequency);                           
; 850 | #endif                                                                 
; 852 | //!Enable to monitor the Y-Axis motor movement                         
;----------------------------------------------------------------------
$C$DW$241	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$241, DW_AT_low_pc(0x00)
	.dwattr $C$DW$241, DW_AT_name("_PWMGetMotor5Status")
	.dwattr $C$DW$241, DW_AT_TI_call

        LCR       #_PWMGetMotor5Status  ; [CPU_] |845| 
        ; call occurs [#_PWMGetMotor5Status] ; [] |845| 
        CMPB      AL,#3                 ; [CPU_] |845| 
        B         $C$L76,NEQ            ; [CPU_] |845| 
        ; branchcc occurs ; [] |845| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 853,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 853 | m_bYAxisMonEnable = TRUE;                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bYAxisMonEnable ; [CPU_U] 
        MOVB      @_m_bYAxisMonEnable,#1,UNC ; [CPU_] |853| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 855,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 855 | switch(g_usnCurrentMode)                                               
; 857 |     case WHOLE_BLOOD_MODE:                                             
; 858 |     //!Do not check for START_PRE_DILUENT_COUNTING_CMD                 
; 859 |     //!Motor movement is mainly for the main mode (pre-dilute mode) and
; 860 |     //!not the intermediate mode of starting the pre-dilute sequence af
;     | ter                                                                    
; 861 |     //!diluting the blood externally in pre-dilute mode                
; 862 |     //case START_PRE_DILUENT_COUNTING_CMD:                             
; 863 |     case PRE_DILUENT_MODE:                                             
; 864 |     case BODY_FLUID_MODE:                                              
; 865 |     case START_UP_SEQUENCE_MODE:                                       
; 866 |     case AUTO_CALIBRATION_WB:                                          
; 867 |     case COMMERCIAL_CALIBRATION_WBC:                                   
; 868 |     case COMMERCIAL_CALIBRATION_RBC:                                   
; 869 |     case COMMERCIAL_CALIBRATION_PLT:                                   
; 870 |     case QUALITY_CONTROL_WB:                                           
; 871 |     case QUALITY_CONTROL_BODY_FLUID:                                   
; 872 |     case PARTIAL_BLOOD_COUNT_WBC:                                      
; 873 |     case PARTIAL_BLOOD_COUNT_RBC_PLT:                                  
; 874 |     case FLOW_CALIBRATION:                                             
; 875 |         break;                                                         
; 877 |     case MBD_WAKEUP_START:                                             
; 878 |     case PRIME_WITH_RINSE_SERVICE_HANDLING:                            
; 879 |     case PRIME_WITH_LYSE_SERVICE_HANDLING:                             
; 880 |     case PRIME_WITH_DILUENT_SERVICE_HANDLING:                          
; 881 |     case PRIME_ALL_SERVICE_HANDLING:                                   
; 882 |     case BACK_FLUSH_SERVICE_HANDLING:                                  
; 883 |     case ZAP_APERTURE_SERVICE_HANDLING:                                
; 884 |     case DRAIN_BATH_SERVICE_HANDLING:                                  
; 885 |     case DRAIN_ALL_SERVICE_HANDLING:                                   
; 886 |     case CLEAN_BATH_SERVICE_HANDLING:                                  
; 887 |     case HEAD_RINSING_SERVICE_HANDLING:                                
; 888 |     case PROBE_CLEANING_SERVICE_HANDLING:                              
; 889 |     case MBD_TO_SHIP_SEQUENCE_START:                                   
; 890 |     case SHUTDOWN_SEQUENCE:                                            
; 891 |     case COUNTING_TIME_SEQUENCE:                                       
; 892 |     //case FLOW_CALIBRATION:                                           
; 893 |     case PRIME_WITH_EZ_CLEANSER_CMD:                                   
; 894 |     case MBD_USER_ABORT_PROCESS:                                       
; 895 |     case RINSE_PRIME_FLOW_CALIBRATION:                                 
; 896 |     case MBD_VOLUMETRIC_BOARD_CHECK:                                   
; 897 |     case MBD_SLEEP_PROCESS:                                            
; 898 |     case MBD_USER_ABORT_Y_AXIS_PROCESS:                                
;----------------------------------------------------------------------
        B         $C$L74,UNC            ; [CPU_] |855| 
        ; branch occurs ; [] |855| 
$C$L73:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 899,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 899 | if(StepFrequency > 500)                                                
;----------------------------------------------------------------------
        CMP       *-SP[3],#500          ; [CPU_] |899| 
        B         $C$L75,LOS            ; [CPU_] |899| 
        ; branchcc occurs ; [] |899| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 901,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 901 | StepFrequency = 500;                                                   
;----------------------------------------------------------------------
        MOV       *-SP[3],#500          ; [CPU_] |901| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 903,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 903 | break;                                                                 
; 905 | default:                                                               
; 906 | break;                                                                 
; 908 | //!Set the motor step size                                             
;----------------------------------------------------------------------
        B         $C$L75,UNC            ; [CPU_] |903| 
        ; branch occurs ; [] |903| 
$C$L74:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 855,column 9,is_stmt,isa 0
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       AH,@_g_usnCurrentMode ; [CPU_] |855| 
        MOV       AL,AH                 ; [CPU_] |855| 
        SUB       AL,#20481             ; [CPU_] |855| 
        CMPB      AL,#46                ; [CPU_] |855| 
        B         $C$L75,HI             ; [CPU_] |855| 
        ; branchcc occurs ; [] |855| 
        MOV       AL,AH                 ; [CPU_] |855| 
        SUB       AL,#20481             ; [CPU_] |855| 
        MOV       ACC,AL << #1          ; [CPU_] |855| 
        MOVZ      AR6,AL                ; [CPU_] |855| 
        MOVL      XAR7,#$C$SW3          ; [CPU_U] |855| 
        MOVL      ACC,XAR7              ; [CPU_] |855| 
        ADDU      ACC,AR6               ; [CPU_] |855| 
        MOVL      XAR7,ACC              ; [CPU_] |855| 
        MOVL      XAR7,*XAR7            ; [CPU_] |855| 
        LB        *XAR7                 ; [CPU_] |855| 
        ; branch occurs ; [] |855| 
	.sect	".switch:_SMDStart_Motor5"
	.clink
$C$SW3:	.long	$C$L73	; 20481
	.long	$C$L73	; 20482
	.long	$C$L73	; 20483
	.long	$C$L73	; 20484
	.long	$C$L73	; 20485
	.long	$C$L73	; 20486
	.long	$C$L75	; 20487
	.long	$C$L73	; 20488
	.long	$C$L73	; 20489
	.long	$C$L73	; 20490
	.long	$C$L73	; 20491
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L75	; 20495
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L75	; 20498
	.long	$C$L73	; 20499
	.long	$C$L75	; 20500
	.long	$C$L73	; 20501
	.long	$C$L73	; 20502
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L75	; 0
	.long	$C$L73	; 20511
	.long	$C$L75	; 20512
	.long	$C$L75	; 20513
	.long	$C$L75	; 20514
	.long	$C$L75	; 20515
	.long	$C$L75	; 20516
	.long	$C$L75	; 20517
	.long	$C$L75	; 20518
	.long	$C$L75	; 20519
	.long	$C$L73	; 20520
	.long	$C$L73	; 20521
	.long	$C$L73	; 20522
	.long	$C$L75	; 20523
	.long	$C$L73	; 20524
	.long	$C$L73	; 20525
	.long	$C$L73	; 20526
	.long	$C$L73	; 20527
	.sect	".text:_SMDStart_Motor5"
$C$L75:    
	.dwpsn	file "../source/StepperMotorDriver.c",line 909,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 909 | SMDSetMotorStepSize(MOTOR5_SELECT, MOTOR5_STEP_CONFIGURATION);         
; 910 | //!Enable motor                                                        
;----------------------------------------------------------------------
        MOVB      AL,#5                 ; [CPU_] |909| 
        MOVB      AH,#8                 ; [CPU_] |909| 
$C$DW$242	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$242, DW_AT_low_pc(0x00)
	.dwattr $C$DW$242, DW_AT_name("_SMDSetMotorStepSize")
	.dwattr $C$DW$242, DW_AT_TI_call

        LCR       #_SMDSetMotorStepSize ; [CPU_] |909| 
        ; call occurs [#_SMDSetMotorStepSize] ; [] |909| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 911,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 911 | SMDSetMotorEnable(MOTOR5_SELECT);                                      
; 912 | //!Set the motor direction                                             
;----------------------------------------------------------------------
        MOVB      AL,#5                 ; [CPU_] |911| 
$C$DW$243	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$243, DW_AT_low_pc(0x00)
	.dwattr $C$DW$243, DW_AT_name("_SMDSetMotorEnable")
	.dwattr $C$DW$243, DW_AT_TI_call

        LCR       #_SMDSetMotorEnable   ; [CPU_] |911| 
        ; call occurs [#_SMDSetMotorEnable] ; [] |911| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 913,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 913 | SMDSetMotorDirection(MOTOR5_SELECT, usnDirection);                     
; 914 | //!Configure the pwm to start the motor                                
;----------------------------------------------------------------------
        MOVB      AL,#5                 ; [CPU_] |913| 
        MOV       AH,*-SP[2]            ; [CPU_] |913| 
$C$DW$244	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$244, DW_AT_low_pc(0x00)
	.dwattr $C$DW$244, DW_AT_name("_SMDSetMotorDirection")
	.dwattr $C$DW$244, DW_AT_TI_call

        LCR       #_SMDSetMotorDirection ; [CPU_] |913| 
        ; call occurs [#_SMDSetMotorDirection] ; [] |913| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 915,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 915 | PWMConfigMotor5Param(usnDisplacement, usnDirection, StepFrequency);    
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |915| 
        MOV       AH,*-SP[3]            ; [CPU_] |915| 
        UI16TOF32 R0H,*-SP[1]           ; [CPU_] |915| 
$C$DW$245	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$245, DW_AT_low_pc(0x00)
	.dwattr $C$DW$245, DW_AT_name("_PWMConfigMotor5Param")
	.dwattr $C$DW$245, DW_AT_TI_call

        LCR       #_PWMConfigMotor5Param ; [CPU_] |915| 
        ; call occurs [#_PWMConfigMotor5Param] ; [] |915| 
	.dwpsn	file "../source/StepperMotorDriver.c",line 918,column 1,is_stmt,isa 0
$C$L76:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$246	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$246, DW_AT_low_pc(0x00)
	.dwattr $C$DW$246, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$234, DW_AT_TI_end_file("../source/StepperMotorDriver.c")
	.dwattr $C$DW$234, DW_AT_TI_end_line(0x396)
	.dwattr $C$DW$234, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$234

;***************************************************************
;* FAR STRINGS                                                 *
;***************************************************************
	.sect	".econst:.string"
	.align	2
$C$FSL1:	.string	10," g_bLowVolumeDisp ",10,0
;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_IGGpioConfig_MotorReset
	.global	_IGGpioConfig_Motor5
	.global	_UartDebugPrint
	.global	_PWMConfigMotor4Param
	.global	_PWMConfigMotor5Param
	.global	_PWMConfigMotor3Param
	.global	_PWMConfigMotor1Param
	.global	_PWMConfigMotor2Param
	.global	_F28x_usDelay
	.global	_IGGpioConfig_Motor2
	.global	_IGGpioConfig_Motor1
	.global	_IGGpioConfig_Motor4
	.global	_IGGpioConfig_Motor3
	.global	_m_bXAxisMonEnable
	.global	_m_bDiluentSyringeMonEnable
	.global	_g_usnCurrentMode
	.global	_m_bYAxisMonEnable
	.global	_m_bBubblingMovEnFlag
	.global	_m_bSampleSyringeMonEnable
	.global	_m_bWasteSyringeMonEnable
	.global	_PWMGetMotor3Status
	.global	_IGReadMotorGpio
	.global	_PWMGetMotor2Status
	.global	_PWMGetMotor5Status
	.global	_sprintf
	.global	_IGWriteMotorGpio
	.global	_PWMGetMotor4Status
	.global	_PWMGetMotor1Status
	.global	_BloodCellCounter_U
	.global	_DebugPrintBuf

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x17)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$247, DW_AT_name("LogicEnable")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_LogicEnable")
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$248, DW_AT_name("StopMotorYaxis")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_StopMotorYaxis")
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$249, DW_AT_name("StopMotorXaxis")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_StopMotorXaxis")
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$250, DW_AT_name("StopMotorLyseSyringe")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_StopMotorLyseSyringe")
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$251, DW_AT_name("StopMotorWasteSyringe")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_StopMotorWasteSyringe")
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$252, DW_AT_name("StopMotorDiluentSyringe")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_StopMotorDiluentSyringe")
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$253, DW_AT_name("CountStop")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_CountStop")
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$254, DW_AT_name("ValveOpenStart")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_ValveOpenStart")
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$255, DW_AT_name("StartAspiration")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_StartAspiration")
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$256, DW_AT_name("ZapComplete")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_ZapComplete")
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$257, DW_AT_name("PopUpCmd")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_PopUpCmd")
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$258, DW_AT_name("MajorCmd")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_MajorCmd")
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$259, DW_AT_name("MinorCmd")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_MinorCmd")
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$260, DW_AT_name("InBubblingFrequency")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_InBubblingFrequency")
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$261, DW_AT_name("HomeStatus")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_HomeStatus")
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$262	.dwtag  DW_TAG_member
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$262, DW_AT_name("Pressure")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_Pressure")
	.dwattr $C$DW$262, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$262, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$263, DW_AT_name("Delay_BeforeCount")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_Delay_BeforeCount")
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$26

$C$DW$T$27	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$27, DW_AT_name("ExternalInputs_BloodCellCount_T")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)

$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$T$19	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$19, DW_AT_name("boolean_T")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)


$C$DW$T$23	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x05)
$C$DW$264	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$264, DW_AT_upper_bound(0x04)

	.dwendtag $C$DW$T$23

$C$DW$T$21	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$21, DW_AT_name("uint8_T")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("int16_T")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$20	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$20, DW_AT_name("uint16_T")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)


$C$DW$T$22	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$265	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$265, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$22

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$35	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x20)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$30	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$30, DW_AT_name("Uint16")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)


$C$DW$T$57	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x03)
$C$DW$266	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$266, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$57

$C$DW$T$59	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$59, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)

$C$DW$T$24	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$24, DW_AT_name("uint32_T")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

$C$DW$T$45	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$45, DW_AT_address_class(0x20)

$C$DW$267	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$5)

$C$DW$T$46	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$267)

$C$DW$T$47	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x20)


$C$DW$T$60	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x32)
$C$DW$268	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$268, DW_AT_upper_bound(0x31)

	.dwendtag $C$DW$T$60

$C$DW$T$63	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$63, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$63, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x01)

$C$DW$T$64	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$64, DW_AT_name("bool_t")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$269	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$269, DW_AT_name("AL")
	.dwattr $C$DW$269, DW_AT_location[DW_OP_reg0]

$C$DW$270	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$270, DW_AT_name("AH")
	.dwattr $C$DW$270, DW_AT_location[DW_OP_reg1]

$C$DW$271	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$271, DW_AT_name("PL")
	.dwattr $C$DW$271, DW_AT_location[DW_OP_reg2]

$C$DW$272	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$272, DW_AT_name("PH")
	.dwattr $C$DW$272, DW_AT_location[DW_OP_reg3]

$C$DW$273	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$273, DW_AT_name("SP")
	.dwattr $C$DW$273, DW_AT_location[DW_OP_reg20]

$C$DW$274	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$274, DW_AT_name("XT")
	.dwattr $C$DW$274, DW_AT_location[DW_OP_reg21]

$C$DW$275	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$275, DW_AT_name("T")
	.dwattr $C$DW$275, DW_AT_location[DW_OP_reg22]

$C$DW$276	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$276, DW_AT_name("ST0")
	.dwattr $C$DW$276, DW_AT_location[DW_OP_reg23]

$C$DW$277	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$277, DW_AT_name("ST1")
	.dwattr $C$DW$277, DW_AT_location[DW_OP_reg24]

$C$DW$278	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$278, DW_AT_name("PC")
	.dwattr $C$DW$278, DW_AT_location[DW_OP_reg25]

$C$DW$279	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$279, DW_AT_name("RPC")
	.dwattr $C$DW$279, DW_AT_location[DW_OP_reg26]

$C$DW$280	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$280, DW_AT_name("FP")
	.dwattr $C$DW$280, DW_AT_location[DW_OP_reg28]

$C$DW$281	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$281, DW_AT_name("DP")
	.dwattr $C$DW$281, DW_AT_location[DW_OP_reg29]

$C$DW$282	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$282, DW_AT_name("SXM")
	.dwattr $C$DW$282, DW_AT_location[DW_OP_reg30]

$C$DW$283	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$283, DW_AT_name("PM")
	.dwattr $C$DW$283, DW_AT_location[DW_OP_reg31]

$C$DW$284	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$284, DW_AT_name("OVM")
	.dwattr $C$DW$284, DW_AT_location[DW_OP_regx 0x20]

$C$DW$285	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$285, DW_AT_name("PAGE0")
	.dwattr $C$DW$285, DW_AT_location[DW_OP_regx 0x21]

$C$DW$286	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$286, DW_AT_name("AMODE")
	.dwattr $C$DW$286, DW_AT_location[DW_OP_regx 0x22]

$C$DW$287	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$287, DW_AT_name("INTM")
	.dwattr $C$DW$287, DW_AT_location[DW_OP_regx 0x23]

$C$DW$288	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$288, DW_AT_name("IFR")
	.dwattr $C$DW$288, DW_AT_location[DW_OP_regx 0x24]

$C$DW$289	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$289, DW_AT_name("IER")
	.dwattr $C$DW$289, DW_AT_location[DW_OP_regx 0x25]

$C$DW$290	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$290, DW_AT_name("V")
	.dwattr $C$DW$290, DW_AT_location[DW_OP_regx 0x26]

$C$DW$291	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$291, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$291, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$292	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$292, DW_AT_name("VOL")
	.dwattr $C$DW$292, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$293	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$293, DW_AT_name("AR0")
	.dwattr $C$DW$293, DW_AT_location[DW_OP_reg4]

$C$DW$294	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$294, DW_AT_name("XAR0")
	.dwattr $C$DW$294, DW_AT_location[DW_OP_reg5]

$C$DW$295	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$295, DW_AT_name("AR1")
	.dwattr $C$DW$295, DW_AT_location[DW_OP_reg6]

$C$DW$296	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$296, DW_AT_name("XAR1")
	.dwattr $C$DW$296, DW_AT_location[DW_OP_reg7]

$C$DW$297	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$297, DW_AT_name("AR2")
	.dwattr $C$DW$297, DW_AT_location[DW_OP_reg8]

$C$DW$298	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$298, DW_AT_name("XAR2")
	.dwattr $C$DW$298, DW_AT_location[DW_OP_reg9]

$C$DW$299	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$299, DW_AT_name("AR3")
	.dwattr $C$DW$299, DW_AT_location[DW_OP_reg10]

$C$DW$300	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$300, DW_AT_name("XAR3")
	.dwattr $C$DW$300, DW_AT_location[DW_OP_reg11]

$C$DW$301	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$301, DW_AT_name("AR4")
	.dwattr $C$DW$301, DW_AT_location[DW_OP_reg12]

$C$DW$302	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$302, DW_AT_name("XAR4")
	.dwattr $C$DW$302, DW_AT_location[DW_OP_reg13]

$C$DW$303	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$303, DW_AT_name("AR5")
	.dwattr $C$DW$303, DW_AT_location[DW_OP_reg14]

$C$DW$304	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$304, DW_AT_name("XAR5")
	.dwattr $C$DW$304, DW_AT_location[DW_OP_reg15]

$C$DW$305	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$305, DW_AT_name("AR6")
	.dwattr $C$DW$305, DW_AT_location[DW_OP_reg16]

$C$DW$306	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$306, DW_AT_name("XAR6")
	.dwattr $C$DW$306, DW_AT_location[DW_OP_reg17]

$C$DW$307	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$307, DW_AT_name("AR7")
	.dwattr $C$DW$307, DW_AT_location[DW_OP_reg18]

$C$DW$308	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$308, DW_AT_name("XAR7")
	.dwattr $C$DW$308, DW_AT_location[DW_OP_reg19]

$C$DW$309	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$309, DW_AT_name("R0HL")
	.dwattr $C$DW$309, DW_AT_location[DW_OP_regx 0x29]

$C$DW$310	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$310, DW_AT_name("R0H")
	.dwattr $C$DW$310, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$311	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$311, DW_AT_name("R1HL")
	.dwattr $C$DW$311, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$312	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$312, DW_AT_name("R1H")
	.dwattr $C$DW$312, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$313	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$313, DW_AT_name("R2HL")
	.dwattr $C$DW$313, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$314	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$314, DW_AT_name("R2H")
	.dwattr $C$DW$314, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$315	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$315, DW_AT_name("R3HL")
	.dwattr $C$DW$315, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$316	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$316, DW_AT_name("R3H")
	.dwattr $C$DW$316, DW_AT_location[DW_OP_regx 0x30]

$C$DW$317	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$317, DW_AT_name("R4HL")
	.dwattr $C$DW$317, DW_AT_location[DW_OP_regx 0x31]

$C$DW$318	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$318, DW_AT_name("R4H")
	.dwattr $C$DW$318, DW_AT_location[DW_OP_regx 0x32]

$C$DW$319	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$319, DW_AT_name("R5HL")
	.dwattr $C$DW$319, DW_AT_location[DW_OP_regx 0x33]

$C$DW$320	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$320, DW_AT_name("R5H")
	.dwattr $C$DW$320, DW_AT_location[DW_OP_regx 0x34]

$C$DW$321	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$321, DW_AT_name("R6HL")
	.dwattr $C$DW$321, DW_AT_location[DW_OP_regx 0x35]

$C$DW$322	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$322, DW_AT_name("R6H")
	.dwattr $C$DW$322, DW_AT_location[DW_OP_regx 0x36]

$C$DW$323	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$323, DW_AT_name("R7HL")
	.dwattr $C$DW$323, DW_AT_location[DW_OP_regx 0x37]

$C$DW$324	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$324, DW_AT_name("R7H")
	.dwattr $C$DW$324, DW_AT_location[DW_OP_regx 0x38]

$C$DW$325	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$325, DW_AT_name("RBL")
	.dwattr $C$DW$325, DW_AT_location[DW_OP_regx 0x49]

$C$DW$326	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$326, DW_AT_name("RB")
	.dwattr $C$DW$326, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$327	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$327, DW_AT_name("STFL")
	.dwattr $C$DW$327, DW_AT_location[DW_OP_regx 0x27]

$C$DW$328	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$328, DW_AT_name("STF")
	.dwattr $C$DW$328, DW_AT_location[DW_OP_regx 0x28]

$C$DW$329	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$329, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$329, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


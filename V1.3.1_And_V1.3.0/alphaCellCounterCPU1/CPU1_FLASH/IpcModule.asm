;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Fri Jan 29 15:14:28 2021                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../source/IpcModule.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cell Counter\V1.3.1_And_V1.3.0\alphaCellCounterCPU1\CPU1_FLASH")

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("IPCRtoLBlockWrite")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_IPCRtoLBlockWrite")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$22)

	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$3, DW_AT_name("F28x_usDelay")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_F28x_usDelay")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$12)

	.dwendtag $C$DW$3


$C$DW$5	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$5, DW_AT_name("IPCInitialize")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_IPCInitialize")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$162)

$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$23)

$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$23)

	.dwendtag $C$DW$5


$C$DW$9	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$9, DW_AT_name("SendIpcFlag")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_SendIpcFlag")
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$26)

	.dwendtag $C$DW$9


$C$DW$11	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$11, DW_AT_name("IpcSync")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_IpcSync")
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$26)

	.dwendtag $C$DW$11


$C$DW$13	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$13, DW_AT_name("InitIpc")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_InitIpc")
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external
	.dwendtag $C$DW$13


$C$DW$14	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$14, DW_AT_name("IPCRtoLSetBits_Protected")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_IPCRtoLSetBits_Protected")
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$22)

	.dwendtag $C$DW$14


$C$DW$16	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$16, DW_AT_name("IPCRtoLDataWrite")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_IPCRtoLDataWrite")
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$22)

	.dwendtag $C$DW$16


$C$DW$18	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$18, DW_AT_name("IPCRtoLClearBits_Protected")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_IPCRtoLClearBits_Protected")
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$22)

	.dwendtag $C$DW$18


$C$DW$20	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$20, DW_AT_name("IPCLtoRBlockWrite")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_IPCLtoRBlockWrite")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external
$C$DW$21	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$162)

$C$DW$22	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$19)

$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$19)

$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$23)

$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$23)

$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$23)

	.dwendtag $C$DW$20


$C$DW$27	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$27, DW_AT_name("IpcGet")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_IpcGet")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external
$C$DW$28	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$162)

$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$22)

$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$23)

	.dwendtag $C$DW$27


$C$DW$31	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$31, DW_AT_name("IPCLtoRDataWrite")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_IPCLtoRDataWrite")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$31, DW_AT_declaration
	.dwattr $C$DW$31, DW_AT_external
$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$162)

$C$DW$33	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$19)

$C$DW$34	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$19)

$C$DW$35	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$23)

$C$DW$36	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$23)

$C$DW$37	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$19)

	.dwendtag $C$DW$31

	.global	_Cpu1pulMsgRam
_Cpu1pulMsgRam:	.usect	".ebss",2,1,1
$C$DW$38	.dwtag  DW_TAG_variable
	.dwattr $C$DW$38, DW_AT_name("Cpu1pulMsgRam")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_Cpu1pulMsgRam")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_addr _Cpu1pulMsgRam]
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$203)
	.dwattr $C$DW$38, DW_AT_external

	.global	_Cpu2pulMsgRam
_Cpu2pulMsgRam:	.usect	".ebss",2,1,1
$C$DW$39	.dwtag  DW_TAG_variable
	.dwattr $C$DW$39, DW_AT_name("Cpu2pulMsgRam")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_Cpu2pulMsgRam")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_addr _Cpu2pulMsgRam]
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$203)
	.dwattr $C$DW$39, DW_AT_external

	.global	_g_sIpcController2
_g_sIpcController2:	.usect	".ebss",14,1,1
$C$DW$40	.dwtag  DW_TAG_variable
	.dwattr $C$DW$40, DW_AT_name("g_sIpcController2")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_g_sIpcController2")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_addr _g_sIpcController2]
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$40, DW_AT_external

	.global	_g_sIpcController1
_g_sIpcController1:	.usect	".ebss",14,1,1
$C$DW$41	.dwtag  DW_TAG_variable
	.dwattr $C$DW$41, DW_AT_name("g_sIpcController1")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_g_sIpcController1")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_addr _g_sIpcController1]
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$41, DW_AT_external

$C$DW$42	.dwtag  DW_TAG_variable
	.dwattr $C$DW$42, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$42, DW_AT_declaration
	.dwattr $C$DW$42, DW_AT_external

$C$DW$43	.dwtag  DW_TAG_variable
	.dwattr $C$DW$43, DW_AT_name("IpcRegs")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_IpcRegs")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external

_Cpu1IpcParam:	.usect	".RAMGS10_Cpu1IpcParam",46,1,1
$C$DW$44	.dwtag  DW_TAG_variable
	.dwattr $C$DW$44, DW_AT_name("Cpu1IpcParam")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_Cpu1IpcParam")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_addr _Cpu1IpcParam]

$C$DW$45	.dwtag  DW_TAG_variable
	.dwattr $C$DW$45, DW_AT_name("MemCfgRegs")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_MemCfgRegs")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$171)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external

	.global	_Cpu1IpcTxMsgQue
_Cpu1IpcTxMsgQue:	.usect	".RAMGS10_Cpu1IpcParam",160,1,1
$C$DW$46	.dwtag  DW_TAG_variable
	.dwattr $C$DW$46, DW_AT_name("Cpu1IpcTxMsgQue")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_Cpu1IpcTxMsgQue")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_addr _Cpu1IpcTxMsgQue]
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$46, DW_AT_external

	.global	_Cpu1IpcRxMsgQue
_Cpu1IpcRxMsgQue:	.usect	".RAMGS10_Cpu1IpcParam",160,1,1
$C$DW$47	.dwtag  DW_TAG_variable
	.dwattr $C$DW$47, DW_AT_name("Cpu1IpcRxMsgQue")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_Cpu1IpcRxMsgQue")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_addr _Cpu1IpcRxMsgQue]
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$47, DW_AT_external

$C$DW$48	.dwtag  DW_TAG_variable
	.dwattr $C$DW$48, DW_AT_name("PieVectTable")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_PieVectTable")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$48, DW_AT_declaration
	.dwattr $C$DW$48, DW_AT_external

;	D:\Softwares\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\DITTOM~1.DEV\\AppData\\Local\\Temp\\2025212 
	.sect	".text:_IPCInitialize_IPC"
	.clink
	.global	_IPCInitialize_IPC

$C$DW$49	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$49, DW_AT_name("IPCInitialize_IPC")
	.dwattr $C$DW$49, DW_AT_low_pc(_IPCInitialize_IPC)
	.dwattr $C$DW$49, DW_AT_high_pc(0x00)
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_IPCInitialize_IPC")
	.dwattr $C$DW$49, DW_AT_external
	.dwattr $C$DW$49, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$49, DW_AT_TI_begin_line(0x45)
	.dwattr $C$DW$49, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$49, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/IpcModule.c",line 70,column 1,is_stmt,address _IPCInitialize_IPC,isa 0

	.dwfde $C$DW$CIE, _IPCInitialize_IPC
;----------------------------------------------------------------------
;  69 | void IPCInitialize_IPC(void)                                           
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCInitialize_IPC            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_IPCInitialize_IPC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/IpcModule.c",line 71,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  71 | IPCCpu2IpcReqMemAccess();                                              
;----------------------------------------------------------------------
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_name("_IPCCpu2IpcReqMemAccess")
	.dwattr $C$DW$50, DW_AT_TI_call

        LCR       #_IPCCpu2IpcReqMemAccess ; [CPU_] |71| 
        ; call occurs [#_IPCCpu2IpcReqMemAccess] ; [] |71| 
	.dwpsn	file "../source/IpcModule.c",line 72,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  72 | DELAY_US(10000);                                                       
;  73 | //! Interrupts that are used in this example are re-mapped to          
;  74 | //! ISR functions found within this file.                              
;----------------------------------------------------------------------
        MOVL      XAR4,#399998          ; [CPU_U] |72| 
        MOVL      ACC,XAR4              ; [CPU_] |72| 
$C$DW$51	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$51, DW_AT_low_pc(0x00)
	.dwattr $C$DW$51, DW_AT_name("_F28x_usDelay")
	.dwattr $C$DW$51, DW_AT_TI_call

        LCR       #_F28x_usDelay        ; [CPU_] |72| 
        ; call occurs [#_F28x_usDelay] ; [] |72| 
	.dwpsn	file "../source/IpcModule.c",line 75,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  75 | EALLOW; //! This is needed to write to EALLOW protected registers      
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../source/IpcModule.c",line 76,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  76 | PieVectTable.IPC0_INT = &CPU02toCPU01IPC0IntHandler;                   
;----------------------------------------------------------------------
        MOVW      DP,#_PieVectTable+264 ; [CPU_U] 
        MOVL      XAR4,#_CPU02toCPU01IPC0IntHandler ; [CPU_U] |76| 
        MOVL      @_PieVectTable+264,XAR4 ; [CPU_] |76| 
	.dwpsn	file "../source/IpcModule.c",line 77,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  77 | PieVectTable.IPC1_INT = &CPU02toCPU01IPC1IntHandler;                   
;----------------------------------------------------------------------
        MOVL      XAR4,#_CPU02toCPU01IPC1IntHandler ; [CPU_U] |77| 
        MOVL      @_PieVectTable+266,XAR4 ; [CPU_] |77| 
	.dwpsn	file "../source/IpcModule.c",line 78,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  78 | EDIS;    //! This is needed to disable write to EALLOW protected regist
;     | ers                                                                    
;  80 | //! Step 4. Initialize the Device Peripherals:                         
;----------------------------------------------------------------------
 EDIS
	.dwpsn	file "../source/IpcModule.c",line 82,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  82 | IPCInitialize (&g_sIpcController1, IPC_INT0, IPC_INT0);                
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |82| 
        MOVB      AH,#1                 ; [CPU_] |82| 
        SPM       #0                    ; [CPU_] 
        MOVL      XAR4,#_g_sIpcController1 ; [CPU_U] |82| 
$C$DW$52	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$52, DW_AT_low_pc(0x00)
	.dwattr $C$DW$52, DW_AT_name("_IPCInitialize")
	.dwattr $C$DW$52, DW_AT_TI_call

        LCR       #_IPCInitialize       ; [CPU_] |82| 
        ; call occurs [#_IPCInitialize] ; [] |82| 
	.dwpsn	file "../source/IpcModule.c",line 83,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  83 | IPCInitialize (&g_sIpcController2, IPC_INT1, IPC_INT1);                
;  84 | // TODO                                                                
;  86 | //! Step 5. User specific code, enable interrupts:                     
;  88 | //! Enable CPU INT1 which is connected to Upper PIE IPC INT0-3:        
;----------------------------------------------------------------------
        MOVB      AL,#2                 ; [CPU_] |83| 
        MOVB      AH,#2                 ; [CPU_] |83| 
        MOVL      XAR4,#_g_sIpcController2 ; [CPU_U] |83| 
$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("_IPCInitialize")
	.dwattr $C$DW$53, DW_AT_TI_call

        LCR       #_IPCInitialize       ; [CPU_] |83| 
        ; call occurs [#_IPCInitialize] ; [] |83| 
	.dwpsn	file "../source/IpcModule.c",line 89,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  89 | IER |= M_INT1;                                                         
;  91 | //! Enable CPU2 to CPU1 IPC INTn in the PIE: Group 1 interrupts        
;----------------------------------------------------------------------
        OR        IER,#0x0001           ; [CPU_] |89| 
	.dwpsn	file "../source/IpcModule.c",line 92,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  92 | PieCtrlRegs.PIEIER1.bit.INTx13 = 1;    //! CPU2 to CPU1 INT0           
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+2    ; [CPU_U] 
        OR        @_PieCtrlRegs+2,#0x1000 ; [CPU_] |92| 
	.dwpsn	file "../source/IpcModule.c",line 93,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  93 | PieCtrlRegs.PIEIER1.bit.INTx14 = 1;    //! CPU2 to CPU1 INT1           
;  95 | //! Enable global Interrupts and higher priority real-time debug events
;     | :                                                                      
;----------------------------------------------------------------------
        OR        @_PieCtrlRegs+2,#0x2000 ; [CPU_] |93| 
	.dwpsn	file "../source/IpcModule.c",line 96,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  96 | EINT;   //! Enable Global interrupt INTM                               
;----------------------------------------------------------------------
 clrc INTM
	.dwpsn	file "../source/IpcModule.c",line 97,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  97 | ERTM;   //! Enable Global realtime interrupt DBGM                      
;----------------------------------------------------------------------
 clrc DBGM
	.dwpsn	file "../source/IpcModule.c",line 99,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  99 | InitIpc();                                                             
;----------------------------------------------------------------------
        SPM       #0                    ; [CPU_] 
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_name("_InitIpc")
	.dwattr $C$DW$54, DW_AT_TI_call

        LCR       #_InitIpc             ; [CPU_] |99| 
        ; call occurs [#_InitIpc] ; [] |99| 
	.dwpsn	file "../source/IpcModule.c",line 100,column 1,is_stmt,isa 0
$C$DW$55	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$55, DW_AT_low_pc(0x00)
	.dwattr $C$DW$55, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$49, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$49, DW_AT_TI_end_line(0x64)
	.dwattr $C$DW$49, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$49

	.sect	".text:_IPCCpu1toCpu2IpcCommSync"
	.clink
	.global	_IPCCpu1toCpu2IpcCommSync

$C$DW$56	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$56, DW_AT_name("IPCCpu1toCpu2IpcCommSync")
	.dwattr $C$DW$56, DW_AT_low_pc(_IPCCpu1toCpu2IpcCommSync)
	.dwattr $C$DW$56, DW_AT_high_pc(0x00)
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_IPCCpu1toCpu2IpcCommSync")
	.dwattr $C$DW$56, DW_AT_external
	.dwattr $C$DW$56, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$56, DW_AT_TI_begin_line(0x70)
	.dwattr $C$DW$56, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$56, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/IpcModule.c",line 113,column 1,is_stmt,address _IPCCpu1toCpu2IpcCommSync,isa 0

	.dwfde $C$DW$CIE, _IPCCpu1toCpu2IpcCommSync
;----------------------------------------------------------------------
; 112 | void IPCCpu1toCpu2IpcCommSync(void)                                    
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCCpu1toCpu2IpcCommSync     FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_IPCCpu1toCpu2IpcCommSync:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/IpcModule.c",line 114,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 114 | Cpu2pulMsgRam = (void *)CPU02TOCPU01_PASSMSG;                          
;----------------------------------------------------------------------
        MOVL      XAR4,#261108          ; [CPU_U] |114| 
        MOVW      DP,#_Cpu2pulMsgRam    ; [CPU_U] 
        MOVL      @_Cpu2pulMsgRam,XAR4  ; [CPU_] |114| 
	.dwpsn	file "../source/IpcModule.c",line 115,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 115 | Cpu1pulMsgRam = (void *)CPU01TOCPU02_PASSMSG;                          
; 117 | //!Get the address of CPU2 to CPU1 buffer location                     
;----------------------------------------------------------------------
        MOVL      XAR4,#262132          ; [CPU_U] |115| 
        MOVL      @_Cpu1pulMsgRam,XAR4  ; [CPU_] |115| 
	.dwpsn	file "../source/IpcModule.c",line 118,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 118 | Cpu1pulMsgRam[0] = (uint32_t)&Cpu1IpcParam.unCpu2ToCpu1RxBuf[0];       
;----------------------------------------------------------------------
        MOVL      XAR5,@_Cpu1pulMsgRam  ; [CPU_] |118| 
        MOVL      XAR4,#_Cpu1IpcParam+20 ; [CPU_U] |118| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |118| 
	.dwpsn	file "../source/IpcModule.c",line 120,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 120 | IpcSync(FLAG17);                                                       
;----------------------------------------------------------------------
        MOVB      AL,#17                ; [CPU_] |120| 
$C$DW$57	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$57, DW_AT_low_pc(0x00)
	.dwattr $C$DW$57, DW_AT_name("_IpcSync")
	.dwattr $C$DW$57, DW_AT_TI_call

        LCR       #_IpcSync             ; [CPU_] |120| 
        ; call occurs [#_IpcSync] ; [] |120| 
	.dwpsn	file "../source/IpcModule.c",line 121,column 1,is_stmt,isa 0
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$56, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$56, DW_AT_TI_end_line(0x79)
	.dwattr $C$DW$56, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$56

	.sect	".text:_IPCCpu1toCpu2IpcInterrupt"
	.clink
	.global	_IPCCpu1toCpu2IpcInterrupt

$C$DW$59	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$59, DW_AT_name("IPCCpu1toCpu2IpcInterrupt")
	.dwattr $C$DW$59, DW_AT_low_pc(_IPCCpu1toCpu2IpcInterrupt)
	.dwattr $C$DW$59, DW_AT_high_pc(0x00)
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_IPCCpu1toCpu2IpcInterrupt")
	.dwattr $C$DW$59, DW_AT_external
	.dwattr $C$DW$59, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$59, DW_AT_TI_begin_line(0x84)
	.dwattr $C$DW$59, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$59, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/IpcModule.c",line 133,column 1,is_stmt,address _IPCCpu1toCpu2IpcInterrupt,isa 0

	.dwfde $C$DW$CIE, _IPCCpu1toCpu2IpcInterrupt
$C$DW$60	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$60, DW_AT_name("Flag")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_Flag")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 132 | void IPCCpu1toCpu2IpcInterrupt(uint16_t Flag)                          
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCCpu1toCpu2IpcInterrupt    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_IPCCpu1toCpu2IpcInterrupt:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$61	.dwtag  DW_TAG_variable
	.dwattr $C$DW$61, DW_AT_name("Flag")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_Flag")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 134 | //!Set IPC flag                                                        
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |133| 
	.dwpsn	file "../source/IpcModule.c",line 135,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 135 | SendIpcFlag(Flag);                                                     
;----------------------------------------------------------------------
$C$DW$62	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$62, DW_AT_low_pc(0x00)
	.dwattr $C$DW$62, DW_AT_name("_SendIpcFlag")
	.dwattr $C$DW$62, DW_AT_TI_call

        LCR       #_SendIpcFlag         ; [CPU_] |135| 
        ; call occurs [#_SendIpcFlag] ; [] |135| 
	.dwpsn	file "../source/IpcModule.c",line 136,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$63	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$63, DW_AT_low_pc(0x00)
	.dwattr $C$DW$63, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$59, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$59, DW_AT_TI_end_line(0x88)
	.dwattr $C$DW$59, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$59

	.sect	".text:_IPCCpu1toCpu2IpcCmdData"
	.clink
	.global	_IPCCpu1toCpu2IpcCmdData

$C$DW$64	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$64, DW_AT_name("IPCCpu1toCpu2IpcCmdData")
	.dwattr $C$DW$64, DW_AT_low_pc(_IPCCpu1toCpu2IpcCmdData)
	.dwattr $C$DW$64, DW_AT_high_pc(0x00)
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_IPCCpu1toCpu2IpcCmdData")
	.dwattr $C$DW$64, DW_AT_external
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$64, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$64, DW_AT_TI_begin_line(0x97)
	.dwattr $C$DW$64, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$64, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/IpcModule.c",line 152,column 1,is_stmt,address _IPCCpu1toCpu2IpcCmdData,isa 0

	.dwfde $C$DW$CIE, _IPCCpu1toCpu2IpcCmdData
$C$DW$65	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$65, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg12]

$C$DW$66	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$66, DW_AT_name("dataLength")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_dataLength")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 151 | uint16_t IPCCpu1toCpu2IpcCmdData(stIpcParameters *Cpu1IpcPacket, uint16
;     | _t dataLength)                                                         
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCCpu1toCpu2IpcCmdData      FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_IPCCpu1toCpu2IpcCmdData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$67	.dwtag  DW_TAG_variable
	.dwattr $C$DW$67, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_breg20 -6]

$C$DW$68	.dwtag  DW_TAG_variable
	.dwattr $C$DW$68, DW_AT_name("dataLength")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_dataLength")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_breg20 -7]

$C$DW$69	.dwtag  DW_TAG_variable
	.dwattr $C$DW$69, DW_AT_name("status")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_breg20 -8]

;----------------------------------------------------------------------
; 153 | //      uint32_t dataLength = 8;                                       
;----------------------------------------------------------------------
        MOV       *-SP[7],AL            ; [CPU_] |152| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |152| 
	.dwpsn	file "../source/IpcModule.c",line 154,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 154 | uint16_t status = 0;                                                   
; 155 | //!If the data length is greater htan 8, limit it to 8                 
;----------------------------------------------------------------------
        MOV       *-SP[8],#0            ; [CPU_] |154| 
	.dwpsn	file "../source/IpcModule.c",line 156,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 156 | if(dataLength > 8)                                                     
;----------------------------------------------------------------------
        CMPB      AL,#8                 ; [CPU_] |156| 
        B         $C$L1,LOS             ; [CPU_] |156| 
        ; branchcc occurs ; [] |156| 
	.dwpsn	file "../source/IpcModule.c",line 158,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 158 | dataLength = 8;                                                        
; 160 | //!Request for CPU1 Memory Access                                      
;----------------------------------------------------------------------
        MOVB      *-SP[7],#8,UNC        ; [CPU_] |158| 
$C$L1:    
	.dwpsn	file "../source/IpcModule.c",line 161,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 161 | IPCCpu1IpcReqMemAccess();                                              
; 162 | //!Write the data to be sent through IPC                               
;----------------------------------------------------------------------
$C$DW$70	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$70, DW_AT_low_pc(0x00)
	.dwattr $C$DW$70, DW_AT_name("_IPCCpu1IpcReqMemAccess")
	.dwattr $C$DW$70, DW_AT_TI_call

        LCR       #_IPCCpu1IpcReqMemAccess ; [CPU_] |161| 
        ; call occurs [#_IPCCpu1IpcReqMemAccess] ; [] |161| 
	.dwpsn	file "../source/IpcModule.c",line 163,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 163 | status = IPCLtoRBlockWrite(&g_sIpcController1, Cpu2pulMsgRam[0],\      
; 164 |         (uint32_t)&Cpu1IpcPacket->unCpu1ToCpu2TxBuf[0], \              
; 165 |         (uint32_t)dataLength, IPC_LENGTH_32_BITS,ENABLE_BLOCKING);     
; 166 | // Write retry code for status failure                                 
;----------------------------------------------------------------------
        MOVW      DP,#_Cpu2pulMsgRam    ; [CPU_U] 
        MOVL      ACC,*-SP[6]           ; [CPU_] |163| 
        MOVL      XAR4,@_Cpu2pulMsgRam  ; [CPU_] |163| 
        MOVL      *-SP[2],ACC           ; [CPU_] |163| 
        MOVB      *-SP[3],#2,UNC        ; [CPU_] |163| 
        MOVB      *-SP[4],#1,UNC        ; [CPU_] |163| 
        MOVZ      AR5,*-SP[7]           ; [CPU_] |163| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |163| 
        MOVL      XAR4,#_g_sIpcController1 ; [CPU_U] |163| 
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_name("_IPCLtoRBlockWrite")
	.dwattr $C$DW$71, DW_AT_TI_call

        LCR       #_IPCLtoRBlockWrite   ; [CPU_] |163| 
        ; call occurs [#_IPCLtoRBlockWrite] ; [] |163| 
        MOV       *-SP[8],AL            ; [CPU_] |163| 
	.dwpsn	file "../source/IpcModule.c",line 167,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 167 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../source/IpcModule.c",line 168,column 1,is_stmt,isa 0
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$72	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$72, DW_AT_low_pc(0x00)
	.dwattr $C$DW$72, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$64, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$64, DW_AT_TI_end_line(0xa8)
	.dwattr $C$DW$64, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$64

	.sect	".text:_IPCCpu1toCpu2IpcCmd"
	.clink
	.global	_IPCCpu1toCpu2IpcCmd

$C$DW$73	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$73, DW_AT_name("IPCCpu1toCpu2IpcCmd")
	.dwattr $C$DW$73, DW_AT_low_pc(_IPCCpu1toCpu2IpcCmd)
	.dwattr $C$DW$73, DW_AT_high_pc(0x00)
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_IPCCpu1toCpu2IpcCmd")
	.dwattr $C$DW$73, DW_AT_external
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$73, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$73, DW_AT_TI_begin_line(0xb5)
	.dwattr $C$DW$73, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$73, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../source/IpcModule.c",line 182,column 1,is_stmt,address _IPCCpu1toCpu2IpcCmd,isa 0

	.dwfde $C$DW$CIE, _IPCCpu1toCpu2IpcCmd
$C$DW$74	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$74, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 181 | uint16_t IPCCpu1toCpu2IpcCmd(stIpcParameters *Cpu1IpcPacket)           
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCCpu1toCpu2IpcCmd          FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            6 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_IPCCpu1toCpu2IpcCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$75	.dwtag  DW_TAG_variable
	.dwattr $C$DW$75, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_breg20 -8]

$C$DW$76	.dwtag  DW_TAG_variable
	.dwattr $C$DW$76, DW_AT_name("status")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_breg20 -9]

        MOVL      *-SP[8],XAR4          ; [CPU_] |182| 
	.dwpsn	file "../source/IpcModule.c",line 183,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 183 | uint16_t status = 0;                                                   
; 185 | //!Request for the memory access                                       
;----------------------------------------------------------------------
        MOV       *-SP[9],#0            ; [CPU_] |183| 
	.dwpsn	file "../source/IpcModule.c",line 186,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 186 | IPCCpu1IpcReqMemAccess();                                              
; 187 | //!Write data to the memory access                                     
;----------------------------------------------------------------------
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("_IPCCpu1IpcReqMemAccess")
	.dwattr $C$DW$77, DW_AT_TI_call

        LCR       #_IPCCpu1IpcReqMemAccess ; [CPU_] |186| 
        ; call occurs [#_IPCCpu1IpcReqMemAccess] ; [] |186| 
	.dwpsn	file "../source/IpcModule.c",line 188,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 188 | status = IPCLtoRDataWrite(&g_sIpcController1, Cpu2pulMsgRam[0], \      
; 189 |         Cpu1IpcPacket->unCpu1ToCpu2TxBuf[0], IPC_LENGTH_32_BITS,\      
; 190 |         ENABLE_BLOCKING,NO_FLAG);                                      
; 191 | // Write retry code for status failure                                 
;----------------------------------------------------------------------
        MOVL      XAR4,*-SP[8]          ; [CPU_] |188| 
        MOVW      DP,#_Cpu2pulMsgRam    ; [CPU_U] 
        MOVL      XAR6,*+XAR4[0]        ; [CPU_] |188| 
        MOVB      ACC,#0                ; [CPU_] |188| 
        MOVB      XAR5,#2               ; [CPU_] |188| 
        MOVL      XAR4,@_Cpu2pulMsgRam  ; [CPU_] |188| 
        MOVL      *-SP[2],XAR6          ; [CPU_] |188| 
        MOVB      *-SP[3],#1,UNC        ; [CPU_] |188| 
        MOVL      *-SP[6],ACC           ; [CPU_] |188| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |188| 
        MOVL      XAR4,#_g_sIpcController1 ; [CPU_U] |188| 
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_name("_IPCLtoRDataWrite")
	.dwattr $C$DW$78, DW_AT_TI_call

        LCR       #_IPCLtoRDataWrite    ; [CPU_] |188| 
        ; call occurs [#_IPCLtoRDataWrite] ; [] |188| 
        MOV       *-SP[9],AL            ; [CPU_] |188| 
	.dwpsn	file "../source/IpcModule.c",line 192,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 192 | return status;                                                         
;----------------------------------------------------------------------
	.dwpsn	file "../source/IpcModule.c",line 193,column 1,is_stmt,isa 0
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$73, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$73, DW_AT_TI_end_line(0xc1)
	.dwattr $C$DW$73, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$73

	.sect	".text:_IPCClearCpu1IpcPacketTxBuf"
	.clink
	.global	_IPCClearCpu1IpcPacketTxBuf

$C$DW$80	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$80, DW_AT_name("IPCClearCpu1IpcPacketTxBuf")
	.dwattr $C$DW$80, DW_AT_low_pc(_IPCClearCpu1IpcPacketTxBuf)
	.dwattr $C$DW$80, DW_AT_high_pc(0x00)
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_IPCClearCpu1IpcPacketTxBuf")
	.dwattr $C$DW$80, DW_AT_external
	.dwattr $C$DW$80, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$80, DW_AT_TI_begin_line(0xca)
	.dwattr $C$DW$80, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$80, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/IpcModule.c",line 203,column 1,is_stmt,address _IPCClearCpu1IpcPacketTxBuf,isa 0

	.dwfde $C$DW$CIE, _IPCClearCpu1IpcPacketTxBuf
$C$DW$81	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$81, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 202 | void IPCClearCpu1IpcPacketTxBuf(stIpcParameters *Cpu1IpcPacket)        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCClearCpu1IpcPacketTxBuf   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_IPCClearCpu1IpcPacketTxBuf:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$82	.dwtag  DW_TAG_variable
	.dwattr $C$DW$82, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_breg20 -2]

$C$DW$83	.dwtag  DW_TAG_variable
	.dwattr $C$DW$83, DW_AT_name("Counter")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_Counter")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_breg20 -3]

        MOVL      *-SP[2],XAR4          ; [CPU_] |203| 
	.dwpsn	file "../source/IpcModule.c",line 204,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 204 | uint16_t Counter=0;                                                    
; 205 | //!Clear the IPC transmit buffer                                       
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |204| 
	.dwpsn	file "../source/IpcModule.c",line 206,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 206 | for(Counter = 0; Counter < 8 ; Counter++)                              
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |206| 
	.dwpsn	file "../source/IpcModule.c",line 206,column 19,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |206| 
        CMPB      AL,#8                 ; [CPU_] |206| 
        B         $C$L3,HIS             ; [CPU_] |206| 
        ; branchcc occurs ; [] |206| 
        CLRC      SXM                   ; [CPU_] 
        MOVB      XAR6,#0               ; [CPU_] |208| 
$C$L2:    
	.dwpsn	file "../source/IpcModule.c",line 208,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 208 | Cpu1IpcPacket->unCpu1ToCpu2TxBuf[Counter] = 0x00000000;                
;----------------------------------------------------------------------
        MOV       ACC,*-SP[3] << 1      ; [CPU_] |208| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |208| 
        MOVL      XAR4,ACC              ; [CPU_] |208| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |208| 
	.dwpsn	file "../source/IpcModule.c",line 206,column 33,is_stmt,isa 0
        INC       *-SP[3]               ; [CPU_] |206| 
	.dwpsn	file "../source/IpcModule.c",line 206,column 19,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |206| 
        CMPB      AL,#8                 ; [CPU_] |206| 
        B         $C$L2,LO              ; [CPU_] |206| 
        ; branchcc occurs ; [] |206| 
	.dwpsn	file "../source/IpcModule.c",line 210,column 1,is_stmt,isa 0
$C$L3:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$84	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$84, DW_AT_low_pc(0x00)
	.dwattr $C$DW$84, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$80, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$80, DW_AT_TI_end_line(0xd2)
	.dwattr $C$DW$80, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$80

	.sect	".text:_IPCClearCpu1IpcPacketRxBuf"
	.clink
	.global	_IPCClearCpu1IpcPacketRxBuf

$C$DW$85	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$85, DW_AT_name("IPCClearCpu1IpcPacketRxBuf")
	.dwattr $C$DW$85, DW_AT_low_pc(_IPCClearCpu1IpcPacketRxBuf)
	.dwattr $C$DW$85, DW_AT_high_pc(0x00)
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_IPCClearCpu1IpcPacketRxBuf")
	.dwattr $C$DW$85, DW_AT_external
	.dwattr $C$DW$85, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$85, DW_AT_TI_begin_line(0xdb)
	.dwattr $C$DW$85, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$85, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/IpcModule.c",line 220,column 1,is_stmt,address _IPCClearCpu1IpcPacketRxBuf,isa 0

	.dwfde $C$DW$CIE, _IPCClearCpu1IpcPacketRxBuf
$C$DW$86	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$86, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 219 | void IPCClearCpu1IpcPacketRxBuf(stIpcParameters *Cpu1IpcPacket)        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCClearCpu1IpcPacketRxBuf   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_IPCClearCpu1IpcPacketRxBuf:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$87	.dwtag  DW_TAG_variable
	.dwattr $C$DW$87, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -2]

$C$DW$88	.dwtag  DW_TAG_variable
	.dwattr $C$DW$88, DW_AT_name("Counter")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_Counter")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_breg20 -3]

        MOVL      *-SP[2],XAR4          ; [CPU_] |220| 
	.dwpsn	file "../source/IpcModule.c",line 221,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 221 | uint16_t Counter=0;                                                    
; 222 | //!Clear the IPC recieve buffer                                        
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |221| 
	.dwpsn	file "../source/IpcModule.c",line 223,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 223 | for(Counter = 0; Counter < 8 ; Counter++)                              
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |223| 
	.dwpsn	file "../source/IpcModule.c",line 223,column 19,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |223| 
        CMPB      AL,#8                 ; [CPU_] |223| 
        B         $C$L5,HIS             ; [CPU_] |223| 
        ; branchcc occurs ; [] |223| 
        CLRC      SXM                   ; [CPU_] 
        MOVB      XAR6,#0               ; [CPU_] |225| 
$C$L4:    
	.dwpsn	file "../source/IpcModule.c",line 225,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 225 | Cpu1IpcPacket->unCpu2ToCpu1RxBuf[Counter] = 0x00000000;                
;----------------------------------------------------------------------
        MOV       ACC,*-SP[3] << 1      ; [CPU_] |225| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |225| 
        MOVL      XAR4,ACC              ; [CPU_] |225| 
        MOVB      XAR0,#20              ; [CPU_] |225| 
        MOVL      *+XAR4[AR0],XAR6      ; [CPU_] |225| 
	.dwpsn	file "../source/IpcModule.c",line 223,column 33,is_stmt,isa 0
        INC       *-SP[3]               ; [CPU_] |223| 
	.dwpsn	file "../source/IpcModule.c",line 223,column 19,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |223| 
        CMPB      AL,#8                 ; [CPU_] |223| 
        B         $C$L4,LO              ; [CPU_] |223| 
        ; branchcc occurs ; [] |223| 
	.dwpsn	file "../source/IpcModule.c",line 227,column 1,is_stmt,isa 0
$C$L5:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$89	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$89, DW_AT_low_pc(0x00)
	.dwattr $C$DW$89, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$85, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$85, DW_AT_TI_end_line(0xe3)
	.dwattr $C$DW$85, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$85

	.sect	".text:_IPCCpu1IpcPacketToTxMsgQue"
	.clink
	.global	_IPCCpu1IpcPacketToTxMsgQue

$C$DW$90	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$90, DW_AT_name("IPCCpu1IpcPacketToTxMsgQue")
	.dwattr $C$DW$90, DW_AT_low_pc(_IPCCpu1IpcPacketToTxMsgQue)
	.dwattr $C$DW$90, DW_AT_high_pc(0x00)
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_IPCCpu1IpcPacketToTxMsgQue")
	.dwattr $C$DW$90, DW_AT_external
	.dwattr $C$DW$90, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$90, DW_AT_TI_begin_line(0xec)
	.dwattr $C$DW$90, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$90, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/IpcModule.c",line 237,column 1,is_stmt,address _IPCCpu1IpcPacketToTxMsgQue,isa 0

	.dwfde $C$DW$CIE, _IPCCpu1IpcPacketToTxMsgQue
$C$DW$91	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$91, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg12]

;----------------------------------------------------------------------
; 236 | void IPCCpu1IpcPacketToTxMsgQue(stIpcParameters *Cpu1IpcPacket)        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCCpu1IpcPacketToTxMsgQue   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_IPCCpu1IpcPacketToTxMsgQue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$92	.dwtag  DW_TAG_variable
	.dwattr $C$DW$92, DW_AT_name("Cpu1IpcPacket")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_Cpu1IpcPacket")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_breg20 -2]

$C$DW$93	.dwtag  DW_TAG_variable
	.dwattr $C$DW$93, DW_AT_name("Counter")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_Counter")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_breg20 -3]

        MOVL      *-SP[2],XAR4          ; [CPU_] |237| 
	.dwpsn	file "../source/IpcModule.c",line 238,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 238 | uint16_t Counter=0;                                                    
; 239 | //!Update the data to be transmitted to Tranmit message queue          
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |238| 
	.dwpsn	file "../source/IpcModule.c",line 240,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 240 | for(Counter = 0; Counter < 8 ; Counter++)                              
;----------------------------------------------------------------------
        MOV       *-SP[3],#0            ; [CPU_] |240| 
	.dwpsn	file "../source/IpcModule.c",line 240,column 19,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |240| 
        CMPB      AL,#8                 ; [CPU_] |240| 
        B         $C$L7,HIS             ; [CPU_] |240| 
        ; branchcc occurs ; [] |240| 
        CLRC      SXM                   ; [CPU_] 
$C$L6:    
	.dwpsn	file "../source/IpcModule.c",line 242,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 242 | Cpu1IpcTxMsgQue[Cpu1IpcPacket->TxMsgQueCurrentPtr][Counter] = \        
; 243 |         Cpu1IpcPacket->unCpu1ToCpu2TxBuf[Counter];                     
;----------------------------------------------------------------------
        MOVB      ACC,#44               ; [CPU_] |242| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |242| 
        MOVL      XAR4,ACC              ; [CPU_] |242| 
        MOV       ACC,*+XAR4[0] << 4    ; [CPU_] |242| 
        MOVL      XAR4,#_Cpu1IpcTxMsgQue ; [CPU_U] |242| 
        ADDL      XAR4,ACC              ; [CPU_] |242| 
        MOV       ACC,*-SP[3] << 1      ; [CPU_] |242| 
        ADDL      ACC,XAR4              ; [CPU_] |242| 
        MOVL      XAR6,ACC              ; [CPU_] |242| 
        MOV       ACC,*-SP[3] << 1      ; [CPU_] |242| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |242| 
        MOVL      XAR4,ACC              ; [CPU_] |242| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |242| 
        MOVL      XAR4,XAR6             ; [CPU_] |242| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |242| 
	.dwpsn	file "../source/IpcModule.c",line 240,column 33,is_stmt,isa 0
        INC       *-SP[3]               ; [CPU_] |240| 
	.dwpsn	file "../source/IpcModule.c",line 240,column 19,is_stmt,isa 0
        MOV       AL,*-SP[3]            ; [CPU_] |240| 
        CMPB      AL,#8                 ; [CPU_] |240| 
        B         $C$L6,LO              ; [CPU_] |240| 
        ; branchcc occurs ; [] |240| 
$C$L7:    
	.dwpsn	file "../source/IpcModule.c",line 245,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 245 | Cpu1IpcPacket->TxMsgQueCurrentPtr++;                                   
;----------------------------------------------------------------------
        MOVB      ACC,#44               ; [CPU_] |245| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |245| 
        MOVL      XAR4,ACC              ; [CPU_] |245| 
        INC       *+XAR4[0]             ; [CPU_] |245| 
	.dwpsn	file "../source/IpcModule.c",line 247,column 1,is_stmt,isa 0
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$90, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$90, DW_AT_TI_end_line(0xf7)
	.dwattr $C$DW$90, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$90

	.sect	".text:_IPCCpu1IpcPacketToRxMsgQue"
	.clink
	.global	_IPCCpu1IpcPacketToRxMsgQue

$C$DW$95	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$95, DW_AT_name("IPCCpu1IpcPacketToRxMsgQue")
	.dwattr $C$DW$95, DW_AT_low_pc(_IPCCpu1IpcPacketToRxMsgQue)
	.dwattr $C$DW$95, DW_AT_high_pc(0x00)
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_IPCCpu1IpcPacketToRxMsgQue")
	.dwattr $C$DW$95, DW_AT_external
	.dwattr $C$DW$95, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$95, DW_AT_TI_begin_line(0x100)
	.dwattr $C$DW$95, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$95, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/IpcModule.c",line 257,column 1,is_stmt,address _IPCCpu1IpcPacketToRxMsgQue,isa 0

	.dwfde $C$DW$CIE, _IPCCpu1IpcPacketToRxMsgQue
;----------------------------------------------------------------------
; 256 | void IPCCpu1IpcPacketToRxMsgQue(void)                                  
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCCpu1IpcPacketToRxMsgQue   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_IPCCpu1IpcPacketToRxMsgQue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$96	.dwtag  DW_TAG_variable
	.dwattr $C$DW$96, DW_AT_name("Counter")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_Counter")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_breg20 -1]

	.dwpsn	file "../source/IpcModule.c",line 258,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 258 | uint16_t Counter=0;                                                    
; 259 | //!Update the data to received to the recieve message queue            
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |258| 
	.dwpsn	file "../source/IpcModule.c",line 260,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 260 | for(Counter = 0; Counter < 8 ; Counter++)                              
;----------------------------------------------------------------------
        MOV       *-SP[1],#0            ; [CPU_] |260| 
	.dwpsn	file "../source/IpcModule.c",line 260,column 19,is_stmt,isa 0
        MOV       AL,*-SP[1]            ; [CPU_] |260| 
        CMPB      AL,#8                 ; [CPU_] |260| 
        B         $C$L9,HIS             ; [CPU_] |260| 
        ; branchcc occurs ; [] |260| 
        CLRC      SXM                   ; [CPU_] 
$C$L8:    
	.dwpsn	file "../source/IpcModule.c",line 262,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 262 | Cpu1IpcTxMsgQue[Cpu1IpcParam.RxMsgQueCurrentPtr][Counter] =\           
; 263 |         Cpu1IpcParam.unCpu2ToCpu1RxBuf[Counter];                       
;----------------------------------------------------------------------
        MOV       ACC,*-SP[1] << 1      ; [CPU_] |262| 
        MOVL      XAR5,#_Cpu1IpcParam+20 ; [CPU_U] |262| 
        MOVW      DP,#_Cpu1IpcParam+45  ; [CPU_U] 
        ADDL      XAR5,ACC              ; [CPU_] |262| 
        MOVL      XAR6,#_Cpu1IpcTxMsgQue ; [CPU_U] |262| 
        MOV       ACC,@_Cpu1IpcParam+45 << 4 ; [CPU_] |262| 
        ADDL      XAR6,ACC              ; [CPU_] |262| 
        MOVL      XAR7,*+XAR5[0]        ; [CPU_] |262| 
        MOV       ACC,*-SP[1] << 1      ; [CPU_] |262| 
        ADDL      ACC,XAR6              ; [CPU_] |262| 
        MOVL      XAR4,ACC              ; [CPU_] |262| 
        MOVL      *+XAR4[0],XAR7        ; [CPU_] |262| 
	.dwpsn	file "../source/IpcModule.c",line 260,column 33,is_stmt,isa 0
        INC       *-SP[1]               ; [CPU_] |260| 
	.dwpsn	file "../source/IpcModule.c",line 260,column 19,is_stmt,isa 0
        MOV       AL,*-SP[1]            ; [CPU_] |260| 
        CMPB      AL,#8                 ; [CPU_] |260| 
        B         $C$L8,LO              ; [CPU_] |260| 
        ; branchcc occurs ; [] |260| 
$C$L9:    
	.dwpsn	file "../source/IpcModule.c",line 266,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 266 | Cpu1IpcParam.RxMsgQueCurrentPtr++;                                     
;----------------------------------------------------------------------
        MOVW      DP,#_Cpu1IpcParam+45  ; [CPU_U] 
        INC       @_Cpu1IpcParam+45     ; [CPU_] |266| 
	.dwpsn	file "../source/IpcModule.c",line 268,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$95, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$95, DW_AT_TI_end_line(0x10c)
	.dwattr $C$DW$95, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$95

	.sect	".text:_IPCCpu1IpcReqMemAccess"
	.clink
	.global	_IPCCpu1IpcReqMemAccess

$C$DW$98	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$98, DW_AT_name("IPCCpu1IpcReqMemAccess")
	.dwattr $C$DW$98, DW_AT_low_pc(_IPCCpu1IpcReqMemAccess)
	.dwattr $C$DW$98, DW_AT_high_pc(0x00)
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_IPCCpu1IpcReqMemAccess")
	.dwattr $C$DW$98, DW_AT_external
	.dwattr $C$DW$98, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$98, DW_AT_TI_begin_line(0x117)
	.dwattr $C$DW$98, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$98, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/IpcModule.c",line 280,column 1,is_stmt,address _IPCCpu1IpcReqMemAccess,isa 0

	.dwfde $C$DW$CIE, _IPCCpu1IpcReqMemAccess
;----------------------------------------------------------------------
; 279 | void IPCCpu1IpcReqMemAccess(void)                                      
; 281 | //!Request Memory Access to GS0 SARAM for CPU01                        
; 282 | //!Clear bits to let CPU01 own GS0                                     
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCCpu1IpcReqMemAccess       FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_IPCCpu1IpcReqMemAccess:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/IpcModule.c",line 283,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 283 | if((MemCfgRegs.GSxMSEL.bit.MSEL_GS10) == 1)                            
;----------------------------------------------------------------------
        MOVW      DP,#_MemCfgRegs+68    ; [CPU_U] 
        AND       AL,@_MemCfgRegs+68,#0x0400 ; [CPU_] |283| 
        LSR       AL,10                 ; [CPU_] |283| 
        CMPB      AL,#1                 ; [CPU_] |283| 
        B         $C$L10,NEQ            ; [CPU_] |283| 
        ; branchcc occurs ; [] |283| 
	.dwpsn	file "../source/IpcModule.c",line 285,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 285 | EALLOW;                                                                
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../source/IpcModule.c",line 286,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 286 | MemCfgRegs.GSxMSEL.bit.MSEL_GS10 = 0;                                  
;----------------------------------------------------------------------
        AND       @_MemCfgRegs+68,#0xfbff ; [CPU_] |286| 
	.dwpsn	file "../source/IpcModule.c",line 287,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 287 | EDIS;                                                                  
;----------------------------------------------------------------------
 EDIS
	.dwpsn	file "../source/IpcModule.c",line 289,column 1,is_stmt,isa 0
$C$L10:    
        SPM       #0                    ; [CPU_] 
$C$DW$99	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$99, DW_AT_low_pc(0x00)
	.dwattr $C$DW$99, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$98, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$98, DW_AT_TI_end_line(0x121)
	.dwattr $C$DW$98, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$98

	.sect	".text:_IPCCpu2IpcReqMemAccess"
	.clink
	.global	_IPCCpu2IpcReqMemAccess

$C$DW$100	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$100, DW_AT_name("IPCCpu2IpcReqMemAccess")
	.dwattr $C$DW$100, DW_AT_low_pc(_IPCCpu2IpcReqMemAccess)
	.dwattr $C$DW$100, DW_AT_high_pc(0x00)
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_IPCCpu2IpcReqMemAccess")
	.dwattr $C$DW$100, DW_AT_external
	.dwattr $C$DW$100, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$100, DW_AT_TI_begin_line(0x12e)
	.dwattr $C$DW$100, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$100, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/IpcModule.c",line 303,column 1,is_stmt,address _IPCCpu2IpcReqMemAccess,isa 0

	.dwfde $C$DW$CIE, _IPCCpu2IpcReqMemAccess
;----------------------------------------------------------------------
; 302 | void IPCCpu2IpcReqMemAccess(void)                                      
; 305 | //!Give Memory Access to GS11 SARAM to CPU02                           
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCCpu2IpcReqMemAccess       FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_IPCCpu2IpcReqMemAccess:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/IpcModule.c",line 306,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 306 | while(!(MemCfgRegs.GSxMSEL.bit.MSEL_GS4))                              
;----------------------------------------------------------------------
        B         $C$L12,UNC            ; [CPU_] |306| 
        ; branch occurs ; [] |306| 
$C$L11:    
	.dwpsn	file "../source/IpcModule.c",line 308,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 308 | EALLOW;                                                                
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../source/IpcModule.c",line 309,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 309 | MemCfgRegs.GSxMSEL.bit.MSEL_GS4 = 1;                                   
;----------------------------------------------------------------------
        OR        @_MemCfgRegs+68,#0x0010 ; [CPU_] |309| 
	.dwpsn	file "../source/IpcModule.c",line 310,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 310 | EDIS;                                                                  
; 314 | //!Give Memory Access to GS11 SARAM to CPU02                           
;----------------------------------------------------------------------
 EDIS
$C$L12:    
	.dwpsn	file "../source/IpcModule.c",line 306,column 11,is_stmt,isa 0
        MOVW      DP,#_MemCfgRegs+68    ; [CPU_U] 
        TBIT      @_MemCfgRegs+68,#4    ; [CPU_] |306| 
        B         $C$L11,NTC            ; [CPU_] |306| 
        ; branchcc occurs ; [] |306| 
	.dwpsn	file "../source/IpcModule.c",line 315,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 315 | while(!(MemCfgRegs.GSxMSEL.bit.MSEL_GS11))                             
;----------------------------------------------------------------------
        B         $C$L14,UNC            ; [CPU_] |315| 
        ; branch occurs ; [] |315| 
$C$L13:    
	.dwpsn	file "../source/IpcModule.c",line 317,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 317 | EALLOW;                                                                
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../source/IpcModule.c",line 318,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 318 | MemCfgRegs.GSxMSEL.bit.MSEL_GS11 = 1;                                  
;----------------------------------------------------------------------
        OR        @_MemCfgRegs+68,#0x0800 ; [CPU_] |318| 
	.dwpsn	file "../source/IpcModule.c",line 319,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 319 | EDIS;                                                                  
; 322 | //!Give Memory Access to GS12 SARAM to CPU02                           
;----------------------------------------------------------------------
 EDIS
$C$L14:    
	.dwpsn	file "../source/IpcModule.c",line 315,column 11,is_stmt,isa 0
        TBIT      @_MemCfgRegs+68,#11   ; [CPU_] |315| 
        B         $C$L13,NTC            ; [CPU_] |315| 
        ; branchcc occurs ; [] |315| 
	.dwpsn	file "../source/IpcModule.c",line 323,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 323 | while(!(MemCfgRegs.GSxMSEL.bit.MSEL_GS12))                             
;----------------------------------------------------------------------
        B         $C$L16,UNC            ; [CPU_] |323| 
        ; branch occurs ; [] |323| 
$C$L15:    
	.dwpsn	file "../source/IpcModule.c",line 325,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 325 | EALLOW;                                                                
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../source/IpcModule.c",line 326,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 326 | MemCfgRegs.GSxMSEL.bit.MSEL_GS12 = 1;                                  
;----------------------------------------------------------------------
        OR        @_MemCfgRegs+68,#0x1000 ; [CPU_] |326| 
	.dwpsn	file "../source/IpcModule.c",line 327,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 327 | EDIS;                                                                  
; 331 | //!Give Memory Access to GS13 SARAM to CPU02                           
;----------------------------------------------------------------------
 EDIS
$C$L16:    
	.dwpsn	file "../source/IpcModule.c",line 323,column 11,is_stmt,isa 0
        TBIT      @_MemCfgRegs+68,#12   ; [CPU_] |323| 
        B         $C$L15,NTC            ; [CPU_] |323| 
        ; branchcc occurs ; [] |323| 
	.dwpsn	file "../source/IpcModule.c",line 332,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 332 | while(!(MemCfgRegs.GSxMSEL.bit.MSEL_GS13))                             
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |332| 
        ; branch occurs ; [] |332| 
$C$L17:    
	.dwpsn	file "../source/IpcModule.c",line 334,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 334 | EALLOW;                                                                
;----------------------------------------------------------------------
 EALLOW
	.dwpsn	file "../source/IpcModule.c",line 335,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 335 | MemCfgRegs.GSxMSEL.bit.MSEL_GS13 = 1;                                  
;----------------------------------------------------------------------
        OR        @_MemCfgRegs+68,#0x2000 ; [CPU_] |335| 
	.dwpsn	file "../source/IpcModule.c",line 336,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
; 336 | EDIS;                                                                  
;----------------------------------------------------------------------
 EDIS
$C$L18:    
	.dwpsn	file "../source/IpcModule.c",line 332,column 11,is_stmt,isa 0
        TBIT      @_MemCfgRegs+68,#13   ; [CPU_] |332| 
        B         $C$L17,NTC            ; [CPU_] |332| 
        ; branchcc occurs ; [] |332| 
	.dwpsn	file "../source/IpcModule.c",line 339,column 1,is_stmt,isa 0
        SPM       #0                    ; [CPU_] 
$C$DW$101	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$101, DW_AT_low_pc(0x00)
	.dwattr $C$DW$101, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$100, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$100, DW_AT_TI_end_line(0x153)
	.dwattr $C$DW$100, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$100

	.sect	".text:_IPCGetCpu1IpcParam"
	.clink
	.global	_IPCGetCpu1IpcParam

$C$DW$102	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$102, DW_AT_name("IPCGetCpu1IpcParam")
	.dwattr $C$DW$102, DW_AT_low_pc(_IPCGetCpu1IpcParam)
	.dwattr $C$DW$102, DW_AT_high_pc(0x00)
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_IPCGetCpu1IpcParam")
	.dwattr $C$DW$102, DW_AT_external
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$102, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$102, DW_AT_TI_begin_line(0x162)
	.dwattr $C$DW$102, DW_AT_TI_begin_column(0x12)
	.dwattr $C$DW$102, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/IpcModule.c",line 355,column 1,is_stmt,address _IPCGetCpu1IpcParam,isa 0

	.dwfde $C$DW$CIE, _IPCGetCpu1IpcParam
;----------------------------------------------------------------------
; 354 | stIpcParameters* IPCGetCpu1IpcParam(void)                              
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _IPCGetCpu1IpcParam           FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_IPCGetCpu1IpcParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/IpcModule.c",line 356,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 356 | return &Cpu1IpcParam;                                                  
;----------------------------------------------------------------------
        MOVL      XAR4,#_Cpu1IpcParam   ; [CPU_U] |356| 
	.dwpsn	file "../source/IpcModule.c",line 357,column 1,is_stmt,isa 0
$C$DW$103	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$103, DW_AT_low_pc(0x00)
	.dwattr $C$DW$103, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$102, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$102, DW_AT_TI_end_line(0x165)
	.dwattr $C$DW$102, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$102

	.sect	".text:_CPU02toCPU01IPC0IntHandler"
	.retain
	.retainrefs
	.global	_CPU02toCPU01IPC0IntHandler

$C$DW$104	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$104, DW_AT_name("CPU02toCPU01IPC0IntHandler")
	.dwattr $C$DW$104, DW_AT_low_pc(_CPU02toCPU01IPC0IntHandler)
	.dwattr $C$DW$104, DW_AT_high_pc(0x00)
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_CPU02toCPU01IPC0IntHandler")
	.dwattr $C$DW$104, DW_AT_external
	.dwattr $C$DW$104, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$104, DW_AT_TI_begin_line(0x17b)
	.dwattr $C$DW$104, DW_AT_TI_begin_column(0x12)
	.dwattr $C$DW$104, DW_AT_TI_interrupt
	.dwattr $C$DW$104, DW_AT_TI_max_frame_size(-34)
	.dwpsn	file "../source/IpcModule.c",line 380,column 1,is_stmt,address _CPU02toCPU01IPC0IntHandler,isa 0

	.dwfde $C$DW$CIE, _CPU02toCPU01IPC0IntHandler
;----------------------------------------------------------------------
; 379 | __interrupt void CPU02toCPU01IPC0IntHandler (void)                     
; 382 | tIpcMessage sMessage;                                                  
; 384 | //! Continue processing messages as long as CPU01 to CPU02 GetBuffer1 i
;     | s full                                                                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPU02toCPU01IPC0IntHandler   FR SIZE:  32           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto, 24 SOE     *
;***************************************************************

_CPU02toCPU01IPC0IntHandler:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        PUSH      AR1H:AR0H             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 5, 4
	.dwcfi	save_reg_to_mem, 7, 5
	.dwcfi	cfa_offset, -6
        MOVL      *SP++,XT              ; [CPU_] 
	.dwcfi	save_reg_to_mem, 21, 6
	.dwcfi	save_reg_to_mem, 22, 7
	.dwcfi	cfa_offset, -8
        MOVL      *SP++,XAR4            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 12, 8
	.dwcfi	save_reg_to_mem, 13, 9
	.dwcfi	cfa_offset, -10
        MOVL      *SP++,XAR5            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 14, 10
	.dwcfi	save_reg_to_mem, 15, 11
	.dwcfi	cfa_offset, -12
        MOVL      *SP++,XAR6            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 16, 12
	.dwcfi	save_reg_to_mem, 17, 13
	.dwcfi	cfa_offset, -14
        MOVL      *SP++,XAR7            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 18, 14
	.dwcfi	save_reg_to_mem, 19, 15
	.dwcfi	cfa_offset, -16
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 16
	.dwcfi	save_reg_to_mem, 40, 17
	.dwcfi	cfa_offset, -18
        MOV32     *SP++,R0H             ; [CPU_] 
        MOV32     *SP++,R1H             ; [CPU_] 
        MOV32     *SP++,R2H             ; [CPU_] 
        MOV32     *SP++,R3H             ; [CPU_] 
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -34
        SPM       0                     ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
$C$DW$105	.dwtag  DW_TAG_variable
	.dwattr $C$DW$105, DW_AT_name("sMessage")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_sMessage")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -8]

	.dwpsn	file "../source/IpcModule.c",line 385,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 385 | while (IpcGet(&g_sIpcController1, &sMessage, DISABLE_BLOCKING) != STATU
;     | S_FAIL)                                                                
; 387 |     switch (sMessage.ulcommand)                                        
; 390 |     case IPC_SET_BITS_PROTECTED:                                       
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |385| 
        ; branch occurs ; [] |385| 
$C$L19:    
	.dwpsn	file "../source/IpcModule.c",line 391,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 391 | IPCRtoLSetBits_Protected(&sMessage);       // Processes                
; 392 |                                            // IPCReqMemAccess()        
; 393 |                                            // function                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |391| 
        SUBB      XAR4,#8               ; [CPU_U] |391| 
        MOVZ      AR4,AR4               ; [CPU_] |391| 
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("_IPCRtoLSetBits_Protected")
	.dwattr $C$DW$106, DW_AT_TI_call

        LCR       #_IPCRtoLSetBits_Protected ; [CPU_] |391| 
        ; call occurs [#_IPCRtoLSetBits_Protected] ; [] |391| 
	.dwpsn	file "../source/IpcModule.c",line 394,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 394 | break;                                                                 
; 395 | case IPC_CLEAR_BITS_PROTECTED:                                         
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |394| 
        ; branch occurs ; [] |394| 
$C$L20:    
	.dwpsn	file "../source/IpcModule.c",line 396,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 396 | IPCRtoLClearBits_Protected(&sMessage);     // Processes                
; 397 |                                            // IPCReqMemAccess()        
; 398 |                                            // function                 
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |396| 
        SUBB      XAR4,#8               ; [CPU_U] |396| 
        MOVZ      AR4,AR4               ; [CPU_] |396| 
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_name("_IPCRtoLClearBits_Protected")
	.dwattr $C$DW$107, DW_AT_TI_call

        LCR       #_IPCRtoLClearBits_Protected ; [CPU_] |396| 
        ; call occurs [#_IPCRtoLClearBits_Protected] ; [] |396| 
	.dwpsn	file "../source/IpcModule.c",line 399,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 399 | break;                                                                 
; 400 | case IPC_BLOCK_WRITE:                                                  
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |399| 
        ; branch occurs ; [] |399| 
$C$L21:    
	.dwpsn	file "../source/IpcModule.c",line 401,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 401 | IPCRtoLBlockWrite(&sMessage);                                          
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |401| 
        SUBB      XAR4,#8               ; [CPU_U] |401| 
        MOVZ      AR4,AR4               ; [CPU_] |401| 
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_name("_IPCRtoLBlockWrite")
	.dwattr $C$DW$108, DW_AT_TI_call

        LCR       #_IPCRtoLBlockWrite   ; [CPU_] |401| 
        ; call occurs [#_IPCRtoLBlockWrite] ; [] |401| 
	.dwpsn	file "../source/IpcModule.c",line 402,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 402 | Cpu1IpcParam.LocalReceiveFlag = 1;                                     
;----------------------------------------------------------------------
        MOVW      DP,#_Cpu1IpcParam+41  ; [CPU_U] 
        MOVB      @_Cpu1IpcParam+41,#1,UNC ; [CPU_] |402| 
	.dwpsn	file "../source/IpcModule.c",line 403,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 403 | break;                                                                 
; 405 | case IPC_DATA_WRITE:                                                   
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |403| 
        ; branch occurs ; [] |403| 
$C$L22:    
	.dwpsn	file "../source/IpcModule.c",line 406,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 406 | IPCRtoLDataWrite(&sMessage);                                           
;----------------------------------------------------------------------
        MOVZ      AR4,SP                ; [CPU_] |406| 
        SUBB      XAR4,#8               ; [CPU_U] |406| 
        MOVZ      AR4,AR4               ; [CPU_] |406| 
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_name("_IPCRtoLDataWrite")
	.dwattr $C$DW$109, DW_AT_TI_call

        LCR       #_IPCRtoLDataWrite    ; [CPU_] |406| 
        ; call occurs [#_IPCRtoLDataWrite] ; [] |406| 
	.dwpsn	file "../source/IpcModule.c",line 407,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 407 | Cpu1IpcParam.LocalReceiveFlag = 1;                                     
;----------------------------------------------------------------------
        MOVW      DP,#_Cpu1IpcParam+41  ; [CPU_U] 
        MOVB      @_Cpu1IpcParam+41,#1,UNC ; [CPU_] |407| 
	.dwpsn	file "../source/IpcModule.c",line 408,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 408 | break;                                                                 
; 410 | default:                                                               
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |408| 
        ; branch occurs ; [] |408| 
$C$L23:    
	.dwpsn	file "../source/IpcModule.c",line 411,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 411 | Cpu1IpcParam.ErrFlag = 1;                                              
; 412 | //! Through Error msg here                                             
;----------------------------------------------------------------------
        MOVW      DP,#_Cpu1IpcParam+42  ; [CPU_U] 
        MOVB      @_Cpu1IpcParam+42,#1,UNC ; [CPU_] |411| 
	.dwpsn	file "../source/IpcModule.c",line 413,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 413 | break;                                                                 
; 416 | //!usnIPCStatusFlag = usnIPCStatusFlag+ 1;                             
; 417 | //! Acknowledge IPC INT0 Flag and PIE to receive more interrupts       
;----------------------------------------------------------------------
        B         $C$L25,UNC            ; [CPU_] |413| 
        ; branch occurs ; [] |413| 
$C$L24:    
	.dwpsn	file "../source/IpcModule.c",line 387,column 9,is_stmt,isa 0
        MOVL      XAR6,*-SP[8]          ; [CPU_] |387| 
        MOVL      XAR4,#65539           ; [CPU_U] |387| 
        MOVL      ACC,XAR4              ; [CPU_] |387| 
        CMPL      ACC,XAR6              ; [CPU_] |387| 
        B         $C$L22,EQ             ; [CPU_] |387| 
        ; branchcc occurs ; [] |387| 
        MOVL      XAR4,#65541           ; [CPU_U] |387| 
        MOVL      ACC,XAR4              ; [CPU_] |387| 
        CMPL      ACC,XAR6              ; [CPU_] |387| 
        B         $C$L21,EQ             ; [CPU_] |387| 
        ; branchcc occurs ; [] |387| 
        MOVL      XAR4,#65544           ; [CPU_U] |387| 
        MOVL      ACC,XAR4              ; [CPU_] |387| 
        CMPL      ACC,XAR6              ; [CPU_] |387| 
        B         $C$L19,EQ             ; [CPU_] |387| 
        ; branchcc occurs ; [] |387| 
        MOVL      XAR4,#65545           ; [CPU_U] |387| 
        MOVL      ACC,XAR4              ; [CPU_] |387| 
        CMPL      ACC,XAR6              ; [CPU_] |387| 
        B         $C$L20,EQ             ; [CPU_] |387| 
        ; branchcc occurs ; [] |387| 
        B         $C$L23,UNC            ; [CPU_] |387| 
        ; branch occurs ; [] |387| 
$C$L25:    
	.dwpsn	file "../source/IpcModule.c",line 385,column 12,is_stmt,isa 0
        MOVZ      AR4,SP                ; [CPU_] |385| 
        MOVB      AL,#0                 ; [CPU_] |385| 
        SUBB      XAR4,#8               ; [CPU_U] |385| 
        MOVZ      AR5,AR4               ; [CPU_] |385| 
        MOVL      XAR4,#_g_sIpcController1 ; [CPU_U] |385| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_IpcGet")
	.dwattr $C$DW$110, DW_AT_TI_call

        LCR       #_IpcGet              ; [CPU_] |385| 
        ; call occurs [#_IpcGet] ; [] |385| 
        CMPB      AL,#1                 ; [CPU_] |385| 
        B         $C$L24,NEQ            ; [CPU_] |385| 
        ; branchcc occurs ; [] |385| 
	.dwpsn	file "../source/IpcModule.c",line 418,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 418 | IpcRegs.IPCACK.bit.IPC0 = 1;                                           
;----------------------------------------------------------------------
        MOVW      DP,#_IpcRegs          ; [CPU_U] 
        OR        @_IpcRegs,#0x0001     ; [CPU_] |418| 
	.dwpsn	file "../source/IpcModule.c",line 419,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 419 | PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;                                
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        MOVB      @_PieCtrlRegs+1,#1,UNC ; [CPU_] |419| 
	.dwpsn	file "../source/IpcModule.c",line 421,column 1,is_stmt,isa 0
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -26
        MOV32     R3H,*--SP             ; [CPU_] 
        MOV32     R2H,*--SP             ; [CPU_] 
        MOV32     R1H,*--SP             ; [CPU_] 
        MOV32     R0H,*--SP             ; [CPU_] 
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -16
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        MOVL      XAR7,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -14
	.dwcfi	restore_reg, 18
        MOVL      XAR6,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -12
	.dwcfi	restore_reg, 16
        MOVL      XAR5,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -10
	.dwcfi	restore_reg, 14
        MOVL      XAR4,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -8
	.dwcfi	restore_reg, 12
        MOVL      XT,*--SP              ; [CPU_] 
	.dwcfi	cfa_offset, -6
	.dwcfi	restore_reg, 22
	.dwcfi	restore_reg, 21
        POP       AR1H:AR0H             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 5
	.dwcfi	restore_reg, 7
        SETC      INTM, DBGM            ; [CPU_] 
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$111	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$111, DW_AT_low_pc(0x00)
	.dwattr $C$DW$111, DW_AT_TI_return

        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$104, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$104, DW_AT_TI_end_line(0x1a5)
	.dwattr $C$DW$104, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$104

	.sect	".text:_CPU02toCPU01IPC1IntHandler"
	.retain
	.retainrefs
	.global	_CPU02toCPU01IPC1IntHandler

$C$DW$112	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$112, DW_AT_name("CPU02toCPU01IPC1IntHandler")
	.dwattr $C$DW$112, DW_AT_low_pc(_CPU02toCPU01IPC1IntHandler)
	.dwattr $C$DW$112, DW_AT_high_pc(0x00)
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_CPU02toCPU01IPC1IntHandler")
	.dwattr $C$DW$112, DW_AT_external
	.dwattr $C$DW$112, DW_AT_TI_begin_file("../source/IpcModule.c")
	.dwattr $C$DW$112, DW_AT_TI_begin_line(0x1b8)
	.dwattr $C$DW$112, DW_AT_TI_begin_column(0x12)
	.dwattr $C$DW$112, DW_AT_TI_interrupt
	.dwattr $C$DW$112, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/IpcModule.c",line 441,column 1,is_stmt,address _CPU02toCPU01IPC1IntHandler,isa 0

	.dwfde $C$DW$CIE, _CPU02toCPU01IPC1IntHandler
;----------------------------------------------------------------------
; 440 | __interrupt void CPU02toCPU01IPC1IntHandler (void)                     
; 442 | //! Should never reach here - Placeholder for Debug                    
; 443 | //! Acknowledge IPC INT1 Flag and PIE to receive more interrupts       
; 445 | //printf("\n CPU-01 IPC INT 1 Handler ");                              
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CPU02toCPU01IPC1IntHandler   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  4 SOE     *
;***************************************************************

_CPU02toCPU01IPC1IntHandler:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 4
	.dwcfi	save_reg_to_mem, 40, 5
	.dwcfi	cfa_offset, -6
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
	.dwpsn	file "../source/IpcModule.c",line 446,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 446 | Cpu1IpcParam.INT1Flag = 1;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_Cpu1IpcParam+43  ; [CPU_U] 
        MOVB      @_Cpu1IpcParam+43,#1,UNC ; [CPU_] |446| 
	.dwpsn	file "../source/IpcModule.c",line 447,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 447 | IpcRegs.IPCACK.bit.IPC1 = 1;                                           
;----------------------------------------------------------------------
        MOVW      DP,#_IpcRegs          ; [CPU_U] 
        OR        @_IpcRegs,#0x0002     ; [CPU_] |447| 
	.dwpsn	file "../source/IpcModule.c",line 448,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 448 | PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;                                
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        MOVB      @_PieCtrlRegs+1,#1,UNC ; [CPU_] |448| 
	.dwpsn	file "../source/IpcModule.c",line 449,column 2,is_stmt,isa 0
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$113	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$113, DW_AT_low_pc(0x00)
	.dwattr $C$DW$113, DW_AT_TI_return

        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$112, DW_AT_TI_end_file("../source/IpcModule.c")
	.dwattr $C$DW$112, DW_AT_TI_end_line(0x1c1)
	.dwattr $C$DW$112, DW_AT_TI_end_column(0x02)
	.dwendentry
	.dwendtag $C$DW$112

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_IPCRtoLBlockWrite
	.global	_F28x_usDelay
	.global	_IPCInitialize
	.global	_SendIpcFlag
	.global	_IpcSync
	.global	_InitIpc
	.global	_IPCRtoLSetBits_Protected
	.global	_IPCRtoLDataWrite
	.global	_IPCRtoLClearBits_Protected
	.global	_IPCLtoRBlockWrite
	.global	_IpcGet
	.global	_IPCLtoRDataWrite
	.global	_PieCtrlRegs
	.global	_IpcRegs
	.global	_MemCfgRegs
	.global	_PieVectTable

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x08)
$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$114, DW_AT_name("ulcommand")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_ulcommand")
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$115, DW_AT_name("uladdress")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_uladdress")
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$116, DW_AT_name("uldataw1")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_uldataw1")
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$117, DW_AT_name("uldataw2")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_uldataw2")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$20

$C$DW$T$21	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$21, DW_AT_name("tIpcMessage")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_address_class(0x20)


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x0e)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$118, DW_AT_name("psPutBuffer")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_psPutBuffer")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$119, DW_AT_name("ulPutFlag")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_ulPutFlag")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$120, DW_AT_name("pusPutWriteIndex")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_pusPutWriteIndex")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$121, DW_AT_name("pusPutReadIndex")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_pusPutReadIndex")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$122, DW_AT_name("psGetBuffer")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_psGetBuffer")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$123, DW_AT_name("pusGetWriteIndex")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_pusGetWriteIndex")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$124, DW_AT_name("pusGetReadIndex")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_pusGetReadIndex")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$25

$C$DW$T$160	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$160, DW_AT_name("tIpcController")
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$160, DW_AT_language(DW_LANG_C)

$C$DW$125	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$160)

$C$DW$T$161	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$161, DW_AT_type(*$C$DW$125)

$C$DW$T$162	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$T$162, DW_AT_address_class(0x20)


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_name("DxACCPROT0_BITS")
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x02)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$126, DW_AT_name("rsvd1")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$126, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$126, DW_AT_bit_size(0x10)
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$127, DW_AT_name("FETCHPROT_D0")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_FETCHPROT_D0")
	.dwattr $C$DW$127, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$127, DW_AT_bit_size(0x01)
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$128, DW_AT_name("CPUWRPROT_D0")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_CPUWRPROT_D0")
	.dwattr $C$DW$128, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$128, DW_AT_bit_size(0x01)
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$129, DW_AT_name("rsvd2")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$129, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$129, DW_AT_bit_size(0x06)
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$130, DW_AT_name("FETCHPROT_D1")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_FETCHPROT_D1")
	.dwattr $C$DW$130, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$130, DW_AT_bit_size(0x01)
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$131, DW_AT_name("CPUWRPROT_D1")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_CPUWRPROT_D1")
	.dwattr $C$DW$131, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$131, DW_AT_bit_size(0x01)
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$132, DW_AT_name("rsvd3")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$132, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$132, DW_AT_bit_size(0x06)
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$27


$C$DW$T$29	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$29, DW_AT_name("DxACCPROT0_REG")
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x02)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$133, DW_AT_name("all")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$134, DW_AT_name("bit")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_name("DxCOMMIT_BITS")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x02)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$135, DW_AT_name("rsvd1")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$135, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$135, DW_AT_bit_size(0x02)
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$136, DW_AT_name("COMMIT_D0")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_COMMIT_D0")
	.dwattr $C$DW$136, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$136, DW_AT_bit_size(0x01)
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$137, DW_AT_name("COMMIT_D1")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_COMMIT_D1")
	.dwattr $C$DW$137, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$137, DW_AT_bit_size(0x01)
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$138, DW_AT_name("rsvd2")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$138, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$138, DW_AT_bit_size(0x0c)
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$139, DW_AT_name("rsvd3")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$139, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$139, DW_AT_bit_size(0x10)
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$31, DW_AT_name("DxCOMMIT_REG")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x02)
$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$140, DW_AT_name("all")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$141, DW_AT_name("bit")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$32, DW_AT_name("DxINITDONE_BITS")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x02)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$142, DW_AT_name("INITDONE_M0")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_INITDONE_M0")
	.dwattr $C$DW$142, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$142, DW_AT_bit_size(0x01)
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$143, DW_AT_name("INITDONE_M1")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_INITDONE_M1")
	.dwattr $C$DW$143, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$143, DW_AT_bit_size(0x01)
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$144, DW_AT_name("INITDONE_D0")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_INITDONE_D0")
	.dwattr $C$DW$144, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$144, DW_AT_bit_size(0x01)
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$145, DW_AT_name("INITDONE_D1")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_INITDONE_D1")
	.dwattr $C$DW$145, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$145, DW_AT_bit_size(0x01)
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$146, DW_AT_name("rsvd1")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$146, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$146, DW_AT_bit_size(0x0c)
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$147, DW_AT_name("rsvd2")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$147, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$147, DW_AT_bit_size(0x10)
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$33, DW_AT_name("DxINITDONE_REG")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x02)
$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$148, DW_AT_name("all")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$149, DW_AT_name("bit")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$34, DW_AT_name("DxINIT_BITS")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x02)
$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$150, DW_AT_name("INIT_M0")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_INIT_M0")
	.dwattr $C$DW$150, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$150, DW_AT_bit_size(0x01)
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$151, DW_AT_name("INIT_M1")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_INIT_M1")
	.dwattr $C$DW$151, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$151, DW_AT_bit_size(0x01)
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$152, DW_AT_name("INIT_D0")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_INIT_D0")
	.dwattr $C$DW$152, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$152, DW_AT_bit_size(0x01)
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$153, DW_AT_name("INIT_D1")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_INIT_D1")
	.dwattr $C$DW$153, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$153, DW_AT_bit_size(0x01)
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$154, DW_AT_name("rsvd1")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$154, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$154, DW_AT_bit_size(0x0c)
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$155, DW_AT_name("rsvd2")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$155, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$155, DW_AT_bit_size(0x10)
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$35, DW_AT_name("DxINIT_REG")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x02)
$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$156, DW_AT_name("all")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$157, DW_AT_name("bit")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$35


$C$DW$T$36	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$36, DW_AT_name("DxLOCK_BITS")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$158, DW_AT_name("rsvd1")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$158, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$158, DW_AT_bit_size(0x02)
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$159, DW_AT_name("LOCK_D0")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_LOCK_D0")
	.dwattr $C$DW$159, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$159, DW_AT_bit_size(0x01)
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$160, DW_AT_name("LOCK_D1")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_LOCK_D1")
	.dwattr $C$DW$160, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$160, DW_AT_bit_size(0x01)
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$161, DW_AT_name("rsvd2")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$161, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$161, DW_AT_bit_size(0x0c)
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$162	.dwtag  DW_TAG_member
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$162, DW_AT_name("rsvd3")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$162, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$162, DW_AT_bit_size(0x10)
	.dwattr $C$DW$162, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$36


$C$DW$T$37	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$37, DW_AT_name("DxLOCK_REG")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x02)
$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$163, DW_AT_name("all")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$164, DW_AT_name("bit")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$37


$C$DW$T$38	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$38, DW_AT_name("DxTEST_BITS")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x02)
$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$165, DW_AT_name("TEST_M0")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_TEST_M0")
	.dwattr $C$DW$165, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$165, DW_AT_bit_size(0x02)
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$166, DW_AT_name("TEST_M1")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_TEST_M1")
	.dwattr $C$DW$166, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$166, DW_AT_bit_size(0x02)
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$167, DW_AT_name("TEST_D0")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_TEST_D0")
	.dwattr $C$DW$167, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$167, DW_AT_bit_size(0x02)
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$168, DW_AT_name("TEST_D1")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_TEST_D1")
	.dwattr $C$DW$168, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$168, DW_AT_bit_size(0x02)
	.dwattr $C$DW$168, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$169, DW_AT_name("rsvd1")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$169, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$169, DW_AT_bit_size(0x08)
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$170, DW_AT_name("rsvd2")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$170, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$170, DW_AT_bit_size(0x10)
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$38


$C$DW$T$39	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$39, DW_AT_name("DxTEST_REG")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x02)
$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$171, DW_AT_name("all")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$172, DW_AT_name("bit")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$39


$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_name("GSxACCPROT0_BITS")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x02)
$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$173, DW_AT_name("FETCHPROT_GS0")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_FETCHPROT_GS0")
	.dwattr $C$DW$173, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$173, DW_AT_bit_size(0x01)
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$174, DW_AT_name("CPUWRPROT_GS0")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_CPUWRPROT_GS0")
	.dwattr $C$DW$174, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$174, DW_AT_bit_size(0x01)
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$175, DW_AT_name("DMAWRPROT_GS0")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_DMAWRPROT_GS0")
	.dwattr $C$DW$175, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$175, DW_AT_bit_size(0x01)
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$176, DW_AT_name("rsvd1")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$176, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$176, DW_AT_bit_size(0x05)
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$177, DW_AT_name("FETCHPROT_GS1")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_FETCHPROT_GS1")
	.dwattr $C$DW$177, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$177, DW_AT_bit_size(0x01)
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$178, DW_AT_name("CPUWRPROT_GS1")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_CPUWRPROT_GS1")
	.dwattr $C$DW$178, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$178, DW_AT_bit_size(0x01)
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$179, DW_AT_name("DMAWRPROT_GS1")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_DMAWRPROT_GS1")
	.dwattr $C$DW$179, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$179, DW_AT_bit_size(0x01)
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$180, DW_AT_name("rsvd2")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$180, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$180, DW_AT_bit_size(0x05)
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$181, DW_AT_name("FETCHPROT_GS2")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_FETCHPROT_GS2")
	.dwattr $C$DW$181, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$181, DW_AT_bit_size(0x01)
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$182, DW_AT_name("CPUWRPROT_GS2")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_CPUWRPROT_GS2")
	.dwattr $C$DW$182, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$182, DW_AT_bit_size(0x01)
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$183, DW_AT_name("DMAWRPROT_GS2")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_DMAWRPROT_GS2")
	.dwattr $C$DW$183, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$183, DW_AT_bit_size(0x01)
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$184, DW_AT_name("rsvd3")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$184, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$184, DW_AT_bit_size(0x05)
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$185, DW_AT_name("FETCHPROT_GS3")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_FETCHPROT_GS3")
	.dwattr $C$DW$185, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$185, DW_AT_bit_size(0x01)
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$186, DW_AT_name("CPUWRPROT_GS3")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_CPUWRPROT_GS3")
	.dwattr $C$DW$186, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$186, DW_AT_bit_size(0x01)
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$187, DW_AT_name("DMAWRPROT_GS3")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_DMAWRPROT_GS3")
	.dwattr $C$DW$187, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$187, DW_AT_bit_size(0x01)
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$188, DW_AT_name("rsvd4")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$188, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$188, DW_AT_bit_size(0x05)
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$40


$C$DW$T$41	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$41, DW_AT_name("GSxACCPROT0_REG")
	.dwattr $C$DW$T$41, DW_AT_byte_size(0x02)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$189, DW_AT_name("all")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$190, DW_AT_name("bit")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$41


$C$DW$T$42	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$42, DW_AT_name("GSxACCPROT1_BITS")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x02)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$191, DW_AT_name("FETCHPROT_GS4")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_FETCHPROT_GS4")
	.dwattr $C$DW$191, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$191, DW_AT_bit_size(0x01)
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$192, DW_AT_name("CPUWRPROT_GS4")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_CPUWRPROT_GS4")
	.dwattr $C$DW$192, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$192, DW_AT_bit_size(0x01)
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$193	.dwtag  DW_TAG_member
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$193, DW_AT_name("DMAWRPROT_GS4")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_DMAWRPROT_GS4")
	.dwattr $C$DW$193, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$193, DW_AT_bit_size(0x01)
	.dwattr $C$DW$193, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$193, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$194, DW_AT_name("rsvd1")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$194, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$194, DW_AT_bit_size(0x05)
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$195, DW_AT_name("FETCHPROT_GS5")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_FETCHPROT_GS5")
	.dwattr $C$DW$195, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$195, DW_AT_bit_size(0x01)
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$196, DW_AT_name("CPUWRPROT_GS5")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_CPUWRPROT_GS5")
	.dwattr $C$DW$196, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$196, DW_AT_bit_size(0x01)
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$197	.dwtag  DW_TAG_member
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$197, DW_AT_name("DMAWRPROT_GS5")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_DMAWRPROT_GS5")
	.dwattr $C$DW$197, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$197, DW_AT_bit_size(0x01)
	.dwattr $C$DW$197, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$197, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$198, DW_AT_name("rsvd2")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$198, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$198, DW_AT_bit_size(0x05)
	.dwattr $C$DW$198, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$199, DW_AT_name("FETCHPROT_GS6")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_FETCHPROT_GS6")
	.dwattr $C$DW$199, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$199, DW_AT_bit_size(0x01)
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$200	.dwtag  DW_TAG_member
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$200, DW_AT_name("CPUWRPROT_GS6")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_CPUWRPROT_GS6")
	.dwattr $C$DW$200, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$200, DW_AT_bit_size(0x01)
	.dwattr $C$DW$200, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$201	.dwtag  DW_TAG_member
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$201, DW_AT_name("DMAWRPROT_GS6")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_DMAWRPROT_GS6")
	.dwattr $C$DW$201, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$201, DW_AT_bit_size(0x01)
	.dwattr $C$DW$201, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$201, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$202	.dwtag  DW_TAG_member
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$202, DW_AT_name("rsvd3")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$202, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$202, DW_AT_bit_size(0x05)
	.dwattr $C$DW$202, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$203	.dwtag  DW_TAG_member
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$203, DW_AT_name("FETCHPROT_GS7")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_FETCHPROT_GS7")
	.dwattr $C$DW$203, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$203, DW_AT_bit_size(0x01)
	.dwattr $C$DW$203, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$203, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$204	.dwtag  DW_TAG_member
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$204, DW_AT_name("CPUWRPROT_GS7")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_CPUWRPROT_GS7")
	.dwattr $C$DW$204, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$204, DW_AT_bit_size(0x01)
	.dwattr $C$DW$204, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$204, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$205	.dwtag  DW_TAG_member
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$205, DW_AT_name("DMAWRPROT_GS7")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_DMAWRPROT_GS7")
	.dwattr $C$DW$205, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$205, DW_AT_bit_size(0x01)
	.dwattr $C$DW$205, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$205, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$206	.dwtag  DW_TAG_member
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$206, DW_AT_name("rsvd4")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$206, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$206, DW_AT_bit_size(0x05)
	.dwattr $C$DW$206, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$42


$C$DW$T$43	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$43, DW_AT_name("GSxACCPROT1_REG")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x02)
$C$DW$207	.dwtag  DW_TAG_member
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$207, DW_AT_name("all")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$207, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$208	.dwtag  DW_TAG_member
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$208, DW_AT_name("bit")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$208, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$208, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$43


$C$DW$T$44	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$44, DW_AT_name("GSxACCPROT2_BITS")
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x02)
$C$DW$209	.dwtag  DW_TAG_member
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$209, DW_AT_name("FETCHPROT_GS8")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_FETCHPROT_GS8")
	.dwattr $C$DW$209, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$209, DW_AT_bit_size(0x01)
	.dwattr $C$DW$209, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$210	.dwtag  DW_TAG_member
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$210, DW_AT_name("CPUWRPROT_GS8")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_CPUWRPROT_GS8")
	.dwattr $C$DW$210, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$210, DW_AT_bit_size(0x01)
	.dwattr $C$DW$210, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$210, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$211	.dwtag  DW_TAG_member
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$211, DW_AT_name("DMAWRPROT_GS8")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_DMAWRPROT_GS8")
	.dwattr $C$DW$211, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$211, DW_AT_bit_size(0x01)
	.dwattr $C$DW$211, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$211, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$212	.dwtag  DW_TAG_member
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$212, DW_AT_name("rsvd1")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$212, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$212, DW_AT_bit_size(0x05)
	.dwattr $C$DW$212, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$212, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$213	.dwtag  DW_TAG_member
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$213, DW_AT_name("FETCHPROT_GS9")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_FETCHPROT_GS9")
	.dwattr $C$DW$213, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$213, DW_AT_bit_size(0x01)
	.dwattr $C$DW$213, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$213, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$214	.dwtag  DW_TAG_member
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$214, DW_AT_name("CPUWRPROT_GS9")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_CPUWRPROT_GS9")
	.dwattr $C$DW$214, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$214, DW_AT_bit_size(0x01)
	.dwattr $C$DW$214, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$214, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$215	.dwtag  DW_TAG_member
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$215, DW_AT_name("DMAWRPROT_GS9")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_DMAWRPROT_GS9")
	.dwattr $C$DW$215, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$215, DW_AT_bit_size(0x01)
	.dwattr $C$DW$215, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$215, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$216	.dwtag  DW_TAG_member
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$216, DW_AT_name("rsvd2")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$216, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$216, DW_AT_bit_size(0x05)
	.dwattr $C$DW$216, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$216, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$217	.dwtag  DW_TAG_member
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$217, DW_AT_name("FETCHPROT_GS10")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_FETCHPROT_GS10")
	.dwattr $C$DW$217, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$217, DW_AT_bit_size(0x01)
	.dwattr $C$DW$217, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$217, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$218	.dwtag  DW_TAG_member
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$218, DW_AT_name("CPUWRPROT_GS10")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_CPUWRPROT_GS10")
	.dwattr $C$DW$218, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$218, DW_AT_bit_size(0x01)
	.dwattr $C$DW$218, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$218, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$219	.dwtag  DW_TAG_member
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$219, DW_AT_name("DMAWRPROT_GS10")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_DMAWRPROT_GS10")
	.dwattr $C$DW$219, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$219, DW_AT_bit_size(0x01)
	.dwattr $C$DW$219, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$219, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$220	.dwtag  DW_TAG_member
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$220, DW_AT_name("rsvd3")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$220, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$220, DW_AT_bit_size(0x05)
	.dwattr $C$DW$220, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$220, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$221	.dwtag  DW_TAG_member
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$221, DW_AT_name("FETCHPROT_GS11")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_FETCHPROT_GS11")
	.dwattr $C$DW$221, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$221, DW_AT_bit_size(0x01)
	.dwattr $C$DW$221, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$221, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$222	.dwtag  DW_TAG_member
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$222, DW_AT_name("CPUWRPROT_GS11")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_CPUWRPROT_GS11")
	.dwattr $C$DW$222, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$222, DW_AT_bit_size(0x01)
	.dwattr $C$DW$222, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$222, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$223	.dwtag  DW_TAG_member
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$223, DW_AT_name("DMAWRPROT_GS11")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_DMAWRPROT_GS11")
	.dwattr $C$DW$223, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$223, DW_AT_bit_size(0x01)
	.dwattr $C$DW$223, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$223, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$224	.dwtag  DW_TAG_member
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$224, DW_AT_name("rsvd4")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$224, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$224, DW_AT_bit_size(0x05)
	.dwattr $C$DW$224, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$224, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$44


$C$DW$T$45	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$45, DW_AT_name("GSxACCPROT2_REG")
	.dwattr $C$DW$T$45, DW_AT_byte_size(0x02)
$C$DW$225	.dwtag  DW_TAG_member
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$225, DW_AT_name("all")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$225, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$225, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$226	.dwtag  DW_TAG_member
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$226, DW_AT_name("bit")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$226, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$226, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$45


$C$DW$T$46	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$46, DW_AT_name("GSxACCPROT3_BITS")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x02)
$C$DW$227	.dwtag  DW_TAG_member
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$227, DW_AT_name("FETCHPROT_GS12")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_FETCHPROT_GS12")
	.dwattr $C$DW$227, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$227, DW_AT_bit_size(0x01)
	.dwattr $C$DW$227, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$227, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$228	.dwtag  DW_TAG_member
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$228, DW_AT_name("CPUWRPROT_GS12")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_CPUWRPROT_GS12")
	.dwattr $C$DW$228, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$228, DW_AT_bit_size(0x01)
	.dwattr $C$DW$228, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$228, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$229	.dwtag  DW_TAG_member
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$229, DW_AT_name("DMAWRPROT_GS12")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_DMAWRPROT_GS12")
	.dwattr $C$DW$229, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$229, DW_AT_bit_size(0x01)
	.dwattr $C$DW$229, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$229, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$230	.dwtag  DW_TAG_member
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$230, DW_AT_name("rsvd1")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$230, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$230, DW_AT_bit_size(0x05)
	.dwattr $C$DW$230, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$230, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$231, DW_AT_name("FETCHPROT_GS13")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_FETCHPROT_GS13")
	.dwattr $C$DW$231, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$231, DW_AT_bit_size(0x01)
	.dwattr $C$DW$231, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$232	.dwtag  DW_TAG_member
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$232, DW_AT_name("CPUWRPROT_GS13")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_CPUWRPROT_GS13")
	.dwattr $C$DW$232, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$232, DW_AT_bit_size(0x01)
	.dwattr $C$DW$232, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$233, DW_AT_name("DMAWRPROT_GS13")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_DMAWRPROT_GS13")
	.dwattr $C$DW$233, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$233, DW_AT_bit_size(0x01)
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$234	.dwtag  DW_TAG_member
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$234, DW_AT_name("rsvd2")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$234, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$234, DW_AT_bit_size(0x05)
	.dwattr $C$DW$234, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$235, DW_AT_name("FETCHPROT_GS14")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_FETCHPROT_GS14")
	.dwattr $C$DW$235, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$235, DW_AT_bit_size(0x01)
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$236	.dwtag  DW_TAG_member
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$236, DW_AT_name("CPUWRPROT_GS14")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_CPUWRPROT_GS14")
	.dwattr $C$DW$236, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$236, DW_AT_bit_size(0x01)
	.dwattr $C$DW$236, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$236, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$237, DW_AT_name("DMAWRPROT_GS14")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_DMAWRPROT_GS14")
	.dwattr $C$DW$237, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$237, DW_AT_bit_size(0x01)
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$238, DW_AT_name("rsvd3")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$238, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$238, DW_AT_bit_size(0x05)
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$239, DW_AT_name("FETCHPROT_GS15")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_FETCHPROT_GS15")
	.dwattr $C$DW$239, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$239, DW_AT_bit_size(0x01)
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$240	.dwtag  DW_TAG_member
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$240, DW_AT_name("CPUWRPROT_GS15")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_CPUWRPROT_GS15")
	.dwattr $C$DW$240, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$240, DW_AT_bit_size(0x01)
	.dwattr $C$DW$240, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$240, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$241, DW_AT_name("DMAWRPROT_GS15")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_DMAWRPROT_GS15")
	.dwattr $C$DW$241, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$241, DW_AT_bit_size(0x01)
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$242, DW_AT_name("rsvd4")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$242, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$242, DW_AT_bit_size(0x05)
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$46


$C$DW$T$47	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$47, DW_AT_name("GSxACCPROT3_REG")
	.dwattr $C$DW$T$47, DW_AT_byte_size(0x02)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$243, DW_AT_name("all")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$244, DW_AT_name("bit")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$47


$C$DW$T$48	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$48, DW_AT_name("GSxCOMMIT_BITS")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x02)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$245, DW_AT_name("COMMIT_GS0")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_COMMIT_GS0")
	.dwattr $C$DW$245, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$245, DW_AT_bit_size(0x01)
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$246, DW_AT_name("COMMIT_GS1")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_COMMIT_GS1")
	.dwattr $C$DW$246, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$246, DW_AT_bit_size(0x01)
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$247, DW_AT_name("COMMIT_GS2")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_COMMIT_GS2")
	.dwattr $C$DW$247, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$247, DW_AT_bit_size(0x01)
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$248, DW_AT_name("COMMIT_GS3")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_COMMIT_GS3")
	.dwattr $C$DW$248, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$248, DW_AT_bit_size(0x01)
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$249, DW_AT_name("COMMIT_GS4")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_COMMIT_GS4")
	.dwattr $C$DW$249, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$249, DW_AT_bit_size(0x01)
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$250, DW_AT_name("COMMIT_GS5")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_COMMIT_GS5")
	.dwattr $C$DW$250, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$250, DW_AT_bit_size(0x01)
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$251, DW_AT_name("COMMIT_GS6")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_COMMIT_GS6")
	.dwattr $C$DW$251, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$251, DW_AT_bit_size(0x01)
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$252, DW_AT_name("COMMIT_GS7")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_COMMIT_GS7")
	.dwattr $C$DW$252, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$252, DW_AT_bit_size(0x01)
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$253, DW_AT_name("COMMIT_GS8")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_COMMIT_GS8")
	.dwattr $C$DW$253, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$253, DW_AT_bit_size(0x01)
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$254, DW_AT_name("COMMIT_GS9")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_COMMIT_GS9")
	.dwattr $C$DW$254, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$254, DW_AT_bit_size(0x01)
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$255, DW_AT_name("COMMIT_GS10")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_COMMIT_GS10")
	.dwattr $C$DW$255, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$255, DW_AT_bit_size(0x01)
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$256, DW_AT_name("COMMIT_GS11")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_COMMIT_GS11")
	.dwattr $C$DW$256, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$256, DW_AT_bit_size(0x01)
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$257, DW_AT_name("COMMIT_GS12")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_COMMIT_GS12")
	.dwattr $C$DW$257, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$257, DW_AT_bit_size(0x01)
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$258, DW_AT_name("COMMIT_GS13")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_COMMIT_GS13")
	.dwattr $C$DW$258, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$258, DW_AT_bit_size(0x01)
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$259, DW_AT_name("COMMIT_GS14")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_COMMIT_GS14")
	.dwattr $C$DW$259, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$259, DW_AT_bit_size(0x01)
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$260, DW_AT_name("COMMIT_GS15")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_COMMIT_GS15")
	.dwattr $C$DW$260, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$260, DW_AT_bit_size(0x01)
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$261, DW_AT_name("rsvd1")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$261, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$261, DW_AT_bit_size(0x10)
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$48


$C$DW$T$49	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$49, DW_AT_name("GSxCOMMIT_REG")
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x02)
$C$DW$262	.dwtag  DW_TAG_member
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$262, DW_AT_name("all")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$262, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$262, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$263, DW_AT_name("bit")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$49


$C$DW$T$50	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$50, DW_AT_name("GSxINITDONE_BITS")
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x02)
$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$264, DW_AT_name("INITDONE_GS0")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_INITDONE_GS0")
	.dwattr $C$DW$264, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$264, DW_AT_bit_size(0x01)
	.dwattr $C$DW$264, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$265, DW_AT_name("INITDONE_GS1")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_INITDONE_GS1")
	.dwattr $C$DW$265, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$265, DW_AT_bit_size(0x01)
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$266	.dwtag  DW_TAG_member
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$266, DW_AT_name("INITDONE_GS2")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_INITDONE_GS2")
	.dwattr $C$DW$266, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$266, DW_AT_bit_size(0x01)
	.dwattr $C$DW$266, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$266, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$267	.dwtag  DW_TAG_member
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$267, DW_AT_name("INITDONE_GS3")
	.dwattr $C$DW$267, DW_AT_TI_symbol_name("_INITDONE_GS3")
	.dwattr $C$DW$267, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$267, DW_AT_bit_size(0x01)
	.dwattr $C$DW$267, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$267, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$268	.dwtag  DW_TAG_member
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$268, DW_AT_name("INITDONE_GS4")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_INITDONE_GS4")
	.dwattr $C$DW$268, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$268, DW_AT_bit_size(0x01)
	.dwattr $C$DW$268, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$268, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$269	.dwtag  DW_TAG_member
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$269, DW_AT_name("INITDONE_GS5")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_INITDONE_GS5")
	.dwattr $C$DW$269, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$269, DW_AT_bit_size(0x01)
	.dwattr $C$DW$269, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$269, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$270, DW_AT_name("INITDONE_GS6")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_INITDONE_GS6")
	.dwattr $C$DW$270, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$270, DW_AT_bit_size(0x01)
	.dwattr $C$DW$270, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$271	.dwtag  DW_TAG_member
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$271, DW_AT_name("INITDONE_GS7")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_INITDONE_GS7")
	.dwattr $C$DW$271, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$271, DW_AT_bit_size(0x01)
	.dwattr $C$DW$271, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$272	.dwtag  DW_TAG_member
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$272, DW_AT_name("INITDONE_GS8")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_INITDONE_GS8")
	.dwattr $C$DW$272, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$272, DW_AT_bit_size(0x01)
	.dwattr $C$DW$272, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$272, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$273	.dwtag  DW_TAG_member
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$273, DW_AT_name("INITDONE_GS9")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_INITDONE_GS9")
	.dwattr $C$DW$273, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$273, DW_AT_bit_size(0x01)
	.dwattr $C$DW$273, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$274	.dwtag  DW_TAG_member
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$274, DW_AT_name("INITDONE_GS10")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_INITDONE_GS10")
	.dwattr $C$DW$274, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$274, DW_AT_bit_size(0x01)
	.dwattr $C$DW$274, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$274, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$275	.dwtag  DW_TAG_member
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$275, DW_AT_name("INITDONE_GS11")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_INITDONE_GS11")
	.dwattr $C$DW$275, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$275, DW_AT_bit_size(0x01)
	.dwattr $C$DW$275, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$275, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$276	.dwtag  DW_TAG_member
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$276, DW_AT_name("INITDONE_GS12")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_INITDONE_GS12")
	.dwattr $C$DW$276, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$276, DW_AT_bit_size(0x01)
	.dwattr $C$DW$276, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$276, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$277	.dwtag  DW_TAG_member
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$277, DW_AT_name("INITDONE_GS13")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_INITDONE_GS13")
	.dwattr $C$DW$277, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$277, DW_AT_bit_size(0x01)
	.dwattr $C$DW$277, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$277, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$278	.dwtag  DW_TAG_member
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$278, DW_AT_name("INITDONE_GS14")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_INITDONE_GS14")
	.dwattr $C$DW$278, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$278, DW_AT_bit_size(0x01)
	.dwattr $C$DW$278, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$278, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$279	.dwtag  DW_TAG_member
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$279, DW_AT_name("INITDONE_GS15")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_INITDONE_GS15")
	.dwattr $C$DW$279, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$279, DW_AT_bit_size(0x01)
	.dwattr $C$DW$279, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$279, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$280	.dwtag  DW_TAG_member
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$280, DW_AT_name("rsvd1")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$280, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$280, DW_AT_bit_size(0x10)
	.dwattr $C$DW$280, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$280, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$50


$C$DW$T$51	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$51, DW_AT_name("GSxINITDONE_REG")
	.dwattr $C$DW$T$51, DW_AT_byte_size(0x02)
$C$DW$281	.dwtag  DW_TAG_member
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$281, DW_AT_name("all")
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$281, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$281, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$282	.dwtag  DW_TAG_member
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$282, DW_AT_name("bit")
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$282, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$282, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$51


$C$DW$T$52	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$52, DW_AT_name("GSxINIT_BITS")
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x02)
$C$DW$283	.dwtag  DW_TAG_member
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$283, DW_AT_name("INIT_GS0")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_INIT_GS0")
	.dwattr $C$DW$283, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$283, DW_AT_bit_size(0x01)
	.dwattr $C$DW$283, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$283, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$284	.dwtag  DW_TAG_member
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$284, DW_AT_name("INIT_GS1")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_INIT_GS1")
	.dwattr $C$DW$284, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$284, DW_AT_bit_size(0x01)
	.dwattr $C$DW$284, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$284, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$285	.dwtag  DW_TAG_member
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$285, DW_AT_name("INIT_GS2")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_INIT_GS2")
	.dwattr $C$DW$285, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$285, DW_AT_bit_size(0x01)
	.dwattr $C$DW$285, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$285, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$286	.dwtag  DW_TAG_member
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$286, DW_AT_name("INIT_GS3")
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_INIT_GS3")
	.dwattr $C$DW$286, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$286, DW_AT_bit_size(0x01)
	.dwattr $C$DW$286, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$286, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$287	.dwtag  DW_TAG_member
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$287, DW_AT_name("INIT_GS4")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_INIT_GS4")
	.dwattr $C$DW$287, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$287, DW_AT_bit_size(0x01)
	.dwattr $C$DW$287, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$287, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$288	.dwtag  DW_TAG_member
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$288, DW_AT_name("INIT_GS5")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_INIT_GS5")
	.dwattr $C$DW$288, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$288, DW_AT_bit_size(0x01)
	.dwattr $C$DW$288, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$288, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$289, DW_AT_name("INIT_GS6")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_INIT_GS6")
	.dwattr $C$DW$289, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$289, DW_AT_bit_size(0x01)
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$290, DW_AT_name("INIT_GS7")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_INIT_GS7")
	.dwattr $C$DW$290, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$290, DW_AT_bit_size(0x01)
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$291	.dwtag  DW_TAG_member
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$291, DW_AT_name("INIT_GS8")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_INIT_GS8")
	.dwattr $C$DW$291, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$291, DW_AT_bit_size(0x01)
	.dwattr $C$DW$291, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$292	.dwtag  DW_TAG_member
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$292, DW_AT_name("INIT_GS9")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_INIT_GS9")
	.dwattr $C$DW$292, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$292, DW_AT_bit_size(0x01)
	.dwattr $C$DW$292, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$292, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$293	.dwtag  DW_TAG_member
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$293, DW_AT_name("INIT_GS10")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_INIT_GS10")
	.dwattr $C$DW$293, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$293, DW_AT_bit_size(0x01)
	.dwattr $C$DW$293, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$294, DW_AT_name("INIT_GS11")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_INIT_GS11")
	.dwattr $C$DW$294, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$294, DW_AT_bit_size(0x01)
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$295	.dwtag  DW_TAG_member
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$295, DW_AT_name("INIT_GS12")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_INIT_GS12")
	.dwattr $C$DW$295, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$295, DW_AT_bit_size(0x01)
	.dwattr $C$DW$295, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$295, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$296	.dwtag  DW_TAG_member
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$296, DW_AT_name("INIT_GS13")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_INIT_GS13")
	.dwattr $C$DW$296, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$296, DW_AT_bit_size(0x01)
	.dwattr $C$DW$296, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$296, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$297, DW_AT_name("INIT_GS14")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_INIT_GS14")
	.dwattr $C$DW$297, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$297, DW_AT_bit_size(0x01)
	.dwattr $C$DW$297, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$298	.dwtag  DW_TAG_member
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$298, DW_AT_name("INIT_GS15")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_INIT_GS15")
	.dwattr $C$DW$298, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$298, DW_AT_bit_size(0x01)
	.dwattr $C$DW$298, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$299	.dwtag  DW_TAG_member
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$299, DW_AT_name("rsvd1")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$299, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$299, DW_AT_bit_size(0x10)
	.dwattr $C$DW$299, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$299, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$52


$C$DW$T$53	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$53, DW_AT_name("GSxINIT_REG")
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x02)
$C$DW$300	.dwtag  DW_TAG_member
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$300, DW_AT_name("all")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$300, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$301	.dwtag  DW_TAG_member
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$301, DW_AT_name("bit")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$301, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$301, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$53


$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("GSxLOCK_BITS")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x02)
$C$DW$302	.dwtag  DW_TAG_member
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$302, DW_AT_name("LOCK_GS0")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_LOCK_GS0")
	.dwattr $C$DW$302, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$302, DW_AT_bit_size(0x01)
	.dwattr $C$DW$302, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$302, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$303	.dwtag  DW_TAG_member
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$303, DW_AT_name("LOCK_GS1")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_LOCK_GS1")
	.dwattr $C$DW$303, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$303, DW_AT_bit_size(0x01)
	.dwattr $C$DW$303, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$303, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$304, DW_AT_name("LOCK_GS2")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_LOCK_GS2")
	.dwattr $C$DW$304, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$304, DW_AT_bit_size(0x01)
	.dwattr $C$DW$304, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$305, DW_AT_name("LOCK_GS3")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_LOCK_GS3")
	.dwattr $C$DW$305, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$305, DW_AT_bit_size(0x01)
	.dwattr $C$DW$305, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$306, DW_AT_name("LOCK_GS4")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_LOCK_GS4")
	.dwattr $C$DW$306, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$306, DW_AT_bit_size(0x01)
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$307, DW_AT_name("LOCK_GS5")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_LOCK_GS5")
	.dwattr $C$DW$307, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$307, DW_AT_bit_size(0x01)
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$308, DW_AT_name("LOCK_GS6")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_LOCK_GS6")
	.dwattr $C$DW$308, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$308, DW_AT_bit_size(0x01)
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$309, DW_AT_name("LOCK_GS7")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_LOCK_GS7")
	.dwattr $C$DW$309, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$309, DW_AT_bit_size(0x01)
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$310, DW_AT_name("LOCK_GS8")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_LOCK_GS8")
	.dwattr $C$DW$310, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$310, DW_AT_bit_size(0x01)
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$311, DW_AT_name("LOCK_GS9")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_LOCK_GS9")
	.dwattr $C$DW$311, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$311, DW_AT_bit_size(0x01)
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$312, DW_AT_name("LOCK_GS10")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_LOCK_GS10")
	.dwattr $C$DW$312, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$312, DW_AT_bit_size(0x01)
	.dwattr $C$DW$312, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$313, DW_AT_name("LOCK_GS11")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_LOCK_GS11")
	.dwattr $C$DW$313, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$313, DW_AT_bit_size(0x01)
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$314, DW_AT_name("LOCK_GS12")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_LOCK_GS12")
	.dwattr $C$DW$314, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$314, DW_AT_bit_size(0x01)
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$315, DW_AT_name("LOCK_GS13")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_LOCK_GS13")
	.dwattr $C$DW$315, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$315, DW_AT_bit_size(0x01)
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$316, DW_AT_name("LOCK_GS14")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_LOCK_GS14")
	.dwattr $C$DW$316, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$316, DW_AT_bit_size(0x01)
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$317, DW_AT_name("LOCK_GS15")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_LOCK_GS15")
	.dwattr $C$DW$317, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$317, DW_AT_bit_size(0x01)
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$318, DW_AT_name("rsvd1")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$318, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$318, DW_AT_bit_size(0x10)
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$54


$C$DW$T$55	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$55, DW_AT_name("GSxLOCK_REG")
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x02)
$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$319, DW_AT_name("all")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$320, DW_AT_name("bit")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$55


$C$DW$T$56	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$56, DW_AT_name("GSxMSEL_BITS")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x02)
$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$321, DW_AT_name("MSEL_GS0")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_MSEL_GS0")
	.dwattr $C$DW$321, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$321, DW_AT_bit_size(0x01)
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$322	.dwtag  DW_TAG_member
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$322, DW_AT_name("MSEL_GS1")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_MSEL_GS1")
	.dwattr $C$DW$322, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$322, DW_AT_bit_size(0x01)
	.dwattr $C$DW$322, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$323, DW_AT_name("MSEL_GS2")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_MSEL_GS2")
	.dwattr $C$DW$323, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$323, DW_AT_bit_size(0x01)
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$324, DW_AT_name("MSEL_GS3")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_MSEL_GS3")
	.dwattr $C$DW$324, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$324, DW_AT_bit_size(0x01)
	.dwattr $C$DW$324, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$325, DW_AT_name("MSEL_GS4")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_MSEL_GS4")
	.dwattr $C$DW$325, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$325, DW_AT_bit_size(0x01)
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$326, DW_AT_name("MSEL_GS5")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_MSEL_GS5")
	.dwattr $C$DW$326, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$326, DW_AT_bit_size(0x01)
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$327, DW_AT_name("MSEL_GS6")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_MSEL_GS6")
	.dwattr $C$DW$327, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$327, DW_AT_bit_size(0x01)
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$328, DW_AT_name("MSEL_GS7")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_MSEL_GS7")
	.dwattr $C$DW$328, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$328, DW_AT_bit_size(0x01)
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$329, DW_AT_name("MSEL_GS8")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_MSEL_GS8")
	.dwattr $C$DW$329, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$329, DW_AT_bit_size(0x01)
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$330, DW_AT_name("MSEL_GS9")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_MSEL_GS9")
	.dwattr $C$DW$330, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$330, DW_AT_bit_size(0x01)
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$331, DW_AT_name("MSEL_GS10")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_MSEL_GS10")
	.dwattr $C$DW$331, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$331, DW_AT_bit_size(0x01)
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$332, DW_AT_name("MSEL_GS11")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_MSEL_GS11")
	.dwattr $C$DW$332, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$332, DW_AT_bit_size(0x01)
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$333, DW_AT_name("MSEL_GS12")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_MSEL_GS12")
	.dwattr $C$DW$333, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$333, DW_AT_bit_size(0x01)
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$334, DW_AT_name("MSEL_GS13")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_MSEL_GS13")
	.dwattr $C$DW$334, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$334, DW_AT_bit_size(0x01)
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$335, DW_AT_name("MSEL_GS14")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_MSEL_GS14")
	.dwattr $C$DW$335, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$335, DW_AT_bit_size(0x01)
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$336, DW_AT_name("MSEL_GS15")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_MSEL_GS15")
	.dwattr $C$DW$336, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$336, DW_AT_bit_size(0x01)
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$337, DW_AT_name("rsvd1")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$337, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$337, DW_AT_bit_size(0x10)
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$56


$C$DW$T$57	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$57, DW_AT_name("GSxMSEL_REG")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x02)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$338, DW_AT_name("all")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$339, DW_AT_name("bit")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$57


$C$DW$T$58	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$58, DW_AT_name("GSxTEST_BITS")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x02)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$340, DW_AT_name("TEST_GS0")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_TEST_GS0")
	.dwattr $C$DW$340, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$340, DW_AT_bit_size(0x02)
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$341, DW_AT_name("TEST_GS1")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_TEST_GS1")
	.dwattr $C$DW$341, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$341, DW_AT_bit_size(0x02)
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$342, DW_AT_name("TEST_GS2")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_TEST_GS2")
	.dwattr $C$DW$342, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$342, DW_AT_bit_size(0x02)
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$343, DW_AT_name("TEST_GS3")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_TEST_GS3")
	.dwattr $C$DW$343, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$343, DW_AT_bit_size(0x02)
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$344, DW_AT_name("TEST_GS4")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_TEST_GS4")
	.dwattr $C$DW$344, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$344, DW_AT_bit_size(0x02)
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$345, DW_AT_name("TEST_GS5")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_TEST_GS5")
	.dwattr $C$DW$345, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$345, DW_AT_bit_size(0x02)
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$346, DW_AT_name("TEST_GS6")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_TEST_GS6")
	.dwattr $C$DW$346, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$346, DW_AT_bit_size(0x02)
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$347, DW_AT_name("TEST_GS7")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_TEST_GS7")
	.dwattr $C$DW$347, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$347, DW_AT_bit_size(0x02)
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$348, DW_AT_name("TEST_GS8")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_TEST_GS8")
	.dwattr $C$DW$348, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$348, DW_AT_bit_size(0x02)
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$349, DW_AT_name("TEST_GS9")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_TEST_GS9")
	.dwattr $C$DW$349, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$349, DW_AT_bit_size(0x02)
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$350, DW_AT_name("TEST_GS10")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_TEST_GS10")
	.dwattr $C$DW$350, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$350, DW_AT_bit_size(0x02)
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$351, DW_AT_name("TEST_GS11")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_TEST_GS11")
	.dwattr $C$DW$351, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$351, DW_AT_bit_size(0x02)
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$352, DW_AT_name("TEST_GS12")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_TEST_GS12")
	.dwattr $C$DW$352, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$352, DW_AT_bit_size(0x02)
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$353, DW_AT_name("TEST_GS13")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_TEST_GS13")
	.dwattr $C$DW$353, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$353, DW_AT_bit_size(0x02)
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$354, DW_AT_name("TEST_GS14")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_TEST_GS14")
	.dwattr $C$DW$354, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$354, DW_AT_bit_size(0x02)
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$355, DW_AT_name("TEST_GS15")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_TEST_GS15")
	.dwattr $C$DW$355, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$355, DW_AT_bit_size(0x02)
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$58


$C$DW$T$59	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$59, DW_AT_name("GSxTEST_REG")
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x02)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$356, DW_AT_name("all")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$357, DW_AT_name("bit")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$59


$C$DW$T$60	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$60, DW_AT_name("IPCACK_BITS")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x02)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$358, DW_AT_name("IPC0")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$358, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$358, DW_AT_bit_size(0x01)
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$359, DW_AT_name("IPC1")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$359, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$359, DW_AT_bit_size(0x01)
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$360, DW_AT_name("IPC2")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$360, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$360, DW_AT_bit_size(0x01)
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$361, DW_AT_name("IPC3")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$361, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$361, DW_AT_bit_size(0x01)
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$362, DW_AT_name("IPC4")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$362, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$362, DW_AT_bit_size(0x01)
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$363, DW_AT_name("IPC5")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$363, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$363, DW_AT_bit_size(0x01)
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$364, DW_AT_name("IPC6")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$364, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$364, DW_AT_bit_size(0x01)
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$365, DW_AT_name("IPC7")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$365, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$365, DW_AT_bit_size(0x01)
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$366, DW_AT_name("IPC8")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$366, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$366, DW_AT_bit_size(0x01)
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$367, DW_AT_name("IPC9")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$367, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$367, DW_AT_bit_size(0x01)
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$368, DW_AT_name("IPC10")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$368, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$368, DW_AT_bit_size(0x01)
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$369, DW_AT_name("IPC11")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$369, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$369, DW_AT_bit_size(0x01)
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$370, DW_AT_name("IPC12")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$370, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$370, DW_AT_bit_size(0x01)
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$371, DW_AT_name("IPC13")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$371, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$371, DW_AT_bit_size(0x01)
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$372, DW_AT_name("IPC14")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$372, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$372, DW_AT_bit_size(0x01)
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$373, DW_AT_name("IPC15")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$373, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$373, DW_AT_bit_size(0x01)
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$374, DW_AT_name("IPC16")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$374, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$374, DW_AT_bit_size(0x01)
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$375, DW_AT_name("IPC17")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$375, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$375, DW_AT_bit_size(0x01)
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$376	.dwtag  DW_TAG_member
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$376, DW_AT_name("IPC18")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$376, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$376, DW_AT_bit_size(0x01)
	.dwattr $C$DW$376, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$377, DW_AT_name("IPC19")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$377, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$377, DW_AT_bit_size(0x01)
	.dwattr $C$DW$377, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$378, DW_AT_name("IPC20")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$378, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$378, DW_AT_bit_size(0x01)
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$379, DW_AT_name("IPC21")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$379, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$379, DW_AT_bit_size(0x01)
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$380, DW_AT_name("IPC22")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$380, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$380, DW_AT_bit_size(0x01)
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$381, DW_AT_name("IPC23")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$381, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$381, DW_AT_bit_size(0x01)
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$382, DW_AT_name("IPC24")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$382, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$382, DW_AT_bit_size(0x01)
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$383, DW_AT_name("IPC25")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$383, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$383, DW_AT_bit_size(0x01)
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$384, DW_AT_name("IPC26")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$384, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$384, DW_AT_bit_size(0x01)
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$385	.dwtag  DW_TAG_member
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$385, DW_AT_name("IPC27")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$385, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$385, DW_AT_bit_size(0x01)
	.dwattr $C$DW$385, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$385, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$386	.dwtag  DW_TAG_member
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$386, DW_AT_name("IPC28")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$386, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$386, DW_AT_bit_size(0x01)
	.dwattr $C$DW$386, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$386, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$387	.dwtag  DW_TAG_member
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$387, DW_AT_name("IPC29")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$387, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$387, DW_AT_bit_size(0x01)
	.dwattr $C$DW$387, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$387, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$388, DW_AT_name("IPC30")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$388, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$388, DW_AT_bit_size(0x01)
	.dwattr $C$DW$388, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$389, DW_AT_name("IPC31")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$389, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$389, DW_AT_bit_size(0x01)
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$60


$C$DW$T$61	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$61, DW_AT_name("IPCACK_REG")
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x02)
$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$390, DW_AT_name("all")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$391, DW_AT_name("bit")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$61


$C$DW$T$62	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$62, DW_AT_name("IPCCLR_BITS")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x02)
$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$392, DW_AT_name("IPC0")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$392, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$392, DW_AT_bit_size(0x01)
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$393, DW_AT_name("IPC1")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$393, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$393, DW_AT_bit_size(0x01)
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$394, DW_AT_name("IPC2")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$394, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$394, DW_AT_bit_size(0x01)
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$395, DW_AT_name("IPC3")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$395, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$395, DW_AT_bit_size(0x01)
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$396, DW_AT_name("IPC4")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$396, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$396, DW_AT_bit_size(0x01)
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$397, DW_AT_name("IPC5")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$397, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$397, DW_AT_bit_size(0x01)
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$398, DW_AT_name("IPC6")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$398, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$398, DW_AT_bit_size(0x01)
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$399, DW_AT_name("IPC7")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$399, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$399, DW_AT_bit_size(0x01)
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$400, DW_AT_name("IPC8")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$400, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$400, DW_AT_bit_size(0x01)
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$401, DW_AT_name("IPC9")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$401, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$401, DW_AT_bit_size(0x01)
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$402, DW_AT_name("IPC10")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$402, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$402, DW_AT_bit_size(0x01)
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$403, DW_AT_name("IPC11")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$403, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$403, DW_AT_bit_size(0x01)
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$404, DW_AT_name("IPC12")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$404, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$404, DW_AT_bit_size(0x01)
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$405, DW_AT_name("IPC13")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$405, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$405, DW_AT_bit_size(0x01)
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$406, DW_AT_name("IPC14")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$406, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$406, DW_AT_bit_size(0x01)
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$407, DW_AT_name("IPC15")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$407, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$407, DW_AT_bit_size(0x01)
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$408, DW_AT_name("IPC16")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$408, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$408, DW_AT_bit_size(0x01)
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$409	.dwtag  DW_TAG_member
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$409, DW_AT_name("IPC17")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$409, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$409, DW_AT_bit_size(0x01)
	.dwattr $C$DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$409, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$410, DW_AT_name("IPC18")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$410, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$410, DW_AT_bit_size(0x01)
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$411, DW_AT_name("IPC19")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$411, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$411, DW_AT_bit_size(0x01)
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$412, DW_AT_name("IPC20")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$412, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$412, DW_AT_bit_size(0x01)
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$413, DW_AT_name("IPC21")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$413, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$413, DW_AT_bit_size(0x01)
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$414, DW_AT_name("IPC22")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$414, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$414, DW_AT_bit_size(0x01)
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$415, DW_AT_name("IPC23")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$415, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$415, DW_AT_bit_size(0x01)
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$416, DW_AT_name("IPC24")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$416, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$416, DW_AT_bit_size(0x01)
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$417, DW_AT_name("IPC25")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$417, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$417, DW_AT_bit_size(0x01)
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$418, DW_AT_name("IPC26")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$418, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$418, DW_AT_bit_size(0x01)
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$419, DW_AT_name("IPC27")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$419, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$419, DW_AT_bit_size(0x01)
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$420	.dwtag  DW_TAG_member
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$420, DW_AT_name("IPC28")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$420, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$420, DW_AT_bit_size(0x01)
	.dwattr $C$DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$421, DW_AT_name("IPC29")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$421, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$421, DW_AT_bit_size(0x01)
	.dwattr $C$DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$422, DW_AT_name("IPC30")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$422, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$422, DW_AT_bit_size(0x01)
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$423, DW_AT_name("IPC31")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$423, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$423, DW_AT_bit_size(0x01)
	.dwattr $C$DW$423, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$62


$C$DW$T$63	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$63, DW_AT_name("IPCCLR_REG")
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x02)
$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$424, DW_AT_name("all")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$425, DW_AT_name("bit")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$63


$C$DW$T$64	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$64, DW_AT_name("IPCFLG_BITS")
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x02)
$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$426, DW_AT_name("IPC0")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$426, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$426, DW_AT_bit_size(0x01)
	.dwattr $C$DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$427, DW_AT_name("IPC1")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$427, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$427, DW_AT_bit_size(0x01)
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$428, DW_AT_name("IPC2")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$428, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$428, DW_AT_bit_size(0x01)
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$429, DW_AT_name("IPC3")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$429, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$429, DW_AT_bit_size(0x01)
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$430, DW_AT_name("IPC4")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$430, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$430, DW_AT_bit_size(0x01)
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$431	.dwtag  DW_TAG_member
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$431, DW_AT_name("IPC5")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$431, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$431, DW_AT_bit_size(0x01)
	.dwattr $C$DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$432, DW_AT_name("IPC6")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$432, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$432, DW_AT_bit_size(0x01)
	.dwattr $C$DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$433, DW_AT_name("IPC7")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$433, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$433, DW_AT_bit_size(0x01)
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$434, DW_AT_name("IPC8")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$434, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$434, DW_AT_bit_size(0x01)
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$435	.dwtag  DW_TAG_member
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$435, DW_AT_name("IPC9")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$435, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$435, DW_AT_bit_size(0x01)
	.dwattr $C$DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$436, DW_AT_name("IPC10")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$436, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$436, DW_AT_bit_size(0x01)
	.dwattr $C$DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$437, DW_AT_name("IPC11")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$437, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$437, DW_AT_bit_size(0x01)
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$438, DW_AT_name("IPC12")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$438, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$438, DW_AT_bit_size(0x01)
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$439, DW_AT_name("IPC13")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$439, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$439, DW_AT_bit_size(0x01)
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$440, DW_AT_name("IPC14")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$440, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$440, DW_AT_bit_size(0x01)
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$441, DW_AT_name("IPC15")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$441, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$441, DW_AT_bit_size(0x01)
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$442, DW_AT_name("IPC16")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$442, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$442, DW_AT_bit_size(0x01)
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$443, DW_AT_name("IPC17")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$443, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$443, DW_AT_bit_size(0x01)
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$444, DW_AT_name("IPC18")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$444, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$444, DW_AT_bit_size(0x01)
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$445, DW_AT_name("IPC19")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$445, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$445, DW_AT_bit_size(0x01)
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$446, DW_AT_name("IPC20")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$446, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$446, DW_AT_bit_size(0x01)
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$447, DW_AT_name("IPC21")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$447, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$447, DW_AT_bit_size(0x01)
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$448, DW_AT_name("IPC22")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$448, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$448, DW_AT_bit_size(0x01)
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$449, DW_AT_name("IPC23")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$449, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$449, DW_AT_bit_size(0x01)
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$450, DW_AT_name("IPC24")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$450, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$450, DW_AT_bit_size(0x01)
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$451, DW_AT_name("IPC25")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$451, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$451, DW_AT_bit_size(0x01)
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$452, DW_AT_name("IPC26")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$452, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$452, DW_AT_bit_size(0x01)
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$453, DW_AT_name("IPC27")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$453, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$453, DW_AT_bit_size(0x01)
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$454, DW_AT_name("IPC28")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$454, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$454, DW_AT_bit_size(0x01)
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$455, DW_AT_name("IPC29")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$455, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$455, DW_AT_bit_size(0x01)
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$456, DW_AT_name("IPC30")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$456, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$456, DW_AT_bit_size(0x01)
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$457, DW_AT_name("IPC31")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$457, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$457, DW_AT_bit_size(0x01)
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$64


$C$DW$T$65	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$65, DW_AT_name("IPCFLG_REG")
	.dwattr $C$DW$T$65, DW_AT_byte_size(0x02)
$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$458, DW_AT_name("all")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$459, DW_AT_name("bit")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$65


$C$DW$T$66	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$66, DW_AT_name("IPCSET_BITS")
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x02)
$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$460, DW_AT_name("IPC0")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$460, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$460, DW_AT_bit_size(0x01)
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$461, DW_AT_name("IPC1")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$461, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$461, DW_AT_bit_size(0x01)
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$462, DW_AT_name("IPC2")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$462, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$462, DW_AT_bit_size(0x01)
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$463, DW_AT_name("IPC3")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$463, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$463, DW_AT_bit_size(0x01)
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$464, DW_AT_name("IPC4")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$464, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$464, DW_AT_bit_size(0x01)
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$465, DW_AT_name("IPC5")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$465, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$465, DW_AT_bit_size(0x01)
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$466, DW_AT_name("IPC6")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$466, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$466, DW_AT_bit_size(0x01)
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$467, DW_AT_name("IPC7")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$467, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$467, DW_AT_bit_size(0x01)
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$468, DW_AT_name("IPC8")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$468, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$468, DW_AT_bit_size(0x01)
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$469, DW_AT_name("IPC9")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$469, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$469, DW_AT_bit_size(0x01)
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$470, DW_AT_name("IPC10")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$470, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$470, DW_AT_bit_size(0x01)
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$471, DW_AT_name("IPC11")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$471, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$471, DW_AT_bit_size(0x01)
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$472, DW_AT_name("IPC12")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$472, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$472, DW_AT_bit_size(0x01)
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$473, DW_AT_name("IPC13")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$473, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$473, DW_AT_bit_size(0x01)
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$474, DW_AT_name("IPC14")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$474, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$474, DW_AT_bit_size(0x01)
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$475, DW_AT_name("IPC15")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$475, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$475, DW_AT_bit_size(0x01)
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$476, DW_AT_name("IPC16")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$476, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$476, DW_AT_bit_size(0x01)
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$477, DW_AT_name("IPC17")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$477, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$477, DW_AT_bit_size(0x01)
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$478	.dwtag  DW_TAG_member
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$478, DW_AT_name("IPC18")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$478, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$478, DW_AT_bit_size(0x01)
	.dwattr $C$DW$478, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$479, DW_AT_name("IPC19")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$479, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$479, DW_AT_bit_size(0x01)
	.dwattr $C$DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$480, DW_AT_name("IPC20")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$480, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$480, DW_AT_bit_size(0x01)
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$481, DW_AT_name("IPC21")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$481, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$481, DW_AT_bit_size(0x01)
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$482, DW_AT_name("IPC22")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$482, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$482, DW_AT_bit_size(0x01)
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$483, DW_AT_name("IPC23")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$483, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$483, DW_AT_bit_size(0x01)
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$484, DW_AT_name("IPC24")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$484, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$484, DW_AT_bit_size(0x01)
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$485, DW_AT_name("IPC25")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$485, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$485, DW_AT_bit_size(0x01)
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$486, DW_AT_name("IPC26")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$486, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$486, DW_AT_bit_size(0x01)
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$487, DW_AT_name("IPC27")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$487, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$487, DW_AT_bit_size(0x01)
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$488	.dwtag  DW_TAG_member
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$488, DW_AT_name("IPC28")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$488, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$488, DW_AT_bit_size(0x01)
	.dwattr $C$DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$489	.dwtag  DW_TAG_member
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$489, DW_AT_name("IPC29")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$489, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$489, DW_AT_bit_size(0x01)
	.dwattr $C$DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$489, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$490, DW_AT_name("IPC30")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$490, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$490, DW_AT_bit_size(0x01)
	.dwattr $C$DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$491	.dwtag  DW_TAG_member
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$491, DW_AT_name("IPC31")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$491, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$491, DW_AT_bit_size(0x01)
	.dwattr $C$DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$491, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$66


$C$DW$T$67	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$67, DW_AT_name("IPCSET_REG")
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x02)
$C$DW$492	.dwtag  DW_TAG_member
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$492, DW_AT_name("all")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$492, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$493	.dwtag  DW_TAG_member
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$493, DW_AT_name("bit")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$493, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$67


$C$DW$T$68	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$68, DW_AT_name("IPCSTS_BITS")
	.dwattr $C$DW$T$68, DW_AT_byte_size(0x02)
$C$DW$494	.dwtag  DW_TAG_member
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$494, DW_AT_name("IPC0")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_IPC0")
	.dwattr $C$DW$494, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$494, DW_AT_bit_size(0x01)
	.dwattr $C$DW$494, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$494, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$495, DW_AT_name("IPC1")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_IPC1")
	.dwattr $C$DW$495, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$495, DW_AT_bit_size(0x01)
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$496, DW_AT_name("IPC2")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_IPC2")
	.dwattr $C$DW$496, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$496, DW_AT_bit_size(0x01)
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$497, DW_AT_name("IPC3")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_IPC3")
	.dwattr $C$DW$497, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$497, DW_AT_bit_size(0x01)
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$498, DW_AT_name("IPC4")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_IPC4")
	.dwattr $C$DW$498, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$498, DW_AT_bit_size(0x01)
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$499, DW_AT_name("IPC5")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_IPC5")
	.dwattr $C$DW$499, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$499, DW_AT_bit_size(0x01)
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$500, DW_AT_name("IPC6")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_IPC6")
	.dwattr $C$DW$500, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$500, DW_AT_bit_size(0x01)
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$501, DW_AT_name("IPC7")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_IPC7")
	.dwattr $C$DW$501, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$501, DW_AT_bit_size(0x01)
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$502, DW_AT_name("IPC8")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_IPC8")
	.dwattr $C$DW$502, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$502, DW_AT_bit_size(0x01)
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$503, DW_AT_name("IPC9")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_IPC9")
	.dwattr $C$DW$503, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$503, DW_AT_bit_size(0x01)
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$504, DW_AT_name("IPC10")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_IPC10")
	.dwattr $C$DW$504, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$504, DW_AT_bit_size(0x01)
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$505, DW_AT_name("IPC11")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_IPC11")
	.dwattr $C$DW$505, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$505, DW_AT_bit_size(0x01)
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$506, DW_AT_name("IPC12")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_IPC12")
	.dwattr $C$DW$506, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$506, DW_AT_bit_size(0x01)
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$507, DW_AT_name("IPC13")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_IPC13")
	.dwattr $C$DW$507, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$507, DW_AT_bit_size(0x01)
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$508, DW_AT_name("IPC14")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_IPC14")
	.dwattr $C$DW$508, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$508, DW_AT_bit_size(0x01)
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$509, DW_AT_name("IPC15")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_IPC15")
	.dwattr $C$DW$509, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$509, DW_AT_bit_size(0x01)
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$510, DW_AT_name("IPC16")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_IPC16")
	.dwattr $C$DW$510, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$510, DW_AT_bit_size(0x01)
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$511, DW_AT_name("IPC17")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_IPC17")
	.dwattr $C$DW$511, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$511, DW_AT_bit_size(0x01)
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$512, DW_AT_name("IPC18")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_IPC18")
	.dwattr $C$DW$512, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$512, DW_AT_bit_size(0x01)
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$513, DW_AT_name("IPC19")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_IPC19")
	.dwattr $C$DW$513, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$513, DW_AT_bit_size(0x01)
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$514, DW_AT_name("IPC20")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_IPC20")
	.dwattr $C$DW$514, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$514, DW_AT_bit_size(0x01)
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$515, DW_AT_name("IPC21")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_IPC21")
	.dwattr $C$DW$515, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$515, DW_AT_bit_size(0x01)
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$516, DW_AT_name("IPC22")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_IPC22")
	.dwattr $C$DW$516, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$516, DW_AT_bit_size(0x01)
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$517, DW_AT_name("IPC23")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_IPC23")
	.dwattr $C$DW$517, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$517, DW_AT_bit_size(0x01)
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$518, DW_AT_name("IPC24")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_IPC24")
	.dwattr $C$DW$518, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$518, DW_AT_bit_size(0x01)
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$519, DW_AT_name("IPC25")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_IPC25")
	.dwattr $C$DW$519, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$519, DW_AT_bit_size(0x01)
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$520, DW_AT_name("IPC26")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_IPC26")
	.dwattr $C$DW$520, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$520, DW_AT_bit_size(0x01)
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$521, DW_AT_name("IPC27")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_IPC27")
	.dwattr $C$DW$521, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$521, DW_AT_bit_size(0x01)
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$522, DW_AT_name("IPC28")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_IPC28")
	.dwattr $C$DW$522, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$522, DW_AT_bit_size(0x01)
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$523, DW_AT_name("IPC29")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_IPC29")
	.dwattr $C$DW$523, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$523, DW_AT_bit_size(0x01)
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$524, DW_AT_name("IPC30")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_IPC30")
	.dwattr $C$DW$524, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$524, DW_AT_bit_size(0x01)
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$525, DW_AT_name("IPC31")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_IPC31")
	.dwattr $C$DW$525, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$525, DW_AT_bit_size(0x01)
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$68


$C$DW$T$69	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$69, DW_AT_name("IPCSTS_REG")
	.dwattr $C$DW$T$69, DW_AT_byte_size(0x02)
$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$526, DW_AT_name("all")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$527, DW_AT_name("bit")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$69


$C$DW$T$71	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$71, DW_AT_name("IPC_PARAMETER")
	.dwattr $C$DW$T$71, DW_AT_byte_size(0x2e)
$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$528, DW_AT_name("unCpu1ToCpu2TxBuf")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_unCpu1ToCpu2TxBuf")
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$529, DW_AT_name("unCpu2ToCpu1RxBuf")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_unCpu2ToCpu1RxBuf")
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$530, DW_AT_name("RemoteResponseFlag")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_RemoteResponseFlag")
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$531, DW_AT_name("LocalReceiveFlag")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_LocalReceiveFlag")
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x29]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$532, DW_AT_name("ErrFlag")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_ErrFlag")
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$533, DW_AT_name("INT1Flag")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_INT1Flag")
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$534, DW_AT_name("TxMsgQueCurrentPtr")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_TxMsgQueCurrentPtr")
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$535, DW_AT_name("RxMsgQueCurrentPtr")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_RxMsgQueCurrentPtr")
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$71

$C$DW$T$167	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$167, DW_AT_name("stIpcParameters")
	.dwattr $C$DW$T$167, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$167, DW_AT_language(DW_LANG_C)

$C$DW$T$168	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$168, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$T$168, DW_AT_address_class(0x20)


$C$DW$T$73	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$73, DW_AT_name("IPC_REGS_CPU1")
	.dwattr $C$DW$T$73, DW_AT_byte_size(0x24)
$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$536, DW_AT_name("IPCACK")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_IPCACK")
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$537, DW_AT_name("IPCSTS")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_IPCSTS")
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$538, DW_AT_name("IPCSET")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_IPCSET")
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$539, DW_AT_name("IPCCLR")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_IPCCLR")
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$540, DW_AT_name("IPCFLG")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_IPCFLG")
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$541, DW_AT_name("rsvd1")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$542, DW_AT_name("IPCCOUNTERL")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_IPCCOUNTERL")
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$543, DW_AT_name("IPCCOUNTERH")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_IPCCOUNTERH")
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$544, DW_AT_name("IPCSENDCOM")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_IPCSENDCOM")
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$545, DW_AT_name("IPCSENDADDR")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_IPCSENDADDR")
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$546, DW_AT_name("IPCSENDDATA")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_IPCSENDDATA")
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$547, DW_AT_name("IPCREMOTEREPLY")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_IPCREMOTEREPLY")
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$548, DW_AT_name("IPCRECVCOM")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_IPCRECVCOM")
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$549, DW_AT_name("IPCRECVADDR")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_IPCRECVADDR")
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$550, DW_AT_name("IPCRECVDATA")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_IPCRECVDATA")
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$551, DW_AT_name("IPCLOCALREPLY")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_IPCLOCALREPLY")
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$552, DW_AT_name("IPCBOOTSTS")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_IPCBOOTSTS")
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$553, DW_AT_name("IPCBOOTMODE")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_IPCBOOTMODE")
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$73

$C$DW$554	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$73)

$C$DW$T$170	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$170, DW_AT_type(*$C$DW$554)


$C$DW$T$74	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$74, DW_AT_name("LSxACCPROT0_BITS")
	.dwattr $C$DW$T$74, DW_AT_byte_size(0x02)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$555, DW_AT_name("FETCHPROT_LS0")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_FETCHPROT_LS0")
	.dwattr $C$DW$555, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$555, DW_AT_bit_size(0x01)
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$556, DW_AT_name("CPUWRPROT_LS0")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_CPUWRPROT_LS0")
	.dwattr $C$DW$556, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$556, DW_AT_bit_size(0x01)
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$557, DW_AT_name("rsvd1")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$557, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$557, DW_AT_bit_size(0x06)
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$558, DW_AT_name("FETCHPROT_LS1")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_FETCHPROT_LS1")
	.dwattr $C$DW$558, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$558, DW_AT_bit_size(0x01)
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$559, DW_AT_name("CPUWRPROT_LS1")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_CPUWRPROT_LS1")
	.dwattr $C$DW$559, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$559, DW_AT_bit_size(0x01)
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$560, DW_AT_name("rsvd2")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$560, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$560, DW_AT_bit_size(0x06)
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$561, DW_AT_name("FETCHPROT_LS2")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_FETCHPROT_LS2")
	.dwattr $C$DW$561, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$561, DW_AT_bit_size(0x01)
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$562, DW_AT_name("CPUWRPROT_LS2")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_CPUWRPROT_LS2")
	.dwattr $C$DW$562, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$562, DW_AT_bit_size(0x01)
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$563, DW_AT_name("rsvd3")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$563, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$563, DW_AT_bit_size(0x06)
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$564, DW_AT_name("FETCHPROT_LS3")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_FETCHPROT_LS3")
	.dwattr $C$DW$564, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$564, DW_AT_bit_size(0x01)
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$565, DW_AT_name("CPUWRPROT_LS3")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_CPUWRPROT_LS3")
	.dwattr $C$DW$565, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$565, DW_AT_bit_size(0x01)
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$566, DW_AT_name("rsvd4")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$566, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$566, DW_AT_bit_size(0x06)
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$74


$C$DW$T$75	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$75, DW_AT_name("LSxACCPROT0_REG")
	.dwattr $C$DW$T$75, DW_AT_byte_size(0x02)
$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$567, DW_AT_name("all")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$568, DW_AT_name("bit")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$75


$C$DW$T$76	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$76, DW_AT_name("LSxACCPROT1_BITS")
	.dwattr $C$DW$T$76, DW_AT_byte_size(0x02)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$569, DW_AT_name("FETCHPROT_LS4")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_FETCHPROT_LS4")
	.dwattr $C$DW$569, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$569, DW_AT_bit_size(0x01)
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$570, DW_AT_name("CPUWRPROT_LS4")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_CPUWRPROT_LS4")
	.dwattr $C$DW$570, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$570, DW_AT_bit_size(0x01)
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$571, DW_AT_name("rsvd1")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$571, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$571, DW_AT_bit_size(0x06)
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$572, DW_AT_name("FETCHPROT_LS5")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_FETCHPROT_LS5")
	.dwattr $C$DW$572, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$572, DW_AT_bit_size(0x01)
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$573, DW_AT_name("CPUWRPROT_LS5")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_CPUWRPROT_LS5")
	.dwattr $C$DW$573, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$573, DW_AT_bit_size(0x01)
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$574, DW_AT_name("rsvd2")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$574, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$574, DW_AT_bit_size(0x06)
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$575, DW_AT_name("rsvd3")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$575, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$575, DW_AT_bit_size(0x10)
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$76


$C$DW$T$77	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$77, DW_AT_name("LSxACCPROT1_REG")
	.dwattr $C$DW$T$77, DW_AT_byte_size(0x02)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$576, DW_AT_name("all")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$577, DW_AT_name("bit")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$77


$C$DW$T$78	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$78, DW_AT_name("LSxCLAPGM_BITS")
	.dwattr $C$DW$T$78, DW_AT_byte_size(0x02)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$578, DW_AT_name("CLAPGM_LS0")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_CLAPGM_LS0")
	.dwattr $C$DW$578, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$578, DW_AT_bit_size(0x01)
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$579, DW_AT_name("CLAPGM_LS1")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_CLAPGM_LS1")
	.dwattr $C$DW$579, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$579, DW_AT_bit_size(0x01)
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$580, DW_AT_name("CLAPGM_LS2")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_CLAPGM_LS2")
	.dwattr $C$DW$580, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$580, DW_AT_bit_size(0x01)
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$581, DW_AT_name("CLAPGM_LS3")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_CLAPGM_LS3")
	.dwattr $C$DW$581, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$581, DW_AT_bit_size(0x01)
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$582, DW_AT_name("CLAPGM_LS4")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_CLAPGM_LS4")
	.dwattr $C$DW$582, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$582, DW_AT_bit_size(0x01)
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$583, DW_AT_name("CLAPGM_LS5")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_CLAPGM_LS5")
	.dwattr $C$DW$583, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$583, DW_AT_bit_size(0x01)
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$584, DW_AT_name("rsvd1")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$584, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$584, DW_AT_bit_size(0x0a)
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$585, DW_AT_name("rsvd2")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$585, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$585, DW_AT_bit_size(0x10)
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$78


$C$DW$T$79	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$79, DW_AT_name("LSxCLAPGM_REG")
	.dwattr $C$DW$T$79, DW_AT_byte_size(0x02)
$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$586, DW_AT_name("all")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$587, DW_AT_name("bit")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$79


$C$DW$T$80	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$80, DW_AT_name("LSxCOMMIT_BITS")
	.dwattr $C$DW$T$80, DW_AT_byte_size(0x02)
$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$588, DW_AT_name("COMMIT_LS0")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_COMMIT_LS0")
	.dwattr $C$DW$588, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$588, DW_AT_bit_size(0x01)
	.dwattr $C$DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$589, DW_AT_name("COMMIT_LS1")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_COMMIT_LS1")
	.dwattr $C$DW$589, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$589, DW_AT_bit_size(0x01)
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$590, DW_AT_name("COMMIT_LS2")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_COMMIT_LS2")
	.dwattr $C$DW$590, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$590, DW_AT_bit_size(0x01)
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$591, DW_AT_name("COMMIT_LS3")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_COMMIT_LS3")
	.dwattr $C$DW$591, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$591, DW_AT_bit_size(0x01)
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$592, DW_AT_name("COMMIT_LS4")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_COMMIT_LS4")
	.dwattr $C$DW$592, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$592, DW_AT_bit_size(0x01)
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$593, DW_AT_name("COMMIT_LS5")
	.dwattr $C$DW$593, DW_AT_TI_symbol_name("_COMMIT_LS5")
	.dwattr $C$DW$593, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$593, DW_AT_bit_size(0x01)
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$594, DW_AT_name("rsvd1")
	.dwattr $C$DW$594, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$594, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$594, DW_AT_bit_size(0x0a)
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$595, DW_AT_name("rsvd2")
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$595, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$595, DW_AT_bit_size(0x10)
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$80


$C$DW$T$81	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$81, DW_AT_name("LSxCOMMIT_REG")
	.dwattr $C$DW$T$81, DW_AT_byte_size(0x02)
$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$596, DW_AT_name("all")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$597, DW_AT_name("bit")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$81


$C$DW$T$82	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$82, DW_AT_name("LSxINITDONE_BITS")
	.dwattr $C$DW$T$82, DW_AT_byte_size(0x02)
$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$598, DW_AT_name("INITDONE_LS0")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_INITDONE_LS0")
	.dwattr $C$DW$598, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$598, DW_AT_bit_size(0x01)
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$599, DW_AT_name("INITDONE_LS1")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_INITDONE_LS1")
	.dwattr $C$DW$599, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$599, DW_AT_bit_size(0x01)
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$600, DW_AT_name("INITDONE_LS2")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_INITDONE_LS2")
	.dwattr $C$DW$600, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$600, DW_AT_bit_size(0x01)
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$601, DW_AT_name("INITDONE_LS3")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_INITDONE_LS3")
	.dwattr $C$DW$601, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$601, DW_AT_bit_size(0x01)
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$602, DW_AT_name("INITDONE_LS4")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_INITDONE_LS4")
	.dwattr $C$DW$602, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$602, DW_AT_bit_size(0x01)
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$603, DW_AT_name("INITDONE_LS5")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_INITDONE_LS5")
	.dwattr $C$DW$603, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$603, DW_AT_bit_size(0x01)
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$604, DW_AT_name("rsvd1")
	.dwattr $C$DW$604, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$604, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$604, DW_AT_bit_size(0x0a)
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$605, DW_AT_name("rsvd2")
	.dwattr $C$DW$605, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$605, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$605, DW_AT_bit_size(0x10)
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$82


$C$DW$T$83	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$83, DW_AT_name("LSxINITDONE_REG")
	.dwattr $C$DW$T$83, DW_AT_byte_size(0x02)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$606, DW_AT_name("all")
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$607, DW_AT_name("bit")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$83


$C$DW$T$84	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$84, DW_AT_name("LSxINIT_BITS")
	.dwattr $C$DW$T$84, DW_AT_byte_size(0x02)
$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$608, DW_AT_name("INIT_LS0")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_INIT_LS0")
	.dwattr $C$DW$608, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$608, DW_AT_bit_size(0x01)
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$609, DW_AT_name("INIT_LS1")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_INIT_LS1")
	.dwattr $C$DW$609, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$609, DW_AT_bit_size(0x01)
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$610, DW_AT_name("INIT_LS2")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_INIT_LS2")
	.dwattr $C$DW$610, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$610, DW_AT_bit_size(0x01)
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$611, DW_AT_name("INIT_LS3")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_INIT_LS3")
	.dwattr $C$DW$611, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$611, DW_AT_bit_size(0x01)
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$612, DW_AT_name("INIT_LS4")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_INIT_LS4")
	.dwattr $C$DW$612, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$612, DW_AT_bit_size(0x01)
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$613, DW_AT_name("INIT_LS5")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_INIT_LS5")
	.dwattr $C$DW$613, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$613, DW_AT_bit_size(0x01)
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$614, DW_AT_name("rsvd1")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$614, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$614, DW_AT_bit_size(0x0a)
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$615, DW_AT_name("rsvd2")
	.dwattr $C$DW$615, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$615, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$615, DW_AT_bit_size(0x10)
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$84


$C$DW$T$85	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$85, DW_AT_name("LSxINIT_REG")
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x02)
$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$616, DW_AT_name("all")
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$617, DW_AT_name("bit")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$85


$C$DW$T$86	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$86, DW_AT_name("LSxLOCK_BITS")
	.dwattr $C$DW$T$86, DW_AT_byte_size(0x02)
$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$618, DW_AT_name("LOCK_LS0")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_LOCK_LS0")
	.dwattr $C$DW$618, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$618, DW_AT_bit_size(0x01)
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$619, DW_AT_name("LOCK_LS1")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_LOCK_LS1")
	.dwattr $C$DW$619, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$619, DW_AT_bit_size(0x01)
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$620, DW_AT_name("LOCK_LS2")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_LOCK_LS2")
	.dwattr $C$DW$620, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$620, DW_AT_bit_size(0x01)
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$621, DW_AT_name("LOCK_LS3")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_LOCK_LS3")
	.dwattr $C$DW$621, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$621, DW_AT_bit_size(0x01)
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$622, DW_AT_name("LOCK_LS4")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_LOCK_LS4")
	.dwattr $C$DW$622, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$622, DW_AT_bit_size(0x01)
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$623, DW_AT_name("LOCK_LS5")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_LOCK_LS5")
	.dwattr $C$DW$623, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$623, DW_AT_bit_size(0x01)
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$624, DW_AT_name("rsvd1")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$624, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$624, DW_AT_bit_size(0x0a)
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$625, DW_AT_name("rsvd2")
	.dwattr $C$DW$625, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$625, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$625, DW_AT_bit_size(0x10)
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$86


$C$DW$T$87	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$87, DW_AT_name("LSxLOCK_REG")
	.dwattr $C$DW$T$87, DW_AT_byte_size(0x02)
$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$626, DW_AT_name("all")
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$627	.dwtag  DW_TAG_member
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$627, DW_AT_name("bit")
	.dwattr $C$DW$627, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$87


$C$DW$T$88	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$88, DW_AT_name("LSxMSEL_BITS")
	.dwattr $C$DW$T$88, DW_AT_byte_size(0x02)
$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$628, DW_AT_name("MSEL_LS0")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_MSEL_LS0")
	.dwattr $C$DW$628, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$628, DW_AT_bit_size(0x02)
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$629, DW_AT_name("MSEL_LS1")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_MSEL_LS1")
	.dwattr $C$DW$629, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$629, DW_AT_bit_size(0x02)
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$630, DW_AT_name("MSEL_LS2")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_MSEL_LS2")
	.dwattr $C$DW$630, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$630, DW_AT_bit_size(0x02)
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$631, DW_AT_name("MSEL_LS3")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_MSEL_LS3")
	.dwattr $C$DW$631, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$631, DW_AT_bit_size(0x02)
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$632, DW_AT_name("MSEL_LS4")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_MSEL_LS4")
	.dwattr $C$DW$632, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$632, DW_AT_bit_size(0x02)
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$633, DW_AT_name("MSEL_LS5")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_MSEL_LS5")
	.dwattr $C$DW$633, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$633, DW_AT_bit_size(0x02)
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$634, DW_AT_name("rsvd1")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$634, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$634, DW_AT_bit_size(0x04)
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$635, DW_AT_name("rsvd2")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$635, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$635, DW_AT_bit_size(0x10)
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$88


$C$DW$T$89	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$89, DW_AT_name("LSxMSEL_REG")
	.dwattr $C$DW$T$89, DW_AT_byte_size(0x02)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$636, DW_AT_name("all")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$637	.dwtag  DW_TAG_member
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$637, DW_AT_name("bit")
	.dwattr $C$DW$637, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$637, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$89


$C$DW$T$90	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$90, DW_AT_name("LSxTEST_BITS")
	.dwattr $C$DW$T$90, DW_AT_byte_size(0x02)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$638, DW_AT_name("TEST_LS0")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_TEST_LS0")
	.dwattr $C$DW$638, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$638, DW_AT_bit_size(0x02)
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$639, DW_AT_name("TEST_LS1")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_TEST_LS1")
	.dwattr $C$DW$639, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$639, DW_AT_bit_size(0x02)
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$640, DW_AT_name("TEST_LS2")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_TEST_LS2")
	.dwattr $C$DW$640, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$640, DW_AT_bit_size(0x02)
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$641, DW_AT_name("TEST_LS3")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_TEST_LS3")
	.dwattr $C$DW$641, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$641, DW_AT_bit_size(0x02)
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$642, DW_AT_name("TEST_LS4")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_TEST_LS4")
	.dwattr $C$DW$642, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$642, DW_AT_bit_size(0x02)
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$643, DW_AT_name("TEST_LS5")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_TEST_LS5")
	.dwattr $C$DW$643, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$643, DW_AT_bit_size(0x02)
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$644, DW_AT_name("rsvd1")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$644, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$644, DW_AT_bit_size(0x04)
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$645, DW_AT_name("rsvd2")
	.dwattr $C$DW$645, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$645, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$645, DW_AT_bit_size(0x10)
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$90


$C$DW$T$91	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$91, DW_AT_name("LSxTEST_REG")
	.dwattr $C$DW$T$91, DW_AT_byte_size(0x02)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$646, DW_AT_name("all")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$647, DW_AT_name("bit")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$91


$C$DW$T$96	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$96, DW_AT_name("MEM_CFG_REGS")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x80)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$648, DW_AT_name("DxLOCK")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_DxLOCK")
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$649, DW_AT_name("DxCOMMIT")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_DxCOMMIT")
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$650, DW_AT_name("rsvd1")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$651, DW_AT_name("DxACCPROT0")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_DxACCPROT0")
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$652	.dwtag  DW_TAG_member
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$652, DW_AT_name("rsvd2")
	.dwattr $C$DW$652, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$652, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$653, DW_AT_name("DxTEST")
	.dwattr $C$DW$653, DW_AT_TI_symbol_name("_DxTEST")
	.dwattr $C$DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$654, DW_AT_name("DxINIT")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_DxINIT")
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$655, DW_AT_name("DxINITDONE")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_DxINITDONE")
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$656, DW_AT_name("rsvd3")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$657, DW_AT_name("LSxLOCK")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_LSxLOCK")
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$658, DW_AT_name("LSxCOMMIT")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_LSxCOMMIT")
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$659, DW_AT_name("LSxMSEL")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_LSxMSEL")
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$660, DW_AT_name("LSxCLAPGM")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_LSxCLAPGM")
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$661, DW_AT_name("LSxACCPROT0")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_LSxACCPROT0")
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$662, DW_AT_name("LSxACCPROT1")
	.dwattr $C$DW$662, DW_AT_TI_symbol_name("_LSxACCPROT1")
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$663, DW_AT_name("rsvd4")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$664, DW_AT_name("LSxTEST")
	.dwattr $C$DW$664, DW_AT_TI_symbol_name("_LSxTEST")
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$665, DW_AT_name("LSxINIT")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_LSxINIT")
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x32]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$666, DW_AT_name("LSxINITDONE")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_LSxINITDONE")
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$667, DW_AT_name("rsvd5")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$668, DW_AT_name("GSxLOCK")
	.dwattr $C$DW$668, DW_AT_TI_symbol_name("_GSxLOCK")
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$669, DW_AT_name("GSxCOMMIT")
	.dwattr $C$DW$669, DW_AT_TI_symbol_name("_GSxCOMMIT")
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x42]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$670, DW_AT_name("GSxMSEL")
	.dwattr $C$DW$670, DW_AT_TI_symbol_name("_GSxMSEL")
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$671, DW_AT_name("rsvd6")
	.dwattr $C$DW$671, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x46]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$672, DW_AT_name("GSxACCPROT0")
	.dwattr $C$DW$672, DW_AT_TI_symbol_name("_GSxACCPROT0")
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$673, DW_AT_name("GSxACCPROT1")
	.dwattr $C$DW$673, DW_AT_TI_symbol_name("_GSxACCPROT1")
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x4a]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$674, DW_AT_name("GSxACCPROT2")
	.dwattr $C$DW$674, DW_AT_TI_symbol_name("_GSxACCPROT2")
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$675, DW_AT_name("GSxACCPROT3")
	.dwattr $C$DW$675, DW_AT_TI_symbol_name("_GSxACCPROT3")
	.dwattr $C$DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0x4e]
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$676	.dwtag  DW_TAG_member
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$676, DW_AT_name("GSxTEST")
	.dwattr $C$DW$676, DW_AT_TI_symbol_name("_GSxTEST")
	.dwattr $C$DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$676, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$677, DW_AT_name("GSxINIT")
	.dwattr $C$DW$677, DW_AT_TI_symbol_name("_GSxINIT")
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x52]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$678	.dwtag  DW_TAG_member
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$678, DW_AT_name("GSxINITDONE")
	.dwattr $C$DW$678, DW_AT_TI_symbol_name("_GSxINITDONE")
	.dwattr $C$DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$679	.dwtag  DW_TAG_member
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$679, DW_AT_name("rsvd7")
	.dwattr $C$DW$679, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$679, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$680	.dwtag  DW_TAG_member
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$680, DW_AT_name("MSGxTEST")
	.dwattr $C$DW$680, DW_AT_TI_symbol_name("_MSGxTEST")
	.dwattr $C$DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$681	.dwtag  DW_TAG_member
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$681, DW_AT_name("MSGxINIT")
	.dwattr $C$DW$681, DW_AT_TI_symbol_name("_MSGxINIT")
	.dwattr $C$DW$681, DW_AT_data_member_location[DW_OP_plus_uconst 0x72]
	.dwattr $C$DW$681, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$682	.dwtag  DW_TAG_member
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$682, DW_AT_name("MSGxINITDONE")
	.dwattr $C$DW$682, DW_AT_TI_symbol_name("_MSGxINITDONE")
	.dwattr $C$DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$682, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$683	.dwtag  DW_TAG_member
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$683, DW_AT_name("rsvd8")
	.dwattr $C$DW$683, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x76]
	.dwattr $C$DW$683, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$96

$C$DW$684	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$96)

$C$DW$T$171	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$171, DW_AT_type(*$C$DW$684)


$C$DW$T$97	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$97, DW_AT_name("MSGxINITDONE_BITS")
	.dwattr $C$DW$T$97, DW_AT_byte_size(0x02)
$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$685, DW_AT_name("INITDONE_CPUTOCPU")
	.dwattr $C$DW$685, DW_AT_TI_symbol_name("_INITDONE_CPUTOCPU")
	.dwattr $C$DW$685, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$685, DW_AT_bit_size(0x01)
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$686	.dwtag  DW_TAG_member
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$686, DW_AT_name("INITDONE_CPUTOCLA1")
	.dwattr $C$DW$686, DW_AT_TI_symbol_name("_INITDONE_CPUTOCLA1")
	.dwattr $C$DW$686, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$686, DW_AT_bit_size(0x01)
	.dwattr $C$DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$686, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$687	.dwtag  DW_TAG_member
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$687, DW_AT_name("INITDONE_CLA1TOCPU")
	.dwattr $C$DW$687, DW_AT_TI_symbol_name("_INITDONE_CLA1TOCPU")
	.dwattr $C$DW$687, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$687, DW_AT_bit_size(0x01)
	.dwattr $C$DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$688	.dwtag  DW_TAG_member
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$688, DW_AT_name("rsvd1")
	.dwattr $C$DW$688, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$688, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$688, DW_AT_bit_size(0x01)
	.dwattr $C$DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$688, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$689	.dwtag  DW_TAG_member
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$689, DW_AT_name("rsvd2")
	.dwattr $C$DW$689, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$689, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$689, DW_AT_bit_size(0x01)
	.dwattr $C$DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$690	.dwtag  DW_TAG_member
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$690, DW_AT_name("rsvd3")
	.dwattr $C$DW$690, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$690, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$690, DW_AT_bit_size(0x0b)
	.dwattr $C$DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$690, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$691	.dwtag  DW_TAG_member
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$691, DW_AT_name("rsvd4")
	.dwattr $C$DW$691, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$691, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$691, DW_AT_bit_size(0x10)
	.dwattr $C$DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$691, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$97


$C$DW$T$98	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$98, DW_AT_name("MSGxINITDONE_REG")
	.dwattr $C$DW$T$98, DW_AT_byte_size(0x02)
$C$DW$692	.dwtag  DW_TAG_member
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$692, DW_AT_name("all")
	.dwattr $C$DW$692, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$692, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$692, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$693	.dwtag  DW_TAG_member
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$693, DW_AT_name("bit")
	.dwattr $C$DW$693, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$693, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$693, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$98


$C$DW$T$99	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$99, DW_AT_name("MSGxINIT_BITS")
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x02)
$C$DW$694	.dwtag  DW_TAG_member
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$694, DW_AT_name("INIT_CPUTOCPU")
	.dwattr $C$DW$694, DW_AT_TI_symbol_name("_INIT_CPUTOCPU")
	.dwattr $C$DW$694, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$694, DW_AT_bit_size(0x01)
	.dwattr $C$DW$694, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$694, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$695, DW_AT_name("INIT_CPUTOCLA1")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_INIT_CPUTOCLA1")
	.dwattr $C$DW$695, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$695, DW_AT_bit_size(0x01)
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$696, DW_AT_name("INIT_CLA1TOCPU")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_INIT_CLA1TOCPU")
	.dwattr $C$DW$696, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$696, DW_AT_bit_size(0x01)
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$697, DW_AT_name("rsvd1")
	.dwattr $C$DW$697, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$697, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$697, DW_AT_bit_size(0x01)
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$698, DW_AT_name("rsvd2")
	.dwattr $C$DW$698, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$698, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$698, DW_AT_bit_size(0x01)
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$699, DW_AT_name("rsvd3")
	.dwattr $C$DW$699, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$699, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$699, DW_AT_bit_size(0x0b)
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$700	.dwtag  DW_TAG_member
	.dwattr $C$DW$700, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$700, DW_AT_name("rsvd4")
	.dwattr $C$DW$700, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$700, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$700, DW_AT_bit_size(0x10)
	.dwattr $C$DW$700, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$700, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$99


$C$DW$T$100	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$100, DW_AT_name("MSGxINIT_REG")
	.dwattr $C$DW$T$100, DW_AT_byte_size(0x02)
$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$701, DW_AT_name("all")
	.dwattr $C$DW$701, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$702, DW_AT_name("bit")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$100


$C$DW$T$101	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$101, DW_AT_name("MSGxTEST_BITS")
	.dwattr $C$DW$T$101, DW_AT_byte_size(0x02)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$703, DW_AT_name("TEST_CPUTOCPU")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_TEST_CPUTOCPU")
	.dwattr $C$DW$703, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$703, DW_AT_bit_size(0x02)
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$704, DW_AT_name("TEST_CPUTOCLA1")
	.dwattr $C$DW$704, DW_AT_TI_symbol_name("_TEST_CPUTOCLA1")
	.dwattr $C$DW$704, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$704, DW_AT_bit_size(0x02)
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$705, DW_AT_name("TEST_CLA1TOCPU")
	.dwattr $C$DW$705, DW_AT_TI_symbol_name("_TEST_CLA1TOCPU")
	.dwattr $C$DW$705, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$705, DW_AT_bit_size(0x02)
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$706	.dwtag  DW_TAG_member
	.dwattr $C$DW$706, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$706, DW_AT_name("rsvd1")
	.dwattr $C$DW$706, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$706, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$706, DW_AT_bit_size(0x02)
	.dwattr $C$DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$706, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$707, DW_AT_name("rsvd2")
	.dwattr $C$DW$707, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$707, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$707, DW_AT_bit_size(0x02)
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$708	.dwtag  DW_TAG_member
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$708, DW_AT_name("rsvd3")
	.dwattr $C$DW$708, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$708, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$708, DW_AT_bit_size(0x06)
	.dwattr $C$DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$708, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$709, DW_AT_name("rsvd4")
	.dwattr $C$DW$709, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$709, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$709, DW_AT_bit_size(0x10)
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$101


$C$DW$T$102	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$102, DW_AT_name("MSGxTEST_REG")
	.dwattr $C$DW$T$102, DW_AT_byte_size(0x02)
$C$DW$710	.dwtag  DW_TAG_member
	.dwattr $C$DW$710, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$710, DW_AT_name("all")
	.dwattr $C$DW$710, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$710, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$711	.dwtag  DW_TAG_member
	.dwattr $C$DW$711, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$711, DW_AT_name("bit")
	.dwattr $C$DW$711, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$711, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$102


$C$DW$T$103	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$103, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$103, DW_AT_byte_size(0x01)
$C$DW$712	.dwtag  DW_TAG_member
	.dwattr $C$DW$712, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$712, DW_AT_name("ACK1")
	.dwattr $C$DW$712, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$712, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$712, DW_AT_bit_size(0x01)
	.dwattr $C$DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$712, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$713	.dwtag  DW_TAG_member
	.dwattr $C$DW$713, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$713, DW_AT_name("ACK2")
	.dwattr $C$DW$713, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$713, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$713, DW_AT_bit_size(0x01)
	.dwattr $C$DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$713, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$714	.dwtag  DW_TAG_member
	.dwattr $C$DW$714, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$714, DW_AT_name("ACK3")
	.dwattr $C$DW$714, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$714, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$714, DW_AT_bit_size(0x01)
	.dwattr $C$DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$714, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$715, DW_AT_name("ACK4")
	.dwattr $C$DW$715, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$715, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$715, DW_AT_bit_size(0x01)
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$716	.dwtag  DW_TAG_member
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$716, DW_AT_name("ACK5")
	.dwattr $C$DW$716, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$716, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$716, DW_AT_bit_size(0x01)
	.dwattr $C$DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$717	.dwtag  DW_TAG_member
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$717, DW_AT_name("ACK6")
	.dwattr $C$DW$717, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$717, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$717, DW_AT_bit_size(0x01)
	.dwattr $C$DW$717, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$717, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$718	.dwtag  DW_TAG_member
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$718, DW_AT_name("ACK7")
	.dwattr $C$DW$718, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$718, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$718, DW_AT_bit_size(0x01)
	.dwattr $C$DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$719	.dwtag  DW_TAG_member
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$719, DW_AT_name("ACK8")
	.dwattr $C$DW$719, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$719, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$719, DW_AT_bit_size(0x01)
	.dwattr $C$DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$719, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$720	.dwtag  DW_TAG_member
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$720, DW_AT_name("ACK9")
	.dwattr $C$DW$720, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$720, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$720, DW_AT_bit_size(0x01)
	.dwattr $C$DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$720, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$721	.dwtag  DW_TAG_member
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$721, DW_AT_name("ACK10")
	.dwattr $C$DW$721, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$721, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$721, DW_AT_bit_size(0x01)
	.dwattr $C$DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$721, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$722, DW_AT_name("ACK11")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$722, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$722, DW_AT_bit_size(0x01)
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$723, DW_AT_name("ACK12")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$723, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$723, DW_AT_bit_size(0x01)
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$724, DW_AT_name("rsvd1")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$724, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$724, DW_AT_bit_size(0x04)
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$103


$C$DW$T$104	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$104, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$104, DW_AT_byte_size(0x01)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$725, DW_AT_name("all")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$726, DW_AT_name("bit")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$104


$C$DW$T$105	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$105, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$105, DW_AT_byte_size(0x01)
$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$727, DW_AT_name("ENPIE")
	.dwattr $C$DW$727, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$727, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$727, DW_AT_bit_size(0x01)
	.dwattr $C$DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$728, DW_AT_name("PIEVECT")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$728, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$728, DW_AT_bit_size(0x0f)
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$105


$C$DW$T$106	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$106, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x01)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$729, DW_AT_name("all")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$730, DW_AT_name("bit")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$106


$C$DW$T$107	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$107, DW_AT_name("PIEIER10_BITS")
	.dwattr $C$DW$T$107, DW_AT_byte_size(0x01)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$731, DW_AT_name("INTx1")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$731, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$731, DW_AT_bit_size(0x01)
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$732, DW_AT_name("INTx2")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$732, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$732, DW_AT_bit_size(0x01)
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$733, DW_AT_name("INTx3")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$733, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$733, DW_AT_bit_size(0x01)
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$734, DW_AT_name("INTx4")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$734, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$734, DW_AT_bit_size(0x01)
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$735, DW_AT_name("INTx5")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$735, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$735, DW_AT_bit_size(0x01)
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$736, DW_AT_name("INTx6")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$736, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$736, DW_AT_bit_size(0x01)
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$737, DW_AT_name("INTx7")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$737, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$737, DW_AT_bit_size(0x01)
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$738, DW_AT_name("INTx8")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$738, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$738, DW_AT_bit_size(0x01)
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$739, DW_AT_name("INTx9")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$739, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$739, DW_AT_bit_size(0x01)
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$740, DW_AT_name("INTx10")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$740, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$740, DW_AT_bit_size(0x01)
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$741, DW_AT_name("INTx11")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$741, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$741, DW_AT_bit_size(0x01)
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$742, DW_AT_name("INTx12")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$742, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$742, DW_AT_bit_size(0x01)
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$743, DW_AT_name("INTx13")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$743, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$743, DW_AT_bit_size(0x01)
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$744, DW_AT_name("INTx14")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$744, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$744, DW_AT_bit_size(0x01)
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$745, DW_AT_name("INTx15")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$745, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$745, DW_AT_bit_size(0x01)
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$746, DW_AT_name("INTx16")
	.dwattr $C$DW$746, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$746, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$746, DW_AT_bit_size(0x01)
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$107


$C$DW$T$108	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$108, DW_AT_name("PIEIER10_REG")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x01)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$747, DW_AT_name("all")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$748, DW_AT_name("bit")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$108


$C$DW$T$109	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$109, DW_AT_name("PIEIER11_BITS")
	.dwattr $C$DW$T$109, DW_AT_byte_size(0x01)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$749, DW_AT_name("INTx1")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$749, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$749, DW_AT_bit_size(0x01)
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$750, DW_AT_name("INTx2")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$750, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$750, DW_AT_bit_size(0x01)
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$751, DW_AT_name("INTx3")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$751, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$751, DW_AT_bit_size(0x01)
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$752, DW_AT_name("INTx4")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$752, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$752, DW_AT_bit_size(0x01)
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$753, DW_AT_name("INTx5")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$753, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$753, DW_AT_bit_size(0x01)
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$754, DW_AT_name("INTx6")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$754, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$754, DW_AT_bit_size(0x01)
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$755, DW_AT_name("INTx7")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$755, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$755, DW_AT_bit_size(0x01)
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$756, DW_AT_name("INTx8")
	.dwattr $C$DW$756, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$756, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$756, DW_AT_bit_size(0x01)
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$757, DW_AT_name("INTx9")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$757, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$757, DW_AT_bit_size(0x01)
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$758, DW_AT_name("INTx10")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$758, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$758, DW_AT_bit_size(0x01)
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$759, DW_AT_name("INTx11")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$759, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$759, DW_AT_bit_size(0x01)
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$760, DW_AT_name("INTx12")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$760, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$760, DW_AT_bit_size(0x01)
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$761, DW_AT_name("INTx13")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$761, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$761, DW_AT_bit_size(0x01)
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$762, DW_AT_name("INTx14")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$762, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$762, DW_AT_bit_size(0x01)
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$763, DW_AT_name("INTx15")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$763, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$763, DW_AT_bit_size(0x01)
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$764, DW_AT_name("INTx16")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$764, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$764, DW_AT_bit_size(0x01)
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$109


$C$DW$T$110	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$110, DW_AT_name("PIEIER11_REG")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x01)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$765, DW_AT_name("all")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$766, DW_AT_name("bit")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$110


$C$DW$T$111	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$111, DW_AT_name("PIEIER12_BITS")
	.dwattr $C$DW$T$111, DW_AT_byte_size(0x01)
$C$DW$767	.dwtag  DW_TAG_member
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$767, DW_AT_name("INTx1")
	.dwattr $C$DW$767, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$767, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$767, DW_AT_bit_size(0x01)
	.dwattr $C$DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$767, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$768, DW_AT_name("INTx2")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$768, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$768, DW_AT_bit_size(0x01)
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$769, DW_AT_name("INTx3")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$769, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$769, DW_AT_bit_size(0x01)
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$770, DW_AT_name("INTx4")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$770, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$770, DW_AT_bit_size(0x01)
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$771, DW_AT_name("INTx5")
	.dwattr $C$DW$771, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$771, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$771, DW_AT_bit_size(0x01)
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$772, DW_AT_name("INTx6")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$772, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$772, DW_AT_bit_size(0x01)
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$773, DW_AT_name("INTx7")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$773, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$773, DW_AT_bit_size(0x01)
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$774, DW_AT_name("INTx8")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$774, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$774, DW_AT_bit_size(0x01)
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$775, DW_AT_name("INTx9")
	.dwattr $C$DW$775, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$775, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$775, DW_AT_bit_size(0x01)
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$776, DW_AT_name("INTx10")
	.dwattr $C$DW$776, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$776, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$776, DW_AT_bit_size(0x01)
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$777, DW_AT_name("INTx11")
	.dwattr $C$DW$777, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$777, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$777, DW_AT_bit_size(0x01)
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$778, DW_AT_name("INTx12")
	.dwattr $C$DW$778, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$778, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$778, DW_AT_bit_size(0x01)
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$779, DW_AT_name("INTx13")
	.dwattr $C$DW$779, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$779, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$779, DW_AT_bit_size(0x01)
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$780, DW_AT_name("INTx14")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$780, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$780, DW_AT_bit_size(0x01)
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$781, DW_AT_name("INTx15")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$781, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$781, DW_AT_bit_size(0x01)
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$782, DW_AT_name("INTx16")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$782, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$782, DW_AT_bit_size(0x01)
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$111


$C$DW$T$112	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$112, DW_AT_name("PIEIER12_REG")
	.dwattr $C$DW$T$112, DW_AT_byte_size(0x01)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$783, DW_AT_name("all")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$784, DW_AT_name("bit")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$112


$C$DW$T$113	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$113, DW_AT_name("PIEIER1_BITS")
	.dwattr $C$DW$T$113, DW_AT_byte_size(0x01)
$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$785, DW_AT_name("INTx1")
	.dwattr $C$DW$785, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$785, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$785, DW_AT_bit_size(0x01)
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$786, DW_AT_name("INTx2")
	.dwattr $C$DW$786, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$786, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$786, DW_AT_bit_size(0x01)
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$787	.dwtag  DW_TAG_member
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$787, DW_AT_name("INTx3")
	.dwattr $C$DW$787, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$787, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$787, DW_AT_bit_size(0x01)
	.dwattr $C$DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$787, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$788, DW_AT_name("INTx4")
	.dwattr $C$DW$788, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$788, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$788, DW_AT_bit_size(0x01)
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$789, DW_AT_name("INTx5")
	.dwattr $C$DW$789, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$789, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$789, DW_AT_bit_size(0x01)
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$790	.dwtag  DW_TAG_member
	.dwattr $C$DW$790, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$790, DW_AT_name("INTx6")
	.dwattr $C$DW$790, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$790, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$790, DW_AT_bit_size(0x01)
	.dwattr $C$DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$790, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$791, DW_AT_name("INTx7")
	.dwattr $C$DW$791, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$791, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$791, DW_AT_bit_size(0x01)
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$792, DW_AT_name("INTx8")
	.dwattr $C$DW$792, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$792, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$792, DW_AT_bit_size(0x01)
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$793, DW_AT_name("INTx9")
	.dwattr $C$DW$793, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$793, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$793, DW_AT_bit_size(0x01)
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$794	.dwtag  DW_TAG_member
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$794, DW_AT_name("INTx10")
	.dwattr $C$DW$794, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$794, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$794, DW_AT_bit_size(0x01)
	.dwattr $C$DW$794, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$794, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$795	.dwtag  DW_TAG_member
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$795, DW_AT_name("INTx11")
	.dwattr $C$DW$795, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$795, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$795, DW_AT_bit_size(0x01)
	.dwattr $C$DW$795, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$795, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$796	.dwtag  DW_TAG_member
	.dwattr $C$DW$796, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$796, DW_AT_name("INTx12")
	.dwattr $C$DW$796, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$796, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$796, DW_AT_bit_size(0x01)
	.dwattr $C$DW$796, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$796, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$797	.dwtag  DW_TAG_member
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$797, DW_AT_name("INTx13")
	.dwattr $C$DW$797, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$797, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$797, DW_AT_bit_size(0x01)
	.dwattr $C$DW$797, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$797, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$798	.dwtag  DW_TAG_member
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$798, DW_AT_name("INTx14")
	.dwattr $C$DW$798, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$798, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$798, DW_AT_bit_size(0x01)
	.dwattr $C$DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$798, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$799	.dwtag  DW_TAG_member
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$799, DW_AT_name("INTx15")
	.dwattr $C$DW$799, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$799, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$799, DW_AT_bit_size(0x01)
	.dwattr $C$DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$799, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$800	.dwtag  DW_TAG_member
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$800, DW_AT_name("INTx16")
	.dwattr $C$DW$800, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$800, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$800, DW_AT_bit_size(0x01)
	.dwattr $C$DW$800, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$800, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$113


$C$DW$T$114	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$114, DW_AT_name("PIEIER1_REG")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x01)
$C$DW$801	.dwtag  DW_TAG_member
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$801, DW_AT_name("all")
	.dwattr $C$DW$801, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$801, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$801, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$802	.dwtag  DW_TAG_member
	.dwattr $C$DW$802, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$802, DW_AT_name("bit")
	.dwattr $C$DW$802, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$802, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$802, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$114


$C$DW$T$115	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$115, DW_AT_name("PIEIER2_BITS")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x01)
$C$DW$803	.dwtag  DW_TAG_member
	.dwattr $C$DW$803, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$803, DW_AT_name("INTx1")
	.dwattr $C$DW$803, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$803, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$803, DW_AT_bit_size(0x01)
	.dwattr $C$DW$803, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$803, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$804	.dwtag  DW_TAG_member
	.dwattr $C$DW$804, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$804, DW_AT_name("INTx2")
	.dwattr $C$DW$804, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$804, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$804, DW_AT_bit_size(0x01)
	.dwattr $C$DW$804, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$804, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$805	.dwtag  DW_TAG_member
	.dwattr $C$DW$805, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$805, DW_AT_name("INTx3")
	.dwattr $C$DW$805, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$805, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$805, DW_AT_bit_size(0x01)
	.dwattr $C$DW$805, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$805, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$806	.dwtag  DW_TAG_member
	.dwattr $C$DW$806, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$806, DW_AT_name("INTx4")
	.dwattr $C$DW$806, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$806, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$806, DW_AT_bit_size(0x01)
	.dwattr $C$DW$806, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$806, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$807	.dwtag  DW_TAG_member
	.dwattr $C$DW$807, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$807, DW_AT_name("INTx5")
	.dwattr $C$DW$807, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$807, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$807, DW_AT_bit_size(0x01)
	.dwattr $C$DW$807, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$807, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$808	.dwtag  DW_TAG_member
	.dwattr $C$DW$808, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$808, DW_AT_name("INTx6")
	.dwattr $C$DW$808, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$808, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$808, DW_AT_bit_size(0x01)
	.dwattr $C$DW$808, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$808, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$809	.dwtag  DW_TAG_member
	.dwattr $C$DW$809, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$809, DW_AT_name("INTx7")
	.dwattr $C$DW$809, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$809, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$809, DW_AT_bit_size(0x01)
	.dwattr $C$DW$809, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$809, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$810	.dwtag  DW_TAG_member
	.dwattr $C$DW$810, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$810, DW_AT_name("INTx8")
	.dwattr $C$DW$810, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$810, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$810, DW_AT_bit_size(0x01)
	.dwattr $C$DW$810, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$810, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$811	.dwtag  DW_TAG_member
	.dwattr $C$DW$811, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$811, DW_AT_name("INTx9")
	.dwattr $C$DW$811, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$811, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$811, DW_AT_bit_size(0x01)
	.dwattr $C$DW$811, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$811, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$812	.dwtag  DW_TAG_member
	.dwattr $C$DW$812, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$812, DW_AT_name("INTx10")
	.dwattr $C$DW$812, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$812, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$812, DW_AT_bit_size(0x01)
	.dwattr $C$DW$812, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$812, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$813	.dwtag  DW_TAG_member
	.dwattr $C$DW$813, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$813, DW_AT_name("INTx11")
	.dwattr $C$DW$813, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$813, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$813, DW_AT_bit_size(0x01)
	.dwattr $C$DW$813, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$813, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$814	.dwtag  DW_TAG_member
	.dwattr $C$DW$814, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$814, DW_AT_name("INTx12")
	.dwattr $C$DW$814, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$814, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$814, DW_AT_bit_size(0x01)
	.dwattr $C$DW$814, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$814, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$815	.dwtag  DW_TAG_member
	.dwattr $C$DW$815, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$815, DW_AT_name("INTx13")
	.dwattr $C$DW$815, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$815, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$815, DW_AT_bit_size(0x01)
	.dwattr $C$DW$815, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$815, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$816	.dwtag  DW_TAG_member
	.dwattr $C$DW$816, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$816, DW_AT_name("INTx14")
	.dwattr $C$DW$816, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$816, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$816, DW_AT_bit_size(0x01)
	.dwattr $C$DW$816, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$816, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$817	.dwtag  DW_TAG_member
	.dwattr $C$DW$817, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$817, DW_AT_name("INTx15")
	.dwattr $C$DW$817, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$817, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$817, DW_AT_bit_size(0x01)
	.dwattr $C$DW$817, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$817, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$818	.dwtag  DW_TAG_member
	.dwattr $C$DW$818, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$818, DW_AT_name("INTx16")
	.dwattr $C$DW$818, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$818, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$818, DW_AT_bit_size(0x01)
	.dwattr $C$DW$818, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$818, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$115


$C$DW$T$116	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$116, DW_AT_name("PIEIER2_REG")
	.dwattr $C$DW$T$116, DW_AT_byte_size(0x01)
$C$DW$819	.dwtag  DW_TAG_member
	.dwattr $C$DW$819, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$819, DW_AT_name("all")
	.dwattr $C$DW$819, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$819, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$819, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$820	.dwtag  DW_TAG_member
	.dwattr $C$DW$820, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$820, DW_AT_name("bit")
	.dwattr $C$DW$820, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$820, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$820, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$116


$C$DW$T$117	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$117, DW_AT_name("PIEIER3_BITS")
	.dwattr $C$DW$T$117, DW_AT_byte_size(0x01)
$C$DW$821	.dwtag  DW_TAG_member
	.dwattr $C$DW$821, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$821, DW_AT_name("INTx1")
	.dwattr $C$DW$821, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$821, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$821, DW_AT_bit_size(0x01)
	.dwattr $C$DW$821, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$821, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$822	.dwtag  DW_TAG_member
	.dwattr $C$DW$822, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$822, DW_AT_name("INTx2")
	.dwattr $C$DW$822, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$822, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$822, DW_AT_bit_size(0x01)
	.dwattr $C$DW$822, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$822, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$823	.dwtag  DW_TAG_member
	.dwattr $C$DW$823, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$823, DW_AT_name("INTx3")
	.dwattr $C$DW$823, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$823, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$823, DW_AT_bit_size(0x01)
	.dwattr $C$DW$823, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$823, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$824	.dwtag  DW_TAG_member
	.dwattr $C$DW$824, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$824, DW_AT_name("INTx4")
	.dwattr $C$DW$824, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$824, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$824, DW_AT_bit_size(0x01)
	.dwattr $C$DW$824, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$824, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$825	.dwtag  DW_TAG_member
	.dwattr $C$DW$825, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$825, DW_AT_name("INTx5")
	.dwattr $C$DW$825, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$825, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$825, DW_AT_bit_size(0x01)
	.dwattr $C$DW$825, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$825, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$826	.dwtag  DW_TAG_member
	.dwattr $C$DW$826, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$826, DW_AT_name("INTx6")
	.dwattr $C$DW$826, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$826, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$826, DW_AT_bit_size(0x01)
	.dwattr $C$DW$826, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$826, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$827	.dwtag  DW_TAG_member
	.dwattr $C$DW$827, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$827, DW_AT_name("INTx7")
	.dwattr $C$DW$827, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$827, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$827, DW_AT_bit_size(0x01)
	.dwattr $C$DW$827, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$827, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$828	.dwtag  DW_TAG_member
	.dwattr $C$DW$828, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$828, DW_AT_name("INTx8")
	.dwattr $C$DW$828, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$828, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$828, DW_AT_bit_size(0x01)
	.dwattr $C$DW$828, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$828, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$829	.dwtag  DW_TAG_member
	.dwattr $C$DW$829, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$829, DW_AT_name("INTx9")
	.dwattr $C$DW$829, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$829, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$829, DW_AT_bit_size(0x01)
	.dwattr $C$DW$829, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$829, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$830	.dwtag  DW_TAG_member
	.dwattr $C$DW$830, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$830, DW_AT_name("INTx10")
	.dwattr $C$DW$830, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$830, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$830, DW_AT_bit_size(0x01)
	.dwattr $C$DW$830, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$830, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$831	.dwtag  DW_TAG_member
	.dwattr $C$DW$831, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$831, DW_AT_name("INTx11")
	.dwattr $C$DW$831, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$831, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$831, DW_AT_bit_size(0x01)
	.dwattr $C$DW$831, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$831, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$832	.dwtag  DW_TAG_member
	.dwattr $C$DW$832, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$832, DW_AT_name("INTx12")
	.dwattr $C$DW$832, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$832, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$832, DW_AT_bit_size(0x01)
	.dwattr $C$DW$832, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$832, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$833	.dwtag  DW_TAG_member
	.dwattr $C$DW$833, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$833, DW_AT_name("INTx13")
	.dwattr $C$DW$833, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$833, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$833, DW_AT_bit_size(0x01)
	.dwattr $C$DW$833, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$833, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$834	.dwtag  DW_TAG_member
	.dwattr $C$DW$834, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$834, DW_AT_name("INTx14")
	.dwattr $C$DW$834, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$834, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$834, DW_AT_bit_size(0x01)
	.dwattr $C$DW$834, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$834, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$835	.dwtag  DW_TAG_member
	.dwattr $C$DW$835, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$835, DW_AT_name("INTx15")
	.dwattr $C$DW$835, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$835, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$835, DW_AT_bit_size(0x01)
	.dwattr $C$DW$835, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$835, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$836	.dwtag  DW_TAG_member
	.dwattr $C$DW$836, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$836, DW_AT_name("INTx16")
	.dwattr $C$DW$836, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$836, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$836, DW_AT_bit_size(0x01)
	.dwattr $C$DW$836, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$836, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$117


$C$DW$T$118	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$118, DW_AT_name("PIEIER3_REG")
	.dwattr $C$DW$T$118, DW_AT_byte_size(0x01)
$C$DW$837	.dwtag  DW_TAG_member
	.dwattr $C$DW$837, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$837, DW_AT_name("all")
	.dwattr $C$DW$837, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$837, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$837, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$838	.dwtag  DW_TAG_member
	.dwattr $C$DW$838, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$838, DW_AT_name("bit")
	.dwattr $C$DW$838, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$838, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$838, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$118


$C$DW$T$119	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$119, DW_AT_name("PIEIER4_BITS")
	.dwattr $C$DW$T$119, DW_AT_byte_size(0x01)
$C$DW$839	.dwtag  DW_TAG_member
	.dwattr $C$DW$839, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$839, DW_AT_name("INTx1")
	.dwattr $C$DW$839, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$839, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$839, DW_AT_bit_size(0x01)
	.dwattr $C$DW$839, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$839, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$840	.dwtag  DW_TAG_member
	.dwattr $C$DW$840, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$840, DW_AT_name("INTx2")
	.dwattr $C$DW$840, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$840, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$840, DW_AT_bit_size(0x01)
	.dwattr $C$DW$840, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$840, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$841	.dwtag  DW_TAG_member
	.dwattr $C$DW$841, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$841, DW_AT_name("INTx3")
	.dwattr $C$DW$841, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$841, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$841, DW_AT_bit_size(0x01)
	.dwattr $C$DW$841, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$841, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$842	.dwtag  DW_TAG_member
	.dwattr $C$DW$842, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$842, DW_AT_name("INTx4")
	.dwattr $C$DW$842, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$842, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$842, DW_AT_bit_size(0x01)
	.dwattr $C$DW$842, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$842, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$843	.dwtag  DW_TAG_member
	.dwattr $C$DW$843, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$843, DW_AT_name("INTx5")
	.dwattr $C$DW$843, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$843, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$843, DW_AT_bit_size(0x01)
	.dwattr $C$DW$843, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$843, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$844	.dwtag  DW_TAG_member
	.dwattr $C$DW$844, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$844, DW_AT_name("INTx6")
	.dwattr $C$DW$844, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$844, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$844, DW_AT_bit_size(0x01)
	.dwattr $C$DW$844, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$844, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$845	.dwtag  DW_TAG_member
	.dwattr $C$DW$845, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$845, DW_AT_name("INTx7")
	.dwattr $C$DW$845, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$845, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$845, DW_AT_bit_size(0x01)
	.dwattr $C$DW$845, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$845, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$846	.dwtag  DW_TAG_member
	.dwattr $C$DW$846, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$846, DW_AT_name("INTx8")
	.dwattr $C$DW$846, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$846, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$846, DW_AT_bit_size(0x01)
	.dwattr $C$DW$846, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$846, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$847	.dwtag  DW_TAG_member
	.dwattr $C$DW$847, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$847, DW_AT_name("INTx9")
	.dwattr $C$DW$847, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$847, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$847, DW_AT_bit_size(0x01)
	.dwattr $C$DW$847, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$847, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$848	.dwtag  DW_TAG_member
	.dwattr $C$DW$848, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$848, DW_AT_name("INTx10")
	.dwattr $C$DW$848, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$848, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$848, DW_AT_bit_size(0x01)
	.dwattr $C$DW$848, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$848, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$849	.dwtag  DW_TAG_member
	.dwattr $C$DW$849, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$849, DW_AT_name("INTx11")
	.dwattr $C$DW$849, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$849, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$849, DW_AT_bit_size(0x01)
	.dwattr $C$DW$849, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$849, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$850	.dwtag  DW_TAG_member
	.dwattr $C$DW$850, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$850, DW_AT_name("INTx12")
	.dwattr $C$DW$850, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$850, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$850, DW_AT_bit_size(0x01)
	.dwattr $C$DW$850, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$850, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$851	.dwtag  DW_TAG_member
	.dwattr $C$DW$851, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$851, DW_AT_name("INTx13")
	.dwattr $C$DW$851, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$851, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$851, DW_AT_bit_size(0x01)
	.dwattr $C$DW$851, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$851, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$852	.dwtag  DW_TAG_member
	.dwattr $C$DW$852, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$852, DW_AT_name("INTx14")
	.dwattr $C$DW$852, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$852, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$852, DW_AT_bit_size(0x01)
	.dwattr $C$DW$852, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$852, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$853	.dwtag  DW_TAG_member
	.dwattr $C$DW$853, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$853, DW_AT_name("INTx15")
	.dwattr $C$DW$853, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$853, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$853, DW_AT_bit_size(0x01)
	.dwattr $C$DW$853, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$853, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$854	.dwtag  DW_TAG_member
	.dwattr $C$DW$854, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$854, DW_AT_name("INTx16")
	.dwattr $C$DW$854, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$854, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$854, DW_AT_bit_size(0x01)
	.dwattr $C$DW$854, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$854, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$119


$C$DW$T$120	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$120, DW_AT_name("PIEIER4_REG")
	.dwattr $C$DW$T$120, DW_AT_byte_size(0x01)
$C$DW$855	.dwtag  DW_TAG_member
	.dwattr $C$DW$855, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$855, DW_AT_name("all")
	.dwattr $C$DW$855, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$855, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$855, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$856	.dwtag  DW_TAG_member
	.dwattr $C$DW$856, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$856, DW_AT_name("bit")
	.dwattr $C$DW$856, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$856, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$856, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$120


$C$DW$T$121	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$121, DW_AT_name("PIEIER5_BITS")
	.dwattr $C$DW$T$121, DW_AT_byte_size(0x01)
$C$DW$857	.dwtag  DW_TAG_member
	.dwattr $C$DW$857, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$857, DW_AT_name("INTx1")
	.dwattr $C$DW$857, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$857, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$857, DW_AT_bit_size(0x01)
	.dwattr $C$DW$857, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$857, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$858	.dwtag  DW_TAG_member
	.dwattr $C$DW$858, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$858, DW_AT_name("INTx2")
	.dwattr $C$DW$858, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$858, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$858, DW_AT_bit_size(0x01)
	.dwattr $C$DW$858, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$858, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$859	.dwtag  DW_TAG_member
	.dwattr $C$DW$859, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$859, DW_AT_name("INTx3")
	.dwattr $C$DW$859, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$859, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$859, DW_AT_bit_size(0x01)
	.dwattr $C$DW$859, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$859, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$860	.dwtag  DW_TAG_member
	.dwattr $C$DW$860, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$860, DW_AT_name("INTx4")
	.dwattr $C$DW$860, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$860, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$860, DW_AT_bit_size(0x01)
	.dwattr $C$DW$860, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$860, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$861	.dwtag  DW_TAG_member
	.dwattr $C$DW$861, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$861, DW_AT_name("INTx5")
	.dwattr $C$DW$861, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$861, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$861, DW_AT_bit_size(0x01)
	.dwattr $C$DW$861, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$861, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$862	.dwtag  DW_TAG_member
	.dwattr $C$DW$862, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$862, DW_AT_name("INTx6")
	.dwattr $C$DW$862, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$862, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$862, DW_AT_bit_size(0x01)
	.dwattr $C$DW$862, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$862, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$863	.dwtag  DW_TAG_member
	.dwattr $C$DW$863, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$863, DW_AT_name("INTx7")
	.dwattr $C$DW$863, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$863, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$863, DW_AT_bit_size(0x01)
	.dwattr $C$DW$863, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$863, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$864	.dwtag  DW_TAG_member
	.dwattr $C$DW$864, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$864, DW_AT_name("INTx8")
	.dwattr $C$DW$864, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$864, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$864, DW_AT_bit_size(0x01)
	.dwattr $C$DW$864, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$864, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$865	.dwtag  DW_TAG_member
	.dwattr $C$DW$865, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$865, DW_AT_name("INTx9")
	.dwattr $C$DW$865, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$865, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$865, DW_AT_bit_size(0x01)
	.dwattr $C$DW$865, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$865, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$866	.dwtag  DW_TAG_member
	.dwattr $C$DW$866, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$866, DW_AT_name("INTx10")
	.dwattr $C$DW$866, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$866, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$866, DW_AT_bit_size(0x01)
	.dwattr $C$DW$866, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$866, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$867	.dwtag  DW_TAG_member
	.dwattr $C$DW$867, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$867, DW_AT_name("INTx11")
	.dwattr $C$DW$867, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$867, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$867, DW_AT_bit_size(0x01)
	.dwattr $C$DW$867, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$867, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$868	.dwtag  DW_TAG_member
	.dwattr $C$DW$868, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$868, DW_AT_name("INTx12")
	.dwattr $C$DW$868, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$868, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$868, DW_AT_bit_size(0x01)
	.dwattr $C$DW$868, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$868, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$869	.dwtag  DW_TAG_member
	.dwattr $C$DW$869, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$869, DW_AT_name("INTx13")
	.dwattr $C$DW$869, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$869, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$869, DW_AT_bit_size(0x01)
	.dwattr $C$DW$869, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$869, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$870	.dwtag  DW_TAG_member
	.dwattr $C$DW$870, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$870, DW_AT_name("INTx14")
	.dwattr $C$DW$870, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$870, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$870, DW_AT_bit_size(0x01)
	.dwattr $C$DW$870, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$870, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$871	.dwtag  DW_TAG_member
	.dwattr $C$DW$871, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$871, DW_AT_name("INTx15")
	.dwattr $C$DW$871, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$871, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$871, DW_AT_bit_size(0x01)
	.dwattr $C$DW$871, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$871, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$872	.dwtag  DW_TAG_member
	.dwattr $C$DW$872, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$872, DW_AT_name("INTx16")
	.dwattr $C$DW$872, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$872, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$872, DW_AT_bit_size(0x01)
	.dwattr $C$DW$872, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$872, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$121


$C$DW$T$122	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$122, DW_AT_name("PIEIER5_REG")
	.dwattr $C$DW$T$122, DW_AT_byte_size(0x01)
$C$DW$873	.dwtag  DW_TAG_member
	.dwattr $C$DW$873, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$873, DW_AT_name("all")
	.dwattr $C$DW$873, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$873, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$873, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$874	.dwtag  DW_TAG_member
	.dwattr $C$DW$874, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$874, DW_AT_name("bit")
	.dwattr $C$DW$874, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$874, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$874, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$122


$C$DW$T$123	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$123, DW_AT_name("PIEIER6_BITS")
	.dwattr $C$DW$T$123, DW_AT_byte_size(0x01)
$C$DW$875	.dwtag  DW_TAG_member
	.dwattr $C$DW$875, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$875, DW_AT_name("INTx1")
	.dwattr $C$DW$875, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$875, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$875, DW_AT_bit_size(0x01)
	.dwattr $C$DW$875, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$875, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$876	.dwtag  DW_TAG_member
	.dwattr $C$DW$876, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$876, DW_AT_name("INTx2")
	.dwattr $C$DW$876, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$876, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$876, DW_AT_bit_size(0x01)
	.dwattr $C$DW$876, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$876, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$877	.dwtag  DW_TAG_member
	.dwattr $C$DW$877, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$877, DW_AT_name("INTx3")
	.dwattr $C$DW$877, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$877, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$877, DW_AT_bit_size(0x01)
	.dwattr $C$DW$877, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$877, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$878	.dwtag  DW_TAG_member
	.dwattr $C$DW$878, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$878, DW_AT_name("INTx4")
	.dwattr $C$DW$878, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$878, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$878, DW_AT_bit_size(0x01)
	.dwattr $C$DW$878, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$878, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$879	.dwtag  DW_TAG_member
	.dwattr $C$DW$879, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$879, DW_AT_name("INTx5")
	.dwattr $C$DW$879, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$879, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$879, DW_AT_bit_size(0x01)
	.dwattr $C$DW$879, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$879, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$880	.dwtag  DW_TAG_member
	.dwattr $C$DW$880, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$880, DW_AT_name("INTx6")
	.dwattr $C$DW$880, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$880, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$880, DW_AT_bit_size(0x01)
	.dwattr $C$DW$880, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$880, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$881	.dwtag  DW_TAG_member
	.dwattr $C$DW$881, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$881, DW_AT_name("INTx7")
	.dwattr $C$DW$881, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$881, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$881, DW_AT_bit_size(0x01)
	.dwattr $C$DW$881, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$881, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$882	.dwtag  DW_TAG_member
	.dwattr $C$DW$882, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$882, DW_AT_name("INTx8")
	.dwattr $C$DW$882, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$882, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$882, DW_AT_bit_size(0x01)
	.dwattr $C$DW$882, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$882, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$883	.dwtag  DW_TAG_member
	.dwattr $C$DW$883, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$883, DW_AT_name("INTx9")
	.dwattr $C$DW$883, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$883, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$883, DW_AT_bit_size(0x01)
	.dwattr $C$DW$883, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$883, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$884	.dwtag  DW_TAG_member
	.dwattr $C$DW$884, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$884, DW_AT_name("INTx10")
	.dwattr $C$DW$884, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$884, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$884, DW_AT_bit_size(0x01)
	.dwattr $C$DW$884, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$884, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$885	.dwtag  DW_TAG_member
	.dwattr $C$DW$885, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$885, DW_AT_name("INTx11")
	.dwattr $C$DW$885, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$885, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$885, DW_AT_bit_size(0x01)
	.dwattr $C$DW$885, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$885, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$886	.dwtag  DW_TAG_member
	.dwattr $C$DW$886, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$886, DW_AT_name("INTx12")
	.dwattr $C$DW$886, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$886, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$886, DW_AT_bit_size(0x01)
	.dwattr $C$DW$886, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$886, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$887	.dwtag  DW_TAG_member
	.dwattr $C$DW$887, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$887, DW_AT_name("INTx13")
	.dwattr $C$DW$887, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$887, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$887, DW_AT_bit_size(0x01)
	.dwattr $C$DW$887, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$887, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$888	.dwtag  DW_TAG_member
	.dwattr $C$DW$888, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$888, DW_AT_name("INTx14")
	.dwattr $C$DW$888, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$888, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$888, DW_AT_bit_size(0x01)
	.dwattr $C$DW$888, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$888, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$889	.dwtag  DW_TAG_member
	.dwattr $C$DW$889, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$889, DW_AT_name("INTx15")
	.dwattr $C$DW$889, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$889, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$889, DW_AT_bit_size(0x01)
	.dwattr $C$DW$889, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$889, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$890	.dwtag  DW_TAG_member
	.dwattr $C$DW$890, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$890, DW_AT_name("INTx16")
	.dwattr $C$DW$890, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$890, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$890, DW_AT_bit_size(0x01)
	.dwattr $C$DW$890, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$890, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$123


$C$DW$T$124	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$124, DW_AT_name("PIEIER6_REG")
	.dwattr $C$DW$T$124, DW_AT_byte_size(0x01)
$C$DW$891	.dwtag  DW_TAG_member
	.dwattr $C$DW$891, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$891, DW_AT_name("all")
	.dwattr $C$DW$891, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$891, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$891, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$892	.dwtag  DW_TAG_member
	.dwattr $C$DW$892, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$892, DW_AT_name("bit")
	.dwattr $C$DW$892, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$892, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$892, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$124


$C$DW$T$125	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$125, DW_AT_name("PIEIER7_BITS")
	.dwattr $C$DW$T$125, DW_AT_byte_size(0x01)
$C$DW$893	.dwtag  DW_TAG_member
	.dwattr $C$DW$893, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$893, DW_AT_name("INTx1")
	.dwattr $C$DW$893, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$893, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$893, DW_AT_bit_size(0x01)
	.dwattr $C$DW$893, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$893, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$894	.dwtag  DW_TAG_member
	.dwattr $C$DW$894, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$894, DW_AT_name("INTx2")
	.dwattr $C$DW$894, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$894, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$894, DW_AT_bit_size(0x01)
	.dwattr $C$DW$894, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$894, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$895	.dwtag  DW_TAG_member
	.dwattr $C$DW$895, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$895, DW_AT_name("INTx3")
	.dwattr $C$DW$895, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$895, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$895, DW_AT_bit_size(0x01)
	.dwattr $C$DW$895, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$895, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$896	.dwtag  DW_TAG_member
	.dwattr $C$DW$896, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$896, DW_AT_name("INTx4")
	.dwattr $C$DW$896, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$896, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$896, DW_AT_bit_size(0x01)
	.dwattr $C$DW$896, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$896, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$897	.dwtag  DW_TAG_member
	.dwattr $C$DW$897, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$897, DW_AT_name("INTx5")
	.dwattr $C$DW$897, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$897, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$897, DW_AT_bit_size(0x01)
	.dwattr $C$DW$897, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$897, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$898	.dwtag  DW_TAG_member
	.dwattr $C$DW$898, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$898, DW_AT_name("INTx6")
	.dwattr $C$DW$898, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$898, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$898, DW_AT_bit_size(0x01)
	.dwattr $C$DW$898, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$898, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$899	.dwtag  DW_TAG_member
	.dwattr $C$DW$899, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$899, DW_AT_name("INTx7")
	.dwattr $C$DW$899, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$899, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$899, DW_AT_bit_size(0x01)
	.dwattr $C$DW$899, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$899, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$900	.dwtag  DW_TAG_member
	.dwattr $C$DW$900, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$900, DW_AT_name("INTx8")
	.dwattr $C$DW$900, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$900, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$900, DW_AT_bit_size(0x01)
	.dwattr $C$DW$900, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$900, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$901	.dwtag  DW_TAG_member
	.dwattr $C$DW$901, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$901, DW_AT_name("INTx9")
	.dwattr $C$DW$901, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$901, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$901, DW_AT_bit_size(0x01)
	.dwattr $C$DW$901, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$901, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$902	.dwtag  DW_TAG_member
	.dwattr $C$DW$902, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$902, DW_AT_name("INTx10")
	.dwattr $C$DW$902, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$902, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$902, DW_AT_bit_size(0x01)
	.dwattr $C$DW$902, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$902, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$903	.dwtag  DW_TAG_member
	.dwattr $C$DW$903, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$903, DW_AT_name("INTx11")
	.dwattr $C$DW$903, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$903, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$903, DW_AT_bit_size(0x01)
	.dwattr $C$DW$903, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$903, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$904	.dwtag  DW_TAG_member
	.dwattr $C$DW$904, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$904, DW_AT_name("INTx12")
	.dwattr $C$DW$904, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$904, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$904, DW_AT_bit_size(0x01)
	.dwattr $C$DW$904, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$904, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$905	.dwtag  DW_TAG_member
	.dwattr $C$DW$905, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$905, DW_AT_name("INTx13")
	.dwattr $C$DW$905, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$905, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$905, DW_AT_bit_size(0x01)
	.dwattr $C$DW$905, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$905, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$906	.dwtag  DW_TAG_member
	.dwattr $C$DW$906, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$906, DW_AT_name("INTx14")
	.dwattr $C$DW$906, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$906, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$906, DW_AT_bit_size(0x01)
	.dwattr $C$DW$906, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$906, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$907	.dwtag  DW_TAG_member
	.dwattr $C$DW$907, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$907, DW_AT_name("INTx15")
	.dwattr $C$DW$907, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$907, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$907, DW_AT_bit_size(0x01)
	.dwattr $C$DW$907, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$907, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$908	.dwtag  DW_TAG_member
	.dwattr $C$DW$908, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$908, DW_AT_name("INTx16")
	.dwattr $C$DW$908, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$908, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$908, DW_AT_bit_size(0x01)
	.dwattr $C$DW$908, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$908, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$125


$C$DW$T$126	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$126, DW_AT_name("PIEIER7_REG")
	.dwattr $C$DW$T$126, DW_AT_byte_size(0x01)
$C$DW$909	.dwtag  DW_TAG_member
	.dwattr $C$DW$909, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$909, DW_AT_name("all")
	.dwattr $C$DW$909, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$909, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$909, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$910	.dwtag  DW_TAG_member
	.dwattr $C$DW$910, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$910, DW_AT_name("bit")
	.dwattr $C$DW$910, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$910, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$910, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$126


$C$DW$T$127	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$127, DW_AT_name("PIEIER8_BITS")
	.dwattr $C$DW$T$127, DW_AT_byte_size(0x01)
$C$DW$911	.dwtag  DW_TAG_member
	.dwattr $C$DW$911, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$911, DW_AT_name("INTx1")
	.dwattr $C$DW$911, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$911, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$911, DW_AT_bit_size(0x01)
	.dwattr $C$DW$911, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$911, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$912	.dwtag  DW_TAG_member
	.dwattr $C$DW$912, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$912, DW_AT_name("INTx2")
	.dwattr $C$DW$912, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$912, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$912, DW_AT_bit_size(0x01)
	.dwattr $C$DW$912, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$912, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$913	.dwtag  DW_TAG_member
	.dwattr $C$DW$913, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$913, DW_AT_name("INTx3")
	.dwattr $C$DW$913, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$913, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$913, DW_AT_bit_size(0x01)
	.dwattr $C$DW$913, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$913, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$914	.dwtag  DW_TAG_member
	.dwattr $C$DW$914, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$914, DW_AT_name("INTx4")
	.dwattr $C$DW$914, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$914, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$914, DW_AT_bit_size(0x01)
	.dwattr $C$DW$914, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$914, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$915	.dwtag  DW_TAG_member
	.dwattr $C$DW$915, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$915, DW_AT_name("INTx5")
	.dwattr $C$DW$915, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$915, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$915, DW_AT_bit_size(0x01)
	.dwattr $C$DW$915, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$915, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$916	.dwtag  DW_TAG_member
	.dwattr $C$DW$916, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$916, DW_AT_name("INTx6")
	.dwattr $C$DW$916, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$916, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$916, DW_AT_bit_size(0x01)
	.dwattr $C$DW$916, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$916, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$917	.dwtag  DW_TAG_member
	.dwattr $C$DW$917, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$917, DW_AT_name("INTx7")
	.dwattr $C$DW$917, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$917, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$917, DW_AT_bit_size(0x01)
	.dwattr $C$DW$917, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$917, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$918	.dwtag  DW_TAG_member
	.dwattr $C$DW$918, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$918, DW_AT_name("INTx8")
	.dwattr $C$DW$918, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$918, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$918, DW_AT_bit_size(0x01)
	.dwattr $C$DW$918, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$918, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$919	.dwtag  DW_TAG_member
	.dwattr $C$DW$919, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$919, DW_AT_name("INTx9")
	.dwattr $C$DW$919, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$919, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$919, DW_AT_bit_size(0x01)
	.dwattr $C$DW$919, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$919, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$920	.dwtag  DW_TAG_member
	.dwattr $C$DW$920, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$920, DW_AT_name("INTx10")
	.dwattr $C$DW$920, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$920, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$920, DW_AT_bit_size(0x01)
	.dwattr $C$DW$920, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$920, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$921	.dwtag  DW_TAG_member
	.dwattr $C$DW$921, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$921, DW_AT_name("INTx11")
	.dwattr $C$DW$921, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$921, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$921, DW_AT_bit_size(0x01)
	.dwattr $C$DW$921, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$921, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$922	.dwtag  DW_TAG_member
	.dwattr $C$DW$922, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$922, DW_AT_name("INTx12")
	.dwattr $C$DW$922, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$922, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$922, DW_AT_bit_size(0x01)
	.dwattr $C$DW$922, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$922, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$923	.dwtag  DW_TAG_member
	.dwattr $C$DW$923, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$923, DW_AT_name("INTx13")
	.dwattr $C$DW$923, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$923, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$923, DW_AT_bit_size(0x01)
	.dwattr $C$DW$923, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$923, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$924	.dwtag  DW_TAG_member
	.dwattr $C$DW$924, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$924, DW_AT_name("INTx14")
	.dwattr $C$DW$924, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$924, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$924, DW_AT_bit_size(0x01)
	.dwattr $C$DW$924, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$924, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$925	.dwtag  DW_TAG_member
	.dwattr $C$DW$925, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$925, DW_AT_name("INTx15")
	.dwattr $C$DW$925, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$925, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$925, DW_AT_bit_size(0x01)
	.dwattr $C$DW$925, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$925, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$926	.dwtag  DW_TAG_member
	.dwattr $C$DW$926, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$926, DW_AT_name("INTx16")
	.dwattr $C$DW$926, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$926, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$926, DW_AT_bit_size(0x01)
	.dwattr $C$DW$926, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$926, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$127


$C$DW$T$128	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$128, DW_AT_name("PIEIER8_REG")
	.dwattr $C$DW$T$128, DW_AT_byte_size(0x01)
$C$DW$927	.dwtag  DW_TAG_member
	.dwattr $C$DW$927, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$927, DW_AT_name("all")
	.dwattr $C$DW$927, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$927, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$927, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$928	.dwtag  DW_TAG_member
	.dwattr $C$DW$928, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$928, DW_AT_name("bit")
	.dwattr $C$DW$928, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$928, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$928, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$128


$C$DW$T$129	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$129, DW_AT_name("PIEIER9_BITS")
	.dwattr $C$DW$T$129, DW_AT_byte_size(0x01)
$C$DW$929	.dwtag  DW_TAG_member
	.dwattr $C$DW$929, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$929, DW_AT_name("INTx1")
	.dwattr $C$DW$929, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$929, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$929, DW_AT_bit_size(0x01)
	.dwattr $C$DW$929, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$929, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$930	.dwtag  DW_TAG_member
	.dwattr $C$DW$930, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$930, DW_AT_name("INTx2")
	.dwattr $C$DW$930, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$930, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$930, DW_AT_bit_size(0x01)
	.dwattr $C$DW$930, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$930, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$931	.dwtag  DW_TAG_member
	.dwattr $C$DW$931, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$931, DW_AT_name("INTx3")
	.dwattr $C$DW$931, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$931, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$931, DW_AT_bit_size(0x01)
	.dwattr $C$DW$931, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$931, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$932	.dwtag  DW_TAG_member
	.dwattr $C$DW$932, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$932, DW_AT_name("INTx4")
	.dwattr $C$DW$932, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$932, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$932, DW_AT_bit_size(0x01)
	.dwattr $C$DW$932, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$932, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$933	.dwtag  DW_TAG_member
	.dwattr $C$DW$933, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$933, DW_AT_name("INTx5")
	.dwattr $C$DW$933, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$933, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$933, DW_AT_bit_size(0x01)
	.dwattr $C$DW$933, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$933, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$934	.dwtag  DW_TAG_member
	.dwattr $C$DW$934, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$934, DW_AT_name("INTx6")
	.dwattr $C$DW$934, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$934, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$934, DW_AT_bit_size(0x01)
	.dwattr $C$DW$934, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$934, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$935	.dwtag  DW_TAG_member
	.dwattr $C$DW$935, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$935, DW_AT_name("INTx7")
	.dwattr $C$DW$935, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$935, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$935, DW_AT_bit_size(0x01)
	.dwattr $C$DW$935, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$935, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$936	.dwtag  DW_TAG_member
	.dwattr $C$DW$936, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$936, DW_AT_name("INTx8")
	.dwattr $C$DW$936, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$936, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$936, DW_AT_bit_size(0x01)
	.dwattr $C$DW$936, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$936, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$937	.dwtag  DW_TAG_member
	.dwattr $C$DW$937, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$937, DW_AT_name("INTx9")
	.dwattr $C$DW$937, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$937, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$937, DW_AT_bit_size(0x01)
	.dwattr $C$DW$937, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$937, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$938	.dwtag  DW_TAG_member
	.dwattr $C$DW$938, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$938, DW_AT_name("INTx10")
	.dwattr $C$DW$938, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$938, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$938, DW_AT_bit_size(0x01)
	.dwattr $C$DW$938, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$938, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$939	.dwtag  DW_TAG_member
	.dwattr $C$DW$939, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$939, DW_AT_name("INTx11")
	.dwattr $C$DW$939, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$939, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$939, DW_AT_bit_size(0x01)
	.dwattr $C$DW$939, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$939, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$940	.dwtag  DW_TAG_member
	.dwattr $C$DW$940, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$940, DW_AT_name("INTx12")
	.dwattr $C$DW$940, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$940, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$940, DW_AT_bit_size(0x01)
	.dwattr $C$DW$940, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$940, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$941	.dwtag  DW_TAG_member
	.dwattr $C$DW$941, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$941, DW_AT_name("INTx13")
	.dwattr $C$DW$941, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$941, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$941, DW_AT_bit_size(0x01)
	.dwattr $C$DW$941, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$941, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$942	.dwtag  DW_TAG_member
	.dwattr $C$DW$942, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$942, DW_AT_name("INTx14")
	.dwattr $C$DW$942, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$942, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$942, DW_AT_bit_size(0x01)
	.dwattr $C$DW$942, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$942, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$943	.dwtag  DW_TAG_member
	.dwattr $C$DW$943, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$943, DW_AT_name("INTx15")
	.dwattr $C$DW$943, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$943, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$943, DW_AT_bit_size(0x01)
	.dwattr $C$DW$943, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$943, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$944	.dwtag  DW_TAG_member
	.dwattr $C$DW$944, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$944, DW_AT_name("INTx16")
	.dwattr $C$DW$944, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$944, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$944, DW_AT_bit_size(0x01)
	.dwattr $C$DW$944, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$944, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$129


$C$DW$T$130	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$130, DW_AT_name("PIEIER9_REG")
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x01)
$C$DW$945	.dwtag  DW_TAG_member
	.dwattr $C$DW$945, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$945, DW_AT_name("all")
	.dwattr $C$DW$945, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$945, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$945, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$946	.dwtag  DW_TAG_member
	.dwattr $C$DW$946, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$946, DW_AT_name("bit")
	.dwattr $C$DW$946, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$946, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$946, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$130


$C$DW$T$131	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$131, DW_AT_name("PIEIFR10_BITS")
	.dwattr $C$DW$T$131, DW_AT_byte_size(0x01)
$C$DW$947	.dwtag  DW_TAG_member
	.dwattr $C$DW$947, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$947, DW_AT_name("INTx1")
	.dwattr $C$DW$947, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$947, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$947, DW_AT_bit_size(0x01)
	.dwattr $C$DW$947, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$947, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$948	.dwtag  DW_TAG_member
	.dwattr $C$DW$948, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$948, DW_AT_name("INTx2")
	.dwattr $C$DW$948, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$948, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$948, DW_AT_bit_size(0x01)
	.dwattr $C$DW$948, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$948, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$949	.dwtag  DW_TAG_member
	.dwattr $C$DW$949, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$949, DW_AT_name("INTx3")
	.dwattr $C$DW$949, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$949, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$949, DW_AT_bit_size(0x01)
	.dwattr $C$DW$949, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$949, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$950	.dwtag  DW_TAG_member
	.dwattr $C$DW$950, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$950, DW_AT_name("INTx4")
	.dwattr $C$DW$950, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$950, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$950, DW_AT_bit_size(0x01)
	.dwattr $C$DW$950, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$950, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$951	.dwtag  DW_TAG_member
	.dwattr $C$DW$951, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$951, DW_AT_name("INTx5")
	.dwattr $C$DW$951, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$951, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$951, DW_AT_bit_size(0x01)
	.dwattr $C$DW$951, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$951, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$952	.dwtag  DW_TAG_member
	.dwattr $C$DW$952, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$952, DW_AT_name("INTx6")
	.dwattr $C$DW$952, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$952, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$952, DW_AT_bit_size(0x01)
	.dwattr $C$DW$952, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$952, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$953	.dwtag  DW_TAG_member
	.dwattr $C$DW$953, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$953, DW_AT_name("INTx7")
	.dwattr $C$DW$953, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$953, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$953, DW_AT_bit_size(0x01)
	.dwattr $C$DW$953, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$953, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$954	.dwtag  DW_TAG_member
	.dwattr $C$DW$954, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$954, DW_AT_name("INTx8")
	.dwattr $C$DW$954, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$954, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$954, DW_AT_bit_size(0x01)
	.dwattr $C$DW$954, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$954, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$955	.dwtag  DW_TAG_member
	.dwattr $C$DW$955, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$955, DW_AT_name("INTx9")
	.dwattr $C$DW$955, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$955, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$955, DW_AT_bit_size(0x01)
	.dwattr $C$DW$955, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$955, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$956	.dwtag  DW_TAG_member
	.dwattr $C$DW$956, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$956, DW_AT_name("INTx10")
	.dwattr $C$DW$956, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$956, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$956, DW_AT_bit_size(0x01)
	.dwattr $C$DW$956, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$956, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$957	.dwtag  DW_TAG_member
	.dwattr $C$DW$957, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$957, DW_AT_name("INTx11")
	.dwattr $C$DW$957, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$957, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$957, DW_AT_bit_size(0x01)
	.dwattr $C$DW$957, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$957, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$958	.dwtag  DW_TAG_member
	.dwattr $C$DW$958, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$958, DW_AT_name("INTx12")
	.dwattr $C$DW$958, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$958, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$958, DW_AT_bit_size(0x01)
	.dwattr $C$DW$958, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$958, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$959	.dwtag  DW_TAG_member
	.dwattr $C$DW$959, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$959, DW_AT_name("INTx13")
	.dwattr $C$DW$959, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$959, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$959, DW_AT_bit_size(0x01)
	.dwattr $C$DW$959, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$959, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$960	.dwtag  DW_TAG_member
	.dwattr $C$DW$960, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$960, DW_AT_name("INTx14")
	.dwattr $C$DW$960, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$960, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$960, DW_AT_bit_size(0x01)
	.dwattr $C$DW$960, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$960, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$961	.dwtag  DW_TAG_member
	.dwattr $C$DW$961, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$961, DW_AT_name("INTx15")
	.dwattr $C$DW$961, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$961, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$961, DW_AT_bit_size(0x01)
	.dwattr $C$DW$961, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$961, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$962	.dwtag  DW_TAG_member
	.dwattr $C$DW$962, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$962, DW_AT_name("INTx16")
	.dwattr $C$DW$962, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$962, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$962, DW_AT_bit_size(0x01)
	.dwattr $C$DW$962, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$962, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$131


$C$DW$T$132	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$132, DW_AT_name("PIEIFR10_REG")
	.dwattr $C$DW$T$132, DW_AT_byte_size(0x01)
$C$DW$963	.dwtag  DW_TAG_member
	.dwattr $C$DW$963, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$963, DW_AT_name("all")
	.dwattr $C$DW$963, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$963, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$963, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$964	.dwtag  DW_TAG_member
	.dwattr $C$DW$964, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$964, DW_AT_name("bit")
	.dwattr $C$DW$964, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$964, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$964, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$132


$C$DW$T$133	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$133, DW_AT_name("PIEIFR11_BITS")
	.dwattr $C$DW$T$133, DW_AT_byte_size(0x01)
$C$DW$965	.dwtag  DW_TAG_member
	.dwattr $C$DW$965, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$965, DW_AT_name("INTx1")
	.dwattr $C$DW$965, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$965, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$965, DW_AT_bit_size(0x01)
	.dwattr $C$DW$965, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$965, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$966	.dwtag  DW_TAG_member
	.dwattr $C$DW$966, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$966, DW_AT_name("INTx2")
	.dwattr $C$DW$966, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$966, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$966, DW_AT_bit_size(0x01)
	.dwattr $C$DW$966, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$966, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$967	.dwtag  DW_TAG_member
	.dwattr $C$DW$967, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$967, DW_AT_name("INTx3")
	.dwattr $C$DW$967, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$967, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$967, DW_AT_bit_size(0x01)
	.dwattr $C$DW$967, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$967, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$968	.dwtag  DW_TAG_member
	.dwattr $C$DW$968, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$968, DW_AT_name("INTx4")
	.dwattr $C$DW$968, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$968, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$968, DW_AT_bit_size(0x01)
	.dwattr $C$DW$968, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$968, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$969	.dwtag  DW_TAG_member
	.dwattr $C$DW$969, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$969, DW_AT_name("INTx5")
	.dwattr $C$DW$969, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$969, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$969, DW_AT_bit_size(0x01)
	.dwattr $C$DW$969, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$969, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$970	.dwtag  DW_TAG_member
	.dwattr $C$DW$970, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$970, DW_AT_name("INTx6")
	.dwattr $C$DW$970, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$970, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$970, DW_AT_bit_size(0x01)
	.dwattr $C$DW$970, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$970, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$971	.dwtag  DW_TAG_member
	.dwattr $C$DW$971, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$971, DW_AT_name("INTx7")
	.dwattr $C$DW$971, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$971, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$971, DW_AT_bit_size(0x01)
	.dwattr $C$DW$971, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$971, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$972	.dwtag  DW_TAG_member
	.dwattr $C$DW$972, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$972, DW_AT_name("INTx8")
	.dwattr $C$DW$972, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$972, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$972, DW_AT_bit_size(0x01)
	.dwattr $C$DW$972, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$972, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$973	.dwtag  DW_TAG_member
	.dwattr $C$DW$973, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$973, DW_AT_name("INTx9")
	.dwattr $C$DW$973, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$973, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$973, DW_AT_bit_size(0x01)
	.dwattr $C$DW$973, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$973, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$974	.dwtag  DW_TAG_member
	.dwattr $C$DW$974, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$974, DW_AT_name("INTx10")
	.dwattr $C$DW$974, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$974, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$974, DW_AT_bit_size(0x01)
	.dwattr $C$DW$974, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$974, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$975	.dwtag  DW_TAG_member
	.dwattr $C$DW$975, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$975, DW_AT_name("INTx11")
	.dwattr $C$DW$975, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$975, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$975, DW_AT_bit_size(0x01)
	.dwattr $C$DW$975, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$975, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$976	.dwtag  DW_TAG_member
	.dwattr $C$DW$976, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$976, DW_AT_name("INTx12")
	.dwattr $C$DW$976, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$976, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$976, DW_AT_bit_size(0x01)
	.dwattr $C$DW$976, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$976, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$977	.dwtag  DW_TAG_member
	.dwattr $C$DW$977, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$977, DW_AT_name("INTx13")
	.dwattr $C$DW$977, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$977, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$977, DW_AT_bit_size(0x01)
	.dwattr $C$DW$977, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$977, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$978	.dwtag  DW_TAG_member
	.dwattr $C$DW$978, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$978, DW_AT_name("INTx14")
	.dwattr $C$DW$978, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$978, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$978, DW_AT_bit_size(0x01)
	.dwattr $C$DW$978, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$978, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$979	.dwtag  DW_TAG_member
	.dwattr $C$DW$979, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$979, DW_AT_name("INTx15")
	.dwattr $C$DW$979, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$979, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$979, DW_AT_bit_size(0x01)
	.dwattr $C$DW$979, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$979, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$980	.dwtag  DW_TAG_member
	.dwattr $C$DW$980, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$980, DW_AT_name("INTx16")
	.dwattr $C$DW$980, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$980, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$980, DW_AT_bit_size(0x01)
	.dwattr $C$DW$980, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$980, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$133


$C$DW$T$134	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$134, DW_AT_name("PIEIFR11_REG")
	.dwattr $C$DW$T$134, DW_AT_byte_size(0x01)
$C$DW$981	.dwtag  DW_TAG_member
	.dwattr $C$DW$981, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$981, DW_AT_name("all")
	.dwattr $C$DW$981, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$981, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$981, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$982	.dwtag  DW_TAG_member
	.dwattr $C$DW$982, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$982, DW_AT_name("bit")
	.dwattr $C$DW$982, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$982, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$982, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$134


$C$DW$T$135	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$135, DW_AT_name("PIEIFR12_BITS")
	.dwattr $C$DW$T$135, DW_AT_byte_size(0x01)
$C$DW$983	.dwtag  DW_TAG_member
	.dwattr $C$DW$983, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$983, DW_AT_name("INTx1")
	.dwattr $C$DW$983, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$983, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$983, DW_AT_bit_size(0x01)
	.dwattr $C$DW$983, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$983, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$984	.dwtag  DW_TAG_member
	.dwattr $C$DW$984, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$984, DW_AT_name("INTx2")
	.dwattr $C$DW$984, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$984, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$984, DW_AT_bit_size(0x01)
	.dwattr $C$DW$984, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$984, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$985	.dwtag  DW_TAG_member
	.dwattr $C$DW$985, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$985, DW_AT_name("INTx3")
	.dwattr $C$DW$985, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$985, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$985, DW_AT_bit_size(0x01)
	.dwattr $C$DW$985, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$985, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$986	.dwtag  DW_TAG_member
	.dwattr $C$DW$986, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$986, DW_AT_name("INTx4")
	.dwattr $C$DW$986, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$986, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$986, DW_AT_bit_size(0x01)
	.dwattr $C$DW$986, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$986, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$987	.dwtag  DW_TAG_member
	.dwattr $C$DW$987, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$987, DW_AT_name("INTx5")
	.dwattr $C$DW$987, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$987, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$987, DW_AT_bit_size(0x01)
	.dwattr $C$DW$987, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$987, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$988	.dwtag  DW_TAG_member
	.dwattr $C$DW$988, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$988, DW_AT_name("INTx6")
	.dwattr $C$DW$988, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$988, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$988, DW_AT_bit_size(0x01)
	.dwattr $C$DW$988, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$988, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$989	.dwtag  DW_TAG_member
	.dwattr $C$DW$989, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$989, DW_AT_name("INTx7")
	.dwattr $C$DW$989, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$989, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$989, DW_AT_bit_size(0x01)
	.dwattr $C$DW$989, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$989, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$990	.dwtag  DW_TAG_member
	.dwattr $C$DW$990, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$990, DW_AT_name("INTx8")
	.dwattr $C$DW$990, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$990, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$990, DW_AT_bit_size(0x01)
	.dwattr $C$DW$990, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$990, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$991	.dwtag  DW_TAG_member
	.dwattr $C$DW$991, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$991, DW_AT_name("INTx9")
	.dwattr $C$DW$991, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$991, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$991, DW_AT_bit_size(0x01)
	.dwattr $C$DW$991, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$991, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$992	.dwtag  DW_TAG_member
	.dwattr $C$DW$992, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$992, DW_AT_name("INTx10")
	.dwattr $C$DW$992, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$992, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$992, DW_AT_bit_size(0x01)
	.dwattr $C$DW$992, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$992, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$993	.dwtag  DW_TAG_member
	.dwattr $C$DW$993, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$993, DW_AT_name("INTx11")
	.dwattr $C$DW$993, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$993, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$993, DW_AT_bit_size(0x01)
	.dwattr $C$DW$993, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$993, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$994	.dwtag  DW_TAG_member
	.dwattr $C$DW$994, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$994, DW_AT_name("INTx12")
	.dwattr $C$DW$994, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$994, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$994, DW_AT_bit_size(0x01)
	.dwattr $C$DW$994, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$994, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$995	.dwtag  DW_TAG_member
	.dwattr $C$DW$995, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$995, DW_AT_name("INTx13")
	.dwattr $C$DW$995, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$995, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$995, DW_AT_bit_size(0x01)
	.dwattr $C$DW$995, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$995, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$996	.dwtag  DW_TAG_member
	.dwattr $C$DW$996, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$996, DW_AT_name("INTx14")
	.dwattr $C$DW$996, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$996, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$996, DW_AT_bit_size(0x01)
	.dwattr $C$DW$996, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$996, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$997	.dwtag  DW_TAG_member
	.dwattr $C$DW$997, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$997, DW_AT_name("INTx15")
	.dwattr $C$DW$997, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$997, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$997, DW_AT_bit_size(0x01)
	.dwattr $C$DW$997, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$997, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$998	.dwtag  DW_TAG_member
	.dwattr $C$DW$998, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$998, DW_AT_name("INTx16")
	.dwattr $C$DW$998, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$998, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$998, DW_AT_bit_size(0x01)
	.dwattr $C$DW$998, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$998, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$135


$C$DW$T$136	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$136, DW_AT_name("PIEIFR12_REG")
	.dwattr $C$DW$T$136, DW_AT_byte_size(0x01)
$C$DW$999	.dwtag  DW_TAG_member
	.dwattr $C$DW$999, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$999, DW_AT_name("all")
	.dwattr $C$DW$999, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$999, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$999, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1000	.dwtag  DW_TAG_member
	.dwattr $C$DW$1000, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$1000, DW_AT_name("bit")
	.dwattr $C$DW$1000, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1000, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1000, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$136


$C$DW$T$137	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$137, DW_AT_name("PIEIFR1_BITS")
	.dwattr $C$DW$T$137, DW_AT_byte_size(0x01)
$C$DW$1001	.dwtag  DW_TAG_member
	.dwattr $C$DW$1001, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1001, DW_AT_name("INTx1")
	.dwattr $C$DW$1001, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$1001, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$1001, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1001, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1001, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1002	.dwtag  DW_TAG_member
	.dwattr $C$DW$1002, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1002, DW_AT_name("INTx2")
	.dwattr $C$DW$1002, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$1002, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$1002, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1002, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1002, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1003	.dwtag  DW_TAG_member
	.dwattr $C$DW$1003, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1003, DW_AT_name("INTx3")
	.dwattr $C$DW$1003, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$1003, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$1003, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1003, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1003, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1004	.dwtag  DW_TAG_member
	.dwattr $C$DW$1004, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1004, DW_AT_name("INTx4")
	.dwattr $C$DW$1004, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$1004, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$1004, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1004, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1004, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1005	.dwtag  DW_TAG_member
	.dwattr $C$DW$1005, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1005, DW_AT_name("INTx5")
	.dwattr $C$DW$1005, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$1005, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$1005, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1005, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1005, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1006	.dwtag  DW_TAG_member
	.dwattr $C$DW$1006, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1006, DW_AT_name("INTx6")
	.dwattr $C$DW$1006, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$1006, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$1006, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1006, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1006, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1007	.dwtag  DW_TAG_member
	.dwattr $C$DW$1007, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1007, DW_AT_name("INTx7")
	.dwattr $C$DW$1007, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$1007, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$1007, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1007, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1007, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1008	.dwtag  DW_TAG_member
	.dwattr $C$DW$1008, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1008, DW_AT_name("INTx8")
	.dwattr $C$DW$1008, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$1008, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$1008, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1008, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1008, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1009	.dwtag  DW_TAG_member
	.dwattr $C$DW$1009, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1009, DW_AT_name("INTx9")
	.dwattr $C$DW$1009, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$1009, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$1009, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1009, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1009, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1010	.dwtag  DW_TAG_member
	.dwattr $C$DW$1010, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1010, DW_AT_name("INTx10")
	.dwattr $C$DW$1010, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$1010, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$1010, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1010, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1010, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1011	.dwtag  DW_TAG_member
	.dwattr $C$DW$1011, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1011, DW_AT_name("INTx11")
	.dwattr $C$DW$1011, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$1011, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$1011, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1011, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1011, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1012	.dwtag  DW_TAG_member
	.dwattr $C$DW$1012, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1012, DW_AT_name("INTx12")
	.dwattr $C$DW$1012, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$1012, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$1012, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1012, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1012, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1013	.dwtag  DW_TAG_member
	.dwattr $C$DW$1013, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1013, DW_AT_name("INTx13")
	.dwattr $C$DW$1013, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$1013, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$1013, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1013, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1013, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1014	.dwtag  DW_TAG_member
	.dwattr $C$DW$1014, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1014, DW_AT_name("INTx14")
	.dwattr $C$DW$1014, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$1014, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$1014, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1014, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1014, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1015	.dwtag  DW_TAG_member
	.dwattr $C$DW$1015, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1015, DW_AT_name("INTx15")
	.dwattr $C$DW$1015, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$1015, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$1015, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1015, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1015, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1016	.dwtag  DW_TAG_member
	.dwattr $C$DW$1016, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1016, DW_AT_name("INTx16")
	.dwattr $C$DW$1016, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$1016, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$1016, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1016, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1016, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$137


$C$DW$T$138	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$138, DW_AT_name("PIEIFR1_REG")
	.dwattr $C$DW$T$138, DW_AT_byte_size(0x01)
$C$DW$1017	.dwtag  DW_TAG_member
	.dwattr $C$DW$1017, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1017, DW_AT_name("all")
	.dwattr $C$DW$1017, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1017, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1017, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1018	.dwtag  DW_TAG_member
	.dwattr $C$DW$1018, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$1018, DW_AT_name("bit")
	.dwattr $C$DW$1018, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1018, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1018, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$138


$C$DW$T$139	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$139, DW_AT_name("PIEIFR2_BITS")
	.dwattr $C$DW$T$139, DW_AT_byte_size(0x01)
$C$DW$1019	.dwtag  DW_TAG_member
	.dwattr $C$DW$1019, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1019, DW_AT_name("INTx1")
	.dwattr $C$DW$1019, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$1019, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$1019, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1019, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1019, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1020	.dwtag  DW_TAG_member
	.dwattr $C$DW$1020, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1020, DW_AT_name("INTx2")
	.dwattr $C$DW$1020, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$1020, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$1020, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1020, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1020, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1021	.dwtag  DW_TAG_member
	.dwattr $C$DW$1021, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1021, DW_AT_name("INTx3")
	.dwattr $C$DW$1021, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$1021, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$1021, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1021, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1021, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1022	.dwtag  DW_TAG_member
	.dwattr $C$DW$1022, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1022, DW_AT_name("INTx4")
	.dwattr $C$DW$1022, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$1022, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$1022, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1022, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1022, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1023	.dwtag  DW_TAG_member
	.dwattr $C$DW$1023, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1023, DW_AT_name("INTx5")
	.dwattr $C$DW$1023, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$1023, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$1023, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1023, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1023, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1024	.dwtag  DW_TAG_member
	.dwattr $C$DW$1024, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1024, DW_AT_name("INTx6")
	.dwattr $C$DW$1024, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$1024, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$1024, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1024, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1024, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1025	.dwtag  DW_TAG_member
	.dwattr $C$DW$1025, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1025, DW_AT_name("INTx7")
	.dwattr $C$DW$1025, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$1025, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$1025, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1025, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1025, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1026	.dwtag  DW_TAG_member
	.dwattr $C$DW$1026, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1026, DW_AT_name("INTx8")
	.dwattr $C$DW$1026, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$1026, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$1026, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1026, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1026, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1027	.dwtag  DW_TAG_member
	.dwattr $C$DW$1027, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1027, DW_AT_name("INTx9")
	.dwattr $C$DW$1027, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$1027, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$1027, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1027, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1027, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1028	.dwtag  DW_TAG_member
	.dwattr $C$DW$1028, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1028, DW_AT_name("INTx10")
	.dwattr $C$DW$1028, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$1028, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$1028, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1028, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1028, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1029	.dwtag  DW_TAG_member
	.dwattr $C$DW$1029, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1029, DW_AT_name("INTx11")
	.dwattr $C$DW$1029, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$1029, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$1029, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1029, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1029, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1030	.dwtag  DW_TAG_member
	.dwattr $C$DW$1030, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1030, DW_AT_name("INTx12")
	.dwattr $C$DW$1030, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$1030, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$1030, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1030, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1030, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1031	.dwtag  DW_TAG_member
	.dwattr $C$DW$1031, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1031, DW_AT_name("INTx13")
	.dwattr $C$DW$1031, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$1031, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$1031, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1031, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1031, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1032	.dwtag  DW_TAG_member
	.dwattr $C$DW$1032, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1032, DW_AT_name("INTx14")
	.dwattr $C$DW$1032, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$1032, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$1032, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1032, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1032, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1033	.dwtag  DW_TAG_member
	.dwattr $C$DW$1033, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1033, DW_AT_name("INTx15")
	.dwattr $C$DW$1033, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$1033, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$1033, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1033, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1033, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1034	.dwtag  DW_TAG_member
	.dwattr $C$DW$1034, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1034, DW_AT_name("INTx16")
	.dwattr $C$DW$1034, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$1034, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$1034, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1034, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1034, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$139


$C$DW$T$140	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$140, DW_AT_name("PIEIFR2_REG")
	.dwattr $C$DW$T$140, DW_AT_byte_size(0x01)
$C$DW$1035	.dwtag  DW_TAG_member
	.dwattr $C$DW$1035, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1035, DW_AT_name("all")
	.dwattr $C$DW$1035, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1035, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1035, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1036	.dwtag  DW_TAG_member
	.dwattr $C$DW$1036, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$1036, DW_AT_name("bit")
	.dwattr $C$DW$1036, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1036, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1036, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$140


$C$DW$T$141	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$141, DW_AT_name("PIEIFR3_BITS")
	.dwattr $C$DW$T$141, DW_AT_byte_size(0x01)
$C$DW$1037	.dwtag  DW_TAG_member
	.dwattr $C$DW$1037, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1037, DW_AT_name("INTx1")
	.dwattr $C$DW$1037, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$1037, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$1037, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1037, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1037, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1038	.dwtag  DW_TAG_member
	.dwattr $C$DW$1038, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1038, DW_AT_name("INTx2")
	.dwattr $C$DW$1038, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$1038, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$1038, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1038, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1038, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1039	.dwtag  DW_TAG_member
	.dwattr $C$DW$1039, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1039, DW_AT_name("INTx3")
	.dwattr $C$DW$1039, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$1039, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$1039, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1039, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1039, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1040	.dwtag  DW_TAG_member
	.dwattr $C$DW$1040, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1040, DW_AT_name("INTx4")
	.dwattr $C$DW$1040, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$1040, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$1040, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1040, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1040, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1041	.dwtag  DW_TAG_member
	.dwattr $C$DW$1041, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1041, DW_AT_name("INTx5")
	.dwattr $C$DW$1041, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$1041, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$1041, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1041, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1041, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1042	.dwtag  DW_TAG_member
	.dwattr $C$DW$1042, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1042, DW_AT_name("INTx6")
	.dwattr $C$DW$1042, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$1042, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$1042, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1042, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1042, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1043	.dwtag  DW_TAG_member
	.dwattr $C$DW$1043, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1043, DW_AT_name("INTx7")
	.dwattr $C$DW$1043, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$1043, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$1043, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1043, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1043, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1044	.dwtag  DW_TAG_member
	.dwattr $C$DW$1044, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1044, DW_AT_name("INTx8")
	.dwattr $C$DW$1044, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$1044, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$1044, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1044, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1044, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1045	.dwtag  DW_TAG_member
	.dwattr $C$DW$1045, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1045, DW_AT_name("INTx9")
	.dwattr $C$DW$1045, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$1045, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$1045, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1045, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1045, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1046	.dwtag  DW_TAG_member
	.dwattr $C$DW$1046, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1046, DW_AT_name("INTx10")
	.dwattr $C$DW$1046, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$1046, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$1046, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1046, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1046, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1047	.dwtag  DW_TAG_member
	.dwattr $C$DW$1047, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1047, DW_AT_name("INTx11")
	.dwattr $C$DW$1047, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$1047, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$1047, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1047, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1047, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1048	.dwtag  DW_TAG_member
	.dwattr $C$DW$1048, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1048, DW_AT_name("INTx12")
	.dwattr $C$DW$1048, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$1048, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$1048, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1048, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1048, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1049	.dwtag  DW_TAG_member
	.dwattr $C$DW$1049, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1049, DW_AT_name("INTx13")
	.dwattr $C$DW$1049, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$1049, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$1049, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1049, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1049, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1050	.dwtag  DW_TAG_member
	.dwattr $C$DW$1050, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1050, DW_AT_name("INTx14")
	.dwattr $C$DW$1050, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$1050, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$1050, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1050, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1050, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1051	.dwtag  DW_TAG_member
	.dwattr $C$DW$1051, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1051, DW_AT_name("INTx15")
	.dwattr $C$DW$1051, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$1051, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$1051, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1051, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1051, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1052	.dwtag  DW_TAG_member
	.dwattr $C$DW$1052, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1052, DW_AT_name("INTx16")
	.dwattr $C$DW$1052, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$1052, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$1052, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1052, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1052, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$141


$C$DW$T$142	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$142, DW_AT_name("PIEIFR3_REG")
	.dwattr $C$DW$T$142, DW_AT_byte_size(0x01)
$C$DW$1053	.dwtag  DW_TAG_member
	.dwattr $C$DW$1053, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1053, DW_AT_name("all")
	.dwattr $C$DW$1053, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1053, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1053, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1054	.dwtag  DW_TAG_member
	.dwattr $C$DW$1054, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1054, DW_AT_name("bit")
	.dwattr $C$DW$1054, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1054, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1054, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$142


$C$DW$T$143	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$143, DW_AT_name("PIEIFR4_BITS")
	.dwattr $C$DW$T$143, DW_AT_byte_size(0x01)
$C$DW$1055	.dwtag  DW_TAG_member
	.dwattr $C$DW$1055, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1055, DW_AT_name("INTx1")
	.dwattr $C$DW$1055, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$1055, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$1055, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1055, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1055, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1056	.dwtag  DW_TAG_member
	.dwattr $C$DW$1056, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1056, DW_AT_name("INTx2")
	.dwattr $C$DW$1056, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$1056, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$1056, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1056, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1056, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1057	.dwtag  DW_TAG_member
	.dwattr $C$DW$1057, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1057, DW_AT_name("INTx3")
	.dwattr $C$DW$1057, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$1057, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$1057, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1057, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1057, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1058	.dwtag  DW_TAG_member
	.dwattr $C$DW$1058, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1058, DW_AT_name("INTx4")
	.dwattr $C$DW$1058, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$1058, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$1058, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1058, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1058, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1059	.dwtag  DW_TAG_member
	.dwattr $C$DW$1059, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1059, DW_AT_name("INTx5")
	.dwattr $C$DW$1059, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$1059, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$1059, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1059, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1059, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1060	.dwtag  DW_TAG_member
	.dwattr $C$DW$1060, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1060, DW_AT_name("INTx6")
	.dwattr $C$DW$1060, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$1060, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$1060, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1060, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1060, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1061	.dwtag  DW_TAG_member
	.dwattr $C$DW$1061, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1061, DW_AT_name("INTx7")
	.dwattr $C$DW$1061, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$1061, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$1061, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1061, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1061, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1062	.dwtag  DW_TAG_member
	.dwattr $C$DW$1062, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1062, DW_AT_name("INTx8")
	.dwattr $C$DW$1062, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$1062, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$1062, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1062, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1062, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1063	.dwtag  DW_TAG_member
	.dwattr $C$DW$1063, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1063, DW_AT_name("INTx9")
	.dwattr $C$DW$1063, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$1063, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$1063, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1063, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1063, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1064	.dwtag  DW_TAG_member
	.dwattr $C$DW$1064, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1064, DW_AT_name("INTx10")
	.dwattr $C$DW$1064, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$1064, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$1064, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1064, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1064, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1065	.dwtag  DW_TAG_member
	.dwattr $C$DW$1065, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1065, DW_AT_name("INTx11")
	.dwattr $C$DW$1065, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$1065, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$1065, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1065, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1065, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1066	.dwtag  DW_TAG_member
	.dwattr $C$DW$1066, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1066, DW_AT_name("INTx12")
	.dwattr $C$DW$1066, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$1066, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$1066, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1066, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1066, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1067	.dwtag  DW_TAG_member
	.dwattr $C$DW$1067, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1067, DW_AT_name("INTx13")
	.dwattr $C$DW$1067, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$1067, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$1067, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1067, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1067, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1068	.dwtag  DW_TAG_member
	.dwattr $C$DW$1068, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1068, DW_AT_name("INTx14")
	.dwattr $C$DW$1068, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$1068, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$1068, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1068, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1068, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1069	.dwtag  DW_TAG_member
	.dwattr $C$DW$1069, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1069, DW_AT_name("INTx15")
	.dwattr $C$DW$1069, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$1069, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$1069, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1069, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1069, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1070	.dwtag  DW_TAG_member
	.dwattr $C$DW$1070, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1070, DW_AT_name("INTx16")
	.dwattr $C$DW$1070, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$1070, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$1070, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1070, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1070, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$143


$C$DW$T$144	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$144, DW_AT_name("PIEIFR4_REG")
	.dwattr $C$DW$T$144, DW_AT_byte_size(0x01)
$C$DW$1071	.dwtag  DW_TAG_member
	.dwattr $C$DW$1071, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1071, DW_AT_name("all")
	.dwattr $C$DW$1071, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1071, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1071, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1072	.dwtag  DW_TAG_member
	.dwattr $C$DW$1072, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$1072, DW_AT_name("bit")
	.dwattr $C$DW$1072, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1072, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1072, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$144


$C$DW$T$145	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$145, DW_AT_name("PIEIFR5_BITS")
	.dwattr $C$DW$T$145, DW_AT_byte_size(0x01)
$C$DW$1073	.dwtag  DW_TAG_member
	.dwattr $C$DW$1073, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1073, DW_AT_name("INTx1")
	.dwattr $C$DW$1073, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$1073, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$1073, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1073, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1073, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1074	.dwtag  DW_TAG_member
	.dwattr $C$DW$1074, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1074, DW_AT_name("INTx2")
	.dwattr $C$DW$1074, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$1074, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$1074, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1074, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1074, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1075	.dwtag  DW_TAG_member
	.dwattr $C$DW$1075, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1075, DW_AT_name("INTx3")
	.dwattr $C$DW$1075, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$1075, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$1075, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1075, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1075, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1076	.dwtag  DW_TAG_member
	.dwattr $C$DW$1076, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1076, DW_AT_name("INTx4")
	.dwattr $C$DW$1076, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$1076, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$1076, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1076, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1076, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1077	.dwtag  DW_TAG_member
	.dwattr $C$DW$1077, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1077, DW_AT_name("INTx5")
	.dwattr $C$DW$1077, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$1077, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$1077, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1077, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1077, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1078	.dwtag  DW_TAG_member
	.dwattr $C$DW$1078, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1078, DW_AT_name("INTx6")
	.dwattr $C$DW$1078, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$1078, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$1078, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1078, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1078, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1079	.dwtag  DW_TAG_member
	.dwattr $C$DW$1079, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1079, DW_AT_name("INTx7")
	.dwattr $C$DW$1079, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$1079, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$1079, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1079, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1079, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1080	.dwtag  DW_TAG_member
	.dwattr $C$DW$1080, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1080, DW_AT_name("INTx8")
	.dwattr $C$DW$1080, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$1080, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$1080, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1080, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1080, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1081	.dwtag  DW_TAG_member
	.dwattr $C$DW$1081, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1081, DW_AT_name("INTx9")
	.dwattr $C$DW$1081, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$1081, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$1081, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1081, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1081, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1082	.dwtag  DW_TAG_member
	.dwattr $C$DW$1082, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1082, DW_AT_name("INTx10")
	.dwattr $C$DW$1082, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$1082, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$1082, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1082, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1082, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1083	.dwtag  DW_TAG_member
	.dwattr $C$DW$1083, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1083, DW_AT_name("INTx11")
	.dwattr $C$DW$1083, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$1083, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$1083, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1083, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1083, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1084	.dwtag  DW_TAG_member
	.dwattr $C$DW$1084, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1084, DW_AT_name("INTx12")
	.dwattr $C$DW$1084, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$1084, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$1084, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1084, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1084, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1085	.dwtag  DW_TAG_member
	.dwattr $C$DW$1085, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1085, DW_AT_name("INTx13")
	.dwattr $C$DW$1085, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$1085, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$1085, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1085, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1085, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1086	.dwtag  DW_TAG_member
	.dwattr $C$DW$1086, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1086, DW_AT_name("INTx14")
	.dwattr $C$DW$1086, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$1086, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$1086, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1086, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1086, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1087	.dwtag  DW_TAG_member
	.dwattr $C$DW$1087, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1087, DW_AT_name("INTx15")
	.dwattr $C$DW$1087, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$1087, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$1087, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1087, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1087, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1088	.dwtag  DW_TAG_member
	.dwattr $C$DW$1088, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1088, DW_AT_name("INTx16")
	.dwattr $C$DW$1088, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$1088, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$1088, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1088, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1088, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$145


$C$DW$T$146	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$146, DW_AT_name("PIEIFR5_REG")
	.dwattr $C$DW$T$146, DW_AT_byte_size(0x01)
$C$DW$1089	.dwtag  DW_TAG_member
	.dwattr $C$DW$1089, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1089, DW_AT_name("all")
	.dwattr $C$DW$1089, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1089, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1089, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1090	.dwtag  DW_TAG_member
	.dwattr $C$DW$1090, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$1090, DW_AT_name("bit")
	.dwattr $C$DW$1090, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1090, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1090, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$146


$C$DW$T$147	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$147, DW_AT_name("PIEIFR6_BITS")
	.dwattr $C$DW$T$147, DW_AT_byte_size(0x01)
$C$DW$1091	.dwtag  DW_TAG_member
	.dwattr $C$DW$1091, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1091, DW_AT_name("INTx1")
	.dwattr $C$DW$1091, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$1091, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$1091, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1091, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1091, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1092	.dwtag  DW_TAG_member
	.dwattr $C$DW$1092, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1092, DW_AT_name("INTx2")
	.dwattr $C$DW$1092, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$1092, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$1092, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1092, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1092, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1093	.dwtag  DW_TAG_member
	.dwattr $C$DW$1093, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1093, DW_AT_name("INTx3")
	.dwattr $C$DW$1093, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$1093, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$1093, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1093, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1093, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1094	.dwtag  DW_TAG_member
	.dwattr $C$DW$1094, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1094, DW_AT_name("INTx4")
	.dwattr $C$DW$1094, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$1094, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$1094, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1094, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1094, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1095	.dwtag  DW_TAG_member
	.dwattr $C$DW$1095, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1095, DW_AT_name("INTx5")
	.dwattr $C$DW$1095, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$1095, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$1095, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1095, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1095, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1096	.dwtag  DW_TAG_member
	.dwattr $C$DW$1096, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1096, DW_AT_name("INTx6")
	.dwattr $C$DW$1096, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$1096, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$1096, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1096, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1096, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1097	.dwtag  DW_TAG_member
	.dwattr $C$DW$1097, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1097, DW_AT_name("INTx7")
	.dwattr $C$DW$1097, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$1097, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$1097, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1097, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1097, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1098	.dwtag  DW_TAG_member
	.dwattr $C$DW$1098, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1098, DW_AT_name("INTx8")
	.dwattr $C$DW$1098, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$1098, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$1098, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1098, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1098, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1099	.dwtag  DW_TAG_member
	.dwattr $C$DW$1099, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1099, DW_AT_name("INTx9")
	.dwattr $C$DW$1099, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$1099, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$1099, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1099, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1099, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1100	.dwtag  DW_TAG_member
	.dwattr $C$DW$1100, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1100, DW_AT_name("INTx10")
	.dwattr $C$DW$1100, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$1100, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$1100, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1100, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1100, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1101	.dwtag  DW_TAG_member
	.dwattr $C$DW$1101, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1101, DW_AT_name("INTx11")
	.dwattr $C$DW$1101, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$1101, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$1101, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1101, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1101, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1102	.dwtag  DW_TAG_member
	.dwattr $C$DW$1102, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1102, DW_AT_name("INTx12")
	.dwattr $C$DW$1102, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$1102, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$1102, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1102, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1102, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1103	.dwtag  DW_TAG_member
	.dwattr $C$DW$1103, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1103, DW_AT_name("INTx13")
	.dwattr $C$DW$1103, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$1103, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$1103, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1103, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1103, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1104	.dwtag  DW_TAG_member
	.dwattr $C$DW$1104, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1104, DW_AT_name("INTx14")
	.dwattr $C$DW$1104, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$1104, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$1104, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1104, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1104, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1105	.dwtag  DW_TAG_member
	.dwattr $C$DW$1105, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1105, DW_AT_name("INTx15")
	.dwattr $C$DW$1105, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$1105, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$1105, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1105, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1105, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1106	.dwtag  DW_TAG_member
	.dwattr $C$DW$1106, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1106, DW_AT_name("INTx16")
	.dwattr $C$DW$1106, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$1106, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$1106, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1106, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1106, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$147


$C$DW$T$148	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$148, DW_AT_name("PIEIFR6_REG")
	.dwattr $C$DW$T$148, DW_AT_byte_size(0x01)
$C$DW$1107	.dwtag  DW_TAG_member
	.dwattr $C$DW$1107, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1107, DW_AT_name("all")
	.dwattr $C$DW$1107, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1107, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1107, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1108	.dwtag  DW_TAG_member
	.dwattr $C$DW$1108, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$1108, DW_AT_name("bit")
	.dwattr $C$DW$1108, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1108, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1108, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$148


$C$DW$T$149	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$149, DW_AT_name("PIEIFR7_BITS")
	.dwattr $C$DW$T$149, DW_AT_byte_size(0x01)
$C$DW$1109	.dwtag  DW_TAG_member
	.dwattr $C$DW$1109, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1109, DW_AT_name("INTx1")
	.dwattr $C$DW$1109, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$1109, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$1109, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1109, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1109, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1110	.dwtag  DW_TAG_member
	.dwattr $C$DW$1110, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1110, DW_AT_name("INTx2")
	.dwattr $C$DW$1110, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$1110, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$1110, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1110, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1110, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1111	.dwtag  DW_TAG_member
	.dwattr $C$DW$1111, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1111, DW_AT_name("INTx3")
	.dwattr $C$DW$1111, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$1111, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$1111, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1111, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1111, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1112	.dwtag  DW_TAG_member
	.dwattr $C$DW$1112, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1112, DW_AT_name("INTx4")
	.dwattr $C$DW$1112, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$1112, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$1112, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1112, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1112, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1113	.dwtag  DW_TAG_member
	.dwattr $C$DW$1113, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1113, DW_AT_name("INTx5")
	.dwattr $C$DW$1113, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$1113, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$1113, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1113, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1113, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1114	.dwtag  DW_TAG_member
	.dwattr $C$DW$1114, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1114, DW_AT_name("INTx6")
	.dwattr $C$DW$1114, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$1114, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$1114, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1114, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1114, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1115	.dwtag  DW_TAG_member
	.dwattr $C$DW$1115, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1115, DW_AT_name("INTx7")
	.dwattr $C$DW$1115, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$1115, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$1115, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1115, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1115, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1116	.dwtag  DW_TAG_member
	.dwattr $C$DW$1116, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1116, DW_AT_name("INTx8")
	.dwattr $C$DW$1116, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$1116, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$1116, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1116, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1116, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1117	.dwtag  DW_TAG_member
	.dwattr $C$DW$1117, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1117, DW_AT_name("INTx9")
	.dwattr $C$DW$1117, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$1117, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$1117, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1117, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1117, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1118	.dwtag  DW_TAG_member
	.dwattr $C$DW$1118, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1118, DW_AT_name("INTx10")
	.dwattr $C$DW$1118, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$1118, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$1118, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1118, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1118, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1119	.dwtag  DW_TAG_member
	.dwattr $C$DW$1119, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1119, DW_AT_name("INTx11")
	.dwattr $C$DW$1119, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$1119, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$1119, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1119, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1119, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1120	.dwtag  DW_TAG_member
	.dwattr $C$DW$1120, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1120, DW_AT_name("INTx12")
	.dwattr $C$DW$1120, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$1120, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$1120, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1120, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1120, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1121	.dwtag  DW_TAG_member
	.dwattr $C$DW$1121, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1121, DW_AT_name("INTx13")
	.dwattr $C$DW$1121, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$1121, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$1121, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1121, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1121, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1122	.dwtag  DW_TAG_member
	.dwattr $C$DW$1122, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1122, DW_AT_name("INTx14")
	.dwattr $C$DW$1122, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$1122, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$1122, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1122, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1122, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1123	.dwtag  DW_TAG_member
	.dwattr $C$DW$1123, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1123, DW_AT_name("INTx15")
	.dwattr $C$DW$1123, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$1123, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$1123, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1123, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1123, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1124	.dwtag  DW_TAG_member
	.dwattr $C$DW$1124, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1124, DW_AT_name("INTx16")
	.dwattr $C$DW$1124, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$1124, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$1124, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1124, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1124, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$149


$C$DW$T$150	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$150, DW_AT_name("PIEIFR7_REG")
	.dwattr $C$DW$T$150, DW_AT_byte_size(0x01)
$C$DW$1125	.dwtag  DW_TAG_member
	.dwattr $C$DW$1125, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1125, DW_AT_name("all")
	.dwattr $C$DW$1125, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1125, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1125, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1126	.dwtag  DW_TAG_member
	.dwattr $C$DW$1126, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$1126, DW_AT_name("bit")
	.dwattr $C$DW$1126, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1126, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1126, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$150


$C$DW$T$151	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$151, DW_AT_name("PIEIFR8_BITS")
	.dwattr $C$DW$T$151, DW_AT_byte_size(0x01)
$C$DW$1127	.dwtag  DW_TAG_member
	.dwattr $C$DW$1127, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1127, DW_AT_name("INTx1")
	.dwattr $C$DW$1127, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$1127, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$1127, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1127, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1127, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1128	.dwtag  DW_TAG_member
	.dwattr $C$DW$1128, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1128, DW_AT_name("INTx2")
	.dwattr $C$DW$1128, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$1128, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$1128, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1128, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1128, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1129	.dwtag  DW_TAG_member
	.dwattr $C$DW$1129, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1129, DW_AT_name("INTx3")
	.dwattr $C$DW$1129, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$1129, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$1129, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1129, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1129, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1130	.dwtag  DW_TAG_member
	.dwattr $C$DW$1130, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1130, DW_AT_name("INTx4")
	.dwattr $C$DW$1130, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$1130, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$1130, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1130, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1130, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1131	.dwtag  DW_TAG_member
	.dwattr $C$DW$1131, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1131, DW_AT_name("INTx5")
	.dwattr $C$DW$1131, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$1131, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$1131, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1131, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1131, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1132	.dwtag  DW_TAG_member
	.dwattr $C$DW$1132, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1132, DW_AT_name("INTx6")
	.dwattr $C$DW$1132, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$1132, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$1132, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1132, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1132, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1133	.dwtag  DW_TAG_member
	.dwattr $C$DW$1133, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1133, DW_AT_name("INTx7")
	.dwattr $C$DW$1133, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$1133, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$1133, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1133, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1133, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1134	.dwtag  DW_TAG_member
	.dwattr $C$DW$1134, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1134, DW_AT_name("INTx8")
	.dwattr $C$DW$1134, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$1134, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$1134, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1134, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1134, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1135	.dwtag  DW_TAG_member
	.dwattr $C$DW$1135, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1135, DW_AT_name("INTx9")
	.dwattr $C$DW$1135, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$1135, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$1135, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1135, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1135, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1136	.dwtag  DW_TAG_member
	.dwattr $C$DW$1136, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1136, DW_AT_name("INTx10")
	.dwattr $C$DW$1136, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$1136, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$1136, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1136, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1136, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1137	.dwtag  DW_TAG_member
	.dwattr $C$DW$1137, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1137, DW_AT_name("INTx11")
	.dwattr $C$DW$1137, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$1137, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$1137, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1137, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1137, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1138	.dwtag  DW_TAG_member
	.dwattr $C$DW$1138, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1138, DW_AT_name("INTx12")
	.dwattr $C$DW$1138, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$1138, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$1138, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1138, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1138, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1139	.dwtag  DW_TAG_member
	.dwattr $C$DW$1139, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1139, DW_AT_name("INTx13")
	.dwattr $C$DW$1139, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$1139, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$1139, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1139, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1139, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1140	.dwtag  DW_TAG_member
	.dwattr $C$DW$1140, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1140, DW_AT_name("INTx14")
	.dwattr $C$DW$1140, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$1140, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$1140, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1140, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1140, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1141	.dwtag  DW_TAG_member
	.dwattr $C$DW$1141, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1141, DW_AT_name("INTx15")
	.dwattr $C$DW$1141, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$1141, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$1141, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1141, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1141, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1142	.dwtag  DW_TAG_member
	.dwattr $C$DW$1142, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1142, DW_AT_name("INTx16")
	.dwattr $C$DW$1142, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$1142, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$1142, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1142, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1142, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$151


$C$DW$T$152	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$152, DW_AT_name("PIEIFR8_REG")
	.dwattr $C$DW$T$152, DW_AT_byte_size(0x01)
$C$DW$1143	.dwtag  DW_TAG_member
	.dwattr $C$DW$1143, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1143, DW_AT_name("all")
	.dwattr $C$DW$1143, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1143, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1143, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1144	.dwtag  DW_TAG_member
	.dwattr $C$DW$1144, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$1144, DW_AT_name("bit")
	.dwattr $C$DW$1144, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1144, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1144, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$152


$C$DW$T$153	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$153, DW_AT_name("PIEIFR9_BITS")
	.dwattr $C$DW$T$153, DW_AT_byte_size(0x01)
$C$DW$1145	.dwtag  DW_TAG_member
	.dwattr $C$DW$1145, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1145, DW_AT_name("INTx1")
	.dwattr $C$DW$1145, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$1145, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$1145, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1145, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1145, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1146	.dwtag  DW_TAG_member
	.dwattr $C$DW$1146, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1146, DW_AT_name("INTx2")
	.dwattr $C$DW$1146, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$1146, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$1146, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1146, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1146, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1147	.dwtag  DW_TAG_member
	.dwattr $C$DW$1147, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1147, DW_AT_name("INTx3")
	.dwattr $C$DW$1147, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$1147, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$1147, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1147, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1147, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1148	.dwtag  DW_TAG_member
	.dwattr $C$DW$1148, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1148, DW_AT_name("INTx4")
	.dwattr $C$DW$1148, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$1148, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$1148, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1148, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1148, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1149	.dwtag  DW_TAG_member
	.dwattr $C$DW$1149, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1149, DW_AT_name("INTx5")
	.dwattr $C$DW$1149, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$1149, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$1149, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1149, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1149, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1150	.dwtag  DW_TAG_member
	.dwattr $C$DW$1150, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1150, DW_AT_name("INTx6")
	.dwattr $C$DW$1150, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$1150, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$1150, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1150, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1150, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1151	.dwtag  DW_TAG_member
	.dwattr $C$DW$1151, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1151, DW_AT_name("INTx7")
	.dwattr $C$DW$1151, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$1151, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$1151, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1151, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1151, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1152	.dwtag  DW_TAG_member
	.dwattr $C$DW$1152, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1152, DW_AT_name("INTx8")
	.dwattr $C$DW$1152, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$1152, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$1152, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1152, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1152, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1153	.dwtag  DW_TAG_member
	.dwattr $C$DW$1153, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1153, DW_AT_name("INTx9")
	.dwattr $C$DW$1153, DW_AT_TI_symbol_name("_INTx9")
	.dwattr $C$DW$1153, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$1153, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1153, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1153, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1154	.dwtag  DW_TAG_member
	.dwattr $C$DW$1154, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1154, DW_AT_name("INTx10")
	.dwattr $C$DW$1154, DW_AT_TI_symbol_name("_INTx10")
	.dwattr $C$DW$1154, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$1154, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1154, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1154, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1155	.dwtag  DW_TAG_member
	.dwattr $C$DW$1155, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1155, DW_AT_name("INTx11")
	.dwattr $C$DW$1155, DW_AT_TI_symbol_name("_INTx11")
	.dwattr $C$DW$1155, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$1155, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1155, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1155, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1156	.dwtag  DW_TAG_member
	.dwattr $C$DW$1156, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1156, DW_AT_name("INTx12")
	.dwattr $C$DW$1156, DW_AT_TI_symbol_name("_INTx12")
	.dwattr $C$DW$1156, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$1156, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1156, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1156, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1157	.dwtag  DW_TAG_member
	.dwattr $C$DW$1157, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1157, DW_AT_name("INTx13")
	.dwattr $C$DW$1157, DW_AT_TI_symbol_name("_INTx13")
	.dwattr $C$DW$1157, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$1157, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1157, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1157, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1158	.dwtag  DW_TAG_member
	.dwattr $C$DW$1158, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1158, DW_AT_name("INTx14")
	.dwattr $C$DW$1158, DW_AT_TI_symbol_name("_INTx14")
	.dwattr $C$DW$1158, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$1158, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1158, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1158, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1159	.dwtag  DW_TAG_member
	.dwattr $C$DW$1159, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1159, DW_AT_name("INTx15")
	.dwattr $C$DW$1159, DW_AT_TI_symbol_name("_INTx15")
	.dwattr $C$DW$1159, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$1159, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1159, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1159, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1160	.dwtag  DW_TAG_member
	.dwattr $C$DW$1160, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1160, DW_AT_name("INTx16")
	.dwattr $C$DW$1160, DW_AT_TI_symbol_name("_INTx16")
	.dwattr $C$DW$1160, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$1160, DW_AT_bit_size(0x01)
	.dwattr $C$DW$1160, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1160, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$153


$C$DW$T$154	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$154, DW_AT_name("PIEIFR9_REG")
	.dwattr $C$DW$T$154, DW_AT_byte_size(0x01)
$C$DW$1161	.dwtag  DW_TAG_member
	.dwattr $C$DW$1161, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1161, DW_AT_name("all")
	.dwattr $C$DW$1161, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1161, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1161, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1162	.dwtag  DW_TAG_member
	.dwattr $C$DW$1162, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$1162, DW_AT_name("bit")
	.dwattr $C$DW$1162, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1162, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1162, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$154


$C$DW$T$155	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$155, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$155, DW_AT_byte_size(0x1a)
$C$DW$1163	.dwtag  DW_TAG_member
	.dwattr $C$DW$1163, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$1163, DW_AT_name("PIECTRL")
	.dwattr $C$DW$1163, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$1163, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1163, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1164	.dwtag  DW_TAG_member
	.dwattr $C$DW$1164, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$1164, DW_AT_name("PIEACK")
	.dwattr $C$DW$1164, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$1164, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1164, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1165	.dwtag  DW_TAG_member
	.dwattr $C$DW$1165, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$1165, DW_AT_name("PIEIER1")
	.dwattr $C$DW$1165, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$1165, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1165, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1166	.dwtag  DW_TAG_member
	.dwattr $C$DW$1166, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$1166, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$1166, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$1166, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1166, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1167	.dwtag  DW_TAG_member
	.dwattr $C$DW$1167, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$1167, DW_AT_name("PIEIER2")
	.dwattr $C$DW$1167, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$1167, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1167, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1168	.dwtag  DW_TAG_member
	.dwattr $C$DW$1168, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$1168, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$1168, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$1168, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$1168, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1169	.dwtag  DW_TAG_member
	.dwattr $C$DW$1169, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$1169, DW_AT_name("PIEIER3")
	.dwattr $C$DW$1169, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$1169, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1169, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1170	.dwtag  DW_TAG_member
	.dwattr $C$DW$1170, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$1170, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$1170, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$1170, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$1170, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1171	.dwtag  DW_TAG_member
	.dwattr $C$DW$1171, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$1171, DW_AT_name("PIEIER4")
	.dwattr $C$DW$1171, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$1171, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1171, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1172	.dwtag  DW_TAG_member
	.dwattr $C$DW$1172, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$1172, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$1172, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$1172, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$1172, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1173	.dwtag  DW_TAG_member
	.dwattr $C$DW$1173, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$1173, DW_AT_name("PIEIER5")
	.dwattr $C$DW$1173, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$1173, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$1173, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1174	.dwtag  DW_TAG_member
	.dwattr $C$DW$1174, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$1174, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$1174, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$1174, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$1174, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1175	.dwtag  DW_TAG_member
	.dwattr $C$DW$1175, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$1175, DW_AT_name("PIEIER6")
	.dwattr $C$DW$1175, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$1175, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1175, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1176	.dwtag  DW_TAG_member
	.dwattr $C$DW$1176, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$1176, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$1176, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$1176, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$1176, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1177	.dwtag  DW_TAG_member
	.dwattr $C$DW$1177, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$1177, DW_AT_name("PIEIER7")
	.dwattr $C$DW$1177, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$1177, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$1177, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1178	.dwtag  DW_TAG_member
	.dwattr $C$DW$1178, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$1178, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$1178, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$1178, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$1178, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1179	.dwtag  DW_TAG_member
	.dwattr $C$DW$1179, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$1179, DW_AT_name("PIEIER8")
	.dwattr $C$DW$1179, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$1179, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1179, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1180	.dwtag  DW_TAG_member
	.dwattr $C$DW$1180, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$1180, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$1180, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$1180, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$1180, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1181	.dwtag  DW_TAG_member
	.dwattr $C$DW$1181, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$1181, DW_AT_name("PIEIER9")
	.dwattr $C$DW$1181, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$1181, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$1181, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1182	.dwtag  DW_TAG_member
	.dwattr $C$DW$1182, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$1182, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$1182, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$1182, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$1182, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1183	.dwtag  DW_TAG_member
	.dwattr $C$DW$1183, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$1183, DW_AT_name("PIEIER10")
	.dwattr $C$DW$1183, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$1183, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1183, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1184	.dwtag  DW_TAG_member
	.dwattr $C$DW$1184, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$1184, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$1184, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$1184, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$1184, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1185	.dwtag  DW_TAG_member
	.dwattr $C$DW$1185, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$1185, DW_AT_name("PIEIER11")
	.dwattr $C$DW$1185, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$1185, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$1185, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1186	.dwtag  DW_TAG_member
	.dwattr $C$DW$1186, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$1186, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$1186, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$1186, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$1186, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1187	.dwtag  DW_TAG_member
	.dwattr $C$DW$1187, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$1187, DW_AT_name("PIEIER12")
	.dwattr $C$DW$1187, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$1187, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1187, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1188	.dwtag  DW_TAG_member
	.dwattr $C$DW$1188, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$1188, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$1188, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$1188, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$1188, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$155

$C$DW$1189	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1189, DW_AT_type(*$C$DW$T$155)

$C$DW$T$175	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$1189)


$C$DW$T$159	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$159, DW_AT_name("PIE_VECT_TABLE")
	.dwattr $C$DW$T$159, DW_AT_byte_size(0x1c0)
$C$DW$1190	.dwtag  DW_TAG_member
	.dwattr $C$DW$1190, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1190, DW_AT_name("PIE1_RESERVED_INT")
	.dwattr $C$DW$1190, DW_AT_TI_symbol_name("_PIE1_RESERVED_INT")
	.dwattr $C$DW$1190, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1190, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1191	.dwtag  DW_TAG_member
	.dwattr $C$DW$1191, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1191, DW_AT_name("PIE2_RESERVED_INT")
	.dwattr $C$DW$1191, DW_AT_TI_symbol_name("_PIE2_RESERVED_INT")
	.dwattr $C$DW$1191, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1191, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1192	.dwtag  DW_TAG_member
	.dwattr $C$DW$1192, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1192, DW_AT_name("PIE3_RESERVED_INT")
	.dwattr $C$DW$1192, DW_AT_TI_symbol_name("_PIE3_RESERVED_INT")
	.dwattr $C$DW$1192, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1192, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1193	.dwtag  DW_TAG_member
	.dwattr $C$DW$1193, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1193, DW_AT_name("PIE4_RESERVED_INT")
	.dwattr $C$DW$1193, DW_AT_TI_symbol_name("_PIE4_RESERVED_INT")
	.dwattr $C$DW$1193, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1193, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1194	.dwtag  DW_TAG_member
	.dwattr $C$DW$1194, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1194, DW_AT_name("PIE5_RESERVED_INT")
	.dwattr $C$DW$1194, DW_AT_TI_symbol_name("_PIE5_RESERVED_INT")
	.dwattr $C$DW$1194, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1194, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1195	.dwtag  DW_TAG_member
	.dwattr $C$DW$1195, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1195, DW_AT_name("PIE6_RESERVED_INT")
	.dwattr $C$DW$1195, DW_AT_TI_symbol_name("_PIE6_RESERVED_INT")
	.dwattr $C$DW$1195, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$1195, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1196	.dwtag  DW_TAG_member
	.dwattr $C$DW$1196, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1196, DW_AT_name("PIE7_RESERVED_INT")
	.dwattr $C$DW$1196, DW_AT_TI_symbol_name("_PIE7_RESERVED_INT")
	.dwattr $C$DW$1196, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1196, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1197	.dwtag  DW_TAG_member
	.dwattr $C$DW$1197, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1197, DW_AT_name("PIE8_RESERVED_INT")
	.dwattr $C$DW$1197, DW_AT_TI_symbol_name("_PIE8_RESERVED_INT")
	.dwattr $C$DW$1197, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$1197, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1198	.dwtag  DW_TAG_member
	.dwattr $C$DW$1198, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1198, DW_AT_name("PIE9_RESERVED_INT")
	.dwattr $C$DW$1198, DW_AT_TI_symbol_name("_PIE9_RESERVED_INT")
	.dwattr $C$DW$1198, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1198, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1199	.dwtag  DW_TAG_member
	.dwattr $C$DW$1199, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1199, DW_AT_name("PIE10_RESERVED_INT")
	.dwattr $C$DW$1199, DW_AT_TI_symbol_name("_PIE10_RESERVED_INT")
	.dwattr $C$DW$1199, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$1199, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1200	.dwtag  DW_TAG_member
	.dwattr $C$DW$1200, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1200, DW_AT_name("PIE11_RESERVED_INT")
	.dwattr $C$DW$1200, DW_AT_TI_symbol_name("_PIE11_RESERVED_INT")
	.dwattr $C$DW$1200, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1200, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1201	.dwtag  DW_TAG_member
	.dwattr $C$DW$1201, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1201, DW_AT_name("PIE12_RESERVED_INT")
	.dwattr $C$DW$1201, DW_AT_TI_symbol_name("_PIE12_RESERVED_INT")
	.dwattr $C$DW$1201, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$1201, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1202	.dwtag  DW_TAG_member
	.dwattr $C$DW$1202, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1202, DW_AT_name("PIE13_RESERVED_INT")
	.dwattr $C$DW$1202, DW_AT_TI_symbol_name("_PIE13_RESERVED_INT")
	.dwattr $C$DW$1202, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1202, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1203	.dwtag  DW_TAG_member
	.dwattr $C$DW$1203, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1203, DW_AT_name("TIMER1_INT")
	.dwattr $C$DW$1203, DW_AT_TI_symbol_name("_TIMER1_INT")
	.dwattr $C$DW$1203, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$1203, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1204	.dwtag  DW_TAG_member
	.dwattr $C$DW$1204, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1204, DW_AT_name("TIMER2_INT")
	.dwattr $C$DW$1204, DW_AT_TI_symbol_name("_TIMER2_INT")
	.dwattr $C$DW$1204, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1204, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1205	.dwtag  DW_TAG_member
	.dwattr $C$DW$1205, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1205, DW_AT_name("DATALOG_INT")
	.dwattr $C$DW$1205, DW_AT_TI_symbol_name("_DATALOG_INT")
	.dwattr $C$DW$1205, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$1205, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1206	.dwtag  DW_TAG_member
	.dwattr $C$DW$1206, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1206, DW_AT_name("RTOS_INT")
	.dwattr $C$DW$1206, DW_AT_TI_symbol_name("_RTOS_INT")
	.dwattr $C$DW$1206, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1206, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1207	.dwtag  DW_TAG_member
	.dwattr $C$DW$1207, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1207, DW_AT_name("EMU_INT")
	.dwattr $C$DW$1207, DW_AT_TI_symbol_name("_EMU_INT")
	.dwattr $C$DW$1207, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$1207, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1208	.dwtag  DW_TAG_member
	.dwattr $C$DW$1208, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1208, DW_AT_name("NMI_INT")
	.dwattr $C$DW$1208, DW_AT_TI_symbol_name("_NMI_INT")
	.dwattr $C$DW$1208, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1208, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1209	.dwtag  DW_TAG_member
	.dwattr $C$DW$1209, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1209, DW_AT_name("ILLEGAL_INT")
	.dwattr $C$DW$1209, DW_AT_TI_symbol_name("_ILLEGAL_INT")
	.dwattr $C$DW$1209, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$1209, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1210	.dwtag  DW_TAG_member
	.dwattr $C$DW$1210, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1210, DW_AT_name("USER1_INT")
	.dwattr $C$DW$1210, DW_AT_TI_symbol_name("_USER1_INT")
	.dwattr $C$DW$1210, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1210, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1211	.dwtag  DW_TAG_member
	.dwattr $C$DW$1211, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1211, DW_AT_name("USER2_INT")
	.dwattr $C$DW$1211, DW_AT_TI_symbol_name("_USER2_INT")
	.dwattr $C$DW$1211, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$1211, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1212	.dwtag  DW_TAG_member
	.dwattr $C$DW$1212, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1212, DW_AT_name("USER3_INT")
	.dwattr $C$DW$1212, DW_AT_TI_symbol_name("_USER3_INT")
	.dwattr $C$DW$1212, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1212, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1213	.dwtag  DW_TAG_member
	.dwattr $C$DW$1213, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1213, DW_AT_name("USER4_INT")
	.dwattr $C$DW$1213, DW_AT_TI_symbol_name("_USER4_INT")
	.dwattr $C$DW$1213, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$1213, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1214	.dwtag  DW_TAG_member
	.dwattr $C$DW$1214, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1214, DW_AT_name("USER5_INT")
	.dwattr $C$DW$1214, DW_AT_TI_symbol_name("_USER5_INT")
	.dwattr $C$DW$1214, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1214, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1215	.dwtag  DW_TAG_member
	.dwattr $C$DW$1215, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1215, DW_AT_name("USER6_INT")
	.dwattr $C$DW$1215, DW_AT_TI_symbol_name("_USER6_INT")
	.dwattr $C$DW$1215, DW_AT_data_member_location[DW_OP_plus_uconst 0x32]
	.dwattr $C$DW$1215, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1216	.dwtag  DW_TAG_member
	.dwattr $C$DW$1216, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1216, DW_AT_name("USER7_INT")
	.dwattr $C$DW$1216, DW_AT_TI_symbol_name("_USER7_INT")
	.dwattr $C$DW$1216, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$1216, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1217	.dwtag  DW_TAG_member
	.dwattr $C$DW$1217, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1217, DW_AT_name("USER8_INT")
	.dwattr $C$DW$1217, DW_AT_TI_symbol_name("_USER8_INT")
	.dwattr $C$DW$1217, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$1217, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1218	.dwtag  DW_TAG_member
	.dwattr $C$DW$1218, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1218, DW_AT_name("USER9_INT")
	.dwattr $C$DW$1218, DW_AT_TI_symbol_name("_USER9_INT")
	.dwattr $C$DW$1218, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$1218, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1219	.dwtag  DW_TAG_member
	.dwattr $C$DW$1219, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1219, DW_AT_name("USER10_INT")
	.dwattr $C$DW$1219, DW_AT_TI_symbol_name("_USER10_INT")
	.dwattr $C$DW$1219, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$1219, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1220	.dwtag  DW_TAG_member
	.dwattr $C$DW$1220, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1220, DW_AT_name("USER11_INT")
	.dwattr $C$DW$1220, DW_AT_TI_symbol_name("_USER11_INT")
	.dwattr $C$DW$1220, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$1220, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1221	.dwtag  DW_TAG_member
	.dwattr $C$DW$1221, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1221, DW_AT_name("USER12_INT")
	.dwattr $C$DW$1221, DW_AT_TI_symbol_name("_USER12_INT")
	.dwattr $C$DW$1221, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$1221, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1222	.dwtag  DW_TAG_member
	.dwattr $C$DW$1222, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1222, DW_AT_name("ADCA1_INT")
	.dwattr $C$DW$1222, DW_AT_TI_symbol_name("_ADCA1_INT")
	.dwattr $C$DW$1222, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$1222, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1223	.dwtag  DW_TAG_member
	.dwattr $C$DW$1223, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1223, DW_AT_name("ADCB1_INT")
	.dwattr $C$DW$1223, DW_AT_TI_symbol_name("_ADCB1_INT")
	.dwattr $C$DW$1223, DW_AT_data_member_location[DW_OP_plus_uconst 0x42]
	.dwattr $C$DW$1223, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1224	.dwtag  DW_TAG_member
	.dwattr $C$DW$1224, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1224, DW_AT_name("ADCC1_INT")
	.dwattr $C$DW$1224, DW_AT_TI_symbol_name("_ADCC1_INT")
	.dwattr $C$DW$1224, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$1224, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1225	.dwtag  DW_TAG_member
	.dwattr $C$DW$1225, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1225, DW_AT_name("XINT1_INT")
	.dwattr $C$DW$1225, DW_AT_TI_symbol_name("_XINT1_INT")
	.dwattr $C$DW$1225, DW_AT_data_member_location[DW_OP_plus_uconst 0x46]
	.dwattr $C$DW$1225, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1226	.dwtag  DW_TAG_member
	.dwattr $C$DW$1226, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1226, DW_AT_name("XINT2_INT")
	.dwattr $C$DW$1226, DW_AT_TI_symbol_name("_XINT2_INT")
	.dwattr $C$DW$1226, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$1226, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1227	.dwtag  DW_TAG_member
	.dwattr $C$DW$1227, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1227, DW_AT_name("ADCD1_INT")
	.dwattr $C$DW$1227, DW_AT_TI_symbol_name("_ADCD1_INT")
	.dwattr $C$DW$1227, DW_AT_data_member_location[DW_OP_plus_uconst 0x4a]
	.dwattr $C$DW$1227, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1228	.dwtag  DW_TAG_member
	.dwattr $C$DW$1228, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1228, DW_AT_name("TIMER0_INT")
	.dwattr $C$DW$1228, DW_AT_TI_symbol_name("_TIMER0_INT")
	.dwattr $C$DW$1228, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$1228, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1229	.dwtag  DW_TAG_member
	.dwattr $C$DW$1229, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1229, DW_AT_name("WAKE_INT")
	.dwattr $C$DW$1229, DW_AT_TI_symbol_name("_WAKE_INT")
	.dwattr $C$DW$1229, DW_AT_data_member_location[DW_OP_plus_uconst 0x4e]
	.dwattr $C$DW$1229, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1230	.dwtag  DW_TAG_member
	.dwattr $C$DW$1230, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1230, DW_AT_name("EPWM1_TZ_INT")
	.dwattr $C$DW$1230, DW_AT_TI_symbol_name("_EPWM1_TZ_INT")
	.dwattr $C$DW$1230, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$1230, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1231	.dwtag  DW_TAG_member
	.dwattr $C$DW$1231, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1231, DW_AT_name("EPWM2_TZ_INT")
	.dwattr $C$DW$1231, DW_AT_TI_symbol_name("_EPWM2_TZ_INT")
	.dwattr $C$DW$1231, DW_AT_data_member_location[DW_OP_plus_uconst 0x52]
	.dwattr $C$DW$1231, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1232	.dwtag  DW_TAG_member
	.dwattr $C$DW$1232, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1232, DW_AT_name("EPWM3_TZ_INT")
	.dwattr $C$DW$1232, DW_AT_TI_symbol_name("_EPWM3_TZ_INT")
	.dwattr $C$DW$1232, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$1232, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1233	.dwtag  DW_TAG_member
	.dwattr $C$DW$1233, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1233, DW_AT_name("EPWM4_TZ_INT")
	.dwattr $C$DW$1233, DW_AT_TI_symbol_name("_EPWM4_TZ_INT")
	.dwattr $C$DW$1233, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$1233, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1234	.dwtag  DW_TAG_member
	.dwattr $C$DW$1234, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1234, DW_AT_name("EPWM5_TZ_INT")
	.dwattr $C$DW$1234, DW_AT_TI_symbol_name("_EPWM5_TZ_INT")
	.dwattr $C$DW$1234, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$1234, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1235	.dwtag  DW_TAG_member
	.dwattr $C$DW$1235, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1235, DW_AT_name("EPWM6_TZ_INT")
	.dwattr $C$DW$1235, DW_AT_TI_symbol_name("_EPWM6_TZ_INT")
	.dwattr $C$DW$1235, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$1235, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1236	.dwtag  DW_TAG_member
	.dwattr $C$DW$1236, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1236, DW_AT_name("EPWM7_TZ_INT")
	.dwattr $C$DW$1236, DW_AT_TI_symbol_name("_EPWM7_TZ_INT")
	.dwattr $C$DW$1236, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$1236, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1237	.dwtag  DW_TAG_member
	.dwattr $C$DW$1237, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1237, DW_AT_name("EPWM8_TZ_INT")
	.dwattr $C$DW$1237, DW_AT_TI_symbol_name("_EPWM8_TZ_INT")
	.dwattr $C$DW$1237, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$1237, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1238	.dwtag  DW_TAG_member
	.dwattr $C$DW$1238, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1238, DW_AT_name("EPWM1_INT")
	.dwattr $C$DW$1238, DW_AT_TI_symbol_name("_EPWM1_INT")
	.dwattr $C$DW$1238, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$1238, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1239	.dwtag  DW_TAG_member
	.dwattr $C$DW$1239, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1239, DW_AT_name("EPWM2_INT")
	.dwattr $C$DW$1239, DW_AT_TI_symbol_name("_EPWM2_INT")
	.dwattr $C$DW$1239, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$1239, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1240	.dwtag  DW_TAG_member
	.dwattr $C$DW$1240, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1240, DW_AT_name("EPWM3_INT")
	.dwattr $C$DW$1240, DW_AT_TI_symbol_name("_EPWM3_INT")
	.dwattr $C$DW$1240, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$1240, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1241	.dwtag  DW_TAG_member
	.dwattr $C$DW$1241, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1241, DW_AT_name("EPWM4_INT")
	.dwattr $C$DW$1241, DW_AT_TI_symbol_name("_EPWM4_INT")
	.dwattr $C$DW$1241, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$1241, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1242	.dwtag  DW_TAG_member
	.dwattr $C$DW$1242, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1242, DW_AT_name("EPWM5_INT")
	.dwattr $C$DW$1242, DW_AT_TI_symbol_name("_EPWM5_INT")
	.dwattr $C$DW$1242, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$1242, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1243	.dwtag  DW_TAG_member
	.dwattr $C$DW$1243, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1243, DW_AT_name("EPWM6_INT")
	.dwattr $C$DW$1243, DW_AT_TI_symbol_name("_EPWM6_INT")
	.dwattr $C$DW$1243, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$1243, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1244	.dwtag  DW_TAG_member
	.dwattr $C$DW$1244, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1244, DW_AT_name("EPWM7_INT")
	.dwattr $C$DW$1244, DW_AT_TI_symbol_name("_EPWM7_INT")
	.dwattr $C$DW$1244, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$1244, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1245	.dwtag  DW_TAG_member
	.dwattr $C$DW$1245, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1245, DW_AT_name("EPWM8_INT")
	.dwattr $C$DW$1245, DW_AT_TI_symbol_name("_EPWM8_INT")
	.dwattr $C$DW$1245, DW_AT_data_member_location[DW_OP_plus_uconst 0x6e]
	.dwattr $C$DW$1245, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1246	.dwtag  DW_TAG_member
	.dwattr $C$DW$1246, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1246, DW_AT_name("ECAP1_INT")
	.dwattr $C$DW$1246, DW_AT_TI_symbol_name("_ECAP1_INT")
	.dwattr $C$DW$1246, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$1246, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1247	.dwtag  DW_TAG_member
	.dwattr $C$DW$1247, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1247, DW_AT_name("ECAP2_INT")
	.dwattr $C$DW$1247, DW_AT_TI_symbol_name("_ECAP2_INT")
	.dwattr $C$DW$1247, DW_AT_data_member_location[DW_OP_plus_uconst 0x72]
	.dwattr $C$DW$1247, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1248	.dwtag  DW_TAG_member
	.dwattr $C$DW$1248, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1248, DW_AT_name("ECAP3_INT")
	.dwattr $C$DW$1248, DW_AT_TI_symbol_name("_ECAP3_INT")
	.dwattr $C$DW$1248, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$1248, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1249	.dwtag  DW_TAG_member
	.dwattr $C$DW$1249, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1249, DW_AT_name("ECAP4_INT")
	.dwattr $C$DW$1249, DW_AT_TI_symbol_name("_ECAP4_INT")
	.dwattr $C$DW$1249, DW_AT_data_member_location[DW_OP_plus_uconst 0x76]
	.dwattr $C$DW$1249, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1250	.dwtag  DW_TAG_member
	.dwattr $C$DW$1250, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1250, DW_AT_name("ECAP5_INT")
	.dwattr $C$DW$1250, DW_AT_TI_symbol_name("_ECAP5_INT")
	.dwattr $C$DW$1250, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$1250, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1251	.dwtag  DW_TAG_member
	.dwattr $C$DW$1251, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1251, DW_AT_name("ECAP6_INT")
	.dwattr $C$DW$1251, DW_AT_TI_symbol_name("_ECAP6_INT")
	.dwattr $C$DW$1251, DW_AT_data_member_location[DW_OP_plus_uconst 0x7a]
	.dwattr $C$DW$1251, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1252	.dwtag  DW_TAG_member
	.dwattr $C$DW$1252, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1252, DW_AT_name("PIE14_RESERVED_INT")
	.dwattr $C$DW$1252, DW_AT_TI_symbol_name("_PIE14_RESERVED_INT")
	.dwattr $C$DW$1252, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$1252, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1253	.dwtag  DW_TAG_member
	.dwattr $C$DW$1253, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1253, DW_AT_name("PIE15_RESERVED_INT")
	.dwattr $C$DW$1253, DW_AT_TI_symbol_name("_PIE15_RESERVED_INT")
	.dwattr $C$DW$1253, DW_AT_data_member_location[DW_OP_plus_uconst 0x7e]
	.dwattr $C$DW$1253, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1254	.dwtag  DW_TAG_member
	.dwattr $C$DW$1254, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1254, DW_AT_name("EQEP1_INT")
	.dwattr $C$DW$1254, DW_AT_TI_symbol_name("_EQEP1_INT")
	.dwattr $C$DW$1254, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$1254, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1255	.dwtag  DW_TAG_member
	.dwattr $C$DW$1255, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1255, DW_AT_name("EQEP2_INT")
	.dwattr $C$DW$1255, DW_AT_TI_symbol_name("_EQEP2_INT")
	.dwattr $C$DW$1255, DW_AT_data_member_location[DW_OP_plus_uconst 0x82]
	.dwattr $C$DW$1255, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1256	.dwtag  DW_TAG_member
	.dwattr $C$DW$1256, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1256, DW_AT_name("EQEP3_INT")
	.dwattr $C$DW$1256, DW_AT_TI_symbol_name("_EQEP3_INT")
	.dwattr $C$DW$1256, DW_AT_data_member_location[DW_OP_plus_uconst 0x84]
	.dwattr $C$DW$1256, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1257	.dwtag  DW_TAG_member
	.dwattr $C$DW$1257, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1257, DW_AT_name("PIE16_RESERVED_INT")
	.dwattr $C$DW$1257, DW_AT_TI_symbol_name("_PIE16_RESERVED_INT")
	.dwattr $C$DW$1257, DW_AT_data_member_location[DW_OP_plus_uconst 0x86]
	.dwattr $C$DW$1257, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1258	.dwtag  DW_TAG_member
	.dwattr $C$DW$1258, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1258, DW_AT_name("PIE17_RESERVED_INT")
	.dwattr $C$DW$1258, DW_AT_TI_symbol_name("_PIE17_RESERVED_INT")
	.dwattr $C$DW$1258, DW_AT_data_member_location[DW_OP_plus_uconst 0x88]
	.dwattr $C$DW$1258, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1259	.dwtag  DW_TAG_member
	.dwattr $C$DW$1259, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1259, DW_AT_name("PIE18_RESERVED_INT")
	.dwattr $C$DW$1259, DW_AT_TI_symbol_name("_PIE18_RESERVED_INT")
	.dwattr $C$DW$1259, DW_AT_data_member_location[DW_OP_plus_uconst 0x8a]
	.dwattr $C$DW$1259, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1260	.dwtag  DW_TAG_member
	.dwattr $C$DW$1260, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1260, DW_AT_name("PIE19_RESERVED_INT")
	.dwattr $C$DW$1260, DW_AT_TI_symbol_name("_PIE19_RESERVED_INT")
	.dwattr $C$DW$1260, DW_AT_data_member_location[DW_OP_plus_uconst 0x8c]
	.dwattr $C$DW$1260, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1261	.dwtag  DW_TAG_member
	.dwattr $C$DW$1261, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1261, DW_AT_name("PIE20_RESERVED_INT")
	.dwattr $C$DW$1261, DW_AT_TI_symbol_name("_PIE20_RESERVED_INT")
	.dwattr $C$DW$1261, DW_AT_data_member_location[DW_OP_plus_uconst 0x8e]
	.dwattr $C$DW$1261, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1262	.dwtag  DW_TAG_member
	.dwattr $C$DW$1262, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1262, DW_AT_name("SPIA_RX_INT")
	.dwattr $C$DW$1262, DW_AT_TI_symbol_name("_SPIA_RX_INT")
	.dwattr $C$DW$1262, DW_AT_data_member_location[DW_OP_plus_uconst 0x90]
	.dwattr $C$DW$1262, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1263	.dwtag  DW_TAG_member
	.dwattr $C$DW$1263, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1263, DW_AT_name("SPIA_TX_INT")
	.dwattr $C$DW$1263, DW_AT_TI_symbol_name("_SPIA_TX_INT")
	.dwattr $C$DW$1263, DW_AT_data_member_location[DW_OP_plus_uconst 0x92]
	.dwattr $C$DW$1263, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1264	.dwtag  DW_TAG_member
	.dwattr $C$DW$1264, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1264, DW_AT_name("SPIB_RX_INT")
	.dwattr $C$DW$1264, DW_AT_TI_symbol_name("_SPIB_RX_INT")
	.dwattr $C$DW$1264, DW_AT_data_member_location[DW_OP_plus_uconst 0x94]
	.dwattr $C$DW$1264, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1265	.dwtag  DW_TAG_member
	.dwattr $C$DW$1265, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1265, DW_AT_name("SPIB_TX_INT")
	.dwattr $C$DW$1265, DW_AT_TI_symbol_name("_SPIB_TX_INT")
	.dwattr $C$DW$1265, DW_AT_data_member_location[DW_OP_plus_uconst 0x96]
	.dwattr $C$DW$1265, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1266	.dwtag  DW_TAG_member
	.dwattr $C$DW$1266, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1266, DW_AT_name("MCBSPA_RX_INT")
	.dwattr $C$DW$1266, DW_AT_TI_symbol_name("_MCBSPA_RX_INT")
	.dwattr $C$DW$1266, DW_AT_data_member_location[DW_OP_plus_uconst 0x98]
	.dwattr $C$DW$1266, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1267	.dwtag  DW_TAG_member
	.dwattr $C$DW$1267, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1267, DW_AT_name("MCBSPA_TX_INT")
	.dwattr $C$DW$1267, DW_AT_TI_symbol_name("_MCBSPA_TX_INT")
	.dwattr $C$DW$1267, DW_AT_data_member_location[DW_OP_plus_uconst 0x9a]
	.dwattr $C$DW$1267, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1268	.dwtag  DW_TAG_member
	.dwattr $C$DW$1268, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1268, DW_AT_name("MCBSPB_RX_INT")
	.dwattr $C$DW$1268, DW_AT_TI_symbol_name("_MCBSPB_RX_INT")
	.dwattr $C$DW$1268, DW_AT_data_member_location[DW_OP_plus_uconst 0x9c]
	.dwattr $C$DW$1268, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1269	.dwtag  DW_TAG_member
	.dwattr $C$DW$1269, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1269, DW_AT_name("MCBSPB_TX_INT")
	.dwattr $C$DW$1269, DW_AT_TI_symbol_name("_MCBSPB_TX_INT")
	.dwattr $C$DW$1269, DW_AT_data_member_location[DW_OP_plus_uconst 0x9e]
	.dwattr $C$DW$1269, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1270	.dwtag  DW_TAG_member
	.dwattr $C$DW$1270, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1270, DW_AT_name("DMA_CH1_INT")
	.dwattr $C$DW$1270, DW_AT_TI_symbol_name("_DMA_CH1_INT")
	.dwattr $C$DW$1270, DW_AT_data_member_location[DW_OP_plus_uconst 0xa0]
	.dwattr $C$DW$1270, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1271	.dwtag  DW_TAG_member
	.dwattr $C$DW$1271, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1271, DW_AT_name("DMA_CH2_INT")
	.dwattr $C$DW$1271, DW_AT_TI_symbol_name("_DMA_CH2_INT")
	.dwattr $C$DW$1271, DW_AT_data_member_location[DW_OP_plus_uconst 0xa2]
	.dwattr $C$DW$1271, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1272	.dwtag  DW_TAG_member
	.dwattr $C$DW$1272, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1272, DW_AT_name("DMA_CH3_INT")
	.dwattr $C$DW$1272, DW_AT_TI_symbol_name("_DMA_CH3_INT")
	.dwattr $C$DW$1272, DW_AT_data_member_location[DW_OP_plus_uconst 0xa4]
	.dwattr $C$DW$1272, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1273	.dwtag  DW_TAG_member
	.dwattr $C$DW$1273, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1273, DW_AT_name("DMA_CH4_INT")
	.dwattr $C$DW$1273, DW_AT_TI_symbol_name("_DMA_CH4_INT")
	.dwattr $C$DW$1273, DW_AT_data_member_location[DW_OP_plus_uconst 0xa6]
	.dwattr $C$DW$1273, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1274	.dwtag  DW_TAG_member
	.dwattr $C$DW$1274, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1274, DW_AT_name("DMA_CH5_INT")
	.dwattr $C$DW$1274, DW_AT_TI_symbol_name("_DMA_CH5_INT")
	.dwattr $C$DW$1274, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$1274, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1275	.dwtag  DW_TAG_member
	.dwattr $C$DW$1275, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1275, DW_AT_name("DMA_CH6_INT")
	.dwattr $C$DW$1275, DW_AT_TI_symbol_name("_DMA_CH6_INT")
	.dwattr $C$DW$1275, DW_AT_data_member_location[DW_OP_plus_uconst 0xaa]
	.dwattr $C$DW$1275, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1276	.dwtag  DW_TAG_member
	.dwattr $C$DW$1276, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1276, DW_AT_name("PIE21_RESERVED_INT")
	.dwattr $C$DW$1276, DW_AT_TI_symbol_name("_PIE21_RESERVED_INT")
	.dwattr $C$DW$1276, DW_AT_data_member_location[DW_OP_plus_uconst 0xac]
	.dwattr $C$DW$1276, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1277	.dwtag  DW_TAG_member
	.dwattr $C$DW$1277, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1277, DW_AT_name("PIE22_RESERVED_INT")
	.dwattr $C$DW$1277, DW_AT_TI_symbol_name("_PIE22_RESERVED_INT")
	.dwattr $C$DW$1277, DW_AT_data_member_location[DW_OP_plus_uconst 0xae]
	.dwattr $C$DW$1277, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1278	.dwtag  DW_TAG_member
	.dwattr $C$DW$1278, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1278, DW_AT_name("I2CA_INT")
	.dwattr $C$DW$1278, DW_AT_TI_symbol_name("_I2CA_INT")
	.dwattr $C$DW$1278, DW_AT_data_member_location[DW_OP_plus_uconst 0xb0]
	.dwattr $C$DW$1278, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1279	.dwtag  DW_TAG_member
	.dwattr $C$DW$1279, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1279, DW_AT_name("I2CA_FIFO_INT")
	.dwattr $C$DW$1279, DW_AT_TI_symbol_name("_I2CA_FIFO_INT")
	.dwattr $C$DW$1279, DW_AT_data_member_location[DW_OP_plus_uconst 0xb2]
	.dwattr $C$DW$1279, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1280	.dwtag  DW_TAG_member
	.dwattr $C$DW$1280, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1280, DW_AT_name("I2CB_INT")
	.dwattr $C$DW$1280, DW_AT_TI_symbol_name("_I2CB_INT")
	.dwattr $C$DW$1280, DW_AT_data_member_location[DW_OP_plus_uconst 0xb4]
	.dwattr $C$DW$1280, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1281	.dwtag  DW_TAG_member
	.dwattr $C$DW$1281, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1281, DW_AT_name("I2CB_FIFO_INT")
	.dwattr $C$DW$1281, DW_AT_TI_symbol_name("_I2CB_FIFO_INT")
	.dwattr $C$DW$1281, DW_AT_data_member_location[DW_OP_plus_uconst 0xb6]
	.dwattr $C$DW$1281, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1282	.dwtag  DW_TAG_member
	.dwattr $C$DW$1282, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1282, DW_AT_name("SCIC_RX_INT")
	.dwattr $C$DW$1282, DW_AT_TI_symbol_name("_SCIC_RX_INT")
	.dwattr $C$DW$1282, DW_AT_data_member_location[DW_OP_plus_uconst 0xb8]
	.dwattr $C$DW$1282, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1283	.dwtag  DW_TAG_member
	.dwattr $C$DW$1283, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1283, DW_AT_name("SCIC_TX_INT")
	.dwattr $C$DW$1283, DW_AT_TI_symbol_name("_SCIC_TX_INT")
	.dwattr $C$DW$1283, DW_AT_data_member_location[DW_OP_plus_uconst 0xba]
	.dwattr $C$DW$1283, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1284	.dwtag  DW_TAG_member
	.dwattr $C$DW$1284, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1284, DW_AT_name("SCID_RX_INT")
	.dwattr $C$DW$1284, DW_AT_TI_symbol_name("_SCID_RX_INT")
	.dwattr $C$DW$1284, DW_AT_data_member_location[DW_OP_plus_uconst 0xbc]
	.dwattr $C$DW$1284, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1285	.dwtag  DW_TAG_member
	.dwattr $C$DW$1285, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1285, DW_AT_name("SCID_TX_INT")
	.dwattr $C$DW$1285, DW_AT_TI_symbol_name("_SCID_TX_INT")
	.dwattr $C$DW$1285, DW_AT_data_member_location[DW_OP_plus_uconst 0xbe]
	.dwattr $C$DW$1285, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1286	.dwtag  DW_TAG_member
	.dwattr $C$DW$1286, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1286, DW_AT_name("SCIA_RX_INT")
	.dwattr $C$DW$1286, DW_AT_TI_symbol_name("_SCIA_RX_INT")
	.dwattr $C$DW$1286, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$1286, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1287	.dwtag  DW_TAG_member
	.dwattr $C$DW$1287, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1287, DW_AT_name("SCIA_TX_INT")
	.dwattr $C$DW$1287, DW_AT_TI_symbol_name("_SCIA_TX_INT")
	.dwattr $C$DW$1287, DW_AT_data_member_location[DW_OP_plus_uconst 0xc2]
	.dwattr $C$DW$1287, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1288	.dwtag  DW_TAG_member
	.dwattr $C$DW$1288, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1288, DW_AT_name("SCIB_RX_INT")
	.dwattr $C$DW$1288, DW_AT_TI_symbol_name("_SCIB_RX_INT")
	.dwattr $C$DW$1288, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$1288, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1289	.dwtag  DW_TAG_member
	.dwattr $C$DW$1289, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1289, DW_AT_name("SCIB_TX_INT")
	.dwattr $C$DW$1289, DW_AT_TI_symbol_name("_SCIB_TX_INT")
	.dwattr $C$DW$1289, DW_AT_data_member_location[DW_OP_plus_uconst 0xc6]
	.dwattr $C$DW$1289, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1290	.dwtag  DW_TAG_member
	.dwattr $C$DW$1290, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1290, DW_AT_name("CANA0_INT")
	.dwattr $C$DW$1290, DW_AT_TI_symbol_name("_CANA0_INT")
	.dwattr $C$DW$1290, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$1290, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1291	.dwtag  DW_TAG_member
	.dwattr $C$DW$1291, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1291, DW_AT_name("CANA1_INT")
	.dwattr $C$DW$1291, DW_AT_TI_symbol_name("_CANA1_INT")
	.dwattr $C$DW$1291, DW_AT_data_member_location[DW_OP_plus_uconst 0xca]
	.dwattr $C$DW$1291, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1292	.dwtag  DW_TAG_member
	.dwattr $C$DW$1292, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1292, DW_AT_name("CANB0_INT")
	.dwattr $C$DW$1292, DW_AT_TI_symbol_name("_CANB0_INT")
	.dwattr $C$DW$1292, DW_AT_data_member_location[DW_OP_plus_uconst 0xcc]
	.dwattr $C$DW$1292, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1293	.dwtag  DW_TAG_member
	.dwattr $C$DW$1293, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1293, DW_AT_name("CANB1_INT")
	.dwattr $C$DW$1293, DW_AT_TI_symbol_name("_CANB1_INT")
	.dwattr $C$DW$1293, DW_AT_data_member_location[DW_OP_plus_uconst 0xce]
	.dwattr $C$DW$1293, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1294	.dwtag  DW_TAG_member
	.dwattr $C$DW$1294, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1294, DW_AT_name("ADCA_EVT_INT")
	.dwattr $C$DW$1294, DW_AT_TI_symbol_name("_ADCA_EVT_INT")
	.dwattr $C$DW$1294, DW_AT_data_member_location[DW_OP_plus_uconst 0xd0]
	.dwattr $C$DW$1294, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1295	.dwtag  DW_TAG_member
	.dwattr $C$DW$1295, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1295, DW_AT_name("ADCA2_INT")
	.dwattr $C$DW$1295, DW_AT_TI_symbol_name("_ADCA2_INT")
	.dwattr $C$DW$1295, DW_AT_data_member_location[DW_OP_plus_uconst 0xd2]
	.dwattr $C$DW$1295, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1296	.dwtag  DW_TAG_member
	.dwattr $C$DW$1296, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1296, DW_AT_name("ADCA3_INT")
	.dwattr $C$DW$1296, DW_AT_TI_symbol_name("_ADCA3_INT")
	.dwattr $C$DW$1296, DW_AT_data_member_location[DW_OP_plus_uconst 0xd4]
	.dwattr $C$DW$1296, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1297	.dwtag  DW_TAG_member
	.dwattr $C$DW$1297, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1297, DW_AT_name("ADCA4_INT")
	.dwattr $C$DW$1297, DW_AT_TI_symbol_name("_ADCA4_INT")
	.dwattr $C$DW$1297, DW_AT_data_member_location[DW_OP_plus_uconst 0xd6]
	.dwattr $C$DW$1297, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1298	.dwtag  DW_TAG_member
	.dwattr $C$DW$1298, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1298, DW_AT_name("ADCB_EVT_INT")
	.dwattr $C$DW$1298, DW_AT_TI_symbol_name("_ADCB_EVT_INT")
	.dwattr $C$DW$1298, DW_AT_data_member_location[DW_OP_plus_uconst 0xd8]
	.dwattr $C$DW$1298, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1299	.dwtag  DW_TAG_member
	.dwattr $C$DW$1299, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1299, DW_AT_name("ADCB2_INT")
	.dwattr $C$DW$1299, DW_AT_TI_symbol_name("_ADCB2_INT")
	.dwattr $C$DW$1299, DW_AT_data_member_location[DW_OP_plus_uconst 0xda]
	.dwattr $C$DW$1299, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1300	.dwtag  DW_TAG_member
	.dwattr $C$DW$1300, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1300, DW_AT_name("ADCB3_INT")
	.dwattr $C$DW$1300, DW_AT_TI_symbol_name("_ADCB3_INT")
	.dwattr $C$DW$1300, DW_AT_data_member_location[DW_OP_plus_uconst 0xdc]
	.dwattr $C$DW$1300, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1301	.dwtag  DW_TAG_member
	.dwattr $C$DW$1301, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1301, DW_AT_name("ADCB4_INT")
	.dwattr $C$DW$1301, DW_AT_TI_symbol_name("_ADCB4_INT")
	.dwattr $C$DW$1301, DW_AT_data_member_location[DW_OP_plus_uconst 0xde]
	.dwattr $C$DW$1301, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1302	.dwtag  DW_TAG_member
	.dwattr $C$DW$1302, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1302, DW_AT_name("CLA1_1_INT")
	.dwattr $C$DW$1302, DW_AT_TI_symbol_name("_CLA1_1_INT")
	.dwattr $C$DW$1302, DW_AT_data_member_location[DW_OP_plus_uconst 0xe0]
	.dwattr $C$DW$1302, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1303	.dwtag  DW_TAG_member
	.dwattr $C$DW$1303, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1303, DW_AT_name("CLA1_2_INT")
	.dwattr $C$DW$1303, DW_AT_TI_symbol_name("_CLA1_2_INT")
	.dwattr $C$DW$1303, DW_AT_data_member_location[DW_OP_plus_uconst 0xe2]
	.dwattr $C$DW$1303, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1304	.dwtag  DW_TAG_member
	.dwattr $C$DW$1304, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1304, DW_AT_name("CLA1_3_INT")
	.dwattr $C$DW$1304, DW_AT_TI_symbol_name("_CLA1_3_INT")
	.dwattr $C$DW$1304, DW_AT_data_member_location[DW_OP_plus_uconst 0xe4]
	.dwattr $C$DW$1304, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1305	.dwtag  DW_TAG_member
	.dwattr $C$DW$1305, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1305, DW_AT_name("CLA1_4_INT")
	.dwattr $C$DW$1305, DW_AT_TI_symbol_name("_CLA1_4_INT")
	.dwattr $C$DW$1305, DW_AT_data_member_location[DW_OP_plus_uconst 0xe6]
	.dwattr $C$DW$1305, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1306	.dwtag  DW_TAG_member
	.dwattr $C$DW$1306, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1306, DW_AT_name("CLA1_5_INT")
	.dwattr $C$DW$1306, DW_AT_TI_symbol_name("_CLA1_5_INT")
	.dwattr $C$DW$1306, DW_AT_data_member_location[DW_OP_plus_uconst 0xe8]
	.dwattr $C$DW$1306, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1307	.dwtag  DW_TAG_member
	.dwattr $C$DW$1307, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1307, DW_AT_name("CLA1_6_INT")
	.dwattr $C$DW$1307, DW_AT_TI_symbol_name("_CLA1_6_INT")
	.dwattr $C$DW$1307, DW_AT_data_member_location[DW_OP_plus_uconst 0xea]
	.dwattr $C$DW$1307, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1308	.dwtag  DW_TAG_member
	.dwattr $C$DW$1308, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1308, DW_AT_name("CLA1_7_INT")
	.dwattr $C$DW$1308, DW_AT_TI_symbol_name("_CLA1_7_INT")
	.dwattr $C$DW$1308, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$1308, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1309	.dwtag  DW_TAG_member
	.dwattr $C$DW$1309, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1309, DW_AT_name("CLA1_8_INT")
	.dwattr $C$DW$1309, DW_AT_TI_symbol_name("_CLA1_8_INT")
	.dwattr $C$DW$1309, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$1309, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1310	.dwtag  DW_TAG_member
	.dwattr $C$DW$1310, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1310, DW_AT_name("XINT3_INT")
	.dwattr $C$DW$1310, DW_AT_TI_symbol_name("_XINT3_INT")
	.dwattr $C$DW$1310, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$1310, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1311	.dwtag  DW_TAG_member
	.dwattr $C$DW$1311, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1311, DW_AT_name("XINT4_INT")
	.dwattr $C$DW$1311, DW_AT_TI_symbol_name("_XINT4_INT")
	.dwattr $C$DW$1311, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$1311, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1312	.dwtag  DW_TAG_member
	.dwattr $C$DW$1312, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1312, DW_AT_name("XINT5_INT")
	.dwattr $C$DW$1312, DW_AT_TI_symbol_name("_XINT5_INT")
	.dwattr $C$DW$1312, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$1312, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1313	.dwtag  DW_TAG_member
	.dwattr $C$DW$1313, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1313, DW_AT_name("PIE23_RESERVED_INT")
	.dwattr $C$DW$1313, DW_AT_TI_symbol_name("_PIE23_RESERVED_INT")
	.dwattr $C$DW$1313, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$1313, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1314	.dwtag  DW_TAG_member
	.dwattr $C$DW$1314, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1314, DW_AT_name("PIE24_RESERVED_INT")
	.dwattr $C$DW$1314, DW_AT_TI_symbol_name("_PIE24_RESERVED_INT")
	.dwattr $C$DW$1314, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$1314, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1315	.dwtag  DW_TAG_member
	.dwattr $C$DW$1315, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1315, DW_AT_name("VCU_INT")
	.dwattr $C$DW$1315, DW_AT_TI_symbol_name("_VCU_INT")
	.dwattr $C$DW$1315, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$1315, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1316	.dwtag  DW_TAG_member
	.dwattr $C$DW$1316, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1316, DW_AT_name("FPU_OVERFLOW_INT")
	.dwattr $C$DW$1316, DW_AT_TI_symbol_name("_FPU_OVERFLOW_INT")
	.dwattr $C$DW$1316, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$1316, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1317	.dwtag  DW_TAG_member
	.dwattr $C$DW$1317, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1317, DW_AT_name("FPU_UNDERFLOW_INT")
	.dwattr $C$DW$1317, DW_AT_TI_symbol_name("_FPU_UNDERFLOW_INT")
	.dwattr $C$DW$1317, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$1317, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1318	.dwtag  DW_TAG_member
	.dwattr $C$DW$1318, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1318, DW_AT_name("PIE25_RESERVED_INT")
	.dwattr $C$DW$1318, DW_AT_TI_symbol_name("_PIE25_RESERVED_INT")
	.dwattr $C$DW$1318, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$1318, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1319	.dwtag  DW_TAG_member
	.dwattr $C$DW$1319, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1319, DW_AT_name("PIE26_RESERVED_INT")
	.dwattr $C$DW$1319, DW_AT_TI_symbol_name("_PIE26_RESERVED_INT")
	.dwattr $C$DW$1319, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$1319, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1320	.dwtag  DW_TAG_member
	.dwattr $C$DW$1320, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1320, DW_AT_name("PIE27_RESERVED_INT")
	.dwattr $C$DW$1320, DW_AT_TI_symbol_name("_PIE27_RESERVED_INT")
	.dwattr $C$DW$1320, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$1320, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1321	.dwtag  DW_TAG_member
	.dwattr $C$DW$1321, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1321, DW_AT_name("PIE28_RESERVED_INT")
	.dwattr $C$DW$1321, DW_AT_TI_symbol_name("_PIE28_RESERVED_INT")
	.dwattr $C$DW$1321, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$1321, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1322	.dwtag  DW_TAG_member
	.dwattr $C$DW$1322, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1322, DW_AT_name("IPC0_INT")
	.dwattr $C$DW$1322, DW_AT_TI_symbol_name("_IPC0_INT")
	.dwattr $C$DW$1322, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$1322, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1323	.dwtag  DW_TAG_member
	.dwattr $C$DW$1323, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1323, DW_AT_name("IPC1_INT")
	.dwattr $C$DW$1323, DW_AT_TI_symbol_name("_IPC1_INT")
	.dwattr $C$DW$1323, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$1323, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1324	.dwtag  DW_TAG_member
	.dwattr $C$DW$1324, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1324, DW_AT_name("IPC2_INT")
	.dwattr $C$DW$1324, DW_AT_TI_symbol_name("_IPC2_INT")
	.dwattr $C$DW$1324, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$1324, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1325	.dwtag  DW_TAG_member
	.dwattr $C$DW$1325, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1325, DW_AT_name("IPC3_INT")
	.dwattr $C$DW$1325, DW_AT_TI_symbol_name("_IPC3_INT")
	.dwattr $C$DW$1325, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$1325, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1326	.dwtag  DW_TAG_member
	.dwattr $C$DW$1326, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1326, DW_AT_name("EPWM9_TZ_INT")
	.dwattr $C$DW$1326, DW_AT_TI_symbol_name("_EPWM9_TZ_INT")
	.dwattr $C$DW$1326, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$1326, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1327	.dwtag  DW_TAG_member
	.dwattr $C$DW$1327, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1327, DW_AT_name("EPWM10_TZ_INT")
	.dwattr $C$DW$1327, DW_AT_TI_symbol_name("_EPWM10_TZ_INT")
	.dwattr $C$DW$1327, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$1327, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1328	.dwtag  DW_TAG_member
	.dwattr $C$DW$1328, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1328, DW_AT_name("EPWM11_TZ_INT")
	.dwattr $C$DW$1328, DW_AT_TI_symbol_name("_EPWM11_TZ_INT")
	.dwattr $C$DW$1328, DW_AT_data_member_location[DW_OP_plus_uconst 0x114]
	.dwattr $C$DW$1328, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1329	.dwtag  DW_TAG_member
	.dwattr $C$DW$1329, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1329, DW_AT_name("EPWM12_TZ_INT")
	.dwattr $C$DW$1329, DW_AT_TI_symbol_name("_EPWM12_TZ_INT")
	.dwattr $C$DW$1329, DW_AT_data_member_location[DW_OP_plus_uconst 0x116]
	.dwattr $C$DW$1329, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1330	.dwtag  DW_TAG_member
	.dwattr $C$DW$1330, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1330, DW_AT_name("PIE29_RESERVED_INT")
	.dwattr $C$DW$1330, DW_AT_TI_symbol_name("_PIE29_RESERVED_INT")
	.dwattr $C$DW$1330, DW_AT_data_member_location[DW_OP_plus_uconst 0x118]
	.dwattr $C$DW$1330, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1331	.dwtag  DW_TAG_member
	.dwattr $C$DW$1331, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1331, DW_AT_name("PIE30_RESERVED_INT")
	.dwattr $C$DW$1331, DW_AT_TI_symbol_name("_PIE30_RESERVED_INT")
	.dwattr $C$DW$1331, DW_AT_data_member_location[DW_OP_plus_uconst 0x11a]
	.dwattr $C$DW$1331, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1332	.dwtag  DW_TAG_member
	.dwattr $C$DW$1332, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1332, DW_AT_name("PIE31_RESERVED_INT")
	.dwattr $C$DW$1332, DW_AT_TI_symbol_name("_PIE31_RESERVED_INT")
	.dwattr $C$DW$1332, DW_AT_data_member_location[DW_OP_plus_uconst 0x11c]
	.dwattr $C$DW$1332, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1333	.dwtag  DW_TAG_member
	.dwattr $C$DW$1333, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1333, DW_AT_name("PIE32_RESERVED_INT")
	.dwattr $C$DW$1333, DW_AT_TI_symbol_name("_PIE32_RESERVED_INT")
	.dwattr $C$DW$1333, DW_AT_data_member_location[DW_OP_plus_uconst 0x11e]
	.dwattr $C$DW$1333, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1334	.dwtag  DW_TAG_member
	.dwattr $C$DW$1334, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1334, DW_AT_name("EPWM9_INT")
	.dwattr $C$DW$1334, DW_AT_TI_symbol_name("_EPWM9_INT")
	.dwattr $C$DW$1334, DW_AT_data_member_location[DW_OP_plus_uconst 0x120]
	.dwattr $C$DW$1334, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1335	.dwtag  DW_TAG_member
	.dwattr $C$DW$1335, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1335, DW_AT_name("EPWM10_INT")
	.dwattr $C$DW$1335, DW_AT_TI_symbol_name("_EPWM10_INT")
	.dwattr $C$DW$1335, DW_AT_data_member_location[DW_OP_plus_uconst 0x122]
	.dwattr $C$DW$1335, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1336	.dwtag  DW_TAG_member
	.dwattr $C$DW$1336, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1336, DW_AT_name("EPWM11_INT")
	.dwattr $C$DW$1336, DW_AT_TI_symbol_name("_EPWM11_INT")
	.dwattr $C$DW$1336, DW_AT_data_member_location[DW_OP_plus_uconst 0x124]
	.dwattr $C$DW$1336, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1337	.dwtag  DW_TAG_member
	.dwattr $C$DW$1337, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1337, DW_AT_name("EPWM12_INT")
	.dwattr $C$DW$1337, DW_AT_TI_symbol_name("_EPWM12_INT")
	.dwattr $C$DW$1337, DW_AT_data_member_location[DW_OP_plus_uconst 0x126]
	.dwattr $C$DW$1337, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1338	.dwtag  DW_TAG_member
	.dwattr $C$DW$1338, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1338, DW_AT_name("PIE33_RESERVED_INT")
	.dwattr $C$DW$1338, DW_AT_TI_symbol_name("_PIE33_RESERVED_INT")
	.dwattr $C$DW$1338, DW_AT_data_member_location[DW_OP_plus_uconst 0x128]
	.dwattr $C$DW$1338, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1339	.dwtag  DW_TAG_member
	.dwattr $C$DW$1339, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1339, DW_AT_name("PIE34_RESERVED_INT")
	.dwattr $C$DW$1339, DW_AT_TI_symbol_name("_PIE34_RESERVED_INT")
	.dwattr $C$DW$1339, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$1339, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1340	.dwtag  DW_TAG_member
	.dwattr $C$DW$1340, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1340, DW_AT_name("PIE35_RESERVED_INT")
	.dwattr $C$DW$1340, DW_AT_TI_symbol_name("_PIE35_RESERVED_INT")
	.dwattr $C$DW$1340, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$1340, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1341	.dwtag  DW_TAG_member
	.dwattr $C$DW$1341, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1341, DW_AT_name("PIE36_RESERVED_INT")
	.dwattr $C$DW$1341, DW_AT_TI_symbol_name("_PIE36_RESERVED_INT")
	.dwattr $C$DW$1341, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$1341, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1342	.dwtag  DW_TAG_member
	.dwattr $C$DW$1342, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1342, DW_AT_name("PIE37_RESERVED_INT")
	.dwattr $C$DW$1342, DW_AT_TI_symbol_name("_PIE37_RESERVED_INT")
	.dwattr $C$DW$1342, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$1342, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1343	.dwtag  DW_TAG_member
	.dwattr $C$DW$1343, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1343, DW_AT_name("PIE38_RESERVED_INT")
	.dwattr $C$DW$1343, DW_AT_TI_symbol_name("_PIE38_RESERVED_INT")
	.dwattr $C$DW$1343, DW_AT_data_member_location[DW_OP_plus_uconst 0x132]
	.dwattr $C$DW$1343, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1344	.dwtag  DW_TAG_member
	.dwattr $C$DW$1344, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1344, DW_AT_name("PIE39_RESERVED_INT")
	.dwattr $C$DW$1344, DW_AT_TI_symbol_name("_PIE39_RESERVED_INT")
	.dwattr $C$DW$1344, DW_AT_data_member_location[DW_OP_plus_uconst 0x134]
	.dwattr $C$DW$1344, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1345	.dwtag  DW_TAG_member
	.dwattr $C$DW$1345, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1345, DW_AT_name("PIE40_RESERVED_INT")
	.dwattr $C$DW$1345, DW_AT_TI_symbol_name("_PIE40_RESERVED_INT")
	.dwattr $C$DW$1345, DW_AT_data_member_location[DW_OP_plus_uconst 0x136]
	.dwattr $C$DW$1345, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1346	.dwtag  DW_TAG_member
	.dwattr $C$DW$1346, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1346, DW_AT_name("PIE41_RESERVED_INT")
	.dwattr $C$DW$1346, DW_AT_TI_symbol_name("_PIE41_RESERVED_INT")
	.dwattr $C$DW$1346, DW_AT_data_member_location[DW_OP_plus_uconst 0x138]
	.dwattr $C$DW$1346, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1347	.dwtag  DW_TAG_member
	.dwattr $C$DW$1347, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1347, DW_AT_name("PIE42_RESERVED_INT")
	.dwattr $C$DW$1347, DW_AT_TI_symbol_name("_PIE42_RESERVED_INT")
	.dwattr $C$DW$1347, DW_AT_data_member_location[DW_OP_plus_uconst 0x13a]
	.dwattr $C$DW$1347, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1348	.dwtag  DW_TAG_member
	.dwattr $C$DW$1348, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1348, DW_AT_name("PIE43_RESERVED_INT")
	.dwattr $C$DW$1348, DW_AT_TI_symbol_name("_PIE43_RESERVED_INT")
	.dwattr $C$DW$1348, DW_AT_data_member_location[DW_OP_plus_uconst 0x13c]
	.dwattr $C$DW$1348, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1349	.dwtag  DW_TAG_member
	.dwattr $C$DW$1349, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1349, DW_AT_name("PIE44_RESERVED_INT")
	.dwattr $C$DW$1349, DW_AT_TI_symbol_name("_PIE44_RESERVED_INT")
	.dwattr $C$DW$1349, DW_AT_data_member_location[DW_OP_plus_uconst 0x13e]
	.dwattr $C$DW$1349, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1350	.dwtag  DW_TAG_member
	.dwattr $C$DW$1350, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1350, DW_AT_name("SD1_INT")
	.dwattr $C$DW$1350, DW_AT_TI_symbol_name("_SD1_INT")
	.dwattr $C$DW$1350, DW_AT_data_member_location[DW_OP_plus_uconst 0x140]
	.dwattr $C$DW$1350, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1351	.dwtag  DW_TAG_member
	.dwattr $C$DW$1351, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1351, DW_AT_name("SD2_INT")
	.dwattr $C$DW$1351, DW_AT_TI_symbol_name("_SD2_INT")
	.dwattr $C$DW$1351, DW_AT_data_member_location[DW_OP_plus_uconst 0x142]
	.dwattr $C$DW$1351, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1352	.dwtag  DW_TAG_member
	.dwattr $C$DW$1352, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1352, DW_AT_name("PIE45_RESERVED_INT")
	.dwattr $C$DW$1352, DW_AT_TI_symbol_name("_PIE45_RESERVED_INT")
	.dwattr $C$DW$1352, DW_AT_data_member_location[DW_OP_plus_uconst 0x144]
	.dwattr $C$DW$1352, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1353	.dwtag  DW_TAG_member
	.dwattr $C$DW$1353, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1353, DW_AT_name("PIE46_RESERVED_INT")
	.dwattr $C$DW$1353, DW_AT_TI_symbol_name("_PIE46_RESERVED_INT")
	.dwattr $C$DW$1353, DW_AT_data_member_location[DW_OP_plus_uconst 0x146]
	.dwattr $C$DW$1353, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1354	.dwtag  DW_TAG_member
	.dwattr $C$DW$1354, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1354, DW_AT_name("PIE47_RESERVED_INT")
	.dwattr $C$DW$1354, DW_AT_TI_symbol_name("_PIE47_RESERVED_INT")
	.dwattr $C$DW$1354, DW_AT_data_member_location[DW_OP_plus_uconst 0x148]
	.dwattr $C$DW$1354, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1355	.dwtag  DW_TAG_member
	.dwattr $C$DW$1355, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1355, DW_AT_name("PIE48_RESERVED_INT")
	.dwattr $C$DW$1355, DW_AT_TI_symbol_name("_PIE48_RESERVED_INT")
	.dwattr $C$DW$1355, DW_AT_data_member_location[DW_OP_plus_uconst 0x14a]
	.dwattr $C$DW$1355, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1356	.dwtag  DW_TAG_member
	.dwattr $C$DW$1356, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1356, DW_AT_name("PIE49_RESERVED_INT")
	.dwattr $C$DW$1356, DW_AT_TI_symbol_name("_PIE49_RESERVED_INT")
	.dwattr $C$DW$1356, DW_AT_data_member_location[DW_OP_plus_uconst 0x14c]
	.dwattr $C$DW$1356, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1357	.dwtag  DW_TAG_member
	.dwattr $C$DW$1357, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1357, DW_AT_name("PIE50_RESERVED_INT")
	.dwattr $C$DW$1357, DW_AT_TI_symbol_name("_PIE50_RESERVED_INT")
	.dwattr $C$DW$1357, DW_AT_data_member_location[DW_OP_plus_uconst 0x14e]
	.dwattr $C$DW$1357, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1358	.dwtag  DW_TAG_member
	.dwattr $C$DW$1358, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1358, DW_AT_name("SPIC_RX_INT")
	.dwattr $C$DW$1358, DW_AT_TI_symbol_name("_SPIC_RX_INT")
	.dwattr $C$DW$1358, DW_AT_data_member_location[DW_OP_plus_uconst 0x150]
	.dwattr $C$DW$1358, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1359	.dwtag  DW_TAG_member
	.dwattr $C$DW$1359, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1359, DW_AT_name("SPIC_TX_INT")
	.dwattr $C$DW$1359, DW_AT_TI_symbol_name("_SPIC_TX_INT")
	.dwattr $C$DW$1359, DW_AT_data_member_location[DW_OP_plus_uconst 0x152]
	.dwattr $C$DW$1359, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1360	.dwtag  DW_TAG_member
	.dwattr $C$DW$1360, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1360, DW_AT_name("PIE51_RESERVED_INT")
	.dwattr $C$DW$1360, DW_AT_TI_symbol_name("_PIE51_RESERVED_INT")
	.dwattr $C$DW$1360, DW_AT_data_member_location[DW_OP_plus_uconst 0x154]
	.dwattr $C$DW$1360, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1361	.dwtag  DW_TAG_member
	.dwattr $C$DW$1361, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1361, DW_AT_name("PIE52_RESERVED_INT")
	.dwattr $C$DW$1361, DW_AT_TI_symbol_name("_PIE52_RESERVED_INT")
	.dwattr $C$DW$1361, DW_AT_data_member_location[DW_OP_plus_uconst 0x156]
	.dwattr $C$DW$1361, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1362	.dwtag  DW_TAG_member
	.dwattr $C$DW$1362, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1362, DW_AT_name("PIE53_RESERVED_INT")
	.dwattr $C$DW$1362, DW_AT_TI_symbol_name("_PIE53_RESERVED_INT")
	.dwattr $C$DW$1362, DW_AT_data_member_location[DW_OP_plus_uconst 0x158]
	.dwattr $C$DW$1362, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1363	.dwtag  DW_TAG_member
	.dwattr $C$DW$1363, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1363, DW_AT_name("PIE54_RESERVED_INT")
	.dwattr $C$DW$1363, DW_AT_TI_symbol_name("_PIE54_RESERVED_INT")
	.dwattr $C$DW$1363, DW_AT_data_member_location[DW_OP_plus_uconst 0x15a]
	.dwattr $C$DW$1363, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1364	.dwtag  DW_TAG_member
	.dwattr $C$DW$1364, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1364, DW_AT_name("PIE55_RESERVED_INT")
	.dwattr $C$DW$1364, DW_AT_TI_symbol_name("_PIE55_RESERVED_INT")
	.dwattr $C$DW$1364, DW_AT_data_member_location[DW_OP_plus_uconst 0x15c]
	.dwattr $C$DW$1364, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1365	.dwtag  DW_TAG_member
	.dwattr $C$DW$1365, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1365, DW_AT_name("PIE56_RESERVED_INT")
	.dwattr $C$DW$1365, DW_AT_TI_symbol_name("_PIE56_RESERVED_INT")
	.dwattr $C$DW$1365, DW_AT_data_member_location[DW_OP_plus_uconst 0x15e]
	.dwattr $C$DW$1365, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1366	.dwtag  DW_TAG_member
	.dwattr $C$DW$1366, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1366, DW_AT_name("PIE57_RESERVED_INT")
	.dwattr $C$DW$1366, DW_AT_TI_symbol_name("_PIE57_RESERVED_INT")
	.dwattr $C$DW$1366, DW_AT_data_member_location[DW_OP_plus_uconst 0x160]
	.dwattr $C$DW$1366, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1367	.dwtag  DW_TAG_member
	.dwattr $C$DW$1367, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1367, DW_AT_name("PIE58_RESERVED_INT")
	.dwattr $C$DW$1367, DW_AT_TI_symbol_name("_PIE58_RESERVED_INT")
	.dwattr $C$DW$1367, DW_AT_data_member_location[DW_OP_plus_uconst 0x162]
	.dwattr $C$DW$1367, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1368	.dwtag  DW_TAG_member
	.dwattr $C$DW$1368, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1368, DW_AT_name("PIE59_RESERVED_INT")
	.dwattr $C$DW$1368, DW_AT_TI_symbol_name("_PIE59_RESERVED_INT")
	.dwattr $C$DW$1368, DW_AT_data_member_location[DW_OP_plus_uconst 0x164]
	.dwattr $C$DW$1368, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1369	.dwtag  DW_TAG_member
	.dwattr $C$DW$1369, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1369, DW_AT_name("PIE60_RESERVED_INT")
	.dwattr $C$DW$1369, DW_AT_TI_symbol_name("_PIE60_RESERVED_INT")
	.dwattr $C$DW$1369, DW_AT_data_member_location[DW_OP_plus_uconst 0x166]
	.dwattr $C$DW$1369, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1370	.dwtag  DW_TAG_member
	.dwattr $C$DW$1370, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1370, DW_AT_name("PIE61_RESERVED_INT")
	.dwattr $C$DW$1370, DW_AT_TI_symbol_name("_PIE61_RESERVED_INT")
	.dwattr $C$DW$1370, DW_AT_data_member_location[DW_OP_plus_uconst 0x168]
	.dwattr $C$DW$1370, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1371	.dwtag  DW_TAG_member
	.dwattr $C$DW$1371, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1371, DW_AT_name("PIE62_RESERVED_INT")
	.dwattr $C$DW$1371, DW_AT_TI_symbol_name("_PIE62_RESERVED_INT")
	.dwattr $C$DW$1371, DW_AT_data_member_location[DW_OP_plus_uconst 0x16a]
	.dwattr $C$DW$1371, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1372	.dwtag  DW_TAG_member
	.dwattr $C$DW$1372, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1372, DW_AT_name("PIE63_RESERVED_INT")
	.dwattr $C$DW$1372, DW_AT_TI_symbol_name("_PIE63_RESERVED_INT")
	.dwattr $C$DW$1372, DW_AT_data_member_location[DW_OP_plus_uconst 0x16c]
	.dwattr $C$DW$1372, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1373	.dwtag  DW_TAG_member
	.dwattr $C$DW$1373, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1373, DW_AT_name("PIE64_RESERVED_INT")
	.dwattr $C$DW$1373, DW_AT_TI_symbol_name("_PIE64_RESERVED_INT")
	.dwattr $C$DW$1373, DW_AT_data_member_location[DW_OP_plus_uconst 0x16e]
	.dwattr $C$DW$1373, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1374	.dwtag  DW_TAG_member
	.dwattr $C$DW$1374, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1374, DW_AT_name("PIE65_RESERVED_INT")
	.dwattr $C$DW$1374, DW_AT_TI_symbol_name("_PIE65_RESERVED_INT")
	.dwattr $C$DW$1374, DW_AT_data_member_location[DW_OP_plus_uconst 0x170]
	.dwattr $C$DW$1374, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1375	.dwtag  DW_TAG_member
	.dwattr $C$DW$1375, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1375, DW_AT_name("PIE66_RESERVED_INT")
	.dwattr $C$DW$1375, DW_AT_TI_symbol_name("_PIE66_RESERVED_INT")
	.dwattr $C$DW$1375, DW_AT_data_member_location[DW_OP_plus_uconst 0x172]
	.dwattr $C$DW$1375, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1376	.dwtag  DW_TAG_member
	.dwattr $C$DW$1376, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1376, DW_AT_name("PIE67_RESERVED_INT")
	.dwattr $C$DW$1376, DW_AT_TI_symbol_name("_PIE67_RESERVED_INT")
	.dwattr $C$DW$1376, DW_AT_data_member_location[DW_OP_plus_uconst 0x174]
	.dwattr $C$DW$1376, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1377	.dwtag  DW_TAG_member
	.dwattr $C$DW$1377, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1377, DW_AT_name("PIE68_RESERVED_INT")
	.dwattr $C$DW$1377, DW_AT_TI_symbol_name("_PIE68_RESERVED_INT")
	.dwattr $C$DW$1377, DW_AT_data_member_location[DW_OP_plus_uconst 0x176]
	.dwattr $C$DW$1377, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1378	.dwtag  DW_TAG_member
	.dwattr $C$DW$1378, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1378, DW_AT_name("PIE69_RESERVED_INT")
	.dwattr $C$DW$1378, DW_AT_TI_symbol_name("_PIE69_RESERVED_INT")
	.dwattr $C$DW$1378, DW_AT_data_member_location[DW_OP_plus_uconst 0x178]
	.dwattr $C$DW$1378, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1379	.dwtag  DW_TAG_member
	.dwattr $C$DW$1379, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1379, DW_AT_name("PIE70_RESERVED_INT")
	.dwattr $C$DW$1379, DW_AT_TI_symbol_name("_PIE70_RESERVED_INT")
	.dwattr $C$DW$1379, DW_AT_data_member_location[DW_OP_plus_uconst 0x17a]
	.dwattr $C$DW$1379, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1380	.dwtag  DW_TAG_member
	.dwattr $C$DW$1380, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1380, DW_AT_name("UPPA_INT")
	.dwattr $C$DW$1380, DW_AT_TI_symbol_name("_UPPA_INT")
	.dwattr $C$DW$1380, DW_AT_data_member_location[DW_OP_plus_uconst 0x17c]
	.dwattr $C$DW$1380, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1381	.dwtag  DW_TAG_member
	.dwattr $C$DW$1381, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1381, DW_AT_name("PIE71_RESERVED_INT")
	.dwattr $C$DW$1381, DW_AT_TI_symbol_name("_PIE71_RESERVED_INT")
	.dwattr $C$DW$1381, DW_AT_data_member_location[DW_OP_plus_uconst 0x17e]
	.dwattr $C$DW$1381, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1382	.dwtag  DW_TAG_member
	.dwattr $C$DW$1382, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1382, DW_AT_name("PIE72_RESERVED_INT")
	.dwattr $C$DW$1382, DW_AT_TI_symbol_name("_PIE72_RESERVED_INT")
	.dwattr $C$DW$1382, DW_AT_data_member_location[DW_OP_plus_uconst 0x180]
	.dwattr $C$DW$1382, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1383	.dwtag  DW_TAG_member
	.dwattr $C$DW$1383, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1383, DW_AT_name("PIE73_RESERVED_INT")
	.dwattr $C$DW$1383, DW_AT_TI_symbol_name("_PIE73_RESERVED_INT")
	.dwattr $C$DW$1383, DW_AT_data_member_location[DW_OP_plus_uconst 0x182]
	.dwattr $C$DW$1383, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1384	.dwtag  DW_TAG_member
	.dwattr $C$DW$1384, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1384, DW_AT_name("PIE74_RESERVED_INT")
	.dwattr $C$DW$1384, DW_AT_TI_symbol_name("_PIE74_RESERVED_INT")
	.dwattr $C$DW$1384, DW_AT_data_member_location[DW_OP_plus_uconst 0x184]
	.dwattr $C$DW$1384, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1385	.dwtag  DW_TAG_member
	.dwattr $C$DW$1385, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1385, DW_AT_name("PIE75_RESERVED_INT")
	.dwattr $C$DW$1385, DW_AT_TI_symbol_name("_PIE75_RESERVED_INT")
	.dwattr $C$DW$1385, DW_AT_data_member_location[DW_OP_plus_uconst 0x186]
	.dwattr $C$DW$1385, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1386	.dwtag  DW_TAG_member
	.dwattr $C$DW$1386, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1386, DW_AT_name("PIE76_RESERVED_INT")
	.dwattr $C$DW$1386, DW_AT_TI_symbol_name("_PIE76_RESERVED_INT")
	.dwattr $C$DW$1386, DW_AT_data_member_location[DW_OP_plus_uconst 0x188]
	.dwattr $C$DW$1386, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1387	.dwtag  DW_TAG_member
	.dwattr $C$DW$1387, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1387, DW_AT_name("PIE77_RESERVED_INT")
	.dwattr $C$DW$1387, DW_AT_TI_symbol_name("_PIE77_RESERVED_INT")
	.dwattr $C$DW$1387, DW_AT_data_member_location[DW_OP_plus_uconst 0x18a]
	.dwattr $C$DW$1387, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1388	.dwtag  DW_TAG_member
	.dwattr $C$DW$1388, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1388, DW_AT_name("USBA_INT")
	.dwattr $C$DW$1388, DW_AT_TI_symbol_name("_USBA_INT")
	.dwattr $C$DW$1388, DW_AT_data_member_location[DW_OP_plus_uconst 0x18c]
	.dwattr $C$DW$1388, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1389	.dwtag  DW_TAG_member
	.dwattr $C$DW$1389, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1389, DW_AT_name("PIE78_RESERVED_INT")
	.dwattr $C$DW$1389, DW_AT_TI_symbol_name("_PIE78_RESERVED_INT")
	.dwattr $C$DW$1389, DW_AT_data_member_location[DW_OP_plus_uconst 0x18e]
	.dwattr $C$DW$1389, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1390	.dwtag  DW_TAG_member
	.dwattr $C$DW$1390, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1390, DW_AT_name("ADCC_EVT_INT")
	.dwattr $C$DW$1390, DW_AT_TI_symbol_name("_ADCC_EVT_INT")
	.dwattr $C$DW$1390, DW_AT_data_member_location[DW_OP_plus_uconst 0x190]
	.dwattr $C$DW$1390, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1391	.dwtag  DW_TAG_member
	.dwattr $C$DW$1391, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1391, DW_AT_name("ADCC2_INT")
	.dwattr $C$DW$1391, DW_AT_TI_symbol_name("_ADCC2_INT")
	.dwattr $C$DW$1391, DW_AT_data_member_location[DW_OP_plus_uconst 0x192]
	.dwattr $C$DW$1391, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1392	.dwtag  DW_TAG_member
	.dwattr $C$DW$1392, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1392, DW_AT_name("ADCC3_INT")
	.dwattr $C$DW$1392, DW_AT_TI_symbol_name("_ADCC3_INT")
	.dwattr $C$DW$1392, DW_AT_data_member_location[DW_OP_plus_uconst 0x194]
	.dwattr $C$DW$1392, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1393	.dwtag  DW_TAG_member
	.dwattr $C$DW$1393, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1393, DW_AT_name("ADCC4_INT")
	.dwattr $C$DW$1393, DW_AT_TI_symbol_name("_ADCC4_INT")
	.dwattr $C$DW$1393, DW_AT_data_member_location[DW_OP_plus_uconst 0x196]
	.dwattr $C$DW$1393, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1394	.dwtag  DW_TAG_member
	.dwattr $C$DW$1394, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1394, DW_AT_name("ADCD_EVT_INT")
	.dwattr $C$DW$1394, DW_AT_TI_symbol_name("_ADCD_EVT_INT")
	.dwattr $C$DW$1394, DW_AT_data_member_location[DW_OP_plus_uconst 0x198]
	.dwattr $C$DW$1394, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1395	.dwtag  DW_TAG_member
	.dwattr $C$DW$1395, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1395, DW_AT_name("ADCD2_INT")
	.dwattr $C$DW$1395, DW_AT_TI_symbol_name("_ADCD2_INT")
	.dwattr $C$DW$1395, DW_AT_data_member_location[DW_OP_plus_uconst 0x19a]
	.dwattr $C$DW$1395, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1396	.dwtag  DW_TAG_member
	.dwattr $C$DW$1396, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1396, DW_AT_name("ADCD3_INT")
	.dwattr $C$DW$1396, DW_AT_TI_symbol_name("_ADCD3_INT")
	.dwattr $C$DW$1396, DW_AT_data_member_location[DW_OP_plus_uconst 0x19c]
	.dwattr $C$DW$1396, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1397	.dwtag  DW_TAG_member
	.dwattr $C$DW$1397, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1397, DW_AT_name("ADCD4_INT")
	.dwattr $C$DW$1397, DW_AT_TI_symbol_name("_ADCD4_INT")
	.dwattr $C$DW$1397, DW_AT_data_member_location[DW_OP_plus_uconst 0x19e]
	.dwattr $C$DW$1397, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1398	.dwtag  DW_TAG_member
	.dwattr $C$DW$1398, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1398, DW_AT_name("PIE79_RESERVED_INT")
	.dwattr $C$DW$1398, DW_AT_TI_symbol_name("_PIE79_RESERVED_INT")
	.dwattr $C$DW$1398, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a0]
	.dwattr $C$DW$1398, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1399	.dwtag  DW_TAG_member
	.dwattr $C$DW$1399, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1399, DW_AT_name("PIE80_RESERVED_INT")
	.dwattr $C$DW$1399, DW_AT_TI_symbol_name("_PIE80_RESERVED_INT")
	.dwattr $C$DW$1399, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a2]
	.dwattr $C$DW$1399, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1400	.dwtag  DW_TAG_member
	.dwattr $C$DW$1400, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1400, DW_AT_name("PIE81_RESERVED_INT")
	.dwattr $C$DW$1400, DW_AT_TI_symbol_name("_PIE81_RESERVED_INT")
	.dwattr $C$DW$1400, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a4]
	.dwattr $C$DW$1400, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1401	.dwtag  DW_TAG_member
	.dwattr $C$DW$1401, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1401, DW_AT_name("PIE82_RESERVED_INT")
	.dwattr $C$DW$1401, DW_AT_TI_symbol_name("_PIE82_RESERVED_INT")
	.dwattr $C$DW$1401, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a6]
	.dwattr $C$DW$1401, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1402	.dwtag  DW_TAG_member
	.dwattr $C$DW$1402, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1402, DW_AT_name("PIE83_RESERVED_INT")
	.dwattr $C$DW$1402, DW_AT_TI_symbol_name("_PIE83_RESERVED_INT")
	.dwattr $C$DW$1402, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a8]
	.dwattr $C$DW$1402, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1403	.dwtag  DW_TAG_member
	.dwattr $C$DW$1403, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1403, DW_AT_name("PIE84_RESERVED_INT")
	.dwattr $C$DW$1403, DW_AT_TI_symbol_name("_PIE84_RESERVED_INT")
	.dwattr $C$DW$1403, DW_AT_data_member_location[DW_OP_plus_uconst 0x1aa]
	.dwattr $C$DW$1403, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1404	.dwtag  DW_TAG_member
	.dwattr $C$DW$1404, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1404, DW_AT_name("PIE85_RESERVED_INT")
	.dwattr $C$DW$1404, DW_AT_TI_symbol_name("_PIE85_RESERVED_INT")
	.dwattr $C$DW$1404, DW_AT_data_member_location[DW_OP_plus_uconst 0x1ac]
	.dwattr $C$DW$1404, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1405	.dwtag  DW_TAG_member
	.dwattr $C$DW$1405, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1405, DW_AT_name("PIE86_RESERVED_INT")
	.dwattr $C$DW$1405, DW_AT_TI_symbol_name("_PIE86_RESERVED_INT")
	.dwattr $C$DW$1405, DW_AT_data_member_location[DW_OP_plus_uconst 0x1ae]
	.dwattr $C$DW$1405, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1406	.dwtag  DW_TAG_member
	.dwattr $C$DW$1406, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1406, DW_AT_name("EMIF_ERROR_INT")
	.dwattr $C$DW$1406, DW_AT_TI_symbol_name("_EMIF_ERROR_INT")
	.dwattr $C$DW$1406, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b0]
	.dwattr $C$DW$1406, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1407	.dwtag  DW_TAG_member
	.dwattr $C$DW$1407, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1407, DW_AT_name("RAM_CORRECTABLE_ERROR_INT")
	.dwattr $C$DW$1407, DW_AT_TI_symbol_name("_RAM_CORRECTABLE_ERROR_INT")
	.dwattr $C$DW$1407, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b2]
	.dwattr $C$DW$1407, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1408	.dwtag  DW_TAG_member
	.dwattr $C$DW$1408, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1408, DW_AT_name("FLASH_CORRECTABLE_ERROR_INT")
	.dwattr $C$DW$1408, DW_AT_TI_symbol_name("_FLASH_CORRECTABLE_ERROR_INT")
	.dwattr $C$DW$1408, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b4]
	.dwattr $C$DW$1408, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1409	.dwtag  DW_TAG_member
	.dwattr $C$DW$1409, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1409, DW_AT_name("RAM_ACCESS_VIOLATION_INT")
	.dwattr $C$DW$1409, DW_AT_TI_symbol_name("_RAM_ACCESS_VIOLATION_INT")
	.dwattr $C$DW$1409, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b6]
	.dwattr $C$DW$1409, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1410	.dwtag  DW_TAG_member
	.dwattr $C$DW$1410, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1410, DW_AT_name("SYS_PLL_SLIP_INT")
	.dwattr $C$DW$1410, DW_AT_TI_symbol_name("_SYS_PLL_SLIP_INT")
	.dwattr $C$DW$1410, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b8]
	.dwattr $C$DW$1410, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1411	.dwtag  DW_TAG_member
	.dwattr $C$DW$1411, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1411, DW_AT_name("AUX_PLL_SLIP_INT")
	.dwattr $C$DW$1411, DW_AT_TI_symbol_name("_AUX_PLL_SLIP_INT")
	.dwattr $C$DW$1411, DW_AT_data_member_location[DW_OP_plus_uconst 0x1ba]
	.dwattr $C$DW$1411, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1412	.dwtag  DW_TAG_member
	.dwattr $C$DW$1412, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1412, DW_AT_name("CLA_OVERFLOW_INT")
	.dwattr $C$DW$1412, DW_AT_TI_symbol_name("_CLA_OVERFLOW_INT")
	.dwattr $C$DW$1412, DW_AT_data_member_location[DW_OP_plus_uconst 0x1bc]
	.dwattr $C$DW$1412, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$1413	.dwtag  DW_TAG_member
	.dwattr $C$DW$1413, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1413, DW_AT_name("CLA_UNDERFLOW_INT")
	.dwattr $C$DW$1413, DW_AT_TI_symbol_name("_CLA_UNDERFLOW_INT")
	.dwattr $C$DW$1413, DW_AT_data_member_location[DW_OP_plus_uconst 0x1be]
	.dwattr $C$DW$1413, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$159

$C$DW$1414	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1414, DW_AT_type(*$C$DW$T$159)

$C$DW$T$176	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$176, DW_AT_type(*$C$DW$1414)

$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")


$C$DW$T$156	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$156, DW_AT_language(DW_LANG_C)
	.dwendtag $C$DW$T$156

$C$DW$T$157	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$T$157, DW_AT_address_class(0x20)

$C$DW$T$158	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$158, DW_AT_name("PINT")
	.dwattr $C$DW$T$158, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$T$158, DW_AT_language(DW_LANG_C)

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("Uint16")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)


$C$DW$T$72	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$72, DW_AT_byte_size(0x02)
$C$DW$1415	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1415, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$72


$C$DW$T$92	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$92, DW_AT_byte_size(0x04)
$C$DW$1416	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1416, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$92


$C$DW$T$93	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$93, DW_AT_byte_size(0x06)
$C$DW$1417	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1417, DW_AT_upper_bound(0x05)

	.dwendtag $C$DW$T$93


$C$DW$T$94	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$94, DW_AT_byte_size(0x0a)
$C$DW$1418	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1418, DW_AT_upper_bound(0x09)

	.dwendtag $C$DW$T$94


$C$DW$T$95	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$95, DW_AT_byte_size(0x1a)
$C$DW$1419	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1419, DW_AT_upper_bound(0x19)

	.dwendtag $C$DW$T$95

$C$DW$T$23	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$23, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)

$C$DW$T$24	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_address_class(0x20)

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$28	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$28, DW_AT_name("Uint32")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$19, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)


$C$DW$T$70	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$70, DW_AT_byte_size(0x14)
$C$DW$1420	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1420, DW_AT_upper_bound(0x09)

	.dwendtag $C$DW$T$70


$C$DW$T$200	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$200, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$200, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$200, DW_AT_byte_size(0x10)
$C$DW$1421	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1421, DW_AT_upper_bound(0x07)

	.dwendtag $C$DW$T$200


$C$DW$T$201	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$201, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$201, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$201, DW_AT_byte_size(0xa0)
$C$DW$1422	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1422, DW_AT_upper_bound(0x09)

$C$DW$1423	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1423, DW_AT_upper_bound(0x07)

	.dwendtag $C$DW$T$201

$C$DW$T$203	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$203, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$203, DW_AT_address_class(0x20)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$1424	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1424, DW_AT_name("AL")
	.dwattr $C$DW$1424, DW_AT_location[DW_OP_reg0]

$C$DW$1425	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1425, DW_AT_name("AH")
	.dwattr $C$DW$1425, DW_AT_location[DW_OP_reg1]

$C$DW$1426	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1426, DW_AT_name("PL")
	.dwattr $C$DW$1426, DW_AT_location[DW_OP_reg2]

$C$DW$1427	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1427, DW_AT_name("PH")
	.dwattr $C$DW$1427, DW_AT_location[DW_OP_reg3]

$C$DW$1428	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1428, DW_AT_name("SP")
	.dwattr $C$DW$1428, DW_AT_location[DW_OP_reg20]

$C$DW$1429	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1429, DW_AT_name("XT")
	.dwattr $C$DW$1429, DW_AT_location[DW_OP_reg21]

$C$DW$1430	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1430, DW_AT_name("T")
	.dwattr $C$DW$1430, DW_AT_location[DW_OP_reg22]

$C$DW$1431	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1431, DW_AT_name("ST0")
	.dwattr $C$DW$1431, DW_AT_location[DW_OP_reg23]

$C$DW$1432	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1432, DW_AT_name("ST1")
	.dwattr $C$DW$1432, DW_AT_location[DW_OP_reg24]

$C$DW$1433	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1433, DW_AT_name("PC")
	.dwattr $C$DW$1433, DW_AT_location[DW_OP_reg25]

$C$DW$1434	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1434, DW_AT_name("RPC")
	.dwattr $C$DW$1434, DW_AT_location[DW_OP_reg26]

$C$DW$1435	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1435, DW_AT_name("FP")
	.dwattr $C$DW$1435, DW_AT_location[DW_OP_reg28]

$C$DW$1436	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1436, DW_AT_name("DP")
	.dwattr $C$DW$1436, DW_AT_location[DW_OP_reg29]

$C$DW$1437	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1437, DW_AT_name("SXM")
	.dwattr $C$DW$1437, DW_AT_location[DW_OP_reg30]

$C$DW$1438	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1438, DW_AT_name("PM")
	.dwattr $C$DW$1438, DW_AT_location[DW_OP_reg31]

$C$DW$1439	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1439, DW_AT_name("OVM")
	.dwattr $C$DW$1439, DW_AT_location[DW_OP_regx 0x20]

$C$DW$1440	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1440, DW_AT_name("PAGE0")
	.dwattr $C$DW$1440, DW_AT_location[DW_OP_regx 0x21]

$C$DW$1441	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1441, DW_AT_name("AMODE")
	.dwattr $C$DW$1441, DW_AT_location[DW_OP_regx 0x22]

$C$DW$1442	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1442, DW_AT_name("INTM")
	.dwattr $C$DW$1442, DW_AT_location[DW_OP_regx 0x23]

$C$DW$1443	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1443, DW_AT_name("IFR")
	.dwattr $C$DW$1443, DW_AT_location[DW_OP_regx 0x24]

$C$DW$1444	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1444, DW_AT_name("IER")
	.dwattr $C$DW$1444, DW_AT_location[DW_OP_regx 0x25]

$C$DW$1445	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1445, DW_AT_name("V")
	.dwattr $C$DW$1445, DW_AT_location[DW_OP_regx 0x26]

$C$DW$1446	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1446, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$1446, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$1447	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1447, DW_AT_name("VOL")
	.dwattr $C$DW$1447, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$1448	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1448, DW_AT_name("AR0")
	.dwattr $C$DW$1448, DW_AT_location[DW_OP_reg4]

$C$DW$1449	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1449, DW_AT_name("XAR0")
	.dwattr $C$DW$1449, DW_AT_location[DW_OP_reg5]

$C$DW$1450	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1450, DW_AT_name("AR1")
	.dwattr $C$DW$1450, DW_AT_location[DW_OP_reg6]

$C$DW$1451	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1451, DW_AT_name("XAR1")
	.dwattr $C$DW$1451, DW_AT_location[DW_OP_reg7]

$C$DW$1452	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1452, DW_AT_name("AR2")
	.dwattr $C$DW$1452, DW_AT_location[DW_OP_reg8]

$C$DW$1453	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1453, DW_AT_name("XAR2")
	.dwattr $C$DW$1453, DW_AT_location[DW_OP_reg9]

$C$DW$1454	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1454, DW_AT_name("AR3")
	.dwattr $C$DW$1454, DW_AT_location[DW_OP_reg10]

$C$DW$1455	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1455, DW_AT_name("XAR3")
	.dwattr $C$DW$1455, DW_AT_location[DW_OP_reg11]

$C$DW$1456	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1456, DW_AT_name("AR4")
	.dwattr $C$DW$1456, DW_AT_location[DW_OP_reg12]

$C$DW$1457	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1457, DW_AT_name("XAR4")
	.dwattr $C$DW$1457, DW_AT_location[DW_OP_reg13]

$C$DW$1458	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1458, DW_AT_name("AR5")
	.dwattr $C$DW$1458, DW_AT_location[DW_OP_reg14]

$C$DW$1459	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1459, DW_AT_name("XAR5")
	.dwattr $C$DW$1459, DW_AT_location[DW_OP_reg15]

$C$DW$1460	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1460, DW_AT_name("AR6")
	.dwattr $C$DW$1460, DW_AT_location[DW_OP_reg16]

$C$DW$1461	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1461, DW_AT_name("XAR6")
	.dwattr $C$DW$1461, DW_AT_location[DW_OP_reg17]

$C$DW$1462	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1462, DW_AT_name("AR7")
	.dwattr $C$DW$1462, DW_AT_location[DW_OP_reg18]

$C$DW$1463	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1463, DW_AT_name("XAR7")
	.dwattr $C$DW$1463, DW_AT_location[DW_OP_reg19]

$C$DW$1464	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1464, DW_AT_name("R0HL")
	.dwattr $C$DW$1464, DW_AT_location[DW_OP_regx 0x29]

$C$DW$1465	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1465, DW_AT_name("R0H")
	.dwattr $C$DW$1465, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$1466	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1466, DW_AT_name("R1HL")
	.dwattr $C$DW$1466, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$1467	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1467, DW_AT_name("R1H")
	.dwattr $C$DW$1467, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$1468	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1468, DW_AT_name("R2HL")
	.dwattr $C$DW$1468, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$1469	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1469, DW_AT_name("R2H")
	.dwattr $C$DW$1469, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$1470	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1470, DW_AT_name("R3HL")
	.dwattr $C$DW$1470, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$1471	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1471, DW_AT_name("R3H")
	.dwattr $C$DW$1471, DW_AT_location[DW_OP_regx 0x30]

$C$DW$1472	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1472, DW_AT_name("R4HL")
	.dwattr $C$DW$1472, DW_AT_location[DW_OP_regx 0x31]

$C$DW$1473	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1473, DW_AT_name("R4H")
	.dwattr $C$DW$1473, DW_AT_location[DW_OP_regx 0x32]

$C$DW$1474	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1474, DW_AT_name("R5HL")
	.dwattr $C$DW$1474, DW_AT_location[DW_OP_regx 0x33]

$C$DW$1475	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1475, DW_AT_name("R5H")
	.dwattr $C$DW$1475, DW_AT_location[DW_OP_regx 0x34]

$C$DW$1476	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1476, DW_AT_name("R6HL")
	.dwattr $C$DW$1476, DW_AT_location[DW_OP_regx 0x35]

$C$DW$1477	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1477, DW_AT_name("R6H")
	.dwattr $C$DW$1477, DW_AT_location[DW_OP_regx 0x36]

$C$DW$1478	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1478, DW_AT_name("R7HL")
	.dwattr $C$DW$1478, DW_AT_location[DW_OP_regx 0x37]

$C$DW$1479	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1479, DW_AT_name("R7H")
	.dwattr $C$DW$1479, DW_AT_location[DW_OP_regx 0x38]

$C$DW$1480	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1480, DW_AT_name("RBL")
	.dwattr $C$DW$1480, DW_AT_location[DW_OP_regx 0x49]

$C$DW$1481	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1481, DW_AT_name("RB")
	.dwattr $C$DW$1481, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$1482	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1482, DW_AT_name("STFL")
	.dwattr $C$DW$1482, DW_AT_location[DW_OP_regx 0x27]

$C$DW$1483	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1483, DW_AT_name("STF")
	.dwattr $C$DW$1483, DW_AT_location[DW_OP_regx 0x28]

$C$DW$1484	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$1484, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$1484, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


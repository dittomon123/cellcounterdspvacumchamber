;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Fri Jan 29 15:14:37 2021                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../source/main.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cell Counter\V1.3.1_And_V1.3.0\alphaCellCounterCPU1\CPU1_FLASH")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_unSysHealthMonCount+0,32
	.bits	0,16			; _g_unSysHealthMonCount @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_untimerFor20ms+0,32
	.bits	0,16			; _g_untimerFor20ms @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_unSPIErrorCheckCounter+0,32
	.bits	1,16			; _g_unSPIErrorCheckCounter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_g_unSequenceStateTime+0,32
	.bits	0,32			; _g_unSequenceStateTime @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_g_unSequenceStateTime1+0,32
	.bits	0,32			; _g_unSequenceStateTime1 @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("SysInitGetSystemStatus")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_SysInitGetSystemStatus")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
	.dwendtag $C$DW$1


$C$DW$2	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$2, DW_AT_name("IGConfigHighVoltageModule")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_IGConfigHighVoltageModule")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$11)

$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$11)

	.dwendtag $C$DW$2


$C$DW$5	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$5, DW_AT_name("SysRT_OneStep")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_SysRT_OneStep")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
	.dwendtag $C$DW$5


$C$DW$6	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$6, DW_AT_name("SysSystem_Initalize")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_SysSystem_Initalize")
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
	.dwendtag $C$DW$6


$C$DW$7	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$7, DW_AT_name("F28x_usDelay")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_F28x_usDelay")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$12)

	.dwendtag $C$DW$7


$C$DW$9	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$9, DW_AT_name("ServiceDog")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_ServiceDog")
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
	.dwendtag $C$DW$9


$C$DW$10	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$10, DW_AT_name("BloodCellCounter_initialize")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_BloodCellCounter_initialize")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
	.dwendtag $C$DW$10


$C$DW$11	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$11, DW_AT_name("Enable_WatchdogTimer")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_Enable_WatchdogTimer")
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
	.dwendtag $C$DW$11


$C$DW$12	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$12, DW_AT_name("ADAdcAcquireHealthStatus")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ADAdcAcquireHealthStatus")
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
	.dwendtag $C$DW$12

	.global	_g_unSysHealthMonCount
_g_unSysHealthMonCount:	.usect	".ebss",1,1,0
$C$DW$13	.dwtag  DW_TAG_variable
	.dwattr $C$DW$13, DW_AT_name("g_unSysHealthMonCount")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_g_unSysHealthMonCount")
	.dwattr $C$DW$13, DW_AT_location[DW_OP_addr _g_unSysHealthMonCount]
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$13, DW_AT_external

	.global	_g_untimerFor20ms
_g_untimerFor20ms:	.usect	".ebss",1,1,0
$C$DW$14	.dwtag  DW_TAG_variable
	.dwattr $C$DW$14, DW_AT_name("g_untimerFor20ms")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_g_untimerFor20ms")
	.dwattr $C$DW$14, DW_AT_location[DW_OP_addr _g_untimerFor20ms]
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$14, DW_AT_external

	.global	_g_unSPIErrorCheckCounter
_g_unSPIErrorCheckCounter:	.usect	".ebss",1,1,0
$C$DW$15	.dwtag  DW_TAG_variable
	.dwattr $C$DW$15, DW_AT_name("g_unSPIErrorCheckCounter")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_g_unSPIErrorCheckCounter")
	.dwattr $C$DW$15, DW_AT_location[DW_OP_addr _g_unSPIErrorCheckCounter]
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$15, DW_AT_external

$C$DW$16	.dwtag  DW_TAG_variable
	.dwattr $C$DW$16, DW_AT_name("Counter")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_Counter")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external


$C$DW$17	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$17, DW_AT_name("SPIDmaTxRx")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_SPIDmaTxRx")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
	.dwendtag $C$DW$17


$C$DW$18	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$18, DW_AT_name("SPICommWrite")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_SPICommWrite")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
	.dwendtag $C$DW$18

$C$DW$19	.dwtag  DW_TAG_variable
	.dwattr $C$DW$19, DW_AT_name("usTimerFlag")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_usTimerFlag")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external


$C$DW$20	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$20, DW_AT_name("SPICommRead")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_SPICommRead")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external
	.dwendtag $C$DW$20

	.global	_g_unSequenceStateTime
_g_unSequenceStateTime:	.usect	".ebss",2,1,1
$C$DW$21	.dwtag  DW_TAG_variable
	.dwattr $C$DW$21, DW_AT_name("g_unSequenceStateTime")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_g_unSequenceStateTime")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_addr _g_unSequenceStateTime]
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$21, DW_AT_external

	.global	_g_unSequenceStateTime1
_g_unSequenceStateTime1:	.usect	".ebss",2,1,1
$C$DW$22	.dwtag  DW_TAG_variable
	.dwattr $C$DW$22, DW_AT_name("g_unSequenceStateTime1")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_g_unSequenceStateTime1")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_addr _g_unSequenceStateTime1]
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$22, DW_AT_external

;	D:\Softwares\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\DITTOM~1.DEV\\AppData\\Local\\Temp\\0176812 
	.sect	".text:_main"
	.clink
	.global	_main

$C$DW$23	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$23, DW_AT_name("main")
	.dwattr $C$DW$23, DW_AT_low_pc(_main)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_main")
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_TI_begin_file("../source/main.c")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0x39)
	.dwattr $C$DW$23, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/main.c",line 58,column 1,is_stmt,address _main,isa 0

	.dwfde $C$DW$CIE, _main
;----------------------------------------------------------------------
;  57 | void main(void)                                                        
;  59 | //!SysSystem_Initalize() function initialize CPU peripherals and applic
;     | ation                                                                  
;  60 | //!modules                                                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _main                         FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_main:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/main.c",line 61,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  61 | SysSystem_Initalize();                                                 
;  63 | //!BloodCellCounter_initialize() function initialize all the variables
;     | of MBD                                                                 
;----------------------------------------------------------------------
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("_SysSystem_Initalize")
	.dwattr $C$DW$24, DW_AT_TI_call

        LCR       #_SysSystem_Initalize ; [CPU_] |61| 
        ; call occurs [#_SysSystem_Initalize] ; [] |61| 
	.dwpsn	file "../source/main.c",line 64,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  64 | BloodCellCounter_initialize();                                         
;  66 | //!GPIO used for toggling the LED1 is used for Controlling resistor    
;  67 | //! R150 bypassing                                                     
;  68 | //! This has to be reverted after revising the board                   
;----------------------------------------------------------------------
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("_BloodCellCounter_initialize")
	.dwattr $C$DW$25, DW_AT_TI_call

        LCR       #_BloodCellCounter_initialize ; [CPU_] |64| 
        ; call occurs [#_BloodCellCounter_initialize] ; [] |64| 
	.dwpsn	file "../source/main.c",line 69,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
;  69 | IGConfigHighVoltageModule(LED1_GPIO, 0);                               
;  71 |     //!1 Second delay is introduces so that the process runs after prop
;     | er and                                                                 
;  72 |     //!complete initialization of peripherals                          
;----------------------------------------------------------------------
        MOVB      AL,#116               ; [CPU_] |69| 
        MOVB      AH,#0                 ; [CPU_] |69| 
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_IGConfigHighVoltageModule")
	.dwattr $C$DW$26, DW_AT_TI_call

        LCR       #_IGConfigHighVoltageModule ; [CPU_] |69| 
        ; call occurs [#_IGConfigHighVoltageModule] ; [] |69| 
	.dwpsn	file "../source/main.c",line 73,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  73 | DELAY_US(1000000);                                                     
;----------------------------------------------------------------------
        MOV       AL,#23038             ; [CPU_] |73| 
        MOV       AH,#610               ; [CPU_] |73| 
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("_F28x_usDelay")
	.dwattr $C$DW$27, DW_AT_TI_call

        LCR       #_F28x_usDelay        ; [CPU_] |73| 
        ; call occurs [#_F28x_usDelay] ; [] |73| 
	.dwpsn	file "../source/main.c",line 75,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
;  75 | Enable_WatchdogTimer();                                                
;----------------------------------------------------------------------
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_Enable_WatchdogTimer")
	.dwattr $C$DW$28, DW_AT_TI_call

        LCR       #_Enable_WatchdogTimer ; [CPU_] |75| 
        ; call occurs [#_Enable_WatchdogTimer] ; [] |75| 
	.dwpsn	file "../source/main.c",line 77,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
;  77 | while(1)                                                               
;  79 |     //!The timer is configured to 5mSec.                               
;----------------------------------------------------------------------
$C$L1:    
	.dwpsn	file "../source/main.c",line 80,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
;  80 | if(usTimerFlag == 1)                                                   
;  82 |     //!Reset the timer flag                                            
;----------------------------------------------------------------------
        MOVW      DP,#_usTimerFlag      ; [CPU_U] 
        MOV       AL,@_usTimerFlag      ; [CPU_] |80| 
        CMPB      AL,#1                 ; [CPU_] |80| 
        B         $C$L2,NEQ             ; [CPU_] |80| 
        ; branchcc occurs ; [] |80| 
	.dwpsn	file "../source/main.c",line 83,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
;  83 | usTimerFlag = 0;                                                       
;----------------------------------------------------------------------
        MOV       @_usTimerFlag,#0      ; [CPU_] |83| 
	.dwpsn	file "../source/main.c",line 85,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
;  85 | g_untimerFor20ms++;                                                    
;  87 | //!Acquire the ADC health status                                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_untimerFor20ms ; [CPU_U] 
        INC       @_g_untimerFor20ms    ; [CPU_] |85| 
	.dwpsn	file "../source/main.c",line 88,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
;  88 | ADAdcAcquireHealthStatus();                                            
;  89 | //!Get the System health status from ADC                               
;----------------------------------------------------------------------
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("_ADAdcAcquireHealthStatus")
	.dwattr $C$DW$29, DW_AT_TI_call

        LCR       #_ADAdcAcquireHealthStatus ; [CPU_] |88| 
        ; call occurs [#_ADAdcAcquireHealthStatus] ; [] |88| 
	.dwpsn	file "../source/main.c",line 90,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
;  90 | SysInitGetSystemStatus();                                              
;  92 | //!The timer is configured to 20mSec.                                  
;  93 | //!Each cycle does not exceed 20mSec.                                  
;  94 | //!If the cycle exceeds greater than 20mSec, then either the current   
;  95 | //!execution of activities in 20mSec or the next cycle shall be affecte
;     | d                                                                      
;----------------------------------------------------------------------
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("_SysInitGetSystemStatus")
	.dwattr $C$DW$30, DW_AT_TI_call

        LCR       #_SysInitGetSystemStatus ; [CPU_] |90| 
        ; call occurs [#_SysInitGetSystemStatus] ; [] |90| 
$C$L2:    
	.dwpsn	file "../source/main.c",line 96,column 6,is_stmt,isa 0
;----------------------------------------------------------------------
;  96 | if(4 == g_untimerFor20ms)                                              
;  98 |         //!SysRT_OneStep() function perform the sequence of operation f
;     | or                                                                     
;  99 |         //!blood data analysis and motor control                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_untimerFor20ms ; [CPU_U] 
        MOV       AL,@_g_untimerFor20ms ; [CPU_] |96| 
        CMPB      AL,#4                 ; [CPU_] |96| 
        B         $C$L3,NEQ             ; [CPU_] |96| 
        ; branchcc occurs ; [] |96| 
	.dwpsn	file "../source/main.c",line 100,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 100 | SysRT_OneStep();                                                       
; 102 | //!Reset the timer flag                                                
; 103 | //!If the execution of the function SysRT_OneStep() finishes in less   
; 104 | //!than 20mSec, the cycle shall continue only for the next 20mSec      
;----------------------------------------------------------------------
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_name("_SysRT_OneStep")
	.dwattr $C$DW$31, DW_AT_TI_call

        LCR       #_SysRT_OneStep       ; [CPU_] |100| 
        ; call occurs [#_SysRT_OneStep] ; [] |100| 
	.dwpsn	file "../source/main.c",line 106,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 106 | g_untimerFor20ms = 0;                                                  
; 108 |     //!g_unSequenceStateTime maintains the count of the execution time
;     | or the                                                                 
; 109 |     //!can be used to count no. of 20mSec execution occurred           
;----------------------------------------------------------------------
        MOVW      DP,#_g_untimerFor20ms ; [CPU_U] 
        MOV       @_g_untimerFor20ms,#0 ; [CPU_] |106| 
	.dwpsn	file "../source/main.c",line 110,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 110 | g_unSequenceStateTime++;                                               
;----------------------------------------------------------------------
        MOVB      ACC,#1                ; [CPU_] |110| 
        ADDL      @_g_unSequenceStateTime,ACC ; [CPU_] |110| 
	.dwpsn	file "../source/main.c",line 112,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 112 | g_unSequenceStateTime1++;                                              
; 114 | //!System health status to be monitored every 60ms                     
;----------------------------------------------------------------------
        ADDL      @_g_unSequenceStateTime1,ACC ; [CPU_] |112| 
	.dwpsn	file "../source/main.c",line 115,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 115 | g_unSysHealthMonCount++;                                               
; 117 | //!SPI Error check counter to be incremented                           
;----------------------------------------------------------------------
        INC       @_g_unSysHealthMonCount ; [CPU_] |115| 
	.dwpsn	file "../source/main.c",line 118,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 118 | g_unSPIErrorCheckCounter++;                                            
;----------------------------------------------------------------------
        INC       @_g_unSPIErrorCheckCounter ; [CPU_] |118| 
	.dwpsn	file "../source/main.c",line 120,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 120 | Counter++;                                                             
; 122 | //!Reset the Watchdog timer so that i will not reach max               
;----------------------------------------------------------------------
        MOVW      DP,#_Counter          ; [CPU_U] 
        INC       @_Counter             ; [CPU_] |120| 
	.dwpsn	file "../source/main.c",line 123,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 123 | ServiceDog();                                                          
; 126 | //!SPICommWrite() function to write the data to DMA and SPI buffer     
;----------------------------------------------------------------------
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("_ServiceDog")
	.dwattr $C$DW$32, DW_AT_TI_call

        LCR       #_ServiceDog          ; [CPU_] |123| 
        ; call occurs [#_ServiceDog] ; [] |123| 
$C$L3:    
	.dwpsn	file "../source/main.c",line 127,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 127 | SPICommWrite();                                                        
; 129 | //!SPICommRead() function to read the data from DMA and SPI buffer     
;----------------------------------------------------------------------
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_name("_SPICommWrite")
	.dwattr $C$DW$33, DW_AT_TI_call

        LCR       #_SPICommWrite        ; [CPU_] |127| 
        ; call occurs [#_SPICommWrite] ; [] |127| 
	.dwpsn	file "../source/main.c",line 130,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 130 | SPICommRead();                                                         
; 132 | //!SPIDmaTxRx() function to decode the received data and to start the  
; 133 | //!receive DMA                                                         
;----------------------------------------------------------------------
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_SPICommRead")
	.dwattr $C$DW$34, DW_AT_TI_call

        LCR       #_SPICommRead         ; [CPU_] |130| 
        ; call occurs [#_SPICommRead] ; [] |130| 
	.dwpsn	file "../source/main.c",line 134,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 134 | SPIDmaTxRx();                                                          
;----------------------------------------------------------------------
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("_SPIDmaTxRx")
	.dwattr $C$DW$35, DW_AT_TI_call

        LCR       #_SPIDmaTxRx          ; [CPU_] |134| 
        ; call occurs [#_SPIDmaTxRx] ; [] |134| 
	.dwpsn	file "../source/main.c",line 77,column 8,is_stmt,isa 0
        B         $C$L1,UNC             ; [CPU_] |77| 
        ; branch occurs ; [] |77| 
	.dwattr $C$DW$23, DW_AT_TI_end_file("../source/main.c")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0x88)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$23

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_SysInitGetSystemStatus
	.global	_IGConfigHighVoltageModule
	.global	_SysRT_OneStep
	.global	_SysSystem_Initalize
	.global	_F28x_usDelay
	.global	_ServiceDog
	.global	_BloodCellCounter_initialize
	.global	_Enable_WatchdogTimer
	.global	_ADAdcAcquireHealthStatus
	.global	_Counter
	.global	_SPIDmaTxRx
	.global	_SPICommWrite
	.global	_usTimerFlag
	.global	_SPICommRead

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("Uint16")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)

$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$27	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$27, DW_AT_name("Uint32")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

$C$DW$T$28	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$28, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$28, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x01)

$C$DW$T$29	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$29, DW_AT_name("bool_t")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$36	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$36, DW_AT_name("AL")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg0]

$C$DW$37	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$37, DW_AT_name("AH")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg1]

$C$DW$38	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$38, DW_AT_name("PL")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg2]

$C$DW$39	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$39, DW_AT_name("PH")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg3]

$C$DW$40	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$40, DW_AT_name("SP")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg20]

$C$DW$41	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$41, DW_AT_name("XT")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg21]

$C$DW$42	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$42, DW_AT_name("T")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg22]

$C$DW$43	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$43, DW_AT_name("ST0")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg23]

$C$DW$44	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$44, DW_AT_name("ST1")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg24]

$C$DW$45	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$45, DW_AT_name("PC")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg25]

$C$DW$46	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$46, DW_AT_name("RPC")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg26]

$C$DW$47	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$47, DW_AT_name("FP")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg28]

$C$DW$48	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$48, DW_AT_name("DP")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg29]

$C$DW$49	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$49, DW_AT_name("SXM")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg30]

$C$DW$50	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$50, DW_AT_name("PM")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg31]

$C$DW$51	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$51, DW_AT_name("OVM")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_regx 0x20]

$C$DW$52	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$52, DW_AT_name("PAGE0")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_regx 0x21]

$C$DW$53	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$53, DW_AT_name("AMODE")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_regx 0x22]

$C$DW$54	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$54, DW_AT_name("INTM")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_regx 0x23]

$C$DW$55	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$55, DW_AT_name("IFR")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_regx 0x24]

$C$DW$56	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$56, DW_AT_name("IER")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_regx 0x25]

$C$DW$57	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$57, DW_AT_name("V")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_regx 0x26]

$C$DW$58	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$58, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$59	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$59, DW_AT_name("VOL")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$60	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$60, DW_AT_name("AR0")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg4]

$C$DW$61	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$61, DW_AT_name("XAR0")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg5]

$C$DW$62	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$62, DW_AT_name("AR1")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg6]

$C$DW$63	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$63, DW_AT_name("XAR1")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg7]

$C$DW$64	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$64, DW_AT_name("AR2")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg8]

$C$DW$65	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$65, DW_AT_name("XAR2")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg9]

$C$DW$66	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$66, DW_AT_name("AR3")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg10]

$C$DW$67	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$67, DW_AT_name("XAR3")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg11]

$C$DW$68	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$68, DW_AT_name("AR4")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg12]

$C$DW$69	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$69, DW_AT_name("XAR4")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg13]

$C$DW$70	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$70, DW_AT_name("AR5")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_reg14]

$C$DW$71	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$71, DW_AT_name("XAR5")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_reg15]

$C$DW$72	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$72, DW_AT_name("AR6")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg16]

$C$DW$73	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$73, DW_AT_name("XAR6")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg17]

$C$DW$74	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$74, DW_AT_name("AR7")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg18]

$C$DW$75	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$75, DW_AT_name("XAR7")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg19]

$C$DW$76	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$76, DW_AT_name("R0HL")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_regx 0x29]

$C$DW$77	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$77, DW_AT_name("R0H")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$78	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$78, DW_AT_name("R1HL")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$79	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$79, DW_AT_name("R1H")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$80	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$80, DW_AT_name("R2HL")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$81	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$81, DW_AT_name("R2H")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$82	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$82, DW_AT_name("R3HL")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$83	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$83, DW_AT_name("R3H")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_regx 0x30]

$C$DW$84	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$84, DW_AT_name("R4HL")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_regx 0x31]

$C$DW$85	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$85, DW_AT_name("R4H")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_regx 0x32]

$C$DW$86	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$86, DW_AT_name("R5HL")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_regx 0x33]

$C$DW$87	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$87, DW_AT_name("R5H")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_regx 0x34]

$C$DW$88	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$88, DW_AT_name("R6HL")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x35]

$C$DW$89	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$89, DW_AT_name("R6H")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x36]

$C$DW$90	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$90, DW_AT_name("R7HL")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x37]

$C$DW$91	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$91, DW_AT_name("R7H")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x38]

$C$DW$92	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$92, DW_AT_name("RBL")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x49]

$C$DW$93	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$93, DW_AT_name("RB")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$94	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$94, DW_AT_name("STFL")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x27]

$C$DW$95	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$95, DW_AT_name("STF")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x28]

$C$DW$96	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$96, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


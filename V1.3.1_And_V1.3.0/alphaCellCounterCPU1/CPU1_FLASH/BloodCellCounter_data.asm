;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v15.9.0.STS *
;* Date/Time created: Mon Dec 26 12:02:15 2016                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../mbd/BloodCellCounter_data.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v15.9.0.STS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("F:\Hemadri\CellCounter_Alpha_23_Dec_2016\CellCounter_Alpha\alphaCellCounterCPU1\CPU1_FLASH")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-$C$IR_1,16
	.field  	_BloodCellCounter_P+0,32
	.bits	20515,16			; _BloodCellCounter_P._K_COM_CALIBRATION_PLT @ 0
	.bits	20515,16			; _BloodCellCounter_P._K_COM_CALIBRATION_PLT_COMPLETE @ 16
	.bits	20514,16			; _BloodCellCounter_P._K_COM_CALIBRATION_RBC @ 32
	.bits	20514,16			; _BloodCellCounter_P._K_COM_CALIBRATION_RBC_COMPLETE @ 48
	.bits	20513,16			; _BloodCellCounter_P._K_COM_CALIBRATION_WBC @ 64
	.bits	20513,16			; _BloodCellCounter_P._K_COM_CALIBRATION_WBC_COMPLETE @ 80
	.bits	20519,16			; _BloodCellCounter_P._K_QUAL_CONTROL_BF @ 96
	.bits	20519,16			; _BloodCellCounter_P._K_QUAL_CONTROL_BF_COMPLETE @ 112
	.bits	20518,16			; _BloodCellCounter_P._K_QUAL_CONTROL_WB @ 128
	.bits	20518,16			; _BloodCellCounter_P._K_QUAL_CONTROL_WB_COMPLETE @ 144
$C$IR_1:	.set	10

	.global	_BloodCellCounter_P
_BloodCellCounter_P:	.usect	".ebss",10,1,0
$C$DW$1	.dwtag  DW_TAG_variable, DW_AT_name("BloodCellCounter_P")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_BloodCellCounter_P")
	.dwattr $C$DW$1, DW_AT_location[DW_OP_addr _BloodCellCounter_P]
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$1, DW_AT_external
;	C:\ti\ccsv6\tools\compiler\ti-cgt-c2000_15.9.0.STS\bin\ac2000.exe -@C:\\Users\\20068300\\AppData\\Local\\Temp\\0568012 

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_name("Parameters_BloodCellCounter_T_")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0a)
$C$DW$2	.dwtag  DW_TAG_member
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$2, DW_AT_name("K_COM_CALIBRATION_PLT")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_K_COM_CALIBRATION_PLT")
	.dwattr $C$DW$2, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$2, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$3	.dwtag  DW_TAG_member
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$3, DW_AT_name("K_COM_CALIBRATION_PLT_COMPLETE")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_K_COM_CALIBRATION_PLT_COMPLETE")
	.dwattr $C$DW$3, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$3, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$4	.dwtag  DW_TAG_member
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$4, DW_AT_name("K_COM_CALIBRATION_RBC")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_K_COM_CALIBRATION_RBC")
	.dwattr $C$DW$4, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$4, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$5	.dwtag  DW_TAG_member
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$5, DW_AT_name("K_COM_CALIBRATION_RBC_COMPLETE")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_K_COM_CALIBRATION_RBC_COMPLETE")
	.dwattr $C$DW$5, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$5, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$6	.dwtag  DW_TAG_member
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$6, DW_AT_name("K_COM_CALIBRATION_WBC")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_K_COM_CALIBRATION_WBC")
	.dwattr $C$DW$6, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$6, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$7	.dwtag  DW_TAG_member
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$7, DW_AT_name("K_COM_CALIBRATION_WBC_COMPLETE")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_K_COM_CALIBRATION_WBC_COMPLETE")
	.dwattr $C$DW$7, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$7, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$8	.dwtag  DW_TAG_member
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$8, DW_AT_name("K_QUAL_CONTROL_BF")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_K_QUAL_CONTROL_BF")
	.dwattr $C$DW$8, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$8, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$9	.dwtag  DW_TAG_member
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$9, DW_AT_name("K_QUAL_CONTROL_BF_COMPLETE")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_K_QUAL_CONTROL_BF_COMPLETE")
	.dwattr $C$DW$9, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$9, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$10	.dwtag  DW_TAG_member
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$10, DW_AT_name("K_QUAL_CONTROL_WB")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_K_QUAL_CONTROL_WB")
	.dwattr $C$DW$10, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$10, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$11	.dwtag  DW_TAG_member
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$11, DW_AT_name("K_QUAL_CONTROL_WB_COMPLETE")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_K_QUAL_CONTROL_WB_COMPLETE")
	.dwattr $C$DW$11, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$11, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$21	.dwtag  DW_TAG_typedef, DW_AT_name("Parameters_BloodCellCounter_T")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_T")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)
	.dwendtag $C$DW$CU


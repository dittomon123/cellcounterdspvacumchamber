;***************************************************************
;* TMS320C2000 C/C++ Codegen                    PC v16.9.1.LTS *
;* Date/Time created: Fri Jan 29 15:14:24 2021                 *
;***************************************************************
	.compiler_opts --abi=coffabi --cla_support=cla1 --diag_wrap=off --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=3 --tmu_support=tmu0 
	.asg	XAR2, FP

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../source/CommInterface.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI TMS320C2000 C/C++ Codegen PC v16.9.1.LTS Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\Cell Counter\V1.3.1_And_V1.3.0\alphaCellCounterCPU1\CPU1_FLASH")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_usnCurrentModeMajor+0,32
	.bits	0,16			; _g_usnCurrentModeMajor @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_bAbnormalStartup+0,32
	.bits	0,16			; _m_bAbnormalStartup @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_bPressureCaliStart+0,32
	.bits	0,16			; _g_bPressureCaliStart @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_usnCurrentModeTemp+0,32
	.bits	0,16			; _g_usnCurrentModeTemp @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_bZapInitiateFlag+0,32
	.bits	0,16			; _m_bZapInitiateFlag @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_bNextSecRawDataReq+0,32
	.bits	0,16			; _g_bNextSecRawDataReq @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_bSystemStatusFlag+0,32
	.bits	0,16			; _m_bSystemStatusFlag @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_bNAckReceived+0,32
	.bits	0,16			; _m_bNAckReceived @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_bStartButtonStatus+0,32
	.bits	0,16			; _g_bStartButtonStatus @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_usnPacketCount+0,32
	.bits	0,16			; _m_usnPacketCount @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_m_ucValveNo+0,32
	.bits	0,16			; _m_ucValveNo @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_g_usnCurrentMode+0,32
	.bits	0,16			; _g_usnCurrentMode @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("SysInitValveTest")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_SysInitValveTest")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$6)

	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$3, DW_AT_name("SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
	.dwendtag $C$DW$3


$C$DW$4	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$4, DW_AT_name("SysInitWasteBinReplaced")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_SysInitWasteBinReplaced")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
	.dwendtag $C$DW$4


$C$DW$5	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$5, DW_AT_name("PWMParamDefaultState")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_PWMParamDefaultState")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
	.dwendtag $C$DW$5


$C$DW$6	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$6, DW_AT_name("SysReInit")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_SysReInit")
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
	.dwendtag $C$DW$6


$C$DW$7	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$7, DW_AT_name("UartDebugPrint")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_UartDebugPrint")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$37)

$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$10)

	.dwendtag $C$DW$7


$C$DW$10	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$10, DW_AT_name("BloodCellCounter_initialize")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_BloodCellCounter_initialize")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
	.dwendtag $C$DW$10


$C$DW$11	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$11, DW_AT_name("CPFrameDataPacket")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_CPFrameDataPacket")
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$40)

$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$40)

$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$42)

	.dwendtag $C$DW$11


$C$DW$15	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$15, DW_AT_name("SPISetCommCtrlBusyInt")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external
	.dwendtag $C$DW$15


$C$DW$16	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$16, DW_AT_name("CPReInitialiseOnNewMeasurement")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_CPReInitialiseOnNewMeasurement")
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
	.dwendtag $C$DW$16


$C$DW$17	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$17, DW_AT_name("SPIReConfigCommDmaReg")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_SPIReConfigCommDmaReg")
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
	.dwendtag $C$DW$17


$C$DW$18	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$18, DW_AT_name("PWMHgbPwmControl")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_PWMHgbPwmControl")
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$40)

$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$40)

	.dwendtag $C$DW$18


$C$DW$21	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$21, DW_AT_name("SysMotorControlIdleMode")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_SysMotorControlIdleMode")
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
	.dwendtag $C$DW$21


$C$DW$22	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$22, DW_AT_name("SysInitProbeCleanArrestSequence")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_SysInitProbeCleanArrestSequence")
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$42)

	.dwendtag $C$DW$22

$C$DW$24	.dwtag  DW_TAG_variable
	.dwattr $C$DW$24, DW_AT_name("m_bSequenceCompleted")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_m_bSequenceCompleted")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$24, DW_AT_declaration
	.dwattr $C$DW$24, DW_AT_external

$C$DW$25	.dwtag  DW_TAG_variable
	.dwattr $C$DW$25, DW_AT_name("g_usnFirstDilutionFreq")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_g_usnFirstDilutionFreq")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external

	.global	_g_usnCurrentModeMajor
_g_usnCurrentModeMajor:	.usect	".ebss",1,1,0
$C$DW$26	.dwtag  DW_TAG_variable
	.dwattr $C$DW$26, DW_AT_name("g_usnCurrentModeMajor")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_g_usnCurrentModeMajor")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_addr _g_usnCurrentModeMajor]
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$26, DW_AT_external

$C$DW$27	.dwtag  DW_TAG_variable
	.dwattr $C$DW$27, DW_AT_name("g_usnFinalRBCMixingFreq")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_g_usnFinalRBCMixingFreq")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external

$C$DW$28	.dwtag  DW_TAG_variable
	.dwattr $C$DW$28, DW_AT_name("g_bMonDiluentPriming")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_g_bMonDiluentPriming")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external

$C$DW$29	.dwtag  DW_TAG_variable
	.dwattr $C$DW$29, DW_AT_name("g_bBusyBitStatus")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_g_bBusyBitStatus")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external

	.global	_m_bAbnormalStartup
_m_bAbnormalStartup:	.usect	".ebss",1,1,0
$C$DW$30	.dwtag  DW_TAG_variable
	.dwattr $C$DW$30, DW_AT_name("m_bAbnormalStartup")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_m_bAbnormalStartup")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_addr _m_bAbnormalStartup]
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$30, DW_AT_external

	.global	_g_bPressureCaliStart
_g_bPressureCaliStart:	.usect	".ebss",1,1,0
$C$DW$31	.dwtag  DW_TAG_variable
	.dwattr $C$DW$31, DW_AT_name("g_bPressureCaliStart")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_g_bPressureCaliStart")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_addr _g_bPressureCaliStart]
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$31, DW_AT_external

	.global	_g_usnCurrentModeTemp
_g_usnCurrentModeTemp:	.usect	".ebss",1,1,0
$C$DW$32	.dwtag  DW_TAG_variable
	.dwattr $C$DW$32, DW_AT_name("g_usnCurrentModeTemp")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_g_usnCurrentModeTemp")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_addr _g_usnCurrentModeTemp]
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$32, DW_AT_external

	.global	_m_bZapInitiateFlag
_m_bZapInitiateFlag:	.usect	".ebss",1,1,0
$C$DW$33	.dwtag  DW_TAG_variable
	.dwattr $C$DW$33, DW_AT_name("m_bZapInitiateFlag")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_m_bZapInitiateFlag")
	.dwattr $C$DW$33, DW_AT_location[DW_OP_addr _m_bZapInitiateFlag]
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$33, DW_AT_external

$C$DW$34	.dwtag  DW_TAG_variable
	.dwattr $C$DW$34, DW_AT_name("g_usnLyseMixingFreq")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_g_usnLyseMixingFreq")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external

	.global	_g_bNextSecRawDataReq
_g_bNextSecRawDataReq:	.usect	".ebss",1,1,0
$C$DW$35	.dwtag  DW_TAG_variable
	.dwattr $C$DW$35, DW_AT_name("g_bNextSecRawDataReq")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_g_bNextSecRawDataReq")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_addr _g_bNextSecRawDataReq]
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$35, DW_AT_external

$C$DW$36	.dwtag  DW_TAG_variable
	.dwattr $C$DW$36, DW_AT_name("m_bTxPendingFlag")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_m_bTxPendingFlag")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external

$C$DW$37	.dwtag  DW_TAG_variable
	.dwattr $C$DW$37, DW_AT_name("m_bTxToDMAFlag")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_m_bTxToDMAFlag")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$37, DW_AT_declaration
	.dwattr $C$DW$37, DW_AT_external

$C$DW$38	.dwtag  DW_TAG_variable
	.dwattr $C$DW$38, DW_AT_name("m_bPressureMonitor")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_m_bPressureMonitor")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external

$C$DW$39	.dwtag  DW_TAG_variable
	.dwattr $C$DW$39, DW_AT_name("g_bDataTransmitted")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_g_bDataTransmitted")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$39, DW_AT_declaration
	.dwattr $C$DW$39, DW_AT_external

$C$DW$40	.dwtag  DW_TAG_variable
	.dwattr $C$DW$40, DW_AT_name("g_bWasteDetectCheck")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_g_bWasteDetectCheck")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external

$C$DW$41	.dwtag  DW_TAG_variable
	.dwattr $C$DW$41, DW_AT_name("m_bSPIWriteControlStatus")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_m_bSPIWriteControlStatus")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external

$C$DW$42	.dwtag  DW_TAG_variable
	.dwattr $C$DW$42, DW_AT_name("g_bMonRinsePriming")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_g_bMonRinsePriming")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$42, DW_AT_declaration
	.dwattr $C$DW$42, DW_AT_external

$C$DW$43	.dwtag  DW_TAG_variable
	.dwattr $C$DW$43, DW_AT_name("m_eCycleCompletion")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_m_eCycleCompletion")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external

	.global	_m_bSystemStatusFlag
_m_bSystemStatusFlag:	.usect	".ebss",1,1,0
$C$DW$44	.dwtag  DW_TAG_variable
	.dwattr $C$DW$44, DW_AT_name("m_bSystemStatusFlag")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_m_bSystemStatusFlag")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_addr _m_bSystemStatusFlag]
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$44, DW_AT_external

$C$DW$45	.dwtag  DW_TAG_variable
	.dwattr $C$DW$45, DW_AT_name("m_bReadyforAspCommand")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_m_bReadyforAspCommand")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external

$C$DW$46	.dwtag  DW_TAG_variable
	.dwattr $C$DW$46, DW_AT_name("g_usnDataMode")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_g_usnDataMode")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$46, DW_AT_declaration
	.dwattr $C$DW$46, DW_AT_external

$C$DW$47	.dwtag  DW_TAG_variable
	.dwattr $C$DW$47, DW_AT_name("g_bMonLysePriming")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_g_bMonLysePriming")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$47, DW_AT_declaration
	.dwattr $C$DW$47, DW_AT_external

$C$DW$48	.dwtag  DW_TAG_variable
	.dwattr $C$DW$48, DW_AT_name("m_ucValveControlTime")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_m_ucValveControlTime")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$48, DW_AT_declaration
	.dwattr $C$DW$48, DW_AT_external

$C$DW$49	.dwtag  DW_TAG_variable
	.dwattr $C$DW$49, DW_AT_name("g_LyseMixTime")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_g_LyseMixTime")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external

$C$DW$50	.dwtag  DW_TAG_variable
	.dwattr $C$DW$50, DW_AT_name("g_usnWholeBloodZapTime")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_g_usnWholeBloodZapTime")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external

$C$DW$51	.dwtag  DW_TAG_variable
	.dwattr $C$DW$51, DW_AT_name("g_DiluentMixTime")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_g_DiluentMixTime")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$51, DW_AT_declaration
	.dwattr $C$DW$51, DW_AT_external

$C$DW$52	.dwtag  DW_TAG_variable
	.dwattr $C$DW$52, DW_AT_name("g_usnWbcCalibratedTime")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_g_usnWbcCalibratedTime")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external

$C$DW$53	.dwtag  DW_TAG_variable
	.dwattr $C$DW$53, DW_AT_name("g_RBCMixTime")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_g_RBCMixTime")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external


$C$DW$54	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$54, DW_AT_name("sprintf")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_sprintf")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$54, DW_AT_declaration
	.dwattr $C$DW$54, DW_AT_external
$C$DW$55	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$58)

$C$DW$56	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$60)

$C$DW$57	.dwtag  DW_TAG_unspecified_parameters

	.dwendtag $C$DW$54

	.global	_m_bNAckReceived
_m_bNAckReceived:	.usect	".ebss",1,1,0
$C$DW$58	.dwtag  DW_TAG_variable
	.dwattr $C$DW$58, DW_AT_name("m_bNAckReceived")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_m_bNAckReceived")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_addr _m_bNAckReceived]
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$58, DW_AT_external

$C$DW$59	.dwtag  DW_TAG_variable
	.dwattr $C$DW$59, DW_AT_name("g_bYMotorFaultMonEn")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_g_bYMotorFaultMonEn")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$59, DW_AT_declaration
	.dwattr $C$DW$59, DW_AT_external

$C$DW$60	.dwtag  DW_TAG_variable
	.dwattr $C$DW$60, DW_AT_name("m_bVlaveCheckStatus")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_m_bVlaveCheckStatus")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$60, DW_AT_declaration
	.dwattr $C$DW$60, DW_AT_external

$C$DW$61	.dwtag  DW_TAG_variable
	.dwattr $C$DW$61, DW_AT_name("g_usnRbcCalibratedTime")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_g_usnRbcCalibratedTime")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$61, DW_AT_declaration
	.dwattr $C$DW$61, DW_AT_external

$C$DW$62	.dwtag  DW_TAG_variable
	.dwattr $C$DW$62, DW_AT_name("g_TestDataI0")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_g_TestDataI0")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$62, DW_AT_declaration
	.dwattr $C$DW$62, DW_AT_external

	.global	_g_bStartButtonStatus
_g_bStartButtonStatus:	.usect	".ebss",1,1,0
$C$DW$63	.dwtag  DW_TAG_variable
	.dwattr $C$DW$63, DW_AT_name("g_bStartButtonStatus")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_g_bStartButtonStatus")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_addr _g_bStartButtonStatus]
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$63, DW_AT_external

$C$DW$64	.dwtag  DW_TAG_variable
	.dwattr $C$DW$64, DW_AT_name("g_bLowVolumeDisp")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_g_bLowVolumeDisp")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$64, DW_AT_declaration
	.dwattr $C$DW$64, DW_AT_external

	.global	_m_eTypeofCell
_m_eTypeofCell:	.usect	".ebss",1,1,0
$C$DW$65	.dwtag  DW_TAG_variable
	.dwattr $C$DW$65, DW_AT_name("m_eTypeofCell")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_m_eTypeofCell")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_addr _m_eTypeofCell]
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$65, DW_AT_external

	.global	_m_usnPacketCount
_m_usnPacketCount:	.usect	".ebss",1,1,0
$C$DW$66	.dwtag  DW_TAG_variable
	.dwattr $C$DW$66, DW_AT_name("m_usnPacketCount")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_m_usnPacketCount")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_addr _m_usnPacketCount]
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$66, DW_AT_external

	.global	_m_ucValveNo
_m_ucValveNo:	.usect	".ebss",1,1,0
$C$DW$67	.dwtag  DW_TAG_variable
	.dwattr $C$DW$67, DW_AT_name("m_ucValveNo")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_m_ucValveNo")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_addr _m_ucValveNo]
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$67, DW_AT_external

	.global	_g_usnCurrentMode
_g_usnCurrentMode:	.usect	".ebss",1,1,0
$C$DW$68	.dwtag  DW_TAG_variable
	.dwattr $C$DW$68, DW_AT_name("g_usnCurrentMode")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_g_usnCurrentMode")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_addr _g_usnCurrentMode]
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$68, DW_AT_external

$C$DW$69	.dwtag  DW_TAG_variable
	.dwattr $C$DW$69, DW_AT_name("Counter")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_Counter")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_external

$C$DW$70	.dwtag  DW_TAG_variable
	.dwattr $C$DW$70, DW_AT_name("g_TestDataI2")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_g_TestDataI2")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$70, DW_AT_declaration
	.dwattr $C$DW$70, DW_AT_external

$C$DW$71	.dwtag  DW_TAG_variable
	.dwattr $C$DW$71, DW_AT_name("g_TestDataI1")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_g_TestDataI1")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$71, DW_AT_declaration
	.dwattr $C$DW$71, DW_AT_external

$C$DW$72	.dwtag  DW_TAG_variable
	.dwattr $C$DW$72, DW_AT_name("g_unSequenceStateTime1")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_g_unSequenceStateTime1")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$72, DW_AT_declaration
	.dwattr $C$DW$72, DW_AT_external

$C$DW$73	.dwtag  DW_TAG_variable
	.dwattr $C$DW$73, DW_AT_name("g_unSequenceStateTime")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_g_unSequenceStateTime")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$73, DW_AT_declaration
	.dwattr $C$DW$73, DW_AT_external

$C$DW$74	.dwtag  DW_TAG_variable
	.dwattr $C$DW$74, DW_AT_name("sncalZeroPSI")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_sncalZeroPSI")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$74, DW_AT_declaration
	.dwattr $C$DW$74, DW_AT_external

$C$DW$75	.dwtag  DW_TAG_variable
	.dwattr $C$DW$75, DW_AT_name("utErrorInfoCheck")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_utErrorInfoCheck")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$75, DW_AT_declaration
	.dwattr $C$DW$75, DW_AT_external

$C$DW$76	.dwtag  DW_TAG_variable
	.dwattr $C$DW$76, DW_AT_name("BloodCellCounter_U")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_BloodCellCounter_U")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$76, DW_AT_declaration
	.dwattr $C$DW$76, DW_AT_external

$C$DW$77	.dwtag  DW_TAG_variable
	.dwattr $C$DW$77, DW_AT_name("DebugPrintBuf")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_DebugPrintBuf")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$77, DW_AT_declaration
	.dwattr $C$DW$77, DW_AT_external

_statDebugPrintBuf:	.usect	".ebss",500,1,0
$C$DW$78	.dwtag  DW_TAG_variable
	.dwattr $C$DW$78, DW_AT_name("statDebugPrintBuf")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_statDebugPrintBuf")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_addr _statDebugPrintBuf]

$C$DW$79	.dwtag  DW_TAG_variable
	.dwattr $C$DW$79, DW_AT_name("m_stPrevTxPacket")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_m_stPrevTxPacket")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$79, DW_AT_declaration
	.dwattr $C$DW$79, DW_AT_external

$C$DW$80	.dwtag  DW_TAG_variable
	.dwattr $C$DW$80, DW_AT_name("m_stReceivedPacketFrame")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_m_stReceivedPacketFrame")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$80, DW_AT_declaration
	.dwattr $C$DW$80, DW_AT_external

;	D:\Softwares\ti-cgt-c2000_16.9.1.LTS\bin\ac2000.exe -@C:\\Users\\DITTOM~1.DEV\\AppData\\Local\\Temp\\0744012 
	.sect	".text:_CIProcessCommandAttribute"
	.clink
	.global	_CIProcessCommandAttribute

$C$DW$81	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$81, DW_AT_name("CIProcessCommandAttribute")
	.dwattr $C$DW$81, DW_AT_low_pc(_CIProcessCommandAttribute)
	.dwattr $C$DW$81, DW_AT_high_pc(0x00)
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_CIProcessCommandAttribute")
	.dwattr $C$DW$81, DW_AT_external
	.dwattr $C$DW$81, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$81, DW_AT_TI_begin_line(0xaf)
	.dwattr $C$DW$81, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$81, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 176,column 1,is_stmt,address _CIProcessCommandAttribute,isa 0

	.dwfde $C$DW$CIE, _CIProcessCommandAttribute
$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_name("usnMajorCmd")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_usnMajorCmd")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg0]

$C$DW$83	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$83, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 175 | void CIProcessCommandAttribute(uint16_t usnMajorCmd, uint16_t usnMinorC
;     | md)                                                                    
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessCommandAttribute    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CIProcessCommandAttribute:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$84	.dwtag  DW_TAG_variable
	.dwattr $C$DW$84, DW_AT_name("usnMajorCmd")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_usnMajorCmd")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_breg20 -1]

$C$DW$85	.dwtag  DW_TAG_variable
	.dwattr $C$DW$85, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 177 | //!Check for the major command of the command received from the process
;     | or                                                                     
; 178 | //                                                                     
; 179 | //    sprintf(DebugPrintBuf, "\r\n CIProcessCommandAttribute %d %d\n\n"
;     | ,usnMajorCmd, usnMinorCmd);                                            
; 180 | //    UartDebugPrint((int *)DebugPrintBuf, 50);                        
;----------------------------------------------------------------------
        MOV       *-SP[2],AH            ; [CPU_] |176| 
        MOV       *-SP[1],AL            ; [CPU_] |176| 
	.dwpsn	file "../source/CommInterface.c",line 181,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 181 | switch (usnMajorCmd)                                                   
; 183 |     //!If the decoded data is Sync command perform the respective actio
;     | n                                                                      
; 184 |         case SITARA_SYNC_MAIN_CMD:                                     
;----------------------------------------------------------------------
        B         $C$L14,UNC            ; [CPU_] |181| 
        ; branch occurs ; [] |181| 
$C$L1:    
	.dwpsn	file "../source/CommInterface.c",line 185,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 185 | if( SITARA_SYNC_SUB_CMD == usnMinorCmd)                                
; 187 |         //Do Something                                                 
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[2]           ; [CPU_] |185| 
        MOVL      XAR4,#43690           ; [CPU_U] |185| 
        CMPL      ACC,XAR4              ; [CPU_] |185| 
        B         $C$L18,NEQ            ; [CPU_] |185| 
        ; branchcc occurs ; [] |185| 
	.dwpsn	file "../source/CommInterface.c",line 189,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 189 | break;                                                                 
; 191 | //!If the decoded data is Patient Handling command perform the action  
; 192 | //!with respect to patient handling sequences                          
; 193 | case SITARA_PATIENT_HANDLING:                                          
; 195 | //!Patient handling function is called by passing the minor command    
; 196 |     //!Patient handling has different seqence as Whole blood,          
; 197 |     //!Pre-Dilute, Body fluid, Partial blood count of WBC, RBC & PLT   
; 198 |     //!Corresponding actions are performed depending upon the minor    
; 199 |     //!command passed                                                  
; 200 |     //!Patient handling command is used for sending the results in any 
; 201 |     //!of the above mentioned sequence                                 
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |189| 
        ; branch occurs ; [] |189| 
$C$L2:    
	.dwpsn	file "../source/CommInterface.c",line 202,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 202 | g_usnCurrentModeMajor = SITARA_PATIENT_HANDLING;                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4097 ; [CPU_] |202| 
	.dwpsn	file "../source/CommInterface.c",line 203,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 203 | CIProcessPatientHandlingCmd(usnMinorCmd);                              
; 205 | //!Since it is only processing command,                                
; 206 | //!and no further sequence is initiated, clear the busy pin            
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |203| 
$C$DW$86	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$86, DW_AT_low_pc(0x00)
	.dwattr $C$DW$86, DW_AT_name("_CIProcessPatientHandlingCmd")
	.dwattr $C$DW$86, DW_AT_TI_call

        LCR       #_CIProcessPatientHandlingCmd ; [CPU_] |203| 
        ; call occurs [#_CIProcessPatientHandlingCmd] ; [] |203| 
	.dwpsn	file "../source/CommInterface.c",line 207,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 207 | SPIClearCommCtrlBusyInt();                                             
; 209 | //!Enable the flag for ready for aspiration as soon as the trigger     
; 210 | //! button is pressed                                                  
;----------------------------------------------------------------------
$C$DW$87	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$87, DW_AT_low_pc(0x00)
	.dwattr $C$DW$87, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$87, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |207| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |207| 
	.dwpsn	file "../source/CommInterface.c",line 211,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 211 | m_bReadyforAspCommand = TRUE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_m_bReadyforAspCommand ; [CPU_U] 
        MOVB      @_m_bReadyforAspCommand,#1,UNC ; [CPU_] |211| 
	.dwpsn	file "../source/CommInterface.c",line 213,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 213 | break;                                                                 
; 215 | //!If the decoded data is Service Handling command perform the action  
; 216 | //!with respect to service handling sequences                          
; 217 | case SITARA_SERVICE_HANDLING:                                          
; 219 | //!Service handling function is called by passing the minor command    
; 220 | //!Service handling has different seqence:  Diluent, Lyse,Rinse        
; 221 | //!Priming, Prime all, Flush Apperture, E-Z Cleaning, drain bath,      
; 222 | //!clean bath, ZAP Aperture, Valve test, Drain all,                    
; 223 | //!Startup, System status, waste bin replace, Head rinse,              
; 224 | //!Probe cleaning, RBC & WBC Count time sequence, Wakeup,              
; 225 | //!Motor check (X, Y, Sample, Diluent & Waste)                         
; 226 | //!Corresponding actions are performed depending upon the minor command
; 227 | //!passed                                                              
; 228 | //            sprintf(DebugPrintBuf, "\r\n SITARA_SERVICE_HANDLING mir:
;     |  %d \n",usnMinorCmd);                                                  
; 229 | //            UartDebugPrint((int *)DebugPrintBuf, 50);                
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |213| 
        ; branch occurs ; [] |213| 
$C$L3:    
	.dwpsn	file "../source/CommInterface.c",line 230,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 230 | g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4100 ; [CPU_] |230| 
	.dwpsn	file "../source/CommInterface.c",line 231,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 231 | CIProcessServiceHandlingCmd(usnMinorCmd);                              
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |231| 
$C$DW$88	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$88, DW_AT_low_pc(0x00)
	.dwattr $C$DW$88, DW_AT_name("_CIProcessServiceHandlingCmd")
	.dwattr $C$DW$88, DW_AT_TI_call

        LCR       #_CIProcessServiceHandlingCmd ; [CPU_] |231| 
        ; call occurs [#_CIProcessServiceHandlingCmd] ; [] |231| 
	.dwpsn	file "../source/CommInterface.c",line 233,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 233 | break;                                                                 
; 235 | //!If the decoded data is Acknowledgment command from Sitara, process  
; 236 | //!the acknowledgment                                                  
; 237 | case SITARA_ACK_MAIN_CMD:                                              
; 239 |     //!Process the acknowledgment command that is received             
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |233| 
        ; branch occurs ; [] |233| 
$C$L4:    
	.dwpsn	file "../source/CommInterface.c",line 240,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 240 | CIProcessACKCmd(usnMinorCmd);                                          
; 242 | //!Busy pin is set if any sequence to be initiated.                    
; 243 | //!Since it is only processing the ACK command, and no further         
; 244 | //!sequence is initiated, clear the busy pin                           
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |240| 
$C$DW$89	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$89, DW_AT_low_pc(0x00)
	.dwattr $C$DW$89, DW_AT_name("_CIProcessACKCmd")
	.dwattr $C$DW$89, DW_AT_TI_call

        LCR       #_CIProcessACKCmd     ; [CPU_] |240| 
        ; call occurs [#_CIProcessACKCmd] ; [] |240| 
	.dwpsn	file "../source/CommInterface.c",line 245,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 245 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$90	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$90, DW_AT_low_pc(0x00)
	.dwattr $C$DW$90, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$90, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |245| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |245| 
	.dwpsn	file "../source/CommInterface.c",line 247,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 247 | break;                                                                 
; 249 | //!If the decoded data is NAK command from Sitara, process             
; 250 | //!the NAK command                                                     
; 251 | case SITARA_NACK_MAIN_CMD:                                             
; 253 |     //!Process the Negative acknowledgement command that is received   
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |247| 
        ; branch occurs ; [] |247| 
$C$L5:    
	.dwpsn	file "../source/CommInterface.c",line 254,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 254 | CIProcessNACKCmd(usnMinorCmd);                                         
; 256 | //!Busy pin is set if any sequence to be initiated.                    
; 257 | //!Since it is only processing the NACK command, and no further        
; 258 | //!sequence is initiated, clear the busy pin                           
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |254| 
$C$DW$91	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$91, DW_AT_low_pc(0x00)
	.dwattr $C$DW$91, DW_AT_name("_CIProcessNACKCmd")
	.dwattr $C$DW$91, DW_AT_TI_call

        LCR       #_CIProcessNACKCmd    ; [CPU_] |254| 
        ; call occurs [#_CIProcessNACKCmd] ; [] |254| 
	.dwpsn	file "../source/CommInterface.c",line 259,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 259 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$92	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$92, DW_AT_low_pc(0x00)
	.dwattr $C$DW$92, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$92, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |259| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |259| 
	.dwpsn	file "../source/CommInterface.c",line 261,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 261 | break;                                                                 
; 263 | //!If the decoded data is the request for version command, send the    
; 264 | //!CPU1 and CPU2 version command                                       
; 265 | case SITARA_FIRMWARE_VERSION_CMD:                                      
; 267 |     //!Function is called to packetize the firmware version of the     
; 268 |     //!Controller                                                      
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |261| 
        ; branch occurs ; [] |261| 
$C$L6:    
	.dwpsn	file "../source/CommInterface.c",line 269,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 269 | CIProcessFirmwareVersionReq();                                         
; 271 | //!Busy pin is set if any sequence to be initiated.                    
; 272 | //!Since it is only processing and framing the Version command,        
; 273 | //!and no further sequence is initiated, clear the busy pin            
;----------------------------------------------------------------------
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("_CIProcessFirmwareVersionReq")
	.dwattr $C$DW$93, DW_AT_TI_call

        LCR       #_CIProcessFirmwareVersionReq ; [CPU_] |269| 
        ; call occurs [#_CIProcessFirmwareVersionReq] ; [] |269| 
	.dwpsn	file "../source/CommInterface.c",line 274,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 274 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$94, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |274| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |274| 
	.dwpsn	file "../source/CommInterface.c",line 277,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 277 | break;                                                                 
; 279 | //!If the decoded data is Calibration Handling command perform the acti
;     | on                                                                     
; 280 | //!with resspect to Calibration handling sequences                     
; 281 | case SITARA_CALIBRATION_HANDLING:                                      
; 283 | //!Calibration handling function is called by passing the minor command
; 284 | //!Calibration handling has different seqence: Auto Calibration,       
; 285 | //!Commercial Calibration WBC, Commercial Calibration RBC,             
; 286 | //!Commercial Calibration PLT                                          
; 287 | //!Calibration handling command is used for sending the results in     
; 288 | //!any of the above mentioned sequence                                 
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |277| 
        ; branch occurs ; [] |277| 
$C$L7:    
	.dwpsn	file "../source/CommInterface.c",line 289,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 289 | g_usnCurrentModeMajor = SITARA_CALIBRATION_HANDLING;                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4098 ; [CPU_] |289| 
	.dwpsn	file "../source/CommInterface.c",line 290,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 290 | CIProcessCalibrationHandlingCmd(usnMinorCmd);                          
; 292 | //!Since it is only processing command,                                
; 293 | //!and no further sequence is initiated, clear the busy pin            
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |290| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("_CIProcessCalibrationHandlingCmd")
	.dwattr $C$DW$95, DW_AT_TI_call

        LCR       #_CIProcessCalibrationHandlingCmd ; [CPU_] |290| 
        ; call occurs [#_CIProcessCalibrationHandlingCmd] ; [] |290| 
	.dwpsn	file "../source/CommInterface.c",line 294,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 294 | SPIClearCommCtrlBusyInt();                                             
; 295 | //!Enable the flag for ready for aspiration as soon as the trigger     
; 296 | //! button is pressed                                                  
;----------------------------------------------------------------------
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$96, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |294| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |294| 
	.dwpsn	file "../source/CommInterface.c",line 297,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 297 | m_bReadyforAspCommand = TRUE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_m_bReadyforAspCommand ; [CPU_U] 
        MOVB      @_m_bReadyforAspCommand,#1,UNC ; [CPU_] |297| 
	.dwpsn	file "../source/CommInterface.c",line 299,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 299 | break;                                                                 
; 301 | //!If the decoded data is Quality Handling command perform the action  
; 302 | //!with resspect to Quality handling sequences                         
; 303 | case SITARA_QUALITY_HANDLING:                                          
; 305 | //!Quality handling function is called by passing the minor command    
; 306 | //!Quality handling has different seqence: Quality Handling for        
; 307 | //!Whole blood and body fluid                                          
; 308 | //!Quality handling command is used for sending the results in         
; 309 | //!any of the above mentioned sequence                                 
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |299| 
        ; branch occurs ; [] |299| 
$C$L8:    
	.dwpsn	file "../source/CommInterface.c",line 310,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 310 | g_usnCurrentModeMajor = SITARA_QUALITY_HANDLING;                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4101 ; [CPU_] |310| 
	.dwpsn	file "../source/CommInterface.c",line 311,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 311 | CIProcessQualityHandlingCmd(usnMinorCmd);                              
; 313 | //!Since it is only processing command,                                
; 314 | //!and no further sequence is initiated, clear the busy pin            
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |311| 
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_name("_CIProcessQualityHandlingCmd")
	.dwattr $C$DW$97, DW_AT_TI_call

        LCR       #_CIProcessQualityHandlingCmd ; [CPU_] |311| 
        ; call occurs [#_CIProcessQualityHandlingCmd] ; [] |311| 
	.dwpsn	file "../source/CommInterface.c",line 315,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 315 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$98	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$98, DW_AT_low_pc(0x00)
	.dwattr $C$DW$98, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$98, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |315| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |315| 
	.dwpsn	file "../source/CommInterface.c",line 317,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 317 | break;                                                                 
; 319 | //!If the decoded data is Shutdown sequence perform the shutdown action
; 320 | case SITARA_SHUTDOWN_CMD:                                              
; 322 |     //!Shudown function is called to initiate shutdown sequence        
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |317| 
        ; branch occurs ; [] |317| 
$C$L9:    
	.dwpsn	file "../source/CommInterface.c",line 323,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 323 | g_usnCurrentModeMajor = SITARA_SHUTDOWN_CMD;                           
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4103 ; [CPU_] |323| 
	.dwpsn	file "../source/CommInterface.c",line 324,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 324 | CIProcessShutdownCmd(usnMinorCmd);                                     
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |324| 
$C$DW$99	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$99, DW_AT_low_pc(0x00)
	.dwattr $C$DW$99, DW_AT_name("_CIProcessShutdownCmd")
	.dwattr $C$DW$99, DW_AT_TI_call

        LCR       #_CIProcessShutdownCmd ; [CPU_] |324| 
        ; call occurs [#_CIProcessShutdownCmd] ; [] |324| 
	.dwpsn	file "../source/CommInterface.c",line 326,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 326 | break;                                                                 
; 328 | //!If the decoded data is to abort the the current running process,    
; 329 | //!Abort the sequence immediately                                      
; 330 | case SITARA_ABORT_MAIN_PROCESS_CMD:                                    
; 332 | //!Set Major command to MBD                                            
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |326| 
        ; branch occurs ; [] |326| 
$C$L10:    
	.dwpsn	file "../source/CommInterface.c",line 333,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 333 | g_usnCurrentModeMajor = SITARA_ABORT_MAIN_PROCESS_CMD;                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4102 ; [CPU_] |333| 
	.dwpsn	file "../source/CommInterface.c",line 335,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 335 | CIProcessAbortMainProcessCmd(usnMinorCmd);                             
; 337 | //!After aborting any sequence, controller is in idle condition.       
; 338 | //!Clear the busy pin                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |335| 
$C$DW$100	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$100, DW_AT_low_pc(0x00)
	.dwattr $C$DW$100, DW_AT_name("_CIProcessAbortMainProcessCmd")
	.dwattr $C$DW$100, DW_AT_TI_call

        LCR       #_CIProcessAbortMainProcessCmd ; [CPU_] |335| 
        ; call occurs [#_CIProcessAbortMainProcessCmd] ; [] |335| 
	.dwpsn	file "../source/CommInterface.c",line 339,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 339 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$101	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$101, DW_AT_low_pc(0x00)
	.dwattr $C$DW$101, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$101, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |339| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |339| 
	.dwpsn	file "../source/CommInterface.c",line 341,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 341 | break;                                                                 
; 343 | //!If the decoded data is System settings command, set the correspondin
;     | g                                                                      
; 344 | //!settings to the parameters                                          
; 345 | case SITARA_SETTINGS_CMD:                                              
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |341| 
        ; branch occurs ; [] |341| 
$C$L11:    
	.dwpsn	file "../source/CommInterface.c",line 347,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 347 | g_usnCurrentModeMajor = SITARA_SETTINGS_CMD;                           
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4104 ; [CPU_] |347| 
	.dwpsn	file "../source/CommInterface.c",line 348,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 348 | CIProcessSystemSettingsCmd(usnMinorCmd);                               
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |348| 
$C$DW$102	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$102, DW_AT_low_pc(0x00)
	.dwattr $C$DW$102, DW_AT_name("_CIProcessSystemSettingsCmd")
	.dwattr $C$DW$102, DW_AT_TI_call

        LCR       #_CIProcessSystemSettingsCmd ; [CPU_] |348| 
        ; call occurs [#_CIProcessSystemSettingsCmd] ; [] |348| 
	.dwpsn	file "../source/CommInterface.c",line 351,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 351 | break;                                                                 
; 353 | //!If the decoded data is Patient Handling command perform the action  
; 354 | //!with respect to patient handling sequences                          
; 355 | case SITARA_STARTUP_CMD:                                               
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |351| 
        ; branch occurs ; [] |351| 
$C$L12:    
	.dwpsn	file "../source/CommInterface.c",line 357,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 357 | g_bYMotorFaultMonEn = true;                                            
; 358 | //!Start monitoring the waste bin during start up                      
;----------------------------------------------------------------------
        MOVW      DP,#_g_bYMotorFaultMonEn ; [CPU_U] 
        MOVB      @_g_bYMotorFaultMonEn,#1,UNC ; [CPU_] |357| 
	.dwpsn	file "../source/CommInterface.c",line 359,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 359 | g_bWasteDetectCheck = TRUE;                                            
; 361 | //!Startup handling function is called by passing the minor command    
;----------------------------------------------------------------------
        MOVW      DP,#_g_bWasteDetectCheck ; [CPU_U] 
        MOVB      @_g_bWasteDetectCheck,#1,UNC ; [CPU_] |359| 
	.dwpsn	file "../source/CommInterface.c",line 362,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 362 | g_usnCurrentModeMajor = SITARA_STARTUP_CMD;                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4105 ; [CPU_] |362| 
	.dwpsn	file "../source/CommInterface.c",line 363,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 363 | CIProcessStartupHandlingCmd(usnMinorCmd);                              
; 365 | //!Since it is only processing command,                                
; 366 | //!and no further sequence is initiated, clear the busy pin            
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |363| 
$C$DW$103	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$103, DW_AT_low_pc(0x00)
	.dwattr $C$DW$103, DW_AT_name("_CIProcessStartupHandlingCmd")
	.dwattr $C$DW$103, DW_AT_TI_call

        LCR       #_CIProcessStartupHandlingCmd ; [CPU_] |363| 
        ; call occurs [#_CIProcessStartupHandlingCmd] ; [] |363| 
	.dwpsn	file "../source/CommInterface.c",line 367,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 367 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$104	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$104, DW_AT_low_pc(0x00)
	.dwattr $C$DW$104, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$104, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |367| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |367| 
	.dwpsn	file "../source/CommInterface.c",line 369,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 369 | break;                                                                 
; 371 | //!If the decoded data is Patient Handling command perform the action  
; 372 | //!with respect to patient handling sequences                          
; 373 | case SITARA_ERROR_CMD:                                                 
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |369| 
        ; branch occurs ; [] |369| 
$C$L13:    
	.dwpsn	file "../source/CommInterface.c",line 375,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 375 | CIProcessErrorCommand(usnMinorCmd);                                    
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |375| 
$C$DW$105	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$105, DW_AT_low_pc(0x00)
	.dwattr $C$DW$105, DW_AT_name("_CIProcessErrorCommand")
	.dwattr $C$DW$105, DW_AT_TI_call

        LCR       #_CIProcessErrorCommand ; [CPU_] |375| 
        ; call occurs [#_CIProcessErrorCommand] ; [] |375| 
	.dwpsn	file "../source/CommInterface.c",line 376,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 376 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$106, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |376| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |376| 
	.dwpsn	file "../source/CommInterface.c",line 378,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 378 | break;                                                                 
; 380 |         default:                                                       
; 382 |     //!If the proper command is received and no major command,         
; 383 |             //!Do not abort the current running sequence also          
; 384 |     //!TBD to clear the busy pin and do not initiate any sequence      
; 386 |             //SPIClearCommCtrlBusyInt();                               
; 388 |         break;                                                         
;----------------------------------------------------------------------
        B         $C$L18,UNC            ; [CPU_] |378| 
        ; branch occurs ; [] |378| 
$C$L14:    
	.dwpsn	file "../source/CommInterface.c",line 181,column 2,is_stmt,isa 0
        MOVZ      AR6,*-SP[1]           ; [CPU_] |181| 
        CMP       AR6,#4102             ; [CPU_] |181| 
        B         $C$L16,GT             ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        CMP       AR6,#4102             ; [CPU_] |181| 
        B         $C$L10,EQ             ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        CMP       AR6,#4099             ; [CPU_] |181| 
        B         $C$L15,GT             ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        CMP       AR6,#4099             ; [CPU_] |181| 
        B         $C$L6,EQ              ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        MOVZ      AR7,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#4096            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR7              ; [CPU_] |181| 
        B         $C$L1,EQ              ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        MOVZ      AR7,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#4097            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR7              ; [CPU_] |181| 
        B         $C$L2,EQ              ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        MOVZ      AR6,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#4098            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR6              ; [CPU_] |181| 
        B         $C$L7,EQ              ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        B         $C$L18,UNC            ; [CPU_] |181| 
        ; branch occurs ; [] |181| 
$C$L15:    
        MOVZ      AR7,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#4100            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR7              ; [CPU_] |181| 
        B         $C$L3,EQ              ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        MOVZ      AR6,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#4101            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR6              ; [CPU_] |181| 
        B         $C$L8,EQ              ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        B         $C$L18,UNC            ; [CPU_] |181| 
        ; branch occurs ; [] |181| 
$C$L16:    
        CMP       AR6,#4106             ; [CPU_] |181| 
        B         $C$L17,GT             ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        CMP       AR6,#4106             ; [CPU_] |181| 
        B         $C$L13,EQ             ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        MOVZ      AR7,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#4103            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR7              ; [CPU_] |181| 
        B         $C$L9,EQ              ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        MOVZ      AR7,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#4104            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR7              ; [CPU_] |181| 
        B         $C$L11,EQ             ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        MOVZ      AR6,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#4105            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR6              ; [CPU_] |181| 
        B         $C$L12,EQ             ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        B         $C$L18,UNC            ; [CPU_] |181| 
        ; branch occurs ; [] |181| 
$C$L17:    
        MOVZ      AR7,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#8190            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR7              ; [CPU_] |181| 
        B         $C$L5,EQ              ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        MOVZ      AR6,AR6               ; [CPU_] |181| 
        MOVL      XAR4,#8191            ; [CPU_U] |181| 
        MOVL      ACC,XAR4              ; [CPU_] |181| 
        CMPL      ACC,XAR6              ; [CPU_] |181| 
        B         $C$L4,EQ              ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        B         $C$L18,UNC            ; [CPU_] |181| 
        ; branch occurs ; [] |181| 
$C$L18:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$81, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$81, DW_AT_TI_end_line(0x186)
	.dwattr $C$DW$81, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$81

	.sect	".text:_CIProcessPatientHandlingCmd"
	.clink
	.global	_CIProcessPatientHandlingCmd

$C$DW$108	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$108, DW_AT_name("CIProcessPatientHandlingCmd")
	.dwattr $C$DW$108, DW_AT_low_pc(_CIProcessPatientHandlingCmd)
	.dwattr $C$DW$108, DW_AT_high_pc(0x00)
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_CIProcessPatientHandlingCmd")
	.dwattr $C$DW$108, DW_AT_external
	.dwattr $C$DW$108, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$108, DW_AT_TI_begin_line(0x18f)
	.dwattr $C$DW$108, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$108, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommInterface.c",line 400,column 1,is_stmt,address _CIProcessPatientHandlingCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessPatientHandlingCmd
$C$DW$109	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$109, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 399 | void CIProcessPatientHandlingCmd(uint16_t usnMinorCmd)                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessPatientHandlingCmd  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessPatientHandlingCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$110	.dwtag  DW_TAG_variable
	.dwattr $C$DW$110, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_breg20 -5]

        MOV       *-SP[5],AL            ; [CPU_] |400| 
	.dwpsn	file "../source/CommInterface.c",line 401,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 401 | m_bReadyforAspCommand = TRUE;                                          
; 402 | //    sprintf(DebugPrintBuf, "\r\n CIProcessPatientHandlingCmd %d\n\n",
;     |  usnMinorCmd);                                                         
; 403 | //    UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 404 | //!Check for the minor command of the command received from the process
;     | or                                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_bReadyforAspCommand ; [CPU_U] 
        MOVB      @_m_bReadyforAspCommand,#1,UNC ; [CPU_] |401| 
	.dwpsn	file "../source/CommInterface.c",line 405,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 405 | switch (usnMinorCmd)                                                   
; 407 |     //!If the command received from processor is Whole blood mode,     
; 408 |     //!perform the below operations                                    
; 409 |         case SITARA_WHOLE_BLOOD_WBC_HGB:                               
; 410 |         case SITARA_WHOLE_BLOOD_RBC_PLT:                               
; 411 |         case SITARA_WHOLE_BLOOD_RBC_WBC_PLT:                           
; 413 |             //!if the previous sequence is pre-dilunet and we changed t
;     | o whole blood mode                                                     
; 414 |             //! ReSet the g_bLowVolumeDisp                             
;----------------------------------------------------------------------
        B         $C$L37,UNC            ; [CPU_] |405| 
        ; branch occurs ; [] |405| 
$C$L19:    
	.dwpsn	file "../source/CommInterface.c",line 415,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 415 | g_bLowVolumeDisp = FALSE;                                              
; 416 | //!Re-initilaise the major variables releated to new sequence          
; 417 |     //!starting                                                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_bLowVolumeDisp ; [CPU_U] 
        MOV       @_g_bLowVolumeDisp,#0 ; [CPU_] |415| 
	.dwpsn	file "../source/CommInterface.c",line 418,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 418 | CIReInitialiseOnNewMeasurement();                                      
; 420 | //!Set the current mode to Whole Blood mode which will be given        
; 421 | //!input to MBD to initiate Whole blood                                
;----------------------------------------------------------------------
$C$DW$111	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$111, DW_AT_low_pc(0x00)
	.dwattr $C$DW$111, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$111, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |418| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |418| 
	.dwpsn	file "../source/CommInterface.c",line 422,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 422 | g_usnCurrentMode = WHOLE_BLOOD_MODE;                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20487 ; [CPU_] |422| 
	.dwpsn	file "../source/CommInterface.c",line 423,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 423 | sprintf(DebugPrintBuf, "\r\n WHOLE_BLOOD_MODE\n");                     
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL1         ; [CPU_U] |423| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |423| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |423| 
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_name("_sprintf")
	.dwattr $C$DW$112, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |423| 
        ; call occurs [#_sprintf] ; [] |423| 
	.dwpsn	file "../source/CommInterface.c",line 424,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 424 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |424| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |424| 
$C$DW$113	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$113, DW_AT_low_pc(0x00)
	.dwattr $C$DW$113, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$113, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |424| 
        ; call occurs [#_UartDebugPrint] ; [] |424| 
	.dwpsn	file "../source/CommInterface.c",line 426,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 426 | break;                                                                 
; 428 | //!If the command received from processor is Pre-dilute mode,          
; 429 | //!perform the pre-dilute mode                                         
; 430 | case SITARA_PRE_DILUENT_MODE:                                          
; 431 |     //!Reset the pop-up cmd                                            
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |426| 
        ; branch occurs ; [] |426| 
$C$L20:    
	.dwpsn	file "../source/CommInterface.c",line 432,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 432 | g_usnCurrentModeTemp = ZERO;//HN_added 2/12/2020                       
; 433 | //!Re-initilaise the major variables releated to new sequence          
; 434 | //!starting                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeTemp ; [CPU_U] 
        MOV       @_g_usnCurrentModeTemp,#0 ; [CPU_] |432| 
	.dwpsn	file "../source/CommInterface.c",line 435,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 435 | CIReInitialiseOnNewMeasurement();                                      
; 436 | //!Set the current mode to Pre-dilute mode which will be given         
; 437 | //!input to MBD to initiate Pre-dilute                                 
;----------------------------------------------------------------------
$C$DW$114	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$114, DW_AT_low_pc(0x00)
	.dwattr $C$DW$114, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$114, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |435| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |435| 
	.dwpsn	file "../source/CommInterface.c",line 438,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 438 | g_usnCurrentMode = PRE_DILUENT_MODE;                                   
; 439 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20495 ; [CPU_] |438| 
	.dwpsn	file "../source/CommInterface.c",line 440,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 440 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$115	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$115, DW_AT_low_pc(0x00)
	.dwattr $C$DW$115, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$115, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |440| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |440| 
	.dwpsn	file "../source/CommInterface.c",line 442,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 442 | break;                                                                 
; 444 | //!If the command received from processor is body fluid mode,          
; 445 | case SITARA_BODY_FLUID:                                                
; 446 |     //!perform the body fluid                                          
; 448 |     //!if the previous sequence is pre-dilunet and we changed to whole
;     | blood mode                                                             
; 449 | //! ReSet the g_bLowVolumeDisp                                         
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |442| 
        ; branch occurs ; [] |442| 
$C$L21:    
	.dwpsn	file "../source/CommInterface.c",line 450,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 450 | g_bLowVolumeDisp = FALSE;                                              
; 453 | //!Re-initilaise the major variables releated to new sequence          
; 454 | //!starting                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_bLowVolumeDisp ; [CPU_U] 
        MOV       @_g_bLowVolumeDisp,#0 ; [CPU_] |450| 
	.dwpsn	file "../source/CommInterface.c",line 455,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 455 | CIReInitialiseOnNewMeasurement();                                      
; 456 | //!Set the current mode to Body Fluid mode which will be given         
; 457 | //!input to MBD to initiate Pre-dilute                                 
;----------------------------------------------------------------------
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$116, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |455| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |455| 
	.dwpsn	file "../source/CommInterface.c",line 458,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 458 | g_usnCurrentMode = BODY_FLUID_MODE;                                    
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20498 ; [CPU_] |458| 
	.dwpsn	file "../source/CommInterface.c",line 460,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 460 | break;                                                                 
; 462 | //!If the command received from processor is to request WBC pulse heigh
;     | t,                                                                     
; 463 | //!Frame the packet for WBC pulse height and send it to processor      
; 464 | case SITARA_WB_WBC_PULSE_HEIGHT_CMD:                                   
; 465 |     //!Set the cell type to WBC so that the packet framed shall be for 
; 466 |     //!WBC Pulse height                                                
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |460| 
        ; branch occurs ; [] |460| 
$C$L22:    
	.dwpsn	file "../source/CommInterface.c",line 467,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 467 | m_eTypeofCell = eCellWBC;                                              
; 468 | //!Frame the packet for Patient handling: WBC pulse height and         
; 469 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |467| 
	.dwpsn	file "../source/CommInterface.c",line 470,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 470 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\                           
; 471 |         DELFINO_WBC_PULSE_HEIGHT_DATA, FALSE);                         
; 472 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8193              ; [CPU_] |470| 
        MOVB      XAR4,#0               ; [CPU_] |470| 
        MOV       AH,#8193              ; [CPU_] |470| 
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$117, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |470| 
        ; call occurs [#_CPFrameDataPacket] ; [] |470| 
	.dwpsn	file "../source/CommInterface.c",line 473,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 473 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$118, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |473| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |473| 
	.dwpsn	file "../source/CommInterface.c",line 474,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 474 | break;                                                                 
; 476 | //!If the command received from processor is to request RBC pulse heigh
;     | t,                                                                     
; 477 | //!Frame the packet for RBC pulse height and send it to processor      
; 478 | case SITARA_WB_RBC_PULSE_HEIGHT_CMD:                                   
; 479 | //!Set the cell type to RBC so that the packet framed shall be for     
; 480 | //!RBC Pulse height                                                    
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |474| 
        ; branch occurs ; [] |474| 
$C$L23:    
	.dwpsn	file "../source/CommInterface.c",line 481,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 481 | m_eTypeofCell = eCellRBC;                                              
; 482 | //!Frame the packet for Patient handling: RBC pulse height and         
; 483 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |481| 
	.dwpsn	file "../source/CommInterface.c",line 484,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 484 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\                           
; 485 |         DELFINO_RBC_PULSE_HEIGHT_DATA, FALSE);                         
; 486 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8193              ; [CPU_] |484| 
        MOVB      XAR4,#0               ; [CPU_] |484| 
        MOV       AH,#8194              ; [CPU_] |484| 
$C$DW$119	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$119, DW_AT_low_pc(0x00)
	.dwattr $C$DW$119, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$119, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |484| 
        ; call occurs [#_CPFrameDataPacket] ; [] |484| 
	.dwpsn	file "../source/CommInterface.c",line 487,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 487 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$120, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |487| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |487| 
	.dwpsn	file "../source/CommInterface.c",line 488,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 488 | break;                                                                 
; 490 | //!If the command received from processor is to request PLT pulse heigh
;     | t,                                                                     
; 491 | //!Frame the packet for PLT pulse height and send it to processor      
; 492 | case SITARA_WB_PLT_PULSE_HEIGHT_CMD:                                   
; 493 | //!Set the cell type to PLT so that the packet framed shall be for     
; 494 | //!PLT Pulse height                                                    
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |488| 
        ; branch occurs ; [] |488| 
$C$L24:    
	.dwpsn	file "../source/CommInterface.c",line 495,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 495 | m_eTypeofCell = eCellPLT;                                              
; 496 | //!Frame the packet for Patient handling: PLT pulse height and         
; 497 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |495| 
	.dwpsn	file "../source/CommInterface.c",line 498,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 498 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\                           
; 499 |         DELFINO_PLT_PULSE_HEIGHT_DATA, FALSE);                         
; 500 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8193              ; [CPU_] |498| 
        MOVB      XAR4,#0               ; [CPU_] |498| 
        MOV       AH,#8195              ; [CPU_] |498| 
$C$DW$121	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$121, DW_AT_low_pc(0x00)
	.dwattr $C$DW$121, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$121, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |498| 
        ; call occurs [#_CPFrameDataPacket] ; [] |498| 
	.dwpsn	file "../source/CommInterface.c",line 501,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 501 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$122, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |501| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |501| 
	.dwpsn	file "../source/CommInterface.c",line 502,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 502 | break;                                                                 
; 504 | //!If the command received from processor is to request 1st second of  
; 505 | //!WBC Raw Data, frame the packet for WBC Raw Data and send it to proce
;     | ssor                                                                   
; 506 | case SITARA_WBC_RAW_DATA_1SEC:                                         
; 507 | //!Set the cell type to WBC so that the packet framed shall be for     
; 508 | //!WBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |502| 
        ; branch occurs ; [] |502| 
$C$L25:    
	.dwpsn	file "../source/CommInterface.c",line 509,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 509 | m_eTypeofCell = eCellWBC;                                              
; 510 | //!Frame the packet for Patient handling: WBC Raw data and             
; 511 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |509| 
	.dwpsn	file "../source/CommInterface.c",line 512,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 512 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\                           
; 513 |         DELFINO_WBC_ADC_RAW_DATA, FALSE);                              
; 514 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8193              ; [CPU_] |512| 
        MOVB      XAR4,#0               ; [CPU_] |512| 
        MOV       AH,#8198              ; [CPU_] |512| 
$C$DW$123	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$123, DW_AT_low_pc(0x00)
	.dwattr $C$DW$123, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$123, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |512| 
        ; call occurs [#_CPFrameDataPacket] ; [] |512| 
	.dwpsn	file "../source/CommInterface.c",line 515,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 515 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$124, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |515| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |515| 
	.dwpsn	file "../source/CommInterface.c",line 516,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 516 | break;                                                                 
; 518 | //!If the command received from processor is to request 1st second of  
; 519 | //!RBC Raw Data, frame the packet for RBC Raw Data and send it to proce
;     | ssor                                                                   
; 520 | case SITARA_RBC_RAW_DATA_1SEC:                                         
; 521 | //!Set the cell type to RBC so that the packet framed shall be for     
; 522 | //!RBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |516| 
        ; branch occurs ; [] |516| 
$C$L26:    
	.dwpsn	file "../source/CommInterface.c",line 523,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 523 | m_eTypeofCell = eCellRBC;                                              
; 524 | //!Frame the packet for Patient handling: RBC Raw data and             
; 525 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |523| 
	.dwpsn	file "../source/CommInterface.c",line 526,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 526 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\                           
; 527 |         DELFINO_RBC_ADC_RAW_DATA, FALSE);                              
; 528 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8193              ; [CPU_] |526| 
        MOVB      XAR4,#0               ; [CPU_] |526| 
        MOV       AH,#8199              ; [CPU_] |526| 
$C$DW$125	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$125, DW_AT_low_pc(0x00)
	.dwattr $C$DW$125, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$125, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |526| 
        ; call occurs [#_CPFrameDataPacket] ; [] |526| 
	.dwpsn	file "../source/CommInterface.c",line 529,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 529 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$126	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$126, DW_AT_low_pc(0x00)
	.dwattr $C$DW$126, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$126, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |529| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |529| 
	.dwpsn	file "../source/CommInterface.c",line 530,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 530 | break;                                                                 
; 532 | //!If the command received from processor is to request 1st second of  
; 533 | //!PLT Raw Data, frame the packet for PLT Raw Data and send it to proce
;     | ssor                                                                   
; 534 | case SITARA_PLT_RAW_DATA_1SEC:                                         
; 535 | //!Set the cell type to PLT so that the packet framed shall be for     
; 536 | //!PLT Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |530| 
        ; branch occurs ; [] |530| 
$C$L27:    
	.dwpsn	file "../source/CommInterface.c",line 537,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 537 | m_eTypeofCell = eCellPLT;                                              
; 538 | //!Frame the packet for Patient handling: PLT Raw data and             
; 539 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |537| 
	.dwpsn	file "../source/CommInterface.c",line 540,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 540 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\                           
; 541 |         DELFINO_PLT_ADC_RAW_DATA, FALSE);                              
; 542 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8193              ; [CPU_] |540| 
        MOVB      XAR4,#0               ; [CPU_] |540| 
        MOV       AH,#8200              ; [CPU_] |540| 
$C$DW$127	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$127, DW_AT_low_pc(0x00)
	.dwattr $C$DW$127, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$127, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |540| 
        ; call occurs [#_CPFrameDataPacket] ; [] |540| 
	.dwpsn	file "../source/CommInterface.c",line 543,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 543 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$128	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$128, DW_AT_low_pc(0x00)
	.dwattr $C$DW$128, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$128, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |543| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |543| 
	.dwpsn	file "../source/CommInterface.c",line 544,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 544 | break;                                                                 
; 546 | //!If the command received from processor is for dispensing the diluent
; 547 | //!in predilute mode, input the command to the MBD to dispense the dilu
;     | ent                                                                    
; 548 | case SITARA_START_DISPENSE_CMD:                                        
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |544| 
        ; branch occurs ; [] |544| 
$C$L28:    
	.dwpsn	file "../source/CommInterface.c",line 549,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 549 | sprintf(DebugPrintBuf, "\r\n SITARA_START_DISPENSE_CMD\n");            
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL2         ; [CPU_U] |549| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |549| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |549| 
$C$DW$129	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$129, DW_AT_low_pc(0x00)
	.dwattr $C$DW$129, DW_AT_name("_sprintf")
	.dwattr $C$DW$129, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |549| 
        ; call occurs [#_sprintf] ; [] |549| 
	.dwpsn	file "../source/CommInterface.c",line 550,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 550 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |550| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |550| 
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$130, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |550| 
        ; call occurs [#_UartDebugPrint] ; [] |550| 
	.dwpsn	file "../source/CommInterface.c",line 551,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 551 | g_usnCurrentModeTemp = DISPENSE_THE_DILUENT;                           
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeTemp ; [CPU_U] 
        MOV       @_g_usnCurrentModeTemp,#20496 ; [CPU_] |551| 
	.dwpsn	file "../source/CommInterface.c",line 552,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 552 | break;                                                                 
; 554 | //!If the command received from processor is to continue the pre-diluen
;     | t                                                                      
; 555 | //!after dipensing the diluent to the blood sample tube,               
; 556 | //!input the command to the MBD to dispense the diluent                
; 557 | case SITARA_START_PRE_DILUENT_COUNTING_CMD:                            
; 558 |         //!Re-initilaize the variables and start proceed with pre-dilue
;     | nt                                                                     
; 559 |         //!mode after mixing with the blood sample                     
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |552| 
        ; branch occurs ; [] |552| 
$C$L29:    
	.dwpsn	file "../source/CommInterface.c",line 560,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 560 | CIReInitialiseOnNewMeasurement();                                      
; 561 | //!Set the secondary mode (Temp mode) to Pre-Diluent Mode              
; 562 | //!this mode is the secondary input to Pre-Diluent mode whcih is       
; 563 | //!continued after the initial pre-diluent mode                        
;----------------------------------------------------------------------
$C$DW$131	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$131, DW_AT_low_pc(0x00)
	.dwattr $C$DW$131, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$131, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |560| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |560| 
	.dwpsn	file "../source/CommInterface.c",line 564,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 564 | g_usnCurrentModeTemp = START_PRE_DILUENT_COUNTING_CMD;                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeTemp ; [CPU_U] 
        MOV       @_g_usnCurrentModeTemp,#20497 ; [CPU_] |564| 
	.dwpsn	file "../source/CommInterface.c",line 565,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 565 | break;                                                                 
; 567 | //!If the command received from processor is to request next second of 
; 568 | //!WBC Raw Data, frame the packet for WBC Raw Data and send it to proce
;     | ssor                                                                   
; 569 | case SITARA_WBC_RAW_DATA_NEXT_SEC:                                     
; 570 | //!Set the cell type to WBC so that the packet framed shall be for     
; 571 | //!WBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |565| 
        ; branch occurs ; [] |565| 
$C$L30:    
	.dwpsn	file "../source/CommInterface.c",line 572,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 572 | m_eTypeofCell = eCellWBC;                                              
; 573 | //!Raw data is acquired for total acquisition time of WBC, RBC & PLT   
; 574 | //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
; 575 | //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be 
; 576 | //!transmitted to processor. It is transmitted in 1 second each as requ
;     | ested                                                                  
; 577 | //!by processor.                                                       
; 578 | //!So next second data is packetized by enabling the variable          
; 579 | //!g_bNextSecRawDataReq                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |572| 
	.dwpsn	file "../source/CommInterface.c",line 580,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 580 | g_bNextSecRawDataReq = TRUE;                                           
; 581 | //!Frame the packet for Patient handling: WBC Raw data and             
; 582 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      @_g_bNextSecRawDataReq,#1,UNC ; [CPU_] |580| 
	.dwpsn	file "../source/CommInterface.c",line 583,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 583 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\                           
; 584 |         DELFINO_WBC_ADC_RAW_DATA, FALSE);                              
; 585 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8193              ; [CPU_] |583| 
        MOVB      XAR4,#0               ; [CPU_] |583| 
        MOV       AH,#8198              ; [CPU_] |583| 
$C$DW$132	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$132, DW_AT_low_pc(0x00)
	.dwattr $C$DW$132, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$132, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |583| 
        ; call occurs [#_CPFrameDataPacket] ; [] |583| 
	.dwpsn	file "../source/CommInterface.c",line 586,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 586 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$133	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$133, DW_AT_low_pc(0x00)
	.dwattr $C$DW$133, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$133, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |586| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |586| 
	.dwpsn	file "../source/CommInterface.c",line 587,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 587 | break;                                                                 
; 589 | //!If the command received from processor is to request next second of 
; 590 | //!RBC Raw Data, frame the packet for RBC Raw Data and send it to proce
;     | ssor                                                                   
; 591 | case SITARA_RBC_RAW_DATA_NEXT_SEC:                                     
; 592 | //!Set the cell type to RBC so that the packet framed shall be for     
; 593 | //!RBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |587| 
        ; branch occurs ; [] |587| 
$C$L31:    
	.dwpsn	file "../source/CommInterface.c",line 594,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 594 | m_eTypeofCell = eCellRBC;                                              
; 595 | //!Raw data is acquired for total acquisition time of WBC, RBC & PLT   
; 596 | //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
; 597 | //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be 
; 598 | //!transmitted to processor. It is transmitted in 1 second each as requ
;     | ested                                                                  
; 599 | //!by processor.                                                       
; 600 | //!So next second data is packetized by enabling the variable          
; 601 | //!g_bNextSecRawDataReq                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |594| 
	.dwpsn	file "../source/CommInterface.c",line 602,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 602 | g_bNextSecRawDataReq = TRUE;                                           
; 603 | //!Frame the packet for Patient handling: RBC Raw data and             
; 604 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      @_g_bNextSecRawDataReq,#1,UNC ; [CPU_] |602| 
	.dwpsn	file "../source/CommInterface.c",line 605,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 605 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\                           
; 606 |         DELFINO_RBC_ADC_RAW_DATA, FALSE);                              
; 607 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8193              ; [CPU_] |605| 
        MOVB      XAR4,#0               ; [CPU_] |605| 
        MOV       AH,#8199              ; [CPU_] |605| 
$C$DW$134	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$134, DW_AT_low_pc(0x00)
	.dwattr $C$DW$134, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$134, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |605| 
        ; call occurs [#_CPFrameDataPacket] ; [] |605| 
	.dwpsn	file "../source/CommInterface.c",line 608,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 608 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$135	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$135, DW_AT_low_pc(0x00)
	.dwattr $C$DW$135, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$135, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |608| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |608| 
	.dwpsn	file "../source/CommInterface.c",line 609,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 609 | break;                                                                 
; 611 | //!If the command received from processor is to request next second of 
; 612 | //!PLT Raw Data, frame the packet for PLT Raw Data and send it to proce
;     | ssor                                                                   
; 613 | case SITARA_PLT_RAW_DATA_NEXT_SEC:                                     
; 614 | //!Set the cell type to PLT so that the packet framed shall be for     
; 615 | //!PLT Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |609| 
        ; branch occurs ; [] |609| 
$C$L32:    
	.dwpsn	file "../source/CommInterface.c",line 616,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 616 | m_eTypeofCell = eCellPLT;                                              
; 617 | //!Raw data is acquired for total acquisition time of WBC, RBC & PLT   
; 618 | //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
; 619 | //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be 
; 620 | //!transmitted to processor. It is transmitted in 1 second each as requ
;     | ested                                                                  
; 621 | //!by processor.                                                       
; 622 | //!So next second data is packetized by enabling the variable          
; 623 | //!g_bNextSecRawDataReq                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |616| 
	.dwpsn	file "../source/CommInterface.c",line 624,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 624 | g_bNextSecRawDataReq = TRUE;                                           
; 625 | //!Frame the packet for Patient handling: PLT Raw data and             
; 626 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      @_g_bNextSecRawDataReq,#1,UNC ; [CPU_] |624| 
	.dwpsn	file "../source/CommInterface.c",line 627,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 627 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING,\                           
; 628 |         DELFINO_PLT_ADC_RAW_DATA, FALSE);                              
; 629 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8193              ; [CPU_] |627| 
        MOVB      XAR4,#0               ; [CPU_] |627| 
        MOV       AH,#8200              ; [CPU_] |627| 
$C$DW$136	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$136, DW_AT_low_pc(0x00)
	.dwattr $C$DW$136, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$136, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |627| 
        ; call occurs [#_CPFrameDataPacket] ; [] |627| 
	.dwpsn	file "../source/CommInterface.c",line 630,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 630 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$137	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$137, DW_AT_low_pc(0x00)
	.dwattr $C$DW$137, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$137, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |630| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |630| 
	.dwpsn	file "../source/CommInterface.c",line 631,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 631 | break;                                                                 
; 633 | //!If the command received from processor is to enable the start button
; 634 | //!polling, enable the start button polling                            
; 635 | case SITARA_START_POLLING_START_BUTTON_CMD:                            
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |631| 
        ; branch occurs ; [] |631| 
$C$L33:    
	.dwpsn	file "../source/CommInterface.c",line 636,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 636 | sprintf(DebugPrintBuf, "\n SITARA_START_POLLING_START_BUTTON_CMD\n");  
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL3         ; [CPU_U] |636| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |636| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |636| 
$C$DW$138	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$138, DW_AT_low_pc(0x00)
	.dwattr $C$DW$138, DW_AT_name("_sprintf")
	.dwattr $C$DW$138, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |636| 
        ; call occurs [#_sprintf] ; [] |636| 
	.dwpsn	file "../source/CommInterface.c",line 637,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 637 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 638 | //!Set the cell type to invalid when initiating any seqence or         
; 639 | //!during the polling of start button                                  
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |637| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |637| 
$C$DW$139	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$139, DW_AT_low_pc(0x00)
	.dwattr $C$DW$139, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$139, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |637| 
        ; call occurs [#_UartDebugPrint] ; [] |637| 
	.dwpsn	file "../source/CommInterface.c",line 640,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 640 | m_eTypeofCell = ecellInvalid;                                          
; 641 | //!To enable start button for next measurement                         
; 642 | //g_bStartButtonStatus = TRUE;                                         
; 643 | //!Clear the busy pin, so that process shall send any command for      
; 644 | //!further sequence initiation                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOV       @_m_eTypeofCell,#0    ; [CPU_] |640| 
	.dwpsn	file "../source/CommInterface.c",line 645,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 645 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$140	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$140, DW_AT_low_pc(0x00)
	.dwattr $C$DW$140, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$140, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |645| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |645| 
	.dwpsn	file "../source/CommInterface.c",line 646,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 646 | break;                                                                 
; 648 | //!If the command received from processor is to disable the start butto
;     | n                                                                      
; 649 | //!polling, disable the start button polling                           
; 650 | case SITARA_STOP_POLLING_START_BUTTON_CMD:                             
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |646| 
        ; branch occurs ; [] |646| 
$C$L34:    
	.dwpsn	file "../source/CommInterface.c",line 651,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 651 | sprintf(DebugPrintBuf, "\n SITARA_STOP_POLLING_START_BUTTON_CMD\n");   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL4         ; [CPU_U] |651| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |651| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |651| 
$C$DW$141	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$141, DW_AT_low_pc(0x00)
	.dwattr $C$DW$141, DW_AT_name("_sprintf")
	.dwattr $C$DW$141, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |651| 
        ; call occurs [#_sprintf] ; [] |651| 
	.dwpsn	file "../source/CommInterface.c",line 652,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 652 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 653 | //!Set the cell type to invalid when initiating any seqence or         
; 654 | //!during the polling of start button                                  
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |652| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |652| 
$C$DW$142	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$142, DW_AT_low_pc(0x00)
	.dwattr $C$DW$142, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$142, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |652| 
        ; call occurs [#_UartDebugPrint] ; [] |652| 
	.dwpsn	file "../source/CommInterface.c",line 655,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 655 | m_eTypeofCell = ecellInvalid;                                          
; 656 | //!To disable start button for next measurement                        
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOV       @_m_eTypeofCell,#0    ; [CPU_] |655| 
	.dwpsn	file "../source/CommInterface.c",line 657,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 657 | g_bStartButtonStatus = FALSE;                                          
; 658 | //!Clear the busy pin, so that process shall send any command for      
; 659 | //!further sequence initiation                                         
;----------------------------------------------------------------------
        MOV       @_g_bStartButtonStatus,#0 ; [CPU_] |657| 
	.dwpsn	file "../source/CommInterface.c",line 660,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 660 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$143	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$143, DW_AT_low_pc(0x00)
	.dwattr $C$DW$143, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$143, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |660| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |660| 
	.dwpsn	file "../source/CommInterface.c",line 661,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 661 | break;                                                                 
; 663 | //!If the command received from processor is to disable the start butto
;     | n                                                                      
; 664 | //!polling, disable the start button polling                           
; 665 | case SITARA_PULSE_HEIGHT_DATA_RECEIVED:                                
; 666 | //m_eCycleCompletion = eERROR_CHECK;                                   
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |661| 
        ; branch occurs ; [] |661| 
$C$L35:    
	.dwpsn	file "../source/CommInterface.c",line 667,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 667 | g_bDataTransmitted = TRUE;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bDataTransmitted ; [CPU_U] 
        MOVB      @_g_bDataTransmitted,#1,UNC ; [CPU_] |667| 
	.dwpsn	file "../source/CommInterface.c",line 668,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 668 | SPISetCommCtrlBusyInt();                                               
;----------------------------------------------------------------------
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$144, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |668| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |668| 
	.dwpsn	file "../source/CommInterface.c",line 669,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 669 | sprintf(DebugPrintBuf, "\r\n PULSE_HEIGHT_DATA_received at %ld\n",g_unS
;     | equenceStateTime);                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_g_unSequenceStateTime ; [CPU_U] 
        MOVL      XAR4,#$C$FSL5         ; [CPU_U] |669| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |669| 
        MOVL      ACC,@_g_unSequenceStateTime ; [CPU_] |669| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |669| 
        MOVL      *-SP[4],ACC           ; [CPU_] |669| 
$C$DW$145	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$145, DW_AT_low_pc(0x00)
	.dwattr $C$DW$145, DW_AT_name("_sprintf")
	.dwattr $C$DW$145, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |669| 
        ; call occurs [#_sprintf] ; [] |669| 
	.dwpsn	file "../source/CommInterface.c",line 670,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 670 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |670| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |670| 
$C$DW$146	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$146, DW_AT_low_pc(0x00)
	.dwattr $C$DW$146, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$146, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |670| 
        ; call occurs [#_UartDebugPrint] ; [] |670| 
	.dwpsn	file "../source/CommInterface.c",line 672,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 672 | m_bTxPendingFlag = FALSE;                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bTxPendingFlag ; [CPU_U] 
        MOV       @_m_bTxPendingFlag,#0 ; [CPU_] |672| 
	.dwpsn	file "../source/CommInterface.c",line 673,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 673 | break;                                                                 
; 675 | //!If the minor command is not valid, do nothing and set the cell type 
; 676 | //!to invalid                                                          
; 677 |         default:                                                       
; 678 |             //!Cell type is set to invalid when the minor command is in
;     | valid                                                                  
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |673| 
        ; branch occurs ; [] |673| 
$C$L36:    
	.dwpsn	file "../source/CommInterface.c",line 679,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 679 | m_eTypeofCell = ecellInvalid;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOV       @_m_eTypeofCell,#0    ; [CPU_] |679| 
	.dwpsn	file "../source/CommInterface.c",line 680,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 680 | break;                                                                 
; 681 | }//end of switch case                                                  
;----------------------------------------------------------------------
        B         $C$L38,UNC            ; [CPU_] |680| 
        ; branch occurs ; [] |680| 
$C$L37:    
	.dwpsn	file "../source/CommInterface.c",line 405,column 2,is_stmt,isa 0
        MOV       AH,*-SP[5]            ; [CPU_] |405| 
        SUB       AL,#4097              ; [CPU_] |405| 
        CMPB      AL,#18                ; [CPU_] |405| 
        B         $C$L36,HI             ; [CPU_] |405| 
        ; branchcc occurs ; [] |405| 
        MOV       AL,AH                 ; [CPU_] |405| 
        SUB       AL,#4097              ; [CPU_] |405| 
        MOV       ACC,AL << #1          ; [CPU_] |405| 
        MOVZ      AR6,AL                ; [CPU_] |405| 
        MOVL      XAR7,#$C$SW1          ; [CPU_U] |405| 
        MOVL      ACC,XAR7              ; [CPU_] |405| 
        ADDU      ACC,AR6               ; [CPU_] |405| 
        MOVL      XAR7,ACC              ; [CPU_] |405| 
        MOVL      XAR7,*XAR7            ; [CPU_] |405| 
        LB        *XAR7                 ; [CPU_] |405| 
        ; branch occurs ; [] |405| 
	.sect	".switch:_CIProcessPatientHandlingCmd"
	.clink
$C$SW1:	.long	$C$L19	; 4097
	.long	$C$L19	; 4098
	.long	$C$L19	; 4099
	.long	$C$L20	; 4100
	.long	$C$L21	; 4101
	.long	$C$L22	; 4102
	.long	$C$L23	; 4103
	.long	$C$L24	; 4104
	.long	$C$L25	; 4105
	.long	$C$L28	; 4106
	.long	$C$L29	; 4107
	.long	$C$L30	; 4108
	.long	$C$L31	; 4109
	.long	$C$L32	; 4110
	.long	$C$L33	; 4111
	.long	$C$L26	; 4112
	.long	$C$L27	; 4113
	.long	$C$L34	; 4114
	.long	$C$L35	; 4115
	.sect	".text:_CIProcessPatientHandlingCmd"
$C$L38:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$147	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$147, DW_AT_low_pc(0x00)
	.dwattr $C$DW$147, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$108, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$108, DW_AT_TI_end_line(0x2aa)
	.dwattr $C$DW$108, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$108

	.sect	".text:_CIProcessACKCmd"
	.clink
	.global	_CIProcessACKCmd

$C$DW$148	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$148, DW_AT_name("CIProcessACKCmd")
	.dwattr $C$DW$148, DW_AT_low_pc(_CIProcessACKCmd)
	.dwattr $C$DW$148, DW_AT_high_pc(0x00)
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_CIProcessACKCmd")
	.dwattr $C$DW$148, DW_AT_external
	.dwattr $C$DW$148, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$148, DW_AT_TI_begin_line(0x2bc)
	.dwattr $C$DW$148, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$148, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 701,column 1,is_stmt,address _CIProcessACKCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessACKCmd
$C$DW$149	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$149, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$149, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 700 | void CIProcessACKCmd(uint16_t usnMinorCmd)                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessACKCmd              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessACKCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$150	.dwtag  DW_TAG_variable
	.dwattr $C$DW$150, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$150, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 702 | //!If the minor command is Acknowledgment command                      
; 703 | //!Set the NAK received is False, so nothing to be processed and set th
;     | e packet                                                               
; 704 | //!Count to Zero, so that no further packet to be transmitted to proces
;     | sor                                                                    
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |701| 
	.dwpsn	file "../source/CommInterface.c",line 705,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 705 | if(usnMinorCmd == SITARA_ACK_SUB_CMD)                                  
;----------------------------------------------------------------------
        MOV       AL,#8188              ; [CPU_] |705| 
        CMP       AL,*-SP[1]            ; [CPU_] |705| 
        B         $C$L39,NEQ            ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
	.dwpsn	file "../source/CommInterface.c",line 707,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 707 | m_bNAckReceived = FALSE;                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOV       @_m_bNAckReceived,#0  ; [CPU_] |707| 
$C$L39:    
	.dwpsn	file "../source/CommInterface.c",line 709,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 709 | m_usnPacketCount = 0;                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_usnPacketCount ; [CPU_U] 
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |709| 
	.dwpsn	file "../source/CommInterface.c",line 710,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$151	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$151, DW_AT_low_pc(0x00)
	.dwattr $C$DW$151, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$148, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$148, DW_AT_TI_end_line(0x2c6)
	.dwattr $C$DW$148, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$148

	.sect	".text:_CIProcessNACKCmd"
	.clink
	.global	_CIProcessNACKCmd

$C$DW$152	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$152, DW_AT_name("CIProcessNACKCmd")
	.dwattr $C$DW$152, DW_AT_low_pc(_CIProcessNACKCmd)
	.dwattr $C$DW$152, DW_AT_high_pc(0x00)
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_CIProcessNACKCmd")
	.dwattr $C$DW$152, DW_AT_external
	.dwattr $C$DW$152, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$152, DW_AT_TI_begin_line(0x2cf)
	.dwattr $C$DW$152, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$152, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommInterface.c",line 720,column 1,is_stmt,address _CIProcessNACKCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessNACKCmd
$C$DW$153	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$153, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$153, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 719 | void CIProcessNACKCmd(uint16_t usnMinorCmd)                            
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessNACKCmd             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessNACKCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$154	.dwtag  DW_TAG_variable
	.dwattr $C$DW$154, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$154, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 721 | //!If the minor command is NAK command, re-transmit the packet which wa
;     | s                                                                      
; 722 | //!not transmitted                                                     
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |720| 
	.dwpsn	file "../source/CommInterface.c",line 723,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 723 | if(usnMinorCmd == SITARA_NACK_SUB_CMD)                                 
;----------------------------------------------------------------------
        MOV       AL,#8189              ; [CPU_] |723| 
        CMP       AL,*-SP[3]            ; [CPU_] |723| 
        B         $C$L45,NEQ            ; [CPU_] |723| 
        ; branchcc occurs ; [] |723| 
	.dwpsn	file "../source/CommInterface.c",line 725,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 725 | sprintf(DebugPrintBuf, "\r\n NEGATIVE_ACKNOWLEDGEMENT_HANDELING\n");   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL6         ; [CPU_U] |725| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |725| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |725| 
$C$DW$155	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$155, DW_AT_low_pc(0x00)
	.dwattr $C$DW$155, DW_AT_name("_sprintf")
	.dwattr $C$DW$155, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |725| 
        ; call occurs [#_sprintf] ; [] |725| 
	.dwpsn	file "../source/CommInterface.c",line 726,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 726 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 727 | //!Reset the g_usnCurrentModeTemp                                      
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |726| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |726| 
$C$DW$156	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$156, DW_AT_low_pc(0x00)
	.dwattr $C$DW$156, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$156, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |726| 
        ; call occurs [#_UartDebugPrint] ; [] |726| 
	.dwpsn	file "../source/CommInterface.c",line 728,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 728 | g_usnCurrentModeTemp = ZERO; // Added 8/1/2020                         
; 729 |     //!Check for the previously transmitted minor command for the      
; 730 |     //!re-transmission of the packet                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeTemp ; [CPU_U] 
        MOV       @_g_usnCurrentModeTemp,#0 ; [CPU_] |728| 
	.dwpsn	file "../source/CommInterface.c",line 731,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 731 | switch(m_stPrevTxPacket.usnMinorCommand)                               
; 733 |     //!If the previously transmitted packet is WBC ADC Raw data,       
; 734 |     //!perform the below operations                                    
; 735 |         case DELFINO_WBC_ADC_RAW_DATA:                                 
; 736 |             //!Set NAK is received to indicate problem in transmission
;     | of packet                                                              
;----------------------------------------------------------------------
        B         $C$L44,UNC            ; [CPU_] |731| 
        ; branch occurs ; [] |731| 
$C$L40:    
	.dwpsn	file "../source/CommInterface.c",line 737,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 737 | m_bNAckReceived = TRUE;                                                
; 738 | //!Set the cell type to WBC to transmit WBC packet                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVB      @_m_bNAckReceived,#1,UNC ; [CPU_] |737| 
	.dwpsn	file "../source/CommInterface.c",line 739,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 739 | m_eTypeofCell = eCellWBC;                                              
; 740 | //!Frame the packet for re-transmission                                
;----------------------------------------------------------------------
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |739| 
	.dwpsn	file "../source/CommInterface.c",line 741,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 741 | CPFrameDataPacket(m_stPrevTxPacket.usnMajorCommand,\                   
; 742 |         m_stPrevTxPacket.usnMinorCommand, FALSE);                      
;----------------------------------------------------------------------
        MOVW      DP,#_m_stPrevTxPacket+4 ; [CPU_U] 
        MOVB      XAR4,#0               ; [CPU_] |741| 
        MOV       AL,@_m_stPrevTxPacket+4 ; [CPU_] |741| 
        MOV       AH,@_m_stPrevTxPacket+5 ; [CPU_] |741| 
$C$DW$157	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$157, DW_AT_low_pc(0x00)
	.dwattr $C$DW$157, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$157, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |741| 
        ; call occurs [#_CPFrameDataPacket] ; [] |741| 
	.dwpsn	file "../source/CommInterface.c",line 743,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 743 | sprintf(DebugPrintBuf, "\r\n N_A_H..WBC\n");                           
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL7         ; [CPU_U] |743| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |743| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |743| 
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("_sprintf")
	.dwattr $C$DW$158, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |743| 
        ; call occurs [#_sprintf] ; [] |743| 
	.dwpsn	file "../source/CommInterface.c",line 744,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 744 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |744| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |744| 
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$159, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |744| 
        ; call occurs [#_UartDebugPrint] ; [] |744| 
	.dwpsn	file "../source/CommInterface.c",line 745,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 745 | break;                                                                 
; 747 | //!If the previously transmitted packet is RBC ADC Raw data,           
; 748 | //!perform the below operations                                        
; 749 | case DELFINO_RBC_ADC_RAW_DATA:                                         
; 750 | //!Set NAK is received to indicate problem in transmission of packet   
;----------------------------------------------------------------------
        B         $C$L45,UNC            ; [CPU_] |745| 
        ; branch occurs ; [] |745| 
$C$L41:    
	.dwpsn	file "../source/CommInterface.c",line 751,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 751 | m_bNAckReceived = TRUE;                                                
; 752 | //!Set the cell type to RBC to transmit RBC packet                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVB      @_m_bNAckReceived,#1,UNC ; [CPU_] |751| 
	.dwpsn	file "../source/CommInterface.c",line 753,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 753 | m_eTypeofCell = eCellRBC;                                              
; 754 | //!Frame the packet for re-transmission                                
;----------------------------------------------------------------------
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |753| 
	.dwpsn	file "../source/CommInterface.c",line 755,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 755 | CPFrameDataPacket(m_stPrevTxPacket.usnMajorCommand,\                   
; 756 |         m_stPrevTxPacket.usnMinorCommand, FALSE);                      
;----------------------------------------------------------------------
        MOVW      DP,#_m_stPrevTxPacket+4 ; [CPU_U] 
        MOVB      XAR4,#0               ; [CPU_] |755| 
        MOV       AL,@_m_stPrevTxPacket+4 ; [CPU_] |755| 
        MOV       AH,@_m_stPrevTxPacket+5 ; [CPU_] |755| 
$C$DW$160	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$160, DW_AT_low_pc(0x00)
	.dwattr $C$DW$160, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$160, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |755| 
        ; call occurs [#_CPFrameDataPacket] ; [] |755| 
	.dwpsn	file "../source/CommInterface.c",line 757,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 757 | sprintf(DebugPrintBuf, "\r\n N_A_H...RBC\n");                          
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL8         ; [CPU_U] |757| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |757| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |757| 
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_name("_sprintf")
	.dwattr $C$DW$161, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |757| 
        ; call occurs [#_sprintf] ; [] |757| 
	.dwpsn	file "../source/CommInterface.c",line 758,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 758 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |758| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |758| 
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$162, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |758| 
        ; call occurs [#_UartDebugPrint] ; [] |758| 
	.dwpsn	file "../source/CommInterface.c",line 759,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 759 | break;                                                                 
; 761 | //!If the previously transmitted packet is PLT ADC Raw data,           
; 762 | //!perform the below operations                                        
; 763 | case DELFINO_PLT_ADC_RAW_DATA:                                         
; 764 | //!Set NAK is received to indicate problem in transmission of packet   
;----------------------------------------------------------------------
        B         $C$L45,UNC            ; [CPU_] |759| 
        ; branch occurs ; [] |759| 
$C$L42:    
	.dwpsn	file "../source/CommInterface.c",line 765,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 765 | m_bNAckReceived = TRUE;                                                
; 766 | //!Set the cell type to PLT to transmit PLT packet                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVB      @_m_bNAckReceived,#1,UNC ; [CPU_] |765| 
	.dwpsn	file "../source/CommInterface.c",line 767,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 767 | m_eTypeofCell = eCellPLT;                                              
; 768 | //!Frame the packet for re-transmission                                
;----------------------------------------------------------------------
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |767| 
	.dwpsn	file "../source/CommInterface.c",line 769,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 769 | CPFrameDataPacket(m_stPrevTxPacket.usnMajorCommand,\                   
; 770 |         m_stPrevTxPacket.usnMinorCommand, FALSE);                      
;----------------------------------------------------------------------
        MOVW      DP,#_m_stPrevTxPacket+4 ; [CPU_U] 
        MOVB      XAR4,#0               ; [CPU_] |769| 
        MOV       AL,@_m_stPrevTxPacket+4 ; [CPU_] |769| 
        MOV       AH,@_m_stPrevTxPacket+5 ; [CPU_] |769| 
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$163, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |769| 
        ; call occurs [#_CPFrameDataPacket] ; [] |769| 
	.dwpsn	file "../source/CommInterface.c",line 771,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 771 | sprintf(DebugPrintBuf, "\r\n N_A_H....PLT\n");                         
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL9         ; [CPU_U] |771| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |771| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |771| 
$C$DW$164	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$164, DW_AT_low_pc(0x00)
	.dwattr $C$DW$164, DW_AT_name("_sprintf")
	.dwattr $C$DW$164, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |771| 
        ; call occurs [#_sprintf] ; [] |771| 
	.dwpsn	file "../source/CommInterface.c",line 772,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 772 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |772| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |772| 
$C$DW$165	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$165, DW_AT_low_pc(0x00)
	.dwattr $C$DW$165, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$165, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |772| 
        ; call occurs [#_UartDebugPrint] ; [] |772| 
	.dwpsn	file "../source/CommInterface.c",line 773,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 773 | break;                                                                 
; 775 | default:                                                               
; 776 | //!Set NAK is received to indicate problem in transmission of packet   
;----------------------------------------------------------------------
        B         $C$L45,UNC            ; [CPU_] |773| 
        ; branch occurs ; [] |773| 
$C$L43:    
	.dwpsn	file "../source/CommInterface.c",line 777,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 777 | m_bNAckReceived = TRUE;                                                
; 778 | //!In default scenario, re-transmit the previous packet                
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOVB      @_m_bNAckReceived,#1,UNC ; [CPU_] |777| 
	.dwpsn	file "../source/CommInterface.c",line 779,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 779 | CPFrameDataPacket(m_stPrevTxPacket.usnMajorCommand,\                   
; 780 |         m_stPrevTxPacket.usnMinorCommand, FALSE);                      
; 781 | //!Set NAK to false after framing of packet                            
;----------------------------------------------------------------------
        MOVW      DP,#_m_stPrevTxPacket+4 ; [CPU_U] 
        MOVB      XAR4,#0               ; [CPU_] |779| 
        MOV       AL,@_m_stPrevTxPacket+4 ; [CPU_] |779| 
        MOV       AH,@_m_stPrevTxPacket+5 ; [CPU_] |779| 
$C$DW$166	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$166, DW_AT_low_pc(0x00)
	.dwattr $C$DW$166, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$166, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |779| 
        ; call occurs [#_CPFrameDataPacket] ; [] |779| 
	.dwpsn	file "../source/CommInterface.c",line 782,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 782 | m_bNAckReceived = FALSE;                                               
; 783 | //!Set packet count to zero so that no further packet to be            
; 784 | //!transmitted to processor                                            
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOV       @_m_bNAckReceived,#0  ; [CPU_] |782| 
	.dwpsn	file "../source/CommInterface.c",line 785,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 785 | m_usnPacketCount =0;                                                   
;----------------------------------------------------------------------
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |785| 
	.dwpsn	file "../source/CommInterface.c",line 786,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 786 | sprintf(DebugPrintBuf, "\r\n N_A_H default\n");                        
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL10        ; [CPU_U] |786| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |786| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |786| 
$C$DW$167	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$167, DW_AT_low_pc(0x00)
	.dwattr $C$DW$167, DW_AT_name("_sprintf")
	.dwattr $C$DW$167, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |786| 
        ; call occurs [#_sprintf] ; [] |786| 
	.dwpsn	file "../source/CommInterface.c",line 787,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 787 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |787| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |787| 
$C$DW$168	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$168, DW_AT_low_pc(0x00)
	.dwattr $C$DW$168, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$168, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |787| 
        ; call occurs [#_UartDebugPrint] ; [] |787| 
	.dwpsn	file "../source/CommInterface.c",line 788,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 788 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L45,UNC            ; [CPU_] |788| 
        ; branch occurs ; [] |788| 
$C$L44:    
	.dwpsn	file "../source/CommInterface.c",line 731,column 3,is_stmt,isa 0
        MOVW      DP,#_m_stPrevTxPacket+5 ; [CPU_U] 
        MOVZ      AR6,@_m_stPrevTxPacket+5 ; [CPU_] |731| 
        MOVZ      AR7,AR6               ; [CPU_] |731| 
        MOVL      XAR4,#8198            ; [CPU_U] |731| 
        MOVL      ACC,XAR4              ; [CPU_] |731| 
        CMPL      ACC,XAR7              ; [CPU_] |731| 
        B         $C$L40,EQ             ; [CPU_] |731| 
        ; branchcc occurs ; [] |731| 
        MOVZ      AR7,AR6               ; [CPU_] |731| 
        MOVL      XAR4,#8199            ; [CPU_U] |731| 
        MOVL      ACC,XAR4              ; [CPU_] |731| 
        CMPL      ACC,XAR7              ; [CPU_] |731| 
        B         $C$L41,EQ             ; [CPU_] |731| 
        ; branchcc occurs ; [] |731| 
        MOVZ      AR6,AR6               ; [CPU_] |731| 
        MOVL      XAR4,#8200            ; [CPU_U] |731| 
        MOVL      ACC,XAR4              ; [CPU_] |731| 
        CMPL      ACC,XAR6              ; [CPU_] |731| 
        B         $C$L42,EQ             ; [CPU_] |731| 
        ; branchcc occurs ; [] |731| 
        B         $C$L43,UNC            ; [CPU_] |731| 
        ; branch occurs ; [] |731| 
$C$L45:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$169	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$169, DW_AT_low_pc(0x00)
	.dwattr $C$DW$169, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$152, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$152, DW_AT_TI_end_line(0x319)
	.dwattr $C$DW$152, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$152

	.sect	".text:_CIFormServiceHandlingPacket"
	.clink
	.global	_CIFormServiceHandlingPacket

$C$DW$170	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$170, DW_AT_name("CIFormServiceHandlingPacket")
	.dwattr $C$DW$170, DW_AT_low_pc(_CIFormServiceHandlingPacket)
	.dwattr $C$DW$170, DW_AT_high_pc(0x00)
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_CIFormServiceHandlingPacket")
	.dwattr $C$DW$170, DW_AT_external
	.dwattr $C$DW$170, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$170, DW_AT_TI_begin_line(0x325)
	.dwattr $C$DW$170, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$170, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/CommInterface.c",line 806,column 1,is_stmt,address _CIFormServiceHandlingPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormServiceHandlingPacket
$C$DW$171	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$171, DW_AT_name("usnServicehandled")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_usnServicehandled")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg0]

$C$DW$172	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$172, DW_AT_name("bStartEnd")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_bStartEnd")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 805 | void CIFormServiceHandlingPacket(uint16_t usnServicehandled, bool_t bSt
;     | artEnd)                                                                
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormServiceHandlingPacket  FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CIFormServiceHandlingPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$173	.dwtag  DW_TAG_variable
	.dwattr $C$DW$173, DW_AT_name("usnServicehandled")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_usnServicehandled")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$173, DW_AT_location[DW_OP_breg20 -5]

$C$DW$174	.dwtag  DW_TAG_variable
	.dwattr $C$DW$174, DW_AT_name("bStartEnd")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_bStartEnd")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$174, DW_AT_location[DW_OP_breg20 -6]

$C$DW$175	.dwtag  DW_TAG_variable
	.dwattr $C$DW$175, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$175, DW_AT_location[DW_OP_breg20 -7]

;----------------------------------------------------------------------
; 807 | uint16_t usnMinorCmd;                                                  
; 809 | //    sprintf(DebugPrintBuf, "\n CIFormServiceHandlingPacket %x %x\n",u
;     | snServicehandled, bStartEnd);                                          
; 810 | //    UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 811 | //!During service handling, starting of the service handling and end of
; 812 | //!service handling to be sent to processor.                           
; 813 | //!\var bStartEnd is used to identify whether the service handling is  
; 814 | //!starting or ending                                                  
; 815 | //!If bStartEnd is TRUE, service handling is starting and if bStartEnd
;     | is                                                                     
; 816 | //!FALSE, Service handling is ending.                                  
; 818 | //!If the Service handling is starting (bStartEnd is true), set the bus
;     | y                                                                      
; 819 | //!pin to high to indicate processor that controller is busy in running
;     |  the sequence.                                                         
; 820 | //!else (bStartEnd is false), clear the busy pin to indicate controller
;     |  is                                                                    
; 821 | //!not busy and the current sequence is ended.                         
;----------------------------------------------------------------------
        MOV       *-SP[6],AH            ; [CPU_] |806| 
        MOV       *-SP[5],AL            ; [CPU_] |806| 
	.dwpsn	file "../source/CommInterface.c",line 822,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 822 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |822| 
        CMPB      AL,#1                 ; [CPU_] |822| 
        B         $C$L46,NEQ            ; [CPU_] |822| 
        ; branchcc occurs ; [] |822| 
	.dwpsn	file "../source/CommInterface.c",line 824,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 824 | if(g_bBusyBitStatus == FALSE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bBusyBitStatus ; [CPU_U] 
        MOV       AL,@_g_bBusyBitStatus ; [CPU_] |824| 
        B         $C$L126,NEQ           ; [CPU_] |824| 
        ; branchcc occurs ; [] |824| 
	.dwpsn	file "../source/CommInterface.c",line 826,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 826 | SPISetCommCtrlBusyInt();                                               
;----------------------------------------------------------------------
$C$DW$176	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$176, DW_AT_low_pc(0x00)
	.dwattr $C$DW$176, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$176, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |826| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |826| 
	.dwpsn	file "../source/CommInterface.c",line 828,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 829 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L126,UNC           ; [CPU_] |828| 
        ; branch occurs ; [] |828| 
$C$L46:    
	.dwpsn	file "../source/CommInterface.c",line 831,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 831 | if((SYSTEM_STATUS_SERVICE_HANDLING != usnServicehandled) || \          
; 832 |                 (SPI_ERROR_CHECK != usnServicehandled))                
;----------------------------------------------------------------------
        MOVU      ACC,*-SP[5]           ; [CPU_] |831| 
        MOVL      XAR4,#20492           ; [CPU_U] |831| 
        CMPL      ACC,XAR4              ; [CPU_] |831| 
        B         $C$L47,NEQ            ; [CPU_] |831| 
        ; branchcc occurs ; [] |831| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |831| 
        MOVL      XAR4,#20535           ; [CPU_U] |831| 
        CMPL      ACC,XAR4              ; [CPU_] |831| 
        B         $C$L126,EQ            ; [CPU_] |831| 
        ; branchcc occurs ; [] |831| 
$C$L47:    
	.dwpsn	file "../source/CommInterface.c",line 834,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 834 | if(g_bBusyBitStatus == TRUE)                                           
;----------------------------------------------------------------------
        MOVW      DP,#_g_bBusyBitStatus ; [CPU_U] 
        MOV       AL,@_g_bBusyBitStatus ; [CPU_] |834| 
        CMPB      AL,#1                 ; [CPU_] |834| 
        B         $C$L126,NEQ           ; [CPU_] |834| 
        ; branchcc occurs ; [] |834| 
	.dwpsn	file "../source/CommInterface.c",line 836,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 836 | SPIClearCommCtrlBusyInt();                                             
; 840 | //!Check for the current service handling that was initiated           
;----------------------------------------------------------------------
$C$DW$177	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$177, DW_AT_low_pc(0x00)
	.dwattr $C$DW$177, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$177, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |836| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |836| 
	.dwpsn	file "../source/CommInterface.c",line 841,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 841 | switch(usnServicehandled)                                              
; 843 |     //!If the service handling initiated is Rinse Prime and if the sequ
;     | ence                                                                   
; 844 |     //!is starting, set the \var usnMinorCmd to Rinse Prime started    
; 845 |     //!If the sequence is ending, set the \var usnMinorCmd to Rinse    
; 846 |     //!Prime completed                                                 
; 847 |         case PRIME_WITH_RINSE_SERVICE_HANDLING:                        
;----------------------------------------------------------------------
        B         $C$L126,UNC           ; [CPU_] |841| 
        ; branch occurs ; [] |841| 
$C$L48:    
	.dwpsn	file "../source/CommInterface.c",line 848,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 848 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |848| 
        CMPB      AL,#1                 ; [CPU_] |848| 
        B         $C$L49,NEQ            ; [CPU_] |848| 
        ; branchcc occurs ; [] |848| 
	.dwpsn	file "../source/CommInterface.c",line 850,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 850 | usnMinorCmd = DELFINO_PRIME_WITH_RINSE_STARTED_CMD;                    
; 851 | //!Disable the Rinse Prime monitoring                                  
;----------------------------------------------------------------------
        MOV       *-SP[7],#8216         ; [CPU_] |850| 
	.dwpsn	file "../source/CommInterface.c",line 852,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 852 | g_bMonRinsePriming = FALSE;                                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOV       @_g_bMonRinsePriming,#0 ; [CPU_] |852| 
	.dwpsn	file "../source/CommInterface.c",line 853,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 854 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |853| 
        ; branch occurs ; [] |853| 
$C$L49:    
	.dwpsn	file "../source/CommInterface.c",line 856,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 856 | usnMinorCmd = DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD;                  
; 858 | //!Enable the Lyse Prime monitoring                                    
;----------------------------------------------------------------------
        MOV       *-SP[7],#8193         ; [CPU_] |856| 
	.dwpsn	file "../source/CommInterface.c",line 859,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 859 | g_bMonLysePriming = TRUE;                                              
; 861 | //!Enable the Diluent Prime monitoring                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOVB      @_g_bMonLysePriming,#1,UNC ; [CPU_] |859| 
	.dwpsn	file "../source/CommInterface.c",line 862,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 862 | g_bMonDiluentPriming = TRUE;                                           
; 864 | //!Enable the Rinse Prime monitoring                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOVB      @_g_bMonDiluentPriming,#1,UNC ; [CPU_] |862| 
	.dwpsn	file "../source/CommInterface.c",line 865,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 865 | g_bMonRinsePriming = TRUE;                                             
; 867 | //!Clear Rinse prime error only if the priming sequence is             
; 868 | //! completed successfully                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOVB      @_g_bMonRinsePriming,#1,UNC ; [CPU_] |865| 
	.dwpsn	file "../source/CommInterface.c",line 869,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 869 | utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;                    
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       @_utErrorInfoCheck+2,#0xff7f ; [CPU_] |869| 
	.dwpsn	file "../source/CommInterface.c",line 871,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 871 | break;                                                                 
; 873 | //!If the service handling initiated is Lyse Prime and if the sequence 
; 874 | //!is starting, set the \var usnMinorCmd to Lyse Prime started         
; 875 | //!If the sequence is ending, set the \var usnMinorCmd to Lyse         
; 876 | //!Prime completed                                                     
; 877 | case PRIME_WITH_LYSE_SERVICE_HANDLING:                                 
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |871| 
        ; branch occurs ; [] |871| 
$C$L50:    
	.dwpsn	file "../source/CommInterface.c",line 878,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 878 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |878| 
        CMPB      AL,#1                 ; [CPU_] |878| 
        B         $C$L51,NEQ            ; [CPU_] |878| 
        ; branchcc occurs ; [] |878| 
	.dwpsn	file "../source/CommInterface.c",line 880,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 880 | usnMinorCmd = DELFINO_PRIME_WITH_LYSE_STARTED_CMD;                     
; 882 | //!Disable the Lyse Prime monitoring                                   
;----------------------------------------------------------------------
        MOV       *-SP[7],#8217         ; [CPU_] |880| 
	.dwpsn	file "../source/CommInterface.c",line 883,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 883 | g_bMonLysePriming = FALSE;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOV       @_g_bMonLysePriming,#0 ; [CPU_] |883| 
	.dwpsn	file "../source/CommInterface.c",line 884,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 885 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |884| 
        ; branch occurs ; [] |884| 
$C$L51:    
	.dwpsn	file "../source/CommInterface.c",line 887,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 887 | usnMinorCmd = DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD;                   
; 888 | //!Enable the Lyse Prime monitoring                                    
;----------------------------------------------------------------------
        MOV       *-SP[7],#8194         ; [CPU_] |887| 
	.dwpsn	file "../source/CommInterface.c",line 889,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 889 | g_bMonLysePriming = TRUE;                                              
; 891 | //!Enable the Diluent Prime monitoring                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOVB      @_g_bMonLysePriming,#1,UNC ; [CPU_] |889| 
	.dwpsn	file "../source/CommInterface.c",line 892,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 892 | g_bMonDiluentPriming = TRUE;                                           
; 894 | //!Enable the Rinse Prime monitoring                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOVB      @_g_bMonDiluentPriming,#1,UNC ; [CPU_] |892| 
	.dwpsn	file "../source/CommInterface.c",line 895,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 895 | g_bMonRinsePriming = TRUE;                                             
; 897 | //!Clear the lyse prime error only if the priming sequence is          
; 898 | //! completed successfully                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOVB      @_g_bMonRinsePriming,#1,UNC ; [CPU_] |895| 
	.dwpsn	file "../source/CommInterface.c",line 899,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 899 | utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;                     
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       @_utErrorInfoCheck+2,#0xffbf ; [CPU_] |899| 
	.dwpsn	file "../source/CommInterface.c",line 901,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 901 | break;                                                                 
; 903 | //!If the service handling initiated is Diluent Prime and if the sequen
;     | ce                                                                     
; 904 | //!is starting, set the \var usnMinorCmd to Diluent Prime started      
; 905 | //!If the sequence is ending, set the \var usnMinorCmd to Diluent      
; 906 | //!Prime completed                                                     
; 907 | case PRIME_WITH_DILUENT_SERVICE_HANDLING:                              
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |901| 
        ; branch occurs ; [] |901| 
$C$L52:    
	.dwpsn	file "../source/CommInterface.c",line 908,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 908 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |908| 
        CMPB      AL,#1                 ; [CPU_] |908| 
        B         $C$L53,NEQ            ; [CPU_] |908| 
        ; branchcc occurs ; [] |908| 
	.dwpsn	file "../source/CommInterface.c",line 910,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 910 | usnMinorCmd = DELFINO_PRIME_WITH_DILUENT_STARTED_CMD;                  
; 911 | //!Disable the Diluent Prime monitoring                                
;----------------------------------------------------------------------
        MOV       *-SP[7],#8218         ; [CPU_] |910| 
	.dwpsn	file "../source/CommInterface.c",line 912,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 912 | g_bMonDiluentPriming = FALSE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOV       @_g_bMonDiluentPriming,#0 ; [CPU_] |912| 
	.dwpsn	file "../source/CommInterface.c",line 913,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 914 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |913| 
        ; branch occurs ; [] |913| 
$C$L53:    
	.dwpsn	file "../source/CommInterface.c",line 916,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 916 | usnMinorCmd = DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD;                
; 917 | //!Enable the Lyse Prime monitoring                                    
;----------------------------------------------------------------------
        MOV       *-SP[7],#8195         ; [CPU_] |916| 
	.dwpsn	file "../source/CommInterface.c",line 918,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 918 | g_bMonLysePriming = TRUE;                                              
; 920 | //!Enable the Diluent Prime monitoring                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOVB      @_g_bMonLysePriming,#1,UNC ; [CPU_] |918| 
	.dwpsn	file "../source/CommInterface.c",line 921,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 921 | g_bMonDiluentPriming = TRUE;                                           
; 923 | //!Enable the Rinse Prime monitoring                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOVB      @_g_bMonDiluentPriming,#1,UNC ; [CPU_] |921| 
	.dwpsn	file "../source/CommInterface.c",line 924,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 924 | g_bMonRinsePriming = TRUE;                                             
; 926 | //!Clear the Diluent prime error only if the priming sequence is       
; 927 | //! completed successfully                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOVB      @_g_bMonRinsePriming,#1,UNC ; [CPU_] |924| 
	.dwpsn	file "../source/CommInterface.c",line 928,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 928 | utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;                  
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       @_utErrorInfoCheck+2,#0xffdf ; [CPU_] |928| 
	.dwpsn	file "../source/CommInterface.c",line 931,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 931 | break;                                                                 
; 933 | //!If the service handling initiated is EZ Cleanser and if the sequence
; 934 | //!is starting, set the \var usnMinorCmd to EZ cleanser started        
; 935 | //!If the sequence is ending, set the \var usnMinorCmd to EZ Cleanser  
; 936 | //!Prime completed                                                     
; 937 | case PRIME_WITH_EZ_CLEANSER_CMD:                                       
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |931| 
        ; branch occurs ; [] |931| 
$C$L54:    
	.dwpsn	file "../source/CommInterface.c",line 938,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 938 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |938| 
        CMPB      AL,#1                 ; [CPU_] |938| 
        B         $C$L55,NEQ            ; [CPU_] |938| 
        ; branchcc occurs ; [] |938| 
	.dwpsn	file "../source/CommInterface.c",line 940,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 940 | usnMinorCmd = DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD;              
;----------------------------------------------------------------------
        MOV       *-SP[7],#8221         ; [CPU_] |940| 
	.dwpsn	file "../source/CommInterface.c",line 941,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 941 | sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD\n
;     | ");                                                                    
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL11        ; [CPU_U] |941| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |941| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |941| 
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_name("_sprintf")
	.dwattr $C$DW$178, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |941| 
        ; call occurs [#_sprintf] ; [] |941| 
	.dwpsn	file "../source/CommInterface.c",line 942,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 942 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |942| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |942| 
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$179, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |942| 
        ; call occurs [#_UartDebugPrint] ; [] |942| 
	.dwpsn	file "../source/CommInterface.c",line 943,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 944 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |943| 
        ; branch occurs ; [] |943| 
$C$L55:    
	.dwpsn	file "../source/CommInterface.c",line 946,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 946 | usnMinorCmd = DELFINO_PRIME_WITH_EZ_CLEANSER_CMD;                      
;----------------------------------------------------------------------
        MOV       *-SP[7],#8198         ; [CPU_] |946| 
	.dwpsn	file "../source/CommInterface.c",line 947,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 947 | sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_EZ_CLEANSER_CMD : %ld\n",
;     |  g_unSequenceStateTime1);                                              
;----------------------------------------------------------------------
        MOVW      DP,#_g_unSequenceStateTime1 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL12        ; [CPU_U] |947| 
        MOVL      ACC,@_g_unSequenceStateTime1 ; [CPU_] |947| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |947| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |947| 
        MOVL      *-SP[4],ACC           ; [CPU_] |947| 
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("_sprintf")
	.dwattr $C$DW$180, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |947| 
        ; call occurs [#_sprintf] ; [] |947| 
	.dwpsn	file "../source/CommInterface.c",line 948,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 948 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |948| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |948| 
$C$DW$181	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$181, DW_AT_low_pc(0x00)
	.dwattr $C$DW$181, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$181, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |948| 
        ; call occurs [#_UartDebugPrint] ; [] |948| 
	.dwpsn	file "../source/CommInterface.c",line 950,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 950 | break;                                                                 
; 952 | //!If the service handling initiated is Prime All and if the sequence  
; 953 | //!is starting, set the \var usnMinorCmd to Prime All started          
; 954 | //!If the sequence is ending, set the \var usnMinorCmd to Prime ALL    
; 955 | //!completed                                                           
; 956 | case PRIME_ALL_SERVICE_HANDLING:                                       
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |950| 
        ; branch occurs ; [] |950| 
$C$L56:    
	.dwpsn	file "../source/CommInterface.c",line 957,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 957 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |957| 
        CMPB      AL,#1                 ; [CPU_] |957| 
        B         $C$L57,NEQ            ; [CPU_] |957| 
        ; branchcc occurs ; [] |957| 
	.dwpsn	file "../source/CommInterface.c",line 959,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 959 | usnMinorCmd = DELFINO_PRIME_ALL_STARTED_CMD;                           
; 961 | //!Disable the Diluent Prime monitoring                                
;----------------------------------------------------------------------
        MOV       *-SP[7],#8219         ; [CPU_] |959| 
	.dwpsn	file "../source/CommInterface.c",line 962,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 962 | g_bMonDiluentPriming = FALSE;                                          
; 964 | //!Disable the Lyse Prime monitoring                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOV       @_g_bMonDiluentPriming,#0 ; [CPU_] |962| 
	.dwpsn	file "../source/CommInterface.c",line 965,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 965 | g_bMonLysePriming = FALSE;                                             
; 967 | //!Disable the Rinse Prime monitoring                                  
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOV       @_g_bMonLysePriming,#0 ; [CPU_] |965| 
	.dwpsn	file "../source/CommInterface.c",line 968,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 968 | g_bMonRinsePriming = FALSE;                                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOV       @_g_bMonRinsePriming,#0 ; [CPU_] |968| 
	.dwpsn	file "../source/CommInterface.c",line 970,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 971 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |970| 
        ; branch occurs ; [] |970| 
$C$L57:    
	.dwpsn	file "../source/CommInterface.c",line 973,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 973 | usnMinorCmd = DELFINO_PRIME_ALL_COMPLETED_CMD;                         
; 975 | //!Enable the Rinse Prime monitoring                                   
;----------------------------------------------------------------------
        MOV       *-SP[7],#8196         ; [CPU_] |973| 
	.dwpsn	file "../source/CommInterface.c",line 976,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 976 | g_bMonRinsePriming = TRUE;                                             
; 978 | //!Clear the Diluent prime error                                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOVB      @_g_bMonRinsePriming,#1,UNC ; [CPU_] |976| 
	.dwpsn	file "../source/CommInterface.c",line 979,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 979 | utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;                  
; 981 | //!Enable the Lyse Prime monitoring                                    
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       @_utErrorInfoCheck+2,#0xffdf ; [CPU_] |979| 
	.dwpsn	file "../source/CommInterface.c",line 982,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 982 | g_bMonLysePriming = TRUE;                                              
; 984 | //!Clear the lyse prime error                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOVB      @_g_bMonLysePriming,#1,UNC ; [CPU_] |982| 
	.dwpsn	file "../source/CommInterface.c",line 985,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 985 | utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;                     
; 987 | //!Enable the Diluent Prime monitoring                                 
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       @_utErrorInfoCheck+2,#0xffbf ; [CPU_] |985| 
	.dwpsn	file "../source/CommInterface.c",line 988,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 988 | g_bMonDiluentPriming = TRUE;                                           
; 990 | //!Clear Rinse prime error                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOVB      @_g_bMonDiluentPriming,#1,UNC ; [CPU_] |988| 
	.dwpsn	file "../source/CommInterface.c",line 991,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 991 | utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;                    
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       @_utErrorInfoCheck+2,#0xff7f ; [CPU_] |991| 
	.dwpsn	file "../source/CommInterface.c",line 994,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 994 | break;                                                                 
; 996 | //!If the service handling initiated is Back Flush and if the sequence 
; 997 | //!is starting, set the \var usnMinorCmd to Back Flush started         
; 998 | //!If the sequence is ending, set the \var usnMinorCmd to Back Flush   
; 999 | //!completed                                                           
; 1000 | case BACK_FLUSH_SERVICE_HANDLING:                                      
; 1001 | case MBD_STARTUP_BACKFLUSH_CMD:                                        
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |994| 
        ; branch occurs ; [] |994| 
$C$L58:    
	.dwpsn	file "../source/CommInterface.c",line 1002,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1002 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1002| 
        CMPB      AL,#1                 ; [CPU_] |1002| 
        B         $C$L59,NEQ            ; [CPU_] |1002| 
        ; branchcc occurs ; [] |1002| 
	.dwpsn	file "../source/CommInterface.c",line 1004,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1004 | usnMinorCmd = DELFINO_BACK_FLUSH_STARTED_CMD;                          
;----------------------------------------------------------------------
        MOV       *-SP[7],#8220         ; [CPU_] |1004| 
	.dwpsn	file "../source/CommInterface.c",line 1005,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1005 | sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_STARTED_CMD\n");         
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL13        ; [CPU_U] |1005| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1005| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1005| 
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_name("_sprintf")
	.dwattr $C$DW$182, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1005| 
        ; call occurs [#_sprintf] ; [] |1005| 
	.dwpsn	file "../source/CommInterface.c",line 1006,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1006 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1006| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1006| 
$C$DW$183	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$183, DW_AT_low_pc(0x00)
	.dwattr $C$DW$183, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$183, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1006| 
        ; call occurs [#_UartDebugPrint] ; [] |1006| 
	.dwpsn	file "../source/CommInterface.c",line 1007,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1008 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1007| 
        ; branch occurs ; [] |1007| 
$C$L59:    
	.dwpsn	file "../source/CommInterface.c",line 1010,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1010 | usnMinorCmd = DELFINO_BACK_FLUSH_COMPLETED_CMD;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8197         ; [CPU_] |1010| 
	.dwpsn	file "../source/CommInterface.c",line 1011,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1011 | sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_COMPLETED_CMD : %ld\n", g
;     | _unSequenceStateTime1);                                                
;----------------------------------------------------------------------
        MOVW      DP,#_g_unSequenceStateTime1 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL14        ; [CPU_U] |1011| 
        MOVL      ACC,@_g_unSequenceStateTime1 ; [CPU_] |1011| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1011| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1011| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1011| 
$C$DW$184	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$184, DW_AT_low_pc(0x00)
	.dwattr $C$DW$184, DW_AT_name("_sprintf")
	.dwattr $C$DW$184, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1011| 
        ; call occurs [#_sprintf] ; [] |1011| 
	.dwpsn	file "../source/CommInterface.c",line 1012,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1012 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1012| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1012| 
$C$DW$185	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$185, DW_AT_low_pc(0x00)
	.dwattr $C$DW$185, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$185, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1012| 
        ; call occurs [#_UartDebugPrint] ; [] |1012| 
	.dwpsn	file "../source/CommInterface.c",line 1014,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1014 | break;                                                                 
; 1016 | //!If the service handling initiated is ZAP Aperture and if the sequenc
;     | e                                                                      
; 1017 | //!is starting, set the \var usnMinorCmd to ZAP Aperture started       
; 1018 | //!If the sequence is ending, set the \var usnMinorCmd to ZAP Aperture 
; 1019 | //!completed                                                           
; 1020 | case ZAP_APERTURE_SERVICE_HANDLING:                                    
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1014| 
        ; branch occurs ; [] |1014| 
$C$L60:    
	.dwpsn	file "../source/CommInterface.c",line 1021,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1021 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1021| 
        CMPB      AL,#1                 ; [CPU_] |1021| 
        B         $C$L61,NEQ            ; [CPU_] |1021| 
        ; branchcc occurs ; [] |1021| 
	.dwpsn	file "../source/CommInterface.c",line 1023,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1023 | usnMinorCmd = DELFINO_ZAP_APERTURE_STARTED_CMD;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8222         ; [CPU_] |1023| 
	.dwpsn	file "../source/CommInterface.c",line 1024,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1025 | else                                                                   
; 1027 |     //!Zap Completed input given to MBD variable to be made false      
; 1028 |     //!when Zapping completed command is sent to Sitara                
; 1029 |     //!To avoid conflict in the next sequence of Zap complete          
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1024| 
        ; branch occurs ; [] |1024| 
$C$L61:    
	.dwpsn	file "../source/CommInterface.c",line 1030,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1030 | BloodCellCounter_U.ZapComplete  = FALSE;                               
;----------------------------------------------------------------------
        MOVW      DP,#_BloodCellCounter_U+9 ; [CPU_U] 
        MOV       @_BloodCellCounter_U+9,#0 ; [CPU_] |1030| 
	.dwpsn	file "../source/CommInterface.c",line 1031,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1031 | usnMinorCmd = DELFINO_ZAP_APERTURE_COMPLETED_CMD;                      
;----------------------------------------------------------------------
        MOV       *-SP[7],#8199         ; [CPU_] |1031| 
	.dwpsn	file "../source/CommInterface.c",line 1033,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1033 | break;                                                                 
; 1035 | //!If the service handling initiated is Drain Bath and if the sequence 
; 1036 | //!is starting, set the \var usnMinorCmd to Drain Bath started         
; 1037 | //!If the sequence is ending, set the \var usnMinorCmd to Drain Bath   
; 1038 | //!completed                                                           
; 1039 |         case DRAIN_BATH_SERVICE_HANDLING:                              
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1033| 
        ; branch occurs ; [] |1033| 
$C$L62:    
	.dwpsn	file "../source/CommInterface.c",line 1040,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1040 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1040| 
        CMPB      AL,#1                 ; [CPU_] |1040| 
        B         $C$L63,NEQ            ; [CPU_] |1040| 
        ; branchcc occurs ; [] |1040| 
	.dwpsn	file "../source/CommInterface.c",line 1042,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1042 | usnMinorCmd = DELFINO_DRAIN_BATH_STARTED_CMD;                          
;----------------------------------------------------------------------
        MOV       *-SP[7],#8223         ; [CPU_] |1042| 
	.dwpsn	file "../source/CommInterface.c",line 1043,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1044 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1043| 
        ; branch occurs ; [] |1043| 
$C$L63:    
	.dwpsn	file "../source/CommInterface.c",line 1046,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1046 | usnMinorCmd = DELFINO_DRAIN_BATH_COMPLETED_CMD;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8200         ; [CPU_] |1046| 
	.dwpsn	file "../source/CommInterface.c",line 1048,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1048 | break;                                                                 
; 1050 | //!If the service handling initiated is Drain All and if the sequence  
; 1051 | //!is starting, set the \var usnMinorCmd to Drain All started          
; 1052 | //!If the sequence is ending, set the \var usnMinorCmd to Drain All    
; 1053 | //!completed                                                           
; 1054 |         case DRAIN_ALL_SERVICE_HANDLING:                               
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1048| 
        ; branch occurs ; [] |1048| 
$C$L64:    
	.dwpsn	file "../source/CommInterface.c",line 1055,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1055 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1055| 
        CMPB      AL,#1                 ; [CPU_] |1055| 
        B         $C$L65,NEQ            ; [CPU_] |1055| 
        ; branchcc occurs ; [] |1055| 
	.dwpsn	file "../source/CommInterface.c",line 1057,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1057 | usnMinorCmd = DELFINO_DRAIN_ALL_STARTED_CMD;                           
;----------------------------------------------------------------------
        MOV       *-SP[7],#8224         ; [CPU_] |1057| 
	.dwpsn	file "../source/CommInterface.c",line 1058,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1058 | sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_ALL_STARTED_CMD\n");          
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL15        ; [CPU_U] |1058| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1058| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1058| 
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("_sprintf")
	.dwattr $C$DW$186, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1058| 
        ; call occurs [#_sprintf] ; [] |1058| 
	.dwpsn	file "../source/CommInterface.c",line 1059,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1059 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1059| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1059| 
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$187, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1059| 
        ; call occurs [#_UartDebugPrint] ; [] |1059| 
	.dwpsn	file "../source/CommInterface.c",line 1060,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1061 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1060| 
        ; branch occurs ; [] |1060| 
$C$L65:    
	.dwpsn	file "../source/CommInterface.c",line 1063,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1063 | usnMinorCmd = DELFINO_DRAIN_ALL_COMPLETED_CMD;                         
;----------------------------------------------------------------------
        MOV       *-SP[7],#8201         ; [CPU_] |1063| 
	.dwpsn	file "../source/CommInterface.c",line 1064,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1064 | sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_ALL_COMPLETED_CMD : %ld\n", g_
;     | unSequenceStateTime1);                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_unSequenceStateTime1 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL16        ; [CPU_U] |1064| 
        MOVL      ACC,@_g_unSequenceStateTime1 ; [CPU_] |1064| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1064| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1064| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1064| 
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_name("_sprintf")
	.dwattr $C$DW$188, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1064| 
        ; call occurs [#_sprintf] ; [] |1064| 
	.dwpsn	file "../source/CommInterface.c",line 1065,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1065 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1065| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1065| 
$C$DW$189	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$189, DW_AT_low_pc(0x00)
	.dwattr $C$DW$189, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$189, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1065| 
        ; call occurs [#_UartDebugPrint] ; [] |1065| 
	.dwpsn	file "../source/CommInterface.c",line 1067,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1067 | break;                                                                 
; 1069 | //!If the service handling initiated is Clean Bath and if the sequence 
; 1070 | //!is starting, set the \var usnMinorCmd to Clean Bath started         
; 1071 | //!If the sequence is ending, set the \var usnMinorCmd to Clean Bath   
; 1072 | //!completed                                                           
; 1073 |         case CLEAN_BATH_SERVICE_HANDLING:                              
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1067| 
        ; branch occurs ; [] |1067| 
$C$L66:    
	.dwpsn	file "../source/CommInterface.c",line 1074,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1074 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1074| 
        CMPB      AL,#1                 ; [CPU_] |1074| 
        B         $C$L67,NEQ            ; [CPU_] |1074| 
        ; branchcc occurs ; [] |1074| 
	.dwpsn	file "../source/CommInterface.c",line 1076,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1076 | usnMinorCmd = DELFINO_CLEAN_BATH_STARTED_CMD;                          
;----------------------------------------------------------------------
        MOV       *-SP[7],#8225         ; [CPU_] |1076| 
	.dwpsn	file "../source/CommInterface.c",line 1077,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1078 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1077| 
        ; branch occurs ; [] |1077| 
$C$L67:    
	.dwpsn	file "../source/CommInterface.c",line 1080,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1080 | usnMinorCmd = DELFINO_CLEAN_BATH_COMPLETED_CMD;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8202         ; [CPU_] |1080| 
	.dwpsn	file "../source/CommInterface.c",line 1082,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1082 | break;                                                                 
; 1084 | //!If the data request from processor is System status, no sequence is 
; 1085 | //!involved, it is only sending the system status to processor.        
; 1086 | //!So minor command is set to System Status                            
; 1087 |         case SYSTEM_STATUS_SERVICE_HANDLING:                           
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1082| 
        ; branch occurs ; [] |1082| 
$C$L68:    
	.dwpsn	file "../source/CommInterface.c",line 1088,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1088 | usnMinorCmd = DELFINO_SYSTEM_STATUS_DATA;                              
;----------------------------------------------------------------------
        MOV       *-SP[7],#8233         ; [CPU_] |1088| 
	.dwpsn	file "../source/CommInterface.c",line 1090,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1090 | break;                                                                 
; 1092 | //!If the command is valve test, no sequence is involved, it is only   
; 1093 | //!sending the status of completion of valve test.                     
; 1094 | //!So minor command is set to System Status                            
; 1095 | case VALVE_TEST_COMPLETION:                                            
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1090| 
        ; branch occurs ; [] |1090| 
$C$L69:    
	.dwpsn	file "../source/CommInterface.c",line 1096,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1096 | usnMinorCmd = DELFINO_VALVE_TEST_COMPLETED_CMD;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8203         ; [CPU_] |1096| 
	.dwpsn	file "../source/CommInterface.c",line 1098,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1098 | break;                                                                 
; 1100 | //!If Waste Bin is detected, then send Waste bin full command to proces
;     | sor.                                                                   
; 1101 | //!Set the minor command to Waste Bin full.                            
; 1102 | case WASTE_BIN_FULL_CMD:                                               
; 1103 |         //usnMinorCmd = DELFINO_SYSTEM_STATUS_DATA;                    
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1098| 
        ; branch occurs ; [] |1098| 
$C$L70:    
	.dwpsn	file "../source/CommInterface.c",line 1104,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1104 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1104| 
        CMPB      AL,#1                 ; [CPU_] |1104| 
        B         $C$L71,NEQ            ; [CPU_] |1104| 
        ; branchcc occurs ; [] |1104| 
	.dwpsn	file "../source/CommInterface.c",line 1106,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1106 | usnMinorCmd = DELFINO_SET_WASTE_BIN_FULL;                              
;----------------------------------------------------------------------
        MOV       *-SP[7],#8259         ; [CPU_] |1106| 
	.dwpsn	file "../source/CommInterface.c",line 1107,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1107 | sprintf(DebugPrintBuf, "\n DELFINO_SET_WASTE_BIN_FULL\n");             
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL17        ; [CPU_U] |1107| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1107| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1107| 
$C$DW$190	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$190, DW_AT_low_pc(0x00)
	.dwattr $C$DW$190, DW_AT_name("_sprintf")
	.dwattr $C$DW$190, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1107| 
        ; call occurs [#_sprintf] ; [] |1107| 
	.dwpsn	file "../source/CommInterface.c",line 1108,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1108 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1108| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1108| 
$C$DW$191	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$191, DW_AT_low_pc(0x00)
	.dwattr $C$DW$191, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$191, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1108| 
        ; call occurs [#_UartDebugPrint] ; [] |1108| 
	.dwpsn	file "../source/CommInterface.c",line 1109,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1110 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1109| 
        ; branch occurs ; [] |1109| 
$C$L71:    
	.dwpsn	file "../source/CommInterface.c",line 1112,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1112 | usnMinorCmd = DELFINO_CLEAR_WASTE_BIN_FULL;                            
;----------------------------------------------------------------------
        MOV       *-SP[7],#8260         ; [CPU_] |1112| 
	.dwpsn	file "../source/CommInterface.c",line 1113,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1113 | sprintf(DebugPrintBuf, "\n DELFINO_CLEAR_WASTE_BIN_FULL\n");           
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL18        ; [CPU_U] |1113| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1113| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1113| 
$C$DW$192	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$192, DW_AT_low_pc(0x00)
	.dwattr $C$DW$192, DW_AT_name("_sprintf")
	.dwattr $C$DW$192, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1113| 
        ; call occurs [#_sprintf] ; [] |1113| 
	.dwpsn	file "../source/CommInterface.c",line 1114,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1114 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1114| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1114| 
$C$DW$193	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$193, DW_AT_low_pc(0x00)
	.dwattr $C$DW$193, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$193, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1114| 
        ; call occurs [#_UartDebugPrint] ; [] |1114| 
	.dwpsn	file "../source/CommInterface.c",line 1116,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1116 | break;                                                                 
; 1118 | //!If the service handling initiated is Head Rinse and if the sequence 
; 1119 | //!is starting, set the \var usnMinorCmd to Head Rinse started         
; 1120 | //!If the sequence is ending, set the \var usnMinorCmd to Head Rinse   
; 1121 | //!completed                                                           
; 1122 | case HEAD_RINSING_SERVICE_HANDLING:                                    
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1116| 
        ; branch occurs ; [] |1116| 
$C$L72:    
	.dwpsn	file "../source/CommInterface.c",line 1123,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1123 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1123| 
        CMPB      AL,#1                 ; [CPU_] |1123| 
        B         $C$L130,EQ            ; [CPU_] |1123| 
        ; branchcc occurs ; [] |1123| 
	.dwpsn	file "../source/CommInterface.c",line 1126,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1127 | else                                                                   
;----------------------------------------------------------------------
	.dwpsn	file "../source/CommInterface.c",line 1129,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 1129 | usnMinorCmd = DELFINO_HEAD_RINSING_COMPLETED_CMD;                      
;----------------------------------------------------------------------
        MOV       *-SP[7],#8207         ; [CPU_] |1129| 
	.dwpsn	file "../source/CommInterface.c",line 1131,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1131 | break;                                                                 
; 1133 | //!If the service handling initiated is Probe Cleaning and if the seque
;     | nce                                                                    
; 1134 | //!is starting, set the \var usnMinorCmd to Probe Cleaning started     
; 1135 | //!If the sequence is ending, set the \var usnMinorCmd to Probe Cleanin
;     | g                                                                      
; 1136 | //!completed                                                           
; 1137 | case  PROBE_CLEANING_SERVICE_HANDLING:                                 
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1131| 
        ; branch occurs ; [] |1131| 
$C$L73:    
	.dwpsn	file "../source/CommInterface.c",line 1138,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1138 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1138| 
        CMPB      AL,#1                 ; [CPU_] |1138| 
        B         $C$L130,EQ            ; [CPU_] |1138| 
        ; branchcc occurs ; [] |1138| 
	.dwpsn	file "../source/CommInterface.c",line 1141,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1142 | else                                                                   
;----------------------------------------------------------------------
	.dwpsn	file "../source/CommInterface.c",line 1144,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1144 | usnMinorCmd = DELFINO_PROBE_CLEANING_COMPLETED_CMD;                    
;----------------------------------------------------------------------
        MOV       *-SP[7],#8208         ; [CPU_] |1144| 
	.dwpsn	file "../source/CommInterface.c",line 1145,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1145 | sprintf(DebugPrintBuf, "\n DELFINO_PROBE_CLEANING_COMPLETED_CMD\n");   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL19        ; [CPU_U] |1145| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1145| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1145| 
$C$DW$194	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$194, DW_AT_low_pc(0x00)
	.dwattr $C$DW$194, DW_AT_name("_sprintf")
	.dwattr $C$DW$194, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1145| 
        ; call occurs [#_sprintf] ; [] |1145| 
	.dwpsn	file "../source/CommInterface.c",line 1146,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1146 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1146| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1146| 
$C$DW$195	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$195, DW_AT_low_pc(0x00)
	.dwattr $C$DW$195, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$195, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1146| 
        ; call occurs [#_UartDebugPrint] ; [] |1146| 
	.dwpsn	file "../source/CommInterface.c",line 1148,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1148 | break;                                                                 
; 1149 | //SAM_MOTOR_CHK -  START                                               
; 1151 | //!If the service handling initiated is X-Motor Check, send only the   
; 1152 | //!sequence completed status to the processor.                         
; 1153 | case X_AXIS_MOTOR_CHECK:                                               
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1148| 
        ; branch occurs ; [] |1148| 
$C$L74:    
	.dwpsn	file "../source/CommInterface.c",line 1154,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1154 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1154| 
        CMPB      AL,#1                 ; [CPU_] |1154| 
        B         $C$L75,NEQ            ; [CPU_] |1154| 
        ; branchcc occurs ; [] |1154| 
	.dwpsn	file "../source/CommInterface.c",line 1156,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1156 | usnMinorCmd = DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD;                    
;----------------------------------------------------------------------
        MOV       *-SP[7],#8227         ; [CPU_] |1156| 
	.dwpsn	file "../source/CommInterface.c",line 1158,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1159 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1158| 
        ; branch occurs ; [] |1158| 
$C$L75:    
	.dwpsn	file "../source/CommInterface.c",line 1161,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1161 | usnMinorCmd = DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD;                  
;----------------------------------------------------------------------
        MOV       *-SP[7],#8209         ; [CPU_] |1161| 
	.dwpsn	file "../source/CommInterface.c",line 1164,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1164 | break;                                                                 
; 1166 | //!If the service handling initiated is Y-Motor Check, send only the   
; 1167 | //!sequence completed status to the processor.                         
; 1168 | case Y_AXIS_MOTOR_CHECK:                                               
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1164| 
        ; branch occurs ; [] |1164| 
$C$L76:    
	.dwpsn	file "../source/CommInterface.c",line 1169,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1169 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1169| 
        CMPB      AL,#1                 ; [CPU_] |1169| 
        B         $C$L77,NEQ            ; [CPU_] |1169| 
        ; branchcc occurs ; [] |1169| 
	.dwpsn	file "../source/CommInterface.c",line 1171,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1171 | usnMinorCmd = DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD;                    
;----------------------------------------------------------------------
        MOV       *-SP[7],#8228         ; [CPU_] |1171| 
	.dwpsn	file "../source/CommInterface.c",line 1172,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1173 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1172| 
        ; branch occurs ; [] |1172| 
$C$L77:    
	.dwpsn	file "../source/CommInterface.c",line 1175,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1175 | usnMinorCmd = DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD;                  
;----------------------------------------------------------------------
        MOV       *-SP[7],#8210         ; [CPU_] |1175| 
	.dwpsn	file "../source/CommInterface.c",line 1177,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1177 | break;                                                                 
; 1179 | //!If the service handling initiated is Diluent Motor Check, send only
;     | the                                                                    
; 1180 | //!sequence completed status to the processor.                         
; 1181 | case DILUENT_SYRINGE_MOTOR_CHECK:                                      
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1177| 
        ; branch occurs ; [] |1177| 
$C$L78:    
	.dwpsn	file "../source/CommInterface.c",line 1182,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1182 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1182| 
        CMPB      AL,#1                 ; [CPU_] |1182| 
        B         $C$L79,NEQ            ; [CPU_] |1182| 
        ; branchcc occurs ; [] |1182| 
	.dwpsn	file "../source/CommInterface.c",line 1184,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1184 | usnMinorCmd = DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD;           
;----------------------------------------------------------------------
        MOV       *-SP[7],#8229         ; [CPU_] |1184| 
	.dwpsn	file "../source/CommInterface.c",line 1185,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1186 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1185| 
        ; branch occurs ; [] |1185| 
$C$L79:    
	.dwpsn	file "../source/CommInterface.c",line 1188,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1188 | usnMinorCmd = DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD;         
;----------------------------------------------------------------------
        MOV       *-SP[7],#8211         ; [CPU_] |1188| 
	.dwpsn	file "../source/CommInterface.c",line 1190,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1190 | break;                                                                 
; 1192 | //!If the service handling initiated is Waste Motor Check, send only th
;     | e                                                                      
; 1193 | //!sequence completed status to the processor.                         
; 1194 | case WASTE_SYRINGE_MOTOR_CHECK:                                        
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1190| 
        ; branch occurs ; [] |1190| 
$C$L80:    
	.dwpsn	file "../source/CommInterface.c",line 1195,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1195 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1195| 
        CMPB      AL,#1                 ; [CPU_] |1195| 
        B         $C$L81,NEQ            ; [CPU_] |1195| 
        ; branchcc occurs ; [] |1195| 
	.dwpsn	file "../source/CommInterface.c",line 1197,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1197 | usnMinorCmd = DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD;             
;----------------------------------------------------------------------
        MOV       *-SP[7],#8230         ; [CPU_] |1197| 
	.dwpsn	file "../source/CommInterface.c",line 1198,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1199 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1198| 
        ; branch occurs ; [] |1198| 
$C$L81:    
	.dwpsn	file "../source/CommInterface.c",line 1201,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1201 | usnMinorCmd = DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD;           
;----------------------------------------------------------------------
        MOV       *-SP[7],#8212         ; [CPU_] |1201| 
	.dwpsn	file "../source/CommInterface.c",line 1203,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1203 | break;                                                                 
; 1205 | //!If the service handling initiated is Sample Motor Check, send only t
;     | he                                                                     
; 1206 | //!sequence completed status to the processor.                         
; 1207 | case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:                                  
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1203| 
        ; branch occurs ; [] |1203| 
$C$L82:    
	.dwpsn	file "../source/CommInterface.c",line 1208,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1208 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1208| 
        CMPB      AL,#1                 ; [CPU_] |1208| 
        B         $C$L83,NEQ            ; [CPU_] |1208| 
        ; branchcc occurs ; [] |1208| 
	.dwpsn	file "../source/CommInterface.c",line 1210,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1210 | usnMinorCmd = DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD;       
;----------------------------------------------------------------------
        MOV       *-SP[7],#8231         ; [CPU_] |1210| 
	.dwpsn	file "../source/CommInterface.c",line 1211,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1212 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1211| 
        ; branch occurs ; [] |1211| 
$C$L83:    
	.dwpsn	file "../source/CommInterface.c",line 1214,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1214 | usnMinorCmd = DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD;     
;----------------------------------------------------------------------
        MOV       *-SP[7],#8213         ; [CPU_] |1214| 
	.dwpsn	file "../source/CommInterface.c",line 1216,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1216 | break;                                                                 
; 1217 | //SAM_MOTOR_CHK - END                                                  
; 1219 | //!If the service handling initiated is Xountign Time Sequence, send on
;     | ly the                                                                 
; 1220 | //!sequence completed status to the processor.                         
; 1221 | case COUNTING_TIME_SEQUENCE:                                           
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1216| 
        ; branch occurs ; [] |1216| 
$C$L84:    
	.dwpsn	file "../source/CommInterface.c",line 1222,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1222 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1222| 
        CMPB      AL,#1                 ; [CPU_] |1222| 
        B         $C$L85,NEQ            ; [CPU_] |1222| 
        ; branchcc occurs ; [] |1222| 
	.dwpsn	file "../source/CommInterface.c",line 1224,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1224 | usnMinorCmd = DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD;//SKM_CHANGE_C
;     | OUNTSTART                                                              
;----------------------------------------------------------------------
        MOV       *-SP[7],#8234         ; [CPU_] |1224| 
	.dwpsn	file "../source/CommInterface.c",line 1225,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1226 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1225| 
        ; branch occurs ; [] |1225| 
$C$L85:    
	.dwpsn	file "../source/CommInterface.c",line 1228,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1228 | usnMinorCmd = DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD;            
;----------------------------------------------------------------------
        MOV       *-SP[7],#8214         ; [CPU_] |1228| 
	.dwpsn	file "../source/CommInterface.c",line 1230,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1230 | break;                                                                 
; 1231 | //SKM_WAKEUP_START                                                     
; 1232 | //!If the service handling initiated is Wake Up and if the sequence    
; 1233 | //!is starting, set the \var usnMinorCmd to Wake Up started            
; 1234 | //!If the sequence is ending, set the \var usnMinorCmd to Wake Up      
; 1235 | //!completed                                                           
; 1236 | case MBD_WAKEUP_START:                                                 
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1230| 
        ; branch occurs ; [] |1230| 
$C$L86:    
	.dwpsn	file "../source/CommInterface.c",line 1237,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1237 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1237| 
        CMPB      AL,#1                 ; [CPU_] |1237| 
        B         $C$L87,NEQ            ; [CPU_] |1237| 
        ; branchcc occurs ; [] |1237| 
	.dwpsn	file "../source/CommInterface.c",line 1239,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 1239 | usnMinorCmd =  DELFINO_WAKEUP_SEQUENCE_STARTED;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8254         ; [CPU_] |1239| 
	.dwpsn	file "../source/CommInterface.c",line 1240,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 1240 | sprintf(DebugPrintBuf, "\n DELFINO_WAKEUP_SEQUENCE_STARTED \n");       
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL20        ; [CPU_U] |1240| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1240| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1240| 
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_name("_sprintf")
	.dwattr $C$DW$196, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1240| 
        ; call occurs [#_sprintf] ; [] |1240| 
	.dwpsn	file "../source/CommInterface.c",line 1241,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 1241 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1241| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1241| 
$C$DW$197	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$197, DW_AT_low_pc(0x00)
	.dwattr $C$DW$197, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$197, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1241| 
        ; call occurs [#_UartDebugPrint] ; [] |1241| 
	.dwpsn	file "../source/CommInterface.c",line 1242,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1243 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1242| 
        ; branch occurs ; [] |1242| 
$C$L87:    
	.dwpsn	file "../source/CommInterface.c",line 1245,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1245 | usnMinorCmd = DELFINO_WAKE_UP_COMPLETED_CMD;                           
;----------------------------------------------------------------------
        MOV       *-SP[7],#8226         ; [CPU_] |1245| 
	.dwpsn	file "../source/CommInterface.c",line 1246,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1246 | sprintf(DebugPrintBuf, "\n DELFINO_WAKE_UP_COMPLETED_CMD : %ld\n", g_un
;     | SequenceStateTime1);                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_unSequenceStateTime1 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL21        ; [CPU_U] |1246| 
        MOVL      ACC,@_g_unSequenceStateTime1 ; [CPU_] |1246| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1246| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1246| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1246| 
$C$DW$198	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$198, DW_AT_low_pc(0x00)
	.dwattr $C$DW$198, DW_AT_name("_sprintf")
	.dwattr $C$DW$198, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1246| 
        ; call occurs [#_sprintf] ; [] |1246| 
	.dwpsn	file "../source/CommInterface.c",line 1247,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1247 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1247| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1247| 
$C$DW$199	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$199, DW_AT_low_pc(0x00)
	.dwattr $C$DW$199, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$199, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1247| 
        ; call occurs [#_UartDebugPrint] ; [] |1247| 
	.dwpsn	file "../source/CommInterface.c",line 1249,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1249 | break;                                                                 
; 1250 | //SKM_WAKEUP_END                                                       
; 1252 | //HN_TOSHIP_Start                                                      
; 1253 | case MBD_TO_SHIP_SEQUENCE_START:                                       
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1249| 
        ; branch occurs ; [] |1249| 
$C$L88:    
	.dwpsn	file "../source/CommInterface.c",line 1254,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1254 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1254| 
        CMPB      AL,#1                 ; [CPU_] |1254| 
        B         $C$L89,NEQ            ; [CPU_] |1254| 
        ; branchcc occurs ; [] |1254| 
	.dwpsn	file "../source/CommInterface.c",line 1256,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1256 | usnMinorCmd = DELFINO_TO_SHIP_STARTED;                                 
;----------------------------------------------------------------------
        MOV       *-SP[7],#8235         ; [CPU_] |1256| 
	.dwpsn	file "../source/CommInterface.c",line 1257,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1258 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1257| 
        ; branch occurs ; [] |1257| 
$C$L89:    
	.dwpsn	file "../source/CommInterface.c",line 1260,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 1260 | usnMinorCmd = DELFINO_TO_SHIP_COMPLETED_CMD;                           
;----------------------------------------------------------------------
        MOV       *-SP[7],#8236         ; [CPU_] |1260| 
	.dwpsn	file "../source/CommInterface.c",line 1262,column 8,is_stmt,isa 0
;----------------------------------------------------------------------
; 1262 | break;                                                                 
; 1263 | //HN_TOSHIP_end                                                        
; 1265 | //PH_CHANGE_SAMPLE_START_BUTTON - START                                
; 1266 | case DELFINO_READY_FOR_ASPIRATION:                                     
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1262| 
        ; branch occurs ; [] |1262| 
$C$L90:    
	.dwpsn	file "../source/CommInterface.c",line 1267,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1267 | usnMinorCmd = DELFINO_READY_FOR_ASPIRATION;                            
;----------------------------------------------------------------------
        MOV       *-SP[7],#8204         ; [CPU_] |1267| 
	.dwpsn	file "../source/CommInterface.c",line 1268,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1268 | break;                                                                 
; 1269 | //PH_CHANGE_SAMPLE_START_BUTTON - END                                  
; 1271 | case DELFINO_ASPIRATION_COMPLETED:                                     
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1268| 
        ; branch occurs ; [] |1268| 
$C$L91:    
	.dwpsn	file "../source/CommInterface.c",line 1272,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1272 | usnMinorCmd = DELFINO_ASPIRATION_COMPLETED;                            
;----------------------------------------------------------------------
        MOV       *-SP[7],#8201         ; [CPU_] |1272| 
	.dwpsn	file "../source/CommInterface.c",line 1273,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1273 | break;                                                                 
; 1275 | case START_UP_SEQUENCE_MODE:                                           
; 1276 | case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:                              
; 1277 | case MBD_STARTUP_FAILED_CMD:                                           
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1273| 
        ; branch occurs ; [] |1273| 
$C$L92:    
	.dwpsn	file "../source/CommInterface.c",line 1278,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1278 | usnMinorCmd = DELFINO_START_UP_SEQUENCE_STARTED_CMD;                   
;----------------------------------------------------------------------
        MOV       *-SP[7],#8237         ; [CPU_] |1278| 
	.dwpsn	file "../source/CommInterface.c",line 1279,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1279 | break;                                                                 
; 1281 | case MBD_VOLUMETRIC_BOARD_CHECK:                                       
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1279| 
        ; branch occurs ; [] |1279| 
$C$L93:    
	.dwpsn	file "../source/CommInterface.c",line 1282,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1282 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1282| 
        CMPB      AL,#1                 ; [CPU_] |1282| 
        B         $C$L94,NEQ            ; [CPU_] |1282| 
        ; branchcc occurs ; [] |1282| 
	.dwpsn	file "../source/CommInterface.c",line 1284,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 1284 | usnMinorCmd = DELFINO_VOLUMETRIC_CHECK_STARTED;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8238         ; [CPU_] |1284| 
	.dwpsn	file "../source/CommInterface.c",line 1285,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1286 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1285| 
        ; branch occurs ; [] |1285| 
$C$L94:    
	.dwpsn	file "../source/CommInterface.c",line 1288,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1288 | usnMinorCmd = DELFINO_VOLUMETRIC_CHECK_COMPLETED;                      
;----------------------------------------------------------------------
        MOV       *-SP[7],#8239         ; [CPU_] |1288| 
	.dwpsn	file "../source/CommInterface.c",line 1290,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1290 | break;                                                                 
; 1293 | case MBD_SLEEP_PROCESS:                                                
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1290| 
        ; branch occurs ; [] |1290| 
$C$L95:    
	.dwpsn	file "../source/CommInterface.c",line 1294,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1294 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1294| 
        CMPB      AL,#1                 ; [CPU_] |1294| 
        B         $C$L96,NEQ            ; [CPU_] |1294| 
        ; branchcc occurs ; [] |1294| 
	.dwpsn	file "../source/CommInterface.c",line 1296,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1296 | usnMinorCmd = DELFINO_SLEEP_SEQUENCE_STARTED;                          
;----------------------------------------------------------------------
        MOV       *-SP[7],#8240         ; [CPU_] |1296| 
	.dwpsn	file "../source/CommInterface.c",line 1297,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1298 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1297| 
        ; branch occurs ; [] |1297| 
$C$L96:    
	.dwpsn	file "../source/CommInterface.c",line 1300,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1300 | usnMinorCmd = DELFINO_SLEEP_SEQUENCE_COMPLETED;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8241         ; [CPU_] |1300| 
	.dwpsn	file "../source/CommInterface.c",line 1302,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1302 | break;                                                                 
; 1304 |     case DELFINO_ERROR_MINOR_CMD:                                      
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1302| 
        ; branch occurs ; [] |1302| 
$C$L97:    
	.dwpsn	file "../source/CommInterface.c",line 1305,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1305 | usnMinorCmd = DELFINO_ERROR_MINOR_CMD;                                 
;----------------------------------------------------------------------
        MOV       *-SP[7],#9217         ; [CPU_] |1305| 
	.dwpsn	file "../source/CommInterface.c",line 1306,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 1306 | break;                                                                 
; 1308 | case MBD_ALL_MOTOR_Y_AXIS_CHECK:                                       
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1306| 
        ; branch occurs ; [] |1306| 
$C$L98:    
	.dwpsn	file "../source/CommInterface.c",line 1309,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1309 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1309| 
        CMPB      AL,#1                 ; [CPU_] |1309| 
        B         $C$L99,NEQ            ; [CPU_] |1309| 
        ; branchcc occurs ; [] |1309| 
	.dwpsn	file "../source/CommInterface.c",line 1311,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1311 | usnMinorCmd = DELFINO_ALL_MOTOR_Y_AXIS_CHECK_SEQUENCE_STARTED;         
;----------------------------------------------------------------------
        MOV       *-SP[7],#8242         ; [CPU_] |1311| 
	.dwpsn	file "../source/CommInterface.c",line 1312,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1313 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1312| 
        ; branch occurs ; [] |1312| 
$C$L99:    
	.dwpsn	file "../source/CommInterface.c",line 1315,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1315 | usnMinorCmd = DELFINO_ALL_MOTOR_Y_AXIS_CHECK_SEQUENCE_COMPLETED;       
;----------------------------------------------------------------------
        MOV       *-SP[7],#8243         ; [CPU_] |1315| 
	.dwpsn	file "../source/CommInterface.c",line 1317,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1317 | break;                                                                 
; 1319 | case MBD_ALL_MOTOR_X_AXIS_CHECK:                                       
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1317| 
        ; branch occurs ; [] |1317| 
$C$L100:    
	.dwpsn	file "../source/CommInterface.c",line 1320,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1320 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1320| 
        CMPB      AL,#1                 ; [CPU_] |1320| 
        B         $C$L101,NEQ           ; [CPU_] |1320| 
        ; branchcc occurs ; [] |1320| 
	.dwpsn	file "../source/CommInterface.c",line 1322,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1322 | usnMinorCmd = DELFINO_ALL_MOTOR_X_AXIS_CHECK_SEQUENCE_STARTED;         
;----------------------------------------------------------------------
        MOV       *-SP[7],#8244         ; [CPU_] |1322| 
	.dwpsn	file "../source/CommInterface.c",line 1323,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1324 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1323| 
        ; branch occurs ; [] |1323| 
$C$L101:    
	.dwpsn	file "../source/CommInterface.c",line 1326,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1326 | usnMinorCmd = DELFINO_ALL_MOTOR_X_AXIS_CHECK_SEQUENCE_COMPLETED;       
;----------------------------------------------------------------------
        MOV       *-SP[7],#8245         ; [CPU_] |1326| 
	.dwpsn	file "../source/CommInterface.c",line 1328,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1328 | break;                                                                 
; 1330 | case MBD_ALL_MOTOR_SAMPLE_CHECK:                                       
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1328| 
        ; branch occurs ; [] |1328| 
$C$L102:    
	.dwpsn	file "../source/CommInterface.c",line 1331,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1331 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1331| 
        CMPB      AL,#1                 ; [CPU_] |1331| 
        B         $C$L103,NEQ           ; [CPU_] |1331| 
        ; branchcc occurs ; [] |1331| 
	.dwpsn	file "../source/CommInterface.c",line 1333,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1333 | usnMinorCmd = DELFINO_ALL_MOTOR_SAMPLE_CHECK_SEQUENCE_STARTED;         
;----------------------------------------------------------------------
        MOV       *-SP[7],#8246         ; [CPU_] |1333| 
	.dwpsn	file "../source/CommInterface.c",line 1334,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1335 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1334| 
        ; branch occurs ; [] |1334| 
$C$L103:    
	.dwpsn	file "../source/CommInterface.c",line 1337,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1337 | usnMinorCmd = DELFINO_ALL_MOTOR_SAMPLE_CHECK_SEQUENCE_COMPLETED;       
;----------------------------------------------------------------------
        MOV       *-SP[7],#8247         ; [CPU_] |1337| 
	.dwpsn	file "../source/CommInterface.c",line 1339,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1339 | break;                                                                 
; 1341 | case MBD_ALL_MOTOR_DILUENT_CHECK:                                      
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1339| 
        ; branch occurs ; [] |1339| 
$C$L104:    
	.dwpsn	file "../source/CommInterface.c",line 1342,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1342 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1342| 
        CMPB      AL,#1                 ; [CPU_] |1342| 
        B         $C$L105,NEQ           ; [CPU_] |1342| 
        ; branchcc occurs ; [] |1342| 
	.dwpsn	file "../source/CommInterface.c",line 1344,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1344 | usnMinorCmd = DELFINO_ALL_MOTOR_DILUENT_CHECK_SEQUENCE_STARTED;        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8248         ; [CPU_] |1344| 
	.dwpsn	file "../source/CommInterface.c",line 1345,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1346 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1345| 
        ; branch occurs ; [] |1345| 
$C$L105:    
	.dwpsn	file "../source/CommInterface.c",line 1348,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1348 | usnMinorCmd = DELFINO_ALL_MOTOR_DILUENT_CHECK_SEQUENCE_COMPLETED;      
;----------------------------------------------------------------------
        MOV       *-SP[7],#8249         ; [CPU_] |1348| 
	.dwpsn	file "../source/CommInterface.c",line 1350,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1350 | break;                                                                 
; 1352 | case MBD_ALL_MOTOR_WASTE_CHECK:                                        
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1350| 
        ; branch occurs ; [] |1350| 
$C$L106:    
	.dwpsn	file "../source/CommInterface.c",line 1353,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1353 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1353| 
        CMPB      AL,#1                 ; [CPU_] |1353| 
        B         $C$L107,NEQ           ; [CPU_] |1353| 
        ; branchcc occurs ; [] |1353| 
	.dwpsn	file "../source/CommInterface.c",line 1355,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1355 | usnMinorCmd = DELFINO_ALL_MOTOR_WASTE_CHECK_SEQUENCE_STARTED;          
;----------------------------------------------------------------------
        MOV       *-SP[7],#8250         ; [CPU_] |1355| 
	.dwpsn	file "../source/CommInterface.c",line 1356,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1357 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1356| 
        ; branch occurs ; [] |1356| 
$C$L107:    
	.dwpsn	file "../source/CommInterface.c",line 1359,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1359 | usnMinorCmd = DELFINO_ALL_MOTOR_WSATE_CHECK_SEQUENCE_COMPLETED;        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8251         ; [CPU_] |1359| 
	.dwpsn	file "../source/CommInterface.c",line 1361,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1361 | break;                                                                 
; 1363 | case MBD_VOL_TUBE_EMPTY:                                               
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1361| 
        ; branch occurs ; [] |1361| 
$C$L108:    
	.dwpsn	file "../source/CommInterface.c",line 1364,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1364 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1364| 
        CMPB      AL,#1                 ; [CPU_] |1364| 
        B         $C$L109,NEQ           ; [CPU_] |1364| 
        ; branchcc occurs ; [] |1364| 
	.dwpsn	file "../source/CommInterface.c",line 1366,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1366 | usnMinorCmd = DELFINO_VOL_TUBE_EMPTY_SEQUENCE_STARTED;                 
;----------------------------------------------------------------------
        MOV       *-SP[7],#8252         ; [CPU_] |1366| 
	.dwpsn	file "../source/CommInterface.c",line 1367,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1368 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1367| 
        ; branch occurs ; [] |1367| 
$C$L109:    
	.dwpsn	file "../source/CommInterface.c",line 1370,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1370 | usnMinorCmd = DELFINO_VOL_TUBE_EMPTY_SEQUENCE_COMPLETED;               
;----------------------------------------------------------------------
        MOV       *-SP[7],#8253         ; [CPU_] |1370| 
	.dwpsn	file "../source/CommInterface.c",line 1372,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1372 | break;                                                                 
; 1374 | case SPI_ERROR_CHECK:                                                  
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1372| 
        ; branch occurs ; [] |1372| 
$C$L110:    
	.dwpsn	file "../source/CommInterface.c",line 1375,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1375 | usnMinorCmd = DELFINO_SPI_ERROR_CHECK;                                 
;----------------------------------------------------------------------
        MOV       *-SP[7],#8738         ; [CPU_] |1375| 
	.dwpsn	file "../source/CommInterface.c",line 1376,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1376 | sprintf(DebugPrintBuf, "\r\n SPI Error Check\n");                      
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL22        ; [CPU_U] |1376| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1376| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1376| 
$C$DW$200	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$200, DW_AT_low_pc(0x00)
	.dwattr $C$DW$200, DW_AT_name("_sprintf")
	.dwattr $C$DW$200, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1376| 
        ; call occurs [#_sprintf] ; [] |1376| 
	.dwpsn	file "../source/CommInterface.c",line 1377,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1377 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1377| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1377| 
$C$DW$201	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$201, DW_AT_low_pc(0x00)
	.dwattr $C$DW$201, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$201, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1377| 
        ; call occurs [#_UartDebugPrint] ; [] |1377| 
	.dwpsn	file "../source/CommInterface.c",line 1378,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1378 | break;                                                                 
; 1380 | case MBD_USER_ABORT_PROCESS:                                           
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1378| 
        ; branch occurs ; [] |1378| 
$C$L111:    
	.dwpsn	file "../source/CommInterface.c",line 1381,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1381 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1381| 
        CMPB      AL,#1                 ; [CPU_] |1381| 
        B         $C$L112,NEQ           ; [CPU_] |1381| 
        ; branchcc occurs ; [] |1381| 
	.dwpsn	file "../source/CommInterface.c",line 1383,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1383 | usnMinorCmd = DELFINO_ABORT_SEQUENCE_STARTED_CMD;                      
;----------------------------------------------------------------------
        MOV       *-SP[7],#8961         ; [CPU_] |1383| 
	.dwpsn	file "../source/CommInterface.c",line 1384,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1384 | sprintf(DebugPrintBuf, "\r\n MBD_USER_ABORT_PROCESS started\n");       
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL23        ; [CPU_U] |1384| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1384| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1384| 
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_name("_sprintf")
	.dwattr $C$DW$202, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1384| 
        ; call occurs [#_sprintf] ; [] |1384| 
	.dwpsn	file "../source/CommInterface.c",line 1385,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1385 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1385| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1385| 
$C$DW$203	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$203, DW_AT_low_pc(0x00)
	.dwattr $C$DW$203, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$203, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1385| 
        ; call occurs [#_UartDebugPrint] ; [] |1385| 
	.dwpsn	file "../source/CommInterface.c",line 1386,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1387 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1386| 
        ; branch occurs ; [] |1386| 
$C$L112:    
	.dwpsn	file "../source/CommInterface.c",line 1389,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1389 | usnMinorCmd = DELFINO_ABORT_SEQUENCE_COMPLETED_CMD;                    
;----------------------------------------------------------------------
        MOV       *-SP[7],#8962         ; [CPU_] |1389| 
	.dwpsn	file "../source/CommInterface.c",line 1390,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1390 | sprintf(DebugPrintBuf, "\r\n MBD_USER_ABORT_PROCESS Completed\n");     
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL24        ; [CPU_U] |1390| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1390| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1390| 
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_name("_sprintf")
	.dwattr $C$DW$204, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1390| 
        ; call occurs [#_sprintf] ; [] |1390| 
	.dwpsn	file "../source/CommInterface.c",line 1391,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1391 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1391| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1391| 
$C$DW$205	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$205, DW_AT_low_pc(0x00)
	.dwattr $C$DW$205, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$205, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1391| 
        ; call occurs [#_UartDebugPrint] ; [] |1391| 
	.dwpsn	file "../source/CommInterface.c",line 1393,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1393 | break;                                                                 
; 1394 | case MBD_BATH_FILL_CMD:                                                
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1393| 
        ; branch occurs ; [] |1393| 
$C$L113:    
	.dwpsn	file "../source/CommInterface.c",line 1395,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1395 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1395| 
        CMPB      AL,#1                 ; [CPU_] |1395| 
        B         $C$L114,NEQ           ; [CPU_] |1395| 
        ; branchcc occurs ; [] |1395| 
	.dwpsn	file "../source/CommInterface.c",line 1397,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1397 | usnMinorCmd = DELFINO_BATH_FILL_SEQUENCE_STARTED_CMD;                  
;----------------------------------------------------------------------
        MOV       *-SP[7],#8255         ; [CPU_] |1397| 
	.dwpsn	file "../source/CommInterface.c",line 1398,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1399 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1398| 
        ; branch occurs ; [] |1398| 
$C$L114:    
	.dwpsn	file "../source/CommInterface.c",line 1401,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1401 | usnMinorCmd = DELFINO_BATH_FILL_SEQUENCE_COMPLETED_CMD;                
;----------------------------------------------------------------------
        MOV       *-SP[7],#8256         ; [CPU_] |1401| 
	.dwpsn	file "../source/CommInterface.c",line 1403,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1403 | break;                                                                 
; 1404 | case MBD_USER_ABORT_Y_AXIS_PROCESS:                                    
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1403| 
        ; branch occurs ; [] |1403| 
$C$L115:    
	.dwpsn	file "../source/CommInterface.c",line 1405,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1405 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1405| 
        CMPB      AL,#1                 ; [CPU_] |1405| 
        B         $C$L116,NEQ           ; [CPU_] |1405| 
        ; branchcc occurs ; [] |1405| 
	.dwpsn	file "../source/CommInterface.c",line 1407,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1407 | usnMinorCmd = DELFINO_Y_AXIS_ABORT_SEQUENCE_STARTED_CMD;               
;----------------------------------------------------------------------
        MOV       *-SP[7],#8963         ; [CPU_] |1407| 
	.dwpsn	file "../source/CommInterface.c",line 1408,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1409 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1408| 
        ; branch occurs ; [] |1408| 
$C$L116:    
	.dwpsn	file "../source/CommInterface.c",line 1411,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1411 | usnMinorCmd = DELFINO_Y_AXIS_ABORT_SEQUENCE_COMPLETED_CMD;             
;----------------------------------------------------------------------
        MOV       *-SP[7],#8964         ; [CPU_] |1411| 
	.dwpsn	file "../source/CommInterface.c",line 1413,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1413 | break;                                                                 
; 1414 | case MBD_ZAP_INITIATE:                                                 
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1413| 
        ; branch occurs ; [] |1413| 
$C$L117:    
	.dwpsn	file "../source/CommInterface.c",line 1415,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1415 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1415| 
        CMPB      AL,#1                 ; [CPU_] |1415| 
        B         $C$L118,NEQ           ; [CPU_] |1415| 
        ; branchcc occurs ; [] |1415| 
	.dwpsn	file "../source/CommInterface.c",line 1417,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1417 | usnMinorCmd = DELFINO_ZAP_INITIATE_STARTED_CMD;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8257         ; [CPU_] |1417| 
	.dwpsn	file "../source/CommInterface.c",line 1418,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 1419 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1418| 
        ; branch occurs ; [] |1418| 
$C$L118:    
	.dwpsn	file "../source/CommInterface.c",line 1421,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 1421 | usnMinorCmd = DELFINO_ZAP_INITIATE_COMPLETED_CMD;                      
;----------------------------------------------------------------------
        MOV       *-SP[7],#8258         ; [CPU_] |1421| 
	.dwpsn	file "../source/CommInterface.c",line 1423,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1423 | break;                                                                 
; 1424 | case  GLASS_TUBE_FILLING:                                              
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1423| 
        ; branch occurs ; [] |1423| 
$C$L119:    
	.dwpsn	file "../source/CommInterface.c",line 1425,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1425 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1425| 
        CMPB      AL,#1                 ; [CPU_] |1425| 
        B         $C$L120,NEQ           ; [CPU_] |1425| 
        ; branchcc occurs ; [] |1425| 
	.dwpsn	file "../source/CommInterface.c",line 1427,column 16,is_stmt,isa 0
;----------------------------------------------------------------------
; 1427 | usnMinorCmd = GLASS_TUBE_FILLING_STARTED;                              
;----------------------------------------------------------------------
        MOV       *-SP[7],#8261         ; [CPU_] |1427| 
	.dwpsn	file "../source/CommInterface.c",line 1428,column 16,is_stmt,isa 0
;----------------------------------------------------------------------
; 1428 | sprintf(DebugPrintBuf, "\r\n GLASS_TUBE_FILLING_STARTED\n");           
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL25        ; [CPU_U] |1428| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1428| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1428| 
$C$DW$206	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$206, DW_AT_low_pc(0x00)
	.dwattr $C$DW$206, DW_AT_name("_sprintf")
	.dwattr $C$DW$206, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1428| 
        ; call occurs [#_sprintf] ; [] |1428| 
	.dwpsn	file "../source/CommInterface.c",line 1429,column 33,is_stmt,isa 0
;----------------------------------------------------------------------
; 1429 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1429| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1429| 
$C$DW$207	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$207, DW_AT_low_pc(0x00)
	.dwattr $C$DW$207, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$207, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1429| 
        ; call occurs [#_UartDebugPrint] ; [] |1429| 
	.dwpsn	file "../source/CommInterface.c",line 1430,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1431 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1430| 
        ; branch occurs ; [] |1430| 
$C$L120:    
	.dwpsn	file "../source/CommInterface.c",line 1433,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1433 | usnMinorCmd = GLASS_TUBE_FILLING_COMPLETED;                            
;----------------------------------------------------------------------
        MOV       *-SP[7],#8262         ; [CPU_] |1433| 
	.dwpsn	file "../source/CommInterface.c",line 1434,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1434 | sprintf(DebugPrintBuf, "\r\n GLASS_TUBE_FILLING_COMPLETED\n");         
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL26        ; [CPU_U] |1434| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1434| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1434| 
$C$DW$208	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$208, DW_AT_low_pc(0x00)
	.dwattr $C$DW$208, DW_AT_name("_sprintf")
	.dwattr $C$DW$208, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1434| 
        ; call occurs [#_sprintf] ; [] |1434| 
	.dwpsn	file "../source/CommInterface.c",line 1435,column 34,is_stmt,isa 0
;----------------------------------------------------------------------
; 1435 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1435| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1435| 
$C$DW$209	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$209, DW_AT_low_pc(0x00)
	.dwattr $C$DW$209, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$209, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1435| 
        ; call occurs [#_UartDebugPrint] ; [] |1435| 
	.dwpsn	file "../source/CommInterface.c",line 1437,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1437 | break;                                                                 
; 1438 | case COUNT_FAIL:                                                       
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1437| 
        ; branch occurs ; [] |1437| 
$C$L121:    
	.dwpsn	file "../source/CommInterface.c",line 1439,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1439 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1439| 
        CMPB      AL,#1                 ; [CPU_] |1439| 
        B         $C$L122,NEQ           ; [CPU_] |1439| 
        ; branchcc occurs ; [] |1439| 
	.dwpsn	file "../source/CommInterface.c",line 1441,column 16,is_stmt,isa 0
;----------------------------------------------------------------------
; 1441 | usnMinorCmd = COUNT_FAIL_SEQUNCE_STARTED;                              
;----------------------------------------------------------------------
        MOV       *-SP[7],#8263         ; [CPU_] |1441| 
	.dwpsn	file "../source/CommInterface.c",line 1442,column 16,is_stmt,isa 0
;----------------------------------------------------------------------
; 1442 | sprintf(DebugPrintBuf, "\r\n COUNT_FAIL_SEQUNCE_STARTED\n");           
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL27        ; [CPU_U] |1442| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1442| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1442| 
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_name("_sprintf")
	.dwattr $C$DW$210, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1442| 
        ; call occurs [#_sprintf] ; [] |1442| 
	.dwpsn	file "../source/CommInterface.c",line 1443,column 33,is_stmt,isa 0
;----------------------------------------------------------------------
; 1443 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1443| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1443| 
$C$DW$211	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$211, DW_AT_low_pc(0x00)
	.dwattr $C$DW$211, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$211, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1443| 
        ; call occurs [#_UartDebugPrint] ; [] |1443| 
	.dwpsn	file "../source/CommInterface.c",line 1444,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1445 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1444| 
        ; branch occurs ; [] |1444| 
$C$L122:    
	.dwpsn	file "../source/CommInterface.c",line 1447,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1447 | usnMinorCmd = COUNT_FAIL_SEQUNCE_COMPLETED;                            
;----------------------------------------------------------------------
        MOV       *-SP[7],#8264         ; [CPU_] |1447| 
	.dwpsn	file "../source/CommInterface.c",line 1448,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1448 | sprintf(DebugPrintBuf, "\r\n COUNT_FAIL_SEQUNCE_COMPLETED\n");         
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL28        ; [CPU_U] |1448| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1448| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1448| 
$C$DW$212	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$212, DW_AT_low_pc(0x00)
	.dwattr $C$DW$212, DW_AT_name("_sprintf")
	.dwattr $C$DW$212, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1448| 
        ; call occurs [#_sprintf] ; [] |1448| 
	.dwpsn	file "../source/CommInterface.c",line 1449,column 34,is_stmt,isa 0
;----------------------------------------------------------------------
; 1449 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1449| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1449| 
$C$DW$213	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$213, DW_AT_low_pc(0x00)
	.dwattr $C$DW$213, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$213, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1449| 
        ; call occurs [#_UartDebugPrint] ; [] |1449| 
	.dwpsn	file "../source/CommInterface.c",line 1451,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1451 | break;                                                                 
; 1452 | case HYPO_CLEANER_FAIL:                                                
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1451| 
        ; branch occurs ; [] |1451| 
$C$L123:    
	.dwpsn	file "../source/CommInterface.c",line 1453,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1453 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1453| 
        CMPB      AL,#1                 ; [CPU_] |1453| 
        B         $C$L124,NEQ           ; [CPU_] |1453| 
        ; branchcc occurs ; [] |1453| 
	.dwpsn	file "../source/CommInterface.c",line 1455,column 16,is_stmt,isa 0
;----------------------------------------------------------------------
; 1455 | usnMinorCmd = HYPO_CLEANER_FAIL_SEQUNCE_STARTED;                       
;----------------------------------------------------------------------
        MOV       *-SP[7],#8265         ; [CPU_] |1455| 
	.dwpsn	file "../source/CommInterface.c",line 1456,column 16,is_stmt,isa 0
;----------------------------------------------------------------------
; 1456 | sprintf(DebugPrintBuf, "\r\n HYPO_CLEANER_FAIL_SEQUNCE_STARTED\n");    
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL29        ; [CPU_U] |1456| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1456| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1456| 
$C$DW$214	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$214, DW_AT_low_pc(0x00)
	.dwattr $C$DW$214, DW_AT_name("_sprintf")
	.dwattr $C$DW$214, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1456| 
        ; call occurs [#_sprintf] ; [] |1456| 
	.dwpsn	file "../source/CommInterface.c",line 1457,column 33,is_stmt,isa 0
;----------------------------------------------------------------------
; 1457 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1457| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1457| 
$C$DW$215	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$215, DW_AT_low_pc(0x00)
	.dwattr $C$DW$215, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$215, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1457| 
        ; call occurs [#_UartDebugPrint] ; [] |1457| 
	.dwpsn	file "../source/CommInterface.c",line 1458,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1459 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1458| 
        ; branch occurs ; [] |1458| 
$C$L124:    
	.dwpsn	file "../source/CommInterface.c",line 1461,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1461 | usnMinorCmd = HYPO_CLEANER_FAIL_SEQUNCE_COMPLETED;                     
;----------------------------------------------------------------------
        MOV       *-SP[7],#8272         ; [CPU_] |1461| 
	.dwpsn	file "../source/CommInterface.c",line 1462,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1462 | sprintf(DebugPrintBuf, "\r\n HYPO_CLEANER_FAIL_SEQUNCE_COMPLETED\n");  
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL30        ; [CPU_U] |1462| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1462| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1462| 
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_name("_sprintf")
	.dwattr $C$DW$216, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1462| 
        ; call occurs [#_sprintf] ; [] |1462| 
	.dwpsn	file "../source/CommInterface.c",line 1463,column 34,is_stmt,isa 0
;----------------------------------------------------------------------
; 1463 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1463| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1463| 
$C$DW$217	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$217, DW_AT_low_pc(0x00)
	.dwattr $C$DW$217, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$217, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1463| 
        ; call occurs [#_UartDebugPrint] ; [] |1463| 
	.dwpsn	file "../source/CommInterface.c",line 1465,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1465 | break;                                                                 
; 1467 |     default:                                                           
; 1468 |         //!In default scenario, set the minor command to null          
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1465| 
        ; branch occurs ; [] |1465| 
$C$L125:    
	.dwpsn	file "../source/CommInterface.c",line 1469,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1469 | usnMinorCmd = NULL;//MBD_UPDATION                                      
;----------------------------------------------------------------------
        MOV       *-SP[7],#0            ; [CPU_] |1469| 
	.dwpsn	file "../source/CommInterface.c",line 1470,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 1470 | break;                                                                 
; 1472 | }//end of switch case                                                  
; 1473 | //!If the minor command is set, then frame the Service handling packet
;     | and                                                                    
; 1474 | //!send to the processor                                               
;----------------------------------------------------------------------
        B         $C$L130,UNC           ; [CPU_] |1470| 
        ; branch occurs ; [] |1470| 
$C$L126:    
	.dwpsn	file "../source/CommInterface.c",line 841,column 2,is_stmt,isa 0
        MOVZ      AR6,*-SP[5]           ; [CPU_] |841| 
        CMP       AR6,#9217             ; [CPU_] |841| 
        B         $C$L127,GT            ; [CPU_] |841| 
        ; branchcc occurs ; [] |841| 
        CMP       AR6,#9217             ; [CPU_] |841| 
        B         $C$L97,EQ             ; [CPU_] |841| 
        ; branchcc occurs ; [] |841| 
        MOVZ      AR7,AR6               ; [CPU_] |841| 
        MOVL      XAR4,#8201            ; [CPU_U] |841| 
        MOVL      ACC,XAR4              ; [CPU_] |841| 
        CMPL      ACC,XAR7              ; [CPU_] |841| 
        B         $C$L91,EQ             ; [CPU_] |841| 
        ; branchcc occurs ; [] |841| 
        MOVZ      AR6,AR6               ; [CPU_] |841| 
        MOVL      XAR4,#8204            ; [CPU_U] |841| 
        MOVL      ACC,XAR4              ; [CPU_] |841| 
        CMPL      ACC,XAR6              ; [CPU_] |841| 
        B         $C$L90,EQ             ; [CPU_] |841| 
        ; branchcc occurs ; [] |841| 
        B         $C$L125,UNC           ; [CPU_] |841| 
        ; branch occurs ; [] |841| 
$C$L127:    
        MOV       AL,AR6                ; [CPU_] |841| 
        SUB       AL,#20481             ; [CPU_] |841| 
        CMPB      AL,#28                ; [CPU_] |841| 
        B         $C$L128,LOS           ; [CPU_] |841| 
        ; branchcc occurs ; [] |841| 
        MOV       AL,AR6                ; [CPU_] |841| 
        SUB       AL,#20520             ; [CPU_] |841| 
        CMPB      AL,#23                ; [CPU_] |841| 
        B         $C$L129,LOS           ; [CPU_] |841| 
        ; branchcc occurs ; [] |841| 
        B         $C$L125,UNC           ; [CPU_] |841| 
        ; branch occurs ; [] |841| 
$C$L128:    
        SUB       AR6,#20481            ; [CPU_] |841| 
        MOV       ACC,AR6 << #1         ; [CPU_] |841| 
        MOVZ      AR6,AL                ; [CPU_] |841| 
        MOVL      XAR7,#$C$SW4          ; [CPU_U] |841| 
        MOVL      ACC,XAR7              ; [CPU_] |841| 
        ADDU      ACC,AR6               ; [CPU_] |841| 
        MOVL      XAR7,ACC              ; [CPU_] |841| 
        MOVL      XAR7,*XAR7            ; [CPU_] |841| 
        LB        *XAR7                 ; [CPU_] |841| 
        ; branch occurs ; [] |841| 
	.sect	".switch:_CIFormServiceHandlingPacket"
	.clink
$C$SW4:	.long	$C$L48	; 20481
	.long	$C$L50	; 20482
	.long	$C$L52	; 20483
	.long	$C$L54	; 20484
	.long	$C$L56	; 20485
	.long	$C$L58	; 20486
	.long	$C$L125	; 0
	.long	$C$L60	; 20488
	.long	$C$L62	; 20489
	.long	$C$L64	; 20490
	.long	$C$L66	; 20491
	.long	$C$L68	; 20492
	.long	$C$L69	; 20493
	.long	$C$L70	; 20494
	.long	$C$L125	; 0
	.long	$C$L125	; 0
	.long	$C$L125	; 0
	.long	$C$L125	; 0
	.long	$C$L84	; 20499
	.long	$C$L92	; 20500
	.long	$C$L72	; 20501
	.long	$C$L73	; 20502
	.long	$C$L125	; 0
	.long	$C$L125	; 0
	.long	$C$L74	; 20505
	.long	$C$L76	; 20506
	.long	$C$L78	; 20507
	.long	$C$L80	; 20508
	.long	$C$L82	; 20509
	.sect	".text:_CIFormServiceHandlingPacket"
$C$L129:    
        SUB       AR6,#20520            ; [CPU_] |841| 
        MOV       ACC,AR6 << #1         ; [CPU_] |841| 
        MOVZ      AR6,AL                ; [CPU_] |841| 
        MOVL      XAR7,#$C$SW5          ; [CPU_U] |841| 
        MOVL      ACC,XAR7              ; [CPU_] |841| 
        ADDU      ACC,AR6               ; [CPU_] |841| 
        MOVL      XAR7,ACC              ; [CPU_] |841| 
        MOVL      XAR7,*XAR7            ; [CPU_] |841| 
        LB        *XAR7                 ; [CPU_] |841| 
        ; branch occurs ; [] |841| 
	.sect	".switch:_CIFormServiceHandlingPacket"
	.clink
$C$SW5:	.long	$C$L111	; 20520
	.long	$C$L86	; 20521
	.long	$C$L88	; 20522
	.long	$C$L125	; 0
	.long	$C$L125	; 0
	.long	$C$L115	; 20525
	.long	$C$L93	; 20526
	.long	$C$L95	; 20527
	.long	$C$L98	; 20528
	.long	$C$L100	; 20529
	.long	$C$L102	; 20530
	.long	$C$L104	; 20531
	.long	$C$L106	; 20532
	.long	$C$L125	; 0
	.long	$C$L108	; 20534
	.long	$C$L110	; 20535
	.long	$C$L113	; 20536
	.long	$C$L92	; 20537
	.long	$C$L58	; 20538
	.long	$C$L117	; 20539
	.long	$C$L92	; 20540
	.long	$C$L121	; 20541
	.long	$C$L119	; 20542
	.long	$C$L123	; 20543
	.sect	".text:_CIFormServiceHandlingPacket"
$C$L130:    
	.dwpsn	file "../source/CommInterface.c",line 1475,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 1475 | if(usnMinorCmd != NULL)                                                
;----------------------------------------------------------------------
        MOV       AL,*-SP[7]            ; [CPU_] |1475| 
        B         $C$L131,EQ            ; [CPU_] |1475| 
        ; branchcc occurs ; [] |1475| 
	.dwpsn	file "../source/CommInterface.c",line 1477,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 1477 | CPFrameDataPacket(DELFINO_SERVICE_HANDLING,usnMinorCmd, FALSE);        
;----------------------------------------------------------------------
        MOV       AL,#8196              ; [CPU_] |1477| 
        MOV       AH,*-SP[7]            ; [CPU_] |1477| 
        MOVB      XAR4,#0               ; [CPU_] |1477| 
$C$DW$218	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$218, DW_AT_low_pc(0x00)
	.dwattr $C$DW$218, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$218, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |1477| 
        ; call occurs [#_CPFrameDataPacket] ; [] |1477| 
	.dwpsn	file "../source/CommInterface.c",line 1479,column 1,is_stmt,isa 0
$C$L131:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$219	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$219, DW_AT_low_pc(0x00)
	.dwattr $C$DW$219, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$170, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$170, DW_AT_TI_end_line(0x5c7)
	.dwattr $C$DW$170, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$170

	.sect	".text:_CIFormStartupHandlingPacket"
	.clink
	.global	_CIFormStartupHandlingPacket

$C$DW$220	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$220, DW_AT_name("CIFormStartupHandlingPacket")
	.dwattr $C$DW$220, DW_AT_low_pc(_CIFormStartupHandlingPacket)
	.dwattr $C$DW$220, DW_AT_high_pc(0x00)
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_CIFormStartupHandlingPacket")
	.dwattr $C$DW$220, DW_AT_external
	.dwattr $C$DW$220, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$220, DW_AT_TI_begin_line(0x5d3)
	.dwattr $C$DW$220, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$220, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../source/CommInterface.c",line 1492,column 1,is_stmt,address _CIFormStartupHandlingPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormStartupHandlingPacket
$C$DW$221	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$221, DW_AT_name("usnServicehandled")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_usnServicehandled")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_reg0]

$C$DW$222	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$222, DW_AT_name("bStartEnd")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_bStartEnd")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg1]

;----------------------------------------------------------------------
; 1491 | void CIFormStartupHandlingPacket(uint16_t usnServicehandled, bool_t bSt
;     | artEnd)                                                                
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormStartupHandlingPacket  FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CIFormStartupHandlingPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$223	.dwtag  DW_TAG_variable
	.dwattr $C$DW$223, DW_AT_name("usnServicehandled")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_usnServicehandled")
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$223, DW_AT_location[DW_OP_breg20 -5]

$C$DW$224	.dwtag  DW_TAG_variable
	.dwattr $C$DW$224, DW_AT_name("bStartEnd")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_bStartEnd")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_breg20 -6]

$C$DW$225	.dwtag  DW_TAG_variable
	.dwattr $C$DW$225, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$225, DW_AT_location[DW_OP_breg20 -7]

;----------------------------------------------------------------------
; 1493 | uint16_t usnMinorCmd;                                                  
; 1495 | //!During startup handling, starting of the startup handling and end of
; 1496 | //!startup handling to be sent to processor.                           
; 1497 | //!\var bStartEnd is used to identify whether the startup handling is  
; 1498 | //!starting or ending                                                  
; 1499 | //!If bStartEnd is TRUE, startup handling is starting and if bStartEnd
;     | is                                                                     
; 1500 | //!FALSE, startup handling is ending.                                  
; 1502 | //!If the startup handling is starting (bStartEnd is true), set the bus
;     | y                                                                      
; 1503 | //!pin to high to indicate processor that controller is busy in running
;     |  the sequence.                                                         
; 1504 | //!else (bStartEnd is false), clear the busy pin to indicate controller
;     |  is                                                                    
; 1505 | //!not busy and the current sequence is ended.                         
;----------------------------------------------------------------------
        MOV       *-SP[6],AH            ; [CPU_] |1492| 
        MOV       *-SP[5],AL            ; [CPU_] |1492| 
	.dwpsn	file "../source/CommInterface.c",line 1506,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1506 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1506| 
        CMPB      AL,#1                 ; [CPU_] |1506| 
        B         $C$L132,NEQ           ; [CPU_] |1506| 
        ; branchcc occurs ; [] |1506| 
	.dwpsn	file "../source/CommInterface.c",line 1508,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1508 | if(g_bBusyBitStatus == FALSE)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bBusyBitStatus ; [CPU_U] 
        MOV       AL,@_g_bBusyBitStatus ; [CPU_] |1508| 
        B         $C$L152,NEQ           ; [CPU_] |1508| 
        ; branchcc occurs ; [] |1508| 
	.dwpsn	file "../source/CommInterface.c",line 1510,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1510 | SPISetCommCtrlBusyInt();                                               
;----------------------------------------------------------------------
$C$DW$226	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$226, DW_AT_low_pc(0x00)
	.dwattr $C$DW$226, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$226, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |1510| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |1510| 
	.dwpsn	file "../source/CommInterface.c",line 1512,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1513 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L152,UNC           ; [CPU_] |1512| 
        ; branch occurs ; [] |1512| 
$C$L132:    
	.dwpsn	file "../source/CommInterface.c",line 1515,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1515 | if(g_bBusyBitStatus == TRUE)                                           
;----------------------------------------------------------------------
        MOVW      DP,#_g_bBusyBitStatus ; [CPU_U] 
        MOV       AL,@_g_bBusyBitStatus ; [CPU_] |1515| 
        CMPB      AL,#1                 ; [CPU_] |1515| 
        B         $C$L152,NEQ           ; [CPU_] |1515| 
        ; branchcc occurs ; [] |1515| 
	.dwpsn	file "../source/CommInterface.c",line 1517,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1517 | SPIClearCommCtrlBusyInt();                                             
; 1520 | //!Check for the current startup handling that was initiated           
;----------------------------------------------------------------------
$C$DW$227	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$227, DW_AT_low_pc(0x00)
	.dwattr $C$DW$227, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$227, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |1517| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |1517| 
	.dwpsn	file "../source/CommInterface.c",line 1521,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 1521 | switch(usnServicehandled)                                              
; 1524 |     //!If the data request from processor is System status, no sequence
;     |  is                                                                    
; 1525 |     //!involved, it is only sending the system status to processor.    
; 1526 |     //!So minor command is set to System Status                        
; 1527 |     case SYSTEM_STATUS_SERVICE_HANDLING:                               
;----------------------------------------------------------------------
        B         $C$L152,UNC           ; [CPU_] |1521| 
        ; branch occurs ; [] |1521| 
$C$L133:    
	.dwpsn	file "../source/CommInterface.c",line 1528,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1528 | usnMinorCmd = DELFINO_SYSTEM_STATUS_DATA;                              
;----------------------------------------------------------------------
        MOV       *-SP[7],#8233         ; [CPU_] |1528| 
	.dwpsn	file "../source/CommInterface.c",line 1530,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1530 | break;                                                                 
; 1531 | case BACK_FLUSH_SERVICE_HANDLING:                                      
; 1532 | case MBD_STARTUP_BACKFLUSH_CMD:                                        
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1530| 
        ; branch occurs ; [] |1530| 
$C$L134:    
	.dwpsn	file "../source/CommInterface.c",line 1533,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1533 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1533| 
        CMPB      AL,#1                 ; [CPU_] |1533| 
        B         $C$L135,NEQ           ; [CPU_] |1533| 
        ; branchcc occurs ; [] |1533| 
	.dwpsn	file "../source/CommInterface.c",line 1535,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1535 | usnMinorCmd = DELFINO_BACK_FLUSH_STARTED_CMD;                          
;----------------------------------------------------------------------
        MOV       *-SP[7],#8220         ; [CPU_] |1535| 
	.dwpsn	file "../source/CommInterface.c",line 1536,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1537 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1536| 
        ; branch occurs ; [] |1536| 
$C$L135:    
	.dwpsn	file "../source/CommInterface.c",line 1539,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1539 | usnMinorCmd = DELFINO_BACK_FLUSH_COMPLETED_CMD;                        
;----------------------------------------------------------------------
        MOV       *-SP[7],#8197         ; [CPU_] |1539| 
	.dwpsn	file "../source/CommInterface.c",line 1540,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1540 | sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_COMPLETED_CMD : %ld\n", g
;     | _unSequenceStateTime1);                                                
;----------------------------------------------------------------------
        MOVW      DP,#_g_unSequenceStateTime1 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL14        ; [CPU_U] |1540| 
        MOVL      ACC,@_g_unSequenceStateTime1 ; [CPU_] |1540| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1540| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1540| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1540| 
$C$DW$228	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$228, DW_AT_low_pc(0x00)
	.dwattr $C$DW$228, DW_AT_name("_sprintf")
	.dwattr $C$DW$228, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1540| 
        ; call occurs [#_sprintf] ; [] |1540| 
	.dwpsn	file "../source/CommInterface.c",line 1541,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1541 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1541| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1541| 
$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$229, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1541| 
        ; call occurs [#_UartDebugPrint] ; [] |1541| 
	.dwpsn	file "../source/CommInterface.c",line 1543,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1543 | break;                                                                 
; 1545 | //!If the startup handling initiated is Prime All and if the sequence  
; 1546 | //!is starting, set the \var usnMinorCmd to Prime All started          
; 1547 | //!If the sequence is ending, set the \var usnMinorCmd to Prime ALL    
; 1548 | //!completed                                                           
; 1549 | case PRIME_ALL_SERVICE_HANDLING:                                       
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1543| 
        ; branch occurs ; [] |1543| 
$C$L136:    
	.dwpsn	file "../source/CommInterface.c",line 1550,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1550 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1550| 
        CMPB      AL,#1                 ; [CPU_] |1550| 
        B         $C$L137,NEQ           ; [CPU_] |1550| 
        ; branchcc occurs ; [] |1550| 
	.dwpsn	file "../source/CommInterface.c",line 1552,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1552 | usnMinorCmd = DELFINO_PRIME_ALL_STARTED_CMD;                           
; 1554 | //!Disable the Diluent Prime monitoring                                
;----------------------------------------------------------------------
        MOV       *-SP[7],#8219         ; [CPU_] |1552| 
	.dwpsn	file "../source/CommInterface.c",line 1555,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1555 | g_bMonDiluentPriming = FALSE;                                          
; 1557 | //!Disable the Lyse Prime monitoring                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOV       @_g_bMonDiluentPriming,#0 ; [CPU_] |1555| 
	.dwpsn	file "../source/CommInterface.c",line 1558,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1558 | g_bMonLysePriming = FALSE;                                             
; 1560 | //!Disable the Rinse Prime monitoring                                  
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOV       @_g_bMonLysePriming,#0 ; [CPU_] |1558| 
	.dwpsn	file "../source/CommInterface.c",line 1561,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1561 | g_bMonRinsePriming = FALSE;                                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOV       @_g_bMonRinsePriming,#0 ; [CPU_] |1561| 
	.dwpsn	file "../source/CommInterface.c",line 1563,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1564 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1563| 
        ; branch occurs ; [] |1563| 
$C$L137:    
	.dwpsn	file "../source/CommInterface.c",line 1566,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1566 | usnMinorCmd = DELFINO_PRIME_ALL_COMPLETED_CMD;                         
; 1568 | //!Enable the Rinse Prime monitoring                                   
;----------------------------------------------------------------------
        MOV       *-SP[7],#8196         ; [CPU_] |1566| 
	.dwpsn	file "../source/CommInterface.c",line 1569,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1569 | g_bMonRinsePriming = TRUE;                                             
; 1571 | //!Clear the Diluent prime error                                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOVB      @_g_bMonRinsePriming,#1,UNC ; [CPU_] |1569| 
	.dwpsn	file "../source/CommInterface.c",line 1572,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1572 | utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;                  
; 1574 | //!Enable the Lyse Prime monitoring                                    
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       @_utErrorInfoCheck+2,#0xffdf ; [CPU_] |1572| 
	.dwpsn	file "../source/CommInterface.c",line 1575,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1575 | g_bMonLysePriming = TRUE;                                              
; 1577 | //!Clear the lyse prime error                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOVB      @_g_bMonLysePriming,#1,UNC ; [CPU_] |1575| 
	.dwpsn	file "../source/CommInterface.c",line 1578,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1578 | utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;                     
; 1580 | //!Enable the Diluent Prime monitoring                                 
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       @_utErrorInfoCheck+2,#0xffbf ; [CPU_] |1578| 
	.dwpsn	file "../source/CommInterface.c",line 1581,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1581 | g_bMonDiluentPriming = TRUE;                                           
; 1583 | //!Clear Rinse prime error                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOVB      @_g_bMonDiluentPriming,#1,UNC ; [CPU_] |1581| 
	.dwpsn	file "../source/CommInterface.c",line 1584,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1584 | utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;                    
;----------------------------------------------------------------------
        MOVW      DP,#_utErrorInfoCheck+2 ; [CPU_U] 
        AND       @_utErrorInfoCheck+2,#0xff7f ; [CPU_] |1584| 
	.dwpsn	file "../source/CommInterface.c",line 1586,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1586 | sprintf(DebugPrintBuf, "\n DELFINO_PRIME_ALL_COMPLETED_CMD : %ld\n", g_
;     | unSequenceStateTime1);                                                 
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL31        ; [CPU_U] |1586| 
        MOVW      DP,#_g_unSequenceStateTime1 ; [CPU_U] 
        MOVL      ACC,@_g_unSequenceStateTime1 ; [CPU_] |1586| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1586| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1586| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1586| 
$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_name("_sprintf")
	.dwattr $C$DW$230, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1586| 
        ; call occurs [#_sprintf] ; [] |1586| 
	.dwpsn	file "../source/CommInterface.c",line 1587,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1587 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1587| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1587| 
$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$231, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1587| 
        ; call occurs [#_UartDebugPrint] ; [] |1587| 
	.dwpsn	file "../source/CommInterface.c",line 1589,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 1589 | break;                                                                 
; 1591 | //SAM_MOTOR_CHK -  START                                               
; 1592 | //!If the startup handling initiated is X-Motor Check, send only the   
; 1593 | //!sequence completed status to the processor.                         
; 1594 | case X_AXIS_MOTOR_CHECK:                                               
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1589| 
        ; branch occurs ; [] |1589| 
$C$L138:    
	.dwpsn	file "../source/CommInterface.c",line 1595,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1595 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1595| 
        CMPB      AL,#1                 ; [CPU_] |1595| 
        B         $C$L139,NEQ           ; [CPU_] |1595| 
        ; branchcc occurs ; [] |1595| 
	.dwpsn	file "../source/CommInterface.c",line 1597,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1597 | usnMinorCmd = DELFINO_X_AXIS_MOTOR_CHK_STARTED_CMD;                    
;----------------------------------------------------------------------
        MOV       *-SP[7],#8227         ; [CPU_] |1597| 
	.dwpsn	file "../source/CommInterface.c",line 1598,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1599 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1598| 
        ; branch occurs ; [] |1598| 
$C$L139:    
	.dwpsn	file "../source/CommInterface.c",line 1601,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1601 | usnMinorCmd = DELFINO_X_AXIS_MOTOR_CHK_COMPLETED_CMD;                  
;----------------------------------------------------------------------
        MOV       *-SP[7],#8209         ; [CPU_] |1601| 
	.dwpsn	file "../source/CommInterface.c",line 1603,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1603 | break;                                                                 
; 1605 | //!If the startup handling initiated is Y-Motor Check, send only the   
; 1606 | //!sequence completed status to the processor.                         
; 1607 | case Y_AXIS_MOTOR_CHECK:                                               
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1603| 
        ; branch occurs ; [] |1603| 
$C$L140:    
	.dwpsn	file "../source/CommInterface.c",line 1608,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1608 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1608| 
        CMPB      AL,#1                 ; [CPU_] |1608| 
        B         $C$L141,NEQ           ; [CPU_] |1608| 
        ; branchcc occurs ; [] |1608| 
	.dwpsn	file "../source/CommInterface.c",line 1610,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1610 | usnMinorCmd = DELFINO_Y_AXIS_MOTOR_CHK_STARTED_CMD;                    
;----------------------------------------------------------------------
        MOV       *-SP[7],#8228         ; [CPU_] |1610| 
	.dwpsn	file "../source/CommInterface.c",line 1611,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1612 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1611| 
        ; branch occurs ; [] |1611| 
$C$L141:    
	.dwpsn	file "../source/CommInterface.c",line 1614,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1614 | usnMinorCmd = DELFINO_Y_AXIS_MOTOR_CHK_COMPLETED_CMD;                  
;----------------------------------------------------------------------
        MOV       *-SP[7],#8210         ; [CPU_] |1614| 
	.dwpsn	file "../source/CommInterface.c",line 1616,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1616 | break;                                                                 
; 1618 | //!If the startup handling initiated is Diluent Motor Check, send only
;     | the                                                                    
; 1619 | //!sequence completed status to the processor.                         
; 1620 | case DILUENT_SYRINGE_MOTOR_CHECK:                                      
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1616| 
        ; branch occurs ; [] |1616| 
$C$L142:    
	.dwpsn	file "../source/CommInterface.c",line 1621,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1621 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1621| 
        CMPB      AL,#1                 ; [CPU_] |1621| 
        B         $C$L143,NEQ           ; [CPU_] |1621| 
        ; branchcc occurs ; [] |1621| 
	.dwpsn	file "../source/CommInterface.c",line 1623,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1623 | usnMinorCmd = DELFINO_DILUENT_SYRINGE_MOTOR_CHK_STARTED_CMD;           
;----------------------------------------------------------------------
        MOV       *-SP[7],#8229         ; [CPU_] |1623| 
	.dwpsn	file "../source/CommInterface.c",line 1624,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1625 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1624| 
        ; branch occurs ; [] |1624| 
$C$L143:    
	.dwpsn	file "../source/CommInterface.c",line 1627,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1627 | usnMinorCmd = DELFINO_DILUENT_SYRINGE_MOTOR_CHK_COMPLETED_CMD;         
;----------------------------------------------------------------------
        MOV       *-SP[7],#8211         ; [CPU_] |1627| 
	.dwpsn	file "../source/CommInterface.c",line 1629,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1629 | break;                                                                 
; 1631 | //!If the startup handling initiated is Waste Motor Check, send only th
;     | e                                                                      
; 1632 | //!sequence completed status to the processor.                         
; 1633 | case WASTE_SYRINGE_MOTOR_CHECK:                                        
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1629| 
        ; branch occurs ; [] |1629| 
$C$L144:    
	.dwpsn	file "../source/CommInterface.c",line 1634,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1634 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1634| 
        CMPB      AL,#1                 ; [CPU_] |1634| 
        B         $C$L145,NEQ           ; [CPU_] |1634| 
        ; branchcc occurs ; [] |1634| 
	.dwpsn	file "../source/CommInterface.c",line 1636,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1636 | usnMinorCmd = DELFINO_WASTE_SYRINGE_MOTOR_CHK_STARTED_CMD;             
;----------------------------------------------------------------------
        MOV       *-SP[7],#8230         ; [CPU_] |1636| 
	.dwpsn	file "../source/CommInterface.c",line 1637,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1638 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1637| 
        ; branch occurs ; [] |1637| 
$C$L145:    
	.dwpsn	file "../source/CommInterface.c",line 1640,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1640 | usnMinorCmd = DELFINO_WASTE_SYRINGE_MOTOR_CHK_COMPLETED_CMD;           
;----------------------------------------------------------------------
        MOV       *-SP[7],#8212         ; [CPU_] |1640| 
	.dwpsn	file "../source/CommInterface.c",line 1642,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1642 | break;                                                                 
; 1644 | //!If the startup handling initiated is Sample Motor Check, send only t
;     | he                                                                     
; 1645 | //!sequence completed status to the processor.                         
; 1646 | case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:                                  
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1642| 
        ; branch occurs ; [] |1642| 
$C$L146:    
	.dwpsn	file "../source/CommInterface.c",line 1647,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1647 | if(TRUE == bStartEnd)                                                  
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1647| 
        CMPB      AL,#1                 ; [CPU_] |1647| 
        B         $C$L147,NEQ           ; [CPU_] |1647| 
        ; branchcc occurs ; [] |1647| 
	.dwpsn	file "../source/CommInterface.c",line 1649,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1649 | usnMinorCmd = DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_STARTED_CMD;       
;----------------------------------------------------------------------
        MOV       *-SP[7],#8231         ; [CPU_] |1649| 
	.dwpsn	file "../source/CommInterface.c",line 1650,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1651 | else                                                                   
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1650| 
        ; branch occurs ; [] |1650| 
$C$L147:    
	.dwpsn	file "../source/CommInterface.c",line 1653,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 1653 | usnMinorCmd = DELFINO_SAMPLE_LYSE_SYRINGE_MOTOR_CHK_COMPLETED_CMD;     
;----------------------------------------------------------------------
        MOV       *-SP[7],#8213         ; [CPU_] |1653| 
	.dwpsn	file "../source/CommInterface.c",line 1655,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1655 | break;                                                                 
; 1656 | case MBD_VOLUMETRIC_BOARD_CHECK:                                       
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1655| 
        ; branch occurs ; [] |1655| 
$C$L148:    
	.dwpsn	file "../source/CommInterface.c",line 1657,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1657 | if(TRUE == bStartEnd)                                                  
; 1659 |     //!sequence started                                                
;----------------------------------------------------------------------
        MOV       AL,*-SP[6]            ; [CPU_] |1657| 
        CMPB      AL,#1                 ; [CPU_] |1657| 
        B         $C$L149,EQ            ; [CPU_] |1657| 
        ; branchcc occurs ; [] |1657| 
	.dwpsn	file "../source/CommInterface.c",line 1660,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1661 | else                                                                   
; 1663 |   //!sequence completed                                                
; 1665 | //SAM_MOTOR_CHK - END                                                  
; 1666 | #if 0                                                                  
; 1667 | //!If the startup handling initiated is Rinse Prime and if the sequence
; 1668 | //!is starting, set the \var usnMinorCmd to Rinse Prime started        
; 1669 | //!If the sequence is ending, set the \var usnMinorCmd to Rinse        
; 1670 | //!Prime completed                                                     
; 1671 | case PRIME_WITH_RINSE_SERVICE_HANDLING:                                
; 1672 | if(TRUE == bStartEnd)                                                  
; 1674 |     usnMinorCmd = DELFINO_PRIME_WITH_RINSE_STARTED_CMD;                
; 1675 |     //!Disable the Rinse Prime monitoring                              
; 1676 |     g_bMonRinsePriming = FALSE;                                        
; 1677 |     //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_RINSE_STARTED_CMD\n
;     | ");                                                                    
; 1678 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1680 | else                                                                   
; 1682 |     usnMinorCmd = DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD;              
; 1683 |     //!Enable the Rinse Prime monitoring                               
; 1684 |     g_bMonRinsePriming = TRUE;                                         
; 1686 |     //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_RINSE_COMPLETED_CMD
;     |  : %ld\n", g_unSequenceStateTime1);                                    
; 1687 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1688 |     //!Clear Rinse prime error only if the priming sequence is         
; 1689 |     //! completed successfully                                         
; 1690 |     utErrorInfoCheck.stErrorInfo.ulnRinseEmpty = FALSE;                
; 1692 | break;                                                                 
; 1694 | //!If the startup handling initiated is Lyse Prime and if the sequence 
; 1695 | //!is starting, set the \var usnMinorCmd to Lyse Prime started         
; 1696 | //!If the sequence is ending, set the \var usnMinorCmd to Lyse         
; 1697 | //!Prime completed                                                     
; 1698 | case PRIME_WITH_LYSE_SERVICE_HANDLING:                                 
; 1699 | if(TRUE == bStartEnd)                                                  
; 1701 |     usnMinorCmd = DELFINO_PRIME_WITH_LYSE_STARTED_CMD;                 
; 1703 |     //!Disable the Lyse Prime monitoring                               
; 1704 |     g_bMonLysePriming = FALSE;                                         
; 1705 |     //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_LYSE_STARTED_CMD\n"
;     | );                                                                     
; 1706 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1708 | else                                                                   
; 1710 |     usnMinorCmd = DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD;               
; 1711 |     //!Enable the Lyse Prime monitoring                                
; 1712 |     g_bMonLysePriming = TRUE;                                          
; 1714 |     //!Enable the Diluent Prime monitoring                             
; 1715 |     g_bMonDiluentPriming = TRUE;                                       
; 1717 |     //!Clear the lyse prime error only if the priming sequence is      
; 1718 |     //! completed successfully                                         
; 1719 |     utErrorInfoCheck.stErrorInfo.ulnLyseEmpty = FALSE;                 
; 1721 |     //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_LYSE_COMPLETED_CMD
;     | : %ld\n", g_unSequenceStateTime1);                                     
; 1722 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1724 | break;                                                                 
; 1726 | //!If the startup handling initiated is Diluent Prime and if the sequen
;     | ce                                                                     
; 1727 | //!is starting, set the \var usnMinorCmd to Diluent Prime started      
; 1728 | //!If the sequence is ending, set the \var usnMinorCmd to Diluent      
; 1729 | //!Prime completed                                                     
; 1730 | case PRIME_WITH_DILUENT_SERVICE_HANDLING:                              
; 1731 | if(TRUE == bStartEnd)                                                  
; 1733 |     usnMinorCmd = DELFINO_PRIME_WITH_DILUENT_STARTED_CMD;              
; 1734 |     //!Disable the Diluent Prime monitoring                            
; 1735 |     g_bMonDiluentPriming = FALSE;                                      
; 1736 |     //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_DILUENT_STARTED_CMD
;     | \n");                                                                  
; 1737 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1739 | else                                                                   
; 1741 |     usnMinorCmd = DELFINO_PRIME_WITH_DILUENT_COMPLETED_CMD;            
; 1742 |     //!Enable the Diluent Prime monitoring                             
; 1743 |     g_bMonDiluentPriming = TRUE;                                       
; 1745 |     //!Clear the Diluent prime error only if the priming sequence is   
; 1746 |     //! completed successfully                                         
; 1747 |     utErrorInfoCheck.stErrorInfo.ulnDiluentEmpty = FALSE;              
; 1749 |     //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_DILUENT_COMPLETED_C
;     | MD : %ld\n", g_unSequenceStateTime1);                                  
; 1750 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1752 | break;                                                                 
; 1754 | //!If the startup handling initiated is EZ Cleanser and if the sequence
; 1755 | //!is starting, set the \var usnMinorCmd to EZ cleanser started        
; 1756 | //!If the sequence is ending, set the \var usnMinorCmd to EZ Cleanser  
; 1757 | //!Prime completed                                                     
; 1758 | case PRIME_WITH_EZ_CLEANSER_CMD:                                       
; 1759 | if(TRUE == bStartEnd)                                                  
; 1761 |     usnMinorCmd = DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD;          
; 1762 |     //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED
;     | _CMD\n");                                                              
; 1763 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1765 | else                                                                   
; 1767 |     usnMinorCmd = DELFINO_PRIME_WITH_EZ_CLEANSER_CMD;                  
; 1768 |     //sprintf(DebugPrintBuf, "\n DELFINO_PRIME_WITH_EZ_CLEANSER_CMD : %
;     | ld\n", g_unSequenceStateTime1);                                        
; 1769 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1771 | break;                                                                 
; 1773 | //!If the startup handling initiated is Back Flush and if the sequence 
; 1774 | //!is starting, set the \var usnMinorCmd to Back Flush started         
; 1775 | //!If the sequence is ending, set the \var usnMinorCmd to Back Flush   
; 1776 | //!completed                                                           
; 1777 | case BACK_FLUSH_SERVICE_HANDLING:                                      
; 1778 | if(TRUE == bStartEnd)                                                  
; 1780 |     usnMinorCmd = DELFINO_BACK_FLUSH_STARTED_CMD;                      
; 1781 |     //sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_STARTED_CMD\n");   
; 1782 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1784 | else                                                                   
; 1786 |     usnMinorCmd = DELFINO_BACK_FLUSH_COMPLETED_CMD;                    
; 1787 |     sprintf(DebugPrintBuf, "\n DELFINO_BACK_FLUSH_COMPLETED_CMD : %ld\n
;     | ", g_unSequenceStateTime1);                                            
; 1788 |     UartDebugPrint((int *)DebugPrintBuf, 50);                          
; 1790 | break;                                                                 
; 1792 | //!If the startup handling initiated is ZAP Aperture and if the sequenc
;     | e                                                                      
; 1793 | //!is starting, set the \var usnMinorCmd to ZAP Aperture started       
; 1794 | //!If the sequence is ending, set the \var usnMinorCmd to ZAP Aperture 
; 1795 | //!completed                                                           
; 1796 | case ZAP_APERTURE_SERVICE_HANDLING:                                    
; 1797 | if(TRUE == bStartEnd)                                                  
; 1799 |     usnMinorCmd = DELFINO_ZAP_APERTURE_STARTED_CMD;                    
; 1800 |     //sprintf(DebugPrintBuf, "\n DELFINO_ZAP_APERTURE_STARTED_CMD\n"); 
; 1801 |     // UartDebugPrint((int *)DebugPrintBuf, 50);                       
; 1803 | else                                                                   
; 1805 |     //!Zap Completed input given to MBD variable to be made false      
; 1806 |     //!when Zapping completed command is sent to Sitara                
; 1807 |     //!To avoid conflict in the next sequence of Zap complete          
; 1808 |     BloodCellCounter_U.ZapComplete  = FALSE;                           
; 1809 |     usnMinorCmd = DELFINO_ZAP_APERTURE_COMPLETED_CMD;                  
; 1810 |     //sprintf(DebugPrintBuf, "\n DELFINO_ZAP_APERTURE_COMPLETED_CMD : %
;     | ld\n", g_unSequenceStateTime1);                                        
; 1811 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1813 | break;                                                                 
; 1815 | //!If the startup handling initiated is Drain Bath and if the sequence 
; 1816 | //!is starting, set the \var usnMinorCmd to Drain Bath started         
; 1817 | //!If the sequence is ending, set the \var usnMinorCmd to Drain Bath   
; 1818 | //!completed                                                           
; 1819 | case DRAIN_BATH_SERVICE_HANDLING:                                      
; 1820 | if(TRUE == bStartEnd)                                                  
; 1822 |     usnMinorCmd = DELFINO_DRAIN_BATH_STARTED_CMD;                      
; 1823 |     //sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_BATH_STARTED_CMD\n");   
; 1824 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1826 | else                                                                   
; 1828 |     usnMinorCmd = DELFINO_DRAIN_BATH_COMPLETED_CMD;                    
; 1829 |     //sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_BATH_COMPLETED_CMD : %ld
;     | \n", g_unSequenceStateTime1);                                          
; 1830 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1832 | break;                                                                 
; 1834 | //!If the startup handling initiated is Drain All and if the sequence  
; 1835 | //!is starting, set the \var usnMinorCmd to Drain All started          
; 1836 | //!If the sequence is ending, set the \var usnMinorCmd to Drain All    
; 1837 | //!completed                                                           
; 1838 | case DRAIN_ALL_SERVICE_HANDLING:                                       
; 1839 | if(TRUE == bStartEnd)                                                  
; 1841 |     usnMinorCmd = DELFINO_DRAIN_ALL_STARTED_CMD;                       
; 1842 |     //sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_ALL_STARTED_CMD\n");    
; 1843 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1845 | else                                                                   
; 1847 |     usnMinorCmd = DELFINO_DRAIN_ALL_COMPLETED_CMD;                     
; 1848 |     sprintf(DebugPrintBuf, "\n DELFINO_DRAIN_ALL_COMPLETED_CMD : %ld\n"
;     | , g_unSequenceStateTime1);                                             
; 1849 |     UartDebugPrint((int *)DebugPrintBuf, 50);                          
; 1851 | break;                                                                 
; 1853 | //!If the startup handling initiated is Clean Bath and if the sequence 
; 1854 | //!is starting, set the \var usnMinorCmd to Clean Bath started         
; 1855 | //!If the sequence is ending, set the \var usnMinorCmd to Clean Bath   
; 1856 | //!completed                                                           
; 1857 | case CLEAN_BATH_SERVICE_HANDLING:                                      
; 1858 | if(TRUE == bStartEnd)                                                  
; 1860 |     usnMinorCmd = DELFINO_CLEAN_BATH_STARTED_CMD;                      
; 1861 |     //sprintf(DebugPrintBuf, "\n DELFINO_CLEAN_BATH_STARTED_CMD\n");   
; 1862 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1864 | else                                                                   
; 1866 |     usnMinorCmd = DELFINO_CLEAN_BATH_COMPLETED_CMD;                    
; 1867 |     //sprintf(DebugPrintBuf, "\n DELFINO_CLEAN_BATH_COMPLETED_CMD : %ld
;     | \n", g_unSequenceStateTime1);                                          
; 1868 |     // UartDebugPrint((int *)DebugPrintBuf, 50);                       
; 1870 | break;                                                                 
; 1872 | //!If the command is valve test, no sequence is involved, it is only   
; 1873 | //!sending the status of completion of valve test.                     
; 1874 | //!So minor command is set to System Status                            
; 1875 | case VALVE_TEST_COMPLETION:                                            
; 1876 | usnMinorCmd = DELFINO_VALVE_TEST_COMPLETED_CMD;                        
; 1877 | //sprintf(DebugPrintBuf, "\n DELFINO_VALVE_TEST_COMPLETED_CMD\n");     
; 1878 | //UartDebugPrint((int *)DebugPrintBuf, 50);                            
; 1879 | break;                                                                 
; 1881 | //!If Waste Bin is detected, then send Waste bin full command to proces
;     | sor.                                                                   
; 1882 | //!Set the minor command to Waste Bin full.                            
; 1883 | case WASTE_BIN_FULL_CMD:                                               
; 1884 | //sprintf(DebugPrintBuf, "\n DELFINO_SYSTEM_STATUS_DATA\n");           
; 1885 | //UartDebugPrint((int *)DebugPrintBuf, 50);                            
; 1886 | usnMinorCmd = DELFINO_SYSTEM_STATUS_DATA;                              
; 1887 | break;                                                                 
; 1889 | //!If the startup handling initiated is Head Rinse and if the sequence 
; 1890 | //!is starting, set the \var usnMinorCmd to Head Rinse started         
; 1891 | //!If the sequence is ending, set the \var usnMinorCmd to Head Rinse   
; 1892 | //!completed                                                           
; 1893 | case HEAD_RINSING_SERVICE_HANDLING:                                    
; 1894 | if(TRUE == bStartEnd)                                                  
; 1898 | else                                                                   
; 1900 |     usnMinorCmd = DELFINO_HEAD_RINSING_COMPLETED_CMD;                  
; 1901 |     // sprintf(DebugPrintBuf, "\n DELFINO_HEAD_RINSING_COMPLETED_CMD\n"
;     | );                                                                     
; 1902 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1904 | break;                                                                 
; 1906 | //!If the startup handling initiated is Probe Cleaning and if the seque
;     | nce                                                                    
; 1907 | //!is starting, set the \var usnMinorCmd to Probe Cleaning started     
; 1908 | //!If the sequence is ending, set the \var usnMinorCmd to Probe Cleanin
;     | g                                                                      
; 1909 | //!completed                                                           
; 1910 | case  PROBE_CLEANING_SERVICE_HANDLING:                                 
; 1911 | if(TRUE == bStartEnd)                                                  
; 1915 | else                                                                   
; 1917 |     usnMinorCmd = DELFINO_PROBE_CLEANING_COMPLETED_CMD;                
; 1918 |     //sprintf(DebugPrintBuf, "\n DELFINO_PROBE_CLEANING_COMPLETED_CMD\n
;     | ");                                                                    
; 1919 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1921 | break;                                                                 
; 1923 | //!If the startup handling initiated is Xountign Time Sequence, send on
;     | ly the                                                                 
; 1924 | //!sequence completed status to the processor.                         
; 1925 | case COUNTING_TIME_SEQUENCE:                                           
; 1926 | if(TRUE == bStartEnd)                                                  
; 1928 |     usnMinorCmd = DELFINO_COUNTING_TIME_SEQUENCE_STARTED_CMD;//SKM_CHAN
;     | GE_COUNTSTART                                                          
; 1929 |     //sprintf(DebugPrintBuf, "\n DELFINO_COUNTING_TIME_SEQUENCE_STARTED
;     | _CMD : %ld\n", g_unSequenceStateTime1);                                
; 1930 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1932 | else                                                                   
; 1934 |     usnMinorCmd = DELFINO_COUNTING_TIME_SEQUENCE_COMPLETED_CMD;        
; 1935 |     //sprintf(DebugPrintBuf, "\n DELFINO_COUNTING_TIME_SEQUENCE_COMPLET
;     | ED_CMD : %ld\n", g_unSequenceStateTime1);                              
; 1936 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1940 | break;                                                                 
; 1941 | //SKM_WAKEUP_START                                                     
; 1942 | //!If the startup handling initiated is Wake Up and if the sequence    
; 1943 | //!is starting, set the \var usnMinorCmd to Wake Up started            
; 1944 | //!If the sequence is ending, set the \var usnMinorCmd to Wake Up      
; 1945 | //!completed                                                           
; 1946 | case MBD_WAKEUP_START:                                                 
; 1947 | if(TRUE == bStartEnd)                                                  
; 1951 | else                                                                   
; 1953 |     usnMinorCmd = DELFINO_WAKE_UP_COMPLETED_CMD;                       
; 1954 |     // sprintf(DebugPrintBuf, "\n DELFINO_WAKE_UP_COMPLETED_CMD : %ld\n
;     | ", g_unSequenceStateTime1);                                            
; 1955 |     //UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 1957 | break;                                                                 
; 1958 | //SKM_WAKEUP_END                                                       
; 1960 | //HN_TOSHIP_Start                                                      
; 1961 | case MBD_TO_SHIP_SEQUENCE_START:                                       
; 1962 |  if(TRUE == bStartEnd)                                                 
; 1964 |        sprintf(DebugPrintBuf, "\n DELFINO_TO_SHIP_STARTED : %ld\n", g_u
;     | nSequenceStateTime1);                                                  
; 1965 |        UartDebugPrint((int *)DebugPrintBuf, 50);                       
; 1966 |        usnMinorCmd = DELFINO_TO_SHIP_STARTED;                          
; 1968 |  else                                                                  
; 1970 |      usnMinorCmd = DELFINO_TO_SHIP_COMPLETED_CMD;                      
; 1971 |      sprintf(DebugPrintBuf, "\n DELFINO_TO_SHIP_COMPLETED_CMD : %ld\n",
;     |  g_unSequenceStateTime1);                                              
; 1972 |      UartDebugPrint((int *)DebugPrintBuf, 50);                         
; 1974 |  break;                                                                
; 1975 |  //HN_TOSHIP_end                                                       
; 1977 | //PH_CHANGE_SAMPLE_START_BUTTON - START                                
; 1978 | case DELFINO_READY_FOR_ASPIRATION:                                     
; 1979 | usnMinorCmd = DELFINO_READY_FOR_ASPIRATION;                            
; 1980 | break;                                                                 
; 1981 | //PH_CHANGE_SAMPLE_START_BUTTON - END                                  
; 1982 | #endif                                                                 
; 1983 | case START_UP_SEQUENCE_MODE:                                           
; 1984 | case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:                              
; 1985 | case MBD_STARTUP_FAILED_CMD:                                           
;----------------------------------------------------------------------
$C$L149:    
	.dwpsn	file "../source/CommInterface.c",line 1986,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1986 | usnMinorCmd = DELFINO_START_UP_SEQUENCE_STARTED_CMD;                   
;----------------------------------------------------------------------
        MOV       *-SP[7],#8237         ; [CPU_] |1986| 
	.dwpsn	file "../source/CommInterface.c",line 1987,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1987 | sprintf(DebugPrintBuf, "\r\n DELFINO_SEQUENCE_STARTED : %ld\n", g_unSeq
;     | uenceStateTime1);                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_g_unSequenceStateTime1 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL32        ; [CPU_U] |1987| 
        MOVL      ACC,@_g_unSequenceStateTime1 ; [CPU_] |1987| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1987| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1987| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1987| 
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_name("_sprintf")
	.dwattr $C$DW$232, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |1987| 
        ; call occurs [#_sprintf] ; [] |1987| 
	.dwpsn	file "../source/CommInterface.c",line 1988,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1988 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |1988| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |1988| 
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$233, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |1988| 
        ; call occurs [#_UartDebugPrint] ; [] |1988| 
	.dwpsn	file "../source/CommInterface.c",line 1989,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1989 | break;                                                                 
; 1991 | case DELFINO_ERROR_MINOR_CMD:                                          
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1989| 
        ; branch occurs ; [] |1989| 
$C$L150:    
	.dwpsn	file "../source/CommInterface.c",line 1992,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1992 | usnMinorCmd = DELFINO_ERROR_MINOR_CMD;                                 
;----------------------------------------------------------------------
        MOV       *-SP[7],#9217         ; [CPU_] |1992| 
	.dwpsn	file "../source/CommInterface.c",line 1993,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1993 | break;                                                                 
; 1995 | default:                                                               
; 1996 | //!In default scenario, set the minor command to null                  
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1993| 
        ; branch occurs ; [] |1993| 
$C$L151:    
	.dwpsn	file "../source/CommInterface.c",line 1997,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1997 | usnMinorCmd = NULL;//MBD_UPDATION                                      
;----------------------------------------------------------------------
        MOV       *-SP[7],#0            ; [CPU_] |1997| 
	.dwpsn	file "../source/CommInterface.c",line 1998,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 1998 | break;                                                                 
; 2000 | }//end of switch case                                                  
; 2002 | //!If the minor command is set, then frame the startup handling packet
;     | and                                                                    
; 2003 | //!send to the processor                                               
;----------------------------------------------------------------------
        B         $C$L156,UNC           ; [CPU_] |1998| 
        ; branch occurs ; [] |1998| 
$C$L152:    
	.dwpsn	file "../source/CommInterface.c",line 1521,column 5,is_stmt,isa 0
        MOVZ      AR6,*-SP[5]           ; [CPU_] |1521| 
        CMP       AR6,#20507            ; [CPU_] |1521| 
        B         $C$L154,GT            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        CMP       AR6,#20507            ; [CPU_] |1521| 
        B         $C$L142,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        CMP       AR6,#20492            ; [CPU_] |1521| 
        B         $C$L153,GT            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        CMP       AR6,#20492            ; [CPU_] |1521| 
        B         $C$L133,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        MOVZ      AR7,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#9217            ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR7              ; [CPU_] |1521| 
        B         $C$L150,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        MOVZ      AR7,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20485           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR7              ; [CPU_] |1521| 
        B         $C$L136,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        MOVZ      AR6,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20486           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR6              ; [CPU_] |1521| 
        B         $C$L134,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        B         $C$L151,UNC           ; [CPU_] |1521| 
        ; branch occurs ; [] |1521| 
$C$L153:    
        MOVZ      AR7,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20500           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR7              ; [CPU_] |1521| 
        B         $C$L149,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        MOVZ      AR7,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20505           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR7              ; [CPU_] |1521| 
        B         $C$L138,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        MOVZ      AR6,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20506           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR6              ; [CPU_] |1521| 
        B         $C$L140,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        B         $C$L151,UNC           ; [CPU_] |1521| 
        ; branch occurs ; [] |1521| 
$C$L154:    
        CMP       AR6,#20537            ; [CPU_] |1521| 
        B         $C$L155,GT            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        CMP       AR6,#20537            ; [CPU_] |1521| 
        B         $C$L149,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        MOVZ      AR7,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20508           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR7              ; [CPU_] |1521| 
        B         $C$L144,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        MOVZ      AR7,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20509           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR7              ; [CPU_] |1521| 
        B         $C$L146,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        MOVZ      AR6,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20526           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR6              ; [CPU_] |1521| 
        B         $C$L148,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        B         $C$L151,UNC           ; [CPU_] |1521| 
        ; branch occurs ; [] |1521| 
$C$L155:    
        MOVZ      AR7,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20538           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR7              ; [CPU_] |1521| 
        B         $C$L134,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        MOVZ      AR6,AR6               ; [CPU_] |1521| 
        MOVL      XAR4,#20540           ; [CPU_U] |1521| 
        MOVL      ACC,XAR4              ; [CPU_] |1521| 
        CMPL      ACC,XAR6              ; [CPU_] |1521| 
        B         $C$L149,EQ            ; [CPU_] |1521| 
        ; branchcc occurs ; [] |1521| 
        B         $C$L151,UNC           ; [CPU_] |1521| 
        ; branch occurs ; [] |1521| 
$C$L156:    
	.dwpsn	file "../source/CommInterface.c",line 2004,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2004 | if(usnMinorCmd != NULL)                                                
; 2006 |     //!Frame startup handling packet to be sent to processor           
;----------------------------------------------------------------------
        MOV       AL,*-SP[7]            ; [CPU_] |2004| 
        B         $C$L157,EQ            ; [CPU_] |2004| 
        ; branchcc occurs ; [] |2004| 
	.dwpsn	file "../source/CommInterface.c",line 2007,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2007 | CPFrameDataPacket(DELFINO_STARTUP_HANDLING_CMD, usnMinorCmd, FALSE);   
;----------------------------------------------------------------------
        MOV       AL,#8201              ; [CPU_] |2007| 
        MOV       AH,*-SP[7]            ; [CPU_] |2007| 
        MOVB      XAR4,#0               ; [CPU_] |2007| 
$C$DW$234	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$234, DW_AT_low_pc(0x00)
	.dwattr $C$DW$234, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$234, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2007| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2007| 
	.dwpsn	file "../source/CommInterface.c",line 2009,column 1,is_stmt,isa 0
$C$L157:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$235	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$235, DW_AT_low_pc(0x00)
	.dwattr $C$DW$235, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$220, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$220, DW_AT_TI_end_line(0x7d9)
	.dwattr $C$DW$220, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$220

	.sect	".text:_CIFormSyncCmd"
	.clink
	.global	_CIFormSyncCmd

$C$DW$236	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$236, DW_AT_name("CIFormSyncCmd")
	.dwattr $C$DW$236, DW_AT_low_pc(_CIFormSyncCmd)
	.dwattr $C$DW$236, DW_AT_high_pc(0x00)
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_CIFormSyncCmd")
	.dwattr $C$DW$236, DW_AT_external
	.dwattr $C$DW$236, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$236, DW_AT_TI_begin_line(0x7e1)
	.dwattr $C$DW$236, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$236, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/CommInterface.c",line 2018,column 1,is_stmt,address _CIFormSyncCmd,isa 0

	.dwfde $C$DW$CIE, _CIFormSyncCmd
;----------------------------------------------------------------------
; 2017 | void CIFormSyncCmd()                                                   
; 2019 | //!Frame the packet for sync command                                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormSyncCmd                FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_CIFormSyncCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/CommInterface.c",line 2020,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2020 | CPFrameDataPacket(DELFINO_SYNC_MAIN_CMD,DELFINO_SYNC_SUB_CMD, FALSE);  
;----------------------------------------------------------------------
        MOV       AL,#8192              ; [CPU_] |2020| 
        MOV       AH,#56797             ; [CPU_] |2020| 
        MOVB      XAR4,#0               ; [CPU_] |2020| 
$C$DW$237	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$237, DW_AT_low_pc(0x00)
	.dwattr $C$DW$237, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$237, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2020| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2020| 
	.dwpsn	file "../source/CommInterface.c",line 2021,column 1,is_stmt,isa 0
$C$DW$238	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$238, DW_AT_low_pc(0x00)
	.dwattr $C$DW$238, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$236, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$236, DW_AT_TI_end_line(0x7e5)
	.dwattr $C$DW$236, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$236

	.sect	".text:_CIProcessFirmwareVersionReq"
	.clink
	.global	_CIProcessFirmwareVersionReq

$C$DW$239	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$239, DW_AT_name("CIProcessFirmwareVersionReq")
	.dwattr $C$DW$239, DW_AT_low_pc(_CIProcessFirmwareVersionReq)
	.dwattr $C$DW$239, DW_AT_high_pc(0x00)
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_CIProcessFirmwareVersionReq")
	.dwattr $C$DW$239, DW_AT_external
	.dwattr $C$DW$239, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$239, DW_AT_TI_begin_line(0x7ed)
	.dwattr $C$DW$239, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$239, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/CommInterface.c",line 2030,column 1,is_stmt,address _CIProcessFirmwareVersionReq,isa 0

	.dwfde $C$DW$CIE, _CIProcessFirmwareVersionReq
;----------------------------------------------------------------------
; 2029 | void CIProcessFirmwareVersionReq()                                     
; 2031 | //!Frame the packet for Firmware version                               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessFirmwareVersionReq  FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_CIProcessFirmwareVersionReq:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/CommInterface.c",line 2032,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2032 | CPFrameDataPacket(DELFINO_FIRMWARE_VERSION_CMD,DELFINO_CPU1_CPU2_VERSIO
;     | N,\                                                                    
; 2033 |         FALSE);                                                        
;----------------------------------------------------------------------
        MOV       AL,#8195              ; [CPU_] |2032| 
        MOV       AH,#8193              ; [CPU_] |2032| 
        MOVB      XAR4,#0               ; [CPU_] |2032| 
$C$DW$240	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$240, DW_AT_low_pc(0x00)
	.dwattr $C$DW$240, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$240, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2032| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2032| 
	.dwpsn	file "../source/CommInterface.c",line 2034,column 1,is_stmt,isa 0
$C$DW$241	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$241, DW_AT_low_pc(0x00)
	.dwattr $C$DW$241, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$239, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$239, DW_AT_TI_end_line(0x7f2)
	.dwattr $C$DW$239, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$239

	.sect	".text:_CIProcessSystemSettingsCmd"
	.clink
	.global	_CIProcessSystemSettingsCmd

$C$DW$242	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$242, DW_AT_name("CIProcessSystemSettingsCmd")
	.dwattr $C$DW$242, DW_AT_low_pc(_CIProcessSystemSettingsCmd)
	.dwattr $C$DW$242, DW_AT_high_pc(0x00)
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_CIProcessSystemSettingsCmd")
	.dwattr $C$DW$242, DW_AT_external
	.dwattr $C$DW$242, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$242, DW_AT_TI_begin_line(0x7fb)
	.dwattr $C$DW$242, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$242, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommInterface.c",line 2044,column 1,is_stmt,address _CIProcessSystemSettingsCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessSystemSettingsCmd
$C$DW$243	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$243, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2043 | void CIProcessSystemSettingsCmd(uint16_t usnMinorCmd)                  
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessSystemSettingsCmd   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            3 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessSystemSettingsCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$244	.dwtag  DW_TAG_variable
	.dwattr $C$DW$244, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_breg20 -4]

;----------------------------------------------------------------------
; 2045 | //!Check for the minor command                                         
;----------------------------------------------------------------------
        MOV       *-SP[4],AL            ; [CPU_] |2044| 
	.dwpsn	file "../source/CommInterface.c",line 2046,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2046 | switch(usnMinorCmd)                                                    
; 2048 |     //!If the minor command is to set the bubbling time, set the corres
;     | ponding                                                                
; 2049 |     //!parameters                                                      
; 2050 |     case SITARA_BUBBLING_TIME_CMD:                                     
;----------------------------------------------------------------------
        B         $C$L177,UNC           ; [CPU_] |2046| 
        ; branch occurs ; [] |2046| 
$C$L158:    
	.dwpsn	file "../source/CommInterface.c",line 2051,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2051 | sprintf(DebugPrintBuf, "\r\n SETTINGS_BUBBLING_TIME_SEQUENCE_HANDLING \
;     | n");                                                                   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL33        ; [CPU_U] |2051| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2051| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2051| 
$C$DW$245	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$245, DW_AT_low_pc(0x00)
	.dwattr $C$DW$245, DW_AT_name("_sprintf")
	.dwattr $C$DW$245, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2051| 
        ; call occurs [#_sprintf] ; [] |2051| 
	.dwpsn	file "../source/CommInterface.c",line 2052,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2052 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2052| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2052| 
$C$DW$246	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$246, DW_AT_low_pc(0x00)
	.dwattr $C$DW$246, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$246, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2052| 
        ; call occurs [#_UartDebugPrint] ; [] |2052| 
	.dwpsn	file "../source/CommInterface.c",line 2053,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2053 | g_usnFirstDilutionFreq = m_stReceivedPacketFrame.arrusnData[0];        
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+6 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+6 ; [CPU_] |2053| 
        MOVW      DP,#_g_usnFirstDilutionFreq ; [CPU_U] 
        MOV       @_g_usnFirstDilutionFreq,AL ; [CPU_] |2053| 
	.dwpsn	file "../source/CommInterface.c",line 2054,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2054 | g_usnLyseMixingFreq = m_stReceivedPacketFrame.arrusnData[1];           
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+7 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+7 ; [CPU_] |2054| 
        MOVW      DP,#_g_usnLyseMixingFreq ; [CPU_U] 
        MOV       @_g_usnLyseMixingFreq,AL ; [CPU_] |2054| 
	.dwpsn	file "../source/CommInterface.c",line 2055,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2055 | g_usnFinalRBCMixingFreq = m_stReceivedPacketFrame.arrusnData[2];       
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+8 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+8 ; [CPU_] |2055| 
        MOVW      DP,#_g_usnFinalRBCMixingFreq ; [CPU_U] 
        MOV       @_g_usnFinalRBCMixingFreq,AL ; [CPU_] |2055| 
	.dwpsn	file "../source/CommInterface.c",line 2056,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2056 | g_usnWholeBloodZapTime = m_stReceivedPacketFrame.arrusnData[6];        
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+12 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+12 ; [CPU_] |2056| 
        MOVW      DP,#_g_usnWholeBloodZapTime ; [CPU_U] 
        MOV       @_g_usnWholeBloodZapTime,AL ; [CPU_] |2056| 
	.dwpsn	file "../source/CommInterface.c",line 2057,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2057 | g_DiluentMixTime = m_stReceivedPacketFrame.arrusnData[3];              
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+9 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+9 ; [CPU_] |2057| 
        MOVW      DP,#_g_DiluentMixTime ; [CPU_U] 
        MOV       @_g_DiluentMixTime,AL ; [CPU_] |2057| 
	.dwpsn	file "../source/CommInterface.c",line 2058,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2058 | g_RBCMixTime = m_stReceivedPacketFrame.arrusnData[4];                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+10 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+10 ; [CPU_] |2058| 
        MOVW      DP,#_g_RBCMixTime     ; [CPU_U] 
        MOV       @_g_RBCMixTime,AL     ; [CPU_] |2058| 
	.dwpsn	file "../source/CommInterface.c",line 2059,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2059 | g_LyseMixTime = m_stReceivedPacketFrame.arrusnData[5];                 
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+11 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+11 ; [CPU_] |2059| 
        MOVW      DP,#_g_LyseMixTime    ; [CPU_U] 
        MOV       @_g_LyseMixTime,AL    ; [CPU_] |2059| 
	.dwpsn	file "../source/CommInterface.c",line 2060,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2060 | g_usnWbcCalibratedTime = m_stReceivedPacketFrame.arrusnData[7];     //D
;     | S_added                                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+13 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+13 ; [CPU_] |2060| 
        MOVW      DP,#_g_usnWbcCalibratedTime ; [CPU_U] 
        MOV       @_g_usnWbcCalibratedTime,AL ; [CPU_] |2060| 
	.dwpsn	file "../source/CommInterface.c",line 2061,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2061 | g_usnRbcCalibratedTime = m_stReceivedPacketFrame.arrusnData[8];     //D
;     | S_added                                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+14 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+14 ; [CPU_] |2061| 
        MOVW      DP,#_g_usnRbcCalibratedTime ; [CPU_U] 
        MOV       @_g_usnRbcCalibratedTime,AL ; [CPU_] |2061| 
	.dwpsn	file "../source/CommInterface.c",line 2062,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2062 | sncalZeroPSI = m_stReceivedPacketFrame.arrusnData[9];      //HN_added  
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+15 ; [CPU_U] 
        MOVU      ACC,@_m_stReceivedPacketFrame+15 ; [CPU_] |2062| 
        MOVW      DP,#_sncalZeroPSI     ; [CPU_U] 
        MOVL      @_sncalZeroPSI,ACC    ; [CPU_] |2062| 
	.dwpsn	file "../source/CommInterface.c",line 2063,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2063 | g_TestDataI0 = m_stReceivedPacketFrame.arrusnData[11];      //HN_added 
; 2065 | //!If the sign value is negative then change sign of sncalZeroPSI to ne
;     | gative                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+17 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+17 ; [CPU_] |2063| 
        MOVW      DP,#_g_TestDataI0     ; [CPU_U] 
        MOV       @_g_TestDataI0,AL     ; [CPU_] |2063| 
	.dwpsn	file "../source/CommInterface.c",line 2066,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2066 | if(m_stReceivedPacketFrame.arrusnData[10] == 1 ) //index 10 for pressur
;     | e calibration sign data                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+16 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+16 ; [CPU_] |2066| 
        CMPB      AL,#1                 ; [CPU_] |2066| 
        B         $C$L159,NEQ           ; [CPU_] |2066| 
        ; branchcc occurs ; [] |2066| 
	.dwpsn	file "../source/CommInterface.c",line 2068,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2068 | sncalZeroPSI = sncalZeroPSI * (- 1);                                   
; 2072 | //Changing msec to 20msec counter                                      
; 2073 | //g_DiluentMixTime = g_DiluentMixTime/20;                              
; 2074 | //g_RBCMixTime = g_RBCMixTime/20;                                      
; 2075 | //g_LyseMixTime = g_LyseMixTime/20;                                    
;----------------------------------------------------------------------
        MOVW      DP,#_sncalZeroPSI     ; [CPU_U] 
        MOVL      ACC,@_sncalZeroPSI    ; [CPU_] |2068| 
        NEG       ACC                   ; [CPU_] |2068| 
        MOVL      @_sncalZeroPSI,ACC    ; [CPU_] |2068| 
$C$L159:    
	.dwpsn	file "../source/CommInterface.c",line 2076,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2076 | g_usnWbcCalibratedTime = g_usnWbcCalibratedTime/20;                    
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnWbcCalibratedTime ; [CPU_U] 
        MOVB      AH,#20                ; [CPU_] |2076| 
        MOV       AL,@_g_usnWbcCalibratedTime ; [CPU_] |2076| 
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_name("I$$DIV")
	.dwattr $C$DW$247, DW_AT_TI_call

        FFC       XAR7,#I$$DIV          ; [CPU_] |2076| 
        ; call occurs [#I$$DIV] ; [] |2076| 
        MOV       @_g_usnWbcCalibratedTime,AL ; [CPU_] |2076| 
	.dwpsn	file "../source/CommInterface.c",line 2077,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2077 | g_usnRbcCalibratedTime = g_usnRbcCalibratedTime/20;                    
; 2079 | //!------------>  Mixing Time -start <-------------//////////////      
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnRbcCalibratedTime ; [CPU_U] 
        MOVB      AH,#20                ; [CPU_] |2077| 
        MOV       AL,@_g_usnRbcCalibratedTime ; [CPU_] |2077| 
$C$DW$248	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$248, DW_AT_low_pc(0x00)
	.dwattr $C$DW$248, DW_AT_name("I$$DIV")
	.dwattr $C$DW$248, DW_AT_TI_call

        FFC       XAR7,#I$$DIV          ; [CPU_] |2077| 
        ; call occurs [#I$$DIV] ; [] |2077| 
        MOV       @_g_usnRbcCalibratedTime,AL ; [CPU_] |2077| 
	.dwpsn	file "../source/CommInterface.c",line 2080,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2080 | if(g_LyseMixTime >= 3280)                                              
;----------------------------------------------------------------------
        MOVW      DP,#_g_LyseMixTime    ; [CPU_U] 
        CMP       @_g_LyseMixTime,#3280 ; [CPU_] |2080| 
        B         $C$L160,LO            ; [CPU_] |2080| 
        ; branchcc occurs ; [] |2080| 
	.dwpsn	file "../source/CommInterface.c",line 2082,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2082 | g_LyseMixTime = 3280;                                                  
;----------------------------------------------------------------------
        MOV       @_g_LyseMixTime,#3280 ; [CPU_] |2082| 
$C$L160:    
	.dwpsn	file "../source/CommInterface.c",line 2084,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2084 | if(g_LyseMixTime <= 1080)                                              
;----------------------------------------------------------------------
        CMP       @_g_LyseMixTime,#1080 ; [CPU_] |2084| 
        B         $C$L161,HI            ; [CPU_] |2084| 
        ; branchcc occurs ; [] |2084| 
	.dwpsn	file "../source/CommInterface.c",line 2086,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2086 | g_LyseMixTime = 1080;                                                  
; 2088 | /////////////////////////////                                          
;----------------------------------------------------------------------
        MOV       @_g_LyseMixTime,#1080 ; [CPU_] |2086| 
$C$L161:    
	.dwpsn	file "../source/CommInterface.c",line 2089,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2089 | if(g_RBCMixTime >= 1200)                                               
;----------------------------------------------------------------------
        MOVW      DP,#_g_RBCMixTime     ; [CPU_U] 
        CMP       @_g_RBCMixTime,#1200  ; [CPU_] |2089| 
        B         $C$L162,LO            ; [CPU_] |2089| 
        ; branchcc occurs ; [] |2089| 
	.dwpsn	file "../source/CommInterface.c",line 2091,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2091 | g_RBCMixTime = 1200;                                                   
;----------------------------------------------------------------------
        MOV       @_g_RBCMixTime,#1200  ; [CPU_] |2091| 
$C$L162:    
	.dwpsn	file "../source/CommInterface.c",line 2093,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2093 | if(g_RBCMixTime <= 410)                                                
;----------------------------------------------------------------------
        CMP       @_g_RBCMixTime,#410   ; [CPU_] |2093| 
        B         $C$L163,HI            ; [CPU_] |2093| 
        ; branchcc occurs ; [] |2093| 
	.dwpsn	file "../source/CommInterface.c",line 2095,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2095 | g_RBCMixTime = 410;                                                    
; 2097 | ///////////////////////////////                                        
;----------------------------------------------------------------------
        MOV       @_g_RBCMixTime,#410   ; [CPU_] |2095| 
$C$L163:    
	.dwpsn	file "../source/CommInterface.c",line 2098,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2098 | if(g_DiluentMixTime >= 2050)                                           
;----------------------------------------------------------------------
        MOVW      DP,#_g_DiluentMixTime ; [CPU_U] 
        CMP       @_g_DiluentMixTime,#2050 ; [CPU_] |2098| 
        B         $C$L164,LO            ; [CPU_] |2098| 
        ; branchcc occurs ; [] |2098| 
	.dwpsn	file "../source/CommInterface.c",line 2100,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2100 | g_DiluentMixTime = 2050;                                               
;----------------------------------------------------------------------
        MOV       @_g_DiluentMixTime,#2050 ; [CPU_] |2100| 
$C$L164:    
	.dwpsn	file "../source/CommInterface.c",line 2102,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2102 | if(g_DiluentMixTime <= 680)                                            
;----------------------------------------------------------------------
        CMP       @_g_DiluentMixTime,#680 ; [CPU_] |2102| 
        B         $C$L165,HI            ; [CPU_] |2102| 
        ; branchcc occurs ; [] |2102| 
	.dwpsn	file "../source/CommInterface.c",line 2104,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2104 | g_DiluentMixTime = 680;                                                
; 2106 | //////////////////////////////////                                     
;----------------------------------------------------------------------
        MOV       @_g_DiluentMixTime,#680 ; [CPU_] |2104| 
$C$L165:    
	.dwpsn	file "../source/CommInterface.c",line 2107,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2107 | g_DiluentMixTime = g_DiluentMixTime/20;                                
;----------------------------------------------------------------------
        MOVB      XAR6,#20              ; [CPU_] |2107| 
        MOVU      ACC,@_g_DiluentMixTime ; [CPU_] |2107| 
        RPT       #15
||     SUBCU     ACC,AR6               ; [CPU_] |2107| 
        MOV       @_g_DiluentMixTime,AL ; [CPU_] |2107| 
	.dwpsn	file "../source/CommInterface.c",line 2108,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2108 | g_RBCMixTime = g_RBCMixTime/20;                                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_RBCMixTime     ; [CPU_U] 
        MOVB      XAR6,#20              ; [CPU_] |2108| 
        MOVU      ACC,@_g_RBCMixTime    ; [CPU_] |2108| 
        RPT       #15
||     SUBCU     ACC,AR6               ; [CPU_] |2108| 
        MOV       @_g_RBCMixTime,AL     ; [CPU_] |2108| 
	.dwpsn	file "../source/CommInterface.c",line 2109,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2109 | g_LyseMixTime = g_LyseMixTime /20;                                     
; 2110 | //!------------>  Mixing Time - End <-------------//////////////       
;----------------------------------------------------------------------
        MOVB      XAR6,#20              ; [CPU_] |2109| 
        MOVW      DP,#_g_LyseMixTime    ; [CPU_U] 
        MOVU      ACC,@_g_LyseMixTime   ; [CPU_] |2109| 
        RPT       #15
||     SUBCU     ACC,AR6               ; [CPU_] |2109| 
        MOV       @_g_LyseMixTime,AL    ; [CPU_] |2109| 
	.dwpsn	file "../source/CommInterface.c",line 2112,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2112 | if(g_usnWholeBloodZapTime > 10000)                                     
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnWholeBloodZapTime ; [CPU_U] 
        CMP       @_g_usnWholeBloodZapTime,#10000 ; [CPU_] |2112| 
        B         $C$L166,LOS           ; [CPU_] |2112| 
        ; branchcc occurs ; [] |2112| 
	.dwpsn	file "../source/CommInterface.c",line 2114,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2114 | g_usnWholeBloodZapTime = 10000;                                        
;----------------------------------------------------------------------
        MOV       @_g_usnWholeBloodZapTime,#10000 ; [CPU_] |2114| 
$C$L166:    
	.dwpsn	file "../source/CommInterface.c",line 2116,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2116 | g_usnWholeBloodZapTime = g_usnWholeBloodZapTime/20;                    
;----------------------------------------------------------------------
        MOVB      XAR6,#20              ; [CPU_] |2116| 
        MOVU      ACC,@_g_usnWholeBloodZapTime ; [CPU_] |2116| 
        RPT       #15
||     SUBCU     ACC,AR6               ; [CPU_] |2116| 
        MOV       @_g_usnWholeBloodZapTime,AL ; [CPU_] |2116| 
	.dwpsn	file "../source/CommInterface.c",line 2119,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2119 | sprintf(DebugPrintBuf, "\r\n arrusnData[0] %d\n", m_stReceivedPacketFra
;     | me.arrusnData[0]);                                                     
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL34        ; [CPU_U] |2119| 
        MOVW      DP,#_m_stReceivedPacketFrame+6 ; [CPU_U] 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2119| 
        MOV       AL,@_m_stReceivedPacketFrame+6 ; [CPU_] |2119| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2119| 
        MOV       *-SP[3],AL            ; [CPU_] |2119| 
$C$DW$249	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$249, DW_AT_low_pc(0x00)
	.dwattr $C$DW$249, DW_AT_name("_sprintf")
	.dwattr $C$DW$249, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2119| 
        ; call occurs [#_sprintf] ; [] |2119| 
	.dwpsn	file "../source/CommInterface.c",line 2120,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 2120 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2120| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2120| 
$C$DW$250	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$250, DW_AT_low_pc(0x00)
	.dwattr $C$DW$250, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$250, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2120| 
        ; call occurs [#_UartDebugPrint] ; [] |2120| 
	.dwpsn	file "../source/CommInterface.c",line 2121,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2121 | sprintf(DebugPrintBuf, "\r\n arrusnData[1] %d\n", m_stReceivedPacketFra
;     | me.arrusnData[1]);                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+7 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL35        ; [CPU_U] |2121| 
        MOV       AL,@_m_stReceivedPacketFrame+7 ; [CPU_] |2121| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2121| 
        MOV       *-SP[3],AL            ; [CPU_] |2121| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2121| 
$C$DW$251	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$251, DW_AT_low_pc(0x00)
	.dwattr $C$DW$251, DW_AT_name("_sprintf")
	.dwattr $C$DW$251, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2121| 
        ; call occurs [#_sprintf] ; [] |2121| 
	.dwpsn	file "../source/CommInterface.c",line 2122,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 2122 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2122| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2122| 
$C$DW$252	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$252, DW_AT_low_pc(0x00)
	.dwattr $C$DW$252, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$252, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2122| 
        ; call occurs [#_UartDebugPrint] ; [] |2122| 
	.dwpsn	file "../source/CommInterface.c",line 2123,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2123 | sprintf(DebugPrintBuf, "\r\n arrusnData[2] %d\n", m_stReceivedPacketFra
;     | me.arrusnData[2]);                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+8 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL36        ; [CPU_U] |2123| 
        MOV       AL,@_m_stReceivedPacketFrame+8 ; [CPU_] |2123| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2123| 
        MOV       *-SP[3],AL            ; [CPU_] |2123| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2123| 
$C$DW$253	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$253, DW_AT_low_pc(0x00)
	.dwattr $C$DW$253, DW_AT_name("_sprintf")
	.dwattr $C$DW$253, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2123| 
        ; call occurs [#_sprintf] ; [] |2123| 
	.dwpsn	file "../source/CommInterface.c",line 2124,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 2124 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2124| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2124| 
$C$DW$254	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$254, DW_AT_low_pc(0x00)
	.dwattr $C$DW$254, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$254, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2124| 
        ; call occurs [#_UartDebugPrint] ; [] |2124| 
	.dwpsn	file "../source/CommInterface.c",line 2125,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2125 | sprintf(DebugPrintBuf, "\r\n arrusnData[3] %d\n", m_stReceivedPacketFra
;     | me.arrusnData[3]);                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+9 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL37        ; [CPU_U] |2125| 
        MOV       AL,@_m_stReceivedPacketFrame+9 ; [CPU_] |2125| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2125| 
        MOV       *-SP[3],AL            ; [CPU_] |2125| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2125| 
$C$DW$255	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$255, DW_AT_low_pc(0x00)
	.dwattr $C$DW$255, DW_AT_name("_sprintf")
	.dwattr $C$DW$255, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2125| 
        ; call occurs [#_sprintf] ; [] |2125| 
	.dwpsn	file "../source/CommInterface.c",line 2126,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 2126 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2126| 
        MOVB      AL,#50                ; [CPU_] |2126| 
$C$DW$256	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$256, DW_AT_low_pc(0x00)
	.dwattr $C$DW$256, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$256, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2126| 
        ; call occurs [#_UartDebugPrint] ; [] |2126| 
	.dwpsn	file "../source/CommInterface.c",line 2127,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2127 | sprintf(DebugPrintBuf, "\r\n arrusnData[4] %d\n", m_stReceivedPacketFra
;     | me.arrusnData[4]);                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+10 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL38        ; [CPU_U] |2127| 
        MOV       AL,@_m_stReceivedPacketFrame+10 ; [CPU_] |2127| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2127| 
        MOV       *-SP[3],AL            ; [CPU_] |2127| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2127| 
$C$DW$257	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$257, DW_AT_low_pc(0x00)
	.dwattr $C$DW$257, DW_AT_name("_sprintf")
	.dwattr $C$DW$257, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2127| 
        ; call occurs [#_sprintf] ; [] |2127| 
	.dwpsn	file "../source/CommInterface.c",line 2128,column 26,is_stmt,isa 0
;----------------------------------------------------------------------
; 2128 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2128| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2128| 
$C$DW$258	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$258, DW_AT_low_pc(0x00)
	.dwattr $C$DW$258, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$258, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2128| 
        ; call occurs [#_UartDebugPrint] ; [] |2128| 
	.dwpsn	file "../source/CommInterface.c",line 2129,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2129 | sprintf(DebugPrintBuf, "\r\n arrusnData[5] %d\n", m_stReceivedPacketFra
;     | me.arrusnData[5]);                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+11 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL39        ; [CPU_U] |2129| 
        MOV       AL,@_m_stReceivedPacketFrame+11 ; [CPU_] |2129| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2129| 
        MOV       *-SP[3],AL            ; [CPU_] |2129| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2129| 
$C$DW$259	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$259, DW_AT_low_pc(0x00)
	.dwattr $C$DW$259, DW_AT_name("_sprintf")
	.dwattr $C$DW$259, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2129| 
        ; call occurs [#_sprintf] ; [] |2129| 
	.dwpsn	file "../source/CommInterface.c",line 2130,column 26,is_stmt,isa 0
;----------------------------------------------------------------------
; 2130 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2130| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2130| 
$C$DW$260	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$260, DW_AT_low_pc(0x00)
	.dwattr $C$DW$260, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$260, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2130| 
        ; call occurs [#_UartDebugPrint] ; [] |2130| 
	.dwpsn	file "../source/CommInterface.c",line 2131,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2131 | sprintf(DebugPrintBuf, "\r\n arrusnData[6] %d\n", m_stReceivedPacketFra
;     | me.arrusnData[6]);                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+12 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL40        ; [CPU_U] |2131| 
        MOV       AL,@_m_stReceivedPacketFrame+12 ; [CPU_] |2131| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2131| 
        MOV       *-SP[3],AL            ; [CPU_] |2131| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2131| 
$C$DW$261	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$261, DW_AT_low_pc(0x00)
	.dwattr $C$DW$261, DW_AT_name("_sprintf")
	.dwattr $C$DW$261, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2131| 
        ; call occurs [#_sprintf] ; [] |2131| 
	.dwpsn	file "../source/CommInterface.c",line 2132,column 27,is_stmt,isa 0
;----------------------------------------------------------------------
; 2132 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2132| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2132| 
$C$DW$262	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$262, DW_AT_low_pc(0x00)
	.dwattr $C$DW$262, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$262, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2132| 
        ; call occurs [#_UartDebugPrint] ; [] |2132| 
	.dwpsn	file "../source/CommInterface.c",line 2133,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2133 | sprintf(DebugPrintBuf, "\r\n arrusnData[7] %d\n", m_stReceivedPacketFra
;     | me.arrusnData[7]);                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+13 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL41        ; [CPU_U] |2133| 
        MOV       AL,@_m_stReceivedPacketFrame+13 ; [CPU_] |2133| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2133| 
        MOV       *-SP[3],AL            ; [CPU_] |2133| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2133| 
$C$DW$263	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$263, DW_AT_low_pc(0x00)
	.dwattr $C$DW$263, DW_AT_name("_sprintf")
	.dwattr $C$DW$263, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2133| 
        ; call occurs [#_sprintf] ; [] |2133| 
	.dwpsn	file "../source/CommInterface.c",line 2134,column 28,is_stmt,isa 0
;----------------------------------------------------------------------
; 2134 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2134| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2134| 
$C$DW$264	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$264, DW_AT_low_pc(0x00)
	.dwattr $C$DW$264, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$264, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2134| 
        ; call occurs [#_UartDebugPrint] ; [] |2134| 
	.dwpsn	file "../source/CommInterface.c",line 2135,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 2135 | sprintf(DebugPrintBuf, "\r\n arrusnData[8] %d\n", m_stReceivedPacketFra
;     | me.arrusnData[8]);                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+14 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL42        ; [CPU_U] |2135| 
        MOV       AL,@_m_stReceivedPacketFrame+14 ; [CPU_] |2135| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2135| 
        MOV       *-SP[3],AL            ; [CPU_] |2135| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2135| 
$C$DW$265	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$265, DW_AT_low_pc(0x00)
	.dwattr $C$DW$265, DW_AT_name("_sprintf")
	.dwattr $C$DW$265, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2135| 
        ; call occurs [#_sprintf] ; [] |2135| 
	.dwpsn	file "../source/CommInterface.c",line 2136,column 27,is_stmt,isa 0
;----------------------------------------------------------------------
; 2136 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2136| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2136| 
$C$DW$266	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$266, DW_AT_low_pc(0x00)
	.dwattr $C$DW$266, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$266, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2136| 
        ; call occurs [#_UartDebugPrint] ; [] |2136| 
	.dwpsn	file "../source/CommInterface.c",line 2137,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 2137 | sprintf(DebugPrintBuf, "\r\n Default Vacuum: %d\n", m_stReceivedPacketF
;     | rame.arrusnData[11]);                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+17 ; [CPU_U] 
        MOVL      XAR4,#$C$FSL43        ; [CPU_U] |2137| 
        MOV       AL,@_m_stReceivedPacketFrame+17 ; [CPU_] |2137| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2137| 
        MOV       *-SP[3],AL            ; [CPU_] |2137| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2137| 
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_name("_sprintf")
	.dwattr $C$DW$267, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2137| 
        ; call occurs [#_sprintf] ; [] |2137| 
	.dwpsn	file "../source/CommInterface.c",line 2138,column 27,is_stmt,isa 0
;----------------------------------------------------------------------
; 2138 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 2143 | //!Clear the busy pin after receiving the bubbling time.               
; 2144 | //!So that processor can send any command to initiate next sequence    
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2138| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2138| 
$C$DW$268	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$268, DW_AT_low_pc(0x00)
	.dwattr $C$DW$268, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$268, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2138| 
        ; call occurs [#_UartDebugPrint] ; [] |2138| 
	.dwpsn	file "../source/CommInterface.c",line 2145,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2145 | SPIClearCommCtrlBusyInt();                                             
; 2147 | //!Limit the First dilution freq to MIN_MIXING_FREQ (100) if it is     
; 2148 | //! less than MIN_MIXING_FREQ (100)                                    
;----------------------------------------------------------------------
$C$DW$269	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$269, DW_AT_low_pc(0x00)
	.dwattr $C$DW$269, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$269, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2145| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2145| 
	.dwpsn	file "../source/CommInterface.c",line 2149,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2149 | if(g_usnFirstDilutionFreq < MIN_MIXING_FREQ)                           
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnFirstDilutionFreq ; [CPU_U] 
        MOV       AL,@_g_usnFirstDilutionFreq ; [CPU_] |2149| 
        CMPB      AL,#100               ; [CPU_] |2149| 
        B         $C$L167,HIS           ; [CPU_] |2149| 
        ; branchcc occurs ; [] |2149| 
	.dwpsn	file "../source/CommInterface.c",line 2151,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2151 | g_usnFirstDilutionFreq = MIN_MIXING_FREQ;                              
; 2153 | //!Limit the First dilution freq to MAX_MIXING_FREQ (300) if it is     
; 2154 | //! greater than 300                                                   
;----------------------------------------------------------------------
        MOVB      @_g_usnFirstDilutionFreq,#100,UNC ; [CPU_] |2151| 
$C$L167:    
	.dwpsn	file "../source/CommInterface.c",line 2155,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2155 | if(g_usnFirstDilutionFreq > MAX_MIXING_FREQ)                           
;----------------------------------------------------------------------
        CMP       @_g_usnFirstDilutionFreq,#300 ; [CPU_] |2155| 
        B         $C$L168,LOS           ; [CPU_] |2155| 
        ; branchcc occurs ; [] |2155| 
	.dwpsn	file "../source/CommInterface.c",line 2157,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2157 | g_usnFirstDilutionFreq = MAX_MIXING_FREQ;                              
; 2159 | //!Limit the Lyse Mixing freq to MIN_MIXING_FREQ (100) if it is        
; 2160 | //! less than MIN_MIXING_FREQ (100)                                    
;----------------------------------------------------------------------
        MOV       @_g_usnFirstDilutionFreq,#300 ; [CPU_] |2157| 
$C$L168:    
	.dwpsn	file "../source/CommInterface.c",line 2161,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2161 | if(g_usnLyseMixingFreq < MIN_MIXING_FREQ)                              
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnLyseMixingFreq ; [CPU_U] 
        MOV       AL,@_g_usnLyseMixingFreq ; [CPU_] |2161| 
        CMPB      AL,#100               ; [CPU_] |2161| 
        B         $C$L169,HIS           ; [CPU_] |2161| 
        ; branchcc occurs ; [] |2161| 
	.dwpsn	file "../source/CommInterface.c",line 2163,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2163 | g_usnLyseMixingFreq = MIN_MIXING_FREQ;                                 
; 2165 | //!Limit the Lyse Mixing freq to MAX_MIXING_FREQ (300) if it is        
; 2166 | //! greater than MAX_MIXING_FREQ (300)                                 
;----------------------------------------------------------------------
        MOVB      @_g_usnLyseMixingFreq,#100,UNC ; [CPU_] |2163| 
$C$L169:    
	.dwpsn	file "../source/CommInterface.c",line 2167,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2167 | if(g_usnLyseMixingFreq > MAX_MIXING_FREQ)                              
;----------------------------------------------------------------------
        CMP       @_g_usnLyseMixingFreq,#300 ; [CPU_] |2167| 
        B         $C$L170,LOS           ; [CPU_] |2167| 
        ; branchcc occurs ; [] |2167| 
	.dwpsn	file "../source/CommInterface.c",line 2169,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2169 | g_usnLyseMixingFreq = MAX_MIXING_FREQ;                                 
; 2171 | //!Limit the Final RBC Mixing freq to MIN_MIXING_FREQ (100) if it      
; 2172 | //! is less than MIN_MIXING_FREQ (100)                                 
;----------------------------------------------------------------------
        MOV       @_g_usnLyseMixingFreq,#300 ; [CPU_] |2169| 
$C$L170:    
	.dwpsn	file "../source/CommInterface.c",line 2173,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2173 | if(g_usnFinalRBCMixingFreq < MIN_MIXING_FREQ)                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnFinalRBCMixingFreq ; [CPU_U] 
        MOV       AL,@_g_usnFinalRBCMixingFreq ; [CPU_] |2173| 
        CMPB      AL,#100               ; [CPU_] |2173| 
        B         $C$L171,HIS           ; [CPU_] |2173| 
        ; branchcc occurs ; [] |2173| 
	.dwpsn	file "../source/CommInterface.c",line 2175,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2175 | g_usnFinalRBCMixingFreq = MIN_MIXING_FREQ;                             
; 2177 | //!Limit the Final RBC Mixing freq to MAX_MIXING_FREQ (300) if it      
; 2178 | //! is less than MAX_MIXING_FREQ (300)                                 
;----------------------------------------------------------------------
        MOVB      @_g_usnFinalRBCMixingFreq,#100,UNC ; [CPU_] |2175| 
$C$L171:    
	.dwpsn	file "../source/CommInterface.c",line 2179,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2179 | if(g_usnFinalRBCMixingFreq > MAX_MIXING_FREQ)                          
;----------------------------------------------------------------------
        CMP       @_g_usnFinalRBCMixingFreq,#300 ; [CPU_] |2179| 
        B         $C$L179,LOS           ; [CPU_] |2179| 
        ; branchcc occurs ; [] |2179| 
	.dwpsn	file "../source/CommInterface.c",line 2181,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 2181 | g_usnFinalRBCMixingFreq = MAX_MIXING_FREQ;                             
;----------------------------------------------------------------------
        MOV       @_g_usnFinalRBCMixingFreq,#300 ; [CPU_] |2181| 
	.dwpsn	file "../source/CommInterface.c",line 2183,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2183 | break;                                                                 
; 2185 | case SITARA_PRIME_WITH_RINSE_CMD: // HN Added                          
; 2186 | //!Set the current mode to Rinse prime before flow cal Sequence        
; 2187 | //!which will be given                                                 
; 2188 | //!input to MBD to initiate Counting Time Sequence                     
;----------------------------------------------------------------------
        B         $C$L179,UNC           ; [CPU_] |2183| 
        ; branch occurs ; [] |2183| 
$C$L172:    
	.dwpsn	file "../source/CommInterface.c",line 2189,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2189 | g_usnCurrentMode = RINSE_PRIME_FLOW_CALIBRATION;                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20524 ; [CPU_] |2189| 
	.dwpsn	file "../source/CommInterface.c",line 2190,column 14,is_stmt,isa 0
;----------------------------------------------------------------------
; 2190 | break;                                                                 
; 2192 | case SITARA_FLOW_CALIBRATION_CMD:                                      
; 2193 | //!Set the current mode to Counting Time Sequence which will be given  
; 2194 | //!input to MBD to initiate Counting Time Sequence                     
;----------------------------------------------------------------------
        B         $C$L179,UNC           ; [CPU_] |2190| 
        ; branch occurs ; [] |2190| 
$C$L173:    
	.dwpsn	file "../source/CommInterface.c",line 2195,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2195 | g_usnCurrentMode = FLOW_CALIBRATION;                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20523 ; [CPU_] |2195| 
	.dwpsn	file "../source/CommInterface.c",line 2196,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2196 | break;                                                                 
; 2197 | case SITARA_PRESSURE_CALIBRATION_CMD: // HN Added                      
;----------------------------------------------------------------------
        B         $C$L179,UNC           ; [CPU_] |2196| 
        ; branch occurs ; [] |2196| 
$C$L174:    
	.dwpsn	file "../source/CommInterface.c",line 2198,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2198 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$270	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$270, DW_AT_low_pc(0x00)
	.dwattr $C$DW$270, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$270, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2198| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2198| 
	.dwpsn	file "../source/CommInterface.c",line 2199,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2199 | sprintf(statDebugPrintBuf, "\r\n SITARA_PRESSURE_CALIBRATION_CMD receiv
;     | ed\n");                                                                
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL44        ; [CPU_U] |2199| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2199| 
        MOVL      XAR4,#_statDebugPrintBuf ; [CPU_U] |2199| 
$C$DW$271	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$271, DW_AT_low_pc(0x00)
	.dwattr $C$DW$271, DW_AT_name("_sprintf")
	.dwattr $C$DW$271, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2199| 
        ; call occurs [#_sprintf] ; [] |2199| 
	.dwpsn	file "../source/CommInterface.c",line 2200,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2200 | UartDebugPrint((int *)statDebugPrintBuf, 50);                          
; 2201 | //!The current execution mode shall be set to no mode                  
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2200| 
        MOVL      XAR4,#_statDebugPrintBuf ; [CPU_U] |2200| 
$C$DW$272	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$272, DW_AT_low_pc(0x00)
	.dwattr $C$DW$272, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$272, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2200| 
        ; call occurs [#_UartDebugPrint] ; [] |2200| 
	.dwpsn	file "../source/CommInterface.c",line 2202,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2202 | g_usnCurrentModeMajor = ZERO;                                          
; 2203 | //!The current execution mode shall be set to no mode                  
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#0 ; [CPU_] |2202| 
	.dwpsn	file "../source/CommInterface.c",line 2204,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2204 | g_usnCurrentMode = ZERO;                                               
;----------------------------------------------------------------------
        MOV       @_g_usnCurrentMode,#0 ; [CPU_] |2204| 
	.dwpsn	file "../source/CommInterface.c",line 2205,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2205 | g_bPressureCaliStart = TRUE ;                                          
;----------------------------------------------------------------------
        MOVB      @_g_bPressureCaliStart,#1,UNC ; [CPU_] |2205| 
	.dwpsn	file "../source/CommInterface.c",line 2206,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2206 | break;                                                                 
; 2207 | case SITARA_FLOW_CALIBRATION_RESULTS_CMD:                              
; 2208 | //!Set the current mode to Counting Time Sequence which will be given  
; 2209 | //!input to MBD to initiate Counting Time Sequence                     
; 2210 | //g_usnCurrentMode = FLOW_CALIBRATION;                                 
;----------------------------------------------------------------------
        B         $C$L179,UNC           ; [CPU_] |2206| 
        ; branch occurs ; [] |2206| 
$C$L175:    
	.dwpsn	file "../source/CommInterface.c",line 2211,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2211 | g_usnWbcCalibratedTime = m_stReceivedPacketFrame.arrusnData[0];     //D
;     | S_added                                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+6 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+6 ; [CPU_] |2211| 
        MOVW      DP,#_g_usnWbcCalibratedTime ; [CPU_U] 
        MOV       @_g_usnWbcCalibratedTime,AL ; [CPU_] |2211| 
	.dwpsn	file "../source/CommInterface.c",line 2212,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2212 | g_usnRbcCalibratedTime = m_stReceivedPacketFrame.arrusnData[1];     //D
;     | S_added                                                                
; 2214 | //Changing msec to 20msec counter                                      
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+7 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+7 ; [CPU_] |2212| 
        MOVW      DP,#_g_usnRbcCalibratedTime ; [CPU_U] 
        MOV       @_g_usnRbcCalibratedTime,AL ; [CPU_] |2212| 
	.dwpsn	file "../source/CommInterface.c",line 2215,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2215 | g_usnWbcCalibratedTime = g_usnWbcCalibratedTime/20;                    
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnWbcCalibratedTime ; [CPU_U] 
        MOVB      AH,#20                ; [CPU_] |2215| 
        MOV       AL,@_g_usnWbcCalibratedTime ; [CPU_] |2215| 
$C$DW$273	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$273, DW_AT_low_pc(0x00)
	.dwattr $C$DW$273, DW_AT_name("I$$DIV")
	.dwattr $C$DW$273, DW_AT_TI_call

        FFC       XAR7,#I$$DIV          ; [CPU_] |2215| 
        ; call occurs [#I$$DIV] ; [] |2215| 
        MOV       @_g_usnWbcCalibratedTime,AL ; [CPU_] |2215| 
	.dwpsn	file "../source/CommInterface.c",line 2216,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2216 | g_usnRbcCalibratedTime = g_usnRbcCalibratedTime/20;                    
; 2218 | //!Clear the busy pin                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnRbcCalibratedTime ; [CPU_U] 
        MOVB      AH,#20                ; [CPU_] |2216| 
        MOV       AL,@_g_usnRbcCalibratedTime ; [CPU_] |2216| 
$C$DW$274	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$274, DW_AT_low_pc(0x00)
	.dwattr $C$DW$274, DW_AT_name("I$$DIV")
	.dwattr $C$DW$274, DW_AT_TI_call

        FFC       XAR7,#I$$DIV          ; [CPU_] |2216| 
        ; call occurs [#I$$DIV] ; [] |2216| 
        MOV       @_g_usnRbcCalibratedTime,AL ; [CPU_] |2216| 
	.dwpsn	file "../source/CommInterface.c",line 2219,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2219 | SPIClearCommCtrlBusyInt();                                             
; 2220 | //!Reset the current mode variables                                    
;----------------------------------------------------------------------
$C$DW$275	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$275, DW_AT_low_pc(0x00)
	.dwattr $C$DW$275, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$275, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2219| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2219| 
	.dwpsn	file "../source/CommInterface.c",line 2221,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2221 | g_usnCurrentModeMajor = ZERO;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#0 ; [CPU_] |2221| 
	.dwpsn	file "../source/CommInterface.c",line 2222,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2222 | g_usnCurrentMode = ZERO;                                               
;----------------------------------------------------------------------
        MOV       @_g_usnCurrentMode,#0 ; [CPU_] |2222| 
	.dwpsn	file "../source/CommInterface.c",line 2223,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2223 | g_usnCurrentModeTemp = ZERO;                                           
;----------------------------------------------------------------------
        MOV       @_g_usnCurrentModeTemp,#0 ; [CPU_] |2223| 
	.dwpsn	file "../source/CommInterface.c",line 2224,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2224 | sprintf(DebugPrintBuf, "\r\n SITARA_FLOW_CALIBRATION_RESULTS_CMD \n"); 
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL45        ; [CPU_U] |2224| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2224| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2224| 
$C$DW$276	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$276, DW_AT_low_pc(0x00)
	.dwattr $C$DW$276, DW_AT_name("_sprintf")
	.dwattr $C$DW$276, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2224| 
        ; call occurs [#_sprintf] ; [] |2224| 
	.dwpsn	file "../source/CommInterface.c",line 2225,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2225 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2225| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2225| 
$C$DW$277	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$277, DW_AT_low_pc(0x00)
	.dwattr $C$DW$277, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$277, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2225| 
        ; call occurs [#_UartDebugPrint] ; [] |2225| 
	.dwpsn	file "../source/CommInterface.c",line 2226,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2226 | sprintf(DebugPrintBuf, "\r\n g_usnWbcCalibratedTime : %d\n", g_usnWbcCa
;     | libratedTime);                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnWbcCalibratedTime ; [CPU_U] 
        MOVL      XAR4,#$C$FSL46        ; [CPU_U] |2226| 
        MOV       AL,@_g_usnWbcCalibratedTime ; [CPU_] |2226| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2226| 
        MOV       *-SP[3],AL            ; [CPU_] |2226| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2226| 
$C$DW$278	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$278, DW_AT_low_pc(0x00)
	.dwattr $C$DW$278, DW_AT_name("_sprintf")
	.dwattr $C$DW$278, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2226| 
        ; call occurs [#_sprintf] ; [] |2226| 
	.dwpsn	file "../source/CommInterface.c",line 2227,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2227 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2227| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2227| 
$C$DW$279	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$279, DW_AT_low_pc(0x00)
	.dwattr $C$DW$279, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$279, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2227| 
        ; call occurs [#_UartDebugPrint] ; [] |2227| 
	.dwpsn	file "../source/CommInterface.c",line 2228,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2228 | sprintf(DebugPrintBuf, "\r\n g_usnRbcCalibratedTime : %d\n", g_usnRbcCa
;     | libratedTime);                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnRbcCalibratedTime ; [CPU_U] 
        MOVL      XAR4,#$C$FSL47        ; [CPU_U] |2228| 
        MOV       AL,@_g_usnRbcCalibratedTime ; [CPU_] |2228| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2228| 
        MOV       *-SP[3],AL            ; [CPU_] |2228| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2228| 
$C$DW$280	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$280, DW_AT_low_pc(0x00)
	.dwattr $C$DW$280, DW_AT_name("_sprintf")
	.dwattr $C$DW$280, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2228| 
        ; call occurs [#_sprintf] ; [] |2228| 
	.dwpsn	file "../source/CommInterface.c",line 2229,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2229 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2229| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2229| 
$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$281, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2229| 
        ; call occurs [#_UartDebugPrint] ; [] |2229| 
	.dwpsn	file "../source/CommInterface.c",line 2230,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2230 | break;                                                                 
; 2231 | case SITARA_TEST_DATA_FROM_UI:                                         
;----------------------------------------------------------------------
        B         $C$L179,UNC           ; [CPU_] |2230| 
        ; branch occurs ; [] |2230| 
$C$L176:    
	.dwpsn	file "../source/CommInterface.c",line 2232,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2232 | sprintf(DebugPrintBuf, "\r\n SITARA_TEST_DATA_FROM_UI \n");            
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL48        ; [CPU_U] |2232| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2232| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2232| 
$C$DW$282	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$282, DW_AT_low_pc(0x00)
	.dwattr $C$DW$282, DW_AT_name("_sprintf")
	.dwattr $C$DW$282, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2232| 
        ; call occurs [#_sprintf] ; [] |2232| 
	.dwpsn	file "../source/CommInterface.c",line 2233,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2233 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 2234 | //g_TestDataI0 = m_stReceivedPacketFrame.arrusnData[0]; //Using for  co
;     | unt vacuum                                                             
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2233| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2233| 
$C$DW$283	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$283, DW_AT_low_pc(0x00)
	.dwattr $C$DW$283, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$283, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2233| 
        ; call occurs [#_UartDebugPrint] ; [] |2233| 
	.dwpsn	file "../source/CommInterface.c",line 2235,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2235 | g_TestDataI1 = m_stReceivedPacketFrame.arrusnData[1];                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+7 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+7 ; [CPU_] |2235| 
        MOVW      DP,#_g_TestDataI1     ; [CPU_U] 
        MOV       @_g_TestDataI1,AL     ; [CPU_] |2235| 
	.dwpsn	file "../source/CommInterface.c",line 2236,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2236 | g_TestDataI2 = m_stReceivedPacketFrame.arrusnData[2]; //For settling Ti
;     | me                                                                     
; 2238 | //!Clear the busy pin after receiving the bubbling time.               
; 2239 | //!So that processor can send any command to initiate next sequence    
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+8 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+8 ; [CPU_] |2236| 
        MOVW      DP,#_g_TestDataI2     ; [CPU_U] 
        MOV       @_g_TestDataI2,AL     ; [CPU_] |2236| 
	.dwpsn	file "../source/CommInterface.c",line 2240,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2240 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$284	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$284, DW_AT_low_pc(0x00)
	.dwattr $C$DW$284, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$284, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2240| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2240| 
	.dwpsn	file "../source/CommInterface.c",line 2242,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2242 | break;                                                                 
; 2244 | default:                                                               
; 2245 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L179,UNC           ; [CPU_] |2242| 
        ; branch occurs ; [] |2242| 
$C$L177:    
	.dwpsn	file "../source/CommInterface.c",line 2046,column 5,is_stmt,isa 0
        MOVZ      AR6,*-SP[4]           ; [CPU_] |2046| 
        CMP       AR6,#5122             ; [CPU_] |2046| 
        B         $C$L178,GT            ; [CPU_] |2046| 
        ; branchcc occurs ; [] |2046| 
        CMP       AR6,#5122             ; [CPU_] |2046| 
        B         $C$L173,EQ            ; [CPU_] |2046| 
        ; branchcc occurs ; [] |2046| 
        MOVZ      AR7,AR6               ; [CPU_] |2046| 
        MOVL      XAR4,#4097            ; [CPU_U] |2046| 
        MOVL      ACC,XAR4              ; [CPU_] |2046| 
        CMPL      ACC,XAR7              ; [CPU_] |2046| 
        B         $C$L172,EQ            ; [CPU_] |2046| 
        ; branchcc occurs ; [] |2046| 
        MOVZ      AR7,AR6               ; [CPU_] |2046| 
        MOVL      XAR4,#4137            ; [CPU_U] |2046| 
        MOVL      ACC,XAR4              ; [CPU_] |2046| 
        CMPL      ACC,XAR7              ; [CPU_] |2046| 
        B         $C$L174,EQ            ; [CPU_] |2046| 
        ; branchcc occurs ; [] |2046| 
        MOVZ      AR6,AR6               ; [CPU_] |2046| 
        MOVL      XAR4,#5121            ; [CPU_U] |2046| 
        MOVL      ACC,XAR4              ; [CPU_] |2046| 
        CMPL      ACC,XAR6              ; [CPU_] |2046| 
        B         $C$L158,EQ            ; [CPU_] |2046| 
        ; branchcc occurs ; [] |2046| 
        B         $C$L179,UNC           ; [CPU_] |2046| 
        ; branch occurs ; [] |2046| 
$C$L178:    
        MOVZ      AR7,AR6               ; [CPU_] |2046| 
        MOVL      XAR4,#5123            ; [CPU_U] |2046| 
        MOVL      ACC,XAR4              ; [CPU_] |2046| 
        CMPL      ACC,XAR7              ; [CPU_] |2046| 
        B         $C$L175,EQ            ; [CPU_] |2046| 
        ; branchcc occurs ; [] |2046| 
        MOVZ      AR6,AR6               ; [CPU_] |2046| 
        MOVL      XAR4,#5124            ; [CPU_U] |2046| 
        MOVL      ACC,XAR4              ; [CPU_] |2046| 
        CMPL      ACC,XAR6              ; [CPU_] |2046| 
        B         $C$L176,EQ            ; [CPU_] |2046| 
        ; branchcc occurs ; [] |2046| 
        B         $C$L179,UNC           ; [CPU_] |2046| 
        ; branch occurs ; [] |2046| 
$C$L179:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$285	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$285, DW_AT_low_pc(0x00)
	.dwattr $C$DW$285, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$242, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$242, DW_AT_TI_end_line(0x8c8)
	.dwattr $C$DW$242, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$242

	.sect	".text:_CIValveTestCompleted"
	.clink
	.global	_CIValveTestCompleted

$C$DW$286	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$286, DW_AT_name("CIValveTestCompleted")
	.dwattr $C$DW$286, DW_AT_low_pc(_CIValveTestCompleted)
	.dwattr $C$DW$286, DW_AT_high_pc(0x00)
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_CIValveTestCompleted")
	.dwattr $C$DW$286, DW_AT_external
	.dwattr $C$DW$286, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$286, DW_AT_TI_begin_line(0x8cb)
	.dwattr $C$DW$286, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$286, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 2252,column 1,is_stmt,address _CIValveTestCompleted,isa 0

	.dwfde $C$DW$CIE, _CIValveTestCompleted
$C$DW$287	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$287, DW_AT_name("ucValveNumber")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_ucValveNumber")
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$287, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2251 | void CIValveTestCompleted(unsigned char ucValveNumber)                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIValveTestCompleted         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIValveTestCompleted:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$288	.dwtag  DW_TAG_variable
	.dwattr $C$DW$288, DW_AT_name("ucValveNumber")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_ucValveNumber")
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$288, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 2253 | //!Set the valve number which was tested                               
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |2252| 
	.dwpsn	file "../source/CommInterface.c",line 2254,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2254 | m_ucValveNo = ucValveNumber;                                           
; 2255 | //!Frame the service handling packet for valve test completion         
;----------------------------------------------------------------------
        MOVW      DP,#_m_ucValveNo      ; [CPU_U] 
        MOV       @_m_ucValveNo,AL      ; [CPU_] |2254| 
	.dwpsn	file "../source/CommInterface.c",line 2256,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2256 | CIFormServiceHandlingPacket(VALVE_TEST_COMPLETION, FALSE);             
;----------------------------------------------------------------------
        MOVB      AH,#0                 ; [CPU_] |2256| 
        MOV       AL,#20493             ; [CPU_] |2256| 
$C$DW$289	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$289, DW_AT_low_pc(0x00)
	.dwattr $C$DW$289, DW_AT_name("_CIFormServiceHandlingPacket")
	.dwattr $C$DW$289, DW_AT_TI_call

        LCR       #_CIFormServiceHandlingPacket ; [CPU_] |2256| 
        ; call occurs [#_CIFormServiceHandlingPacket] ; [] |2256| 
	.dwpsn	file "../source/CommInterface.c",line 2257,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$290	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$290, DW_AT_low_pc(0x00)
	.dwattr $C$DW$290, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$286, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$286, DW_AT_TI_end_line(0x8d1)
	.dwattr $C$DW$286, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$286

	.sect	".text:_CIFormACKPacket"
	.clink
	.global	_CIFormACKPacket

$C$DW$291	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$291, DW_AT_name("CIFormACKPacket")
	.dwattr $C$DW$291, DW_AT_low_pc(_CIFormACKPacket)
	.dwattr $C$DW$291, DW_AT_high_pc(0x00)
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_CIFormACKPacket")
	.dwattr $C$DW$291, DW_AT_external
	.dwattr $C$DW$291, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$291, DW_AT_TI_begin_line(0x8da)
	.dwattr $C$DW$291, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$291, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommInterface.c",line 2267,column 1,is_stmt,address _CIFormACKPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormACKPacket
$C$DW$292	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$292, DW_AT_name("bTransmitNACK")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_bTransmitNACK")
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$292, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2266 | void CIFormACKPacket(bool_t bTransmitNACK)                             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormACKPacket              FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIFormACKPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$293	.dwtag  DW_TAG_variable
	.dwattr $C$DW$293, DW_AT_name("bTransmitNACK")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_bTransmitNACK")
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$293, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 2268 | //!If the command received from processor is not valid, then send NAK  
; 2269 | //!packet to processor.                                                
; 2270 | //!If the command received from processor is valid, then send Ack packe
;     | t                                                                      
; 2271 | //!to processor                                                        
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |2267| 
	.dwpsn	file "../source/CommInterface.c",line 2272,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2272 | if(bTransmitNACK == TRUE)                                              
; 2274 |     //!AFter boot-up, NAK command is continuously sent to Sitara       
; 2275 |     //! which is blocking the version command and IPC synchronization a
;     | t Sitara                                                               
; 2276 |     //! is not happening due to multiple NAK commands.                 
; 2277 |     //! So NAK is removed.                                             
; 2278 |     //! Additionally, NAK is not handled by process.It shall be used in
;     |  future                                                                
; 2279 | //sprintf(DebugPrintBuf, "\n DELFINO_NACK_SUB_CMD \n");                
; 2280 | //UartDebugPrint((int *)DebugPrintBuf, 50);                            
; 2281 |         //CPFrameDataPacket(DELFINO_NACK_MAIN_CMD, DELFINO_NACK_SUB_CMD
;     | , FALSE);                                                              
;----------------------------------------------------------------------
        CMPB      AL,#1                 ; [CPU_] |2272| 
        B         $C$L180,EQ            ; [CPU_] |2272| 
        ; branchcc occurs ; [] |2272| 
	.dwpsn	file "../source/CommInterface.c",line 2282,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2283 | else                                                                   
;----------------------------------------------------------------------
	.dwpsn	file "../source/CommInterface.c",line 2285,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2285 | sprintf(DebugPrintBuf, "\n DELFINO_ACK_SUB_CMD \n");                   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL49        ; [CPU_U] |2285| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2285| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2285| 
$C$DW$294	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$294, DW_AT_low_pc(0x00)
	.dwattr $C$DW$294, DW_AT_name("_sprintf")
	.dwattr $C$DW$294, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2285| 
        ; call occurs [#_sprintf] ; [] |2285| 
	.dwpsn	file "../source/CommInterface.c",line 2286,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2286 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2286| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2286| 
$C$DW$295	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$295, DW_AT_low_pc(0x00)
	.dwattr $C$DW$295, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$295, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2286| 
        ; call occurs [#_UartDebugPrint] ; [] |2286| 
	.dwpsn	file "../source/CommInterface.c",line 2287,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2287 | CPFrameDataPacket(DELFINO_ACK_MAIN_CMD, DELFINO_ACK_SUB_CMD, FALSE);   
;----------------------------------------------------------------------
        MOV       AL,#12287             ; [CPU_] |2287| 
        MOV       AH,#12284             ; [CPU_] |2287| 
        MOVB      XAR4,#0               ; [CPU_] |2287| 
$C$DW$296	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$296, DW_AT_low_pc(0x00)
	.dwattr $C$DW$296, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$296, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2287| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2287| 
	.dwpsn	file "../source/CommInterface.c",line 2289,column 1,is_stmt,isa 0
$C$L180:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$297	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$297, DW_AT_low_pc(0x00)
	.dwattr $C$DW$297, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$291, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$291, DW_AT_TI_end_line(0x8f1)
	.dwattr $C$DW$291, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$291

	.sect	".text:_CIFormPatientHandlingPacket"
	.clink
	.global	_CIFormPatientHandlingPacket

$C$DW$298	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$298, DW_AT_name("CIFormPatientHandlingPacket")
	.dwattr $C$DW$298, DW_AT_low_pc(_CIFormPatientHandlingPacket)
	.dwattr $C$DW$298, DW_AT_high_pc(0x00)
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_CIFormPatientHandlingPacket")
	.dwattr $C$DW$298, DW_AT_external
	.dwattr $C$DW$298, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$298, DW_AT_TI_begin_line(0x8fa)
	.dwattr $C$DW$298, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$298, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 2299,column 1,is_stmt,address _CIFormPatientHandlingPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormPatientHandlingPacket
$C$DW$299	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$299, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$299, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2298 | void CIFormPatientHandlingPacket(uint16_t usnSubCmd)                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormPatientHandlingPacket  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIFormPatientHandlingPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$300	.dwtag  DW_TAG_variable
	.dwattr $C$DW$300, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$300, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 2300 | //!Frame patient handling packet to be sent to processor               
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |2299| 
	.dwpsn	file "../source/CommInterface.c",line 2301,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2301 | CPFrameDataPacket(DELFINO_PATIENT_HANDLING, usnSubCmd, FALSE);         
;----------------------------------------------------------------------
        MOVB      XAR4,#0               ; [CPU_] |2301| 
        MOV       AL,#8193              ; [CPU_] |2301| 
        MOV       AH,*-SP[1]            ; [CPU_] |2301| 
$C$DW$301	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$301, DW_AT_low_pc(0x00)
	.dwattr $C$DW$301, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$301, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2301| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2301| 
	.dwpsn	file "../source/CommInterface.c",line 2302,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$302	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$302, DW_AT_low_pc(0x00)
	.dwattr $C$DW$302, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$298, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$298, DW_AT_TI_end_line(0x8fe)
	.dwattr $C$DW$298, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$298

	.sect	".text:_CIReInitialiseOnNewMeasurement"
	.clink
	.global	_CIReInitialiseOnNewMeasurement

$C$DW$303	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$303, DW_AT_name("CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$303, DW_AT_low_pc(_CIReInitialiseOnNewMeasurement)
	.dwattr $C$DW$303, DW_AT_high_pc(0x00)
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$303, DW_AT_external
	.dwattr $C$DW$303, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$303, DW_AT_TI_begin_line(0x907)
	.dwattr $C$DW$303, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$303, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/CommInterface.c",line 2312,column 1,is_stmt,address _CIReInitialiseOnNewMeasurement,isa 0

	.dwfde $C$DW$CIE, _CIReInitialiseOnNewMeasurement
;----------------------------------------------------------------------
; 2311 | void CIReInitialiseOnNewMeasurement()                                  
; 2313 | //!Reset the the ACK variable                                          
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIReInitialiseOnNewMeasurement FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_CIReInitialiseOnNewMeasurement:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/CommInterface.c",line 2314,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2314 | m_bNAckReceived = FALSE;                                               
; 2316 | //g_bStartButtonStatus = TRUE;                                         
; 2317 | //! Set the cell type to Invalid                                       
;----------------------------------------------------------------------
        MOVW      DP,#_m_bNAckReceived  ; [CPU_U] 
        MOV       @_m_bNAckReceived,#0  ; [CPU_] |2314| 
	.dwpsn	file "../source/CommInterface.c",line 2318,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2318 | m_eTypeofCell = ecellInvalid;                                          
; 2320 | //!Reset the cycle completion to default on every new measurement -HM  
; 2321 | //m_eCycleCompletion = eDefault;                                       
; 2323 | //!Sequence completed to be set to false at the beginning of the sequen
;     | ce -HM                                                                 
; 2324 | //m_bSequenceCompleted = FALSE;                                        
; 2326 | //! Reset the PAcket count                                             
;----------------------------------------------------------------------
        MOV       @_m_eTypeofCell,#0    ; [CPU_] |2318| 
	.dwpsn	file "../source/CommInterface.c",line 2327,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2327 | m_usnPacketCount = 0;                                                  
; 2328 | //! Reset the NextSecRawDataReq                                        
;----------------------------------------------------------------------
        MOV       @_m_usnPacketCount,#0 ; [CPU_] |2327| 
	.dwpsn	file "../source/CommInterface.c",line 2329,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2329 | g_bNextSecRawDataReq = FALSE;                                          
; 2330 | //! Reset the Monitoring timer on new measurement                      
;----------------------------------------------------------------------
        MOV       @_g_bNextSecRawDataReq,#0 ; [CPU_] |2329| 
	.dwpsn	file "../source/CommInterface.c",line 2331,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2331 | g_unSequenceStateTime = 0;                                             
; 2332 | //! Call Comm Protocol ReInitialiseOnNewMeasurement Function           
;----------------------------------------------------------------------
        MOVB      ACC,#0                ; [CPU_] |2331| 
        MOVW      DP,#_g_unSequenceStateTime ; [CPU_U] 
        MOVL      @_g_unSequenceStateTime,ACC ; [CPU_] |2331| 
	.dwpsn	file "../source/CommInterface.c",line 2333,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2333 | CPReInitialiseOnNewMeasurement();                                      
;----------------------------------------------------------------------
$C$DW$304	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$304, DW_AT_low_pc(0x00)
	.dwattr $C$DW$304, DW_AT_name("_CPReInitialiseOnNewMeasurement")
	.dwattr $C$DW$304, DW_AT_TI_call

        LCR       #_CPReInitialiseOnNewMeasurement ; [CPU_] |2333| 
        ; call occurs [#_CPReInitialiseOnNewMeasurement] ; [] |2333| 
	.dwpsn	file "../source/CommInterface.c",line 2335,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2335 | SysReInit();                                                           
;----------------------------------------------------------------------
$C$DW$305	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$305, DW_AT_low_pc(0x00)
	.dwattr $C$DW$305, DW_AT_name("_SysReInit")
	.dwattr $C$DW$305, DW_AT_TI_call

        LCR       #_SysReInit           ; [CPU_] |2335| 
        ; call occurs [#_SysReInit] ; [] |2335| 
	.dwpsn	file "../source/CommInterface.c",line 2336,column 1,is_stmt,isa 0
$C$DW$306	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$306, DW_AT_low_pc(0x00)
	.dwattr $C$DW$306, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$303, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$303, DW_AT_TI_end_line(0x920)
	.dwattr $C$DW$303, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$303

	.sect	".text:_CIProcessCalibrationHandlingCmd"
	.clink
	.global	_CIProcessCalibrationHandlingCmd

$C$DW$307	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$307, DW_AT_name("CIProcessCalibrationHandlingCmd")
	.dwattr $C$DW$307, DW_AT_low_pc(_CIProcessCalibrationHandlingCmd)
	.dwattr $C$DW$307, DW_AT_high_pc(0x00)
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_CIProcessCalibrationHandlingCmd")
	.dwattr $C$DW$307, DW_AT_external
	.dwattr $C$DW$307, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$307, DW_AT_TI_begin_line(0x929)
	.dwattr $C$DW$307, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$307, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommInterface.c",line 2346,column 1,is_stmt,address _CIProcessCalibrationHandlingCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessCalibrationHandlingCmd
$C$DW$308	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$308, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$308, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2345 | void CIProcessCalibrationHandlingCmd(uint16_t usnMinorCmd)             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessCalibrationHandlingCmd FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessCalibrationHandlingCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$309	.dwtag  DW_TAG_variable
	.dwattr $C$DW$309, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$309, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 2347 | //!Check the minor command received in calibration mode and set the seq
;     | uence                                                                  
; 2348 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |2346| 
	.dwpsn	file "../source/CommInterface.c",line 2349,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2349 | switch(usnMinorCmd)                                                    
; 2351 |     //!If the command received from processor is Auto Calibration,     
; 2352 |     //!Set the current mode to Auto Calibration which will be given    
; 2353 |     //!input to MBD to initiate Auto Calibration                       
; 2354 |         case SITARA_CALIBRATION_AUTO_WB:                               
; 2355 |     //!Re-initilaize the variables and proceed                         
;----------------------------------------------------------------------
        B         $C$L199,UNC           ; [CPU_] |2349| 
        ; branch occurs ; [] |2349| 
$C$L181:    
	.dwpsn	file "../source/CommInterface.c",line 2356,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2356 | CIReInitialiseOnNewMeasurement();                                      
;----------------------------------------------------------------------
$C$DW$310	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$310, DW_AT_low_pc(0x00)
	.dwattr $C$DW$310, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$310, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |2356| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |2356| 
	.dwpsn	file "../source/CommInterface.c",line 2358,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2358 | g_usnCurrentMode = AUTO_CALIBRATION_WB;                                
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20512 ; [CPU_] |2358| 
	.dwpsn	file "../source/CommInterface.c",line 2359,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2359 | break;                                                                 
; 2361 | //!If the command received from processor is WBC Commercial Calibration
;     | ,                                                                      
; 2362 | //!Set the current mode to WBC Commercial Calibration which will be giv
;     | en                                                                     
; 2363 | //!input to MBD to initiate WBC Commercial Calibration                 
; 2364 | case SITARA_CALIBRATION_COMMERCIAL_WBC:                                
; 2365 | //!Re-initilaize the variables and proceed                             
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2359| 
        ; branch occurs ; [] |2359| 
$C$L182:    
	.dwpsn	file "../source/CommInterface.c",line 2366,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2366 | CIReInitialiseOnNewMeasurement();                                      
;----------------------------------------------------------------------
$C$DW$311	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$311, DW_AT_low_pc(0x00)
	.dwattr $C$DW$311, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$311, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |2366| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |2366| 
	.dwpsn	file "../source/CommInterface.c",line 2368,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2368 | g_usnCurrentMode = COMMERCIAL_CALIBRATION_WBC;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20513 ; [CPU_] |2368| 
	.dwpsn	file "../source/CommInterface.c",line 2369,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2369 | break;                                                                 
; 2371 | //!If the command received from processor is RBC Commercial Calibration
;     | ,                                                                      
; 2372 | //!Set the current mode to RBC Commercial Calibration which will be giv
;     | en                                                                     
; 2373 | //!input to MBD to initiate RBC Commercial Calibration                 
; 2374 | case SITARA_CALIBRATION_COMMERCIAL_RBC:                                
; 2375 | //!Re-initilaize the variables and proceed                             
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2369| 
        ; branch occurs ; [] |2369| 
$C$L183:    
	.dwpsn	file "../source/CommInterface.c",line 2376,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2376 | CIReInitialiseOnNewMeasurement();                                      
;----------------------------------------------------------------------
$C$DW$312	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$312, DW_AT_low_pc(0x00)
	.dwattr $C$DW$312, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$312, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |2376| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |2376| 
	.dwpsn	file "../source/CommInterface.c",line 2378,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2378 | g_usnCurrentMode = COMMERCIAL_CALIBRATION_RBC;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20514 ; [CPU_] |2378| 
	.dwpsn	file "../source/CommInterface.c",line 2379,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2379 | break;                                                                 
; 2381 | //!If the command received from processor is PLT Commercial Calibration
;     | ,                                                                      
; 2382 | //!Set the current mode to PLT Commercial Calibration which will be giv
;     | en                                                                     
; 2383 | //!input to MBD to initiate PLT Commercial Calibration                 
; 2384 |         case SITARA_CALIBRATION_COMMERCIAL_PLT:                        
; 2385 |     //!Re-initilaize the variables and proceed                         
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2379| 
        ; branch occurs ; [] |2379| 
$C$L184:    
	.dwpsn	file "../source/CommInterface.c",line 2386,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2386 | CIReInitialiseOnNewMeasurement();                                      
;----------------------------------------------------------------------
$C$DW$313	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$313, DW_AT_low_pc(0x00)
	.dwattr $C$DW$313, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$313, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |2386| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |2386| 
	.dwpsn	file "../source/CommInterface.c",line 2388,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2388 | g_usnCurrentMode = COMMERCIAL_CALIBRATION_PLT;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20515 ; [CPU_] |2388| 
	.dwpsn	file "../source/CommInterface.c",line 2389,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2389 | break;                                                                 
; 2391 | //!If the command received from processor is to request WBC pulse heigh
;     | t,                                                                     
; 2392 | //!Frame the packet for WBC pulse height and send it to processor      
; 2393 |         case SITARA_WB_WBC_PULSE_HEIGHT_CMD:                           
; 2394 |     //!Set the cell type to WBC so that the packet framed shall be for 
; 2395 |     //!WBC Pulse height                                                
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2389| 
        ; branch occurs ; [] |2389| 
$C$L185:    
	.dwpsn	file "../source/CommInterface.c",line 2396,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2396 | m_eTypeofCell = eCellWBC;                                              
; 2397 | //!Frame the packet for Calibration handling: WBC pulse height and     
; 2398 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |2396| 
	.dwpsn	file "../source/CommInterface.c",line 2399,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2399 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\                       
; 2400 |         DELFINO_WBC_PULSE_HEIGHT_DATA, FALSE);                         
; 2401 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |2399| 
        MOVB      XAR4,#0               ; [CPU_] |2399| 
        MOV       AH,#8193              ; [CPU_] |2399| 
$C$DW$314	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$314, DW_AT_low_pc(0x00)
	.dwattr $C$DW$314, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$314, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2399| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2399| 
	.dwpsn	file "../source/CommInterface.c",line 2402,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2402 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$315	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$315, DW_AT_low_pc(0x00)
	.dwattr $C$DW$315, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$315, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2402| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2402| 
	.dwpsn	file "../source/CommInterface.c",line 2403,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2403 | break;                                                                 
; 2405 | //!If the command received from processor is to request RBC pulse heigh
;     | t,                                                                     
; 2406 | //!Frame the packet for WBC pulse height and send it to processor      
; 2407 | case SITARA_WB_RBC_PULSE_HEIGHT_CMD:                                   
; 2408 | //!Set the cell type to RBC so that the packet framed shall be for     
; 2409 | //!rBC Pulse height                                                    
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2403| 
        ; branch occurs ; [] |2403| 
$C$L186:    
	.dwpsn	file "../source/CommInterface.c",line 2410,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2410 | m_eTypeofCell = eCellRBC;                                              
; 2411 | //!Frame the packet for Calibration handling: RBC pulse height and     
; 2412 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |2410| 
	.dwpsn	file "../source/CommInterface.c",line 2413,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2413 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\                       
; 2414 |         DELFINO_RBC_PULSE_HEIGHT_DATA, FALSE);                         
; 2415 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |2413| 
        MOVB      XAR4,#0               ; [CPU_] |2413| 
        MOV       AH,#8194              ; [CPU_] |2413| 
$C$DW$316	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$316, DW_AT_low_pc(0x00)
	.dwattr $C$DW$316, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$316, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2413| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2413| 
	.dwpsn	file "../source/CommInterface.c",line 2416,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2416 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$317	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$317, DW_AT_low_pc(0x00)
	.dwattr $C$DW$317, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$317, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2416| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2416| 
	.dwpsn	file "../source/CommInterface.c",line 2417,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2417 | break;                                                                 
; 2419 | //!If the command received from processor is requent for PLT Pulse Heig
;     | ht,                                                                    
; 2420 | //!perform the below operations                                        
; 2421 | case SITARA_WB_PLT_PULSE_HEIGHT_CMD:                                   
; 2422 |     //!Set the cell type to PLT                                        
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2417| 
        ; branch occurs ; [] |2417| 
$C$L187:    
	.dwpsn	file "../source/CommInterface.c",line 2423,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2423 | m_eTypeofCell = eCellPLT;                                              
; 2424 | //!Frame the PLT pulse height data                                     
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |2423| 
	.dwpsn	file "../source/CommInterface.c",line 2425,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2425 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\                       
; 2426 |         DELFINO_PLT_PULSE_HEIGHT_DATA, FALSE);                         
; 2427 | //!Clear the busy pin after completion of transmission of data         
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |2425| 
        MOVB      XAR4,#0               ; [CPU_] |2425| 
        MOV       AH,#8195              ; [CPU_] |2425| 
$C$DW$318	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$318, DW_AT_low_pc(0x00)
	.dwattr $C$DW$318, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$318, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2425| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2425| 
	.dwpsn	file "../source/CommInterface.c",line 2428,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2428 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$319	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$319, DW_AT_low_pc(0x00)
	.dwattr $C$DW$319, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$319, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2428| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2428| 
	.dwpsn	file "../source/CommInterface.c",line 2429,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2429 | break;                                                                 
; 2431 | //!If the command received from processor is to request 1st second of  
; 2432 | //!WBC Raw Data, frame the packet for WBC Raw Data and send it to proce
;     | ssor                                                                   
; 2433 | case SITARA_WBC_RAW_DATA_1SEC:                                         
; 2434 | //!Set the cell type to WBC so that the packet framed shall be for     
; 2435 | //!WBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2429| 
        ; branch occurs ; [] |2429| 
$C$L188:    
	.dwpsn	file "../source/CommInterface.c",line 2436,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2436 | m_eTypeofCell = eCellWBC;                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |2436| 
	.dwpsn	file "../source/CommInterface.c",line 2438,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2438 | sprintf(DebugPrintBuf, "\r\n SITARA_WBC_RAW_DATA_1SEC rcd\n");         
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL50        ; [CPU_U] |2438| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2438| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2438| 
$C$DW$320	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$320, DW_AT_low_pc(0x00)
	.dwattr $C$DW$320, DW_AT_name("_sprintf")
	.dwattr $C$DW$320, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2438| 
        ; call occurs [#_sprintf] ; [] |2438| 
	.dwpsn	file "../source/CommInterface.c",line 2439,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2439 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 2441 | //!Frame the packet for Patient handling: WBC Raw data and             
; 2442 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2439| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2439| 
$C$DW$321	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$321, DW_AT_low_pc(0x00)
	.dwattr $C$DW$321, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$321, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2439| 
        ; call occurs [#_UartDebugPrint] ; [] |2439| 
	.dwpsn	file "../source/CommInterface.c",line 2443,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2443 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\                       
; 2444 |         DELFINO_WBC_ADC_RAW_DATA, FALSE);                              
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |2443| 
        MOVB      XAR4,#0               ; [CPU_] |2443| 
        MOV       AH,#8198              ; [CPU_] |2443| 
$C$DW$322	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$322, DW_AT_low_pc(0x00)
	.dwattr $C$DW$322, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$322, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2443| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2443| 
	.dwpsn	file "../source/CommInterface.c",line 2446,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2446 | sprintf(DebugPrintBuf, "\r\n SITARA_WBC_RAW_DATA_1SEC in snt \n");     
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL51        ; [CPU_U] |2446| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2446| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2446| 
$C$DW$323	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$323, DW_AT_low_pc(0x00)
	.dwattr $C$DW$323, DW_AT_name("_sprintf")
	.dwattr $C$DW$323, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2446| 
        ; call occurs [#_sprintf] ; [] |2446| 
	.dwpsn	file "../source/CommInterface.c",line 2447,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2447 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 2449 |             //!Clear the busy pin, after framing and sending the packet
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2447| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2447| 
$C$DW$324	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$324, DW_AT_low_pc(0x00)
	.dwattr $C$DW$324, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$324, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2447| 
        ; call occurs [#_UartDebugPrint] ; [] |2447| 
	.dwpsn	file "../source/CommInterface.c",line 2450,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2450 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$325	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$325, DW_AT_low_pc(0x00)
	.dwattr $C$DW$325, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$325, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2450| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2450| 
	.dwpsn	file "../source/CommInterface.c",line 2451,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2451 | break;                                                                 
; 2453 | //!If the command received from processor is to request 1st second of  
; 2454 | //!RBC Raw Data, frame the packet for RBC Raw Data and send it to proce
;     | ssor                                                                   
; 2455 | case SITARA_RBC_RAW_DATA_1SEC:                                         
; 2456 | //!Set the cell type to RBC so that the packet framed shall be for     
; 2457 | //!RBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2451| 
        ; branch occurs ; [] |2451| 
$C$L189:    
	.dwpsn	file "../source/CommInterface.c",line 2458,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2458 | m_eTypeofCell = eCellRBC;                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |2458| 
	.dwpsn	file "../source/CommInterface.c",line 2460,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2460 | sprintf(DebugPrintBuf, "\r\n SITARA_RBC_RAW_DATA_1SEC in rcd\n");      
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL52        ; [CPU_U] |2460| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2460| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2460| 
$C$DW$326	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$326, DW_AT_low_pc(0x00)
	.dwattr $C$DW$326, DW_AT_name("_sprintf")
	.dwattr $C$DW$326, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2460| 
        ; call occurs [#_sprintf] ; [] |2460| 
	.dwpsn	file "../source/CommInterface.c",line 2461,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2461 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 2463 | //!Frame the packet for Patient handling: RBC Raw data and             
; 2464 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2461| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2461| 
$C$DW$327	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$327, DW_AT_low_pc(0x00)
	.dwattr $C$DW$327, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$327, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2461| 
        ; call occurs [#_UartDebugPrint] ; [] |2461| 
	.dwpsn	file "../source/CommInterface.c",line 2465,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2465 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\                       
; 2466 |         DELFINO_RBC_ADC_RAW_DATA, FALSE);                              
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |2465| 
        MOVB      XAR4,#0               ; [CPU_] |2465| 
        MOV       AH,#8199              ; [CPU_] |2465| 
$C$DW$328	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$328, DW_AT_low_pc(0x00)
	.dwattr $C$DW$328, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$328, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2465| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2465| 
	.dwpsn	file "../source/CommInterface.c",line 2468,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2468 | sprintf(DebugPrintBuf, "\r\n SITARA_RBC_RAW_DATA_1SEC in snt\n");      
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL53        ; [CPU_U] |2468| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2468| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2468| 
$C$DW$329	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$329, DW_AT_low_pc(0x00)
	.dwattr $C$DW$329, DW_AT_name("_sprintf")
	.dwattr $C$DW$329, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2468| 
        ; call occurs [#_sprintf] ; [] |2468| 
	.dwpsn	file "../source/CommInterface.c",line 2469,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2469 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 2471 |             //!Clear the busy pin, after framing and sending the packet
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2469| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2469| 
$C$DW$330	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$330, DW_AT_low_pc(0x00)
	.dwattr $C$DW$330, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$330, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2469| 
        ; call occurs [#_UartDebugPrint] ; [] |2469| 
	.dwpsn	file "../source/CommInterface.c",line 2472,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2472 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$331	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$331, DW_AT_low_pc(0x00)
	.dwattr $C$DW$331, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$331, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2472| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2472| 
	.dwpsn	file "../source/CommInterface.c",line 2473,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2473 | break;                                                                 
; 2475 | //!If the command received from processor is to request 1st second of  
; 2476 | //!PLT Raw Data, frame the packet for PLT Raw Data and send it to proce
;     | ssor                                                                   
; 2477 | case SITARA_PLT_RAW_DATA_1SEC:                                         
; 2478 | //!Set the cell type to PLT so that the packet framed shall be for     
; 2479 | //!PLT Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2473| 
        ; branch occurs ; [] |2473| 
$C$L190:    
	.dwpsn	file "../source/CommInterface.c",line 2480,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2480 | m_eTypeofCell = eCellPLT;                                              
; 2481 | //!Frame the packet for Patient handling: PLT Raw data and             
; 2482 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |2480| 
	.dwpsn	file "../source/CommInterface.c",line 2483,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2483 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\                       
; 2484 |         DELFINO_PLT_ADC_RAW_DATA, FALSE);                              
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |2483| 
        MOVB      XAR4,#0               ; [CPU_] |2483| 
        MOV       AH,#8200              ; [CPU_] |2483| 
$C$DW$332	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$332, DW_AT_low_pc(0x00)
	.dwattr $C$DW$332, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$332, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2483| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2483| 
	.dwpsn	file "../source/CommInterface.c",line 2486,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2486 | sprintf(DebugPrintBuf, "\r\n SITARA_PLT_RAW_DATA_1SEC in cmr\n");      
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL54        ; [CPU_U] |2486| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2486| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2486| 
$C$DW$333	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$333, DW_AT_low_pc(0x00)
	.dwattr $C$DW$333, DW_AT_name("_sprintf")
	.dwattr $C$DW$333, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2486| 
        ; call occurs [#_sprintf] ; [] |2486| 
	.dwpsn	file "../source/CommInterface.c",line 2487,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2487 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 2489 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2487| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2487| 
$C$DW$334	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$334, DW_AT_low_pc(0x00)
	.dwattr $C$DW$334, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$334, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2487| 
        ; call occurs [#_UartDebugPrint] ; [] |2487| 
	.dwpsn	file "../source/CommInterface.c",line 2490,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2490 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$335	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$335, DW_AT_low_pc(0x00)
	.dwattr $C$DW$335, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$335, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2490| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2490| 
	.dwpsn	file "../source/CommInterface.c",line 2491,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2491 | break;                                                                 
; 2493 | //!If the command received from processor is for dispensing the diluent
; 2494 | //!in pre-dilute mode, input the command to the MBD to dispense the dil
;     | uent                                                                   
; 2495 | case SITARA_START_DISPENSE_CMD:                                        
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2491| 
        ; branch occurs ; [] |2491| 
$C$L191:    
	.dwpsn	file "../source/CommInterface.c",line 2496,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2496 | g_usnCurrentMode = DISPENSE_THE_DILUENT;                               
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20496 ; [CPU_] |2496| 
	.dwpsn	file "../source/CommInterface.c",line 2497,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2497 | break;                                                                 
; 2499 | //!If the command received from processor is to continue the pre-diluen
;     | t                                                                      
; 2500 | //!after dispensing the diluent to the blood sample tube,              
; 2501 | //!input the command to the MBD to dispense the diluent                
; 2502 | case SITARA_START_PRE_DILUENT_COUNTING_CMD:                            
; 2503 | //!Re-initialize the variables and start proceed with pre-diluent      
; 2504 | //!mode after mixing with the blood sample                             
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2497| 
        ; branch occurs ; [] |2497| 
$C$L192:    
	.dwpsn	file "../source/CommInterface.c",line 2505,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2505 | CIReInitialiseOnNewMeasurement();                                      
; 2506 | //!Set the secondary mode (Temp mode) to Pre-Diluent Mode              
; 2507 | //!this mode is the secondary input to Pre-Diluent mode whcih is       
; 2508 | //!continued after the initial pre-diluent mode                        
;----------------------------------------------------------------------
$C$DW$336	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$336, DW_AT_low_pc(0x00)
	.dwattr $C$DW$336, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$336, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |2505| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |2505| 
	.dwpsn	file "../source/CommInterface.c",line 2509,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2509 | g_usnCurrentMode = START_PRE_DILUENT_COUNTING_CMD; //CRH_MBD           
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20497 ; [CPU_] |2509| 
	.dwpsn	file "../source/CommInterface.c",line 2510,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2510 | break;                                                                 
; 2512 | //!If the command received from processor is to request next second of 
; 2513 | //!WBC Raw Data, frame the packet for WBC Raw Data and send it to proce
;     | ssor                                                                   
; 2514 | case SITARA_WBC_RAW_DATA_NEXT_SEC:                                     
; 2515 | //!Set the cell type to WBC so that the packet framed shall be for     
; 2516 | //!WBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2510| 
        ; branch occurs ; [] |2510| 
$C$L193:    
	.dwpsn	file "../source/CommInterface.c",line 2517,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2517 | m_eTypeofCell = eCellWBC;                                              
; 2518 | //!Raw data is acquired for total acquisition time of WBC, RBC & PLT   
; 2519 | //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
; 2520 | //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be 
; 2521 | //!transmitted to processor. It is transmitted in 1 second each as requ
;     | ested                                                                  
; 2522 | //!by processor.                                                       
; 2523 | //!So next second data is packetized by enabling the variable          
; 2524 | //!g_bNextSecRawDataReq                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |2517| 
	.dwpsn	file "../source/CommInterface.c",line 2525,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2525 | g_bNextSecRawDataReq = TRUE;                                           
; 2526 | //!Frame the packet for Patient handling: WBC Raw data and             
; 2527 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      @_g_bNextSecRawDataReq,#1,UNC ; [CPU_] |2525| 
	.dwpsn	file "../source/CommInterface.c",line 2528,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2528 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\                       
; 2529 |         DELFINO_WBC_ADC_RAW_DATA, FALSE);                              
; 2530 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |2528| 
        MOVB      XAR4,#0               ; [CPU_] |2528| 
        MOV       AH,#8198              ; [CPU_] |2528| 
$C$DW$337	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$337, DW_AT_low_pc(0x00)
	.dwattr $C$DW$337, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$337, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2528| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2528| 
	.dwpsn	file "../source/CommInterface.c",line 2531,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2531 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$338	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$338, DW_AT_low_pc(0x00)
	.dwattr $C$DW$338, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$338, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2531| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2531| 
	.dwpsn	file "../source/CommInterface.c",line 2532,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2532 | break;                                                                 
; 2534 | //!If the command received from processor is to request next second of 
; 2535 | //!RBC Raw Data, frame the packet for RBC Raw Data and send it to proce
;     | ssor                                                                   
; 2536 | case SITARA_RBC_RAW_DATA_NEXT_SEC:                                     
; 2537 | //!Set the cell type to RBC so that the packet framed shall be for     
; 2538 | //!RBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2532| 
        ; branch occurs ; [] |2532| 
$C$L194:    
	.dwpsn	file "../source/CommInterface.c",line 2539,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2539 | m_eTypeofCell = eCellRBC;                                              
; 2540 | //!Raw data is acquired for total acquisition time of WBC, RBC & PLT   
; 2541 | //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
; 2542 | //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be 
; 2543 | //!transmitted to processor. It is transmitted in 1 second each as requ
;     | ested                                                                  
; 2544 | //!by processor.                                                       
; 2545 | //!So next second data is packetized by enabling the variable          
; 2546 | //!g_bNextSecRawDataReq                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |2539| 
	.dwpsn	file "../source/CommInterface.c",line 2547,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2547 | g_bNextSecRawDataReq = TRUE;                                           
; 2548 | //!Frame the packet for Patient handling: RBC Raw data and             
; 2549 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      @_g_bNextSecRawDataReq,#1,UNC ; [CPU_] |2547| 
	.dwpsn	file "../source/CommInterface.c",line 2550,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2550 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\                       
; 2551 |         DELFINO_RBC_ADC_RAW_DATA, FALSE);                              
; 2552 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |2550| 
        MOVB      XAR4,#0               ; [CPU_] |2550| 
        MOV       AH,#8199              ; [CPU_] |2550| 
$C$DW$339	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$339, DW_AT_low_pc(0x00)
	.dwattr $C$DW$339, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$339, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2550| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2550| 
	.dwpsn	file "../source/CommInterface.c",line 2553,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2553 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$340	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$340, DW_AT_low_pc(0x00)
	.dwattr $C$DW$340, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$340, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2553| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2553| 
	.dwpsn	file "../source/CommInterface.c",line 2554,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2554 | break;                                                                 
; 2556 | //!If the command received from processor is to request next second of 
; 2557 | //!PLT Raw Data, frame the packet for PLT Raw Data and send it to proce
;     | ssor                                                                   
; 2558 | case SITARA_PLT_RAW_DATA_NEXT_SEC:                                     
; 2559 | //!Set the cell type to PLT so that the packet framed shall be for     
; 2560 | //!PLT Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2554| 
        ; branch occurs ; [] |2554| 
$C$L195:    
	.dwpsn	file "../source/CommInterface.c",line 2561,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2561 | m_eTypeofCell = eCellPLT;                                              
; 2562 | //!Raw data is acquired for total acquisition time of WBC, RBC & PLT   
; 2563 | //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
; 2564 | //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be 
; 2565 | //!transmitted to processor. It is transmitted in 1 second each as requ
;     | ested                                                                  
; 2566 | //!by processor.                                                       
; 2567 | //!So next second data is packetized by enabling the variable          
; 2568 | //!g_bNextSecRawDataReq                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |2561| 
	.dwpsn	file "../source/CommInterface.c",line 2569,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2569 | g_bNextSecRawDataReq = TRUE;                                           
; 2570 | //!Frame the packet for Patient handling: PLT Raw data and             
; 2571 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      @_g_bNextSecRawDataReq,#1,UNC ; [CPU_] |2569| 
	.dwpsn	file "../source/CommInterface.c",line 2572,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2572 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING,\                       
; 2573 |         DELFINO_PLT_ADC_RAW_DATA, FALSE);                              
; 2574 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |2572| 
        MOVB      XAR4,#0               ; [CPU_] |2572| 
        MOV       AH,#8200              ; [CPU_] |2572| 
$C$DW$341	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$341, DW_AT_low_pc(0x00)
	.dwattr $C$DW$341, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$341, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2572| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2572| 
	.dwpsn	file "../source/CommInterface.c",line 2575,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2575 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$342	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$342, DW_AT_low_pc(0x00)
	.dwattr $C$DW$342, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$342, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2575| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2575| 
	.dwpsn	file "../source/CommInterface.c",line 2576,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2576 | break;                                                                 
; 2578 | //!If the command received from processor is to enable the start button
; 2579 | //!polling, enable the start button polling                            
; 2580 | case SITARA_START_POLLING_START_BUTTON_CMD:                            
; 2581 | //!Set the cell type to invalid when initiating any seqence or         
; 2582 | //!during the polling of start button                                  
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2576| 
        ; branch occurs ; [] |2576| 
$C$L196:    
	.dwpsn	file "../source/CommInterface.c",line 2583,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2583 | m_eTypeofCell = ecellInvalid;                                          
; 2584 | //!To enable start button for next measurement                         
; 2585 | //g_bStartButtonStatus = TRUE;                                         
; 2586 | //!Clear the busy pin, so that process shall send any command for      
; 2587 | //!further sequence initiation                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOV       @_m_eTypeofCell,#0    ; [CPU_] |2583| 
	.dwpsn	file "../source/CommInterface.c",line 2588,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2588 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$343	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$343, DW_AT_low_pc(0x00)
	.dwattr $C$DW$343, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$343, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2588| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2588| 
	.dwpsn	file "../source/CommInterface.c",line 2589,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2589 | break;                                                                 
; 2591 | //!If the command received from processor is to disable the start butto
;     | n                                                                      
; 2592 | //!polling, disable the start button polling                           
; 2593 | case SITARA_STOP_POLLING_START_BUTTON_CMD:                             
; 2594 |     //!Set the cell type to invalid when initiating any seqence or     
; 2595 |     //!during the polling of start button                              
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2589| 
        ; branch occurs ; [] |2589| 
$C$L197:    
	.dwpsn	file "../source/CommInterface.c",line 2596,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2596 | m_eTypeofCell = ecellInvalid;                                          
; 2597 | //!To disable start button for next measurement                        
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOV       @_m_eTypeofCell,#0    ; [CPU_] |2596| 
	.dwpsn	file "../source/CommInterface.c",line 2598,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2598 | g_bStartButtonStatus = FALSE;                                          
; 2599 | //!Clear the busy pin, so that process shall send any command for      
; 2600 | //!further sequence initiation                                         
;----------------------------------------------------------------------
        MOV       @_g_bStartButtonStatus,#0 ; [CPU_] |2598| 
	.dwpsn	file "../source/CommInterface.c",line 2601,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2601 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$344	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$344, DW_AT_low_pc(0x00)
	.dwattr $C$DW$344, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$344, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2601| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2601| 
	.dwpsn	file "../source/CommInterface.c",line 2602,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2602 | break;                                                                 
; 2604 | //!If the command received from processor is to disable the start butto
;     | n                                                                      
; 2605 | //!polling, disable the start button polling                           
; 2606 | case SITARA_PULSE_HEIGHT_DATA_RECEIVED:                                
; 2607 |     //!Set the cycle completion state to error check                   
; 2608 |     //m_eCycleCompletion = eERROR_CHECK;                               
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2602| 
        ; branch occurs ; [] |2602| 
$C$L198:    
	.dwpsn	file "../source/CommInterface.c",line 2609,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2609 | g_bDataTransmitted = TRUE;                                             
; 2610 | //!Set the busy line high                                              
;----------------------------------------------------------------------
        MOVW      DP,#_g_bDataTransmitted ; [CPU_U] 
        MOVB      @_g_bDataTransmitted,#1,UNC ; [CPU_] |2609| 
	.dwpsn	file "../source/CommInterface.c",line 2611,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2611 | SPISetCommCtrlBusyInt();                                               
;----------------------------------------------------------------------
$C$DW$345	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$345, DW_AT_low_pc(0x00)
	.dwattr $C$DW$345, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$345, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |2611| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |2611| 
	.dwpsn	file "../source/CommInterface.c",line 2612,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2612 | sprintf(DebugPrintBuf, "\n SITARA_PULSE_HEIGHT_DATA_received 2\n");    
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL55        ; [CPU_U] |2612| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2612| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2612| 
$C$DW$346	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$346, DW_AT_low_pc(0x00)
	.dwattr $C$DW$346, DW_AT_name("_sprintf")
	.dwattr $C$DW$346, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2612| 
        ; call occurs [#_sprintf] ; [] |2612| 
	.dwpsn	file "../source/CommInterface.c",line 2613,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2613 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2613| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2613| 
$C$DW$347	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$347, DW_AT_low_pc(0x00)
	.dwattr $C$DW$347, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$347, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2613| 
        ; call occurs [#_UartDebugPrint] ; [] |2613| 
	.dwpsn	file "../source/CommInterface.c",line 2614,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2614 | break;                                                                 
; 2616 | default:                                                               
; 2617 |     //!Nothing in case of default scenario                             
; 2618 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L202,UNC           ; [CPU_] |2614| 
        ; branch occurs ; [] |2614| 
$C$L199:    
	.dwpsn	file "../source/CommInterface.c",line 2349,column 2,is_stmt,isa 0
        MOVZ      AR6,*-SP[3]           ; [CPU_] |2349| 
        CMP       AR6,#4354             ; [CPU_] |2349| 
        B         $C$L200,GT            ; [CPU_] |2349| 
        ; branchcc occurs ; [] |2349| 
        CMP       AR6,#4354             ; [CPU_] |2349| 
        B         $C$L182,EQ            ; [CPU_] |2349| 
        ; branchcc occurs ; [] |2349| 
        SUB       AL,#4102              ; [CPU_] |2349| 
        CMPB      AL,#13                ; [CPU_] |2349| 
        B         $C$L201,LOS           ; [CPU_] |2349| 
        ; branchcc occurs ; [] |2349| 
        MOVZ      AR6,AR6               ; [CPU_] |2349| 
        MOVL      XAR4,#4353            ; [CPU_U] |2349| 
        MOVL      ACC,XAR4              ; [CPU_] |2349| 
        CMPL      ACC,XAR6              ; [CPU_] |2349| 
        B         $C$L181,EQ            ; [CPU_] |2349| 
        ; branchcc occurs ; [] |2349| 
        B         $C$L202,UNC           ; [CPU_] |2349| 
        ; branch occurs ; [] |2349| 
$C$L200:    
        MOVZ      AR7,AR6               ; [CPU_] |2349| 
        MOVL      XAR4,#4355            ; [CPU_U] |2349| 
        MOVL      ACC,XAR4              ; [CPU_] |2349| 
        CMPL      ACC,XAR7              ; [CPU_] |2349| 
        B         $C$L183,EQ            ; [CPU_] |2349| 
        ; branchcc occurs ; [] |2349| 
        MOVZ      AR6,AR6               ; [CPU_] |2349| 
        MOVL      XAR4,#4356            ; [CPU_U] |2349| 
        MOVL      ACC,XAR4              ; [CPU_] |2349| 
        CMPL      ACC,XAR6              ; [CPU_] |2349| 
        B         $C$L184,EQ            ; [CPU_] |2349| 
        ; branchcc occurs ; [] |2349| 
        B         $C$L202,UNC           ; [CPU_] |2349| 
        ; branch occurs ; [] |2349| 
$C$L201:    
        SUB       AR6,#4102             ; [CPU_] |2349| 
        MOV       ACC,AR6 << #1         ; [CPU_] |2349| 
        MOVZ      AR6,AL                ; [CPU_] |2349| 
        MOVL      XAR7,#$C$SW7          ; [CPU_U] |2349| 
        MOVL      ACC,XAR7              ; [CPU_] |2349| 
        ADDU      ACC,AR6               ; [CPU_] |2349| 
        MOVL      XAR7,ACC              ; [CPU_] |2349| 
        MOVL      XAR7,*XAR7            ; [CPU_] |2349| 
        LB        *XAR7                 ; [CPU_] |2349| 
        ; branch occurs ; [] |2349| 
	.sect	".switch:_CIProcessCalibrationHandlingCmd"
	.clink
$C$SW7:	.long	$C$L185	; 4102
	.long	$C$L186	; 4103
	.long	$C$L187	; 4104
	.long	$C$L188	; 4105
	.long	$C$L191	; 4106
	.long	$C$L192	; 4107
	.long	$C$L193	; 4108
	.long	$C$L194	; 4109
	.long	$C$L195	; 4110
	.long	$C$L196	; 4111
	.long	$C$L189	; 4112
	.long	$C$L190	; 4113
	.long	$C$L197	; 4114
	.long	$C$L198	; 4115
	.sect	".text:_CIProcessCalibrationHandlingCmd"
$C$L202:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$348	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$348, DW_AT_low_pc(0x00)
	.dwattr $C$DW$348, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$307, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$307, DW_AT_TI_end_line(0xa3c)
	.dwattr $C$DW$307, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$307

	.sect	".text:_CIFormCalibrationHandlingPacket"
	.clink
	.global	_CIFormCalibrationHandlingPacket

$C$DW$349	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$349, DW_AT_name("CIFormCalibrationHandlingPacket")
	.dwattr $C$DW$349, DW_AT_low_pc(_CIFormCalibrationHandlingPacket)
	.dwattr $C$DW$349, DW_AT_high_pc(0x00)
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_CIFormCalibrationHandlingPacket")
	.dwattr $C$DW$349, DW_AT_external
	.dwattr $C$DW$349, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$349, DW_AT_TI_begin_line(0xa45)
	.dwattr $C$DW$349, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$349, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 2630,column 1,is_stmt,address _CIFormCalibrationHandlingPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormCalibrationHandlingPacket
$C$DW$350	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$350, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$350, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2629 | void CIFormCalibrationHandlingPacket(uint16_t usnSubCmd)               
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormCalibrationHandlingPacket FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIFormCalibrationHandlingPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$351	.dwtag  DW_TAG_variable
	.dwattr $C$DW$351, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$351, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 2631 | //!Frame packet for calibration handling                               
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |2630| 
	.dwpsn	file "../source/CommInterface.c",line 2632,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2632 | CPFrameDataPacket(DELFINO_CALIBRATION_HANDLING, usnSubCmd, FALSE);     
;----------------------------------------------------------------------
        MOVB      XAR4,#0               ; [CPU_] |2632| 
        MOV       AL,#8194              ; [CPU_] |2632| 
        MOV       AH,*-SP[1]            ; [CPU_] |2632| 
$C$DW$352	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$352, DW_AT_low_pc(0x00)
	.dwattr $C$DW$352, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$352, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2632| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2632| 
	.dwpsn	file "../source/CommInterface.c",line 2633,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$353	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$353, DW_AT_low_pc(0x00)
	.dwattr $C$DW$353, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$349, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$349, DW_AT_TI_end_line(0xa49)
	.dwattr $C$DW$349, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$349

	.sect	".text:_CIProcessQualityHandlingCmd"
	.clink
	.global	_CIProcessQualityHandlingCmd

$C$DW$354	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$354, DW_AT_name("CIProcessQualityHandlingCmd")
	.dwattr $C$DW$354, DW_AT_low_pc(_CIProcessQualityHandlingCmd)
	.dwattr $C$DW$354, DW_AT_high_pc(0x00)
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_CIProcessQualityHandlingCmd")
	.dwattr $C$DW$354, DW_AT_external
	.dwattr $C$DW$354, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$354, DW_AT_TI_begin_line(0xa52)
	.dwattr $C$DW$354, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$354, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommInterface.c",line 2643,column 1,is_stmt,address _CIProcessQualityHandlingCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessQualityHandlingCmd
$C$DW$355	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$355, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$355, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2642 | void CIProcessQualityHandlingCmd(uint16_t usnMinorCmd)                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessQualityHandlingCmd  FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessQualityHandlingCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$356	.dwtag  DW_TAG_variable
	.dwattr $C$DW$356, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$356, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 2644 | //!Check the minor command received in calibration mode and set the seq
;     | uence                                                                  
; 2645 | //!accordingly                                                         
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |2643| 
	.dwpsn	file "../source/CommInterface.c",line 2646,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2646 | switch(usnMinorCmd)                                                    
; 2648 | //!If the command received from processor is Whole blood QC,           
; 2649 | //!Set the current mode to Whole blood QC which will be given          
; 2650 | //!input to MBD to initiate Whole blood QC                             
; 2651 |         case SITARA_WHOLE_BLOOD_CONTROL:                               
; 2653 |     //!Re-initilaize the variables and proceed                         
;----------------------------------------------------------------------
        B         $C$L217,UNC           ; [CPU_] |2646| 
        ; branch occurs ; [] |2646| 
$C$L203:    
	.dwpsn	file "../source/CommInterface.c",line 2654,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2654 | CIReInitialiseOnNewMeasurement();                                      
;----------------------------------------------------------------------
$C$DW$357	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$357, DW_AT_low_pc(0x00)
	.dwattr $C$DW$357, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$357, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |2654| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |2654| 
	.dwpsn	file "../source/CommInterface.c",line 2656,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2656 | g_usnCurrentMode = QUALITY_CONTROL_WB;                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20518 ; [CPU_] |2656| 
	.dwpsn	file "../source/CommInterface.c",line 2658,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2658 | break;                                                                 
; 2660 | //!If the command received from processor is Body Fluid QC,            
; 2661 | //!Set the current mode to Body Fluid QC which will be given           
; 2662 | //!input to MBD to initiate Body Fluid QC                              
; 2663 | case SITARA_BODY_FLUID_CONTROL://case QUALITY_CONTROL_BODY_FLUID:SKM_CH
;     | ANGE_QC                                                                
; 2665 | //!Re-initilaize the variables and proceed                             
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2658| 
        ; branch occurs ; [] |2658| 
$C$L204:    
	.dwpsn	file "../source/CommInterface.c",line 2666,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2666 | CIReInitialiseOnNewMeasurement();                                      
;----------------------------------------------------------------------
$C$DW$358	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$358, DW_AT_low_pc(0x00)
	.dwattr $C$DW$358, DW_AT_name("_CIReInitialiseOnNewMeasurement")
	.dwattr $C$DW$358, DW_AT_TI_call

        LCR       #_CIReInitialiseOnNewMeasurement ; [CPU_] |2666| 
        ; call occurs [#_CIReInitialiseOnNewMeasurement] ; [] |2666| 
	.dwpsn	file "../source/CommInterface.c",line 2668,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2668 | g_usnCurrentMode = QUALITY_CONTROL_BODY_FLUID;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20519 ; [CPU_] |2668| 
	.dwpsn	file "../source/CommInterface.c",line 2670,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2670 | break;                                                                 
; 2672 | //!If the command received from processor is to request WBC pulse heigh
;     | t,                                                                     
; 2673 | //!Frame the packet for WBC pulse height and send it to processor      
; 2674 | case SITARA_WB_WBC_PULSE_HEIGHT_CMD:                                   
; 2675 | //!Set the cell type to WBC so that the packet framed shall be for     
; 2676 | //!WBC Pulse height                                                    
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2670| 
        ; branch occurs ; [] |2670| 
$C$L205:    
	.dwpsn	file "../source/CommInterface.c",line 2677,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2677 | m_eTypeofCell = eCellWBC;                                              
; 2678 | //!Frame the packet for Patient handling: WBC pulse height and         
; 2679 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |2677| 
	.dwpsn	file "../source/CommInterface.c",line 2680,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2680 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\                           
; 2681 |         DELFINO_WBC_PULSE_HEIGHT_DATA, FALSE);                         
; 2682 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8197              ; [CPU_] |2680| 
        MOVB      XAR4,#0               ; [CPU_] |2680| 
        MOV       AH,#8193              ; [CPU_] |2680| 
$C$DW$359	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$359, DW_AT_low_pc(0x00)
	.dwattr $C$DW$359, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$359, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2680| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2680| 
	.dwpsn	file "../source/CommInterface.c",line 2683,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2683 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$360	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$360, DW_AT_low_pc(0x00)
	.dwattr $C$DW$360, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$360, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2683| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2683| 
	.dwpsn	file "../source/CommInterface.c",line 2684,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2684 | break;                                                                 
; 2686 | //!If the command received from processor is to request RBC pulse heigh
;     | t,                                                                     
; 2687 | //!Frame the packet for RBC pulse height and send it to processor      
; 2688 | case SITARA_WB_RBC_PULSE_HEIGHT_CMD:                                   
; 2689 | //!Set the cell type to RBC so that the packet framed shall be for     
; 2690 | //!RBC Pulse height                                                    
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2684| 
        ; branch occurs ; [] |2684| 
$C$L206:    
	.dwpsn	file "../source/CommInterface.c",line 2691,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2691 | m_eTypeofCell = eCellRBC;                                              
; 2692 | //!Frame the packet for Patient handling: RBC pulse height and         
; 2693 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |2691| 
	.dwpsn	file "../source/CommInterface.c",line 2694,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2694 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\                           
; 2695 |         DELFINO_RBC_PULSE_HEIGHT_DATA, FALSE);                         
; 2696 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8197              ; [CPU_] |2694| 
        MOVB      XAR4,#0               ; [CPU_] |2694| 
        MOV       AH,#8194              ; [CPU_] |2694| 
$C$DW$361	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$361, DW_AT_low_pc(0x00)
	.dwattr $C$DW$361, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$361, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2694| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2694| 
	.dwpsn	file "../source/CommInterface.c",line 2697,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2697 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$362	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$362, DW_AT_low_pc(0x00)
	.dwattr $C$DW$362, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$362, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2697| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2697| 
	.dwpsn	file "../source/CommInterface.c",line 2698,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2698 | break;                                                                 
; 2700 | //!If the command received from processor is to request PLT pulse heigh
;     | t,                                                                     
; 2701 | //!Frame the packet for PLT pulse height and send it to processor      
; 2702 | case SITARA_WB_PLT_PULSE_HEIGHT_CMD:                                   
; 2703 | //!Set the cell type to PLT so that the packet framed shall be for     
; 2704 | //!PLT Pulse height                                                    
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2698| 
        ; branch occurs ; [] |2698| 
$C$L207:    
	.dwpsn	file "../source/CommInterface.c",line 2705,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2705 | m_eTypeofCell = eCellPLT;                                              
; 2706 | //!Frame the packet for Patient handling: PLT pulse height and         
; 2707 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |2705| 
	.dwpsn	file "../source/CommInterface.c",line 2708,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2708 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\                           
; 2709 |         DELFINO_PLT_PULSE_HEIGHT_DATA, FALSE);                         
; 2710 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8197              ; [CPU_] |2708| 
        MOVB      XAR4,#0               ; [CPU_] |2708| 
        MOV       AH,#8195              ; [CPU_] |2708| 
$C$DW$363	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$363, DW_AT_low_pc(0x00)
	.dwattr $C$DW$363, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$363, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2708| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2708| 
	.dwpsn	file "../source/CommInterface.c",line 2711,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2711 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$364	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$364, DW_AT_low_pc(0x00)
	.dwattr $C$DW$364, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$364, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2711| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2711| 
	.dwpsn	file "../source/CommInterface.c",line 2712,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2712 | break;                                                                 
; 2714 | //!If the command received from processor is to request 1st second of  
; 2715 | //!WBC Raw Data, frame the packet for WBC Raw Data and send it to proce
;     | ssor                                                                   
; 2716 | case SITARA_WBC_RAW_DATA_1SEC:                                         
; 2717 | //!Set the cell type to WBC so that the packet framed shall be for     
; 2718 | //!WBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2712| 
        ; branch occurs ; [] |2712| 
$C$L208:    
	.dwpsn	file "../source/CommInterface.c",line 2719,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2719 | m_eTypeofCell = eCellWBC;                                              
; 2720 | //!Frame the packet for Patient handling: WBC Raw data and             
; 2721 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |2719| 
	.dwpsn	file "../source/CommInterface.c",line 2722,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2722 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\                           
; 2723 |         DELFINO_WBC_ADC_RAW_DATA, FALSE);                              
; 2724 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8197              ; [CPU_] |2722| 
        MOVB      XAR4,#0               ; [CPU_] |2722| 
        MOV       AH,#8198              ; [CPU_] |2722| 
$C$DW$365	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$365, DW_AT_low_pc(0x00)
	.dwattr $C$DW$365, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$365, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2722| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2722| 
	.dwpsn	file "../source/CommInterface.c",line 2725,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2725 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$366	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$366, DW_AT_low_pc(0x00)
	.dwattr $C$DW$366, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$366, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2725| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2725| 
	.dwpsn	file "../source/CommInterface.c",line 2726,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2726 | break;                                                                 
; 2728 | //!If the command received from processor is to request 1st second of  
; 2729 | //!RBC Raw Data, frame the packet for RBC Raw Data and send it to proce
;     | ssor                                                                   
; 2730 | case SITARA_RBC_RAW_DATA_1SEC:                                         
; 2731 | //!Set the cell type to RBC so that the packet framed shall be for     
; 2732 | //!RBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2726| 
        ; branch occurs ; [] |2726| 
$C$L209:    
	.dwpsn	file "../source/CommInterface.c",line 2733,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2733 | m_eTypeofCell = eCellRBC;                                              
; 2734 | //!Frame the packet for Patient handling: RBC Raw data and             
; 2735 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |2733| 
	.dwpsn	file "../source/CommInterface.c",line 2736,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2736 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\                           
; 2737 |         DELFINO_RBC_ADC_RAW_DATA, FALSE);                              
; 2738 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8197              ; [CPU_] |2736| 
        MOVB      XAR4,#0               ; [CPU_] |2736| 
        MOV       AH,#8199              ; [CPU_] |2736| 
$C$DW$367	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$367, DW_AT_low_pc(0x00)
	.dwattr $C$DW$367, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$367, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2736| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2736| 
	.dwpsn	file "../source/CommInterface.c",line 2739,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2739 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$368	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$368, DW_AT_low_pc(0x00)
	.dwattr $C$DW$368, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$368, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2739| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2739| 
	.dwpsn	file "../source/CommInterface.c",line 2740,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2740 | break;                                                                 
; 2742 | //!If the command received from processor is to request 1st second of  
; 2743 | //!PLT Raw Data, frame the packet for PLT Raw Data and send it to proce
;     | ssor                                                                   
; 2744 | case SITARA_PLT_RAW_DATA_1SEC:                                         
; 2745 | //!Set the cell type to PLT so that the packet framed shall be for     
; 2746 | //!PLT Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2740| 
        ; branch occurs ; [] |2740| 
$C$L210:    
	.dwpsn	file "../source/CommInterface.c",line 2747,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2747 | m_eTypeofCell = eCellPLT;                                              
; 2748 | //!Frame the packet for Patient handling: PLT Raw data and             
; 2749 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |2747| 
	.dwpsn	file "../source/CommInterface.c",line 2750,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2750 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\                           
; 2751 |         DELFINO_PLT_ADC_RAW_DATA, FALSE);                              
; 2752 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8197              ; [CPU_] |2750| 
        MOVB      XAR4,#0               ; [CPU_] |2750| 
        MOV       AH,#8200              ; [CPU_] |2750| 
$C$DW$369	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$369, DW_AT_low_pc(0x00)
	.dwattr $C$DW$369, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$369, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2750| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2750| 
	.dwpsn	file "../source/CommInterface.c",line 2753,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2753 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$370	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$370, DW_AT_low_pc(0x00)
	.dwattr $C$DW$370, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$370, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2753| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2753| 
	.dwpsn	file "../source/CommInterface.c",line 2754,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2754 | break;                                                                 
; 2756 | //!If the command received from processor is to request next second of 
; 2757 | //!WBC Raw Data, frame the packet for WBC Raw Data and send it to proce
;     | ssor                                                                   
; 2758 | case SITARA_WBC_RAW_DATA_NEXT_SEC:                                     
; 2759 | //!Set the cell type to WBC so that the packet framed shall be for     
; 2760 | //!WBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2754| 
        ; branch occurs ; [] |2754| 
$C$L211:    
	.dwpsn	file "../source/CommInterface.c",line 2761,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2761 | m_eTypeofCell = eCellWBC;                                              
; 2762 | //!Raw data is acquired for total acquisition time of WBC, RBC & PLT   
; 2763 | //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
; 2764 | //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be 
; 2765 | //!transmitted to processor. It is transmitted in 1 second each as requ
;     | ested                                                                  
; 2766 | //!by processor.                                                       
; 2767 | //!So next second data is packetized by enabling the variable          
; 2768 | //!g_bNextSecRawDataReq                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#3,UNC ; [CPU_] |2761| 
	.dwpsn	file "../source/CommInterface.c",line 2769,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2769 | g_bNextSecRawDataReq = TRUE;                                           
; 2770 | //!Frame the packet for Patient handling: WBC Raw data and             
; 2771 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      @_g_bNextSecRawDataReq,#1,UNC ; [CPU_] |2769| 
	.dwpsn	file "../source/CommInterface.c",line 2772,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2772 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\                           
; 2773 |         DELFINO_WBC_ADC_RAW_DATA, FALSE);                              
; 2774 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8197              ; [CPU_] |2772| 
        MOVB      XAR4,#0               ; [CPU_] |2772| 
        MOV       AH,#8198              ; [CPU_] |2772| 
$C$DW$371	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$371, DW_AT_low_pc(0x00)
	.dwattr $C$DW$371, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$371, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2772| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2772| 
	.dwpsn	file "../source/CommInterface.c",line 2775,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2775 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$372	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$372, DW_AT_low_pc(0x00)
	.dwattr $C$DW$372, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$372, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2775| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2775| 
	.dwpsn	file "../source/CommInterface.c",line 2776,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2776 | break;                                                                 
; 2778 | //!If the command received from processor is to request next second of 
; 2779 | //!RBC Raw Data, frame the packet for RBC Raw Data and send it to proce
;     | ssor                                                                   
; 2780 | case SITARA_RBC_RAW_DATA_NEXT_SEC:                                     
; 2781 | //!Set the cell type to RBC so that the packet framed shall be for     
; 2782 | //!RBC Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2776| 
        ; branch occurs ; [] |2776| 
$C$L212:    
	.dwpsn	file "../source/CommInterface.c",line 2783,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2783 | m_eTypeofCell = eCellRBC;                                              
; 2784 | //!Raw data is acquired for total acquisition time of WBC, RBC & PLT   
; 2785 | //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
; 2786 | //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be 
; 2787 | //!transmitted to processor. It is transmitted in 1 second each as requ
;     | ested                                                                  
; 2788 | //!by processor.                                                       
; 2789 | //!So next second data is packetized by enabling the variable          
; 2790 | //!g_bNextSecRawDataReq                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#1,UNC ; [CPU_] |2783| 
	.dwpsn	file "../source/CommInterface.c",line 2791,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2791 | g_bNextSecRawDataReq = TRUE;                                           
; 2792 | //!Frame the packet for Patient handling: RBC Raw data and             
; 2793 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      @_g_bNextSecRawDataReq,#1,UNC ; [CPU_] |2791| 
	.dwpsn	file "../source/CommInterface.c",line 2794,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2794 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\                           
; 2795 |         DELFINO_RBC_ADC_RAW_DATA, FALSE);                              
; 2796 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8197              ; [CPU_] |2794| 
        MOVB      XAR4,#0               ; [CPU_] |2794| 
        MOV       AH,#8199              ; [CPU_] |2794| 
$C$DW$373	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$373, DW_AT_low_pc(0x00)
	.dwattr $C$DW$373, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$373, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2794| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2794| 
	.dwpsn	file "../source/CommInterface.c",line 2797,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2797 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$374	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$374, DW_AT_low_pc(0x00)
	.dwattr $C$DW$374, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$374, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2797| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2797| 
	.dwpsn	file "../source/CommInterface.c",line 2798,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2798 | break;                                                                 
; 2800 | //!If the command received from processor is to request next second of 
; 2801 | //!PLT Raw Data, frame the packet for PLT Raw Data and send it to proce
;     | ssor                                                                   
; 2802 | case SITARA_PLT_RAW_DATA_NEXT_SEC:                                     
; 2803 | //!Set the cell type to PLT so that the packet framed shall be for     
; 2804 | //!PLT Raw Data                                                        
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2798| 
        ; branch occurs ; [] |2798| 
$C$L213:    
	.dwpsn	file "../source/CommInterface.c",line 2805,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2805 | m_eTypeofCell = eCellPLT;                                              
; 2806 | //!Raw data is acquired for total acquisition time of WBC, RBC & PLT   
; 2807 | //!The acquisition time for WBC is approx 7.5 Seconds and for RBC & PLT
; 2808 | //!is 9 Sec. Raw data of 7.5 for WBC, 9 Sec of RBC & 9Sec of PLT to be 
; 2809 | //!transmitted to processor. It is transmitted in 1 second each as requ
;     | ested                                                                  
; 2810 | //!by processor.                                                       
; 2811 | //!So next second data is packetized by enabling the variable          
; 2812 | //!g_bNextSecRawDataReq                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOVB      @_m_eTypeofCell,#2,UNC ; [CPU_] |2805| 
	.dwpsn	file "../source/CommInterface.c",line 2813,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2813 | g_bNextSecRawDataReq = TRUE;                                           
; 2814 | //!Frame the packet for Patient handling: PLT Raw data and             
; 2815 | //!Send to the processor                                               
;----------------------------------------------------------------------
        MOVB      @_g_bNextSecRawDataReq,#1,UNC ; [CPU_] |2813| 
	.dwpsn	file "../source/CommInterface.c",line 2816,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2816 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING,\                           
; 2817 |         DELFINO_PLT_ADC_RAW_DATA, FALSE);                              
; 2818 | //!Clear the busy pin, after framing and sending the packet            
;----------------------------------------------------------------------
        MOV       AL,#8197              ; [CPU_] |2816| 
        MOVB      XAR4,#0               ; [CPU_] |2816| 
        MOV       AH,#8200              ; [CPU_] |2816| 
$C$DW$375	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$375, DW_AT_low_pc(0x00)
	.dwattr $C$DW$375, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$375, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2816| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2816| 
	.dwpsn	file "../source/CommInterface.c",line 2819,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 2819 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$376	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$376, DW_AT_low_pc(0x00)
	.dwattr $C$DW$376, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$376, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2819| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2819| 
	.dwpsn	file "../source/CommInterface.c",line 2820,column 3,is_stmt,isa 0
;----------------------------------------------------------------------
; 2820 | break;                                                                 
; 2822 | //!If the command received from processor is to enable the start button
; 2823 | //!polling, enable the start button polling                            
; 2824 | case SITARA_START_POLLING_START_BUTTON_CMD:                            
; 2825 | //!Set the cell type to invalid when initiating any seqence or         
; 2826 | //!during the polling of start button                                  
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2820| 
        ; branch occurs ; [] |2820| 
$C$L214:    
	.dwpsn	file "../source/CommInterface.c",line 2827,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2827 | m_eTypeofCell = ecellInvalid;                                          
; 2828 | //!To enable start button for next measurement                         
; 2829 | //g_bStartButtonStatus = TRUE;                                         
; 2830 | //!Clear the busy pin, so that process shall send any command for      
; 2831 | //!further sequence initiation                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOV       @_m_eTypeofCell,#0    ; [CPU_] |2827| 
	.dwpsn	file "../source/CommInterface.c",line 2832,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2832 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$377	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$377, DW_AT_low_pc(0x00)
	.dwattr $C$DW$377, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$377, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2832| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2832| 
	.dwpsn	file "../source/CommInterface.c",line 2833,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2833 | break;                                                                 
; 2835 | //!If the command received from processor is to disable the start butto
;     | n                                                                      
; 2836 | //!polling, disable the start button polling                           
; 2837 | case SITARA_STOP_POLLING_START_BUTTON_CMD:                             
; 2838 |     //!Set the cell type to invalid when initiating any seqence or     
; 2839 |     //!during the polling of start button                              
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2833| 
        ; branch occurs ; [] |2833| 
$C$L215:    
	.dwpsn	file "../source/CommInterface.c",line 2840,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2840 | m_eTypeofCell = ecellInvalid;                                          
; 2841 | //!To disable start button for next measurement                        
;----------------------------------------------------------------------
        MOVW      DP,#_m_eTypeofCell    ; [CPU_U] 
        MOV       @_m_eTypeofCell,#0    ; [CPU_] |2840| 
	.dwpsn	file "../source/CommInterface.c",line 2842,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2842 | g_bStartButtonStatus = FALSE;                                          
; 2843 | //!Clear the busy pin, so that process shall send any command for      
; 2844 | //!further sequence initiation                                         
;----------------------------------------------------------------------
        MOV       @_g_bStartButtonStatus,#0 ; [CPU_] |2842| 
	.dwpsn	file "../source/CommInterface.c",line 2845,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2845 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$378	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$378, DW_AT_low_pc(0x00)
	.dwattr $C$DW$378, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$378, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |2845| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |2845| 
	.dwpsn	file "../source/CommInterface.c",line 2846,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2846 | break;                                                                 
; 2848 | //!If the command received from processor is to disable the start butto
;     | n                                                                      
; 2849 | //!polling, disable the start button polling                           
; 2850 | case SITARA_PULSE_HEIGHT_DATA_RECEIVED:                                
; 2851 |     //!Set the cycle state to error check after transmission of pulse h
;     | eight data                                                             
; 2852 |     //! So that the errors if any shall be sent to Processor           
; 2853 |     //m_eCycleCompletion = eERROR_CHECK;                               
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2846| 
        ; branch occurs ; [] |2846| 
$C$L216:    
	.dwpsn	file "../source/CommInterface.c",line 2854,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2854 | g_bDataTransmitted = TRUE;                                             
; 2855 | //!Set the busy pin to high, indicating the current sequence is running
;----------------------------------------------------------------------
        MOVW      DP,#_g_bDataTransmitted ; [CPU_U] 
        MOVB      @_g_bDataTransmitted,#1,UNC ; [CPU_] |2854| 
	.dwpsn	file "../source/CommInterface.c",line 2856,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2856 | SPISetCommCtrlBusyInt();                                               
;----------------------------------------------------------------------
$C$DW$379	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$379, DW_AT_low_pc(0x00)
	.dwattr $C$DW$379, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$379, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |2856| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |2856| 
	.dwpsn	file "../source/CommInterface.c",line 2857,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2857 | sprintf(DebugPrintBuf, "\n SITARA_PULSE_HEIGHT_DATA_received 3\n");    
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL56        ; [CPU_U] |2857| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |2857| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2857| 
$C$DW$380	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$380, DW_AT_low_pc(0x00)
	.dwattr $C$DW$380, DW_AT_name("_sprintf")
	.dwattr $C$DW$380, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |2857| 
        ; call occurs [#_sprintf] ; [] |2857| 
	.dwpsn	file "../source/CommInterface.c",line 2858,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2858 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |2858| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |2858| 
$C$DW$381	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$381, DW_AT_low_pc(0x00)
	.dwattr $C$DW$381, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$381, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |2858| 
        ; call occurs [#_UartDebugPrint] ; [] |2858| 
	.dwpsn	file "../source/CommInterface.c",line 2859,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 2859 | break;                                                                 
; 2861 | //!If the minor command is not valid, do nothing                       
; 2862 |         default :                                                      
; 2863 |                 break;                                                 
;----------------------------------------------------------------------
        B         $C$L219,UNC           ; [CPU_] |2859| 
        ; branch occurs ; [] |2859| 
$C$L217:    
	.dwpsn	file "../source/CommInterface.c",line 2646,column 2,is_stmt,isa 0
        SUB       AL,#4102              ; [CPU_] |2646| 
        MOVZ      AR6,*-SP[3]           ; [CPU_] |2646| 
        CMPB      AL,#13                ; [CPU_] |2646| 
        B         $C$L218,LOS           ; [CPU_] |2646| 
        ; branchcc occurs ; [] |2646| 
        MOVZ      AR7,AR6               ; [CPU_] |2646| 
        MOVL      XAR4,#4609            ; [CPU_U] |2646| 
        MOVL      ACC,XAR4              ; [CPU_] |2646| 
        CMPL      ACC,XAR7              ; [CPU_] |2646| 
        B         $C$L203,EQ            ; [CPU_] |2646| 
        ; branchcc occurs ; [] |2646| 
        MOVZ      AR6,AR6               ; [CPU_] |2646| 
        MOVL      XAR4,#4610            ; [CPU_U] |2646| 
        MOVL      ACC,XAR4              ; [CPU_] |2646| 
        CMPL      ACC,XAR6              ; [CPU_] |2646| 
        B         $C$L204,EQ            ; [CPU_] |2646| 
        ; branchcc occurs ; [] |2646| 
        B         $C$L219,UNC           ; [CPU_] |2646| 
        ; branch occurs ; [] |2646| 
$C$L218:    
        SUB       AR6,#4102             ; [CPU_] |2646| 
        MOV       ACC,AR6 << #1         ; [CPU_] |2646| 
        MOVZ      AR6,AL                ; [CPU_] |2646| 
        MOVL      XAR7,#$C$SW9          ; [CPU_U] |2646| 
        MOVL      ACC,XAR7              ; [CPU_] |2646| 
        ADDU      ACC,AR6               ; [CPU_] |2646| 
        MOVL      XAR7,ACC              ; [CPU_] |2646| 
        MOVL      XAR7,*XAR7            ; [CPU_] |2646| 
        LB        *XAR7                 ; [CPU_] |2646| 
        ; branch occurs ; [] |2646| 
	.sect	".switch:_CIProcessQualityHandlingCmd"
	.clink
$C$SW9:	.long	$C$L205	; 4102
	.long	$C$L206	; 4103
	.long	$C$L207	; 4104
	.long	$C$L208	; 4105
	.long	$C$L219	; 0
	.long	$C$L219	; 0
	.long	$C$L211	; 4108
	.long	$C$L212	; 4109
	.long	$C$L213	; 4110
	.long	$C$L214	; 4111
	.long	$C$L209	; 4112
	.long	$C$L210	; 4113
	.long	$C$L215	; 4114
	.long	$C$L216	; 4115
	.sect	".text:_CIProcessQualityHandlingCmd"
$C$L219:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$382	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$382, DW_AT_low_pc(0x00)
	.dwattr $C$DW$382, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$354, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$354, DW_AT_TI_end_line(0xb31)
	.dwattr $C$DW$354, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$354

	.sect	".text:_CIFormQualityHandlingPacket"
	.clink
	.global	_CIFormQualityHandlingPacket

$C$DW$383	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$383, DW_AT_name("CIFormQualityHandlingPacket")
	.dwattr $C$DW$383, DW_AT_low_pc(_CIFormQualityHandlingPacket)
	.dwattr $C$DW$383, DW_AT_high_pc(0x00)
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_CIFormQualityHandlingPacket")
	.dwattr $C$DW$383, DW_AT_external
	.dwattr $C$DW$383, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$383, DW_AT_TI_begin_line(0xb3a)
	.dwattr $C$DW$383, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$383, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 2875,column 1,is_stmt,address _CIFormQualityHandlingPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormQualityHandlingPacket
$C$DW$384	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$384, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$384, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2874 | void CIFormQualityHandlingPacket(uint16_t usnSubCmd)                   
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormQualityHandlingPacket  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIFormQualityHandlingPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$385	.dwtag  DW_TAG_variable
	.dwattr $C$DW$385, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$385, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 2876 | //!Frame packet for Quality Handling                                   
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |2875| 
	.dwpsn	file "../source/CommInterface.c",line 2877,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2877 | CPFrameDataPacket(DELFINO_QUALITY_HANDLING, usnSubCmd, FALSE);         
;----------------------------------------------------------------------
        MOVB      XAR4,#0               ; [CPU_] |2877| 
        MOV       AL,#8197              ; [CPU_] |2877| 
        MOV       AH,*-SP[1]            ; [CPU_] |2877| 
$C$DW$386	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$386, DW_AT_low_pc(0x00)
	.dwattr $C$DW$386, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$386, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2877| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2877| 
	.dwpsn	file "../source/CommInterface.c",line 2878,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$387	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$387, DW_AT_low_pc(0x00)
	.dwattr $C$DW$387, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$383, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$383, DW_AT_TI_end_line(0xb3e)
	.dwattr $C$DW$383, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$383

	.sect	".text:_CIFormAbortProcessPacket"
	.clink
	.global	_CIFormAbortProcessPacket

$C$DW$388	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$388, DW_AT_name("CIFormAbortProcessPacket")
	.dwattr $C$DW$388, DW_AT_low_pc(_CIFormAbortProcessPacket)
	.dwattr $C$DW$388, DW_AT_high_pc(0x00)
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_CIFormAbortProcessPacket")
	.dwattr $C$DW$388, DW_AT_external
	.dwattr $C$DW$388, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$388, DW_AT_TI_begin_line(0xb47)
	.dwattr $C$DW$388, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$388, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 2888,column 1,is_stmt,address _CIFormAbortProcessPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormAbortProcessPacket
$C$DW$389	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$389, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$389, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2887 | void CIFormAbortProcessPacket(uint16_t usnSubCmd)                      
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormAbortProcessPacket     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIFormAbortProcessPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$390	.dwtag  DW_TAG_variable
	.dwattr $C$DW$390, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$390, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 2889 | //!Frame packet when any sequence is aborted                           
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |2888| 
	.dwpsn	file "../source/CommInterface.c",line 2890,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 2890 | CPFrameDataPacket(DELFINO_ABORT_PROCESS_MAIN_CMD, usnSubCmd, FALSE);   
;----------------------------------------------------------------------
        MOVB      XAR4,#0               ; [CPU_] |2890| 
        MOV       AL,#8202              ; [CPU_] |2890| 
        MOV       AH,*-SP[1]            ; [CPU_] |2890| 
$C$DW$391	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$391, DW_AT_low_pc(0x00)
	.dwattr $C$DW$391, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$391, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |2890| 
        ; call occurs [#_CPFrameDataPacket] ; [] |2890| 
	.dwpsn	file "../source/CommInterface.c",line 2891,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$392	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$392, DW_AT_low_pc(0x00)
	.dwattr $C$DW$392, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$388, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$388, DW_AT_TI_end_line(0xb4b)
	.dwattr $C$DW$388, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$388

	.sect	".text:_CIFormSequenceStatusPacket"
	.clink
	.global	_CIFormSequenceStatusPacket

$C$DW$393	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$393, DW_AT_name("CIFormSequenceStatusPacket")
	.dwattr $C$DW$393, DW_AT_low_pc(_CIFormSequenceStatusPacket)
	.dwattr $C$DW$393, DW_AT_high_pc(0x00)
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_CIFormSequenceStatusPacket")
	.dwattr $C$DW$393, DW_AT_external
	.dwattr $C$DW$393, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$393, DW_AT_TI_begin_line(0xb55)
	.dwattr $C$DW$393, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$393, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 2902,column 1,is_stmt,address _CIFormSequenceStatusPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormSequenceStatusPacket
$C$DW$394	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$394, DW_AT_name("usnSequenceState")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_usnSequenceState")
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$394, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2901 | void CIFormSequenceStatusPacket(uint16_t usnSequenceState)             
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormSequenceStatusPacket   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIFormSequenceStatusPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$395	.dwtag  DW_TAG_variable
	.dwattr $C$DW$395, DW_AT_name("usnSequenceState")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_usnSequenceState")
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$395, DW_AT_location[DW_OP_breg20 -1]

        MOV       *-SP[1],AL            ; [CPU_] |2902| 
	.dwpsn	file "../source/CommInterface.c",line 2920,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$396	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$396, DW_AT_low_pc(0x00)
	.dwattr $C$DW$396, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$393, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$393, DW_AT_TI_end_line(0xb68)
	.dwattr $C$DW$393, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$393

	.sect	".text:_CIFormSequenceStatusMinorCmd"
	.clink
	.global	_CIFormSequenceStatusMinorCmd

$C$DW$397	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$397, DW_AT_name("CIFormSequenceStatusMinorCmd")
	.dwattr $C$DW$397, DW_AT_low_pc(_CIFormSequenceStatusMinorCmd)
	.dwattr $C$DW$397, DW_AT_high_pc(0x00)
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_CIFormSequenceStatusMinorCmd")
	.dwattr $C$DW$397, DW_AT_external
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$397, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$397, DW_AT_TI_begin_line(0xb72)
	.dwattr $C$DW$397, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$397, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 2931,column 1,is_stmt,address _CIFormSequenceStatusMinorCmd,isa 0

	.dwfde $C$DW$CIE, _CIFormSequenceStatusMinorCmd
$C$DW$398	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$398, DW_AT_name("usnSequenceState")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_usnSequenceState")
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$398, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 2930 | uint16_t CIFormSequenceStatusMinorCmd(uint16_t usnSequenceState)       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormSequenceStatusMinorCmd FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CIFormSequenceStatusMinorCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$399	.dwtag  DW_TAG_variable
	.dwattr $C$DW$399, DW_AT_name("usnSequenceState")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_usnSequenceState")
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$399, DW_AT_location[DW_OP_breg20 -1]

$C$DW$400	.dwtag  DW_TAG_variable
	.dwattr $C$DW$400, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$400, DW_AT_location[DW_OP_breg20 -2]

;----------------------------------------------------------------------
; 2932 | //!Set the minor command to Zero by default                            
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |2931| 
	.dwpsn	file "../source/CommInterface.c",line 2933,column 25,is_stmt,isa 0
;----------------------------------------------------------------------
; 2933 | uint16_t usnMinorCmd=0;                                                
; 2934 | //!Check for the current state sent from MBD                           
;----------------------------------------------------------------------
        MOV       *-SP[2],#0            ; [CPU_] |2933| 
	.dwpsn	file "../source/CommInterface.c",line 2935,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 2935 | switch(usnSequenceState)                                               
; 2943 |     //!If the state received from MBD is Ready for Dispensing, set the 
; 2944 |     //!minor command to  the corresponding state                       
; 2945 |     //!\def DELFINO_STATE_READY_FOR_DISPENSING                         
; 2946 |     case eREADY_FOR_DISPENSING:                                        
; 2947 |         //Set the minor comand                                         
;----------------------------------------------------------------------
        B         $C$L231,UNC           ; [CPU_] |2935| 
        ; branch occurs ; [] |2935| 
$C$L220:    
	.dwpsn	file "../source/CommInterface.c",line 2948,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2948 | usnMinorCmd = DELFINO_STATE_READY_FOR_DISPENSING;                      
;----------------------------------------------------------------------
        MOV       *-SP[2],#8194         ; [CPU_] |2948| 
	.dwpsn	file "../source/CommInterface.c",line 2949,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2949 | break;                                                                 
; 2951 | //!If the state received from MBD is Dispensing Completed, set the     
; 2952 | //!minor command to  the corresponding state                           
; 2953 | //!\def DELFINO_STATE_DISPENSE_COMPLETED                               
; 2954 | case eDISPENSE_COMPLETED:                                              
; 2955 | //Set the minor comand                                                 
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |2949| 
        ; branch occurs ; [] |2949| 
$C$L221:    
	.dwpsn	file "../source/CommInterface.c",line 2956,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2956 | usnMinorCmd = DELFINO_STATE_DISPENSE_COMPLETED;                        
;----------------------------------------------------------------------
        MOV       *-SP[2],#8195         ; [CPU_] |2956| 
	.dwpsn	file "../source/CommInterface.c",line 2957,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2957 | break;                                                                 
; 2959 | //!If the state received from MBD is Ready for Aspiration, set the     
; 2960 | //!minor command to  the corresponding state                           
; 2961 | //!\def DELFINO_STATE_READY_FOR_ASPIRATION                             
; 2962 | case eREADY_FOR_ASPIRATION:                                            
; 2963 | //Set the minor comand                                                 
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |2957| 
        ; branch occurs ; [] |2957| 
$C$L222:    
	.dwpsn	file "../source/CommInterface.c",line 2964,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2964 | usnMinorCmd = DELFINO_STATE_READY_FOR_ASPIRATION;                      
;----------------------------------------------------------------------
        MOV       *-SP[2],#8196         ; [CPU_] |2964| 
	.dwpsn	file "../source/CommInterface.c",line 2965,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2965 | break;                                                                 
; 2967 | //!If the state received from MBD is Aspiration Completed, set the     
; 2968 | //!minor command to  the corresponding state                           
; 2969 | //!\def DELFINO_STATE_ASPIRATION_COMPLETED                             
; 2970 | case eASPIRATION_COMPLETED_MONITORING:                                 
; 2971 | //Set the minor comand                                                 
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |2965| 
        ; branch occurs ; [] |2965| 
$C$L223:    
	.dwpsn	file "../source/CommInterface.c",line 2972,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2972 | usnMinorCmd = DELFINO_STATE_ASPIRATION_COMPLETED;                      
;----------------------------------------------------------------------
        MOV       *-SP[2],#8197         ; [CPU_] |2972| 
	.dwpsn	file "../source/CommInterface.c",line 2973,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2973 | break;                                                                 
; 2975 | //!If the state received from MBD is WBC Dispensing Completed, set the 
; 2976 | //!minor command to  the corresponding state                           
; 2977 | //!\def DELFINO_STATE_WBC_DISPENSATION_COMPLETED                       
; 2978 | case eFIRST_DILUTION_COMPLETED:                                        
; 2979 | //Set the minor comand                                                 
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |2973| 
        ; branch occurs ; [] |2973| 
$C$L224:    
	.dwpsn	file "../source/CommInterface.c",line 2980,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2980 | usnMinorCmd = DELFINO_STATE_WBC_DISPENSATION_COMPLETED;                
;----------------------------------------------------------------------
        MOV       *-SP[2],#8198         ; [CPU_] |2980| 
	.dwpsn	file "../source/CommInterface.c",line 2981,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2981 | break;                                                                 
; 2983 | //!If the state received from MBD is RBC Dispensing Completed, set the 
; 2984 | //!minor command to  the corresponding state                           
; 2985 | //!\def DELFINO_STATE_RBC_DISPENSATION_COMPLETED                       
; 2986 | case eRBC_DISPENSATION_COMPLETED:                                      
; 2987 | //Set the minor comand                                                 
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |2981| 
        ; branch occurs ; [] |2981| 
$C$L225:    
	.dwpsn	file "../source/CommInterface.c",line 2988,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2988 | usnMinorCmd = DELFINO_STATE_RBC_DISPENSATION_COMPLETED;                
;----------------------------------------------------------------------
        MOV       *-SP[2],#8199         ; [CPU_] |2988| 
	.dwpsn	file "../source/CommInterface.c",line 2989,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2989 | break;                                                                 
; 2991 | //!If the state received from MBD is WBC & RBC Bubbling Completed, set
;     | the                                                                    
; 2992 | //!minor command to  the corresponding state                           
; 2993 | //!\def DELFINO_STATE_WBC_RBC_BUBBLING_COMPLETED                       
; 2994 | case eWBC_RBC_BUBBLING_COMPLETED:                                      
; 2995 | //Set the minor comand                                                 
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |2989| 
        ; branch occurs ; [] |2989| 
$C$L226:    
	.dwpsn	file "../source/CommInterface.c",line 2996,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2996 | usnMinorCmd = DELFINO_STATE_WBC_RBC_BUBBLING_COMPLETED;                
;----------------------------------------------------------------------
        MOV       *-SP[2],#8200         ; [CPU_] |2996| 
	.dwpsn	file "../source/CommInterface.c",line 2997,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 2997 | break;                                                                 
; 2999 | //!If the state received from MBD is Counting Started, set the         
; 3000 | //!minor command to  the corresponding state                           
; 3001 | //!\def DELFINO_STATE_COUNTING_STARTED                                 
; 3002 | case eCOUNTING_STARTED:                                                
; 3003 | //Set the minor comand                                                 
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |2997| 
        ; branch occurs ; [] |2997| 
$C$L227:    
	.dwpsn	file "../source/CommInterface.c",line 3004,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3004 | usnMinorCmd = DELFINO_STATE_COUNTING_STARTED;                          
;----------------------------------------------------------------------
        MOV       *-SP[2],#8201         ; [CPU_] |3004| 
	.dwpsn	file "../source/CommInterface.c",line 3005,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3005 | break;                                                                 
; 3007 | //!If the state received from MBD is Counting Completed, set the       
; 3008 | //!minor command to  the corresponding state                           
; 3009 | //!\def DELFINO_STATE_COUNTING_COMPLETED                               
; 3010 | case eCOUNTING_COMPLETED:                                              
; 3011 | //Set the minor comand                                                 
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |3005| 
        ; branch occurs ; [] |3005| 
$C$L228:    
	.dwpsn	file "../source/CommInterface.c",line 3012,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3012 | usnMinorCmd = DELFINO_STATE_COUNTING_COMPLETED;                        
;----------------------------------------------------------------------
        MOV       *-SP[2],#8202         ; [CPU_] |3012| 
	.dwpsn	file "../source/CommInterface.c",line 3013,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3013 | break;                                                                 
; 3015 | //!If the state received from MBD is Backflush Completed, set the      
; 3016 | //!minor command to  the corresponding state                           
; 3017 | //!\def DELFINO_STATE_BACKFLUSH_COMPLETED                              
; 3018 | case eBACKFLUSH_COMPLETED:                                             
; 3019 | //Set the minor comand                                                 
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |3013| 
        ; branch occurs ; [] |3013| 
$C$L229:    
	.dwpsn	file "../source/CommInterface.c",line 3020,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3020 | usnMinorCmd = DELFINO_STATE_BACKFLUSH_COMPLETED;                       
;----------------------------------------------------------------------
        MOV       *-SP[2],#8203         ; [CPU_] |3020| 
	.dwpsn	file "../source/CommInterface.c",line 3021,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3021 | break;                                                                 
; 3023 | //!If the state received from MBD is Bath Filling Completed, set the   
; 3024 | //!minor command to  the corresponding state                           
; 3025 | //!\def DELFINO_STATE_BATH_FILLING_COMPLETED                           
; 3026 | case eBATH_FILLING_COMPLETED:                                          
; 3027 | //!Set the minor comand                                                
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |3021| 
        ; branch occurs ; [] |3021| 
$C$L230:    
	.dwpsn	file "../source/CommInterface.c",line 3028,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3028 | usnMinorCmd = DELFINO_STATE_BATH_FILLING_COMPLETED;                    
;----------------------------------------------------------------------
        MOV       *-SP[2],#8204         ; [CPU_] |3028| 
	.dwpsn	file "../source/CommInterface.c",line 3029,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3029 | break;                                                                 
; 3031 | //!Do nothing if the state received from MDB doesnot match with        
; 3032 | //!any above mentioned state.                                          
; 3033 | default:                                                               
; 3034 | break;                                                                 
; 3036 | //!Return the minor command to frame the packet                        
;----------------------------------------------------------------------
        B         $C$L235,UNC           ; [CPU_] |3029| 
        ; branch occurs ; [] |3029| 
$C$L231:    
	.dwpsn	file "../source/CommInterface.c",line 2935,column 5,is_stmt,isa 0
        CMPB      AL,#13                ; [CPU_] |2935| 
        B         $C$L233,GT            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#13                ; [CPU_] |2935| 
        B         $C$L225,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#4                 ; [CPU_] |2935| 
        B         $C$L232,GT            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#4                 ; [CPU_] |2935| 
        B         $C$L222,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#2                 ; [CPU_] |2935| 
        B         $C$L220,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#3                 ; [CPU_] |2935| 
        B         $C$L221,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        B         $C$L235,UNC           ; [CPU_] |2935| 
        ; branch occurs ; [] |2935| 
$C$L232:    
        CMPB      AL,#6                 ; [CPU_] |2935| 
        B         $C$L223,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#12                ; [CPU_] |2935| 
        B         $C$L224,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        B         $C$L235,UNC           ; [CPU_] |2935| 
        ; branch occurs ; [] |2935| 
$C$L233:    
        CMPB      AL,#18                ; [CPU_] |2935| 
        B         $C$L234,GT            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#18                ; [CPU_] |2935| 
        B         $C$L228,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#16                ; [CPU_] |2935| 
        B         $C$L226,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#17                ; [CPU_] |2935| 
        B         $C$L227,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        B         $C$L235,UNC           ; [CPU_] |2935| 
        ; branch occurs ; [] |2935| 
$C$L234:    
        CMPB      AL,#23                ; [CPU_] |2935| 
        B         $C$L229,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
        CMPB      AL,#27                ; [CPU_] |2935| 
        B         $C$L230,EQ            ; [CPU_] |2935| 
        ; branchcc occurs ; [] |2935| 
$C$L235:    
	.dwpsn	file "../source/CommInterface.c",line 3037,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3037 | return usnMinorCmd;                                                    
;----------------------------------------------------------------------
        MOV       AL,*-SP[2]            ; [CPU_] |3037| 
	.dwpsn	file "../source/CommInterface.c",line 3039,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$401	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$401, DW_AT_low_pc(0x00)
	.dwattr $C$DW$401, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$397, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$397, DW_AT_TI_end_line(0xbdf)
	.dwattr $C$DW$397, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$397

	.sect	".text:_CIFormSequneceStartedCmd"
	.clink
	.global	_CIFormSequneceStartedCmd

$C$DW$402	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$402, DW_AT_name("CIFormSequneceStartedCmd")
	.dwattr $C$DW$402, DW_AT_low_pc(_CIFormSequneceStartedCmd)
	.dwattr $C$DW$402, DW_AT_high_pc(0x00)
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_CIFormSequneceStartedCmd")
	.dwattr $C$DW$402, DW_AT_external
	.dwattr $C$DW$402, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$402, DW_AT_TI_begin_line(0xbe9)
	.dwattr $C$DW$402, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$402, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../source/CommInterface.c",line 3050,column 1,is_stmt,address _CIFormSequneceStartedCmd,isa 0

	.dwfde $C$DW$CIE, _CIFormSequneceStartedCmd
;----------------------------------------------------------------------
; 3049 | void CIFormSequneceStartedCmd(void)                                    
; 3051 | //!Check for the current mode                                          
; 3052 | //    sprintf(DebugPrintBuf, "\n CIFormSequneceStartedCmd: %x \n", g_us
;     | nCurrentMode);                                                         
; 3053 | //    UartDebugPrint((int *)DebugPrintBuf, 50);                        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormSequneceStartedCmd     FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_CIFormSequneceStartedCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwpsn	file "../source/CommInterface.c",line 3054,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3054 | switch(g_usnCurrentMode)                                               
; 3056 |         case WHOLE_BLOOD_MODE:                                         
; 3057 |         case PRE_DILUENT_MODE:                                         
; 3058 |         case BODY_FLUID_MODE:                                          
; 3059 |             //usnMajorCmd = DELFINO_WHOLE_BLOOD_STATUS;                
; 3060 |             //usnMinorCmd = CIFormSequenceStatusMinorCmd(usnSequenceSta
;     | te);                                                                   
; 3061 |             //CIFormServiceHandlingPacket(MBD_USER_ABORT_Y_AXIS_PROCESS
;     | , TRUE);                                                               
; 3062 |             break;                                                     
; 3064 |         //!If the current mode mode is any of the service handling mode
;     | ,                                                                      
; 3065 |         //!then frame service handling start and end packet to be sent 
; 3066 |         //!to processor                                                
; 3067 |         case PRIME_WITH_RINSE_SERVICE_HANDLING:                        
; 3068 |         case PRIME_WITH_LYSE_SERVICE_HANDLING:                         
; 3069 |         case PRIME_WITH_DILUENT_SERVICE_HANDLING:                      
; 3070 |         case PRIME_WITH_EZ_CLEANSER_CMD:                               
; 3071 |         case PRIME_ALL_SERVICE_HANDLING:                               
; 3072 |         case BACK_FLUSH_SERVICE_HANDLING:                              
; 3073 |         case ZAP_APERTURE_SERVICE_HANDLING:                            
; 3074 |         case DRAIN_BATH_SERVICE_HANDLING:                              
; 3075 |         case DRAIN_ALL_SERVICE_HANDLING:                               
; 3076 |         case CLEAN_BATH_SERVICE_HANDLING:                              
; 3077 |         case HEAD_RINSING_SERVICE_HANDLING:                            
; 3078 |         case PROBE_CLEANING_SERVICE_HANDLING:                          
; 3079 |         case MBD_WAKEUP_START:                                         
; 3080 |         case COUNTING_TIME_SEQUENCE:                                   
; 3081 |         case MBD_TO_SHIP_SEQUENCE_START:                               
; 3082 |         case MBD_VOLUMETRIC_BOARD_CHECK:                               
; 3083 |         case MBD_SLEEP_PROCESS:                                        
; 3084 |         case MBD_ALL_MOTOR_Y_AXIS_CHECK:                               
; 3085 |         case MBD_ALL_MOTOR_X_AXIS_CHECK:                               
; 3086 |         case MBD_ALL_MOTOR_SAMPLE_CHECK:                               
; 3087 |         case MBD_ALL_MOTOR_DILUENT_CHECK:                              
; 3088 |         case MBD_ALL_MOTOR_WASTE_CHECK:                                
; 3089 |         case MBD_VOL_TUBE_EMPTY:                                       
; 3090 |         case X_AXIS_MOTOR_CHECK:                                       
; 3091 |         case Y_AXIS_MOTOR_CHECK:                                       
; 3092 |         case DILUENT_SYRINGE_MOTOR_CHECK:                              
; 3093 |         case WASTE_SYRINGE_MOTOR_CHECK:                                
; 3094 |         case SAMPLE_LYSE_SYRINGE_MOTOR_CHECK:                          
; 3095 |         case MBD_BATH_FILL_CMD:                                        
; 3096 |         case MBD_ZAP_INITIATE:                                         
; 3097 |         case GLASS_TUBE_FILLING:                                       
; 3098 |         case COUNT_FAIL:                                               
; 3099 |         case HYPO_CLEANER_FAIL:                                        
; 3100 |         //case MBD_USER_ABORT_PROCESS:                                 
; 3101 | //                sprintf(DebugPrintBuf, "\r\n CIFormSequneceStartedCmd
;     |  %d  %d \n",g_usnCurrentMode,g_usnCurrentModeMajor);                   
; 3102 | //                                 UartDebugPrint((int *)DebugPrintBuf,
;     |  50);                                                                  
;----------------------------------------------------------------------
        B         $C$L243,UNC           ; [CPU_] |3054| 
        ; branch occurs ; [] |3054| 
$C$L236:    
	.dwpsn	file "../source/CommInterface.c",line 3104,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3104 | if(SITARA_STARTUP_CMD == g_usnCurrentModeMajor)                        
;----------------------------------------------------------------------
        MOVU      ACC,@_g_usnCurrentModeMajor ; [CPU_] |3104| 
        MOVL      XAR4,#4105            ; [CPU_U] |3104| 
        CMPL      ACC,XAR4              ; [CPU_] |3104| 
        B         $C$L237,NEQ           ; [CPU_] |3104| 
        ; branchcc occurs ; [] |3104| 
	.dwpsn	file "../source/CommInterface.c",line 3106,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 3106 | CIFormStartupHandlingPacket(g_usnCurrentMode, TRUE);                   
;----------------------------------------------------------------------
        MOV       AL,@_g_usnCurrentMode ; [CPU_] |3106| 
        MOVB      AH,#1                 ; [CPU_] |3106| 
$C$DW$403	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$403, DW_AT_low_pc(0x00)
	.dwattr $C$DW$403, DW_AT_name("_CIFormStartupHandlingPacket")
	.dwattr $C$DW$403, DW_AT_TI_call

        LCR       #_CIFormStartupHandlingPacket ; [CPU_] |3106| 
        ; call occurs [#_CIFormStartupHandlingPacket] ; [] |3106| 
	.dwpsn	file "../source/CommInterface.c",line 3107,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3108 | else                                                                   
; 3110 |     //!Frame sequence started command and send to Sitara               
;----------------------------------------------------------------------
        B         $C$L246,UNC           ; [CPU_] |3107| 
        ; branch occurs ; [] |3107| 
$C$L237:    
	.dwpsn	file "../source/CommInterface.c",line 3111,column 21,is_stmt,isa 0
;----------------------------------------------------------------------
; 3111 | CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);                   
;----------------------------------------------------------------------
        MOV       AL,@_g_usnCurrentMode ; [CPU_] |3111| 
        MOVB      AH,#1                 ; [CPU_] |3111| 
$C$DW$404	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$404, DW_AT_low_pc(0x00)
	.dwattr $C$DW$404, DW_AT_name("_CIFormServiceHandlingPacket")
	.dwattr $C$DW$404, DW_AT_TI_call

        LCR       #_CIFormServiceHandlingPacket ; [CPU_] |3111| 
        ; call occurs [#_CIFormServiceHandlingPacket] ; [] |3111| 
	.dwpsn	file "../source/CommInterface.c",line 3113,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3113 | break;                                                                 
; 3115 | case START_UP_SEQUENCE_MODE:                                           
; 3116 | case MBD_STARTUP_FROM_SERVICE_SCREEN_CMD:                              
; 3117 | case MBD_STARTUP_FAILED_CMD:                                           
;----------------------------------------------------------------------
        B         $C$L246,UNC           ; [CPU_] |3113| 
        ; branch occurs ; [] |3113| 
$C$L238:    
	.dwpsn	file "../source/CommInterface.c",line 3118,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3118 | CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);                   
;----------------------------------------------------------------------
        MOV       AL,@_g_usnCurrentMode ; [CPU_] |3118| 
        MOVB      AH,#1                 ; [CPU_] |3118| 
$C$DW$405	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$405, DW_AT_low_pc(0x00)
	.dwattr $C$DW$405, DW_AT_name("_CIFormServiceHandlingPacket")
	.dwattr $C$DW$405, DW_AT_TI_call

        LCR       #_CIFormServiceHandlingPacket ; [CPU_] |3118| 
        ; call occurs [#_CIFormServiceHandlingPacket] ; [] |3118| 
	.dwpsn	file "../source/CommInterface.c",line 3119,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3119 | break;                                                                 
; 3121 | case RINSE_PRIME_FLOW_CALIBRATION://SKM_CHANGE_FLOWCAL                 
;----------------------------------------------------------------------
        B         $C$L246,UNC           ; [CPU_] |3119| 
        ; branch occurs ; [] |3119| 
$C$L239:    
	.dwpsn	file "../source/CommInterface.c",line 3123,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3123 | SPISetCommCtrlBusyInt();                                               
; 3124 | //! Frame sequence started command and send to sitara                  
;----------------------------------------------------------------------
$C$DW$406	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$406, DW_AT_low_pc(0x00)
	.dwattr $C$DW$406, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$406, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |3123| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |3123| 
	.dwpsn	file "../source/CommInterface.c",line 3125,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3125 | CIFormSystemSettingPacket(DELFINO_FLOW_CALIBRATION_STARTED);           
; 3126 | //CPFrameDataPacket(DELFINO_SYSTEM_SETTING_CMD, DELFINO_FLOW_CALIBRATIO
;     | N_STARTED, TRUE);                                                      
;----------------------------------------------------------------------
        MOV       AL,#8194              ; [CPU_] |3125| 
$C$DW$407	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$407, DW_AT_low_pc(0x00)
	.dwattr $C$DW$407, DW_AT_name("_CIFormSystemSettingPacket")
	.dwattr $C$DW$407, DW_AT_TI_call

        LCR       #_CIFormSystemSettingPacket ; [CPU_] |3125| 
        ; call occurs [#_CIFormSystemSettingPacket] ; [] |3125| 
	.dwpsn	file "../source/CommInterface.c",line 3127,column 17,is_stmt,isa 0
;----------------------------------------------------------------------
; 3127 | break;                                                                 
; 3129 | case SHUTDOWN_SEQUENCE://HN_Added                                      
;----------------------------------------------------------------------
        B         $C$L246,UNC           ; [CPU_] |3127| 
        ; branch occurs ; [] |3127| 
$C$L240:    
	.dwpsn	file "../source/CommInterface.c",line 3130,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 3130 | SPISetCommCtrlBusyInt();                                               
; 3131 | //! Frame sequence started command and send to sitara                  
;----------------------------------------------------------------------
$C$DW$408	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$408, DW_AT_low_pc(0x00)
	.dwattr $C$DW$408, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$408, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |3130| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |3130| 
	.dwpsn	file "../source/CommInterface.c",line 3132,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 3132 | CIFormShutDownSequencePacket(DELFINO_SHUTDOWN_SEQUENCE_STARTED);       
;----------------------------------------------------------------------
        MOV       AL,#8962              ; [CPU_] |3132| 
$C$DW$409	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$409, DW_AT_low_pc(0x00)
	.dwattr $C$DW$409, DW_AT_name("_CIFormShutDownSequencePacket")
	.dwattr $C$DW$409, DW_AT_TI_call

        LCR       #_CIFormShutDownSequencePacket ; [CPU_] |3132| 
        ; call occurs [#_CIFormShutDownSequencePacket] ; [] |3132| 
	.dwpsn	file "../source/CommInterface.c",line 3133,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 3133 | break;                                                                 
; 3135 | case MBD_USER_ABORT_PROCESS:                                           
;----------------------------------------------------------------------
        B         $C$L246,UNC           ; [CPU_] |3133| 
        ; branch occurs ; [] |3133| 
$C$L241:    
	.dwpsn	file "../source/CommInterface.c",line 3136,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 3136 | SPISetCommCtrlBusyInt();                                               
;----------------------------------------------------------------------
$C$DW$410	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$410, DW_AT_low_pc(0x00)
	.dwattr $C$DW$410, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$410, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |3136| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |3136| 
	.dwpsn	file "../source/CommInterface.c",line 3137,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 3137 | CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);                   
; 3138 | //CIFormAbortProcessPacket(DELFINO_ABORT_SEQUENCE_STARTED_CMD);        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOVB      AH,#1                 ; [CPU_] |3137| 
        MOV       AL,@_g_usnCurrentMode ; [CPU_] |3137| 
$C$DW$411	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$411, DW_AT_low_pc(0x00)
	.dwattr $C$DW$411, DW_AT_name("_CIFormServiceHandlingPacket")
	.dwattr $C$DW$411, DW_AT_TI_call

        LCR       #_CIFormServiceHandlingPacket ; [CPU_] |3137| 
        ; call occurs [#_CIFormServiceHandlingPacket] ; [] |3137| 
	.dwpsn	file "../source/CommInterface.c",line 3139,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 3139 | break;                                                                 
; 3142 | case MBD_USER_ABORT_Y_AXIS_PROCESS:                                    
;----------------------------------------------------------------------
        B         $C$L246,UNC           ; [CPU_] |3139| 
        ; branch occurs ; [] |3139| 
$C$L242:    
	.dwpsn	file "../source/CommInterface.c",line 3143,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 3143 | SPISetCommCtrlBusyInt();                                               
; 3144 | //CIFormAbortProcessPacket(DELFINO_Y_AXIS_ABORT_SEQUENCE_STARTED_CMD);
;     |         /*DS Testing*/                                                 
;----------------------------------------------------------------------
$C$DW$412	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$412, DW_AT_low_pc(0x00)
	.dwattr $C$DW$412, DW_AT_name("_SPISetCommCtrlBusyInt")
	.dwattr $C$DW$412, DW_AT_TI_call

        LCR       #_SPISetCommCtrlBusyInt ; [CPU_] |3143| 
        ; call occurs [#_SPISetCommCtrlBusyInt] ; [] |3143| 
	.dwpsn	file "../source/CommInterface.c",line 3145,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 3145 | CIFormServiceHandlingPacket(g_usnCurrentMode, TRUE);
;     |         /*DS Testing*/                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOVB      AH,#1                 ; [CPU_] |3145| 
        MOV       AL,@_g_usnCurrentMode ; [CPU_] |3145| 
$C$DW$413	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$413, DW_AT_low_pc(0x00)
	.dwattr $C$DW$413, DW_AT_name("_CIFormServiceHandlingPacket")
	.dwattr $C$DW$413, DW_AT_TI_call

        LCR       #_CIFormServiceHandlingPacket ; [CPU_] |3145| 
        ; call occurs [#_CIFormServiceHandlingPacket] ; [] |3145| 
	.dwpsn	file "../source/CommInterface.c",line 3146,column 18,is_stmt,isa 0
;----------------------------------------------------------------------
; 3146 | break;                                                                 
; 3147 | default:                                                               
; 3148 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L246,UNC           ; [CPU_] |3146| 
        ; branch occurs ; [] |3146| 
$C$L243:    
	.dwpsn	file "../source/CommInterface.c",line 3054,column 5,is_stmt,isa 0
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       AH,@_g_usnCurrentMode ; [CPU_] |3054| 
        MOV       AL,AH                 ; [CPU_] |3054| 
        SUB       AL,#20481             ; [CPU_] |3054| 
        CMPB      AL,#30                ; [CPU_] |3054| 
        B         $C$L244,LOS           ; [CPU_] |3054| 
        ; branchcc occurs ; [] |3054| 
        MOV       AL,AH                 ; [CPU_] |3054| 
        SUB       AL,#20520             ; [CPU_] |3054| 
        CMPB      AL,#23                ; [CPU_] |3054| 
        B         $C$L245,LOS           ; [CPU_] |3054| 
        ; branchcc occurs ; [] |3054| 
        B         $C$L246,UNC           ; [CPU_] |3054| 
        ; branch occurs ; [] |3054| 
$C$L244:    
        MOV       AL,AH                 ; [CPU_] |3054| 
        SUB       AL,#20481             ; [CPU_] |3054| 
        MOV       ACC,AL << #1          ; [CPU_] |3054| 
        MOVZ      AR6,AL                ; [CPU_] |3054| 
        MOVL      XAR7,#$C$SW12         ; [CPU_U] |3054| 
        MOVL      ACC,XAR7              ; [CPU_] |3054| 
        ADDU      ACC,AR6               ; [CPU_] |3054| 
        MOVL      XAR7,ACC              ; [CPU_] |3054| 
        MOVL      XAR7,*XAR7            ; [CPU_] |3054| 
        LB        *XAR7                 ; [CPU_] |3054| 
        ; branch occurs ; [] |3054| 
	.sect	".switch:_CIFormSequneceStartedCmd"
	.clink
$C$SW12:	.long	$C$L236	; 20481
	.long	$C$L236	; 20482
	.long	$C$L236	; 20483
	.long	$C$L236	; 20484
	.long	$C$L236	; 20485
	.long	$C$L236	; 20486
	.long	$C$L246	; 20487
	.long	$C$L236	; 20488
	.long	$C$L236	; 20489
	.long	$C$L236	; 20490
	.long	$C$L236	; 20491
	.long	$C$L246	; 0
	.long	$C$L246	; 0
	.long	$C$L246	; 0
	.long	$C$L246	; 20495
	.long	$C$L246	; 0
	.long	$C$L246	; 0
	.long	$C$L246	; 20498
	.long	$C$L236	; 20499
	.long	$C$L238	; 20500
	.long	$C$L236	; 20501
	.long	$C$L236	; 20502
	.long	$C$L246	; 0
	.long	$C$L246	; 0
	.long	$C$L236	; 20505
	.long	$C$L236	; 20506
	.long	$C$L236	; 20507
	.long	$C$L236	; 20508
	.long	$C$L236	; 20509
	.long	$C$L246	; 0
	.long	$C$L240	; 20511
	.sect	".text:_CIFormSequneceStartedCmd"
$C$L245:    
        MOV       AL,AH                 ; [CPU_] |3054| 
        SUB       AL,#20520             ; [CPU_] |3054| 
        MOV       ACC,AL << #1          ; [CPU_] |3054| 
        MOVZ      AR6,AL                ; [CPU_] |3054| 
        MOVL      XAR7,#$C$SW13         ; [CPU_U] |3054| 
        MOVL      ACC,XAR7              ; [CPU_] |3054| 
        ADDU      ACC,AR6               ; [CPU_] |3054| 
        MOVL      XAR7,ACC              ; [CPU_] |3054| 
        MOVL      XAR7,*XAR7            ; [CPU_] |3054| 
        LB        *XAR7                 ; [CPU_] |3054| 
        ; branch occurs ; [] |3054| 
	.sect	".switch:_CIFormSequneceStartedCmd"
	.clink
$C$SW13:	.long	$C$L241	; 20520
	.long	$C$L236	; 20521
	.long	$C$L236	; 20522
	.long	$C$L246	; 0
	.long	$C$L239	; 20524
	.long	$C$L242	; 20525
	.long	$C$L236	; 20526
	.long	$C$L236	; 20527
	.long	$C$L236	; 20528
	.long	$C$L236	; 20529
	.long	$C$L236	; 20530
	.long	$C$L236	; 20531
	.long	$C$L236	; 20532
	.long	$C$L246	; 0
	.long	$C$L236	; 20534
	.long	$C$L246	; 0
	.long	$C$L236	; 20536
	.long	$C$L238	; 20537
	.long	$C$L246	; 0
	.long	$C$L236	; 20539
	.long	$C$L238	; 20540
	.long	$C$L236	; 20541
	.long	$C$L236	; 20542
	.long	$C$L236	; 20543
	.sect	".text:_CIFormSequneceStartedCmd"
$C$L246:    
$C$DW$414	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$414, DW_AT_low_pc(0x00)
	.dwattr $C$DW$414, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$402, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$402, DW_AT_TI_end_line(0xc4e)
	.dwattr $C$DW$402, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$402

	.sect	".text:_CIFormShutDownSequencePacket"
	.clink
	.global	_CIFormShutDownSequencePacket

$C$DW$415	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$415, DW_AT_name("CIFormShutDownSequencePacket")
	.dwattr $C$DW$415, DW_AT_low_pc(_CIFormShutDownSequencePacket)
	.dwattr $C$DW$415, DW_AT_high_pc(0x00)
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_CIFormShutDownSequencePacket")
	.dwattr $C$DW$415, DW_AT_external
	.dwattr $C$DW$415, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$415, DW_AT_TI_begin_line(0xc5f)
	.dwattr $C$DW$415, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$415, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 3168,column 1,is_stmt,address _CIFormShutDownSequencePacket,isa 0

	.dwfde $C$DW$CIE, _CIFormShutDownSequencePacket
$C$DW$416	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$416, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$416, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 3167 | void CIFormShutDownSequencePacket(uint16_t usnMinorCmd)                
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormShutDownSequencePacket FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIFormShutDownSequencePacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$417	.dwtag  DW_TAG_variable
	.dwattr $C$DW$417, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$417, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 3169 | //sprintf(DebugPrintBuf, "\n Shutdown Minor Command: %x \n", usnMinorCm
;     | d);                                                                    
; 3170 | //UartDebugPrint((int *)DebugPrintBuf, 50);                            
; 3172 | //!Frame shutdown seqeunce completed command and sent to Sitara        
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |3168| 
	.dwpsn	file "../source/CommInterface.c",line 3173,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3173 | if(usnMinorCmd != NULL)                                                
;----------------------------------------------------------------------
        CMPB      AL,#0                 ; [CPU_] |3173| 
        B         $C$L247,EQ            ; [CPU_] |3173| 
        ; branchcc occurs ; [] |3173| 
	.dwpsn	file "../source/CommInterface.c",line 3175,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3175 | CPFrameDataPacket(DELFINO_SHUTDOWN_SEQUENCE_CMD,usnMinorCmd, FALSE);   
;----------------------------------------------------------------------
        MOV       AL,#8199              ; [CPU_] |3175| 
        MOV       AH,*-SP[1]            ; [CPU_] |3175| 
        MOVB      XAR4,#0               ; [CPU_] |3175| 
$C$DW$418	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$418, DW_AT_low_pc(0x00)
	.dwattr $C$DW$418, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$418, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |3175| 
        ; call occurs [#_CPFrameDataPacket] ; [] |3175| 
	.dwpsn	file "../source/CommInterface.c",line 3178,column 1,is_stmt,isa 0
$C$L247:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$419	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$419, DW_AT_low_pc(0x00)
	.dwattr $C$DW$419, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$415, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$415, DW_AT_TI_end_line(0xc6a)
	.dwattr $C$DW$415, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$415

	.sect	".text:_CIProcessAbortMainProcessCmd"
	.clink
	.global	_CIProcessAbortMainProcessCmd

$C$DW$420	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$420, DW_AT_name("CIProcessAbortMainProcessCmd")
	.dwattr $C$DW$420, DW_AT_low_pc(_CIProcessAbortMainProcessCmd)
	.dwattr $C$DW$420, DW_AT_high_pc(0x00)
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_CIProcessAbortMainProcessCmd")
	.dwattr $C$DW$420, DW_AT_external
	.dwattr $C$DW$420, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$420, DW_AT_TI_begin_line(0xc73)
	.dwattr $C$DW$420, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$420, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommInterface.c",line 3188,column 1,is_stmt,address _CIProcessAbortMainProcessCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessAbortMainProcessCmd
$C$DW$421	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$421, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$421, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 3187 | void CIProcessAbortMainProcessCmd(uint16_t usnMinorCmd)                
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessAbortMainProcessCmd FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessAbortMainProcessCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$422	.dwtag  DW_TAG_variable
	.dwattr $C$DW$422, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$422, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 3189 | //!Check for the minor command                                         
; 3190 | //!If the minor command is Abort process, re-initialize all the MBD rel
;     | ated                                                                   
; 3191 | //!variables and set the current mode to Abort process.                
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |3188| 
	.dwpsn	file "../source/CommInterface.c",line 3192,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3192 | switch(usnMinorCmd)                                                    
; 3194 |     case SITARA_ABORT_PROCESS_SUB_CMD:                                 
; 3195 |         //!Initialize MBD related variables                            
;----------------------------------------------------------------------
        B         $C$L252,UNC           ; [CPU_] |3192| 
        ; branch occurs ; [] |3192| 
$C$L248:    
	.dwpsn	file "../source/CommInterface.c",line 3196,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3196 | BloodCellCounter_initialize();                                         
; 3197 | //!Set all motors to idlemode                                          
;----------------------------------------------------------------------
$C$DW$423	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$423, DW_AT_low_pc(0x00)
	.dwattr $C$DW$423, DW_AT_name("_BloodCellCounter_initialize")
	.dwattr $C$DW$423, DW_AT_TI_call

        LCR       #_BloodCellCounter_initialize ; [CPU_] |3196| 
        ; call occurs [#_BloodCellCounter_initialize] ; [] |3196| 
	.dwpsn	file "../source/CommInterface.c",line 3198,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3198 | SysMotorControlIdleMode();                                             
;----------------------------------------------------------------------
$C$DW$424	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$424, DW_AT_low_pc(0x00)
	.dwattr $C$DW$424, DW_AT_name("_SysMotorControlIdleMode")
	.dwattr $C$DW$424, DW_AT_TI_call

        LCR       #_SysMotorControlIdleMode ; [CPU_] |3198| 
        ; call occurs [#_SysMotorControlIdleMode] ; [] |3198| 
	.dwpsn	file "../source/CommInterface.c",line 3200,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3200 | PWMParamDefaultState(); //HN_added                                     
; 3202 | //!Set minor command to MBD                                            
;----------------------------------------------------------------------
$C$DW$425	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$425, DW_AT_low_pc(0x00)
	.dwattr $C$DW$425, DW_AT_name("_PWMParamDefaultState")
	.dwattr $C$DW$425, DW_AT_TI_call

        LCR       #_PWMParamDefaultState ; [CPU_] |3200| 
        ; call occurs [#_PWMParamDefaultState] ; [] |3200| 
	.dwpsn	file "../source/CommInterface.c",line 3203,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3203 | g_usnCurrentMode = MBD_USER_ABORT_PROCESS;                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20520 ; [CPU_] |3203| 
	.dwpsn	file "../source/CommInterface.c",line 3204,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3204 | g_usnCurrentModeTemp = ZERO;                                           
; 3205 | //!Clear the Data mode to Zero                                         
;----------------------------------------------------------------------
        MOV       @_g_usnCurrentModeTemp,#0 ; [CPU_] |3204| 
	.dwpsn	file "../source/CommInterface.c",line 3206,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3206 | g_usnDataMode = 0;                                                     
; 3207 | //!Set sequence completed to False                                     
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnDataMode    ; [CPU_U] 
        MOV       @_g_usnDataMode,#0    ; [CPU_] |3206| 
	.dwpsn	file "../source/CommInterface.c",line 3208,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3208 | m_bSequenceCompleted = FALSE;                                          
; 3209 | //!Set cycle completion state to default                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSequenceCompleted ; [CPU_U] 
        MOV       @_m_bSequenceCompleted,#0 ; [CPU_] |3208| 
	.dwpsn	file "../source/CommInterface.c",line 3210,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3210 | m_eCycleCompletion = eDefault;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_eCycleCompletion ; [CPU_U] 
        MOV       @_m_eCycleCompletion,#0 ; [CPU_] |3210| 
	.dwpsn	file "../source/CommInterface.c",line 3212,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3212 | g_bStartButtonStatus = FALSE;                                          
; 3214 | //sprintf(DebugPrintBuf, "\r\n eDefault 4\n");                         
; 3215 | //UartDebugPrint((int *)DebugPrintBuf, 40);                            
; 3217 | //!Enable the flag for ready for aspiration which is the default condit
;     | ion                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_g_bStartButtonStatus ; [CPU_U] 
        MOV       @_g_bStartButtonStatus,#0 ; [CPU_] |3212| 
	.dwpsn	file "../source/CommInterface.c",line 3218,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3218 | m_bReadyforAspCommand = TRUE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_m_bReadyforAspCommand ; [CPU_U] 
        MOVB      @_m_bReadyforAspCommand,#1,UNC ; [CPU_] |3218| 
	.dwpsn	file "../source/CommInterface.c",line 3220,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3220 | m_bPressureMonitor = FALSE;                                            
; 3222 | //!Reset the control status flag to false to write next command        
; 3223 | //! to send to Sitara                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_bPressureMonitor ; [CPU_U] 
        MOV       @_m_bPressureMonitor,#0 ; [CPU_] |3220| 
	.dwpsn	file "../source/CommInterface.c",line 3224,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3224 | m_bSPIWriteControlStatus = FALSE;                                      
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSPIWriteControlStatus ; [CPU_U] 
        MOV       @_m_bSPIWriteControlStatus,#0 ; [CPU_] |3224| 
	.dwpsn	file "../source/CommInterface.c",line 3227,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3227 | m_bTxToDMAFlag = FALSE;                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_bTxToDMAFlag   ; [CPU_U] 
        MOV       @_m_bTxToDMAFlag,#0   ; [CPU_] |3227| 
	.dwpsn	file "../source/CommInterface.c",line 3230,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3230 | m_bTxPendingFlag = FALSE;                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bTxPendingFlag ; [CPU_U] 
        MOV       @_m_bTxPendingFlag,#0 ; [CPU_] |3230| 
	.dwpsn	file "../source/CommInterface.c",line 3231,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3231 | SPIReConfigCommDmaReg();                                               
;----------------------------------------------------------------------
$C$DW$426	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$426, DW_AT_low_pc(0x00)
	.dwattr $C$DW$426, DW_AT_name("_SPIReConfigCommDmaReg")
	.dwattr $C$DW$426, DW_AT_TI_call

        LCR       #_SPIReConfigCommDmaReg ; [CPU_] |3231| 
        ; call occurs [#_SPIReConfigCommDmaReg] ; [] |3231| 
	.dwpsn	file "../source/CommInterface.c",line 3232,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3232 | break;                                                                 
; 3234 | //!If the command received from processor is to inform the start button
; 3235 | //!is pressed                                                          
; 3236 | case SITARA_TRIGGER_BUTTON_PRESSED:                                    
; 3237 | //!Set the sample start button press to TRUE                           
;----------------------------------------------------------------------
        B         $C$L253,UNC           ; [CPU_] |3232| 
        ; branch occurs ; [] |3232| 
$C$L249:    
	.dwpsn	file "../source/CommInterface.c",line 3238,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3238 | g_bStartButtonStatus = TRUE;                                           
;----------------------------------------------------------------------
        MOVW      DP,#_g_bStartButtonStatus ; [CPU_U] 
        MOVB      @_g_bStartButtonStatus,#1,UNC ; [CPU_] |3238| 
	.dwpsn	file "../source/CommInterface.c",line 3239,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3239 | break;                                                                 
; 3241 | case SITARA_ABORT_PROCESS_RESET_COMMAND:                               
; 3243 |     //!Clear the busy pin so that Processor can send the next command  
;----------------------------------------------------------------------
        B         $C$L253,UNC           ; [CPU_] |3239| 
        ; branch occurs ; [] |3239| 
$C$L250:    
	.dwpsn	file "../source/CommInterface.c",line 3244,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3244 | SPIClearCommCtrlBusyInt();                                             
; 3245 | //!Set minor command to MBD                                            
;----------------------------------------------------------------------
$C$DW$427	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$427, DW_AT_low_pc(0x00)
	.dwattr $C$DW$427, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$427, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |3244| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |3244| 
	.dwpsn	file "../source/CommInterface.c",line 3246,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3246 | g_usnCurrentMode = ZERO;                                               
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#0 ; [CPU_] |3246| 
	.dwpsn	file "../source/CommInterface.c",line 3247,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3247 | g_usnCurrentModeMajor = ZERO;                                          
;----------------------------------------------------------------------
        MOV       @_g_usnCurrentModeMajor,#0 ; [CPU_] |3247| 
	.dwpsn	file "../source/CommInterface.c",line 3248,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3248 | g_usnCurrentModeTemp = ZERO;                                           
; 3251 | //!Initialize MBD related variables                                    
;----------------------------------------------------------------------
        MOV       @_g_usnCurrentModeTemp,#0 ; [CPU_] |3248| 
	.dwpsn	file "../source/CommInterface.c",line 3252,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3252 | BloodCellCounter_initialize();                                         
; 3253 | //!Set all the motors to Idle mode - No Motor shall run after          
; 3254 | //!any process is aborted                                              
;----------------------------------------------------------------------
$C$DW$428	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$428, DW_AT_low_pc(0x00)
	.dwattr $C$DW$428, DW_AT_name("_BloodCellCounter_initialize")
	.dwattr $C$DW$428, DW_AT_TI_call

        LCR       #_BloodCellCounter_initialize ; [CPU_] |3252| 
        ; call occurs [#_BloodCellCounter_initialize] ; [] |3252| 
	.dwpsn	file "../source/CommInterface.c",line 3255,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3255 | SysMotorControlIdleMode();                                             
;----------------------------------------------------------------------
$C$DW$429	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$429, DW_AT_low_pc(0x00)
	.dwattr $C$DW$429, DW_AT_name("_SysMotorControlIdleMode")
	.dwattr $C$DW$429, DW_AT_TI_call

        LCR       #_SysMotorControlIdleMode ; [CPU_] |3255| 
        ; call occurs [#_SysMotorControlIdleMode] ; [] |3255| 
	.dwpsn	file "../source/CommInterface.c",line 3257,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3257 | PWMParamDefaultState();                                                
; 3258 | //!Clear the Data mode to Zero                                         
;----------------------------------------------------------------------
$C$DW$430	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$430, DW_AT_low_pc(0x00)
	.dwattr $C$DW$430, DW_AT_name("_PWMParamDefaultState")
	.dwattr $C$DW$430, DW_AT_TI_call

        LCR       #_PWMParamDefaultState ; [CPU_] |3257| 
        ; call occurs [#_PWMParamDefaultState] ; [] |3257| 
	.dwpsn	file "../source/CommInterface.c",line 3259,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3259 | g_usnDataMode = 0;                                                     
; 3260 | //!Set sequence completed to False                                     
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnDataMode    ; [CPU_U] 
        MOV       @_g_usnDataMode,#0    ; [CPU_] |3259| 
	.dwpsn	file "../source/CommInterface.c",line 3261,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3261 | m_bSequenceCompleted = FALSE;                                          
; 3262 | //!Set cycle completion state to default                               
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSequenceCompleted ; [CPU_U] 
        MOV       @_m_bSequenceCompleted,#0 ; [CPU_] |3261| 
	.dwpsn	file "../source/CommInterface.c",line 3263,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3263 | m_eCycleCompletion = eDefault;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_m_eCycleCompletion ; [CPU_U] 
        MOV       @_m_eCycleCompletion,#0 ; [CPU_] |3263| 
	.dwpsn	file "../source/CommInterface.c",line 3265,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3265 | sprintf(DebugPrintBuf, "\r\n Arrest From Master\n");                   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL57        ; [CPU_U] |3265| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3265| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3265| 
$C$DW$431	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$431, DW_AT_low_pc(0x00)
	.dwattr $C$DW$431, DW_AT_name("_sprintf")
	.dwattr $C$DW$431, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |3265| 
        ; call occurs [#_sprintf] ; [] |3265| 
	.dwpsn	file "../source/CommInterface.c",line 3266,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3266 | UartDebugPrint((int *)DebugPrintBuf, 40);                              
; 3268 | //!Set all the motors to Idle mode - No Motor shall run after          
; 3269 | //!any process is aborted                                              
;----------------------------------------------------------------------
        MOVB      AL,#40                ; [CPU_] |3266| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3266| 
$C$DW$432	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$432, DW_AT_low_pc(0x00)
	.dwattr $C$DW$432, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$432, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |3266| 
        ; call occurs [#_UartDebugPrint] ; [] |3266| 
	.dwpsn	file "../source/CommInterface.c",line 3270,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3270 | SysMotorControlIdleMode();                                             
; 3272 | //!Enable the flag for ready for aspiration which is the default condit
;     | ion                                                                    
;----------------------------------------------------------------------
$C$DW$433	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$433, DW_AT_low_pc(0x00)
	.dwattr $C$DW$433, DW_AT_name("_SysMotorControlIdleMode")
	.dwattr $C$DW$433, DW_AT_TI_call

        LCR       #_SysMotorControlIdleMode ; [CPU_] |3270| 
        ; call occurs [#_SysMotorControlIdleMode] ; [] |3270| 
	.dwpsn	file "../source/CommInterface.c",line 3273,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3273 | m_bReadyforAspCommand = TRUE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_m_bReadyforAspCommand ; [CPU_U] 
        MOVB      @_m_bReadyforAspCommand,#1,UNC ; [CPU_] |3273| 
	.dwpsn	file "../source/CommInterface.c",line 3275,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3275 | g_bStartButtonStatus = FALSE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bStartButtonStatus ; [CPU_U] 
        MOV       @_g_bStartButtonStatus,#0 ; [CPU_] |3275| 
	.dwpsn	file "../source/CommInterface.c",line 3277,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3277 | m_bPressureMonitor = FALSE;                                            
; 3279 | //!Reset the control status flag to false to write next command        
; 3280 | //! to send to Sitara                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_m_bPressureMonitor ; [CPU_U] 
        MOV       @_m_bPressureMonitor,#0 ; [CPU_] |3277| 
	.dwpsn	file "../source/CommInterface.c",line 3281,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3281 | m_bSPIWriteControlStatus = FALSE;                                      
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSPIWriteControlStatus ; [CPU_U] 
        MOV       @_m_bSPIWriteControlStatus,#0 ; [CPU_] |3281| 
	.dwpsn	file "../source/CommInterface.c",line 3284,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3284 | m_bTxToDMAFlag = FALSE;                                                
;----------------------------------------------------------------------
        MOVW      DP,#_m_bTxToDMAFlag   ; [CPU_U] 
        MOV       @_m_bTxToDMAFlag,#0   ; [CPU_] |3284| 
	.dwpsn	file "../source/CommInterface.c",line 3287,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3287 | m_bTxPendingFlag = FALSE;                                              
;----------------------------------------------------------------------
        MOVW      DP,#_m_bTxPendingFlag ; [CPU_U] 
        MOV       @_m_bTxPendingFlag,#0 ; [CPU_] |3287| 
	.dwpsn	file "../source/CommInterface.c",line 3288,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3288 | SPIReConfigCommDmaReg();                                               
;----------------------------------------------------------------------
$C$DW$434	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$434, DW_AT_low_pc(0x00)
	.dwattr $C$DW$434, DW_AT_name("_SPIReConfigCommDmaReg")
	.dwattr $C$DW$434, DW_AT_TI_call

        LCR       #_SPIReConfigCommDmaReg ; [CPU_] |3288| 
        ; call occurs [#_SPIReConfigCommDmaReg] ; [] |3288| 
	.dwpsn	file "../source/CommInterface.c",line 3289,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3289 | break;                                                                 
; 3291 | case SITARA_ABORT_Y_AXIS_PROCESS:                                      
; 3292 | //! valve should close If any pressure valve is open                   
;----------------------------------------------------------------------
        B         $C$L253,UNC           ; [CPU_] |3289| 
        ; branch occurs ; [] |3289| 
$C$L251:    
	.dwpsn	file "../source/CommInterface.c",line 3293,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3293 | m_bVlaveCheckStatus = false;                                           
;----------------------------------------------------------------------
        MOVW      DP,#_m_bVlaveCheckStatus ; [CPU_U] 
        MOV       @_m_bVlaveCheckStatus,#0 ; [CPU_] |3293| 
	.dwpsn	file "../source/CommInterface.c",line 3294,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3294 | m_ucValveControlTime = ZERO;                                           
;----------------------------------------------------------------------
        MOVW      DP,#_m_ucValveControlTime ; [CPU_U] 
        MOV       @_m_ucValveControlTime,#0 ; [CPU_] |3294| 
	.dwpsn	file "../source/CommInterface.c",line 3295,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3295 | g_usnCurrentMode = MBD_USER_ABORT_Y_AXIS_PROCESS;                      
; 3297 | //!Switch on the PWM of the HGB                                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20525 ; [CPU_] |3295| 
	.dwpsn	file "../source/CommInterface.c",line 3298,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3298 | PWMHgbPwmControl(START, HGB_PWM_FREQUENCY);                            
; 3300 | //sprintf(DebugPrintBuf, "\r\n MBD_USER_ABORT_Y_AXIS_PROCESS Recieved\n
;     | \n", BloodCellCounter_Y.CycleFlag);                                    
; 3301 | //UartDebugPrint((int *)DebugPrintBuf, 50);                            
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |3298| 
        MOV       AH,#1000              ; [CPU_] |3298| 
$C$DW$435	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$435, DW_AT_low_pc(0x00)
	.dwattr $C$DW$435, DW_AT_name("_PWMHgbPwmControl")
	.dwattr $C$DW$435, DW_AT_TI_call

        LCR       #_PWMHgbPwmControl    ; [CPU_] |3298| 
        ; call occurs [#_PWMHgbPwmControl] ; [] |3298| 
	.dwpsn	file "../source/CommInterface.c",line 3302,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3302 | break;                                                                 
; 3303 | default:                                                               
; 3304 | break;                                                                 
;----------------------------------------------------------------------
        B         $C$L253,UNC           ; [CPU_] |3302| 
        ; branch occurs ; [] |3302| 
$C$L252:    
	.dwpsn	file "../source/CommInterface.c",line 3192,column 5,is_stmt,isa 0
        MOVZ      AR6,*-SP[3]           ; [CPU_] |3192| 
        MOVZ      AR7,AR6               ; [CPU_] |3192| 
        MOVL      XAR4,#7099            ; [CPU_U] |3192| 
        MOVL      ACC,XAR4              ; [CPU_] |3192| 
        CMPL      ACC,XAR7              ; [CPU_] |3192| 
        B         $C$L248,EQ            ; [CPU_] |3192| 
        ; branchcc occurs ; [] |3192| 
        MOVZ      AR7,AR6               ; [CPU_] |3192| 
        MOVL      XAR4,#7102            ; [CPU_U] |3192| 
        MOVL      ACC,XAR4              ; [CPU_] |3192| 
        CMPL      ACC,XAR7              ; [CPU_] |3192| 
        B         $C$L251,EQ            ; [CPU_] |3192| 
        ; branchcc occurs ; [] |3192| 
        MOVZ      AR7,AR6               ; [CPU_] |3192| 
        MOVL      XAR4,#7117            ; [CPU_U] |3192| 
        MOVL      ACC,XAR4              ; [CPU_] |3192| 
        CMPL      ACC,XAR7              ; [CPU_] |3192| 
        B         $C$L250,EQ            ; [CPU_] |3192| 
        ; branchcc occurs ; [] |3192| 
        MOVZ      AR6,AR6               ; [CPU_] |3192| 
        MOVL      XAR4,#7372            ; [CPU_U] |3192| 
        MOVL      ACC,XAR4              ; [CPU_] |3192| 
        CMPL      ACC,XAR6              ; [CPU_] |3192| 
        B         $C$L249,EQ            ; [CPU_] |3192| 
        ; branchcc occurs ; [] |3192| 
        B         $C$L253,UNC           ; [CPU_] |3192| 
        ; branch occurs ; [] |3192| 
$C$L253:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$436	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$436, DW_AT_low_pc(0x00)
	.dwattr $C$DW$436, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$420, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$420, DW_AT_TI_end_line(0xceb)
	.dwattr $C$DW$420, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$420

	.sect	".text:_CIFormSystemSettingPacket"
	.clink
	.global	_CIFormSystemSettingPacket

$C$DW$437	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$437, DW_AT_name("CIFormSystemSettingPacket")
	.dwattr $C$DW$437, DW_AT_low_pc(_CIFormSystemSettingPacket)
	.dwattr $C$DW$437, DW_AT_high_pc(0x00)
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_CIFormSystemSettingPacket")
	.dwattr $C$DW$437, DW_AT_external
	.dwattr $C$DW$437, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$437, DW_AT_TI_begin_line(0xcf5)
	.dwattr $C$DW$437, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$437, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 3318,column 1,is_stmt,address _CIFormSystemSettingPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormSystemSettingPacket
$C$DW$438	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$438, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$438, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 3317 | void CIFormSystemSettingPacket(uint16_t usnSubCmd)                     
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormSystemSettingPacket    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIFormSystemSettingPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$439	.dwtag  DW_TAG_variable
	.dwattr $C$DW$439, DW_AT_name("usnSubCmd")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_usnSubCmd")
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$439, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 3319 | //!Frame patient handling packet to be sent to processor               
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |3318| 
	.dwpsn	file "../source/CommInterface.c",line 3320,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3320 | CPFrameDataPacket(DELFINO_SYSTEM_SETTING_CMD, usnSubCmd, FALSE);       
;----------------------------------------------------------------------
        MOVB      XAR4,#0               ; [CPU_] |3320| 
        MOV       AL,#8200              ; [CPU_] |3320| 
        MOV       AH,*-SP[1]            ; [CPU_] |3320| 
$C$DW$440	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$440, DW_AT_low_pc(0x00)
	.dwattr $C$DW$440, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$440, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |3320| 
        ; call occurs [#_CPFrameDataPacket] ; [] |3320| 
	.dwpsn	file "../source/CommInterface.c",line 3321,column 1,is_stmt,isa 0
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$441	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$441, DW_AT_low_pc(0x00)
	.dwattr $C$DW$441, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$437, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$437, DW_AT_TI_end_line(0xcf9)
	.dwattr $C$DW$437, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$437

	.sect	".text:_CIProcessServiceHandlingCmd"
	.clink
	.global	_CIProcessServiceHandlingCmd

$C$DW$442	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$442, DW_AT_name("CIProcessServiceHandlingCmd")
	.dwattr $C$DW$442, DW_AT_low_pc(_CIProcessServiceHandlingCmd)
	.dwattr $C$DW$442, DW_AT_high_pc(0x00)
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_CIProcessServiceHandlingCmd")
	.dwattr $C$DW$442, DW_AT_external
	.dwattr $C$DW$442, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$442, DW_AT_TI_begin_line(0xcfe)
	.dwattr $C$DW$442, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$442, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommInterface.c",line 3327,column 1,is_stmt,address _CIProcessServiceHandlingCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessServiceHandlingCmd
$C$DW$443	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$443, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$443, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 3326 | void CIProcessServiceHandlingCmd(uint16_t usnMinorCmd)                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessServiceHandlingCmd  FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessServiceHandlingCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$444	.dwtag  DW_TAG_variable
	.dwattr $C$DW$444, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$444, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 3328 | //!Switch on the PWM of the HGB                                        
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |3327| 
	.dwpsn	file "../source/CommInterface.c",line 3329,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3329 | PWMHgbPwmControl(START, HGB_PWM_FREQUENCY);                            
; 3330 | //    sprintf(DebugPrintBuf, "\r\n CIProcessServiceHandlingCmd %d \n\n"
;     | , usnMinorCmd);                                                        
; 3331 | //    UartDebugPrint((int *)DebugPrintBuf, 50);                        
; 3333 | //!Check for the minor command of the command received from the process
;     | or                                                                     
;----------------------------------------------------------------------
        MOV       AH,#1000              ; [CPU_] |3329| 
        MOVB      AL,#1                 ; [CPU_] |3329| 
$C$DW$445	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$445, DW_AT_low_pc(0x00)
	.dwattr $C$DW$445, DW_AT_name("_PWMHgbPwmControl")
	.dwattr $C$DW$445, DW_AT_TI_call

        LCR       #_PWMHgbPwmControl    ; [CPU_] |3329| 
        ; call occurs [#_PWMHgbPwmControl] ; [] |3329| 
	.dwpsn	file "../source/CommInterface.c",line 3334,column 2,is_stmt,isa 0
;----------------------------------------------------------------------
; 3334 | switch (usnMinorCmd)                                                   
; 3336 | //!If the command received from processor is Rinse Prime,              
; 3337 | //!perform the below operations                                        
; 3338 |         case SITARA_PRIME_WITH_RINSE_CMD:                              
; 3339 |     //!Set the current mode to Rinse Prime which will be given         
; 3340 |     //!input to MBD to initiate Rinse Prime                            
;----------------------------------------------------------------------
        B         $C$L292,UNC           ; [CPU_] |3334| 
        ; branch occurs ; [] |3334| 
$C$L254:    
	.dwpsn	file "../source/CommInterface.c",line 3341,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3341 | g_usnCurrentMode = PRIME_WITH_RINSE_SERVICE_HANDLING;                  
; 3342 | //!Reset the reagent mon variable -HN                                  
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20481 ; [CPU_] |3341| 
	.dwpsn	file "../source/CommInterface.c",line 3343,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3343 | g_bMonRinsePriming = FALSE;                                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOV       @_g_bMonRinsePriming,#0 ; [CPU_] |3343| 
	.dwpsn	file "../source/CommInterface.c",line 3344,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3344 | break;                                                                 
; 3346 | //!If the command received from processor is Lyse Prime,               
; 3347 | //!perform the below operations                                        
; 3348 | case SITARA_PRIME_WITH_LYSE_CMD:                                       
; 3349 | //!Set the current mode to Lyse Prime which will be given              
; 3350 | //!input to MBD to initiate Lyse Prime                                 
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3344| 
        ; branch occurs ; [] |3344| 
$C$L255:    
	.dwpsn	file "../source/CommInterface.c",line 3351,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3351 | g_usnCurrentMode = PRIME_WITH_LYSE_SERVICE_HANDLING;                   
; 3352 | //!Reset the reagent mon variable -HN                                  
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20482 ; [CPU_] |3351| 
	.dwpsn	file "../source/CommInterface.c",line 3353,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3353 | g_bMonLysePriming = FALSE;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOV       @_g_bMonLysePriming,#0 ; [CPU_] |3353| 
	.dwpsn	file "../source/CommInterface.c",line 3354,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3354 | break;                                                                 
; 3356 | //!If the command received from processor is Diluent Prime,            
; 3357 | //!perform the below operations                                        
; 3358 | case SITARA_PRIME_WITH_DILUENT_CMD:                                    
; 3359 | //!Set the current mode to Diluent Prime which will be given           
; 3360 | //!input to MBD to initiate Diluent Prime                              
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3354| 
        ; branch occurs ; [] |3354| 
$C$L256:    
	.dwpsn	file "../source/CommInterface.c",line 3361,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3361 | g_usnCurrentMode = PRIME_WITH_DILUENT_SERVICE_HANDLING;                
; 3362 | //!Reset the reagent mon variable -HN                                  
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20483 ; [CPU_] |3361| 
	.dwpsn	file "../source/CommInterface.c",line 3363,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3363 | g_bMonDiluentPriming = FALSE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOV       @_g_bMonDiluentPriming,#0 ; [CPU_] |3363| 
	.dwpsn	file "../source/CommInterface.c",line 3364,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3364 | break;                                                                 
; 3366 | //!If the command received from processor is EZ Cleaner,               
; 3367 | //!perform the below operations                                        
; 3368 | case SITARA_PRIME_WITH_EZ_CLEANSER_CMD:                                
; 3369 | //!Set the current mode to EZ Cleaner which will be given              
; 3370 | //!input to MBD to initiate EZ Cleaner                                 
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3364| 
        ; branch occurs ; [] |3364| 
$C$L257:    
	.dwpsn	file "../source/CommInterface.c",line 3371,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3371 | g_usnCurrentMode = PRIME_WITH_EZ_CLEANSER_CMD;                         
; 3372 | //g_bStartButtonStatus = TRUE;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20484 ; [CPU_] |3371| 
	.dwpsn	file "../source/CommInterface.c",line 3374,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3374 | break;                                                                 
; 3376 | //!If the command received from processor is Prime All,                
; 3377 | //!perform the below operations                                        
; 3378 | case SITARA_PRIME_ALL_CMD:                                             
; 3379 | //!Set the current mode to Prime All which will be given               
; 3380 | //!input to MBD to initiate Prime All                                  
; 3381 | //sprintf(DebugPrintBuf, "\r\n SITARA_PRIME_ALL_CMD \n");              
; 3382 | ///UartDebugPrint((int *)DebugPrintBuf, 50);                           
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3374| 
        ; branch occurs ; [] |3374| 
$C$L258:    
	.dwpsn	file "../source/CommInterface.c",line 3383,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3383 | g_usnCurrentMode = PRIME_ALL_SERVICE_HANDLING;                         
; 3384 | //!Reset the reagent mon variables -HN                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20485 ; [CPU_] |3383| 
	.dwpsn	file "../source/CommInterface.c",line 3385,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3385 | g_bMonDiluentPriming = FALSE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOV       @_g_bMonDiluentPriming,#0 ; [CPU_] |3385| 
	.dwpsn	file "../source/CommInterface.c",line 3386,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3386 | g_bMonLysePriming = FALSE;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOV       @_g_bMonLysePriming,#0 ; [CPU_] |3386| 
	.dwpsn	file "../source/CommInterface.c",line 3387,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3387 | g_bMonRinsePriming = FALSE;                                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOV       @_g_bMonRinsePriming,#0 ; [CPU_] |3387| 
	.dwpsn	file "../source/CommInterface.c",line 3388,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3388 | break;                                                                 
; 3390 | //!If the command received from processor is Back Flush / Flush Apertur
;     | e,                                                                     
; 3391 | //!perform the below operations                                        
; 3392 | case SITARA_BACK_FLUSH_CMD:                                            
; 3393 | //!Set the current mode to Back Flush / Flush Aperture which will be gi
;     | ven                                                                    
; 3394 | //!input to MBD to initiate Back Flush / Flush Aperture                
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3388| 
        ; branch occurs ; [] |3388| 
$C$L259:    
	.dwpsn	file "../source/CommInterface.c",line 3395,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3395 | g_usnCurrentMode = BACK_FLUSH_SERVICE_HANDLING;                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20486 ; [CPU_] |3395| 
	.dwpsn	file "../source/CommInterface.c",line 3396,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3396 | break;                                                                 
; 3398 | //!If the command received from processor is ZAP Aperture,             
; 3399 | //!perform the below operations                                        
; 3400 | case SITARA_ZAP_APERTURE:                                              
; 3401 | //!Set the current mode to ZAP Aperture which will be given            
; 3402 | //!input to MBD to initiate ZAP Aperture                               
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3396| 
        ; branch occurs ; [] |3396| 
$C$L260:    
	.dwpsn	file "../source/CommInterface.c",line 3403,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3403 | g_usnCurrentMode = ZAP_APERTURE_SERVICE_HANDLING;                      
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20488 ; [CPU_] |3403| 
	.dwpsn	file "../source/CommInterface.c",line 3404,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3404 | break;                                                                 
; 3406 | //!If the command received from processor is Drain Bath,               
; 3407 | //!perform the below operations                                        
; 3408 |     case SITARA_DRAIN_BATH:                                            
; 3409 | //!Set the current mode to Drain Bath which will be given              
; 3410 | //!input to MBD to initiate Drain Bath                                 
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3404| 
        ; branch occurs ; [] |3404| 
$C$L261:    
	.dwpsn	file "../source/CommInterface.c",line 3411,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3411 | g_usnCurrentMode = DRAIN_BATH_SERVICE_HANDLING;                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20489 ; [CPU_] |3411| 
	.dwpsn	file "../source/CommInterface.c",line 3412,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3412 | break;                                                                 
; 3414 | //!If the command received from processor is Drain All,                
; 3415 | //!perform the below operations                                        
; 3416 |     case SITARA_DRAIN_ALL:                                             
; 3417 | //!Set the current mode to Drain All which will be given               
; 3418 | //!input to MBD to initiate Drain All                                  
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3412| 
        ; branch occurs ; [] |3412| 
$C$L262:    
	.dwpsn	file "../source/CommInterface.c",line 3419,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3419 | g_usnCurrentMode = DRAIN_ALL_SERVICE_HANDLING;                         
; 3420 | //!Reset the reagent mon variables -HN                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20490 ; [CPU_] |3419| 
	.dwpsn	file "../source/CommInterface.c",line 3421,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3421 | g_bMonDiluentPriming = FALSE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOV       @_g_bMonDiluentPriming,#0 ; [CPU_] |3421| 
	.dwpsn	file "../source/CommInterface.c",line 3422,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3422 | g_bMonLysePriming = FALSE;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOV       @_g_bMonLysePriming,#0 ; [CPU_] |3422| 
	.dwpsn	file "../source/CommInterface.c",line 3423,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3423 | g_bMonRinsePriming = FALSE;                                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOV       @_g_bMonRinsePriming,#0 ; [CPU_] |3423| 
	.dwpsn	file "../source/CommInterface.c",line 3424,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3424 | break;                                                                 
; 3426 | //!If the command received from processor is Clean Bath,               
; 3427 | //!perform the below operations                                        
; 3428 | case SITARA_CLEAN_BATH:                                                
; 3429 | //!Set the current mode to Clean Bath which will be given              
; 3430 | //!input to MBD to initiate Clean Bath                                 
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3424| 
        ; branch occurs ; [] |3424| 
$C$L263:    
	.dwpsn	file "../source/CommInterface.c",line 3432,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3432 | g_usnCurrentMode = CLEAN_BATH_SERVICE_HANDLING;                        
; 3433 | //g_usnCurrentMode = MBD_VOL_TUBE_EMPTY; //added by jojo               
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20491 ; [CPU_] |3432| 
	.dwpsn	file "../source/CommInterface.c",line 3434,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3434 | break;                                                                 
; 3435 | case SITARA_GLASSTUBE_FILLING:                                         
; 3436 | //JOJO added                                                           
; 3437 | //!Set the current mode to Clean Bath which will be given              
; 3438 | //!input to MBD to initiate Clean Bath                                 
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3434| 
        ; branch occurs ; [] |3434| 
$C$L264:    
	.dwpsn	file "../source/CommInterface.c",line 3440,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3440 | g_usnCurrentMode = GLASS_TUBE_FILLING;                                 
; 3441 | //g_usnCurrentMode = MBD_VOL_TUBE_EMPTY;                               
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20542 ; [CPU_] |3440| 
	.dwpsn	file "../source/CommInterface.c",line 3442,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3442 | break;                                                                 
; 3443 | case SITARA_COUNT_FAIL:                                                
; 3444 | //JOJO Added                                                           
; 3445 | //!Set the current mode to Clean Bath which will be given              
; 3446 | //!input to MBD to initiate Clean Bath                                 
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3442| 
        ; branch occurs ; [] |3442| 
$C$L265:    
	.dwpsn	file "../source/CommInterface.c",line 3447,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3447 | g_usnCurrentMode = COUNT_FAIL;                                         
; 3448 | //g_usnCurrentMode = MBD_VOL_TUBE_EMPTY;                               
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20541 ; [CPU_] |3447| 
	.dwpsn	file "../source/CommInterface.c",line 3449,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3449 | break;                                                                 
; 3450 | case SITARA_HYPO_CLEANER_FAILED:                                       
; 3451 | //JOJO added                                                           
; 3452 | //!Set the current mode to Clean Bath which will be given              
; 3453 | //!input to MBD to initiate Clean Bath                                 
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3449| 
        ; branch occurs ; [] |3449| 
$C$L266:    
	.dwpsn	file "../source/CommInterface.c",line 3454,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3454 | SysInitProbeCleanArrestSequence(TRUE);                                 
;----------------------------------------------------------------------
        MOVB      AL,#1                 ; [CPU_] |3454| 
$C$DW$446	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$446, DW_AT_low_pc(0x00)
	.dwattr $C$DW$446, DW_AT_name("_SysInitProbeCleanArrestSequence")
	.dwattr $C$DW$446, DW_AT_TI_call

        LCR       #_SysInitProbeCleanArrestSequence ; [CPU_] |3454| 
        ; call occurs [#_SysInitProbeCleanArrestSequence] ; [] |3454| 
	.dwpsn	file "../source/CommInterface.c",line 3455,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3455 | g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4100 ; [CPU_] |3455| 
	.dwpsn	file "../source/CommInterface.c",line 3456,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3456 | g_usnCurrentMode = HYPO_CLEANER_FAIL;                                  
;----------------------------------------------------------------------
        MOV       @_g_usnCurrentMode,#20543 ; [CPU_] |3456| 
	.dwpsn	file "../source/CommInterface.c",line 3457,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3457 | break;                                                                 
; 3459 | //!If the command received from processor is Valve Test to test specifi
;     | c                                                                      
; 3460 | //!valve indicated by processor, perform the below operations          
; 3461 |     case SITARA_VALVE_TEST_CMD:                                        
; 3462 | //!Set the current mode to Valve Test and specific valve will be excite
;     | d                                                                      
; 3463 | //!to check its operation                                              
; 3464 | //sprintf(DebugPrintBuf, "\r\n SITARA_VALVE_TEST_CMD \n");             
; 3465 | //UartDebugPrint((int *)DebugPrintBuf, 50);                            
; 3466 |         //!Set the counter to excite the valve to particular time      
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3457| 
        ; branch occurs ; [] |3457| 
$C$L267:    
	.dwpsn	file "../source/CommInterface.c",line 3467,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 3467 | m_ucValveControlTime = VALVE_CONTROL_TIME;                             
;----------------------------------------------------------------------
        MOVW      DP,#_m_ucValveControlTime ; [CPU_U] 
        MOVB      @_m_ucValveControlTime,#20,UNC ; [CPU_] |3467| 
	.dwpsn	file "../source/CommInterface.c",line 3468,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3468 | SysInitValveTest((unsigned char)m_stReceivedPacketFrame.arrusnData[0]);
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+6 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+6 ; [CPU_] |3468| 
$C$DW$447	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$447, DW_AT_low_pc(0x00)
	.dwattr $C$DW$447, DW_AT_name("_SysInitValveTest")
	.dwattr $C$DW$447, DW_AT_TI_call

        LCR       #_SysInitValveTest    ; [CPU_] |3468| 
        ; call occurs [#_SysInitValveTest] ; [] |3468| 
	.dwpsn	file "../source/CommInterface.c",line 3469,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3469 | break;                                                                 
; 3471 | //!If the command received from processor is to get the sytem status,  
; 3472 | //!perform the below operations                                        
; 3473 | case SITARA_SYSTEM_STATUS_CMD:                                         
; 3474 | //            sprintf(DebugPrintBuf, "\r\n SITARA_SYSTEM_STATUS_CMD Rec
;     | eiced\n");                                                             
; 3475 | //            UartDebugPrint((int *)DebugPrintBuf, 50);                
; 3476 | //!Packetize the system status and send to processor                   
; 3477 | //CIFormServiceHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);  
; 3478 | //! Set the flag to indicate the system status command is recieved     
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3469| 
        ; branch occurs ; [] |3469| 
$C$L268:    
	.dwpsn	file "../source/CommInterface.c",line 3479,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 3479 | m_bSystemStatusFlag = TRUE;                                            
; 3480 |     //!System status is requested only when the controller is not      
; 3481 |     //!running any sequence, after sending the system status, pull the 
; 3482 |     //!busy pin low                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_m_bSystemStatusFlag ; [CPU_U] 
        MOVB      @_m_bSystemStatusFlag,#1,UNC ; [CPU_] |3479| 
	.dwpsn	file "../source/CommInterface.c",line 3483,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3483 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$448	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$448, DW_AT_low_pc(0x00)
	.dwattr $C$DW$448, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$448, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |3483| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |3483| 
	.dwpsn	file "../source/CommInterface.c",line 3484,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3484 | Counter = 0;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_Counter          ; [CPU_U] 
        MOV       @_Counter,#0          ; [CPU_] |3484| 
	.dwpsn	file "../source/CommInterface.c",line 3485,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3485 | break;                                                                 
; 3487 | //!If the command received from processor is waste bin replace command,
; 3488 | //!perform the below operations                                        
; 3489 | case SITARA_WASTE_BIN_REPLACED_CMD:                                    
; 3490 | //!Do not monitor the waste bin once the waste bin is detected.        
; 3491 | //!Processor shall send commnad indicating waste bin is replaced.      
; 3492 | //!Start monitoring the waste bin after replaing the waste bin.        
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3485| 
        ; branch occurs ; [] |3485| 
$C$L269:    
	.dwpsn	file "../source/CommInterface.c",line 3493,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3493 | SysInitWasteBinReplaced();                                             
;----------------------------------------------------------------------
$C$DW$449	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$449, DW_AT_low_pc(0x00)
	.dwattr $C$DW$449, DW_AT_name("_SysInitWasteBinReplaced")
	.dwattr $C$DW$449, DW_AT_TI_call

        LCR       #_SysInitWasteBinReplaced ; [CPU_] |3493| 
        ; call occurs [#_SysInitWasteBinReplaced] ; [] |3493| 
	.dwpsn	file "../source/CommInterface.c",line 3495,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3495 | sprintf(DebugPrintBuf, "\r\n SITARA_WASTE_BIN_REPLACED_CMD received \n"
;     | );                                                                     
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL58        ; [CPU_U] |3495| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3495| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3495| 
$C$DW$450	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$450, DW_AT_low_pc(0x00)
	.dwattr $C$DW$450, DW_AT_name("_sprintf")
	.dwattr $C$DW$450, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |3495| 
        ; call occurs [#_sprintf] ; [] |3495| 
	.dwpsn	file "../source/CommInterface.c",line 3496,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3496 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 3497 | //!Clear the busy pin after recieving the waste bin.                   
; 3498 | //!So that processor can send any command to initiate next sequence    
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |3496| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3496| 
$C$DW$451	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$451, DW_AT_low_pc(0x00)
	.dwattr $C$DW$451, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$451, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |3496| 
        ; call occurs [#_UartDebugPrint] ; [] |3496| 
	.dwpsn	file "../source/CommInterface.c",line 3499,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3499 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$452	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$452, DW_AT_low_pc(0x00)
	.dwattr $C$DW$452, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$452, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |3499| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |3499| 
	.dwpsn	file "../source/CommInterface.c",line 3500,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3500 | break;                                                                 
; 3502 | //!If the command received from processor is Prime with Rinse,         
; 3503 | //!perform the below operations                                        
; 3504 | case SITARA_START_UP_SEQUENCE_CMD:                                     
; 3505 | //!Set the current mode to Start up sequence which will be given       
; 3506 | //!input to MBD to initiate start up sequence                          
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3500| 
        ; branch occurs ; [] |3500| 
$C$L270:    
	.dwpsn	file "../source/CommInterface.c",line 3507,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3507 | g_usnCurrentMode = START_UP_SEQUENCE_MODE;                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20500 ; [CPU_] |3507| 
	.dwpsn	file "../source/CommInterface.c",line 3508,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3508 | break;                                                                 
; 3510 | //!If the command received from processor is Head Rinse,               
; 3511 | //!perform the below operations                                        
; 3512 | case SITARA_HEAD_RINSING_CMD:                                          
; 3513 | //!Set the current mode to Head Rinse which will be given              
; 3514 | //!input to MBD to initiate Head Rinse                                 
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3508| 
        ; branch occurs ; [] |3508| 
$C$L271:    
	.dwpsn	file "../source/CommInterface.c",line 3515,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3515 | g_usnCurrentMode = HEAD_RINSING_SERVICE_HANDLING;                      
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20501 ; [CPU_] |3515| 
	.dwpsn	file "../source/CommInterface.c",line 3516,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3516 | break;                                                                 
; 3518 | //!If the command received from processor is Probe Cleaning,           
; 3519 | //!perform the below operations                                        
; 3520 | case SITARA_PROBE_CLEANING_CMD:                                        
; 3521 | //!Set the current mode to Probe Cleaning Prime which will be given    
; 3522 | //!input to MBD to initiate Probe Cleaning                             
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3516| 
        ; branch occurs ; [] |3516| 
$C$L272:    
	.dwpsn	file "../source/CommInterface.c",line 3523,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3523 | g_usnCurrentMode = PROBE_CLEANING_SERVICE_HANDLING;                    
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20502 ; [CPU_] |3523| 
	.dwpsn	file "../source/CommInterface.c",line 3524,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3524 | break;                                                                 
; 3526 | //!If the command received from processor is X-Motor Check,            
; 3527 | //!perform the below operations                                        
; 3528 | case SITARA_X_AXIS_MOTOR_CHECK_CMD:                                    
; 3529 | //!Set the current mode to X-Motor Check which will be given           
; 3530 | //!input to MBD to initiate X-Motor Check                              
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3524| 
        ; branch occurs ; [] |3524| 
$C$L273:    
	.dwpsn	file "../source/CommInterface.c",line 3531,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3531 | g_usnCurrentMode = X_AXIS_MOTOR_CHECK;                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20505 ; [CPU_] |3531| 
	.dwpsn	file "../source/CommInterface.c",line 3532,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3532 | break;                                                                 
; 3534 | //!If the command received from processor is Y-Motor Check,            
; 3535 | //!perform the below operations                                        
; 3536 | case SITARA_Y_AXIS_MOTOR_CHECK_CMD:                                    
; 3537 | //!Set the current mode to Y-Motor Check which will be given           
; 3538 | //!input to MBD to initiate Y-Motor Check                              
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3532| 
        ; branch occurs ; [] |3532| 
$C$L274:    
	.dwpsn	file "../source/CommInterface.c",line 3539,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3539 | g_usnCurrentMode = Y_AXIS_MOTOR_CHECK;                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20506 ; [CPU_] |3539| 
	.dwpsn	file "../source/CommInterface.c",line 3540,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3540 | break;                                                                 
; 3542 | //!If the command received from processor is Diluent Motor Check,      
; 3543 | //!perform the below operations                                        
; 3544 | case SITARA_DILUENT_SYRINGE_MOTOR_CHECK_CMD:                           
; 3545 | //!Set the current mode to Diluent Motor Check which will be given     
; 3546 | //!input to MBD to initiate Diluent Motor Check                        
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3540| 
        ; branch occurs ; [] |3540| 
$C$L275:    
	.dwpsn	file "../source/CommInterface.c",line 3547,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3547 | g_usnCurrentMode = DILUENT_SYRINGE_MOTOR_CHECK;                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20507 ; [CPU_] |3547| 
	.dwpsn	file "../source/CommInterface.c",line 3548,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3548 | break;                                                                 
; 3550 | //!If the command received from processor is Waste Motor Check,        
; 3551 | //!perform the below operations                                        
; 3552 | case SITARA_WASTE_SYRINGE_MOTOR_CHECK_CMD:                             
; 3553 | //!Set the current mode to Waste Motor Check which will be given       
; 3554 | //!input to MBD to initiate Waste Motor Check                          
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3548| 
        ; branch occurs ; [] |3548| 
$C$L276:    
	.dwpsn	file "../source/CommInterface.c",line 3555,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3555 | g_usnCurrentMode = WASTE_SYRINGE_MOTOR_CHECK;                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20508 ; [CPU_] |3555| 
	.dwpsn	file "../source/CommInterface.c",line 3556,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3556 | break;                                                                 
; 3558 | //!If the command received from processor is Sample Motor Check,       
; 3559 | //!perform the below operations                                        
; 3560 | case SITARA_SAMPLE_LYSE_SYRINGE_MOTOR_CHECK_CMD:                       
; 3561 | //!Set the current mode to Sample Motor Check which will be given      
; 3562 | //!input to MBD to initiate Sample Motor Check                         
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3556| 
        ; branch occurs ; [] |3556| 
$C$L277:    
	.dwpsn	file "../source/CommInterface.c",line 3563,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3563 | g_usnCurrentMode = SAMPLE_LYSE_SYRINGE_MOTOR_CHECK;                    
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20509 ; [CPU_] |3563| 
	.dwpsn	file "../source/CommInterface.c",line 3564,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3564 | break;                                                                 
; 3566 | //!If the command received from processor is Counting Time Sequence,   
; 3567 | //!perform the below operations                                        
; 3568 | case SITARA_COUNTING_TIME_SEQUENCE_START_CMD:                          
; 3569 | //!Set the current mode to Counting Time Sequence which will be given  
; 3570 | //!input to MBD to initiate Counting Time Sequence                     
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3564| 
        ; branch occurs ; [] |3564| 
$C$L278:    
	.dwpsn	file "../source/CommInterface.c",line 3571,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3571 | g_usnCurrentMode = COUNTING_TIME_SEQUENCE;                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20499 ; [CPU_] |3571| 
	.dwpsn	file "../source/CommInterface.c",line 3572,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3572 | break;                                                                 
; 3574 | //!If the command received from processor is Wake Up Sequence,         
; 3575 | //!perform the below operations                                        
; 3576 | case SITARA_WAKE_UP_SEQUENCE_CMD:                                      
; 3578 | //!Set the current mode to Wake Up Sequence which will be given        
; 3579 | //!input to MBD to initiate Wake Up Sequence                           
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3572| 
        ; branch occurs ; [] |3572| 
$C$L279:    
	.dwpsn	file "../source/CommInterface.c",line 3580,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 3580 | g_usnCurrentMode = MBD_WAKEUP_START;                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20521 ; [CPU_] |3580| 
	.dwpsn	file "../source/CommInterface.c",line 3581,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 3581 | break;                                                                 
; 3583 | //!If the command received from processor is request for Home position 
; 3584 | //!status, perform the below operations                                
; 3585 | case SITARA_HOME_POSITION_STATUS_CMD:                                  
; 3586 | //!Packetize the current Home position status of the motors and        
; 3587 | //!send to processor                                                   
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3581| 
        ; branch occurs ; [] |3581| 
$C$L280:    
	.dwpsn	file "../source/CommInterface.c",line 3588,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3588 | CPFrameDataPacket(DELFINO_SERVICE_HANDLING, \                          
; 3589 |         DELFINO_HOME_POSITION_SENSOR_CHECK, FALSE);                    
; 3590 | //!Current Home position status of the motors is requested only        
; 3591 | //!when the controller is not running any sequence, after sending      
; 3592 | //!pull the busy pin low                                               
;----------------------------------------------------------------------
        MOV       AL,#8196              ; [CPU_] |3588| 
        MOVB      XAR4,#0               ; [CPU_] |3588| 
        MOV       AH,#8232              ; [CPU_] |3588| 
$C$DW$453	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$453, DW_AT_low_pc(0x00)
	.dwattr $C$DW$453, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$453, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |3588| 
        ; call occurs [#_CPFrameDataPacket] ; [] |3588| 
	.dwpsn	file "../source/CommInterface.c",line 3593,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3593 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$454	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$454, DW_AT_low_pc(0x00)
	.dwattr $C$DW$454, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$454, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |3593| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |3593| 
	.dwpsn	file "../source/CommInterface.c",line 3594,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 3594 | break;                                                                 
; 3595 | case SITARA_START_TOSHIP_SEQUENCE_CMD:                                 
; 3596 | //!Set the current mode to ship which will be given                    
; 3597 | //!input to MBD to initiate to ship sequence                           
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3594| 
        ; branch occurs ; [] |3594| 
$C$L281:    
	.dwpsn	file "../source/CommInterface.c",line 3598,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 3598 | g_usnCurrentMode = MBD_TO_SHIP_SEQUENCE_START;                         
; 3599 | //!Reset the reagent mon variables -HN                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20522 ; [CPU_] |3598| 
	.dwpsn	file "../source/CommInterface.c",line 3600,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 3600 | g_bMonDiluentPriming = FALSE;                                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonDiluentPriming ; [CPU_U] 
        MOV       @_g_bMonDiluentPriming,#0 ; [CPU_] |3600| 
	.dwpsn	file "../source/CommInterface.c",line 3601,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3601 | g_bMonLysePriming = FALSE;                                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonLysePriming ; [CPU_U] 
        MOV       @_g_bMonLysePriming,#0 ; [CPU_] |3601| 
	.dwpsn	file "../source/CommInterface.c",line 3602,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3602 | g_bMonRinsePriming = FALSE;                                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_bMonRinsePriming ; [CPU_U] 
        MOV       @_g_bMonRinsePriming,#0 ; [CPU_] |3602| 
	.dwpsn	file "../source/CommInterface.c",line 3603,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 3603 | break;                                                                 
; 3606 | case SITARA_VOLUMETRIC_CHECK_CMD:                                      
; 3607 | //!Set the current mode to volumetric board check which will be given  
; 3608 | //!input to MBD to initiate to check volumetric sensors                
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3603| 
        ; branch occurs ; [] |3603| 
$C$L282:    
	.dwpsn	file "../source/CommInterface.c",line 3609,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3609 | g_usnCurrentMode = MBD_VOLUMETRIC_BOARD_CHECK;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20526 ; [CPU_] |3609| 
	.dwpsn	file "../source/CommInterface.c",line 3610,column 7,is_stmt,isa 0
;----------------------------------------------------------------------
; 3610 | break;                                                                 
; 3612 | case SITARA_VOLUMETRIC_TUBE_CLEAN_CMD:       //HN_Added                
; 3613 | //!Set the current mode to clean volumetric which will be given        
; 3614 | //!input to MBD to initiate to clean volumetric                        
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3610| 
        ; branch occurs ; [] |3610| 
$C$L283:    
	.dwpsn	file "../source/CommInterface.c",line 3615,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3615 | g_usnCurrentMode = MBD_VOL_TUBE_EMPTY;                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20534 ; [CPU_] |3615| 
	.dwpsn	file "../source/CommInterface.c",line 3616,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3616 | break;                                                                 
; 3618 | case SITARA_SLEEP_SEQUENCE_CMD:                                        
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3616| 
        ; branch occurs ; [] |3616| 
$C$L284:    
	.dwpsn	file "../source/CommInterface.c",line 3619,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3619 | sprintf(DebugPrintBuf, "\r\n SITARA_SLEEP_SEQUENCE_CMD \n");           
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL59        ; [CPU_U] |3619| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3619| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3619| 
$C$DW$455	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$455, DW_AT_low_pc(0x00)
	.dwattr $C$DW$455, DW_AT_name("_sprintf")
	.dwattr $C$DW$455, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |3619| 
        ; call occurs [#_sprintf] ; [] |3619| 
	.dwpsn	file "../source/CommInterface.c",line 3620,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3620 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 3621 | //!Set the current mode to Sleep  which will be given                  
; 3622 | //!input to MBD to initiate sleep sequence                             
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |3620| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3620| 
$C$DW$456	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$456, DW_AT_low_pc(0x00)
	.dwattr $C$DW$456, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$456, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |3620| 
        ; call occurs [#_UartDebugPrint] ; [] |3620| 
	.dwpsn	file "../source/CommInterface.c",line 3623,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3623 | g_usnCurrentMode = MBD_SLEEP_PROCESS;                                  
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20527 ; [CPU_] |3623| 
	.dwpsn	file "../source/CommInterface.c",line 3624,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3624 | break;                                                                 
; 3626 | case SITARA_ALL_MOTOR_Y_AXIS_CMD:                                      
; 3627 | //!Set the current mode to Sleep  which will be given                  
; 3628 | //!input to MBD to initiate sleep sequence                             
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3624| 
        ; branch occurs ; [] |3624| 
$C$L285:    
	.dwpsn	file "../source/CommInterface.c",line 3629,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3629 | g_usnCurrentMode = MBD_ALL_MOTOR_Y_AXIS_CHECK;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20528 ; [CPU_] |3629| 
	.dwpsn	file "../source/CommInterface.c",line 3630,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3630 | break;                                                                 
; 3632 | case SITARA_ALL_MOTOR_X_AXIS_CMD:                                      
; 3633 | //!Set the current mode to Sleep  which will be given                  
; 3634 | //!input to MBD to initiate sleep sequence                             
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3630| 
        ; branch occurs ; [] |3630| 
$C$L286:    
	.dwpsn	file "../source/CommInterface.c",line 3635,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3635 | g_usnCurrentMode = MBD_ALL_MOTOR_X_AXIS_CHECK;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20529 ; [CPU_] |3635| 
	.dwpsn	file "../source/CommInterface.c",line 3636,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3636 | break;                                                                 
; 3638 | case SITARA_ALL_MOTOR_SAMPLE_CMD:                                      
; 3639 | //!Set the current mode to Sleep  which will be given                  
; 3640 | //!input to MBD to initiate sleep sequence                             
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3636| 
        ; branch occurs ; [] |3636| 
$C$L287:    
	.dwpsn	file "../source/CommInterface.c",line 3641,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3641 | g_usnCurrentMode = MBD_ALL_MOTOR_SAMPLE_CHECK;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20530 ; [CPU_] |3641| 
	.dwpsn	file "../source/CommInterface.c",line 3642,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3642 | break;                                                                 
; 3644 | case SITARA_ALL_MOTOR_DILUENT_CMD:                                     
; 3645 | //!Set the current mode to Sleep  which will be given                  
; 3646 | //!input to MBD to initiate sleep sequence                             
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3642| 
        ; branch occurs ; [] |3642| 
$C$L288:    
	.dwpsn	file "../source/CommInterface.c",line 3647,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3647 | g_usnCurrentMode = MBD_ALL_MOTOR_DILUENT_CHECK;                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20531 ; [CPU_] |3647| 
	.dwpsn	file "../source/CommInterface.c",line 3648,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3648 | break;                                                                 
; 3650 | case SITARA_ALL_MOTOR_WASTE_CMD:                                       
; 3651 | //!Set the current mode to Sleep  which will be given                  
; 3652 | //!input to MBD to initiate sleep sequence                             
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3648| 
        ; branch occurs ; [] |3648| 
$C$L289:    
	.dwpsn	file "../source/CommInterface.c",line 3653,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3653 | g_usnCurrentMode = MBD_ALL_MOTOR_WASTE_CHECK;                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20532 ; [CPU_] |3653| 
	.dwpsn	file "../source/CommInterface.c",line 3654,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3654 | break;                                                                 
; 3655 | case SITARA_BATH_FILL_CMD:                                             
; 3656 | //!Set the current mode to bath fill  which will be given              
; 3657 | //!input to MBD to initiate bath filling sequence                      
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3654| 
        ; branch occurs ; [] |3654| 
$C$L290:    
	.dwpsn	file "../source/CommInterface.c",line 3658,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3658 | g_usnCurrentMode = MBD_BATH_FILL_CMD;                                  
; 3659 | //sprintf(DebugPrintBuf, "\r\n SITARA_BATH_FILL_CMD \n");              
; 3660 | //UartDebugPrint((int *)DebugPrintBuf, 50);                            
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20536 ; [CPU_] |3658| 
	.dwpsn	file "../source/CommInterface.c",line 3661,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3661 | break;                                                                 
; 3662 | case SITARA_INITIATE_ZAP:                                              
; 3663 | //!Set the current mode to Zap initiate                                
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3661| 
        ; branch occurs ; [] |3661| 
$C$L291:    
	.dwpsn	file "../source/CommInterface.c",line 3664,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3664 | g_usnCurrentMode = MBD_ZAP_INITIATE;                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20539 ; [CPU_] |3664| 
	.dwpsn	file "../source/CommInterface.c",line 3665,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3665 | sprintf(DebugPrintBuf, "\r\n SITARA_INITIATE_ZAP Receiced\n");         
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL60        ; [CPU_U] |3665| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3665| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3665| 
$C$DW$457	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$457, DW_AT_low_pc(0x00)
	.dwattr $C$DW$457, DW_AT_name("_sprintf")
	.dwattr $C$DW$457, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |3665| 
        ; call occurs [#_sprintf] ; [] |3665| 
	.dwpsn	file "../source/CommInterface.c",line 3666,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3666 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 3667 | //m_bZapInitiateFlag =  true;                                          
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |3666| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3666| 
$C$DW$458	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$458, DW_AT_low_pc(0x00)
	.dwattr $C$DW$458, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$458, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |3666| 
        ; call occurs [#_UartDebugPrint] ; [] |3666| 
	.dwpsn	file "../source/CommInterface.c",line 3668,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3668 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$459	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$459, DW_AT_low_pc(0x00)
	.dwattr $C$DW$459, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$459, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |3668| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |3668| 
	.dwpsn	file "../source/CommInterface.c",line 3669,column 13,is_stmt,isa 0
;----------------------------------------------------------------------
; 3669 | break;                                                                 
; 3670 | //default case                                                         
; 3671 |     default :                                                          
; 3672 |             break;                                                     
; 3674 | }//end of switch case                                                  
;----------------------------------------------------------------------
        B         $C$L293,UNC           ; [CPU_] |3669| 
        ; branch occurs ; [] |3669| 
$C$L292:    
	.dwpsn	file "../source/CommInterface.c",line 3334,column 2,is_stmt,isa 0
        MOV       AH,*-SP[3]            ; [CPU_] |3334| 
        MOV       AL,AH                 ; [CPU_] |3334| 
        SUB       AL,#4097              ; [CPU_] |3334| 
        CMPB      AL,#44                ; [CPU_] |3334| 
        B         $C$L293,HI            ; [CPU_] |3334| 
        ; branchcc occurs ; [] |3334| 
        MOV       AL,AH                 ; [CPU_] |3334| 
        SUB       AL,#4097              ; [CPU_] |3334| 
        MOV       ACC,AL << #1          ; [CPU_] |3334| 
        MOVZ      AR6,AL                ; [CPU_] |3334| 
        MOVL      XAR7,#$C$SW15         ; [CPU_U] |3334| 
        MOVL      ACC,XAR7              ; [CPU_] |3334| 
        ADDU      ACC,AR6               ; [CPU_] |3334| 
        MOVL      XAR7,ACC              ; [CPU_] |3334| 
        MOVL      XAR7,*XAR7            ; [CPU_] |3334| 
        LB        *XAR7                 ; [CPU_] |3334| 
        ; branch occurs ; [] |3334| 
	.sect	".switch:_CIProcessServiceHandlingCmd"
	.clink
$C$SW15:	.long	$C$L254	; 4097
	.long	$C$L255	; 4098
	.long	$C$L256	; 4099
	.long	$C$L258	; 4100
	.long	$C$L259	; 4101
	.long	$C$L257	; 4102
	.long	$C$L260	; 4103
	.long	$C$L261	; 4104
	.long	$C$L262	; 4105
	.long	$C$L263	; 4106
	.long	$C$L267	; 4107
	.long	$C$L268	; 4108
	.long	$C$L269	; 4109
	.long	$C$L270	; 4110
	.long	$C$L271	; 4111
	.long	$C$L272	; 4112
	.long	$C$L273	; 4113
	.long	$C$L274	; 4114
	.long	$C$L275	; 4115
	.long	$C$L276	; 4116
	.long	$C$L277	; 4117
	.long	$C$L278	; 4118
	.long	$C$L281	; 4119
	.long	$C$L280	; 4120
	.long	$C$L279	; 4121
	.long	$C$L293	; 0
	.long	$C$L293	; 0
	.long	$C$L282	; 4124
	.long	$C$L284	; 4125
	.long	$C$L285	; 4126
	.long	$C$L286	; 4127
	.long	$C$L287	; 4128
	.long	$C$L288	; 4129
	.long	$C$L289	; 4130
	.long	$C$L283	; 4131
	.long	$C$L290	; 4132
	.long	$C$L293	; 0
	.long	$C$L293	; 0
	.long	$C$L293	; 0
	.long	$C$L293	; 0
	.long	$C$L293	; 0
	.long	$C$L291	; 4138
	.long	$C$L264	; 4139
	.long	$C$L265	; 4140
	.long	$C$L266	; 4141
	.sect	".text:_CIProcessServiceHandlingCmd"
$C$L293:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$460	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$460, DW_AT_low_pc(0x00)
	.dwattr $C$DW$460, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$442, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$442, DW_AT_TI_end_line(0xe5b)
	.dwattr $C$DW$442, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$442

	.sect	".text:_CIProcessShutdownCmd"
	.clink
	.global	_CIProcessShutdownCmd

$C$DW$461	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$461, DW_AT_name("CIProcessShutdownCmd")
	.dwattr $C$DW$461, DW_AT_low_pc(_CIProcessShutdownCmd)
	.dwattr $C$DW$461, DW_AT_high_pc(0x00)
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_CIProcessShutdownCmd")
	.dwattr $C$DW$461, DW_AT_external
	.dwattr $C$DW$461, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$461, DW_AT_TI_begin_line(0xe5c)
	.dwattr $C$DW$461, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$461, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 3677,column 1,is_stmt,address _CIProcessShutdownCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessShutdownCmd
$C$DW$462	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$462, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$462, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 3676 | void CIProcessShutdownCmd(uint16_t usnMinorCmd)                        
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessShutdownCmd         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessShutdownCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$463	.dwtag  DW_TAG_variable
	.dwattr $C$DW$463, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$463, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 3678 | //!Check for the minor command. If the minor command is Shutdown sequen
;     | ce,                                                                    
; 3679 | //!set the current mode to shutdown sequence and enable the start butto
;     | n                                                                      
; 3680 | //!to aspirate the EZ Cleanser.                                        
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |3677| 
	.dwpsn	file "../source/CommInterface.c",line 3695,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 3695 | if(SITARA_SHUTDOWN_SEQUENCE_START_CMD == usnMinorCmd)                  
;----------------------------------------------------------------------
        MOVL      XAR4,#4865            ; [CPU_U] |3695| 
        MOVU      ACC,*-SP[1]           ; [CPU_] |3695| 
        CMPL      ACC,XAR4              ; [CPU_] |3695| 
        B         $C$L294,NEQ           ; [CPU_] |3695| 
        ; branchcc occurs ; [] |3695| 
	.dwpsn	file "../source/CommInterface.c",line 3697,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3697 | m_bReadyforAspCommand = TRUE;                                          
; 3698 | //!Set the current mode to Shutdown sequence                           
;----------------------------------------------------------------------
        MOVW      DP,#_m_bReadyforAspCommand ; [CPU_U] 
        MOVB      @_m_bReadyforAspCommand,#1,UNC ; [CPU_] |3697| 
	.dwpsn	file "../source/CommInterface.c",line 3699,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 3699 | g_usnCurrentMode = SHUTDOWN_SEQUENCE;                                  
; 3700 | //g_bStartButtonStatus = TRUE;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20511 ; [CPU_] |3699| 
	.dwpsn	file "../source/CommInterface.c",line 3704,column 1,is_stmt,isa 0
$C$L294:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$464	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$464, DW_AT_low_pc(0x00)
	.dwattr $C$DW$464, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$461, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$461, DW_AT_TI_end_line(0xe78)
	.dwattr $C$DW$461, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$461

	.sect	".text:_CIProcessStartupHandlingCmd"
	.clink
	.global	_CIProcessStartupHandlingCmd

$C$DW$465	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$465, DW_AT_name("CIProcessStartupHandlingCmd")
	.dwattr $C$DW$465, DW_AT_low_pc(_CIProcessStartupHandlingCmd)
	.dwattr $C$DW$465, DW_AT_high_pc(0x00)
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_CIProcessStartupHandlingCmd")
	.dwattr $C$DW$465, DW_AT_external
	.dwattr $C$DW$465, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$465, DW_AT_TI_begin_line(0xe81)
	.dwattr $C$DW$465, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$465, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../source/CommInterface.c",line 3714,column 1,is_stmt,address _CIProcessStartupHandlingCmd,isa 0

	.dwfde $C$DW$CIE, _CIProcessStartupHandlingCmd
$C$DW$466	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$466, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$466, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 3713 | void CIProcessStartupHandlingCmd(uint16_t usnMinorCmd)                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessStartupHandlingCmd  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessStartupHandlingCmd:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$467	.dwtag  DW_TAG_variable
	.dwattr $C$DW$467, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$467, DW_AT_location[DW_OP_breg20 -5]

;----------------------------------------------------------------------
; 3715 | //!Check for the minor command of the command received from the process
;     | or                                                                     
;----------------------------------------------------------------------
        MOV       *-SP[5],AL            ; [CPU_] |3714| 
	.dwpsn	file "../source/CommInterface.c",line 3716,column 4,is_stmt,isa 0
;----------------------------------------------------------------------
; 3716 | switch (usnMinorCmd)                                                   
; 3718 |     //!If the command received from processor is Prime All,            
; 3719 |     //!perform the below operations                                    
; 3720 |     case SITARA_PRIME_ALL_CMD:                                         
; 3721 |         //!Set the current mode to Prime All which will be given       
; 3722 |        //!input to MBD to initiate Prime All                           
;----------------------------------------------------------------------
        B         $C$L322,UNC           ; [CPU_] |3716| 
        ; branch occurs ; [] |3716| 
$C$L295:    
	.dwpsn	file "../source/CommInterface.c",line 3723,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 3723 | g_usnCurrentMode = PRIME_ALL_SERVICE_HANDLING;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20485 ; [CPU_] |3723| 
	.dwpsn	file "../source/CommInterface.c",line 3724,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 3724 | sprintf(DebugPrintBuf, "\n PRIME_ALL_SERVICE_HANDLING started: %ld\n",
;     | g_unSequenceStateTime1);                                               
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL61        ; [CPU_U] |3724| 
        MOVW      DP,#_g_unSequenceStateTime1 ; [CPU_U] 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3724| 
        MOVL      ACC,@_g_unSequenceStateTime1 ; [CPU_] |3724| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3724| 
        MOVL      *-SP[4],ACC           ; [CPU_] |3724| 
$C$DW$468	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$468, DW_AT_low_pc(0x00)
	.dwattr $C$DW$468, DW_AT_name("_sprintf")
	.dwattr $C$DW$468, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |3724| 
        ; call occurs [#_sprintf] ; [] |3724| 
	.dwpsn	file "../source/CommInterface.c",line 3725,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 3725 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |3725| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3725| 
$C$DW$469	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$469, DW_AT_low_pc(0x00)
	.dwattr $C$DW$469, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$469, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |3725| 
        ; call occurs [#_UartDebugPrint] ; [] |3725| 
	.dwpsn	file "../source/CommInterface.c",line 3740,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3740 | break;                                                                 
; 3742 | //!If the command received from processor is to get the sytem status,  
; 3743 | //!perform the below operations                                        
; 3744 | case SITARA_SYSTEM_STATUS_CMD:                                         
; 3745 | //sprintf(DebugPrintBuf, "\n SYSTEM_TEST_SERVICE_HANDLING \n");        
; 3746 | //UartDebugPrint((int *)DebugPrintBuf, 50);                            
; 3747 | //!Packetize the system status and send to processor                   
; 3748 | //CPFrameDataPacket(DELFINO_STARTUP_HANDLING_CMD, \                    
; 3749 |         DELFINO_SYSTEM_STATUS_DATA, FALSE);                            
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3740| 
        ; branch occurs ; [] |3740| 
$C$L296:    
	.dwpsn	file "../source/CommInterface.c",line 3751,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3751 | CIFormStartupHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);    
; 3752 | //!System status is requested only when the controller is not          
; 3753 | //!running any sequence, after sending the system status, pull the     
; 3754 | //!busy pin low                                                        
;----------------------------------------------------------------------
        MOV       AL,#20492             ; [CPU_] |3751| 
        MOVB      AH,#0                 ; [CPU_] |3751| 
$C$DW$470	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$470, DW_AT_low_pc(0x00)
	.dwattr $C$DW$470, DW_AT_name("_CIFormStartupHandlingPacket")
	.dwattr $C$DW$470, DW_AT_TI_call

        LCR       #_CIFormStartupHandlingPacket ; [CPU_] |3751| 
        ; call occurs [#_CIFormStartupHandlingPacket] ; [] |3751| 
	.dwpsn	file "../source/CommInterface.c",line 3755,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3755 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$471	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$471, DW_AT_low_pc(0x00)
	.dwattr $C$DW$471, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$471, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |3755| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |3755| 
	.dwpsn	file "../source/CommInterface.c",line 3756,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3756 | break;                                                                 
; 3758 | case SITARA_NORMAL_STARTUP_CMD:    //0x1027                            
; 3759 | //!Do not perform prime all sequence if regular start up sequence is   
; 3760 | //! requested for.                                                     
; 3761 | //m_bAbnormalStartup = FALSE;                                          
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3756| 
        ; branch occurs ; [] |3756| 
$C$L297:    
	.dwpsn	file "../source/CommInterface.c",line 3762,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3762 | sprintf(DebugPrintBuf, "\n Initial SITARA_NORMAL_STARTUP_CMD received \
;     | n");                                                                   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL62        ; [CPU_U] |3762| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3762| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3762| 
$C$DW$472	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$472, DW_AT_low_pc(0x00)
	.dwattr $C$DW$472, DW_AT_name("_sprintf")
	.dwattr $C$DW$472, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |3762| 
        ; call occurs [#_sprintf] ; [] |3762| 
	.dwpsn	file "../source/CommInterface.c",line 3763,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3763 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 3764 | //!Packetize the system status and send to processor                   
; 3765 | //CPFrameDataPacket(DELFINO_STARTUP_HANDLING_CMD, \                    
; 3766 |         DELFINO_SYSTEM_STATUS_DATA, FALSE);                            
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |3763| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3763| 
$C$DW$473	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$473, DW_AT_low_pc(0x00)
	.dwattr $C$DW$473, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$473, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |3763| 
        ; call occurs [#_UartDebugPrint] ; [] |3763| 
	.dwpsn	file "../source/CommInterface.c",line 3767,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3767 | CIFormStartupHandlingPacket(SYSTEM_STATUS_SERVICE_HANDLING, FALSE);    
; 3768 | //!System status is requested only when the controller is not          
; 3769 | //!running any sequence, after sending the system status, pull the     
; 3770 | //!busy pin low                                                        
;----------------------------------------------------------------------
        MOV       AL,#20492             ; [CPU_] |3767| 
        MOVB      AH,#0                 ; [CPU_] |3767| 
$C$DW$474	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$474, DW_AT_low_pc(0x00)
	.dwattr $C$DW$474, DW_AT_name("_CIFormStartupHandlingPacket")
	.dwattr $C$DW$474, DW_AT_TI_call

        LCR       #_CIFormStartupHandlingPacket ; [CPU_] |3767| 
        ; call occurs [#_CIFormStartupHandlingPacket] ; [] |3767| 
	.dwpsn	file "../source/CommInterface.c",line 3771,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3771 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$475	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$475, DW_AT_low_pc(0x00)
	.dwattr $C$DW$475, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$475, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |3771| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |3771| 
	.dwpsn	file "../source/CommInterface.c",line 3772,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3772 | break;                                                                 
; 3773 | //!If the command received from processor is to get the sytem status,  
; 3774 | //!perform the below operations                                        
; 3791 | //!If the command received from processor is Prime with Rinse,         
; 3792 | //!perform the below operations                                        
; 3793 | case SITARA_START_UP_SEQUENCE_CMD:                                     
; 3794 | //!Sitara requires the major start up sequence to proceeded in         
; 3795 | //! Patient handling sequence.                                         
; 3796 | //! So the major command to be changed from Startup to patient handling
; 3797 | //!Set the major command to patient handling for the startup sequence  
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3772| 
        ; branch occurs ; [] |3772| 
$C$L298:    
	.dwpsn	file "../source/CommInterface.c",line 3798,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3798 | g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;                       
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4100 ; [CPU_] |3798| 
	.dwpsn	file "../source/CommInterface.c",line 3800,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3800 | sprintf(DebugPrintBuf, "\n Normal internal STARTUP_CMD received\n");   
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL63        ; [CPU_U] |3800| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3800| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3800| 
$C$DW$476	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$476, DW_AT_low_pc(0x00)
	.dwattr $C$DW$476, DW_AT_name("_sprintf")
	.dwattr $C$DW$476, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |3800| 
        ; call occurs [#_sprintf] ; [] |3800| 
	.dwpsn	file "../source/CommInterface.c",line 3801,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3801 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 3802 | //!Set the current mode to Start up sequence which will be given       
; 3803 | //!input to MBD to initiate start up sequence                          
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |3801| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3801| 
$C$DW$477	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$477, DW_AT_low_pc(0x00)
	.dwattr $C$DW$477, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$477, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |3801| 
        ; call occurs [#_UartDebugPrint] ; [] |3801| 
	.dwpsn	file "../source/CommInterface.c",line 3804,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3804 | g_usnCurrentMode = START_UP_SEQUENCE_MODE;                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20500 ; [CPU_] |3804| 
	.dwpsn	file "../source/CommInterface.c",line 3805,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3805 | break;                                                                 
; 3807 | case SITARA_STARTUP_FAILED_CMD: //HN_Added(19/12/19)                   
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3805| 
        ; branch occurs ; [] |3805| 
$C$L299:    
	.dwpsn	file "../source/CommInterface.c",line 3808,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3808 | sprintf(DebugPrintBuf, "\n SITARA_STARTUP_FAILED_CMD \n");             
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL64        ; [CPU_U] |3808| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3808| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3808| 
$C$DW$478	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$478, DW_AT_low_pc(0x00)
	.dwattr $C$DW$478, DW_AT_name("_sprintf")
	.dwattr $C$DW$478, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |3808| 
        ; call occurs [#_sprintf] ; [] |3808| 
	.dwpsn	file "../source/CommInterface.c",line 3809,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3809 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
; 3810 | //!Set the current mode to Startup failed sequence which will be given 
; 3811 | //!input to MBD to initiate start up sequence                          
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |3809| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3809| 
$C$DW$479	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$479, DW_AT_low_pc(0x00)
	.dwattr $C$DW$479, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$479, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |3809| 
        ; call occurs [#_UartDebugPrint] ; [] |3809| 
	.dwpsn	file "../source/CommInterface.c",line 3812,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3812 | g_usnCurrentMode = MBD_STARTUP_FAILED_CMD;                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20540 ; [CPU_] |3812| 
	.dwpsn	file "../source/CommInterface.c",line 3813,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3813 | break;                                                                 
; 3815 | case SITARA_START_UP_FROM_SERVICE_CMD:   //HN_Added(4/01/19)           
; 3816 | //!Sitara requires the major start up sequence to proceeded in         
; 3817 | //! Patient handling sequence.                                         
; 3818 | //! So the major command to be changed from Startup to patient handling
; 3819 | //!Set the major command to service handling for the startup sequence  
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3813| 
        ; branch occurs ; [] |3813| 
$C$L300:    
	.dwpsn	file "../source/CommInterface.c",line 3820,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 3820 | g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;                       
; 3822 | //!Set the current mode to Start up sequence which will be given       
; 3823 | //!input to MBD to initiate start up sequence                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentModeMajor ; [CPU_U] 
        MOV       @_g_usnCurrentModeMajor,#4100 ; [CPU_] |3820| 
	.dwpsn	file "../source/CommInterface.c",line 3824,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 3824 | g_usnCurrentMode = MBD_STARTUP_FROM_SERVICE_SCREEN_CMD;                
;----------------------------------------------------------------------
        MOV       @_g_usnCurrentMode,#20537 ; [CPU_] |3824| 
	.dwpsn	file "../source/CommInterface.c",line 3825,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 3825 | sprintf(DebugPrintBuf, "\n STARTUP_FROM_SERVICE_SCREEN_CMD received\n")
;     | ;                                                                      
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL65        ; [CPU_U] |3825| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |3825| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3825| 
$C$DW$480	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$480, DW_AT_low_pc(0x00)
	.dwattr $C$DW$480, DW_AT_name("_sprintf")
	.dwattr $C$DW$480, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |3825| 
        ; call occurs [#_sprintf] ; [] |3825| 
	.dwpsn	file "../source/CommInterface.c",line 3826,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 3826 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |3826| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |3826| 
$C$DW$481	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$481, DW_AT_low_pc(0x00)
	.dwattr $C$DW$481, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$481, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |3826| 
        ; call occurs [#_UartDebugPrint] ; [] |3826| 
	.dwpsn	file "../source/CommInterface.c",line 3827,column 11,is_stmt,isa 0
;----------------------------------------------------------------------
; 3827 | break;                                                                 
; 3829 | //!****************************** Imp ******************************** 
; 3830 | //!This CMD we should receive only if the previous sequence is shutdown
; 3831 | //! ****************************************************************** 
; 3832 | case SITARA_STARTUP_BACKFLUSH:                                         
; 3833 | //g_usnCurrentModeMajor = SITARA_SERVICE_HANDLING;                     
; 3834 | //!Set the current mode to Start up sequence which will be given       
; 3835 | //!input to MBD to initiate start up sequence                          
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3827| 
        ; branch occurs ; [] |3827| 
$C$L301:    
	.dwpsn	file "../source/CommInterface.c",line 3836,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 3836 | g_usnCurrentMode = MBD_STARTUP_BACKFLUSH_CMD;                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20538 ; [CPU_] |3836| 
	.dwpsn	file "../source/CommInterface.c",line 3837,column 10,is_stmt,isa 0
;----------------------------------------------------------------------
; 3837 | break;                                                                 
; 3839 | //!If the command received from processor is X-Motor Check,            
; 3840 | //!perform the below operations                                        
; 3841 | case SITARA_X_AXIS_MOTOR_CHECK_CMD:                                    
; 3842 |   //!Set the current mode to X-Motor Check which will be given         
; 3843 |   //!input to MBD to initiate X-Motor Check                            
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3837| 
        ; branch occurs ; [] |3837| 
$C$L302:    
	.dwpsn	file "../source/CommInterface.c",line 3844,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3844 | g_usnCurrentMode = X_AXIS_MOTOR_CHECK;                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20505 ; [CPU_] |3844| 
	.dwpsn	file "../source/CommInterface.c",line 3845,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3845 | break;                                                                 
; 3847 | //!If the command received from processor is Y-Motor Check,            
; 3848 | //!perform the below operations                                        
; 3849 | case SITARA_Y_AXIS_MOTOR_CHECK_CMD:                                    
; 3858 | //!Set the current mode to Y-Motor Check which will be given           
; 3859 | //!input to MBD to initiate Y-Motor Check                              
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3845| 
        ; branch occurs ; [] |3845| 
$C$L303:    
	.dwpsn	file "../source/CommInterface.c",line 3860,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3860 | g_usnCurrentMode = Y_AXIS_MOTOR_CHECK;                                 
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20506 ; [CPU_] |3860| 
	.dwpsn	file "../source/CommInterface.c",line 3861,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3861 | break;                                                                 
; 3863 | //!If the command received from processor is Diluent Motor Check,      
; 3864 | //!perform the below operations                                        
; 3865 | case SITARA_DILUENT_SYRINGE_MOTOR_CHECK_CMD:                           
; 3866 | //!Set the current mode to Diluent Motor Check which will be given     
; 3867 | //!input to MBD to initiate Diluent Motor Check                        
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3861| 
        ; branch occurs ; [] |3861| 
$C$L304:    
	.dwpsn	file "../source/CommInterface.c",line 3868,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3868 | g_usnCurrentMode = DILUENT_SYRINGE_MOTOR_CHECK;                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20507 ; [CPU_] |3868| 
	.dwpsn	file "../source/CommInterface.c",line 3869,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3869 | break;                                                                 
; 3871 | //!If the command received from processor is Waste Motor Check,        
; 3872 | //!perform the below operations                                        
; 3873 | case SITARA_WASTE_SYRINGE_MOTOR_CHECK_CMD:                             
; 3874 | //!Set the current mode to Waste Motor Check which will be given       
; 3875 | //!input to MBD to initiate Waste Motor Check                          
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3869| 
        ; branch occurs ; [] |3869| 
$C$L305:    
	.dwpsn	file "../source/CommInterface.c",line 3876,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3876 | g_usnCurrentMode = WASTE_SYRINGE_MOTOR_CHECK;                          
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20508 ; [CPU_] |3876| 
	.dwpsn	file "../source/CommInterface.c",line 3877,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3877 | break;                                                                 
; 3879 | //!If the command received from processor is Sample Motor Check,       
; 3880 | //!perform the below operations                                        
; 3881 | case SITARA_SAMPLE_LYSE_SYRINGE_MOTOR_CHECK_CMD:                       
; 3882 | //!Set the current mode to Sample Motor Check which will be given      
; 3883 | //!input to MBD to initiate Sample Motor Check                         
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3877| 
        ; branch occurs ; [] |3877| 
$C$L306:    
	.dwpsn	file "../source/CommInterface.c",line 3884,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3884 | g_usnCurrentMode = SAMPLE_LYSE_SYRINGE_MOTOR_CHECK;                    
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20509 ; [CPU_] |3884| 
	.dwpsn	file "../source/CommInterface.c",line 3885,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3885 | break;                                                                 
; 3887 | //!If the command received from processor is Back Flush / Flush Apertur
;     | e,                                                                     
; 3888 | //!perform the below operations                                        
; 3889 | case SITARA_BACK_FLUSH_CMD:                                            
; 3890 | //!Set the current mode to Back Flush / Flush Aperture which will be gi
;     | ven                                                                    
; 3891 | //!input to MBD to initiate Back Flush / Flush Aperture                
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3885| 
        ; branch occurs ; [] |3885| 
$C$L307:    
	.dwpsn	file "../source/CommInterface.c",line 3892,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3892 | g_usnCurrentMode = BACK_FLUSH_SERVICE_HANDLING;                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20486 ; [CPU_] |3892| 
	.dwpsn	file "../source/CommInterface.c",line 3893,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3893 | break;                                                                 
; 3895 | //!If the command received from processor is Rinse Prime,              
; 3896 | //!perform the below operations                                        
; 3897 | case SITARA_PRIME_WITH_RINSE_CMD:                                      
; 3898 | //!Set the current mode to Rinse Prime which will be given             
; 3899 | //!input to MBD to initiate Rinse Prime                                
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3893| 
        ; branch occurs ; [] |3893| 
$C$L308:    
	.dwpsn	file "../source/CommInterface.c",line 3900,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3900 | g_usnCurrentMode = PRIME_WITH_RINSE_SERVICE_HANDLING;                  
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20481 ; [CPU_] |3900| 
	.dwpsn	file "../source/CommInterface.c",line 3901,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3901 | break;                                                                 
; 3903 | //!If the command received from processor is Lyse Prime,               
; 3904 | //!perform the below operations                                        
; 3905 | case SITARA_PRIME_WITH_LYSE_CMD:                                       
; 3906 | //!Set the current mode to Lyse Prime which will be given              
; 3907 | //!input to MBD to initiate Lyse Prime                                 
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3901| 
        ; branch occurs ; [] |3901| 
$C$L309:    
	.dwpsn	file "../source/CommInterface.c",line 3908,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3908 | g_usnCurrentMode = PRIME_WITH_LYSE_SERVICE_HANDLING;                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20482 ; [CPU_] |3908| 
	.dwpsn	file "../source/CommInterface.c",line 3909,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3909 | break;                                                                 
; 3911 | //!If the command received from processor is Diluent Prime,            
; 3912 | //!perform the below operations                                        
; 3913 | case SITARA_PRIME_WITH_DILUENT_CMD:                                    
; 3914 | //!Set the current mode to Diluent Prime which will be given           
; 3915 | //!input to MBD to initiate Diluent Prime                              
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3909| 
        ; branch occurs ; [] |3909| 
$C$L310:    
	.dwpsn	file "../source/CommInterface.c",line 3916,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3916 | g_usnCurrentMode = PRIME_WITH_DILUENT_SERVICE_HANDLING;                
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20483 ; [CPU_] |3916| 
	.dwpsn	file "../source/CommInterface.c",line 3917,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3917 | break;                                                                 
; 3919 | //!If the command received from processor is EZ Cleaner,               
; 3920 | //!perform the below operations                                        
; 3921 | case SITARA_PRIME_WITH_EZ_CLEANSER_CMD:                                
; 3922 | //!Set the current mode to EZ Cleaner which will be given              
; 3923 | //!input to MBD to initiate EZ Cleaner                                 
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3917| 
        ; branch occurs ; [] |3917| 
$C$L311:    
	.dwpsn	file "../source/CommInterface.c",line 3924,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3924 | g_usnCurrentMode = PRIME_WITH_EZ_CLEANSER_CMD;                         
; 3925 | //g_bStartButtonStatus = TRUE;                                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20484 ; [CPU_] |3924| 
	.dwpsn	file "../source/CommInterface.c",line 3926,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3926 | break;                                                                 
; 3928 | //!If the command received from processor is ZAP Aperture,             
; 3929 | //!perform the below operations                                        
; 3930 | case SITARA_ZAP_APERTURE:                                              
; 3931 | //!Set the current mode to ZAP Aperture which will be given            
; 3932 | //!input to MBD to initiate ZAP Aperture                               
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3926| 
        ; branch occurs ; [] |3926| 
$C$L312:    
	.dwpsn	file "../source/CommInterface.c",line 3933,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3933 | g_usnCurrentMode = ZAP_APERTURE_SERVICE_HANDLING;                      
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20488 ; [CPU_] |3933| 
	.dwpsn	file "../source/CommInterface.c",line 3934,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3934 | break;                                                                 
; 3936 | //!If the command received from processor is Drain Bath,               
; 3937 | //!perform the below operations                                        
; 3938 | case SITARA_DRAIN_BATH:                                                
; 3939 | //!Set the current mode to Drain Bath which will be given              
; 3940 | //!input to MBD to initiate Drain Bath                                 
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3934| 
        ; branch occurs ; [] |3934| 
$C$L313:    
	.dwpsn	file "../source/CommInterface.c",line 3941,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3941 | g_usnCurrentMode = DRAIN_BATH_SERVICE_HANDLING;                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20489 ; [CPU_] |3941| 
	.dwpsn	file "../source/CommInterface.c",line 3942,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3942 | break;                                                                 
; 3944 | //!If the command received from processor is Drain All,                
; 3945 | //!perform the below operations                                        
; 3946 | case SITARA_DRAIN_ALL:                                                 
; 3947 | //!Set the current mode to Drain All which will be given               
; 3948 | //!input to MBD to initiate Drain All                                  
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3942| 
        ; branch occurs ; [] |3942| 
$C$L314:    
	.dwpsn	file "../source/CommInterface.c",line 3949,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3949 | g_usnCurrentMode = DRAIN_ALL_SERVICE_HANDLING;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20490 ; [CPU_] |3949| 
	.dwpsn	file "../source/CommInterface.c",line 3950,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3950 | break;                                                                 
; 3952 | //!If the command received from processor is Clean Bath,               
; 3953 | //!perform the below operations                                        
; 3954 | case SITARA_CLEAN_BATH:                                                
; 3955 | //!Set the current mode to Clean Bath which will be given              
; 3956 | //!input to MBD to initiate Clean Bath                                 
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3950| 
        ; branch occurs ; [] |3950| 
$C$L315:    
	.dwpsn	file "../source/CommInterface.c",line 3957,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3957 | g_usnCurrentMode = CLEAN_BATH_SERVICE_HANDLING;                        
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20491 ; [CPU_] |3957| 
	.dwpsn	file "../source/CommInterface.c",line 3958,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3958 | break;                                                                 
; 3960 | //!If the command received from processor is Valve Test to test specifi
;     | c                                                                      
; 3961 | //!valve indicated by processor, perform the below operations          
; 3962 | case SITARA_VALVE_TEST_CMD:                                            
; 3963 | //!Set the current mode to Valve Test and specific valve will be excite
;     | d                                                                      
; 3964 | //!to check its operation                                              
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3958| 
        ; branch occurs ; [] |3958| 
$C$L316:    
	.dwpsn	file "../source/CommInterface.c",line 3965,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3965 | SysInitValveTest((unsigned char)m_stReceivedPacketFrame.arrusnData[0]);
;----------------------------------------------------------------------
        MOVW      DP,#_m_stReceivedPacketFrame+6 ; [CPU_U] 
        MOV       AL,@_m_stReceivedPacketFrame+6 ; [CPU_] |3965| 
$C$DW$482	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$482, DW_AT_low_pc(0x00)
	.dwattr $C$DW$482, DW_AT_name("_SysInitValveTest")
	.dwattr $C$DW$482, DW_AT_TI_call

        LCR       #_SysInitValveTest    ; [CPU_] |3965| 
        ; call occurs [#_SysInitValveTest] ; [] |3965| 
	.dwpsn	file "../source/CommInterface.c",line 3966,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3966 | break;                                                                 
; 3968 | //!If the command received from processor is Counting Time Sequence,   
; 3969 | //!perform the below operations                                        
; 3970 | case SITARA_COUNTING_TIME_SEQUENCE_START_CMD:                          
; 3971 | //!Set the current mode to Counting Time Sequence which will be given  
; 3972 | //!input to MBD to initiate Counting Time Sequence                     
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3966| 
        ; branch occurs ; [] |3966| 
$C$L317:    
	.dwpsn	file "../source/CommInterface.c",line 3973,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3973 | g_usnCurrentMode = COUNTING_TIME_SEQUENCE;                             
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20499 ; [CPU_] |3973| 
	.dwpsn	file "../source/CommInterface.c",line 3974,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3974 | break;                                                                 
; 3976 | //!If the command received from processor is Wake Up Sequence,         
; 3977 | //!perform the below operations                                        
; 3978 | case SITARA_WAKE_UP_SEQUENCE_CMD:                                      
; 3979 | //!Set the current mode to Wake Up Sequence which will be given        
; 3980 | //!input to MBD to initiate Wake Up Sequence                           
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3974| 
        ; branch occurs ; [] |3974| 
$C$L318:    
	.dwpsn	file "../source/CommInterface.c",line 3981,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3981 | g_usnCurrentMode = MBD_WAKEUP_START;                                   
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20521 ; [CPU_] |3981| 
	.dwpsn	file "../source/CommInterface.c",line 3982,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3982 | break;                                                                 
; 3984 | //!If the command received from processor is request for Home position 
; 3985 | //!status, perform the below operations                                
; 3986 | case SITARA_HOME_POSITION_STATUS_CMD:                                  
; 3987 | //!Packetize the current Home position status of the motors and        
; 3988 | //!send to processor                                                   
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3982| 
        ; branch occurs ; [] |3982| 
$C$L319:    
	.dwpsn	file "../source/CommInterface.c",line 3989,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3989 | CPFrameDataPacket(DELFINO_SERVICE_HANDLING, \                          
; 3990 |         DELFINO_HOME_POSITION_SENSOR_CHECK, FALSE);                    
; 3991 | //!Current Home position status of the motors is requested only        
; 3992 | //!when the controller is not running any sequence, after sending      
; 3993 | //!pull the busy pin low                                               
;----------------------------------------------------------------------
        MOV       AL,#8196              ; [CPU_] |3989| 
        MOVB      XAR4,#0               ; [CPU_] |3989| 
        MOV       AH,#8232              ; [CPU_] |3989| 
$C$DW$483	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$483, DW_AT_low_pc(0x00)
	.dwattr $C$DW$483, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$483, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |3989| 
        ; call occurs [#_CPFrameDataPacket] ; [] |3989| 
	.dwpsn	file "../source/CommInterface.c",line 3994,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3994 | SPIClearCommCtrlBusyInt();                                             
;----------------------------------------------------------------------
$C$DW$484	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$484, DW_AT_low_pc(0x00)
	.dwattr $C$DW$484, DW_AT_name("_SPIClearCommCtrlBusyInt")
	.dwattr $C$DW$484, DW_AT_TI_call

        LCR       #_SPIClearCommCtrlBusyInt ; [CPU_] |3994| 
        ; call occurs [#_SPIClearCommCtrlBusyInt] ; [] |3994| 
	.dwpsn	file "../source/CommInterface.c",line 3995,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3995 | break;                                                                 
; 3996 | case SITARA_START_TOSHIP_SEQUENCE_CMD:                                 
; 3997 | //!Set the current mode to to ship which will be given                 
; 3998 | //!input to MBD to initiate to ship sequence                           
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |3995| 
        ; branch occurs ; [] |3995| 
$C$L320:    
	.dwpsn	file "../source/CommInterface.c",line 3999,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 3999 | g_usnCurrentMode = MBD_TO_SHIP_SEQUENCE_START;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20522 ; [CPU_] |3999| 
	.dwpsn	file "../source/CommInterface.c",line 4000,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 4000 | break;                                                                 
; 4002 | case SITARA_VOLUMETRIC_CHECK_CMD:                                      
; 4003 | //!Set the current mode to volumetric board checkwhich will be given   
; 4004 | //!input to MBD to initiate to ship sequence                           
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |4000| 
        ; branch occurs ; [] |4000| 
$C$L321:    
	.dwpsn	file "../source/CommInterface.c",line 4005,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 4005 | g_usnCurrentMode = MBD_VOLUMETRIC_BOARD_CHECK;                         
;----------------------------------------------------------------------
        MOVW      DP,#_g_usnCurrentMode ; [CPU_U] 
        MOV       @_g_usnCurrentMode,#20526 ; [CPU_] |4005| 
	.dwpsn	file "../source/CommInterface.c",line 4006,column 12,is_stmt,isa 0
;----------------------------------------------------------------------
; 4006 | break;                                                                 
; 4007 | //default case                                                         
; 4008 | default :                                                              
; 4009 | break;                                                                 
; 4011 | }//end of switch case                                                  
;----------------------------------------------------------------------
        B         $C$L324,UNC           ; [CPU_] |4006| 
        ; branch occurs ; [] |4006| 
$C$L322:    
	.dwpsn	file "../source/CommInterface.c",line 3716,column 4,is_stmt,isa 0
        SUB       AL,#4097              ; [CPU_] |3716| 
        MOVZ      AR6,*-SP[5]           ; [CPU_] |3716| 
        CMPB      AL,#27                ; [CPU_] |3716| 
        B         $C$L323,LOS           ; [CPU_] |3716| 
        ; branchcc occurs ; [] |3716| 
        MOVZ      AR7,AR6               ; [CPU_] |3716| 
        MOVL      XAR4,#4133            ; [CPU_U] |3716| 
        MOVL      ACC,XAR4              ; [CPU_] |3716| 
        CMPL      ACC,XAR7              ; [CPU_] |3716| 
        B         $C$L300,EQ            ; [CPU_] |3716| 
        ; branchcc occurs ; [] |3716| 
        MOVZ      AR7,AR6               ; [CPU_] |3716| 
        MOVL      XAR4,#4134            ; [CPU_U] |3716| 
        MOVL      ACC,XAR4              ; [CPU_] |3716| 
        CMPL      ACC,XAR7              ; [CPU_] |3716| 
        B         $C$L301,EQ            ; [CPU_] |3716| 
        ; branchcc occurs ; [] |3716| 
        MOVZ      AR6,AR6               ; [CPU_] |3716| 
        MOVL      XAR4,#4135            ; [CPU_U] |3716| 
        MOVL      ACC,XAR4              ; [CPU_] |3716| 
        CMPL      ACC,XAR6              ; [CPU_] |3716| 
        B         $C$L297,EQ            ; [CPU_] |3716| 
        ; branchcc occurs ; [] |3716| 
        B         $C$L324,UNC           ; [CPU_] |3716| 
        ; branch occurs ; [] |3716| 
$C$L323:    
        SUB       AR6,#4097             ; [CPU_] |3716| 
        MOV       ACC,AR6 << #1         ; [CPU_] |3716| 
        MOVZ      AR6,AL                ; [CPU_] |3716| 
        MOVL      XAR7,#$C$SW17         ; [CPU_U] |3716| 
        MOVL      ACC,XAR7              ; [CPU_] |3716| 
        ADDU      ACC,AR6               ; [CPU_] |3716| 
        MOVL      XAR7,ACC              ; [CPU_] |3716| 
        MOVL      XAR7,*XAR7            ; [CPU_] |3716| 
        LB        *XAR7                 ; [CPU_] |3716| 
        ; branch occurs ; [] |3716| 
	.sect	".switch:_CIProcessStartupHandlingCmd"
	.clink
$C$SW17:	.long	$C$L308	; 4097
	.long	$C$L309	; 4098
	.long	$C$L310	; 4099
	.long	$C$L295	; 4100
	.long	$C$L307	; 4101
	.long	$C$L311	; 4102
	.long	$C$L312	; 4103
	.long	$C$L313	; 4104
	.long	$C$L314	; 4105
	.long	$C$L315	; 4106
	.long	$C$L316	; 4107
	.long	$C$L296	; 4108
	.long	$C$L324	; 0
	.long	$C$L298	; 4110
	.long	$C$L299	; 4111
	.long	$C$L324	; 0
	.long	$C$L302	; 4113
	.long	$C$L303	; 4114
	.long	$C$L304	; 4115
	.long	$C$L305	; 4116
	.long	$C$L306	; 4117
	.long	$C$L317	; 4118
	.long	$C$L320	; 4119
	.long	$C$L319	; 4120
	.long	$C$L318	; 4121
	.long	$C$L324	; 0
	.long	$C$L324	; 0
	.long	$C$L321	; 4124
	.sect	".text:_CIProcessStartupHandlingCmd"
$C$L324:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$485	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$485, DW_AT_low_pc(0x00)
	.dwattr $C$DW$485, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$465, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$465, DW_AT_TI_end_line(0xfad)
	.dwattr $C$DW$465, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$465

	.sect	".text:_CIFormErrorPacket"
	.clink
	.global	_CIFormErrorPacket

$C$DW$486	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$486, DW_AT_name("CIFormErrorPacket")
	.dwattr $C$DW$486, DW_AT_low_pc(_CIFormErrorPacket)
	.dwattr $C$DW$486, DW_AT_high_pc(0x00)
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_CIFormErrorPacket")
	.dwattr $C$DW$486, DW_AT_external
	.dwattr $C$DW$486, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$486, DW_AT_TI_begin_line(0xfb6)
	.dwattr $C$DW$486, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$486, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../source/CommInterface.c",line 4023,column 1,is_stmt,address _CIFormErrorPacket,isa 0

	.dwfde $C$DW$CIE, _CIFormErrorPacket
$C$DW$487	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$487, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$487, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 4022 | void CIFormErrorPacket(uint16_t usnMinorCmd)                           
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIFormErrorPacket            FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIFormErrorPacket:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$488	.dwtag  DW_TAG_variable
	.dwattr $C$DW$488, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$488, DW_AT_location[DW_OP_breg20 -1]

;----------------------------------------------------------------------
; 4024 | //sprintf(DebugPrintBuf, "\r\n Error Command: %x \n", usnMinorCmd);    
; 4025 | //UartDebugPrint((int *)DebugPrintBuf, 50);                            
; 4027 | //!Frame shutdown sequence completed command and sent to Sitara        
;----------------------------------------------------------------------
        MOV       *-SP[1],AL            ; [CPU_] |4023| 
	.dwpsn	file "../source/CommInterface.c",line 4028,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 4028 | if(usnMinorCmd != NULL)                                                
;----------------------------------------------------------------------
        CMPB      AL,#0                 ; [CPU_] |4028| 
        B         $C$L325,EQ            ; [CPU_] |4028| 
        ; branchcc occurs ; [] |4028| 
	.dwpsn	file "../source/CommInterface.c",line 4030,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 4030 | CPFrameDataPacket(DELFINO_ERROR_HANDLING_CMD,usnMinorCmd, FALSE);      
;----------------------------------------------------------------------
        MOV       AL,#8203              ; [CPU_] |4030| 
        MOV       AH,*-SP[1]            ; [CPU_] |4030| 
        MOVB      XAR4,#0               ; [CPU_] |4030| 
$C$DW$489	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$489, DW_AT_low_pc(0x00)
	.dwattr $C$DW$489, DW_AT_name("_CPFrameDataPacket")
	.dwattr $C$DW$489, DW_AT_TI_call

        LCR       #_CPFrameDataPacket   ; [CPU_] |4030| 
        ; call occurs [#_CPFrameDataPacket] ; [] |4030| 
	.dwpsn	file "../source/CommInterface.c",line 4033,column 1,is_stmt,isa 0
$C$L325:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$490	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$490, DW_AT_low_pc(0x00)
	.dwattr $C$DW$490, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$486, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$486, DW_AT_TI_end_line(0xfc1)
	.dwattr $C$DW$486, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$486

	.sect	".text:_CIProcessErrorCommand"
	.clink
	.global	_CIProcessErrorCommand

$C$DW$491	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$491, DW_AT_name("CIProcessErrorCommand")
	.dwattr $C$DW$491, DW_AT_low_pc(_CIProcessErrorCommand)
	.dwattr $C$DW$491, DW_AT_high_pc(0x00)
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_CIProcessErrorCommand")
	.dwattr $C$DW$491, DW_AT_external
	.dwattr $C$DW$491, DW_AT_TI_begin_file("../source/CommInterface.c")
	.dwattr $C$DW$491, DW_AT_TI_begin_line(0xfca)
	.dwattr $C$DW$491, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$491, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../source/CommInterface.c",line 4043,column 1,is_stmt,address _CIProcessErrorCommand,isa 0

	.dwfde $C$DW$CIE, _CIProcessErrorCommand
$C$DW$492	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$492, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$492, DW_AT_location[DW_OP_reg0]

;----------------------------------------------------------------------
; 4042 | void CIProcessErrorCommand(uint16_t usnMinorCmd)                       
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _CIProcessErrorCommand        FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_CIProcessErrorCommand:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$493	.dwtag  DW_TAG_variable
	.dwattr $C$DW$493, DW_AT_name("usnMinorCmd")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_usnMinorCmd")
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$493, DW_AT_location[DW_OP_breg20 -3]

;----------------------------------------------------------------------
; 4044 | //!Check for the minor command                                         
;----------------------------------------------------------------------
        MOV       *-SP[3],AL            ; [CPU_] |4043| 
	.dwpsn	file "../source/CommInterface.c",line 4057,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 4057 | if(SITARA_ERROR_REQUEST == usnMinorCmd)     //QA_C                     
;----------------------------------------------------------------------
        MOVL      XAR4,#4097            ; [CPU_U] |4057| 
        MOVU      ACC,*-SP[3]           ; [CPU_] |4057| 
        CMPL      ACC,XAR4              ; [CPU_] |4057| 
        B         $C$L326,NEQ           ; [CPU_] |4057| 
        ; branchcc occurs ; [] |4057| 
	.dwpsn	file "../source/CommInterface.c",line 4059,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 4059 | sprintf(DebugPrintBuf, "\n SITARA_ERROR_REQUEST \n");                  
;----------------------------------------------------------------------
        MOVL      XAR4,#$C$FSL66        ; [CPU_U] |4059| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |4059| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |4059| 
$C$DW$494	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$494, DW_AT_low_pc(0x00)
	.dwattr $C$DW$494, DW_AT_name("_sprintf")
	.dwattr $C$DW$494, DW_AT_TI_call

        LCR       #_sprintf             ; [CPU_] |4059| 
        ; call occurs [#_sprintf] ; [] |4059| 
	.dwpsn	file "../source/CommInterface.c",line 4060,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 4060 | UartDebugPrint((int *)DebugPrintBuf, 50);                              
;----------------------------------------------------------------------
        MOVB      AL,#50                ; [CPU_] |4060| 
        MOVL      XAR4,#_DebugPrintBuf  ; [CPU_U] |4060| 
$C$DW$495	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$495, DW_AT_low_pc(0x00)
	.dwattr $C$DW$495, DW_AT_name("_UartDebugPrint")
	.dwattr $C$DW$495, DW_AT_TI_call

        LCR       #_UartDebugPrint      ; [CPU_] |4060| 
        ; call occurs [#_UartDebugPrint] ; [] |4060| 
	.dwpsn	file "../source/CommInterface.c",line 4061,column 9,is_stmt,isa 0
;----------------------------------------------------------------------
; 4061 | CIFormErrorPacket(DELFINO_ERROR_MINOR_CMD);                            
;----------------------------------------------------------------------
        MOV       AL,#9217              ; [CPU_] |4061| 
$C$DW$496	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$496, DW_AT_low_pc(0x00)
	.dwattr $C$DW$496, DW_AT_name("_CIFormErrorPacket")
	.dwattr $C$DW$496, DW_AT_TI_call

        LCR       #_CIFormErrorPacket   ; [CPU_] |4061| 
        ; call occurs [#_CIFormErrorPacket] ; [] |4061| 
	.dwpsn	file "../source/CommInterface.c",line 4062,column 5,is_stmt,isa 0
;----------------------------------------------------------------------
; 4063 | else                                                                   
; 4065 |     //do nothing!!!                                                    
;----------------------------------------------------------------------
        B         $C$L326,UNC           ; [CPU_] |4062| 
        ; branch occurs ; [] |4062| 
$C$L326:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$497	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$497, DW_AT_low_pc(0x00)
	.dwattr $C$DW$497, DW_AT_TI_return

        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$491, DW_AT_TI_end_file("../source/CommInterface.c")
	.dwattr $C$DW$491, DW_AT_TI_end_line(0xfe4)
	.dwattr $C$DW$491, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$491

;***************************************************************
;* FAR STRINGS                                                 *
;***************************************************************
	.sect	".econst:.string"
	.align	2
$C$FSL1:	.string	13,10," WHOLE_BLOOD_MODE",10,0
	.align	2
$C$FSL2:	.string	13,10," SITARA_START_DISPENSE_CMD",10,0
	.align	2
$C$FSL3:	.string	10," SITARA_START_POLLING_START_BUTTON_CMD",10,0
	.align	2
$C$FSL4:	.string	10," SITARA_STOP_POLLING_START_BUTTON_CMD",10,0
	.align	2
$C$FSL5:	.string	13,10," PULSE_HEIGHT_DATA_received at %ld",10,0
	.align	2
$C$FSL6:	.string	13,10," NEGATIVE_ACKNOWLEDGEMENT_HANDELING",10,0
	.align	2
$C$FSL7:	.string	13,10," N_A_H..WBC",10,0
	.align	2
$C$FSL8:	.string	13,10," N_A_H...RBC",10,0
	.align	2
$C$FSL9:	.string	13,10," N_A_H....PLT",10,0
	.align	2
$C$FSL10:	.string	13,10," N_A_H default",10,0
	.align	2
$C$FSL11:	.string	10," DELFINO_PRIME_WITH_EZ_CLEANSER_STARTED_CMD",10,0
	.align	2
$C$FSL12:	.string	10," DELFINO_PRIME_WITH_EZ_CLEANSER_CMD : %ld",10,0
	.align	2
$C$FSL13:	.string	10," DELFINO_BACK_FLUSH_STARTED_CMD",10,0
	.align	2
$C$FSL14:	.string	10," DELFINO_BACK_FLUSH_COMPLETED_CMD : %ld",10,0
	.align	2
$C$FSL15:	.string	10," DELFINO_DRAIN_ALL_STARTED_CMD",10,0
	.align	2
$C$FSL16:	.string	10," DELFINO_DRAIN_ALL_COMPLETED_CMD : %ld",10,0
	.align	2
$C$FSL17:	.string	10," DELFINO_SET_WASTE_BIN_FULL",10,0
	.align	2
$C$FSL18:	.string	10," DELFINO_CLEAR_WASTE_BIN_FULL",10,0
	.align	2
$C$FSL19:	.string	10," DELFINO_PROBE_CLEANING_COMPLETED_CMD",10,0
	.align	2
$C$FSL20:	.string	10," DELFINO_WAKEUP_SEQUENCE_STARTED ",10,0
	.align	2
$C$FSL21:	.string	10," DELFINO_WAKE_UP_COMPLETED_CMD : %ld",10,0
	.align	2
$C$FSL22:	.string	13,10," SPI Error Check",10,0
	.align	2
$C$FSL23:	.string	13,10," MBD_USER_ABORT_PROCESS started",10,0
	.align	2
$C$FSL24:	.string	13,10," MBD_USER_ABORT_PROCESS Completed",10,0
	.align	2
$C$FSL25:	.string	13,10," GLASS_TUBE_FILLING_STARTED",10,0
	.align	2
$C$FSL26:	.string	13,10," GLASS_TUBE_FILLING_COMPLETED",10,0
	.align	2
$C$FSL27:	.string	13,10," COUNT_FAIL_SEQUNCE_STARTED",10,0
	.align	2
$C$FSL28:	.string	13,10," COUNT_FAIL_SEQUNCE_COMPLETED",10,0
	.align	2
$C$FSL29:	.string	13,10," HYPO_CLEANER_FAIL_SEQUNCE_STARTED",10,0
	.align	2
$C$FSL30:	.string	13,10," HYPO_CLEANER_FAIL_SEQUNCE_COMPLETED",10,0
	.align	2
$C$FSL31:	.string	10," DELFINO_PRIME_ALL_COMPLETED_CMD : %ld",10,0
	.align	2
$C$FSL32:	.string	13,10," DELFINO_SEQUENCE_STARTED : %ld",10,0
	.align	2
$C$FSL33:	.string	13,10," SETTINGS_BUBBLING_TIME_SEQUENCE_HANDLING ",10,0
	.align	2
$C$FSL34:	.string	13,10," arrusnData[0] %d",10,0
	.align	2
$C$FSL35:	.string	13,10," arrusnData[1] %d",10,0
	.align	2
$C$FSL36:	.string	13,10," arrusnData[2] %d",10,0
	.align	2
$C$FSL37:	.string	13,10," arrusnData[3] %d",10,0
	.align	2
$C$FSL38:	.string	13,10," arrusnData[4] %d",10,0
	.align	2
$C$FSL39:	.string	13,10," arrusnData[5] %d",10,0
	.align	2
$C$FSL40:	.string	13,10," arrusnData[6] %d",10,0
	.align	2
$C$FSL41:	.string	13,10," arrusnData[7] %d",10,0
	.align	2
$C$FSL42:	.string	13,10," arrusnData[8] %d",10,0
	.align	2
$C$FSL43:	.string	13,10," Default Vacuum: %d",10,0
	.align	2
$C$FSL44:	.string	13,10," SITARA_PRESSURE_CALIBRATION_CMD received",10,0
	.align	2
$C$FSL45:	.string	13,10," SITARA_FLOW_CALIBRATION_RESULTS_CMD ",10,0
	.align	2
$C$FSL46:	.string	13,10," g_usnWbcCalibratedTime : %d",10,0
	.align	2
$C$FSL47:	.string	13,10," g_usnRbcCalibratedTime : %d",10,0
	.align	2
$C$FSL48:	.string	13,10," SITARA_TEST_DATA_FROM_UI ",10,0
	.align	2
$C$FSL49:	.string	10," DELFINO_ACK_SUB_CMD ",10,0
	.align	2
$C$FSL50:	.string	13,10," SITARA_WBC_RAW_DATA_1SEC rcd",10,0
	.align	2
$C$FSL51:	.string	13,10," SITARA_WBC_RAW_DATA_1SEC in snt ",10,0
	.align	2
$C$FSL52:	.string	13,10," SITARA_RBC_RAW_DATA_1SEC in rcd",10,0
	.align	2
$C$FSL53:	.string	13,10," SITARA_RBC_RAW_DATA_1SEC in snt",10,0
	.align	2
$C$FSL54:	.string	13,10," SITARA_PLT_RAW_DATA_1SEC in cmr",10,0
	.align	2
$C$FSL55:	.string	10," SITARA_PULSE_HEIGHT_DATA_received 2",10,0
	.align	2
$C$FSL56:	.string	10," SITARA_PULSE_HEIGHT_DATA_received 3",10,0
	.align	2
$C$FSL57:	.string	13,10," Arrest From Master",10,0
	.align	2
$C$FSL58:	.string	13,10," SITARA_WASTE_BIN_REPLACED_CMD received ",10,0
	.align	2
$C$FSL59:	.string	13,10," SITARA_SLEEP_SEQUENCE_CMD ",10,0
	.align	2
$C$FSL60:	.string	13,10," SITARA_INITIATE_ZAP Receiced",10,0
	.align	2
$C$FSL61:	.string	10," PRIME_ALL_SERVICE_HANDLING started: %ld",10,0
	.align	2
$C$FSL62:	.string	10," Initial SITARA_NORMAL_STARTUP_CMD received ",10,0
	.align	2
$C$FSL63:	.string	10," Normal internal STARTUP_CMD received",10,0
	.align	2
$C$FSL64:	.string	10," SITARA_STARTUP_FAILED_CMD ",10,0
	.align	2
$C$FSL65:	.string	10," STARTUP_FROM_SERVICE_SCREEN_CMD received",10,0
	.align	2
$C$FSL66:	.string	10," SITARA_ERROR_REQUEST ",10,0
;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_SysInitValveTest
	.global	_SPIClearCommCtrlBusyInt
	.global	_SysInitWasteBinReplaced
	.global	_PWMParamDefaultState
	.global	_SysReInit
	.global	_UartDebugPrint
	.global	_BloodCellCounter_initialize
	.global	_CPFrameDataPacket
	.global	_SPISetCommCtrlBusyInt
	.global	_CPReInitialiseOnNewMeasurement
	.global	_SPIReConfigCommDmaReg
	.global	_PWMHgbPwmControl
	.global	_SysMotorControlIdleMode
	.global	_SysInitProbeCleanArrestSequence
	.global	_m_bSequenceCompleted
	.global	_g_usnFirstDilutionFreq
	.global	_g_usnFinalRBCMixingFreq
	.global	_g_bMonDiluentPriming
	.global	_g_bBusyBitStatus
	.global	_g_usnLyseMixingFreq
	.global	_m_bTxPendingFlag
	.global	_m_bTxToDMAFlag
	.global	_m_bPressureMonitor
	.global	_g_bDataTransmitted
	.global	_g_bWasteDetectCheck
	.global	_m_bSPIWriteControlStatus
	.global	_g_bMonRinsePriming
	.global	_m_eCycleCompletion
	.global	_m_bReadyforAspCommand
	.global	_g_usnDataMode
	.global	_g_bMonLysePriming
	.global	_m_ucValveControlTime
	.global	_g_LyseMixTime
	.global	_g_usnWholeBloodZapTime
	.global	_g_DiluentMixTime
	.global	_g_usnWbcCalibratedTime
	.global	_g_RBCMixTime
	.global	_sprintf
	.global	_g_bYMotorFaultMonEn
	.global	_m_bVlaveCheckStatus
	.global	_g_usnRbcCalibratedTime
	.global	_g_TestDataI0
	.global	_g_bLowVolumeDisp
	.global	_Counter
	.global	_g_TestDataI2
	.global	_g_TestDataI1
	.global	_g_unSequenceStateTime1
	.global	_g_unSequenceStateTime
	.global	_sncalZeroPSI
	.global	_utErrorInfoCheck
	.global	_BloodCellCounter_U
	.global	_DebugPrintBuf
	.global	_m_stPrevTxPacket
	.global	_m_stReceivedPacketFrame
	.global	I$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x17)
$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$498, DW_AT_name("LogicEnable")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_LogicEnable")
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$499, DW_AT_name("StopMotorYaxis")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_StopMotorYaxis")
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$500, DW_AT_name("StopMotorXaxis")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_StopMotorXaxis")
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$501, DW_AT_name("StopMotorLyseSyringe")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_StopMotorLyseSyringe")
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$502, DW_AT_name("StopMotorWasteSyringe")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_StopMotorWasteSyringe")
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$503, DW_AT_name("StopMotorDiluentSyringe")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_StopMotorDiluentSyringe")
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$504, DW_AT_name("CountStop")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_CountStop")
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$505, DW_AT_name("ValveOpenStart")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_ValveOpenStart")
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$506, DW_AT_name("StartAspiration")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_StartAspiration")
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$507, DW_AT_name("ZapComplete")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_ZapComplete")
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$508, DW_AT_name("PopUpCmd")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_PopUpCmd")
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$509, DW_AT_name("MajorCmd")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_MajorCmd")
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$510, DW_AT_name("MinorCmd")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_MinorCmd")
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$511, DW_AT_name("InBubblingFrequency")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_InBubblingFrequency")
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$512, DW_AT_name("HomeStatus")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_HomeStatus")
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$513, DW_AT_name("Pressure")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_Pressure")
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$514, DW_AT_name("Delay_BeforeCount")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_Delay_BeforeCount")
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$26

$C$DW$T$33	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$33, DW_AT_name("ExternalInputs_BloodCellCount_T")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)


$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_name("SSPIPacketFrame")
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x200)
$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$515, DW_AT_name("usnStartOfPacket_MSW")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_usnStartOfPacket_MSW")
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$516, DW_AT_name("usnStartOfPacket_LSW")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_usnStartOfPacket_LSW")
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$517, DW_AT_name("usnReservedWord")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_usnReservedWord")
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$518, DW_AT_name("usnDataLength")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_usnDataLength")
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$519, DW_AT_name("usnMajorCommand")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_usnMajorCommand")
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$520, DW_AT_name("usnMinorCommand")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_usnMinorCommand")
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$521, DW_AT_name("arrusnData")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_arrusnData")
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$522, DW_AT_name("usnRemainingPacketsforTX")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_usnRemainingPacketsforTX")
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x1fc]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$523, DW_AT_name("usnCRC")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_usnCRC")
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x1fd]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$524, DW_AT_name("usnEndOfPacket_MSW")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_usnEndOfPacket_MSW")
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x1fe]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$525, DW_AT_name("usnEndOfPacket_LSW")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_usnEndOfPacket_LSW")
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x1ff]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$29

$C$DW$T$34	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$34, DW_AT_name("ST_SPI_PACKET_FRAME")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)


$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("ST_ERROR_INFO")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x06)
$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$526, DW_AT_name("ulnSequenceAborted")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_ulnSequenceAborted")
	.dwattr $C$DW$526, DW_AT_bit_offset(0x3f)
	.dwattr $C$DW$526, DW_AT_bit_size(0x01)
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$527, DW_AT_name("ulnX_Motor")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_ulnX_Motor")
	.dwattr $C$DW$527, DW_AT_bit_offset(0x3e)
	.dwattr $C$DW$527, DW_AT_bit_size(0x01)
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$528, DW_AT_name("ulnDiluentSyringe")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_ulnDiluentSyringe")
	.dwattr $C$DW$528, DW_AT_bit_offset(0x3d)
	.dwattr $C$DW$528, DW_AT_bit_size(0x01)
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$529, DW_AT_name("ulnSampleSyringe")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_ulnSampleSyringe")
	.dwattr $C$DW$529, DW_AT_bit_offset(0x3c)
	.dwattr $C$DW$529, DW_AT_bit_size(0x01)
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$530, DW_AT_name("ulnWasteSyringe")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_ulnWasteSyringe")
	.dwattr $C$DW$530, DW_AT_bit_offset(0x3b)
	.dwattr $C$DW$530, DW_AT_bit_size(0x01)
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$531, DW_AT_name("ulnY_Motor")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_ulnY_Motor")
	.dwattr $C$DW$531, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$531, DW_AT_bit_size(0x01)
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$532, DW_AT_name("ulnVacWBCDraining")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_ulnVacWBCDraining")
	.dwattr $C$DW$532, DW_AT_bit_offset(0x39)
	.dwattr $C$DW$532, DW_AT_bit_size(0x01)
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$533, DW_AT_name("ulnVacRBCDraining")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_ulnVacRBCDraining")
	.dwattr $C$DW$533, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$533, DW_AT_bit_size(0x01)
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$534, DW_AT_name("ulnVacRBCDrainingEnd")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_ulnVacRBCDrainingEnd")
	.dwattr $C$DW$534, DW_AT_bit_offset(0x37)
	.dwattr $C$DW$534, DW_AT_bit_size(0x01)
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$535, DW_AT_name("ulnPresWBCBubblingStart")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_ulnPresWBCBubblingStart")
	.dwattr $C$DW$535, DW_AT_bit_offset(0x36)
	.dwattr $C$DW$535, DW_AT_bit_size(0x01)
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$536, DW_AT_name("ulnPresWBCBubblingEnd")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_ulnPresWBCBubblingEnd")
	.dwattr $C$DW$536, DW_AT_bit_offset(0x35)
	.dwattr $C$DW$536, DW_AT_bit_size(0x01)
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$537, DW_AT_name("ulnPresWBCBubblingStartLyse")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_ulnPresWBCBubblingStartLyse")
	.dwattr $C$DW$537, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$537, DW_AT_bit_size(0x01)
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$538, DW_AT_name("ulnPresRBCBubblingStartLyse")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_ulnPresRBCBubblingStartLyse")
	.dwattr $C$DW$538, DW_AT_bit_offset(0x33)
	.dwattr $C$DW$538, DW_AT_bit_size(0x01)
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$539, DW_AT_name("ulnPresRBCBubblingStartLyseEnd")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_ulnPresRBCBubblingStartLyseEnd")
	.dwattr $C$DW$539, DW_AT_bit_offset(0x32)
	.dwattr $C$DW$539, DW_AT_bit_size(0x01)
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$540, DW_AT_name("ulnVacCountingStart")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_ulnVacCountingStart")
	.dwattr $C$DW$540, DW_AT_bit_offset(0x31)
	.dwattr $C$DW$540, DW_AT_bit_size(0x01)
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$541, DW_AT_name("ulnVacCountingEnd")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_ulnVacCountingEnd")
	.dwattr $C$DW$541, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$541, DW_AT_bit_size(0x01)
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$542, DW_AT_name("ulnVacWBCDrainingAC")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_ulnVacWBCDrainingAC")
	.dwattr $C$DW$542, DW_AT_bit_offset(0x2f)
	.dwattr $C$DW$542, DW_AT_bit_size(0x01)
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$543, DW_AT_name("ulnVacRBCDrainingAC")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_ulnVacRBCDrainingAC")
	.dwattr $C$DW$543, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$543, DW_AT_bit_size(0x01)
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$544, DW_AT_name("ulnVacRBCDrainingACEnd")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_ulnVacRBCDrainingACEnd")
	.dwattr $C$DW$544, DW_AT_bit_offset(0x2d)
	.dwattr $C$DW$544, DW_AT_bit_size(0x01)
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$545, DW_AT_name("ulnPresBackflushStart")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_ulnPresBackflushStart")
	.dwattr $C$DW$545, DW_AT_bit_offset(0x2c)
	.dwattr $C$DW$545, DW_AT_bit_size(0x01)
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$546, DW_AT_name("ulnPresBackflushEnd")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_ulnPresBackflushEnd")
	.dwattr $C$DW$546, DW_AT_bit_offset(0x2b)
	.dwattr $C$DW$546, DW_AT_bit_size(0x01)
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$547, DW_AT_name("ulnVacWBCDrainingFD")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_ulnVacWBCDrainingFD")
	.dwattr $C$DW$547, DW_AT_bit_offset(0x2a)
	.dwattr $C$DW$547, DW_AT_bit_size(0x01)
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$548, DW_AT_name("ulnVacRBCDrainingFD")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_ulnVacRBCDrainingFD")
	.dwattr $C$DW$548, DW_AT_bit_offset(0x29)
	.dwattr $C$DW$548, DW_AT_bit_size(0x01)
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$549, DW_AT_name("ulnVacRBCDrainingFDEnd")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_ulnVacRBCDrainingFDEnd")
	.dwattr $C$DW$549, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$549, DW_AT_bit_size(0x01)
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$550, DW_AT_name("ulnAspirationNotCompleted")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_ulnAspirationNotCompleted")
	.dwattr $C$DW$550, DW_AT_bit_offset(0x27)
	.dwattr $C$DW$550, DW_AT_bit_size(0x01)
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$551, DW_AT_name("ulnAspirationCompletedImproper")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_ulnAspirationCompletedImproper")
	.dwattr $C$DW$551, DW_AT_bit_offset(0x26)
	.dwattr $C$DW$551, DW_AT_bit_size(0x01)
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$552, DW_AT_name("ulnWBCDispensingNotCompleted")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_ulnWBCDispensingNotCompleted")
	.dwattr $C$DW$552, DW_AT_bit_offset(0x25)
	.dwattr $C$DW$552, DW_AT_bit_size(0x01)
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$553, DW_AT_name("ulnWBCDispensingCompletedImproper")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_ulnWBCDispensingCompletedImproper")
	.dwattr $C$DW$553, DW_AT_bit_offset(0x24)
	.dwattr $C$DW$553, DW_AT_bit_size(0x01)
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$554, DW_AT_name("ulnRBCDispensingNotCompleted")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_ulnRBCDispensingNotCompleted")
	.dwattr $C$DW$554, DW_AT_bit_offset(0x23)
	.dwattr $C$DW$554, DW_AT_bit_size(0x01)
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$555, DW_AT_name("ulnRBCDispensingCompletedImproper")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_ulnRBCDispensingCompletedImproper")
	.dwattr $C$DW$555, DW_AT_bit_offset(0x22)
	.dwattr $C$DW$555, DW_AT_bit_size(0x01)
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$556, DW_AT_name("ulnWBCRBCBubblingNotCompleted")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_ulnWBCRBCBubblingNotCompleted")
	.dwattr $C$DW$556, DW_AT_bit_offset(0x21)
	.dwattr $C$DW$556, DW_AT_bit_size(0x01)
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$557, DW_AT_name("ulnWBCRBCBubblingCompletedImproper")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_ulnWBCRBCBubblingCompletedImproper")
	.dwattr $C$DW$557, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$557, DW_AT_bit_size(0x01)
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$558, DW_AT_name("ulnBackFlushNotCompleted")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_ulnBackFlushNotCompleted")
	.dwattr $C$DW$558, DW_AT_bit_offset(0x1f)
	.dwattr $C$DW$558, DW_AT_bit_size(0x01)
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$559, DW_AT_name("ulnBackFlushCompletedImproper")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_ulnBackFlushCompletedImproper")
	.dwattr $C$DW$559, DW_AT_bit_offset(0x1e)
	.dwattr $C$DW$559, DW_AT_bit_size(0x01)
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$560, DW_AT_name("ulnBathFillingNotCompleted")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_ulnBathFillingNotCompleted")
	.dwattr $C$DW$560, DW_AT_bit_offset(0x1d)
	.dwattr $C$DW$560, DW_AT_bit_size(0x01)
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$561, DW_AT_name("ulnBathFillingCompletedImproper")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_ulnBathFillingCompletedImproper")
	.dwattr $C$DW$561, DW_AT_bit_offset(0x1c)
	.dwattr $C$DW$561, DW_AT_bit_size(0x01)
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$562, DW_AT_name("ulnWasteBinFull")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_ulnWasteBinFull")
	.dwattr $C$DW$562, DW_AT_bit_offset(0x1b)
	.dwattr $C$DW$562, DW_AT_bit_size(0x01)
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$563, DW_AT_name("ulnDiluentEmpty")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_ulnDiluentEmpty")
	.dwattr $C$DW$563, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$563, DW_AT_bit_size(0x01)
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$564, DW_AT_name("ulnLyseEmpty")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_ulnLyseEmpty")
	.dwattr $C$DW$564, DW_AT_bit_offset(0x19)
	.dwattr $C$DW$564, DW_AT_bit_size(0x01)
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$565, DW_AT_name("ulnRinseEmpty")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_ulnRinseEmpty")
	.dwattr $C$DW$565, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$565, DW_AT_bit_size(0x01)
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$566, DW_AT_name("ulnTemperature")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_ulnTemperature")
	.dwattr $C$DW$566, DW_AT_bit_offset(0x17)
	.dwattr $C$DW$566, DW_AT_bit_size(0x01)
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$567, DW_AT_name("uln24V_Check")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_uln24V_Check")
	.dwattr $C$DW$567, DW_AT_bit_offset(0x16)
	.dwattr $C$DW$567, DW_AT_bit_size(0x01)
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$568, DW_AT_name("uln5V_Check")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_uln5V_Check")
	.dwattr $C$DW$568, DW_AT_bit_offset(0x15)
	.dwattr $C$DW$568, DW_AT_bit_size(0x01)
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$569, DW_AT_name("ulnReAcq")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_ulnReAcq")
	.dwattr $C$DW$569, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$569, DW_AT_bit_size(0x01)
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$570, DW_AT_name("ulnPressureSensorFail")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_ulnPressureSensorFail")
	.dwattr $C$DW$570, DW_AT_bit_offset(0x13)
	.dwattr $C$DW$570, DW_AT_bit_size(0x01)
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$571, DW_AT_name("ulnLowVacuum")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_ulnLowVacuum")
	.dwattr $C$DW$571, DW_AT_bit_offset(0x12)
	.dwattr $C$DW$571, DW_AT_bit_size(0x01)
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$572, DW_AT_name("ulnHighPressure")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_ulnHighPressure")
	.dwattr $C$DW$572, DW_AT_bit_offset(0x11)
	.dwattr $C$DW$572, DW_AT_bit_size(0x01)
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$573, DW_AT_name("ulnRBCStartSensor")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_ulnRBCStartSensor")
	.dwattr $C$DW$573, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$573, DW_AT_bit_size(0x01)
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$574, DW_AT_name("ulnRBCStopSensor")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_ulnRBCStopSensor")
	.dwattr $C$DW$574, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$574, DW_AT_bit_size(0x01)
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$575, DW_AT_name("ulnWBCStartSensor")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_ulnWBCStartSensor")
	.dwattr $C$DW$575, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$575, DW_AT_bit_size(0x01)
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$576, DW_AT_name("ulnWBCStopSensor")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_ulnWBCStopSensor")
	.dwattr $C$DW$576, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$576, DW_AT_bit_size(0x01)
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$577, DW_AT_name("ulnSequenceArrested")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_ulnSequenceArrested")
	.dwattr $C$DW$577, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$577, DW_AT_bit_size(0x01)
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$578, DW_AT_name("ulnY_MotorFault")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_ulnY_MotorFault")
	.dwattr $C$DW$578, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$578, DW_AT_bit_size(0x01)
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$579, DW_AT_name("ulnX_MotorFault")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_ulnX_MotorFault")
	.dwattr $C$DW$579, DW_AT_bit_offset(0x0a)
	.dwattr $C$DW$579, DW_AT_bit_size(0x01)
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$580, DW_AT_name("ulnDiluent_MotorFault")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_ulnDiluent_MotorFault")
	.dwattr $C$DW$580, DW_AT_bit_offset(0x09)
	.dwattr $C$DW$580, DW_AT_bit_size(0x01)
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$581, DW_AT_name("ulnSample_MotorFault")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_ulnSample_MotorFault")
	.dwattr $C$DW$581, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$581, DW_AT_bit_size(0x01)
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$582, DW_AT_name("ulnWaste_MotorFault")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_ulnWaste_MotorFault")
	.dwattr $C$DW$582, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$582, DW_AT_bit_size(0x01)
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$583, DW_AT_name("PressureCalCmpltd")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_PressureCalCmpltd")
	.dwattr $C$DW$583, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$583, DW_AT_bit_size(0x01)
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$584, DW_AT_name("ulnReserved")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_ulnReserved")
	.dwattr $C$DW$584, DW_AT_bit_offset(-6)
	.dwattr $C$DW$584, DW_AT_bit_size(0x0c)
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$32, DW_AT_name("UT_ERROR_INFO")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x06)
$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$585, DW_AT_name("stErrorInfo")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_stErrorInfo")
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)

$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$586, DW_AT_name("ulnErrorInfo")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_ulnErrorInfo")
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)

	.dwendtag $C$DW$T$32

$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$587	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$6)

$C$DW$T$56	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$587)

$C$DW$T$19	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$19, DW_AT_name("boolean_T")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)


$C$DW$T$23	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x05)
$C$DW$588	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$588, DW_AT_upper_bound(0x04)

	.dwendtag $C$DW$T$23

$C$DW$T$21	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$21, DW_AT_name("uint8_T")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)

$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("int16_T")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$20	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$20, DW_AT_name("uint16_T")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)


$C$DW$T$22	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$589	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$589, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$22

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)

$C$DW$T$37	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$37, DW_AT_address_class(0x20)

$C$DW$T$63	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$63, DW_AT_name("int16_t")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)

$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)

$C$DW$T$40	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$40, DW_AT_name("Uint16")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)

$C$DW$T$27	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$27, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)


$C$DW$T$28	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x1f6)
$C$DW$590	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$590, DW_AT_upper_bound(0x1f5)

	.dwendtag $C$DW$T$28

$C$DW$591	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$27)

$C$DW$T$66	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$591)

$C$DW$T$24	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$24, DW_AT_name("uint32_T")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$67	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$67, DW_AT_name("Uint32")
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)

$C$DW$T$68	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$68, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$68, DW_AT_language(DW_LANG_C)

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)

$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$30	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$30, DW_AT_name("Uint64")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)

$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

$C$DW$T$41	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$41, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$41, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$41, DW_AT_byte_size(0x01)

$C$DW$T$42	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$42, DW_AT_name("bool_t")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)

$C$DW$592	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$42)

$C$DW$T$125	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$592)

$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x20)

$C$DW$593	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$5)

$C$DW$T$59	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$593)

$C$DW$T$60	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$T$60, DW_AT_address_class(0x20)


$C$DW$T$69	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$69, DW_AT_byte_size(0x1f4)
$C$DW$594	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$594, DW_AT_upper_bound(0x1f3)

	.dwendtag $C$DW$T$69


$C$DW$T$70	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$70, DW_AT_byte_size(0x32)
$C$DW$595	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$595, DW_AT_upper_bound(0x31)

	.dwendtag $C$DW$T$70


$C$DW$T$126	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$126, DW_AT_name("eCellType")
	.dwattr $C$DW$T$126, DW_AT_byte_size(0x01)
$C$DW$596	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$596, DW_AT_name("ecellInvalid")
	.dwattr $C$DW$596, DW_AT_const_value(0x00)

$C$DW$597	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$597, DW_AT_name("eCellRBC")
	.dwattr $C$DW$597, DW_AT_const_value(0x01)

$C$DW$598	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$598, DW_AT_name("eCellPLT")
	.dwattr $C$DW$598, DW_AT_const_value(0x02)

$C$DW$599	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$599, DW_AT_name("eCellWBC")
	.dwattr $C$DW$599, DW_AT_const_value(0x03)

	.dwendtag $C$DW$T$126

$C$DW$T$127	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$127, DW_AT_name("E_CELL_TYPE")
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$T$127, DW_AT_language(DW_LANG_C)

$C$DW$600	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$127)

$C$DW$T$128	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$600)


$C$DW$T$129	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$129, DW_AT_name("eCycleCompletion")
	.dwattr $C$DW$T$129, DW_AT_byte_size(0x01)
$C$DW$601	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$601, DW_AT_name("eDefault")
	.dwattr $C$DW$601, DW_AT_const_value(0x00)

$C$DW$602	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$602, DW_AT_name("eRESULT_TRANSMISSION")
	.dwattr $C$DW$602, DW_AT_const_value(0x01)

$C$DW$603	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$603, DW_AT_name("eERROR_CHECK")
	.dwattr $C$DW$603, DW_AT_const_value(0x02)

$C$DW$604	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$604, DW_AT_name("eSEQ_COMPLETION")
	.dwattr $C$DW$604, DW_AT_const_value(0x03)

$C$DW$605	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$605, DW_AT_name("eERROR_TRANSMISSION")
	.dwattr $C$DW$605, DW_AT_const_value(0x04)

$C$DW$606	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$606, DW_AT_name("eERROR_COMPLETION")
	.dwattr $C$DW$606, DW_AT_const_value(0x05)

$C$DW$607	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$607, DW_AT_name("eDELAY_CASE")
	.dwattr $C$DW$607, DW_AT_const_value(0x06)

	.dwendtag $C$DW$T$129

$C$DW$T$130	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$130, DW_AT_name("E_CYCLE_COMPLETION")
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$T$130, DW_AT_language(DW_LANG_C)

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 26
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 28
	.dwcfi	same_value, 6
	.dwcfi	same_value, 7
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 49
	.dwcfi	same_value, 50
	.dwcfi	same_value, 51
	.dwcfi	same_value, 52
	.dwcfi	same_value, 53
	.dwcfi	same_value, 54
	.dwcfi	same_value, 55
	.dwcfi	same_value, 56
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$608	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$608, DW_AT_name("AL")
	.dwattr $C$DW$608, DW_AT_location[DW_OP_reg0]

$C$DW$609	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$609, DW_AT_name("AH")
	.dwattr $C$DW$609, DW_AT_location[DW_OP_reg1]

$C$DW$610	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$610, DW_AT_name("PL")
	.dwattr $C$DW$610, DW_AT_location[DW_OP_reg2]

$C$DW$611	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$611, DW_AT_name("PH")
	.dwattr $C$DW$611, DW_AT_location[DW_OP_reg3]

$C$DW$612	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$612, DW_AT_name("SP")
	.dwattr $C$DW$612, DW_AT_location[DW_OP_reg20]

$C$DW$613	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$613, DW_AT_name("XT")
	.dwattr $C$DW$613, DW_AT_location[DW_OP_reg21]

$C$DW$614	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$614, DW_AT_name("T")
	.dwattr $C$DW$614, DW_AT_location[DW_OP_reg22]

$C$DW$615	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$615, DW_AT_name("ST0")
	.dwattr $C$DW$615, DW_AT_location[DW_OP_reg23]

$C$DW$616	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$616, DW_AT_name("ST1")
	.dwattr $C$DW$616, DW_AT_location[DW_OP_reg24]

$C$DW$617	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$617, DW_AT_name("PC")
	.dwattr $C$DW$617, DW_AT_location[DW_OP_reg25]

$C$DW$618	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$618, DW_AT_name("RPC")
	.dwattr $C$DW$618, DW_AT_location[DW_OP_reg26]

$C$DW$619	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$619, DW_AT_name("FP")
	.dwattr $C$DW$619, DW_AT_location[DW_OP_reg28]

$C$DW$620	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$620, DW_AT_name("DP")
	.dwattr $C$DW$620, DW_AT_location[DW_OP_reg29]

$C$DW$621	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$621, DW_AT_name("SXM")
	.dwattr $C$DW$621, DW_AT_location[DW_OP_reg30]

$C$DW$622	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$622, DW_AT_name("PM")
	.dwattr $C$DW$622, DW_AT_location[DW_OP_reg31]

$C$DW$623	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$623, DW_AT_name("OVM")
	.dwattr $C$DW$623, DW_AT_location[DW_OP_regx 0x20]

$C$DW$624	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$624, DW_AT_name("PAGE0")
	.dwattr $C$DW$624, DW_AT_location[DW_OP_regx 0x21]

$C$DW$625	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$625, DW_AT_name("AMODE")
	.dwattr $C$DW$625, DW_AT_location[DW_OP_regx 0x22]

$C$DW$626	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$626, DW_AT_name("INTM")
	.dwattr $C$DW$626, DW_AT_location[DW_OP_regx 0x23]

$C$DW$627	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$627, DW_AT_name("IFR")
	.dwattr $C$DW$627, DW_AT_location[DW_OP_regx 0x24]

$C$DW$628	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$628, DW_AT_name("IER")
	.dwattr $C$DW$628, DW_AT_location[DW_OP_regx 0x25]

$C$DW$629	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$629, DW_AT_name("V")
	.dwattr $C$DW$629, DW_AT_location[DW_OP_regx 0x26]

$C$DW$630	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$630, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$630, DW_AT_location[DW_OP_regx 0x4c]

$C$DW$631	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$631, DW_AT_name("VOL")
	.dwattr $C$DW$631, DW_AT_location[DW_OP_regx 0x4d]

$C$DW$632	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$632, DW_AT_name("AR0")
	.dwattr $C$DW$632, DW_AT_location[DW_OP_reg4]

$C$DW$633	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$633, DW_AT_name("XAR0")
	.dwattr $C$DW$633, DW_AT_location[DW_OP_reg5]

$C$DW$634	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$634, DW_AT_name("AR1")
	.dwattr $C$DW$634, DW_AT_location[DW_OP_reg6]

$C$DW$635	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$635, DW_AT_name("XAR1")
	.dwattr $C$DW$635, DW_AT_location[DW_OP_reg7]

$C$DW$636	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$636, DW_AT_name("AR2")
	.dwattr $C$DW$636, DW_AT_location[DW_OP_reg8]

$C$DW$637	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$637, DW_AT_name("XAR2")
	.dwattr $C$DW$637, DW_AT_location[DW_OP_reg9]

$C$DW$638	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$638, DW_AT_name("AR3")
	.dwattr $C$DW$638, DW_AT_location[DW_OP_reg10]

$C$DW$639	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$639, DW_AT_name("XAR3")
	.dwattr $C$DW$639, DW_AT_location[DW_OP_reg11]

$C$DW$640	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$640, DW_AT_name("AR4")
	.dwattr $C$DW$640, DW_AT_location[DW_OP_reg12]

$C$DW$641	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$641, DW_AT_name("XAR4")
	.dwattr $C$DW$641, DW_AT_location[DW_OP_reg13]

$C$DW$642	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$642, DW_AT_name("AR5")
	.dwattr $C$DW$642, DW_AT_location[DW_OP_reg14]

$C$DW$643	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$643, DW_AT_name("XAR5")
	.dwattr $C$DW$643, DW_AT_location[DW_OP_reg15]

$C$DW$644	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$644, DW_AT_name("AR6")
	.dwattr $C$DW$644, DW_AT_location[DW_OP_reg16]

$C$DW$645	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$645, DW_AT_name("XAR6")
	.dwattr $C$DW$645, DW_AT_location[DW_OP_reg17]

$C$DW$646	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$646, DW_AT_name("AR7")
	.dwattr $C$DW$646, DW_AT_location[DW_OP_reg18]

$C$DW$647	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$647, DW_AT_name("XAR7")
	.dwattr $C$DW$647, DW_AT_location[DW_OP_reg19]

$C$DW$648	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$648, DW_AT_name("R0HL")
	.dwattr $C$DW$648, DW_AT_location[DW_OP_regx 0x29]

$C$DW$649	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$649, DW_AT_name("R0H")
	.dwattr $C$DW$649, DW_AT_location[DW_OP_regx 0x2a]

$C$DW$650	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$650, DW_AT_name("R1HL")
	.dwattr $C$DW$650, DW_AT_location[DW_OP_regx 0x2b]

$C$DW$651	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$651, DW_AT_name("R1H")
	.dwattr $C$DW$651, DW_AT_location[DW_OP_regx 0x2c]

$C$DW$652	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$652, DW_AT_name("R2HL")
	.dwattr $C$DW$652, DW_AT_location[DW_OP_regx 0x2d]

$C$DW$653	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$653, DW_AT_name("R2H")
	.dwattr $C$DW$653, DW_AT_location[DW_OP_regx 0x2e]

$C$DW$654	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$654, DW_AT_name("R3HL")
	.dwattr $C$DW$654, DW_AT_location[DW_OP_regx 0x2f]

$C$DW$655	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$655, DW_AT_name("R3H")
	.dwattr $C$DW$655, DW_AT_location[DW_OP_regx 0x30]

$C$DW$656	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$656, DW_AT_name("R4HL")
	.dwattr $C$DW$656, DW_AT_location[DW_OP_regx 0x31]

$C$DW$657	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$657, DW_AT_name("R4H")
	.dwattr $C$DW$657, DW_AT_location[DW_OP_regx 0x32]

$C$DW$658	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$658, DW_AT_name("R5HL")
	.dwattr $C$DW$658, DW_AT_location[DW_OP_regx 0x33]

$C$DW$659	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$659, DW_AT_name("R5H")
	.dwattr $C$DW$659, DW_AT_location[DW_OP_regx 0x34]

$C$DW$660	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$660, DW_AT_name("R6HL")
	.dwattr $C$DW$660, DW_AT_location[DW_OP_regx 0x35]

$C$DW$661	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$661, DW_AT_name("R6H")
	.dwattr $C$DW$661, DW_AT_location[DW_OP_regx 0x36]

$C$DW$662	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$662, DW_AT_name("R7HL")
	.dwattr $C$DW$662, DW_AT_location[DW_OP_regx 0x37]

$C$DW$663	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$663, DW_AT_name("R7H")
	.dwattr $C$DW$663, DW_AT_location[DW_OP_regx 0x38]

$C$DW$664	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$664, DW_AT_name("RBL")
	.dwattr $C$DW$664, DW_AT_location[DW_OP_regx 0x49]

$C$DW$665	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$665, DW_AT_name("RB")
	.dwattr $C$DW$665, DW_AT_location[DW_OP_regx 0x4a]

$C$DW$666	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$666, DW_AT_name("STFL")
	.dwattr $C$DW$666, DW_AT_location[DW_OP_regx 0x27]

$C$DW$667	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$667, DW_AT_name("STF")
	.dwattr $C$DW$667, DW_AT_location[DW_OP_regx 0x28]

$C$DW$668	.dwtag  DW_TAG_TI_assign_register
	.dwattr $C$DW$668, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$668, DW_AT_location[DW_OP_reg27]

	.dwendtag $C$DW$CU


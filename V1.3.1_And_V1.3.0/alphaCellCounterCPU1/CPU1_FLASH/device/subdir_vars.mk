################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../device/2837xD_FLASH_lnk_cpu1_far.cmd \
../device/F2837xD_Headers_nonBIOS_cpu1.cmd 

ASM_SRCS += \
../device/F2837xD_CodeStartBranch.asm \
../device/F2837xD_usDelay.asm \
../device/memcpy_fast_far.asm 

C_SRCS += \
../device/F2837xD_Adc.c \
../device/F2837xD_CpuTimers.c \
../device/F2837xD_DefaultISR.c \
../device/F2837xD_Dma.c \
../device/F2837xD_EPwm.c \
../device/F2837xD_Emif.c \
../device/F2837xD_GlobalVariableDefs.c \
../device/F2837xD_Gpio.c \
../device/F2837xD_Ipc.c \
../device/F2837xD_Ipc_Driver.c \
../device/F2837xD_Ipc_Driver_Lite.c \
../device/F2837xD_Ipc_Driver_Util.c \
../device/F2837xD_PieCtrl.c \
../device/F2837xD_PieVect.c \
../device/F2837xD_Sci.c \
../device/F2837xD_SysCtrl.c \
../device/F2837xD_sci_io.c \
../device/sysctl.c 

C_DEPS += \
./device/F2837xD_Adc.d \
./device/F2837xD_CpuTimers.d \
./device/F2837xD_DefaultISR.d \
./device/F2837xD_Dma.d \
./device/F2837xD_EPwm.d \
./device/F2837xD_Emif.d \
./device/F2837xD_GlobalVariableDefs.d \
./device/F2837xD_Gpio.d \
./device/F2837xD_Ipc.d \
./device/F2837xD_Ipc_Driver.d \
./device/F2837xD_Ipc_Driver_Lite.d \
./device/F2837xD_Ipc_Driver_Util.d \
./device/F2837xD_PieCtrl.d \
./device/F2837xD_PieVect.d \
./device/F2837xD_Sci.d \
./device/F2837xD_SysCtrl.d \
./device/F2837xD_sci_io.d \
./device/sysctl.d 

OBJS += \
./device/F2837xD_Adc.obj \
./device/F2837xD_CodeStartBranch.obj \
./device/F2837xD_CpuTimers.obj \
./device/F2837xD_DefaultISR.obj \
./device/F2837xD_Dma.obj \
./device/F2837xD_EPwm.obj \
./device/F2837xD_Emif.obj \
./device/F2837xD_GlobalVariableDefs.obj \
./device/F2837xD_Gpio.obj \
./device/F2837xD_Ipc.obj \
./device/F2837xD_Ipc_Driver.obj \
./device/F2837xD_Ipc_Driver_Lite.obj \
./device/F2837xD_Ipc_Driver_Util.obj \
./device/F2837xD_PieCtrl.obj \
./device/F2837xD_PieVect.obj \
./device/F2837xD_Sci.obj \
./device/F2837xD_SysCtrl.obj \
./device/F2837xD_sci_io.obj \
./device/F2837xD_usDelay.obj \
./device/memcpy_fast_far.obj \
./device/sysctl.obj 

ASM_DEPS += \
./device/F2837xD_CodeStartBranch.d \
./device/F2837xD_usDelay.d \
./device/memcpy_fast_far.d 

OBJS__QUOTED += \
"device\F2837xD_Adc.obj" \
"device\F2837xD_CodeStartBranch.obj" \
"device\F2837xD_CpuTimers.obj" \
"device\F2837xD_DefaultISR.obj" \
"device\F2837xD_Dma.obj" \
"device\F2837xD_EPwm.obj" \
"device\F2837xD_Emif.obj" \
"device\F2837xD_GlobalVariableDefs.obj" \
"device\F2837xD_Gpio.obj" \
"device\F2837xD_Ipc.obj" \
"device\F2837xD_Ipc_Driver.obj" \
"device\F2837xD_Ipc_Driver_Lite.obj" \
"device\F2837xD_Ipc_Driver_Util.obj" \
"device\F2837xD_PieCtrl.obj" \
"device\F2837xD_PieVect.obj" \
"device\F2837xD_Sci.obj" \
"device\F2837xD_SysCtrl.obj" \
"device\F2837xD_sci_io.obj" \
"device\F2837xD_usDelay.obj" \
"device\memcpy_fast_far.obj" \
"device\sysctl.obj" 

C_DEPS__QUOTED += \
"device\F2837xD_Adc.d" \
"device\F2837xD_CpuTimers.d" \
"device\F2837xD_DefaultISR.d" \
"device\F2837xD_Dma.d" \
"device\F2837xD_EPwm.d" \
"device\F2837xD_Emif.d" \
"device\F2837xD_GlobalVariableDefs.d" \
"device\F2837xD_Gpio.d" \
"device\F2837xD_Ipc.d" \
"device\F2837xD_Ipc_Driver.d" \
"device\F2837xD_Ipc_Driver_Lite.d" \
"device\F2837xD_Ipc_Driver_Util.d" \
"device\F2837xD_PieCtrl.d" \
"device\F2837xD_PieVect.d" \
"device\F2837xD_Sci.d" \
"device\F2837xD_SysCtrl.d" \
"device\F2837xD_sci_io.d" \
"device\sysctl.d" 

ASM_DEPS__QUOTED += \
"device\F2837xD_CodeStartBranch.d" \
"device\F2837xD_usDelay.d" \
"device\memcpy_fast_far.d" 

C_SRCS__QUOTED += \
"../device/F2837xD_Adc.c" \
"../device/F2837xD_CpuTimers.c" \
"../device/F2837xD_DefaultISR.c" \
"../device/F2837xD_Dma.c" \
"../device/F2837xD_EPwm.c" \
"../device/F2837xD_Emif.c" \
"../device/F2837xD_GlobalVariableDefs.c" \
"../device/F2837xD_Gpio.c" \
"../device/F2837xD_Ipc.c" \
"../device/F2837xD_Ipc_Driver.c" \
"../device/F2837xD_Ipc_Driver_Lite.c" \
"../device/F2837xD_Ipc_Driver_Util.c" \
"../device/F2837xD_PieCtrl.c" \
"../device/F2837xD_PieVect.c" \
"../device/F2837xD_Sci.c" \
"../device/F2837xD_SysCtrl.c" \
"../device/F2837xD_sci_io.c" \
"../device/sysctl.c" 

ASM_SRCS__QUOTED += \
"../device/F2837xD_CodeStartBranch.asm" \
"../device/F2837xD_usDelay.asm" \
"../device/memcpy_fast_far.asm" 


